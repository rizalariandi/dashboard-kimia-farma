-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 05:04 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datamart_kf`
--

-- --------------------------------------------------------

--
-- Table structure for table `crm_cj`
--

CREATE TABLE `crm_cj` (
  `cc_id` int(11) NOT NULL,
  `cc_id_customer` int(11) NOT NULL,
  `cc_step` varchar(100) NOT NULL,
  `cc_total` int(11) NOT NULL,
  `cc_probability` varchar(10) NOT NULL,
  `cc_close_date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crm_customers`
--

CREATE TABLE `crm_customers` (
  `ct_id` int(20) NOT NULL,
  `ct_no` varchar(20) NOT NULL,
  `ct_name` varchar(100) NOT NULL,
  `ct_cat` varchar(100) NOT NULL,
  `ct_address` varchar(255) NOT NULL,
  `ct_city` varchar(100) NOT NULL,
  `ct_sales` bigint(20) NOT NULL,
  `ct_salesman` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crm_customers`
--

INSERT INTO `crm_customers` (`ct_id`, `ct_no`, `ct_name`, `ct_cat`, `ct_address`, `ct_city`, `ct_sales`, `ct_salesman`) VALUES
(1, '', 'ABDUL WACHID/AP. SIDO WARAS (JPR)', 'Apotek', 'JL. MAYONG', 'JEPARA', 1462000, 'ADI PRAYOGO ,S.KOM'),
(2, '', 'AP RAUDAH FARMA(BRB)', 'Apotek', 'JL IR P H M NOOR SIMPANG 4', 'BARABAI', 45090000, 'RISKAN KUSWANA'),
(3, '', 'AP. AMANDIT', 'Apotek', 'JL. KAM MEL DA', 'BANJARMASIN', 2739000, 'AHMAD RAHMANI'),
(4, '', 'AP. PRIMA - TSK', 'Apotek', 'JL. KHZ. MUSTOFA', 'TASIKMALAYA', 3270000, 'TANTAN TRISTIAN'),
(5, '', 'AP.NURUL FIKRI', 'Apotek', 'JL.BHAKTI NO.117 RT.06/10 TUGU', 'BOGOR', 2640000, 'RENDY PUTRA PERMADI'),
(6, '', 'APOTEK 39  (KDS)', 'Apotek', 'JL. SUNAN KUDUS NO.39', 'KUDUS', 3630000, 'PRIADI'),
(7, '', 'APOTEK ASIH HUSADA', 'Apotek', 'JL. PASANAH RT. 16', 'PANGKALAN BUN', 183486000, 'SELAMAT HERIYADI'),
(8, '', 'APOTEK BANDUNG', 'Apotek', 'JL.PASAR BARU II NO.2', 'TASIKMALAYA', 2220000, 'TANTAN TRISTIAN'),
(9, '', 'APOTEK JESAYA FARMA', 'Apotek', 'JL. HENDRO SURATMIN NO.179B SUKARAME- BANDAR LAMPUNG', 'BANDAR LAMPUNG', 3216000, 'M. IRFANSYAH'),
(10, '', 'APOTEK K-24 (BJM)', 'Apotek', 'JL P HIDAYATULLAH NO.4 RT.15 RW.01', 'BANJARMASIN', 4022000, 'AHMAD RAHMANI'),
(11, '', 'GROSIR NUSA INDAH', 'Apotek', 'JL NANGKA DEPAN PUSKESMAS OEBA', 'KUPANG', 149817000, ''),
(12, '', 'PT. BUNDA MEDIK(AP. BUNDA MARGONDA)', 'Rumah Sakit', 'JL. MARGONDA RAYA NO. 28 RT.02', 'BOGOR', 8104000, 'ARDHY WICAKSONO'),
(13, '', 'PT. KIMIA FARMA APOTEK', 'Apotek', 'JL. BUDI UTOMO NO.1', 'JAKARTA', 15825000, 'IKHSANNUDIN'),
(14, '', 'R S K BEDAH BANJARMASIN SIAGA', 'Rumah Sakit', 'JL. A. YANI KM 4,5 NO. 59 RT 3', 'BANJARMASIN', 13152000, 'SUBAIDAH'),
(15, '', 'RS. KAMBANG', 'Rumah Sakit', 'JL. KOL AMIR HAMZAH NO. 53 RT 12 KEL. SELAMAT', 'JAMBI', 46953000, ' AFDAL FIRDAUS'),
(16, '', 'RSU. PAMBALAH BATUNG (RUTIN)', 'Rumah Sakit', 'JL. BASUKI RACHMAT', 'AMUNTAI', 7580000, 'RISKAN KUSWANA'),
(17, '', 'RSUI. MUTIARA BUNDA (BREBES)', 'Rumah Sakit', 'JL. RAYA PANTURA', 'PEKALONGAN', 1092000, 'ASEP ZAENUDIN'),
(18, '', 'TOKO GUNASALMA I', 'Apotek', 'JL. SILIWANGI NO. 184, KAWALI DPN TERMINAL LAMA KAWALI', 'CIAMIS', 5717000, 'SE- YANGGI YUDISTIRA'),
(32, '', 'DEDDY HERMAWAN', 'Dokter', 'Bandung', 'Bandung', 6825299, 'ASEP ZAENUDIN'),
(33, '', 'SUTARYO', 'Dokter', 'Bandung', 'Bandung', 2088686, 'ASEP ZAENUDIN');

-- --------------------------------------------------------

--
-- Table structure for table `demografi_age`
--

CREATE TABLE `demografi_age` (
  `entitas` varchar(5) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `usia` varchar(50) DEFAULT NULL,
  `vol` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demografi_edu`
--

CREATE TABLE `demografi_edu` (
  `entitas` varchar(5) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `vol` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demografi_job`
--

CREATE TABLE `demografi_job` (
  `entitas` varchar(5) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `Level` varchar(50) DEFAULT NULL,
  `vol` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `demografi_work`
--

CREATE TABLE `demografi_work` (
  `entitas` varchar(5) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `masa_kerja` varchar(50) DEFAULT NULL,
  `vol` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_balance_month`
--

CREATE TABLE `fi_balance_month` (
  `entitas` varchar(5) DEFAULT NULL,
  `key` varchar(1) DEFAULT NULL,
  `balance_sheet` varchar(155) DEFAULT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `faktor` varchar(255) DEFAULT NULL,
  `target` varchar(29) DEFAULT NULL,
  `real` decimal(29,0) DEFAULT NULL,
  `amount` decimal(29,0) DEFAULT NULL,
  `amount_ytd` decimal(29,0) DEFAULT NULL,
  `adjusment` varchar(29) DEFAULT NULL,
  `amount_ytd1` decimal(29,0) DEFAULT NULL,
  `ket` varchar(25) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_balance_month1`
--

CREATE TABLE `fi_balance_month1` (
  `entitas` varchar(5) DEFAULT NULL,
  `working_capital` decimal(29,0) DEFAULT NULL,
  `working_capital_ytd` decimal(29,0) DEFAULT NULL,
  `current_ratio` decimal(29,0) DEFAULT NULL,
  `current_ratio_ytd` decimal(29,0) DEFAULT NULL,
  `acid_test_rasio` decimal(29,0) DEFAULT NULL,
  `acid_test_rasio_ytd` decimal(29,0) DEFAULT NULL,
  `cash_rasio` decimal(29,0) DEFAULT NULL,
  `cash_rasio_ytd` decimal(29,0) DEFAULT NULL,
  `der_interest_bearing` decimal(29,0) DEFAULT NULL,
  `der_interest_bearing_ytd` decimal(29,0) DEFAULT NULL,
  `debt_rasio` decimal(29,0) DEFAULT NULL,
  `debt_rasio_ytd` decimal(29,0) DEFAULT NULL,
  `equity_rasio` decimal(29,0) DEFAULT NULL,
  `equity_rasio_ytd` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_balance_month1x`
--

CREATE TABLE `fi_balance_month1x` (
  `entitas` varchar(5) DEFAULT NULL,
  `working_capital` varchar(29) DEFAULT NULL,
  `working_capital_ytd` varchar(29) DEFAULT NULL,
  `current_ratio` varchar(29) DEFAULT NULL,
  `current_ratio_ytd` varchar(29) DEFAULT NULL,
  `acid_test_rasio` varchar(29) DEFAULT NULL,
  `acid_test_rasio_ytd` varchar(29) DEFAULT NULL,
  `cash_rasio` varchar(29) DEFAULT NULL,
  `cash_rasio_ytd` varchar(29) DEFAULT NULL,
  `der_interest_bearing` varchar(29) DEFAULT NULL,
  `der_interest_bearing_ytd` varchar(29) DEFAULT NULL,
  `debt_rasio` varchar(29) DEFAULT NULL,
  `debt_rasio_ytd` varchar(29) DEFAULT NULL,
  `equity_rasio` varchar(29) DEFAULT NULL,
  `equity_rasio_ytd` varchar(29) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_balance_month_ori`
--

CREATE TABLE `fi_balance_month_ori` (
  `entitas` varchar(5) DEFAULT NULL,
  `key` varchar(1) DEFAULT NULL,
  `balance_sheet` varchar(155) DEFAULT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `faktor` varchar(255) DEFAULT NULL,
  `amount_` decimal(29,0) DEFAULT NULL,
  `amount_ytd_` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_bs_month_cron`
--

CREATE TABLE `fi_bs_month_cron` (
  `key` varchar(1) DEFAULT NULL,
  `balance_sheet` varchar(155) DEFAULT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `faktor` varchar(255) DEFAULT NULL,
  `real` double(17,0) DEFAULT NULL,
  `target` double(17,0) DEFAULT NULL,
  `amount` decimal(29,0) DEFAULT NULL,
  `amount_ytd` decimal(29,0) DEFAULT NULL,
  `adjusment` double(17,0) DEFAULT NULL,
  `amount_ytd1` double(17,0) DEFAULT NULL,
  `pengurang` double(17,0) DEFAULT NULL,
  `ket` varchar(16) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_bs_rasio_month_cron`
--

CREATE TABLE `fi_bs_rasio_month_cron` (
  `WORKING_CAPITAL` decimal(52,0) DEFAULT NULL,
  `WORKING_CAPITAL_YTD` double(17,0) DEFAULT NULL,
  `CURRENT_RATIO` decimal(58,4) DEFAULT NULL,
  `CURRENT_RATIO_YTD` double(21,4) DEFAULT NULL,
  `ACID_TEST_RASIO` decimal(59,4) DEFAULT NULL,
  `ACID_TEST_RASIO_YTD` double(21,4) DEFAULT NULL,
  `CASH_RASIO` decimal(58,4) DEFAULT NULL,
  `CASH_RASIO_YTD` double(21,4) DEFAULT NULL,
  `DER_INTEREST_BEARING` decimal(62,4) DEFAULT NULL,
  `DER_INTEREST_BEARING_YTD` double(21,4) DEFAULT NULL,
  `DEBT_RASIO` decimal(58,4) DEFAULT NULL,
  `DEBT_RASIO_YTD` double(21,4) DEFAULT NULL,
  `EQUITY_RASIO` decimal(58,4) DEFAULT NULL,
  `EQUITY_RASIO_YTD` double(21,4) DEFAULT NULL,
  `ENTITAS` varchar(5) DEFAULT NULL,
  `YEAR` int(4) DEFAULT NULL,
  `MONTH` int(2) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_investasi_month`
--

CREATE TABLE `fi_investasi_month` (
  `Id` int(11) NOT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `uraian` varchar(155) DEFAULT NULL,
  `target` decimal(29,0) DEFAULT NULL,
  `realisasi` decimal(29,0) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `achievement` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_is_month`
--

CREATE TABLE `fi_is_month` (
  `month` int(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `key_` varchar(1) DEFAULT NULL,
  `income_statement` varchar(255) DEFAULT NULL,
  `prognosa` decimal(29,0) DEFAULT NULL,
  `target` decimal(29,0) DEFAULT NULL,
  `growth` decimal(29,0) DEFAULT NULL,
  `achievement` decimal(29,0) DEFAULT NULL,
  `realisasi` decimal(29,0) DEFAULT NULL,
  `real_KF` decimal(29,0) DEFAULT NULL,
  `F01_KF` varchar(29) DEFAULT NULL,
  `realisasi_f01` decimal(29,0) DEFAULT NULL,
  `adjusment_` varchar(29) DEFAULT NULL,
  `realisasi_ytd` decimal(29,0) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_is_month_cron`
--

CREATE TABLE `fi_is_month_cron` (
  `realisasi` decimal(29,0) DEFAULT NULL,
  `F01` varchar(29) DEFAULT NULL,
  `Adjusment_` varchar(29) DEFAULT NULL,
  `TARGET` varchar(29) DEFAULT NULL,
  `REAL` varchar(29) DEFAULT NULL,
  `PROGNOSA` varchar(29) DEFAULT NULL,
  `GROWTH` decimal(33,4) DEFAULT NULL,
  `REALISASI_YTD` double(17,0) DEFAULT NULL,
  `pengurang` double(17,0) DEFAULT NULL,
  `Ket` varchar(16) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `ACHIEVEMENT` double DEFAULT NULL,
  `income_statement` varchar(255) DEFAULT NULL,
  `key_` varchar(1) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fi_is_month_ori`
--

CREATE TABLE `fi_is_month_ori` (
  `month` int(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `key_` varchar(1) DEFAULT NULL,
  `income_statement` varchar(255) DEFAULT NULL,
  `realisasi` decimal(29,0) DEFAULT NULL,
  `F01` varchar(29) DEFAULT NULL,
  `realisasi_ytd` decimal(29,0) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `last_update` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_all_cb_temp`
--

CREATE TABLE `f_all_cb_temp` (
  `profit_center` varchar(10) DEFAULT NULL,
  `gl_account` varchar(10) DEFAULT NULL,
  `beginning_balance` decimal(29,0) DEFAULT NULL,
  `ending_balance` decimal(29,0) DEFAULT NULL,
  `entitas` varchar(10) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `month` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ap`
--

CREATE TABLE `f_ap` (
  `vendor_code` varchar(50) DEFAULT NULL,
  `vendor_desc` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `aging_category` varchar(10) DEFAULT NULL,
  `aging_status` varchar(20) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ap_general`
--

CREATE TABLE `f_ap_general` (
  `vendor` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ap_kfho_monthly`
--

CREATE TABLE `f_ap_kfho_monthly` (
  `vendor` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ap_kfho_triwulan`
--

CREATE TABLE `f_ap_kfho_triwulan` (
  `vendor` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ap_kfho_yearly`
--

CREATE TABLE `f_ap_kfho_yearly` (
  `vendor` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ar`
--

CREATE TABLE `f_ar` (
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `aging_category` varchar(10) DEFAULT NULL,
  `aging_status` varchar(20) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ar_general`
--

CREATE TABLE `f_ar_general` (
  `customer` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ar_kfho_monthly`
--

CREATE TABLE `f_ar_kfho_monthly` (
  `customer` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ar_kfho_triwulan`
--

CREATE TABLE `f_ar_kfho_triwulan` (
  `customer` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_ar_kfho_yearly`
--

CREATE TABLE `f_ar_kfho_yearly` (
  `customer` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_cashbank`
--

CREATE TABLE `f_cashbank` (
  `profitctr_code` varchar(50) DEFAULT NULL,
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_cashcollection`
--

CREATE TABLE `f_cashcollection` (
  `datacollection` varchar(50) DEFAULT NULL,
  `cakupan` varchar(50) DEFAULT NULL,
  `target` varchar(20) DEFAULT NULL,
  `real` varchar(20) DEFAULT NULL,
  `achievement` varchar(10) DEFAULT NULL,
  `acctarget` varchar(25) DEFAULT NULL,
  `accreal` varchar(25) DEFAULT NULL,
  `accachievement` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `week` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_investasi_anggaran`
--

CREATE TABLE `f_investasi_anggaran` (
  `entitas` varchar(6) DEFAULT NULL,
  `uraian` varchar(155) DEFAULT NULL,
  `target` decimal(29,0) DEFAULT NULL,
  `realisasi` decimal(29,0) DEFAULT NULL,
  `achievement` varchar(20) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ap`
--

CREATE TABLE `f_kfho_ap` (
  `vendor_code` varchar(50) DEFAULT NULL,
  `vendor_desc` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `aging_category` varchar(10) DEFAULT NULL,
  `aging_status` varchar(20) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ap_v2`
--

CREATE TABLE `f_kfho_ap_v2` (
  `entitas` varchar(20) DEFAULT NULL,
  `vendor_code` varchar(50) DEFAULT NULL,
  `vendor_desc` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(50) DEFAULT NULL,
  `text_prc` varchar(50) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ap_v3`
--

CREATE TABLE `f_kfho_ap_v3` (
  `entitas` varchar(20) DEFAULT NULL,
  `vendor_code` varchar(50) DEFAULT NULL,
  `pstng_date` varchar(50) DEFAULT NULL,
  `vendor_desc` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(50) DEFAULT NULL,
  `text_prc` varchar(50) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ar`
--

CREATE TABLE `f_kfho_ar` (
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `aging_category` varchar(10) DEFAULT NULL,
  `aging_status` varchar(20) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ar_v2`
--

CREATE TABLE `f_kfho_ar_v2` (
  `entitas` varchar(20) DEFAULT NULL,
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(50) DEFAULT NULL,
  `text_prc` varchar(50) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ar_v3`
--

CREATE TABLE `f_kfho_ar_v3` (
  `entitas` varchar(20) DEFAULT NULL,
  `customer_code` varchar(50) DEFAULT NULL,
  `pstng_date` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(50) DEFAULT NULL,
  `text_prc` varchar(50) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_ar_v3_temp`
--

CREATE TABLE `f_kfho_ar_v3_temp` (
  `entitas` text,
  `customer_code` text,
  `pstng_date` text,
  `customer_desc` text,
  `profit_ctr` text,
  `text_prc` text,
  `customer_category` text,
  `gl_acc` text,
  `text_gl` text,
  `currency` text,
  `0_30` double DEFAULT NULL,
  `31_60` double DEFAULT NULL,
  `61_90` double DEFAULT NULL,
  `91_120` double DEFAULT NULL,
  `121_150` double DEFAULT NULL,
  `151_360` double DEFAULT NULL,
  `360_` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_cashbank`
--

CREATE TABLE `f_kfho_cashbank` (
  `profitctr_code` varchar(50) DEFAULT NULL,
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance_tot` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_cashbank_backup`
--

CREATE TABLE `f_kfho_cashbank_backup` (
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance_tot` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_cashbank_old`
--

CREATE TABLE `f_kfho_cashbank_old` (
  `profitctr_code` varchar(50) DEFAULT NULL,
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_cashbank_v3`
--

CREATE TABLE `f_kfho_cashbank_v3` (
  `profitctr_code` varchar(50) DEFAULT NULL,
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance_tot` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kfho_cashcollection`
--

CREATE TABLE `f_kfho_cashcollection` (
  `datacollection` varchar(50) DEFAULT NULL,
  `cakupan` varchar(50) DEFAULT NULL,
  `target` varchar(20) DEFAULT NULL,
  `real` varchar(20) DEFAULT NULL,
  `achievement` varchar(10) DEFAULT NULL,
  `acctarget` varchar(25) DEFAULT NULL,
  `accreal` varchar(25) DEFAULT NULL,
  `accachievement` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `week` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_ap_v2`
--

CREATE TABLE `f_kftd_ap_v2` (
  `entitas` varchar(20) DEFAULT NULL,
  `vendor_code` varchar(50) DEFAULT NULL,
  `vendor_desc` varchar(50) DEFAULT NULL,
  `vendor_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(30) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `daops` varchar(10) DEFAULT NULL,
  `segment` varchar(30) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_ar`
--

CREATE TABLE `f_kftd_ar` (
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `aging_category` varchar(10) DEFAULT NULL,
  `aging_status` varchar(20) DEFAULT NULL,
  `aging` int(11) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(20) DEFAULT NULL,
  `profit_ctr` varchar(20) DEFAULT NULL,
  `entitas` varchar(10) DEFAULT NULL,
  `daops` varchar(20) DEFAULT NULL,
  `profit_center` varchar(30) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_ar_pemasukan`
--

CREATE TABLE `f_kftd_ar_pemasukan` (
  `customer_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(30) DEFAULT NULL,
  `text_prc` varchar(50) DEFAULT NULL,
  `daops` varchar(10) DEFAULT NULL,
  `segment_1` varchar(30) DEFAULT NULL,
  `segment_3` varchar(30) DEFAULT NULL,
  `pemasukan` decimal(20,0) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_ar_temp`
--

CREATE TABLE `f_kftd_ar_temp` (
  `customer_code` text,
  `customer_desc` text,
  `customer_category` text,
  `aging_category` text,
  `aging_status` text,
  `aging` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `gl_acc` text,
  `text_gl` text,
  `profit_ctr` text,
  `entitas` text,
  `daops` text,
  `profit_center` text,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_ar_v2`
--

CREATE TABLE `f_kftd_ar_v2` (
  `entitas` varchar(20) DEFAULT NULL,
  `customer_code` varchar(50) DEFAULT NULL,
  `customer_desc` varchar(50) DEFAULT NULL,
  `customer_category` varchar(20) DEFAULT NULL,
  `gl_acc` varchar(20) DEFAULT NULL,
  `text_gl` varchar(50) DEFAULT NULL,
  `profit_ctr` varchar(30) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `daops` varchar(10) DEFAULT NULL,
  `segment_1` varchar(30) DEFAULT NULL,
  `segment_3` varchar(30) DEFAULT NULL,
  `0_30` decimal(20,0) DEFAULT NULL,
  `31_60` decimal(20,0) DEFAULT NULL,
  `61_90` decimal(20,0) DEFAULT NULL,
  `91_120` decimal(20,0) DEFAULT NULL,
  `121_150` decimal(20,0) DEFAULT NULL,
  `151_360` decimal(20,0) DEFAULT NULL,
  `360_` decimal(20,0) DEFAULT NULL,
  `total` decimal(20,0) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_cashbank`
--

CREATE TABLE `f_kftd_cashbank` (
  `profitctr_code` varchar(50) DEFAULT NULL,
  `profit_center` varchar(50) DEFAULT NULL,
  `beginning_balance` decimal(20,0) DEFAULT NULL,
  `ending_balance` decimal(20,0) DEFAULT NULL,
  `glacc` varchar(20) DEFAULT NULL,
  `gl_account` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `f_kftd_cashcollection`
--

CREATE TABLE `f_kftd_cashcollection` (
  `datacollection` varchar(50) DEFAULT NULL,
  `cakupan` varchar(50) DEFAULT NULL,
  `target` varchar(20) DEFAULT NULL,
  `real` varchar(20) DEFAULT NULL,
  `achievement` varchar(10) DEFAULT NULL,
  `acctarget` varchar(25) DEFAULT NULL,
  `accreal` varchar(25) DEFAULT NULL,
  `accachievement` varchar(20) DEFAULT NULL,
  `entitas` varchar(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `week` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `income_kaef_temp`
--

CREATE TABLE `income_kaef_temp` (
  `month` varchar(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `acct_pay_bal` decimal(29,0) DEFAULT NULL,
  `acct_rec_bal` decimal(29,0) DEFAULT NULL,
  `cogs_cogm` decimal(29,0) DEFAULT NULL,
  `cogs_persedian_awal` decimal(29,0) DEFAULT NULL,
  `cogs_real` decimal(29,0) DEFAULT NULL,
  `cash_bank` decimal(29,0) DEFAULT NULL,
  `liabilities` decimal(29,0) DEFAULT NULL,
  `net_income_real` decimal(29,0) DEFAULT NULL,
  `net_sales_exp` decimal(29,0) DEFAULT NULL,
  `other_income_real` decimal(29,0) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kfho_doi`
--

CREATE TABLE `ip_kfho_doi` (
  `var` varchar(10) DEFAULT NULL,
  `plant_desc` varchar(50) DEFAULT NULL,
  `doi` decimal(20,0) DEFAULT NULL,
  `postingdate_year` varchar(4) DEFAULT NULL,
  `postingdate_month` varchar(2) DEFAULT NULL,
  `periode` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kfho_soh_a`
--

CREATE TABLE `ip_kfho_soh_a` (
  `material_type` varchar(50) DEFAULT NULL,
  `sub_cat` varchar(50) DEFAULT NULL,
  `plant_desc` varchar(50) DEFAULT NULL,
  `value_barang` decimal(20,0) DEFAULT NULL,
  `lini` varchar(50) DEFAULT NULL,
  `postingdate_year` varchar(4) DEFAULT NULL,
  `postingdate_month` varchar(2) DEFAULT NULL,
  `postingdate_day` varchar(2) DEFAULT NULL,
  `periode` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kfho_soh_b`
--

CREATE TABLE `ip_kfho_soh_b` (
  `tipe` varchar(20) DEFAULT NULL,
  `materialdescription` varchar(70) DEFAULT NULL,
  `sub_cat` varchar(50) DEFAULT NULL,
  `plant_desc` varchar(50) DEFAULT NULL,
  `value_barang` decimal(20,0) DEFAULT NULL,
  `lini` varchar(50) DEFAULT NULL,
  `postingdate_year` varchar(4) DEFAULT NULL,
  `postingdate_month` varchar(2) DEFAULT NULL,
  `postingdate_day` varchar(2) DEFAULT NULL,
  `periode` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_barang_expired`
--

CREATE TABLE `ip_kftd_barang_expired` (
  `daops` varchar(30) DEFAULT NULL,
  `kode_plant` varchar(50) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `principle` varchar(50) DEFAULT NULL,
  `principle_desc` varchar(50) DEFAULT NULL,
  `sudah_expired` decimal(20,0) DEFAULT NULL,
  `ed_kurang_1tahun` decimal(20,0) DEFAULT NULL,
  `ed_kurang_3bln` decimal(20,0) DEFAULT NULL,
  `ed_kurang_6bln` decimal(20,0) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_be`
--

CREATE TABLE `ip_kftd_be` (
  `material` varchar(50) DEFAULT NULL,
  `value_stock` decimal(20,0) DEFAULT NULL,
  `status_expired` varchar(50) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `daops` varchar(20) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_si`
--

CREATE TABLE `ip_kftd_si` (
  `kode_plant` varchar(6) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `index_stock` decimal(20,0) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_umur_stock`
--

CREATE TABLE `ip_kftd_umur_stock` (
  `daops` varchar(30) DEFAULT NULL,
  `kode_plant` varchar(30) DEFAULT NULL,
  `plant_desc` varchar(50) DEFAULT NULL,
  `kode_principle` varchar(30) DEFAULT NULL,
  `principle_desc` varchar(50) DEFAULT NULL,
  `sku` varchar(30) DEFAULT NULL,
  `sku_desc` varchar(50) DEFAULT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `days` decimal(20,0) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_us`
--

CREATE TABLE `ip_kftd_us` (
  `principal` varchar(50) DEFAULT NULL,
  `material` varchar(50) DEFAULT NULL,
  `umur_stock` bigint(6) DEFAULT NULL,
  `value_stock` decimal(20,0) DEFAULT NULL,
  `daops` varchar(20) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_kftd_vs`
--

CREATE TABLE `ip_kftd_vs` (
  `kode_plant` varchar(6) DEFAULT NULL,
  `plant` varchar(50) DEFAULT NULL,
  `value_stock` decimal(20,0) DEFAULT NULL,
  `daops` varchar(20) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `day` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kf_itbumn_project`
--

CREATE TABLE `kf_itbumn_project` (
  `no` int(11) DEFAULT NULL,
  `project_name` varchar(50) DEFAULT NULL,
  `progress` varchar(4) DEFAULT NULL,
  `year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kf_itbumn_project_detail`
--

CREATE TABLE `kf_itbumn_project_detail` (
  `no` int(11) DEFAULT NULL,
  `task` varchar(50) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `target` varchar(4) DEFAULT NULL,
  `achieve` varchar(4) DEFAULT NULL,
  `deviasi` varchar(4) DEFAULT NULL,
  `year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kf_target_adj_bs`
--

CREATE TABLE `kf_target_adj_bs` (
  `month` int(2) DEFAULT NULL,
  `month_` int(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `uraian` varchar(255) DEFAULT NULL,
  `target` varchar(29) DEFAULT NULL,
  `real` varchar(29) DEFAULT NULL,
  `f01` varchar(29) DEFAULT NULL,
  `adjusment` varchar(29) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `Id` int(11) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `create_by` text,
  `update_by` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kf_target_adj_income`
--

CREATE TABLE `kf_target_adj_income` (
  `month` int(2) DEFAULT NULL,
  `month_` int(2) DEFAULT NULL,
  `entitas` varchar(5) DEFAULT NULL,
  `uraian` varchar(255) DEFAULT NULL,
  `target` varchar(29) DEFAULT NULL,
  `real` varchar(29) DEFAULT NULL,
  `f01` varchar(29) DEFAULT NULL,
  `adjusment` varchar(29) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `Id` int(11) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `create_by` text,
  `update_by` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_gcg`
--

CREATE TABLE `l_gcg` (
  `kategori` varchar(50) DEFAULT NULL,
  `belum` int(11) DEFAULT NULL,
  `progress` int(11) DEFAULT NULL,
  `selesai` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_healthscore`
--

CREATE TABLE `l_healthscore` (
  `health_level_score` decimal(5,2) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `periode` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_kpku`
--

CREATE TABLE `l_kpku` (
  `kategori` varchar(50) DEFAULT NULL,
  `belum` int(11) DEFAULT NULL,
  `progress` int(11) DEFAULT NULL,
  `selesai` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_lhkpn`
--

CREATE TABLE `l_lhkpn` (
  `belumlapor` int(11) DEFAULT NULL,
  `sudahlapor` int(11) DEFAULT NULL,
  `wajiblapor` int(11) DEFAULT NULL,
  `tepatwaktu` int(11) DEFAULT NULL,
  `terlambat` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_portalbumn`
--

CREATE TABLE `l_portalbumn` (
  `jenislaporan` varchar(50) DEFAULT NULL,
  `status` decimal(20,0) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `periode` varchar(10) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_scoregcg`
--

CREATE TABLE `l_scoregcg` (
  `score_gcg` decimal(5,2) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `l_scorekpku`
--

CREATE TABLE `l_scorekpku` (
  `score_kpku` decimal(5,2) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` bigint(20) NOT NULL,
  `level` varchar(20) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `level`, `icon`, `name`, `link`) VALUES
(1, '1', 'fa fa-dashboard', 'Executive Dashboard', '#'),
(2, '2', 'fas fa-chart-line', 'CRM Analytics', '#'),
(3, '3', '	fas fa-file-upload', 'Growth Hacking', '#'),
(4, '4', 'fas fa-chart-pie', 'Operational Dashboard', '#'),
(5, '5', 'fas fa-landmark', 'Insightful Dashboard', '#'),
(6, '6', 'fas fa-users', 'Customer Loyalty Management Program', '#'),
(7, '1.1', NULL, 'Financial', '#'),
(8, '1.1.1', NULL, 'Income Statement', 'page/view/income_s'),
(9, '1.1.2', NULL, 'Balance Sheet', 'page/view/balance_sheet'),
(10, '1.1.3', NULL, 'AP & AR', 'page/view/ap_ar_v2'),
(11, '1.1.4', NULL, 'Cash Bank', 'page/view/cash_bank'),
(12, '1.1.5', NULL, 'Realisasi Investasi', 'page/view/realisasi_investasi'),
(13, '1.1.6', NULL, 'Cash Collection', 'page/view/data_investasi'),
(14, '1.0', NULL, 'Main Menu', 'page/view/dashboard'),
(15, '1.5', NULL, 'Customer', 'page/view/customer'),
(16, '1.3', NULL, 'Internal Process', NULL),
(17, '1.3.1.1', NULL, 'Logistic', NULL),
(18, '1.3.1', NULL, 'Stock on Hand', 'page/view/stock_on_hand'),
(19, '1.3.2', NULL, 'Day of Inventory', 'page/view/day_of_i'),
(20, '1.3.3', NULL, 'Bad Stock', 'page/view/bad_stock'),
(21, '1.3.4', NULL, 'Inventory Turn Over', NULL),
(22, '1.3.5', NULL, 'Demand Forcast', NULL),
(23, '1.3.6', NULL, 'Order Fullfillment', NULL),
(24, '1.3.7', NULL, 'Capacity Planning', NULL),
(25, '1.3.8', NULL, 'Prognosa Plant', NULL),
(26, '1.3.2.1', NULL, 'Sales', NULL),
(27, '1.1.7', NULL, 'Konsolidasi', 'page/view/konsolidasi'),
(28, '1.1.8', NULL, 'GAP per Entitas', 'page/view/gap'),
(29, '9', 'fas fa-users-cog', 'User Management', '#'),
(30, '1.2', NULL, 'Marketing', NULL),
(31, '1.2.1', NULL, 'Overview Marketing', 'page/view/overview_marketing'),
(32, '1.2.2', NULL, 'Detail Informasi', 'page/view/detail_informasi'),
(33, '1.2.3', NULL, 'KFA Performance', 'page/view/kfa_performance'),
(34, '1.2.4', NULL, 'Marketing Performance', 'page/view/marketing_star'),
(35, '4.0.1', NULL, 'Sales Information', 'page/view/sales_information'),
(36, '4.0.2', NULL, 'Marketing Expenses Information', 'page/view/marketing_expences_information'),
(37, '4.0.3', NULL, 'Report Sales Summary', 'page/view/report_sales_summary'),
(38, '4.0.4', NULL, 'Report Sales Margin Summary', 'page/view/report_sales_margin_summary'),
(48, '4.0', NULL, 'Marketing', NULL),
(49, '9.1', NULL, 'Level', 'Menu_management/view'),
(50, '1.4.1', NULL, 'Form Adj IS', 'page/view/f_income'),
(51, '1.4.4', NULL, 'Cash Collection', 'Import/view'),
(52, '1.4.3', NULL, 'Realisasi Investasi', 'realisasi_investasi/view'),
(53, '1.4.2', '', 'Form Adj BS', 'Adj_balance_sheet/view'),
(54, '9.2', '', 'User', 'User_maintenance/view'),
(55, '1.4', NULL, 'Operational Input', '#'),
(56, '1.4.5', '', 'Kelola Uraian', 'UraianMaintenance/view'),
(57, '2.1', NULL, 'Consumer Journey', 'crm/consumer_journey'),
(58, '2.2', NULL, 'Consumer Loyalty Program', 'crm/clp');

-- --------------------------------------------------------

--
-- Table structure for table `myusers`
--

CREATE TABLE `myusers` (
  `USER_ID` int(11) NOT NULL,
  `USER_NAME` varchar(255) NOT NULL,
  `SALARY` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `problem_c`
--

CREATE TABLE `problem_c` (
  `ORDER_NO` varchar(100) NOT NULL,
  `USER_ID` varchar(100) NOT NULL,
  `BOOKING_TIME` varchar(100) DEFAULT NULL,
  `CANCEL_TIME` varchar(100) DEFAULT NULL,
  `COMPLETE_TIME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_merger_fi_is_month_kf_target_adj_income`
--

CREATE TABLE `table_merger_fi_is_month_kf_target_adj_income` (
  `id` int(11) UNSIGNED NOT NULL,
  `realisasi` decimal(29,0) NOT NULL,
  `real_KF` decimal(29,0) NOT NULL,
  `F01_KF` varchar(29) NOT NULL,
  `realisasi_f01` decimal(29,0) NOT NULL,
  `adjusment_` varchar(29) NOT NULL,
  `target` decimal(29,0) NOT NULL,
  `prognosa` decimal(29,0) NOT NULL,
  `growth` decimal(29,0) NOT NULL,
  `realisasi_ytd` decimal(29,0) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `achievement` decimal(29,0) NOT NULL,
  `income_statement` varchar(255) NOT NULL,
  `key_` varchar(1) NOT NULL,
  `entitas` varchar(5) NOT NULL,
  `year` int(5) NOT NULL,
  `month` int(2) NOT NULL,
  `last_update` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_dm_marketing_opration_margin_data`
--

CREATE TABLE `temp_dm_marketing_opration_margin_data` (
  `nama` varchar(201) DEFAULT NULL,
  `description` varchar(19) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `counter` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `target` decimal(65,4) DEFAULT NULL,
  `realisasi_this_year` decimal(57,8) DEFAULT NULL,
  `realisasi_last_year` decimal(57,8) DEFAULT NULL,
  `achievement` decimal(65,8) DEFAULT NULL,
  `growth` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `uraian`
--

CREATE TABLE `uraian` (
  `id` int(6) UNSIGNED NOT NULL,
  `for_to` varchar(100) NOT NULL,
  `desc` text NOT NULL,
  `groupBS` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `level` varchar(30) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `jabatan` text,
  `NIK` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nama`, `email`, `password`, `hp`, `unit`, `level`, `active`, `username`, `jabatan`, `NIK`) VALUES
('admin', 'admin', 'ff0ae55331bcfb7adc7de5c2a7a02a96', '', 'DSC Telkom', 'admin', 1, 'admin', '', ''),
('ajinusa', 'ajinusa', 'fa9376e79ba6dda6fe57ccf22ace5d34', '', 'IT', 'kfho', 0, 'ajinusa', NULL, NULL),
('Amir Hamzah', 'amir.hamzah@kimiafarma.co.id', 'd3019f5b51a577cdc91038044dfe4e6f', '', 'Akuntansi KFHO', 'kfho_fin', 1, 'amir.hamzah', '', ''),
('fitri', 'fitri', '202cb962ac59075b964b07152d234b70', '797070', 'it', 'kfho', 1, 'fitri', NULL, NULL),
('kfho', 'kfho', 'd3789a3f91258fcf605452196e19c21c', NULL, NULL, 'kfho', 1, 'kfho', NULL, NULL),
('kfho_fin', 'kfho_fin', 'd3789a3f91258fcf605452196e19c21c', NULL, NULL, 'kfho_fin', 1, 'kfho_fin', NULL, NULL),
('made', 'made.dharmawan@telkom.co.id', '5d39b23b0ba3a6a015228a13806c8630', '08121091686', 'DSC Telkom', 'superadmin', 1, 'madedsc', 'Junior', '725569');

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `level` varchar(20) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`level`, `id_menu`) VALUES
('admin', 1),
('admin', 14),
('admin', 7),
('admin', 8),
('admin', 9),
('admin', 10),
('admin', 39),
('admin', 11),
('admin', 12),
('admin', 13),
('admin', 27),
('admin', 28),
('admin', 30),
('admin', 31),
('admin', 32),
('admin', 33),
('admin', 34),
('admin', 16),
('admin', 17),
('admin', 18),
('admin', 19),
('admin', 20),
('admin', 21),
('admin', 22),
('admin', 23),
('admin', 24),
('admin', 25),
('admin', 26),
('admin', 4),
('admin', 35),
('admin', 36),
('admin', 38),
('admin', 29),
('dawaa', 1),
('dawaa', 14),
('dawaa', 7),
('dawaa', 8),
('dawaa', 9),
('dawaa', 10),
('dawaa', 39),
('dawaa', 11),
('dawaa', 12),
('dawaa', 13),
('dawaa', 27),
('dawaa', 28),
('dawaa', 30),
('dawaa', 31),
('dawaa', 32),
('dawaa', 33),
('dawaa', 34),
('dawaa', 16),
('dawaa', 17),
('dawaa', 18),
('dawaa', 19),
('dawaa', 20),
('dawaa', 21),
('dawaa', 22),
('dawaa', 23),
('dawaa', 24),
('dawaa', 25),
('dawaa', 26),
('dawaa', 4),
('dawaa', 35),
('dawaa', 36),
('dawaa', 38),
('dawaa', 5),
('dawaa', 6),
('dawaa', 29),
('kfa', 1),
('kfa', 14),
('kfa', 7),
('kfa', 8),
('kfa', 9),
('kfa', 10),
('kfa', 39),
('kfa', 11),
('kfa', 12),
('kfa', 13),
('kfa', 27),
('kfa', 28),
('kfa', 30),
('kfa', 31),
('kfa', 32),
('kfa', 33),
('kfa', 34),
('kfa', 16),
('kfa', 17),
('kfa', 18),
('kfa', 19),
('kfa', 20),
('kfa', 21),
('kfa', 22),
('kfa', 23),
('kfa', 24),
('kfa', 25),
('kfa', 26),
('kfa', 4),
('kfa', 35),
('kfa', 36),
('kfa', 38),
('kfa', 5),
('kfa', 6),
('kfa', 29),
('kfc', 1),
('kfc', 14),
('kfc', 7),
('kfc', 8),
('kfc', 9),
('kfc', 10),
('kfc', 39),
('kfc', 11),
('kfc', 12),
('kfc', 13),
('kfc', 27),
('kfc', 28),
('kfc', 30),
('kfc', 31),
('kfc', 32),
('kfc', 33),
('kfc', 34),
('kfc', 16),
('kfc', 17),
('kfc', 18),
('kfc', 19),
('kfc', 20),
('kfc', 21),
('kfc', 22),
('kfc', 23),
('kfc', 24),
('kfc', 25),
('kfc', 26),
('kfc', 4),
('kfc', 35),
('kfc', 36),
('kfc', 38),
('kfc', 5),
('kfc', 6),
('kfc', 29),
('kfsp', 1),
('kfsp', 14),
('kfsp', 7),
('kfsp', 8),
('kfsp', 9),
('kfsp', 10),
('kfsp', 39),
('kfsp', 11),
('kfsp', 12),
('kfsp', 13),
('kfsp', 27),
('kfsp', 28),
('kfsp', 30),
('kfsp', 31),
('kfsp', 32),
('kfsp', 33),
('kfsp', 34),
('kfsp', 16),
('kfsp', 17),
('kfsp', 18),
('kfsp', 19),
('kfsp', 20),
('kfsp', 21),
('kfsp', 22),
('kfsp', 23),
('kfsp', 24),
('kfsp', 25),
('kfsp', 26),
('kfsp', 4),
('kfsp', 35),
('kfsp', 36),
('kfsp', 38),
('kfsp', 5),
('kfsp', 6),
('kftd', 1),
('kftd', 46),
('kftd', 7),
('kftd', 44),
('kftd', 45),
('kftd', 40),
('kftd', 39),
('kftd', 47),
('kftd', 41),
('kftd', 42),
('kftd', 43),
('kftd', 30),
('kftd', 31),
('kftd', 32),
('kftd', 33),
('kftd', 34),
('kftd', 16),
('kftd', 17),
('kftd', 18),
('kftd', 19),
('kftd', 20),
('kftd', 21),
('kftd', 22),
('kftd', 23),
('kftd', 24),
('kftd', 25),
('kftd', 26),
('kftd', 4),
('kftd', 35),
('kftd', 36),
('kftd', 38),
('kftd', 5),
('kftd', 6),
('sil', 1),
('sil', 14),
('sil', 7),
('sil', 8),
('sil', 9),
('sil', 10),
('sil', 39),
('sil', 11),
('sil', 12),
('sil', 13),
('sil', 27),
('sil', 28),
('sil', 30),
('sil', 31),
('sil', 32),
('sil', 33),
('sil', 34),
('sil', 16),
('sil', 17),
('sil', 18),
('sil', 19),
('sil', 20),
('sil', 21),
('sil', 22),
('sil', 23),
('sil', 24),
('sil', 25),
('sil', 26),
('sil', 4),
('sil', 35),
('sil', 36),
('sil', 38),
('sil', 5),
('sil', 6),
('sil', 29),
('admin', 49),
('admin', 50),
('kfho_marketing', 30),
('kfho_marketing', 31),
('kfho_marketing', 32),
('kfho_marketing', 33),
('kfho_marketing', 34),
('kfho_marketing', 48),
('kfho_marketing', 35),
('kfho_marketing', 36),
('kfho_marketing', 38),
('kfho_marketing', 1),
('kfho_marketing', 4),
('kfho_fin', 1),
('kfho_fin', 7),
('kfho_fin', 14),
('kfho_fin', 55),
('kfho_fin', 8),
('kfho_fin', 9),
('kfho_fin', 11),
('kfho_fin', 12),
('kfho_fin', 13),
('kfho_fin', 50),
('kfho_fin', 51),
('kfho_fin', 52),
('kfho_fin', 53),
('kfho', 1),
('kfho', 4),
('kfho', 29),
('kfho', 14),
('kfho', 7),
('kfho', 30),
('kfho', 16),
('kfho', 55),
('kfho', 48),
('kfho', 49),
('kfho', 54),
('kfho', 8),
('kfho', 9),
('kfho', 10),
('kfho', 11),
('kfho', 12),
('kfho', 13),
('kfho', 31),
('kfho', 32),
('kfho', 33),
('kfho', 34),
('kfho', 18),
('kfho', 19),
('kfho', 20),
('kfho', 21),
('kfho', 22),
('kfho', 23),
('kfho', 24),
('kfho', 25),
('kfho', 50),
('kfho', 53),
('kfho', 52),
('kfho', 51),
('kfho', 56),
('kfho', 35),
('kfho', 36),
('kfho', 37),
('kfho', 38),
('superadmin', 1),
('superadmin', 4),
('superadmin', 29),
('superadmin', 14),
('superadmin', 7),
('superadmin', 30),
('superadmin', 16),
('superadmin', 55),
('superadmin', 48),
('superadmin', 49),
('superadmin', 54),
('superadmin', 8),
('superadmin', 9),
('superadmin', 10),
('superadmin', 11),
('superadmin', 12),
('superadmin', 13),
('superadmin', 18),
('superadmin', 19),
('superadmin', 20),
('superadmin', 21),
('superadmin', 22),
('superadmin', 23),
('superadmin', 24),
('superadmin', 25),
('superadmin', 31),
('superadmin', 32),
('superadmin', 33),
('superadmin', 34),
('superadmin', 35),
('superadmin', 36),
('superadmin', 37),
('superadmin', 38),
('superadmin', 50),
('superadmin', 51),
('superadmin', 52),
('superadmin', 53),
('superadmin', 56),
('kfho', 57),
('kfho', 2),
('kfho', 58);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crm_cj`
--
ALTER TABLE `crm_cj`
  ADD PRIMARY KEY (`cc_id`);

--
-- Indexes for table `crm_customers`
--
ALTER TABLE `crm_customers`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `demografi_age`
--
ALTER TABLE `demografi_age`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `demografi_edu`
--
ALTER TABLE `demografi_edu`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `demografi_job`
--
ALTER TABLE `demografi_job`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `demografi_work`
--
ALTER TABLE `demografi_work`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_balance_month`
--
ALTER TABLE `fi_balance_month`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_balance_month1`
--
ALTER TABLE `fi_balance_month1`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_balance_month1x`
--
ALTER TABLE `fi_balance_month1x`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_balance_month_ori`
--
ALTER TABLE `fi_balance_month_ori`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_investasi_month`
--
ALTER TABLE `fi_investasi_month`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_is_month`
--
ALTER TABLE `fi_is_month`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `fi_is_month_ori`
--
ALTER TABLE `fi_is_month_ori`
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `last_update` (`last_update`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `f_all_cb_temp`
--
ALTER TABLE `f_all_cb_temp`
  ADD KEY `index` (`entitas`,`year`,`month`);

--
-- Indexes for table `f_ap`
--
ALTER TABLE `f_ap`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ap_general`
--
ALTER TABLE `f_ap_general`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ap_kfho_monthly`
--
ALTER TABLE `f_ap_kfho_monthly`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ap_kfho_triwulan`
--
ALTER TABLE `f_ap_kfho_triwulan`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ap_kfho_yearly`
--
ALTER TABLE `f_ap_kfho_yearly`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ar`
--
ALTER TABLE `f_ar`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ar_general`
--
ALTER TABLE `f_ar_general`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ar_kfho_monthly`
--
ALTER TABLE `f_ar_kfho_monthly`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ar_kfho_triwulan`
--
ALTER TABLE `f_ar_kfho_triwulan`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_ar_kfho_yearly`
--
ALTER TABLE `f_ar_kfho_yearly`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_cashbank`
--
ALTER TABLE `f_cashbank`
  ADD KEY `index` (`entitas`,`year`,`month`,`day`);

--
-- Indexes for table `f_cashcollection`
--
ALTER TABLE `f_cashcollection`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`entitas`,`year`,`month`);

--
-- Indexes for table `f_kfho_ap`
--
ALTER TABLE `f_kfho_ap`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_ap_v2`
--
ALTER TABLE `f_kfho_ap_v2`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_ap_v3`
--
ALTER TABLE `f_kfho_ap_v3`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_ar`
--
ALTER TABLE `f_kfho_ar`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_ar_v2`
--
ALTER TABLE `f_kfho_ar_v2`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_ar_v3`
--
ALTER TABLE `f_kfho_ar_v3`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kfho_cashbank`
--
ALTER TABLE `f_kfho_cashbank`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`last_update`,`year`,`month`);

--
-- Indexes for table `f_kfho_cashbank_old`
--
ALTER TABLE `f_kfho_cashbank_old`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`last_update`,`year`,`month`);

--
-- Indexes for table `f_kfho_cashcollection`
--
ALTER TABLE `f_kfho_cashcollection`
  ADD KEY `index` (`entitas`,`year`,`month`);

--
-- Indexes for table `f_kftd_ap_v2`
--
ALTER TABLE `f_kftd_ap_v2`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kftd_ar`
--
ALTER TABLE `f_kftd_ar`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kftd_ar_pemasukan`
--
ALTER TABLE `f_kftd_ar_pemasukan`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kftd_ar_v2`
--
ALTER TABLE `f_kftd_ar_v2`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `f_kftd_cashbank`
--
ALTER TABLE `f_kftd_cashbank`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`last_update`,`year`,`month`);

--
-- Indexes for table `f_kftd_cashcollection`
--
ALTER TABLE `f_kftd_cashcollection`
  ADD KEY `index` (`entitas`,`year`,`month`);

--
-- Indexes for table `income_kaef_temp`
--
ALTER TABLE `income_kaef_temp`
  ADD KEY `year` (`year`);

--
-- Indexes for table `ip_kfho_doi`
--
ALTER TABLE `ip_kfho_doi`
  ADD KEY `index` (`var`,`postingdate_year`,`postingdate_month`);

--
-- Indexes for table `ip_kfho_soh_a`
--
ALTER TABLE `ip_kfho_soh_a`
  ADD KEY `index` (`lini`,`postingdate_year`,`postingdate_month`);

--
-- Indexes for table `ip_kfho_soh_b`
--
ALTER TABLE `ip_kfho_soh_b`
  ADD KEY `index` (`tipe`,`postingdate_year`,`postingdate_month`,`postingdate_day`);

--
-- Indexes for table `ip_kftd_barang_expired`
--
ALTER TABLE `ip_kftd_barang_expired`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `ip_kftd_be`
--
ALTER TABLE `ip_kftd_be`
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `ip_kftd_si`
--
ALTER TABLE `ip_kftd_si`
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `ip_kftd_umur_stock`
--
ALTER TABLE `ip_kftd_umur_stock`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `ip_kftd_us`
--
ALTER TABLE `ip_kftd_us`
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `ip_kftd_vs`
--
ALTER TABLE `ip_kftd_vs`
  ADD KEY `index` (`year`,`month`,`day`);

--
-- Indexes for table `kf_itbumn_project`
--
ALTER TABLE `kf_itbumn_project`
  ADD KEY `index` (`no`,`project_name`,`progress`,`year`);

--
-- Indexes for table `kf_itbumn_project_detail`
--
ALTER TABLE `kf_itbumn_project_detail`
  ADD KEY `index` (`year`);

--
-- Indexes for table `kf_target_adj_bs`
--
ALTER TABLE `kf_target_adj_bs`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `kf_target_adj_income`
--
ALTER TABLE `kf_target_adj_income`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `entitas` (`entitas`);

--
-- Indexes for table `l_gcg`
--
ALTER TABLE `l_gcg`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`tahun`);

--
-- Indexes for table `l_healthscore`
--
ALTER TABLE `l_healthscore`
  ADD KEY `index` (`tahun`,`periode`);

--
-- Indexes for table `l_kpku`
--
ALTER TABLE `l_kpku`
  ADD KEY `index` (`tahun`);

--
-- Indexes for table `l_lhkpn`
--
ALTER TABLE `l_lhkpn`
  ADD KEY `index` (`year`);

--
-- Indexes for table `l_portalbumn`
--
ALTER TABLE `l_portalbumn`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`tahun`,`periode`);

--
-- Indexes for table `l_scoregcg`
--
ALTER TABLE `l_scoregcg`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `index` (`tahun`);

--
-- Indexes for table `l_scorekpku`
--
ALTER TABLE `l_scorekpku`
  ADD KEY `index` (`tahun`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `myusers`
--
ALTER TABLE `myusers`
  ADD PRIMARY KEY (`USER_ID`);

--
-- Indexes for table `problem_c`
--
ALTER TABLE `problem_c`
  ADD UNIQUE KEY `ORDER_NO` (`ORDER_NO`);

--
-- Indexes for table `table_merger_fi_is_month_kf_target_adj_income`
--
ALTER TABLE `table_merger_fi_is_month_kf_target_adj_income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uraian`
--
ALTER TABLE `uraian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `unique_index` (`username`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crm_cj`
--
ALTER TABLE `crm_cj`
  MODIFY `cc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crm_customers`
--
ALTER TABLE `crm_customers`
  MODIFY `ct_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `fi_investasi_month`
--
ALTER TABLE `fi_investasi_month`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_ap`
--
ALTER TABLE `f_ap`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_ar`
--
ALTER TABLE `f_ar`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_cashcollection`
--
ALTER TABLE `f_cashcollection`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ap`
--
ALTER TABLE `f_kfho_ap`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ap_v2`
--
ALTER TABLE `f_kfho_ap_v2`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ap_v3`
--
ALTER TABLE `f_kfho_ap_v3`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ar`
--
ALTER TABLE `f_kfho_ar`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ar_v2`
--
ALTER TABLE `f_kfho_ar_v2`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_ar_v3`
--
ALTER TABLE `f_kfho_ar_v3`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_cashbank`
--
ALTER TABLE `f_kfho_cashbank`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kfho_cashbank_old`
--
ALTER TABLE `f_kfho_cashbank_old`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kftd_ap_v2`
--
ALTER TABLE `f_kftd_ap_v2`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kftd_ar`
--
ALTER TABLE `f_kftd_ar`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kftd_ar_pemasukan`
--
ALTER TABLE `f_kftd_ar_pemasukan`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kftd_ar_v2`
--
ALTER TABLE `f_kftd_ar_v2`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `f_kftd_cashbank`
--
ALTER TABLE `f_kftd_cashbank`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_kftd_barang_expired`
--
ALTER TABLE `ip_kftd_barang_expired`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_kftd_umur_stock`
--
ALTER TABLE `ip_kftd_umur_stock`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kf_target_adj_bs`
--
ALTER TABLE `kf_target_adj_bs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kf_target_adj_income`
--
ALTER TABLE `kf_target_adj_income`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `l_gcg`
--
ALTER TABLE `l_gcg`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `l_portalbumn`
--
ALTER TABLE `l_portalbumn`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `l_scoregcg`
--
ALTER TABLE `l_scoregcg`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `myusers`
--
ALTER TABLE `myusers`
  MODIFY `USER_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_merger_fi_is_month_kf_target_adj_income`
--
ALTER TABLE `table_merger_fi_is_month_kf_target_adj_income`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uraian`
--
ALTER TABLE `uraian`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `CRON DELETE FI_BS_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-11 11:10:00' ON COMPLETION PRESERVE ENABLE COMMENT 'MADE' DO DELETE FROM `datamart_kf`.`FI_BS_MONTH_CRON` 
WHERE last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON DELETE FI_BS_RASIO_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-11 11:15:00' ON COMPLETION PRESERVE ENABLE COMMENT 'MADE' DO DELETE FROM `datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
WHERE last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON DELETE FI_IS_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-14 09:30:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'MADE' DO DELETE FROM `datamart_kf`.`FI_IS_MONTH_CRON` 
WHERE last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON INSERT INTO FI_BS_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-11 11:30:00' ON COMPLETION PRESERVE ENABLE COMMENT 'MADE' DO INSERT INTO datamart_kf.FI_BS_MONTH_CRON (`key`,balance_sheet,jenis,faktor,`real`,
target,amount,amount_ytd,adjusment,amount_ytd1,pengurang,ket,`year`,`month`,entitas,last_update)
SELECT 
A.`key`,
A.balance_sheet,
A.jenis,
A.faktor,
ROUND(COALESCE(B.REAL,0)) AS `real`,
ROUND(COALESCE(B.TARGET,0)) AS target,
ROUND(COALESCE(A.amount_,0)) AS amount,
ROUND(COALESCE(A.amount_ytd_,0)) AS amount_ytd,
ROUND(COALESCE(B.Adjusment,0)) AS adjusment,

#ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN COALESCE(A.amount_ytd_,0)+COALESCE(B.Adjusment,0)
#	ELSE ROUND(COALESCE(B.REAL,0)) END) AS AMOUNT_YTD1,

ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN COALESCE(B.REAL,0)
	ELSE COALESCE(A.amount_ytd_,0)+COALESCE(B.Adjusment,0) END) AS amount_ytd1,
	
ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN ROUND(COALESCE(B.REAL,0))
	ELSE (A.amount_ytd_+B.Adjusment) END)-ROUND(COALESCE(B.REAL,0)) AS pengurang,
	
#ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN COALESCE(A.amount_ytd_,0)+COALESCE(B.Adjusment,0)
#	ELSE ROUND(COALESCE(B.REAL,0)) END)-ROUND(COALESCE(B.REAL,0)) AS PENGURANG,			

		
CASE WHEN B.Adjusment IS NULL THEN 'not yet adjusted' 
     ELSE 'adjusted' END AS ket,
A.year,
A.month,
A.entitas,
A.last_update

FROM
(
  SELECT * FROM datamart_kf.fi_balance_month_ori
) AS A LEFT JOIN
(
SELECT * FROM datamart_kf.kf_target_adj_BS
) AS B ON A.month=B.month AND A.year=B.year AND A.faktor=B.uraian AND A.entitas=B.entitas
WHERE A.last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')
ORDER BY A.key,A.balance_sheet$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON INSERT INTO FI_IS_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-14 09:31:00' ON COMPLETION PRESERVE ENABLE COMMENT 'MADE' DO INSERT INTO datamart_kf.FI_IS_MONTH_CRON (REALISASI,F01,Adjusment_,TARGET,`REAL`,PROGNOSA,GROWTH,REALISASI_YTD,pengurang,Ket,ACHIEVEMENT,
income_statement,
KEY_,
ENTITAS,
`YEAR`,
`MONTH`,
LAST_UPDATE)

SELECT 
A.REALISASI,
A.F01,
COALESCE(C.Adjusment,0) AS Adjusment_,
COALESCE(C.TARGET,0) AS TARGET, 
COALESCE(C.REAL,0) AS `REAL`,
CASE WHEN (A.REALISASI_YTD-C.Adjusment) IS NULL THEN COALESCE(C.REAL,0)
      ELSE A.REALISASI_YTD-C.Adjusment END AS PROGNOSA,

(A.REALISASI_YTD/A.REALISASI) AS GROWTH, 


ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END) AS REALISASI_YTD,
     
     
#ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN  A.REALISASI_YTD-COALESCE(C.Adjusment,0)
#     ELSE COALESCE(C.REAL,0) END) AS REALISASI_YTD_TEST, 
     
# ROUND(COALESCE(C.REAL,0)-ROUND(CASE WHEN A.REALISASI_YTD IS NULL 
#      THEN A.REALISASI_YTD-COALESCE(C.Adjusment,0) ELSE COALESCE(C.REAL,0) END)) AS pengurang,        
     
ROUND(COALESCE(C.REAL,0)-ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)) AS pengurang,
     
CASE WHEN C.Adjusment IS NULL THEN 'not yet adjusted' 
     ELSE 'adjusted' END AS Ket,

#ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN A.REALISASI_YTD-COALESCE(C.Adjusment,0) 
#      ELSE COALESCE(C.REAL,0) END)/C.TARGET AS ACHIEVEMENT,     

ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)/C.TARGET AS ACHIEVEMENT,

A.income_statement,
A.KEY_,
A.ENTITAS,
A.YEAR,
A.MONTH,
A.LAST_UPDATE

FROM 
(
SELECT * FROM datamart_kf.fi_is_month_ori
) AS A LEFT JOIN
(
 SELECT * FROM datamart_kf.kf_target_adj_income
)AS C ON A.month=C.month AND A.year=C.year AND A.entitas=C.entitas AND A.income_statement=C.uraian
WHERE A.last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')
ORDER BY A.key_,A.MONTH$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON_BS - DISABLE` ON SCHEDULE EVERY 1 DAY STARTS '2019-07-30 19:51:02' ON COMPLETION PRESERVE DISABLE DO CREATE TABLE datamart_kf.FI_BS_MONTH_CRON as
SELECT 
A.`key`,
A.balance_sheet,
A.jenis,
A.faktor,
ROUND(COALESCE(B.REAL,0)) AS `REAL`,
ROUND(COALESCE(B.TARGET,0)) AS TARGET,
ROUND(COALESCE(A.amount_,0)) AS AMOUNT,
COALESCE(A.amount_ytd_,0) AS AMOUNT_YTD,
COALESCE(B.Adjusment,0) AS Adjusment,
#ROUND(CASE when A.amount_ytd_ IS NULL then (A.amount_ytd_+B.Adjusment)
#	ELSE B.REAL end) AS amount_YTD1,

COALESCE(ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN (A.amount_ytd_+B.Adjusment)
	ELSE B.REAL END),0) AS amount_YTD1,

COALESCE(ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN (A.amount_ytd_+B.Adjusment)
	ELSE B.REAL END)-ROUND(B.REAL),0) AS PENGURANG,	
CASE WHEN B.Adjusment IS NULL THEN 'not yet adjusted' 
     ELSE 'adjusted' END AS Ket,
A.year,
A.month,
A.entitas,
A.last_update

FROM
(
  SELECT * FROM datamart_kf.fi_balance_month_ori
) AS A LEFT JOIN
(
SELECT * FROM datamart_kf.kf_target_adj_BS
) AS B ON A.month=B.month AND A.year=B.year AND A.faktor=B.uraian AND A.entitas=B.entitas
WHERE #A.ENTITAS='KFHO' AND A.YEAR=2019 AND A.MONTH=5 AND 
A.last_update<'20190720' 
ORDER BY A.key,A.balance_sheet$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON INSERT INTO FI_BS_RASIO_MONTH_CRON` ON SCHEDULE EVERY 1 DAY STARTS '2019-08-11 11:35:00' ON COMPLETION PRESERVE ENABLE COMMENT 'MADE' DO INSERT INTO datamart_kf.FI_BS_RASIO_MONTH_CRON (WORKING_CAPITAL,WORKING_CAPITAL_YTD,CURRENT_RATIO,CURRENT_RATIO_YTD, 
ACID_TEST_RASIO,ACID_TEST_RASIO_YTD,CASH_RASIO,CASH_RASIO_YTD,DER_INTEREST_BEARING,DER_INTEREST_BEARING_YTD,DEBT_RASIO,DEBT_RASIO_YTD, 
EQUITY_RASIO,EQUITY_RASIO_YTD,ENTITAS,`YEAR`,`MONTH`,last_update)

SELECT 
((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))-
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END))) AS WORKING_CAPITAL,
((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))-
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))) AS WORKING_CAPITAL_YTD,

((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS CURRENT_RATIO,
((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS CURRENT_RATIO_YTD,

(((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))-
(SUM(CASE WHEN faktor='PERSEDIAAN' THEN amount ELSE 0 END)))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS ACID_TEST_RASIO,

(((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))-
(SUM(CASE WHEN faktor='PERSEDIAAN' THEN amount_ytd1 ELSE 0 END)))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS ACID_TEST_RASIO_YTD,

((SUM(CASE WHEN faktor='KAS DAN SETARA KAS' THEN amount ELSE 0 END))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS CASH_RASIO,

((SUM(CASE WHEN faktor='KAS DAN SETARA KAS' THEN amount_ytd1 ELSE 0 END))/
(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS CASH_RASIO_YTD,

(((SUM(CASE WHEN faktor='UTANG BANK' THEN amount ELSE 0 END))+
(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN - JANGKA PENDEK' THEN amount ELSE 0 END))+
(SUM(CASE WHEN faktor='PINJAMAN JANGKA MENENGAH (MTN)' THEN amount ELSE 0 END))+
(SUM(CASE WHEN faktor='PINJAMAN JANGKA PANJANG' THEN amount ELSE 0 END))+
(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN JANGKA PANJANG' THEN amount ELSE 0 END)))/
(SUM(CASE WHEN balance_sheet='EKUITAS' THEN amount ELSE 0 END)))*100 AS DER_INTEREST_BEARING,


(((SUM(CASE WHEN faktor='UTANG BANK' THEN amount_ytd1 ELSE 0 END))+
(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN - JANGKA PENDEK' THEN amount_ytd1 ELSE 0 END))+
(SUM(CASE WHEN faktor='PINJAMAN JANGKA MENENGAH (MTN)' THEN amount_ytd1 ELSE 0 END))+
(SUM(CASE WHEN faktor='PINJAMAN JANGKA PANJANG' THEN amount_ytd1 ELSE 0 END))+
(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN JANGKA PANJANG' THEN amount_ytd1 ELSE 0 END)))/
(SUM(CASE WHEN balance_sheet='EKUITAS' THEN amount_ytd1 ELSE 0 END)))*100 AS DER_INTEREST_BEARING_YTD,

(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)/
(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)))*100 AS DEBT_RASIO,

#((SUM(CASE when (balance_sheet='LIABILITAS' AND faktor like 'UTANG %') then amount_ytd1 Else 0 End))/
#(SUM(CASE when balance_sheet in ('LIABILITAS','EKUITAS') then amount_ytd1 Else 0 End)))*100 AS DEBT_RASIO_YTD,

(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)/
(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)))*100
AS DEBT_RASIO_YTD,

#((SUM(CASE when balance_sheet in ('EKUITAS') then amount Else 0 End))/
#(SUM(CASE when balance_sheet in ('LIABILITAS','EKUITAS') then amount Else 0 End))) AS EQUITY_RASIO,

(SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)/
(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)))*100 AS EQUITY_RASIO,

(SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)/
(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)))*100 AS EQUITY_RASIO_YTD,


ENTITAS,
YEAR,
MONTH,
last_update
FROM datamart_kf.FI_BS_MONTH_CRON
#where entitas='KFHO' AND MONTH=5 AND YEAR=2019 AND LAST_UPDATE='20190802'
WHERE last_update=REPLACE((CONVERT(NOW(),DATE)),'-','')
GROUP BY 
ENTITAS,
YEAR,
#BALANCE_SHEET,
#JENIS,
MONTH,
last_update$$

CREATE DEFINER=`root`@`localhost` EVENT `CRON_IS - DISABLE` ON SCHEDULE EVERY 1 DAY STARTS '2019-07-28 11:49:05' ON COMPLETION PRESERVE DISABLE COMMENT 'MADE' DO CREATE TABLE datamart_kf.FI_IS_MONTH_CRON as
SELECT 
A.REALISASI,
A.F01,
COALESCE(C.Adjusment,0) AS Adjusment_,
COALESCE(C.TARGET,0) AS TARGET, 
COALESCE(C.REAL,0) AS `REAL`,
CASE when (A.REALISASI_YTD-C.Adjusment) IS NULL then COALESCE(C.REAL,0)
     ELSE A.REALISASI_YTD-C.Adjusment end AS PROGNOSA,

(A.REALISASI_YTD/A.REALISASI) AS GROWTH, 


#ROUND(CASE when A.REALISASI_YTD IS NULL then COALESCE(C.REAL,0)
#     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) end) AS REALISASI_YTD,
     
     
ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN  A.REALISASI_YTD-COALESCE(C.Adjusment,0)
     ELSE COALESCE(C.REAL,0) END) AS REALISASI_YTD, 
     
 ROUND(COALESCE(C.REAL,0)-ROUND(CASE WHEN A.REALISASI_YTD IS NULL 
      THEN A.REALISASI_YTD-COALESCE(C.Adjusment,0) ELSE COALESCE(C.REAL,0) END)) AS pengurang,        
     
#ROUND(COALESCE(C.REAL,0)-ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
#     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)) AS pengurang,
     
CASE WHEN C.Adjusment IS NULL THEN 'not yet adjusted' 
     ELSE 'adjusted' END AS Ket,

ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN A.REALISASI_YTD-COALESCE(C.Adjusment,0) 
      ELSE COALESCE(C.REAL,0) END)/C.TARGET AS ACHIEVEMENT,     

#ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
#     ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)/C.TARGET AS ACHIEVEMENT,

A.income_statement,
A.KEY_,
A.ENTITAS,
A.YEAR,
A.MONTH,
A.LAST_UPDATE

FROM 
(
SELECT * FROM datamart_kf.fi_is_month_ori
) AS A LEFT JOIN
(
 SELECT * FROM datamart_kf.kf_target_adj_income
)AS C ON A.month=C.month AND A.year=C.year AND A.entitas=C.entitas AND A.income_statement=C.uraian
#WHERE A.entitas='KFA' AND A.YEAR=2018 AND A.MONTH=5
ORDER BY A.key_,A.MONTH$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
