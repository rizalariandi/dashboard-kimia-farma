<?php
class Template{
    protected $_ci;

    function __construct(){
        $this->_ci = &get_instance();
    }

    function template($content, $data = NULL){
        $data['header'] = $this->_ci->load->view('generate/header', $data, TRUE);
        $data['sidebar'] = $this->_ci->load->view('generate/sidebar', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footer'] = $this->_ci->load->view('generate/footer', $data, TRUE);

        $this->_ci->load->view('generate/index', $data);
    }
}
