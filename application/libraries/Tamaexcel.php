<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\Color;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;


class Tamaexcel
{
    protected $ci;

    public function __construct()
    {
        $this->ci = &get_instance();
    }


    function exportexcel($header, $data, $arrtotal, $namafile)
    {
        $fileName = $namafile.".xlsx";
        $writer = WriterEntityFactory::createXLSXWriter();
        // $writer = WriterEntityFactory::createODSWriter();
        // $writer = WriterEntityFactory::createCSVWriter();

        // $writer->openToFile($filePath); // write data to a file or to a PHP stream
        
        $writer->openToBrowser($fileName); // stream data directly to the browser
        
        $cells = [
            // WriterEntityFactory::createCell('Carl'),
            // WriterEntityFactory::createCell('is'),
            // WriterEntityFactory::createCell('great!'),
        ];

        foreach($header as $col){
            array_push($cells, WriterEntityFactory::createCell($col));
        }
        // array_push($cells, WriterEntityFactory::createCell("Total"));

        /** add a row at a time */
        $singleRow = WriterEntityFactory::createRow($cells);
        $writer->addRow($singleRow);

        /** add multiple rows at a time */
        // $multipleRows = [
        //     WriterEntityFactory::createRow($cells),
        //     WriterEntityFactory::createRow($cells),
        // ];
        // $writer->addRows($multipleRows);

        /** Shortcut: add a row from an array of values */
        $indextotal = 0;
        foreach($data as $val){
            $values = array();
            $total = 0;
            $i = 0;
            foreach($header as $head){
                // echo $val->$head."\n";
                if($i != 0){
                    $total = $val->$head / $arrtotal[$indextotal] * 100;
                    $total = number_format((float)$total, 2, '.', '');
                    $values[] = $total."%";
                }else{
                    $values[] = $val->$head;
                }
                
                
                $i++;
            }  
            $indextotal++;
            // array_push($values, $total);
            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
            $writer->addRow($rowFromValues); 
        }
        

        $writer->close();
    }

   
   
}

/* End of file Tamaexecel.php */
