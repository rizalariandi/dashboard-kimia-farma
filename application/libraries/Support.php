<?php
class Support{
    protected $_ci;
    
    function __construct(){
        $this->_ci = &get_instance();
    }
     
  function bulan($no){ 
        $no = strlen($no) < 2 ? '0'.$no : $no;
        $bulan = array(
            '01' => 'januari',
            '02' => 'februari',
            '03' => 'maret',
            '04' => 'april',
            '05' => 'mei',
            '06' => 'juni',
            '07' => 'juli',
            '08' => 'agustus',
            '09' => 'september',
            '10' => 'oktober',
            '11' => 'november',
            '12' => 'desember',
        );
    return $bulan[$no];
    }
    
}
