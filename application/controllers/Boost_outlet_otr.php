<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_outlet_otr extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->model('Boost_outlet_otr_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{
		$area = $this->input->post('area');
		// $channel = $this->input->post('channel');
		// $periode = $this->input->post('periode', true);

		// =================== ditambahkan 26-10-2021 =========================
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);
        // ====================================================================

		$data["data_area"] = $this->input->post("area_table", true);
		$data["data_channel"] = $this->input->post("channel_table", true);
		$data["periode_table_outlet"] = $this->input->post("periode_table_outlet", true);
	
		if (empty($data["periode_table_outlet"])) {
			$data["periode_table_outlet"] = date('Y');
		}

		// ============= sepertinya tdk dipakai (checked 26-10-2021) ================
		// $data_otr = $this->Boost_outlet_otr_model->outlet_otr($area, $channel);
		// ==================================================================
		// die($data_otr);

		$tahun_otr = $this->Boost_outlet_otr_model->get_tahun_otr('usc_bst_sales_outlet_channel');
		// die($tahun_otr);
		// $json_data_otr = array();
		
		$list_brand = $this->Boost_outlet_otr_model->get_brand_outlet('usc_bst_sales_outlet_channel');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		$tahun_table =  $this->db->distinct()->select("year(tanggal) as tahun")->order_by('tahun', 'DESC')->limit(2)->get('usc_bst_sales_outlet_channel')->result();

		$bulan = $this->db->distinct()->select('month(tanggal) as bulan')->get('usc_bst_sales_brand_channel')->result();
		
		// ============= sepertinya tdk dipakai (checked 26-10-2021) ================
		// foreach ($tahun_otr as $key=>$value) {
		// 	$json_data_otr[$key]['name'] = $value->tahun;
		// 	$jml_outlet = array();
		// 	foreach ($data_otr as $otr){
		// 		if ($otr->tahun == $value->tahun){
		// 			 $jml_outlet[] = (int)$otr->jumlah_outlet;
		// 		}
		// 	}
		// 	$json_data_otr[$key]['data'] = $jml_outlet;
		// 	$json_data_otr[$key]['color'] = '#' . random_color();
		// }
		// =================================================================

		// dipake di $data_otr untuk top 20 brand
		$outlet = $this->Boost_outlet_otr_model->json_outlet_otr($periode, $kftd, $channel, $lini, $brand);
		// die($outlet);

		$trend = $this->Boost_outlet_otr_model->json_outlet_trend();
		// die($trend);

		$dec = json_decode($outlet);
		$decode_trend = json_decode($trend);
		$last_update = $this->Boost_outlet_otr_model->get_last_update();
		$data_otr=json_encode($dec->data, JSON_NUMERIC_CHECK);
		$lini =$this->Boost_outlet_otr_model->get_lini_otr('usc_bst_sales_brand_channel');
		$channels = $this->Boost_outlet_otr_model->get_channels('usc_bst_sales_outlet');

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Outlet OTR',
			'bulan' => $bulan,
			'tahun' => $tahun,
			'tahun_otr' => $tahun_otr,
			'provinsi' => $this->Boost_outlet_otr_model->get_provinsi('usc_bst_sales_outlet_channel'),
			'area' => $area,
			'areas' => $this->Boost_outlet_otr_model->get_list_kftd('usc_bst_sales_outlet_channel'),
			'channel' => $channel,
			'channels' => $channels,
			'lini' => $lini,
			'linis' => $this->Boost_outlet_otr_model->get_lini_otr('usc_bst_sales_outlet_channel'),
			'brand' => $this->Boost_outlet_otr_model->get_brand_otr('usc_bst_sales_brand_channel'),
			'list_brand' => $list_brand,
			'nama_brand_otr' => json_encode($dec->nama_brand),
			'data_otr' => $data_otr,
			'trend_this_year' => json_encode($this->Boost_outlet_otr_model->trend_outlet($area, $channel, $year = date("Y")),JSON_NUMERIC_CHECK),
			'trend_last_year' => json_encode($this->Boost_outlet_otr_model->trend_outlet($area, $channel, $year - 1), JSON_NUMERIC_CHECK),
			'trend' => json_encode($decode_trend->data, JSON_NUMERIC_CHECK),
			'cat_month' => json_encode($decode_trend->bulan),
			'last_update' => $last_update[0]->tanggal,
			'periode' => $periode,
			'tahun_table' => $tahun_table
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/sales/outlet_otr', $data);
        $this->load->view('templates/footer', $data);
	}

	public function json_outlet_otr()
	{
		header('Content-Type: application/json');
		// =================== ditambahkan 26-10-2021 =========================
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);
        // ====================================================================
		echo $this->Boost_outlet_otr_model->json_outlet_otr($periode, $kftd, $channel, $lini, $brand);
	}

	public function json_get_list_product()
	{
		header('Content-Type: application/json');
		echo $this->Boost_outlet_otr_model->json_get_list_product();
		// die($this->Boost_outlet_otr_model->json_get_list_product());
	}

	public function json_outlet_trend()
	{
		header('Content-Type: application/json');
		echo $this->Boost_outlet_otr_model->json_outlet_trend();
	}

	public function json_outlet_transaction_quantity()
	{
		header('Content-Type: application/json');
		$a = $this->Boost_outlet_otr_model->json_outlet_transaction_quantity();
		// die($this->db->last_query());
		echo $a;
		// var_dump("tes = ",$a);
	}

	public function json_outlet_transaction_brand()
	{
		header('Content-Type: application/json');
		echo $this->Boost_outlet_otr_model->json_outlet_transaction_brand();
		// die($this->Boost_outlet_otr_model->json_outlet_transaction_brand());
	}

	public function json_outlet_transaction_area()
	{
		header('Content-Type: application/json');
		echo $this->Boost_outlet_otr_model->json_outlet_transaction_area();
		// die($this->Boost_outlet_otr_model->json_outlet_transaction_area());
	}

	public function json_get_brand()
	{
		$table = $this->input->post("table", true);
		$lini = $this->input->post("lini", true);
		$brand_by_lini = $this->Boost_outlet_otr_model->get_brand($table, $lini);
		foreach ($brand_by_lini as $list) {
			echo '<option value="' . $list->nama_brand . '">' . $list->nama_brand . "</option>";
		}
	}

}