<?php
class Page extends CI_Controller
{
	var $marketingMenu =  array(
		'sales_information',
		'marketing_expences_information',
		'report_sales_margin_summary',
		'report_sales_margin_summary_newprogress',
		'report_sales_summary'
	);
	public function user_level()
	{
		$data_arr = array();
		foreach ($_POST as $k => $p) {
			foreach ($p as $c) {
				$data_arr[] = "('$k',$c)";
			}
		}
		$this->load->model('user_model');
		$q =  "insert into user_menu values " . implode(',', $data_arr);
		$this->user_model->saveUserMenu($q);
		redirect(base_url() . 'index.php/page/view/user_management');
	}

	public function register()
	{
		$data['form'] = $this->input->post();
		$data['p'] = "";
		$this->load->model('user_model');
		if (!empty($_POST)) {
			if ($_POST['password'] == $_POST['password2']) {
				$this->user_model->register($data['form']);
				$data['p'] = '<div style="display:inline-block;background:#0F0;padding:3px">Register Success!</div>';
				$msg = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><title>Digital Touchpoint</title></head><body style='margin:0px; background: #f8f8f8; '><div width='100%' style='background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;'><div style='max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px'>
				<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; margin-bottom: 20px'>
				<tbody>
				</tbody>
				</table>
				<div style='padding: 40px; background: #fff;'>
				<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'>
				<tbody>
				<tr>
				<td>Hi, <b>$_POST[nama]</b>!
				<p>Registrasi Digital Touchpoint Anda Berhasil</p>
				<p>Silahkan login dengan email dan password anda</p>
				<p>Terima Kasih</p>
				<b>- Thanks (Admin team)</b> </td>
				</tr>
				</tbody>
				</table>
				</div><div style='text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px'><p> Digital Touchpoint Data Scientist Group <br></div></div></div></body></html>";
				$this->sendMail($_POST['email'], 'Registrasi Digital Touchpoint Berhasil!', $msg);
			} else {
				$data['p'] = '<div style="display:inline-block;background:#F00;padding:3px;margin:5px;">Register Gagal, Password tidak sama!</div>';
			}
		}
		$this->load->view('pages/register', $data);
	}

	public function sendingMail($id)
	{
		$this->load->model('dtp_model');
		$email = $this->dtp_model->email($id);

		$btn = "";
		switch (strtolower($email->status)) {
			case 'rejected':
				$btn = '#f33155';
				break;
			case 'process':
				$btn = '#41b3f9';
				break;
			case 'closed':
				$btn = '#7ace4c';
				break;
			case 'accepted':
				$btn = '#7ace4c';
				break;
		}
		$subject = "Notifikasi Digital Touchpoint id Request :#DSC$email->id_dtp";
		$msg = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><title>Digital Touchpoint</title></head><body style='margin:0px; background: #f8f8f8; '><div width='100%' style='background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;'><div style='max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px'>
		<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; margin-bottom: 20px'>
		<tbody>
		</tbody>
		</table>
		<div style='padding: 40px; background: #fff;'>
		<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'>
		<tbody>
		<tr>
		<td>Hi, <b>$email->nama1</b>!
		<p>Progress request anda dengan nomor <b>#DSC$email->id_dtp</b> : </p>
		<p>Process : <b>$email->nama</b></p>
		<p>Detail prosess : <b>$email->detail</b></p>
		<p>PIC : <b>$email->pic</b></p>
		<p>Due Date : <b>$email->due_date</b></p>
		<p>Status :
		<a href='javascript: void(0);' style='display: inline-block; padding: 10px; font-size: 15px; color: #fff; background: $btn; border-radius: 5px; text-decoration:none;'> $email->status </a></p>
		<p>Terima Kasih</p>
		<b>- Thanks (Admin team)</b> </td>
		</tr>
		</tbody>
		</table>
		</div><div style='text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px'><p> Digital Touchpoint Data Scientist Group <br></div></div></div></body></html>";
		$this->sendMail($email->email, $subject, $msg);
	}

	public function view($page = 'dashboard')
	{
		error_reporting(0);
		if ($this->session->userdata('loged_in') == null) {
			redirect('page/login');
		}
		//echo APPPATH.'views/'.$page.'';die;
		if (!file_exists(APPPATH . 'views/pages/' . $page . '.php') &&  !file_exists(APPPATH . 'views/' . $page)) {
			// Whoops, we don't have a page for that!
			show_404();
		}

		$this->load->model('user_model');
		$this->load->model('kf_model');

		$data['menu'] = $this->user_model->getMenu();
		$data['p'] = "";
		$data['title'] = ucfirst($page); // Capitalize the first letter
		if (!empty($_POST)) {
			foreach ($_POST as $k => $v) {
				$_SESSION[$k] = $v;
			}
		}
		$_SESSION['period'] = isset($_SESSION['period']) ? $_SESSION['period'] : 'YearToDate';
		$_SESSION['entitas'] = isset($_SESSION['entitas']) ? $_SESSION['entitas'] : 'KFHO';
		$_SESSION['year'] = isset($_SESSION['year']) ? $_SESSION['year'] : '2018';
		$_SESSION['month'] = isset($_SESSION['month']) ? $_SESSION['month'] : '1';
		$_SESSION['week'] = isset($_SESSION['week']) ? $_SESSION['week'] : '1';
		$_SESSION['day'] = isset($_SESSION['day']) ? $_SESSION['day'] : '1';
		switch ($page) {
			case 'f_income':
				$this->load->model('uraian_model');
				$data['uraian_is'] = $this->uraian_model->getUraianIS();
				break;
			case 'balance_sheet':
				$data['bs'] = $this->kf_model->balance_sheet();
				break;
			case 'kftd_balance_sheet':
				$data['bs'] = $this->kf_model->balance_sheet();
				break;
			case 'income_s':
				$data['max'] = $this->kf_model->max_lastupdate();
				$max_lastupdate = $data['max'][0];
				$data['is'] = $this->kf_model->income_statement($max_lastupdate);
				break;
			case 'kftd_income_s':
				$data['max'] = $this->kf_model->max_lastupdate();
				$max_lastupdate = $data['max'][0];
				$data['is'] = $this->kf_model->income_statement($max_lastupdate);
				break;
			case 'cash_bank':
				$data['profit_center'] = $this->kf_model->get_profit_center();
				$data['cb'] = $this->kf_model->cash_bank();
				break;
			case 'kftd_cash_bank':
				$data['profit_center'] = $this->kf_model->get_profit_center_cb_kftd();
				$data['status'] = $this->kf_model->get_status_kftd();
				$data['cb'] = $this->kf_model->cash_bank();

				break;
			case 'user_management':
				$data['menus'] = $this->user_model->getAllMenu();
				$data['levels'] = $this->user_model->getAllLevel();
				$data['usermenu'] = $this->user_model->getAllUserMenu();
				break;
			case 'ap_ar':
				$data['menus'] = $this->user_model->getAllMenu();
				$data['levels'] = $this->user_model->getAllLevel();
				$data['usermenu'] = $this->user_model->getAllUserMenu();
				$data['ap_data'] = $this->kf_model->ap_data();
				$data['ar_data'] = $this->kf_model->ar_data();
				$data['ar_data_aging'] = $this->kf_model->ar_data_aging();
				break;
			case 'kftd_ar':

				$data['daops'] = $this->kf_model->get_daops();
				$data['plant'] = $this->kf_model->get_plant();
				$data['segment_1'] = $this->kf_model->get_segment1();
				$data['segment_3'] = $this->kf_model->get_segment3();
				break;
			case 'kftd_ar_pem':

				$data['cust_cat'] = $this->kf_model->get_cust_cat();
				$data['plant'] = $this->kf_model->get_plant_pem();
				$data['segment_1'] = $this->kf_model->get_segment1_pem();
				$data['segment_3'] = $this->kf_model->get_segment3_pem();
				break;
			case 'kftd_ap':

				$data['daops'] = $this->kf_model->get_daops_ap();
				$data['plant'] = $this->kf_model->get_plant_ap();
				$data['segment_1'] = $this->kf_model->get_segment1_ap();
				break;
			case 'dtp':
				$data['form'] = (!empty($this->input->post())) ? $this->input->post() : "";

				if (!empty($data['form'])) {
					$this->dtp_model->inputDtp($data['form']);
					$data['p'] = "<div class='alert alert-success alert-dismissable'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Berhasil disimpan </div>";
					$msg = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><title>Digital Touchpoint</title></head><body style='margin:0px; background: #f8f8f8; '><div width='100%' style='background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;'><div style='max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px'>
					<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; margin-bottom: 20px'>
					<tbody>
					</tbody>
					</table>
					<div style='padding: 40px; background: #fff;'>
					<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'>
					<tbody>
					<tr>
					<td>Hi, <b>$_SESSION[nama]</b>!
					<p>Request anda tentang <b>$_POST[subject]</b></p>
					<p>detail pertanyaan/permintaan : $_POST[isi]</p>
					<p>Sudah kami terima dan akan segera kami proses.</p>
					<p>Terima Kasih</p>
					<b>- Thanks (Admin team)</b> </td>
					</tr>
					</tbody>
					</table>
					</div><div style='text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px'><p> Digital Touchpoint Data Scientist Group <br></div></div></div></body></html>";
					$this->sendMail($_SESSION['username'], 'Request anda sudah kami terima!', $msg);
				}
				break;
			case 'summary':
				$data['table'] = $this->dtp_model->getData();
				break;
			case 'progress':
				$id = isset($_GET['id']) ? $_GET['id'] : "";
				if (!empty($_POST)) {
					$this->dtp_model->saveProcess($id, $_POST);
					$this->sendingMail($id);
				}
				$data['table'] = $this->dtp_model->getProcess($id);
				$data['form'] = $this->dtp_model->getDataId($id);
				$data['table2'] = $this->dtp_model->getPIC();
				break;
			case 'follow_up':
				$id = isset($_GET['id']) ? $_GET['id'] : "";
				$data['table'] = $this->dtp_model->getProcess($id);
				$data['form'] = $this->dtp_model->getDataId($id);
				break;
		}

		$conditionPage = !in_array($page, $this->marketingMenu) ? true : false;		
		if ($conditionPage === true) {
			$data['opration'] = false;
			$this->load->view('templates/header', $data);
			$this->load->view('pages/' . $page, $data);
			$this->load->view('templates/footer_main', $data);
		} else {
			$data['opration'] = $page == 'marketing_expences_information' ? false : true;
			$data['filter'] = array('page' => $page . '/filter', 'data' => $data);
			$data['javascript'] = array(base_url('assets/js/custom/' . $page . '.js'));
			//print_r($data);die();

			$this->load->view('templates/header_opt', $data);
			$this->load->view($page . '/home', $data);
			$this->load->view('templates/footer_main', $data);
		}
	}

	public function index()
	{
		if($this->session->userdata('level') === 'IT_KFHO' || $this->session->userdata('level') === 'MARKETING_KFHO')
		{
			redirect('Marketing_overview/view');
		}
		else if ($this->session->userdata('loged_in') !== null) {
			redirect('page/view');
		} else {
			$this->load->view('pages/login');
		}
	}

	public function refresh_income_statement()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$period = json_decode($this->input->get_post('period'));
		$day = json_decode($this->input->get_post('day'));


		$_SESSION['entitas'] = $entitas;
		$_SESSION['year'] = $year;
		$_SESSION['month'] = $month;
		$_SESSION['week'] = 1;
		$_SESSION['day'] = $day;
		if ($period === 'Monthly' || $period === 'YearToDate') {
			$data['max'] = $this->kf_model->max_lastupdate();
			$max_lastupdate = str_replace("-", "", $data['max'][0]);

			$data['max_growth'] = $this->kf_model->max_lastupdate_growth();
			$max_lastupdate_growth = str_replace("-", "", $data['max_growth'][0]);
			$data['is_cogs'] = $this->kf_model->is_cogs($month, $year, $entitas, $max_lastupdate);
			$data['is_cogs_lastmonth'] = $this->kf_model->is_cogs_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_gross_profit'] = $this->kf_model->is_gross_profit($month, $year, $entitas, $max_lastupdate);
			$data['is_gross_profit_lastmonth'] = $this->kf_model->is_gross_profit_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_net_income'] = $this->kf_model->is_net_income($month, $year, $entitas, $max_lastupdate);
			$data['is_net_income_lastmonth'] = $this->kf_model->is_net_income_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_net_operating'] = $this->kf_model->is_net_operating($month, $year, $entitas, $max_lastupdate);
			$data['is_net_operating_lastmonth'] = $this->kf_model->is_net_operating_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_net_sales'] = $this->kf_model->is_net_sales($month, $year, $entitas, $max_lastupdate);
			$data['is_net_sales_lastmonth'] = $this->kf_model->is_net_sales_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_operating_expenses'] = $this->kf_model->is_operating_expenses($month, $year, $entitas, $max_lastupdate);
			$data['is_operating_expenses_lastmonth'] = $this->kf_model->is_operating_expenses_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_other_income'] = $this->kf_model->is_other_income($month, $year, $entitas, $max_lastupdate);
			$data['is_other_income_lastmonth'] = $this->kf_model->is_other_income_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_interest_expense'] = $this->kf_model->is_interest_expense($month, $year, $entitas, $max_lastupdate);
			$data['is_interest_expense_lastmonth'] = $this->kf_model->is_interest_expense_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_income_before'] = $this->kf_model->is_income_before($month, $year, $entitas, $max_lastupdate);
			$data['is_income_before_lastmonth'] = $this->kf_model->is_income_before_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_tax_expense'] = $this->kf_model->is_tax_expense($month, $year, $entitas, $max_lastupdate);
			$data['is_tax_expense_lastmonth'] = $this->kf_model->is_tax_expense_lastmonth($month, $year, $entitas, $max_lastupdate_growth);

			$data['is_sum_income'] = $this->kf_model->sum_income_statement($month, $year, $entitas, $max_lastupdate, $period);
		} else if ($period === 'Daily') {

			$data['is_cogs'] = $this->kf_model->is_cogs($month, $year, $entitas);
			$data['is_cogs_lastmonth'] = $this->kf_model->is_cogs_lastmonth($month, $year, $entitas);

			$data['is_gross_profit'] = $this->kf_model->is_gross_profit($month, $year, $entitas);
			$data['is_gross_profit_lastmonth'] = $this->kf_model->is_gross_profit_lastmonth($month, $year, $entitas);

			$data['is_net_income'] = $this->kf_model->is_net_income($month, $year, $entitas);
			$data['is_net_income_lastmonth'] = $this->kf_model->is_net_income_lastmonth($month, $year, $entitas);

			$data['is_net_operating'] = $this->kf_model->is_net_operating($month, $year, $entitas);
			$data['is_net_operating_lastmonth'] = $this->kf_model->is_net_operating_lastmonth($month, $year, $entitas);

			$data['is_net_sales'] = $this->kf_model->is_net_sales($month, $year, $entitas);
			$data['is_net_sales_lastmonth'] = $this->kf_model->is_net_sales_lastmonth($month, $year, $entitas);

			$data['is_operating_expenses'] = $this->kf_model->is_operating_expenses($month, $year, $entitas);
			$data['is_operating_expenses_lastmonth'] = $this->kf_model->is_operating_expenses_lastmonth($month, $year, $entitas);

			$data['is_other_income'] = $this->kf_model->is_other_income($month, $year, $entitas);
			$data['is_other_income_lastmonth'] = $this->kf_model->is_other_income_lastmonth($month, $year, $entitas);

			$data['is_sum_income'] = $this->kf_model->sum_income_statement_daily($month, $year, $entitas, $day);
		}


		$data['marketing_statement'] = $this->kf_model->sum_marketing_statement($month, $year, $entitas);




		echo json_encode($data);
	}

	public function refresh_overview_marketing()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$startdate = json_decode($this->input->get_post('startDate'));
		$enddate = json_decode($this->input->get_post('endDate'));
		$entitas = strtolower(json_decode($this->input->get_post('entitas')));
		$data['sum_all'] = $this->kf_model->sum_all($startdate, $enddate, $entitas);
		$data['sum_kftd'] = $this->kf_model->sum_kftd($startdate, $enddate, $entitas);
		$data['sum_other'] = $this->kf_model->sum_other($startdate, $enddate, $entitas);

		$data['sales_today'] = $this->kf_model->sum_today($startdate, $enddate, $entitas);
		$data['wtd'] = $this->kf_model->sum_wtd($startdate, $enddate, $entitas);
		$data['mtd'] = $this->kf_model->sum_mtd($startdate, $enddate, $entitas);
		$data['ytd'] = $this->kf_model->sum_ytd($startdate, $enddate, $entitas);
		$data['growth'] = $this->kf_model->sum_other($startdate, $enddate, $entitas);
		//$data['achieve'] = $this->kf_model->sum_other($startdate,$enddate,$entitas);
		//$data['market_exp'] = $this->kf_model->sum_other($startdate,$enddate,$entitas);
		$data['sales_kf'] = $this->kf_model->sum_kf($startdate, $enddate, $entitas);
		$data['sales_nonkf'] = $this->kf_model->sum_nonkf($startdate, $enddate, $entitas);



		echo json_encode($data);
	}

	public function refresh_balance_sheet()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$day = json_decode($this->input->get_post('day'));
		$period = json_decode($this->input->get_post('period'));

		if (strlen($month) === 1) {
			$month = "0" . $month;
		}
		if (strlen($day) === 1) {
			$day = "0" . $day;
		}

		$days = $year . $month . $day;
		$data['max_lastupdate'] = $this->kf_model->max_lastupdate_balance_sheet($year, $month, $entitas);
		$data['max_onebeforeupdate'] = $this->kf_model->max_lastupdate_balance_sheet(($year - 1), $month, $entitas);
		$max_lastupdate = str_replace("-", "", $data['max_lastupdate'][0]);
		$max_onebeforeupdate = str_replace("-", "", $data['max_onebeforeupdate'][0]);
		$data['total'] = $this->kf_model->balance_sheet_total($days, $month, $year, $entitas, $period, $max_lastupdate, $max_onebeforeupdate);
		$data['data'] = $this->kf_model->balance_sheet_tittle($days, $month, $year, $entitas, $period);
		$data['bs'] = $this->kf_model->balance_sheet_refresh($days, $month, $year, $entitas, $period, $max_lastupdate, $max_onebeforeupdate);

		echo json_encode($data);
	}


	public function refresh_apar()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$day = json_decode($this->input->get_post('day'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$data['ar_sum'] = $this->kf_model->get_ar_sum($month, $year, $day, $entitas);
		$data['ap_sum'] = $this->kf_model->get_ap_sum($month, $year, $day, $entitas);
		$data['ar_idr'] = $this->kf_model->get_ar_idr($month, $year, $day, $entitas);
		$data['ar_nonidr'] = $this->kf_model->get_ar_nonidr($month, $year, $day, $entitas);
		$data['ap_idr'] = $this->kf_model->get_ap_idr($month, $year, $day, $entitas);
		$data['ap_nonidr'] = $this->kf_model->get_ap_nonidr($month, $year, $day, $entitas);
		$data['ar_currency_ketiga'] = $this->kf_model->get_ar_currency_ketiga($month, $year, $day, $entitas);
		$data['ar_currency_berelasi'] = $this->kf_model->get_ar_currency_berelasi($month, $year, $day, $entitas);
		$data['ap_currency_ketiga'] = $this->kf_model->get_ap_currency_ketiga($month, $year, $day, $entitas);
		$data['ap_currency_berelasi'] = $this->kf_model->get_ap_currency_berelasi($month, $year, $day, $entitas);


		echo json_encode($data);
	}


	public function refresh_apar_kftd()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$data['ar_sum'] = $this->kf_model->get_ar_sum_kftd_2($month, $year, $entitas);
		$data['ap_sum'] = $this->kf_model->get_ap_sum_kftd_2($month, $year, $entitas);


		echo json_encode($data);
	}


	public function refresh_cash_collection()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$week = json_decode($this->input->get_post('week'));
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$data['data'] = $this->kf_model->get_cash_collection($week, $month, $year, $entitas);
		$data['ChartInternal'] = $this->kf_model->chart_cash_collection($year, $entitas, 'INTERNAL');
		$data['ChartEksternal'] = $this->kf_model->chart_cash_collection($year, $entitas, 'EKSTERNAL');
		echo json_encode($data);
	}


	public function get_profitcenter()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$entitas = json_decode($this->input->get_post('entitas'));
		$data['data'] = $this->kf_model->get_profit_category($entitas);

		echo json_encode($data);
	}

	public function get_plant()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$daops = json_decode($this->input->get_post('daops'));
		$daops_cat = "";
		$filter = "";
		$is_null = "";
		$i = 0;
		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter = "daops in (" . $daops_cat . ") OR " . $is_null;
				} else {
					$filter = $is_null;
				}
			}
			$data['data'] = $this->kf_model->get_plant_by_daops($filter);
		} else {
			$data['data'] = [];
		}


		echo json_encode($data);
	}


	public function get_plant_pem()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$daops = json_decode($this->input->get_post('daops'));
		$daops_cat = "";
		$filter = "";
		$is_null = "";
		$i = 0;
		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "customer_category is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "customer_category is null";
					}
				}
			}
			if ($is_null === "") {
				$filter = "customer_category in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter = "customer_category in (" . $daops_cat . ") OR " . $is_null;
				} else {
					$filter = $is_null;
				}
			}
			$data['data'] = $this->kf_model->get_plant_by_cust($filter);
		} else {
			$data['data'] = [];
		}


		echo json_encode($data);
	}

	public function get_plant_ap()
	{
		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$daops = json_decode($this->input->get_post('daops'));
		$daops_cat = "";
		$filter = "";
		$is_null = "";
		$i = 0;
		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter = "daops in (" . $daops_cat . ") OR " . $is_null;
				} else {
					$filter = $is_null;
				}
			}
			$data['data'] = $this->kf_model->get_plant_by_daops_ap($filter);
		} else {
			$data['data'] = [];
		}


		echo json_encode($data);
	}

	public function realisasi_investasi_data()
	{
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$data['data'] = $this->kf_model->realisasi_investasi_data($month, $year, $entitas);
		$data['chart'] = $this->kf_model->realisasi_investasi_chart($year, $entitas);
		echo json_encode($data);
	}

	// public function realisasi_investasi_data()
	// {
	// 	ini_set('memory_limit', '-1');
	// 	$this->load->library('Datatablessp');
	// 	$year = json_decode($this->input->get_post('year'));
	// 	$month = json_decode($this->input->get_post('month'));
	// 	$entitas = json_decode($this->input->get_post('entitas'));


	// 	$table = 'datamart_kf.fi_investasi_month cust';
	// 	$primaryKey = 'cust.Id';
	// 	// Table's primary key

	// 	// Array of database columns which should be read and sent back to DataTables.
	// 	// The db parameter represents the column name in the database, while the dt
	// 	// parameter represents the DataTables column identifier. In this case simple
	// 	// indexes

	// 	$columns = array(
	// 		array('db' => 'cust.Id', 'dt' => 0),
	// 		array('db' => 'cust.uraian', 'dt' => 1),
	// 		array('db' => 'cust.target', 'dt' => 2),
	// 		array('db' => 'cust.realisasi', 'dt' => 3),
	// 		array('db' => 'cust.achievement', 'dt' => 4)



	// 	);

	// 	$additional = "";
	// 	$sql_details = array(
	// 		'user' => $this->db->username,
	// 		'pass' => $this->db->password,
	// 		'db'   => $this->db->database,
	// 		'host' => $this->db->hostname
	// 	);



	// 	$filter[] = "month = '" . $month . "'";
	// 	$filter[] = "year = '" . $year . "'";
	// 	$filter[] = "entitas = '" . $entitas . "'";
	// 	$condition = implode(' AND ', $filter);



	// 	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//  * If you just want to use the basic configuration for DataTables with PHP
	//  * server-side, there is no need to edit below this line.
	//  */

	// 	$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

	// 	echo json_encode($result);
	// }


	public function refresh_cashbank()
	{
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$profit_center = json_decode($this->input->get_post('profit_center'));
		$status = json_decode($this->input->get_post('status'));
		$this->load->model('kf_model');
		$data = $this->kf_model->refresh_cashbank($year, $entitas, $month, $profit_center);
		echo json_encode($data);
	}

	// public function refresh_cashbank()
	// {
	// 	ini_set('memory_limit', '-1');
	// 	$this->load->library('Datatablessp');
	// 	$year = json_decode($this->input->get_post('year'));
	// 	$month = json_decode($this->input->get_post('month'));
	// 	$entitas = json_decode($this->input->get_post('entitas'));
	// 	$profit_center = json_decode($this->input->get_post('profit_center'));
	// 	$status = json_decode($this->input->get_post('status'));


	// 	$profit_cat = "";
	// 	$status_cat = "";
	// 	$columns = array();

	// 	// Table's primary key

	// 	// Array of database columns which should be read and sent back to DataTables.
	// 	// The db parameter represents the column name in the database, while the dt
	// 	// parameter represents the DataTables column identifier. In this case simple
	// 	// indexes
	// 	if ($entitas === 'KFHO') {
	// 		$columns = array(
	// 			array('db' => 'cust.profit_center', 'dt' => 0),
	// 			array('db' => 'cust.gl_account', 'dt' => 1),

	// 			array('db' => 'cust.beginning_balance', 'dt' => 2),
	// 			array('db' => 'cust.ending_balance', 'dt' => 3)


	// 		);
	// 	} else if ($entitas === 'KFTD') {
	// 		$columns = array(
	// 			array('db' => 'cust.profit_center', 'dt' => 0),
	// 			array('db' => 'cust.gl_account', 'dt' => 1),
	// 			array('db' => 'cust.status', 'dt' => 2),
	// 			array('db' => 'cust.beginning_balance', 'dt' => 3),
	// 			array('db' => 'cust.ending_balance', 'dt' => 4)


	// 		);
	// 	}


	// 	$additional = "";
	// 	$sql_details = array(
	// 		'user' => $this->db->username,
	// 		'pass' => $this->db->password,
	// 		'db'   => $this->db->database,
	// 		'host' => $this->db->hostname
	// 	);



	// 	$table = "";


	// 	if ($entitas === 'KFHO') {
	// 		$table = 'datamart_kf.f_kfho_cashbank cust';
	// 	} else if ($entitas === 'KFTD') {
	// 		$table = 'datamart_kf.f_kftd_cashbank cust';
	// 	}


	// 	$primaryKey = 'cust.Id';

	// 	$i = 0;
	// 	if (sizeof(($profit_center)) !== 0) {
	// 		foreach ($profit_center as $key => $value) {
	// 			$i++;

	// 			if ($i === sizeof($profit_center)) {
	// 				$profit_cat .= "'" . $value . "'";
	// 			} else {
	// 				$profit_cat .= "'" . $value . "',";
	// 			}
	// 		}
	// 		$filter[] = "profit_center in (" . $profit_cat . ")";
	// 	}

	// 	$i = 0;
	// 	if (sizeof(($status)) !== 0) {
	// 		foreach ($status as $key => $value) {
	// 			$i++;

	// 			if ($i === sizeof($status)) {
	// 				$status_cat .= "'" . $value . "'";
	// 			} else {
	// 				$status_cat .= "'" . $value . "',";
	// 			}
	// 		}
	// 		$filter[] = "status in (" . $status_cat . ")";
	// 	}

	// 	$filter[] = "month = '" . $month . "'";
	// 	$filter[] = "year = '" . $year . "'";
	// 	if ($entitas !== "") {
	// 		$filter[] = "entitas = '" . $entitas . "'";
	// 	}




	// 	$condition = implode(' AND ', $filter);


	// 	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	//  * If you just want to use the basic configuration for DataTables with PHP
	//  * server-side, there is no need to edit below this line.
	//  */

	// 	$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);
	// 	echo json_encode($result);
	// }

	public function balance_sheet_ap()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$age = json_decode($this->input->get_post('age'));

		if ($entitas === 'KFHO') {
			$table = 'datamart_kf.f_kfho_ap cust';
		} else if ($entitas === 'KFTD') {
			$table = 'datamart_kf.f_kftd_ap cust';
		}

		$primaryKey = 'cust.Id';
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.vendor_desc', 'dt' => 0),
			array('db' => 'cust.total', 'dt' => 1),
			array('db' => 'cust.aging', 'dt' => 2),
			array('db' => 'cust.aging_category', 'dt' => 3),
			array('db' => 'cust.aging_status', 'dt' => 4)



		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";
		$filter[] = "entitas = '" . $entitas . "'";
		$filter[] = "aging_category = '" . $age . "'";
		$condition = implode(' AND ', $filter);



		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}


	public function balance_sheet_ap2()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$day = json_decode($this->input->get_post('day'));
		$condition = "";

		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;

		if ($entitas === 'KFHO') {
			//$table = 'datamart_kf.f_kfho_ap_v3 cust';
			$table = "(select Id as Id,vendor_desc as vendor_desc,text_prc as text_prc ,text_gl as text_gl, 
sum(total) as total, 
sum(0_30) as 0_30,
sum(31_60) as 31_60,
sum(61_90) as 61_90,
sum(91_120) as 91_120,
sum(121_150) as 121_150,
sum(151_360) as 151_360,
sum(360_) as 360_
from datamart_kf.f_kfho_ap_v3 where STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE('" . $filter_date . "','%Y%m%d') AND entitas='" . $entitas . "' 
group by vendor_desc,text_prc ,text_gl) cust";
		} else if ($entitas === 'KFTD') {
			$table = 'datamart_kf.f_kftd_ap_v2 cust';
		} else {
			$table = 'datamart_kf.f_kfho_ap_v3 cust';
			$filter[] = "entitas = '" . $entitas . "'";

			$condition = implode(' AND ', $filter);
		}

		$primaryKey = 'cust.Id';
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.vendor_desc', 'dt' => 0),
			array('db' => 'cust.text_prc', 'dt' => 1),
			array('db' => 'cust.text_gl', 'dt' => 2),
			array('db' => 'cust.total', 'dt' => 3),
			array('db' => 'cust.0_30', 'dt' => 4),
			array('db' => 'cust.31_60', 'dt' => 5),
			array('db' => 'cust.61_90', 'dt' => 6),
			array('db' => 'cust.91_120', 'dt' => 7),
			array('db' => 'cust.121_150', 'dt' => 8),
			array('db' => 'cust.151_360', 'dt' => 9),
			array('db' => 'cust.360_', 'dt' => 10)



		);

		$additional = "";
		$group_by = " GROUP BY vendor_desc, text_prc, text_gl ";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);


		//		$filter[] = "day = '" . $day . "'";
		//		$filter[] = "month = '" . $month . "'";
		//		$filter[] = "year = '" . $year . "'";
		//		$filter[] = "entitas = '" . $entitas . "'";

		//		$condition = implode(' AND ', $filter);




		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}


	public function balance_sheet_ar2()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$day = json_decode($this->input->get_post('day'));
		$condition = "";
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;

		if ($entitas === 'KFHO') {
			//$table = 'datamart_kf.f_kfho_ar_v2 cust';
			$table = "(select Id as Id,customer_desc as customer_desc,text_prc as text_prc ,text_gl as text_gl, 
sum(total) as total, 
sum(0_30) as 0_30,
sum(31_60) as 31_60,
sum(61_90) as 61_90,
sum(91_120) as 91_120,
sum(121_150) as 121_150,
sum(151_360) as 151_360,
sum(360_) as 360_
from datamart_kf.f_kfho_ar_v3 where STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE('" . $filter_date . "','%Y%m%d') AND entitas='" . $entitas . "' 
group by customer_desc,text_prc ,text_gl) cust";
		} else if ($entitas === 'KFTD') {
			$table = 'datamart_kf.f_kftd_ar_v2 cust';
		} else {
			$table = 'datamart_kf.f_kfho_ar_v3 cust';
			$filter[] = "entitas = '" . $entitas . "'";

			$condition = implode(' AND ', $filter);
		}

		$primaryKey = 'cust.Id';
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.customer_desc', 'dt' => 0),
			array('db' => 'cust.text_prc', 'dt' => 1),
			array('db' => 'cust.text_gl', 'dt' => 2),
			array('db' => 'cust.total', 'dt' => 3),
			array('db' => 'cust.0_30', 'dt' => 4),
			array('db' => 'cust.31_60', 'dt' => 5),
			array('db' => 'cust.61_90', 'dt' => 6),
			array('db' => 'cust.91_120', 'dt' => 7),
			array('db' => 'cust.121_150', 'dt' => 8),
			array('db' => 'cust.151_360', 'dt' => 9),
			array('db' => 'cust.360_', 'dt' => 10)

		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);


		//		$filter[] = "day = '" . $day . "'";
		//		$filter[] = "month = '" . $month . "'";
		//		$filter[] = "year = '" . $year . "'";
		//		$filter[] = "entitas = '" . $entitas . "'";
		//
		//		$condition = implode(' AND ', $filter);




		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}

	public function pemasukan_ar2_kftd()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$cust_cat = json_decode($this->input->get_post('cust_cat'));
		$plant = json_decode($this->input->get_post('plant'));
		$segment1 = json_decode($this->input->get_post('segment1'));
		$segment3 = json_decode($this->input->get_post('segment3'));

		$table = 'datamart_kf.f_kftd_ar_v2 cust';

		$primaryKey = 'cust.Id';
		$daops = "";
		$daops_cat = "";
		$cust_cat = "";
		$plant_cat = "";
		$segment1_cat = "";
		$segment3_cat = "";
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.Id', 'dt' => 0),
			array('db' => 'cust.daops', 'dt' => 1),
			array('db' => 'cust.plant', 'dt' => 2),
			array('db' => 'cust.segment_1', 'dt' => 3),
			array('db' => 'cust.segment_3', 'dt' => 4),
			array('db' => 'cust.0_30', 'dt' => 5),
			array('db' => 'cust.31_60', 'dt' => 6),
			array('db' => 'cust.61_90', 'dt' => 7),
			array('db' => 'cust.91_120', 'dt' => 8),
			array('db' => 'cust.121_150', 'dt' => 9),
			array('db' => 'cust.151_360', 'dt' => 10),
			array('db' => 'cust.360_', 'dt' => 11),
			array('db' => 'cust.total', 'dt' => 12)

		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";
		$filter[] = "entitas = '" . $entitas . "'";


		$i = 0;
		$is_null = "";


		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter[] = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter[] = "(daops in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter[] = $is_null;
				}
			}
		}

		$i = 0;
		if (sizeof(($plant)) !== 0) {
			foreach ($plant as $key => $value) {
				$i++;

				if ($i === sizeof($plant)) {
					$plant_cat .= "'" . $value . "'";
				} else {
					$plant_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "plant in (" . $plant_cat . ")";
		}

		$i = 0;
		if (sizeof(($segment1)) !== 0) {
			foreach ($segment1 as $key => $value) {
				$i++;

				if ($i === sizeof($segment1)) {
					$segment1_cat .= "'" . $value . "'";
				} else {
					$segment1_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_1 in (" . $segment1_cat . ")";
		}


		$i = 0;
		if (sizeof(($segment3)) !== 0) {
			foreach ($segment3 as $key => $value) {
				$i++;

				if ($i === sizeof($segment3)) {
					$segment3_cat .= "'" . $value . "'";
				} else {
					$segment3_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_3 in (" . $segment3_cat . ")";
		}

		$condition = implode(' AND ', $filter);
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}

	public function balance_sheet_ar2_kftd()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$daops = json_decode($this->input->get_post('daops'));
		$plant = json_decode($this->input->get_post('plant'));
		$segment1 = json_decode($this->input->get_post('segment1'));
		$segment3 = json_decode($this->input->get_post('segment3'));




		$table = 'datamart_kf.f_kftd_ar_v2 cust';

		$primaryKey = 'cust.Id';

		$daops_cat = "";
		$plant_cat = "";
		$segment1_cat = "";
		$segment3_cat = "";
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.Id', 'dt' => 0),
			array('db' => 'cust.daops', 'dt' => 1),
			array('db' => 'cust.plant', 'dt' => 2),
			array('db' => 'cust.segment_1', 'dt' => 3),
			array('db' => 'cust.segment_3', 'dt' => 4),
			array('db' => 'cust.0_30', 'dt' => 5),
			array('db' => 'cust.31_60', 'dt' => 6),
			array('db' => 'cust.61_90', 'dt' => 7),
			array('db' => 'cust.91_120', 'dt' => 8),
			array('db' => 'cust.121_150', 'dt' => 9),
			array('db' => 'cust.151_360', 'dt' => 10),
			array('db' => 'cust.360_', 'dt' => 11),
			array('db' => 'cust.total', 'dt' => 12)

		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";
		$filter[] = "entitas = '" . $entitas . "'";


		$i = 0;
		$is_null = "";


		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter[] = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter[] = "(daops in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter[] = $is_null;
				}
			}
		}

		$i = 0;
		if (sizeof(($plant)) !== 0) {
			foreach ($plant as $key => $value) {
				$i++;

				if ($i === sizeof($plant)) {
					$plant_cat .= "'" . $value . "'";
				} else {
					$plant_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "plant in (" . $plant_cat . ")";
		}

		$i = 0;
		if (sizeof(($segment1)) !== 0) {
			foreach ($segment1 as $key => $value) {
				$i++;

				if ($i === sizeof($segment1)) {
					$segment1_cat .= "'" . $value . "'";
				} else {
					$segment1_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_1 in (" . $segment1_cat . ")";
		}


		$i = 0;
		if (sizeof(($segment3)) !== 0) {
			foreach ($segment3 as $key => $value) {
				$i++;

				if ($i === sizeof($segment3)) {
					$segment3_cat .= "'" . $value . "'";
				} else {
					$segment3_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_3 in (" . $segment3_cat . ")";
		}

		$condition = implode(' AND ', $filter);
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}

	public function balance_sheet_ar2_kftd_pem()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$daops = json_decode($this->input->get_post('daops'));
		$plant = json_decode($this->input->get_post('plant'));
		$segment1 = json_decode($this->input->get_post('segment1'));
		$segment3 = json_decode($this->input->get_post('segment3'));




		$table = 'datamart_kf.f_kftd_ar_pemasukan cust';

		$primaryKey = 'cust.Id';

		$daops_cat = "";
		$plant_cat = "";
		$segment1_cat = "";
		$segment3_cat = "";
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.Id', 'dt' => 0),
			array('db' => 'cust.customer_category', 'dt' => 1),
			array('db' => 'cust.text_prc', 'dt' => 2),
			array('db' => 'cust.segment_1', 'dt' => 3),
			array('db' => 'cust.segment_3', 'dt' => 4),
			array('db' => 'cust.pemasukan', 'dt' => 5),
			array('db' => 'cust.year', 'dt' => 6),
			array('db' => 'cust.month', 'dt' => 7),
			array('db' => 'cust.day', 'dt' => 8)
		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";


		$i = 0;
		$is_null = "";


		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "customer_category is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "customer_category is null";
					}
				}
			}
			if ($is_null === "") {
				$filter[] = "customer_category in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter[] = "(customer_category in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter[] = $is_null;
				}
			}
		}

		$i = 0;
		if (sizeof(($plant)) !== 0) {
			foreach ($plant as $key => $value) {
				$i++;

				if ($i === sizeof($plant)) {
					$plant_cat .= "'" . $value . "'";
				} else {
					$plant_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "text_prc in (" . $plant_cat . ")";
		}

		$i = 0;
		if (sizeof(($segment1)) !== 0) {
			foreach ($segment1 as $key => $value) {
				$i++;

				if ($i === sizeof($segment1)) {
					$segment1_cat .= "'" . $value . "'";
				} else {
					$segment1_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_1 in (" . $segment1_cat . ")";
		}


		$i = 0;
		if (sizeof(($segment3)) !== 0) {
			foreach ($segment3 as $key => $value) {
				$i++;

				if ($i === sizeof($segment3)) {
					$segment3_cat .= "'" . $value . "'";
				} else {
					$segment3_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment_3 in (" . $segment3_cat . ")";
		}

		$condition = implode(' AND ', $filter);
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */
		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}

	public function balance_sheet_ap2_kftd()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$daops = json_decode($this->input->get_post('daops'));
		$plant = json_decode($this->input->get_post('plant'));
		$segment1 = json_decode($this->input->get_post('segment1'));




		$table = 'datamart_kf.f_kftd_ap_v2 cust';

		$primaryKey = 'cust.Id';

		$daops_cat = "";
		$plant_cat = "";
		$segment1_cat = "";
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.Id', 'dt' => 0),
			array('db' => 'cust.daops', 'dt' => 1),
			array('db' => 'cust.plant', 'dt' => 2),
			array('db' => 'cust.segment', 'dt' => 3),
			array('db' => 'cust.0_30', 'dt' => 4),
			array('db' => 'cust.31_60', 'dt' => 5),
			array('db' => 'cust.61_90', 'dt' => 6),
			array('db' => 'cust.91_120', 'dt' => 7),
			array('db' => 'cust.121_150', 'dt' => 8),
			array('db' => 'cust.151_360', 'dt' => 9),
			array('db' => 'cust.360_', 'dt' => 10),
			array('db' => 'cust.total', 'dt' => 11)

		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";
		$filter[] = "entitas = '" . $entitas . "'";


		$i = 0;
		$is_null = "";


		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter[] = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter[] = "(daops in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter[] = $is_null;
				}
			}
		}

		$i = 0;
		if (sizeof(($plant)) !== 0) {
			foreach ($plant as $key => $value) {
				$i++;

				if ($i === sizeof($plant)) {
					$plant_cat .= "'" . $value . "'";
				} else {
					$plant_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "plant in (" . $plant_cat . ")";
		}

		$i = 0;
		if (sizeof(($segment1)) !== 0) {
			foreach ($segment1 as $key => $value) {
				$i++;

				if ($i === sizeof($segment1)) {
					$segment1_cat .= "'" . $value . "'";
				} else {
					$segment1_cat .= "'" . $value . "',";
				}
			}
			$filter[] = "segment in (" . $segment1_cat . ")";
		}




		$condition = implode(' AND ', $filter);
		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}


	public function kftd_tipe_segment_ap()
	{

		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$daops = json_decode($this->input->get_post('daops'));
		$data['ar_sum'] = $this->kf_model->get_ar_sum_kftd_2($month, $year, $entitas);
		$data['ap_sum'] = $this->kf_model->get_ap_sum_kftd_2($month, $year, $entitas);

		$daops_cat = "";
		$filter = "";
		$is_null = "";
		$i = 0;
		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter = "(daops in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter = $is_null;
				}
			}
			$data['data'] = $this->kf_model->get_group_tipe_segment_ap($filter, $year, $month, $entitas);
			$data['data_chart'] = $this->kf_model->get_chart_kftd_ap($filter, $year, $month, $entitas);
		} else {
			$data['data'] = [];
			$data['data_chart'] = [];
		}



		echo json_encode($data);
	}



	public function kftd_tipe_segment()
	{

		ini_set('memory_limit', '-1');
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));

		$daops = json_decode($this->input->get_post('daops'));

		$data['ar_sum'] = $this->kf_model->get_ar_sum_kftd_2($month, $year, $entitas);
		$data['ap_sum'] = $this->kf_model->get_ap_sum_kftd_2($month, $year, $entitas);
		$daops_cat = "";
		$filter = "";
		$is_null = "";
		$i = 0;
		if (sizeof(($daops)) !== 0) {
			foreach ($daops as $key => $value) {
				$i++;

				if ($i === sizeof($daops)) {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "'";
					} else {
						$is_null = "daops is null";
					}
				} else {
					if ($value !== "NULL") {
						$daops_cat .= "'" . $value . "',";
					} else {
						$is_null = "daops is null";
					}
				}
			}
			if ($is_null === "") {
				$filter = "daops in (" . $daops_cat . ")";
			} else {
				if (sizeof($daops) > 1) {
					$filter = "(daops in (" . $daops_cat . ") OR " . $is_null . ")";
				} else {
					$filter = $is_null;
				}
			}

			$data['data'] = $this->kf_model->get_group_tipe_segment($filter, $year, $month, $entitas);
			$data['data_chart'] = $this->kf_model->get_chart_kftd($filter, $year, $month, $entitas);
		} else {
			$data['data'] = [];
			$data['data_chart'] = [];
		}



		echo json_encode($data);
	}



	public function balance_sheet_ar()
	{
		ini_set('memory_limit', '-1');
		$this->load->library('Datatablessp');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$age = json_decode($this->input->get_post('age'));



		if ($entitas === 'KFHO') {
			$table = 'datamart_kf.f_kfho_ar cust';
		} else if ($entitas === 'KFTD') {
			$table = 'datamart_kf.f_kftd_ar cust';
		}

		$primaryKey = 'cust.Id';
		// Table's primary key

		// Array of database columns which should be read and sent back to DataTables.
		// The db parameter represents the column name in the database, while the dt
		// parameter represents the DataTables column identifier. In this case simple
		// indexes

		$columns = array(
			array('db' => 'cust.customer_desc', 'dt' => 0),
			array('db' => 'cust.total', 'dt' => 1),
			array('db' => 'cust.aging', 'dt' => 2),
			array('db' => 'cust.aging_category', 'dt' => 3),
			array('db' => 'cust.aging_status', 'dt' => 4)

		);

		$additional = "";
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);



		$filter[] = "month = '" . $month . "'";
		$filter[] = "year = '" . $year . "'";
		$filter[] = "entitas = '" . $entitas . "'";
		$filter[] = "aging_category = '" . $age . "'";
		$condition = implode(' AND ', $filter);



		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $additional);

		echo json_encode($result);
	}

	public function login()
	{
		$data['user'] = $this->input->post('username');
		$data['pass'] = $this->input->post('password');

		$this->load->model('user_model');

		$cek = $this->user_model->login($data, true);
		if (array_key_exists('email', $cek)) {
			$this->session->set_userdata(
				array(
					'username' => $data['user'],
					'nama' => $cek['nama'],
					'email' => $cek['email'],
					'hp' => $cek['hp'],
					'level' => $cek['level'],
					'unit' => $cek['unit'],
					'role_entitas' => explode(",", $cek['entitas']),
					'loged_in' => true
				)
			);
		}
		$_SESSION['photo'] = base_url() . "asset/user.png";
		redirect('page/index');
	}

	public function logout()
	{
		session_destroy();
		redirect('page/index');
	}


	public function getAdjusmentIncomeStatement()
	{

		$year = $this->input->get_post('year');
		$entitas = $this->input->get_post('entitas');
		$month = $this->input->get_post('month');
		// 		ini_set('memory_limit', '-1');
		// 		$this->load->library('Datatablessp');

		// 		$table = 'datamart_kf.kf_target_adj_income cust';
		// 		$primaryKey = 'cust.Id';
		// 		// Table's primary key

		// 		// Array of database columns which should be read and sent back to DataTables.
		// 		// The db parameter represents the column name in the database, while the dt
		// 		// parameter represents the DataTables column identifier. In this case simple
		// 		// indexes

		// 		$columns = array(
		// 			array('db' => 'cust.Id', 'dt' => 'id'),
		// 			array('db' => 'cust.month', 'dt' => 'month'),
		// 			array('db' => 'cust.entitas', 'dt' => 'entitas'),
		// 			array('db' => 'cust.uraian', 'dt' => 'uraian'),
		// 			array('db' => 'cust.target', 'dt' => 'target'),
		// 			array('db' => 'cust.real', 'dt' => 'real'),
		// 			array('db' => 'cust.f01', 'dt' => 'f01'),
		// 			array('db' => 'cust.adjusment', 'dt' => 'adjusment'),
		// 			array('db' => 'cust.year', 'dt' => 'year')



		// 		);

		// 		$additional = "";
		// 		$condition = "";
		// 		$where = $year;

		// 		$sql_details = array(
		// 			'user' => $this->db->username,
		// 			'pass' => $this->db->password,
		// 			'db'   => $this->db->database,
		// 			'host' => $this->db->hostname
		// 		);



		// 		/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		// * If you just want to use the basic configuration for DataTables with PHP
		// * server-side, there is no need to edit below this line.
		// */

		// 		$result =  $this->datatablessp->complex($_POST, $sql_details, $table, $primaryKey, $columns, $condition, $condition, $where);

		// 		echo json_encode($result);


		$this->load->model('kf_model');
		$data = $this->kf_model->getAdjusmentIncomeStatement($year, $entitas, $month);
		echo json_encode($data);
	}

	public function balance_sheet_aging()
	{
		$this->load->model('kf_model');
		$year = json_decode($this->input->get_post('year'));
		$month = json_decode($this->input->get_post('month'));
		$entitas = json_decode($this->input->get_post('entitas'));
		$day = json_decode($this->input->get_post('day'));
		$table = json_decode($this->input->get_post('table'));
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;

		$data = $this->kf_model->balance_sheet_aging($filter_date, $entitas, $table);
		echo json_encode($data);
	}

	public function postAdjustment()
	{
		$this->load->model('kf_model');
		$id = $this->input->get_post('id');
		$adjusment = $this->input->get_post('adjusment');
		$entitas = $this->input->get_post('entitas');
		$f01 = $this->input->get_post('f01');
		$month = $this->input->get_post('month');
		$real = $this->input->get_post('real');
		$target = $this->input->get_post('target');
		$uraian = $this->input->get_post('uraian');
		$year = $this->input->get_post('year');
		$delete = $this->input->get_post('delete');
		$data['result'] = $this->kf_model->postAdjustment($id, $adjusment, $entitas, $f01, $month, $real, $target, $uraian, $year, $delete, $this->session->userdata('nama'));
		echo json_encode($data);
	}
}
