<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UraianMaintenance extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');        
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('UraianMaintenance/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'uraian_maintenance')
    {
        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/form/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getDataUrain()
    {
        $this->load->model('Uraian_model');
        $ket = $this->input->get_post('ket'); 
        $data = $this->Uraian_model->getUraian($ket);
        echo json_encode($data);
    }

    public function InsertDataUraian()
    {
        $this->load->model('Uraian_model');
        $forTo = $this->input->get_post('inform');
        $desc = $this->input->get_post('desc'); 
        $groupBS = $this->input->get_post('groupBS'); 
        $data = $this->Uraian_model->InsertDataUraian($forTo, $desc, $groupBS);
        echo json_encode($data);
    }

    public function UpdateDataUraian()
    {
        $this->load->model('Uraian_model');
        $id = $this->input->get_post('id'); 
        $forTo = $this->input->get_post('inform');
        $desc = $this->input->get_post('desc');     
        $groupBS = $this->input->get_post('groupBS'); 
        $data = $this->Uraian_model->UpdateDataUraian($id,$forTo, $desc,$groupBS);
        echo json_encode($data);
    }

    public function DeleteDataUraian()
    {
        $this->load->model('Uraian_model');
        $id = $this->input->get_post('id'); 
        $data = $this->Uraian_model->DeleteDataUraian($id);
        echo json_encode($data);
    }    
}
