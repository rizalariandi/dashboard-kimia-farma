<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Marketing_overview extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
        $this->load->model('overview_marketing');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Overview_marketing/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'overview_marketing')
    {

        if ($this->session->userdata('loged_in') !== null) :
            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter            
            $this->load->view('templates/header', $data);
            $this->load->view('pages/marketing_php/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function detail($page = 'detail_marketing')
    {
        // $this->load->model('overview_marketing');
        if ($this->session->userdata('loged_in') !== null) :
            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter                        
            $data['layanan'] =  $this->overview_marketing->layanan('');
            $data['group_layanan'] =  $this->overview_marketing->group_layanan();
            $this->load->view('templates/header', $data);
            $this->load->view('pages/marketing_php/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getDetail()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $start = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $branch = json_decode($this->input->get_post('branch'));
        $segment = json_decode($this->input->get_post('segment'));
        $customer = json_decode($this->input->get_post('customer'));  
        $material_brand = json_decode($this->input->get_post('material_brand'));                
        $material_name = json_decode($this->input->get_post('material_name'));                
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
        $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));        
        $data["detail_segment"] = $this->overview_marketing->detail_segment($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        $data["detail_customer_name"] = $this->overview_marketing->detail_customer_name($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        $data["detail_product_brand"] = $this->overview_marketing->detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        $data["detail_product"] = $this->overview_marketing->detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        echo json_encode($data);
    }
    // public function getDetailSegment()
    // {
    //     // $this->load->model('overview_marketing');
    //     $value = json_decode($this->input->get_post('value'));
    //     $start = json_decode($this->input->get_post('start'));
    //     $end = json_decode($this->input->get_post('end'));
    //     $branch = json_decode($this->input->get_post('branch'));
    //     $segment = json_decode($this->input->get_post('segment'));
    //     $customer = json_decode($this->input->get_post('customer'));  
    //     $material_brand = json_decode($this->input->get_post('material_brand'));                
    //     $material_name = json_decode($this->input->get_post('material_name'));                
    //     $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
    //     $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));        
    //     $data["detail_segment"] = $this->overview_marketing->detail_segment($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        
    //     echo json_encode($data);
    // }

    // public function getDetailCustomerName()
    // {
    //     // $this->load->model('overview_marketing');
    //     $value = json_decode($this->input->get_post('value'));
    //     $start = json_decode($this->input->get_post('start'));
    //     $end = json_decode($this->input->get_post('end'));
    //     $branch = json_decode($this->input->get_post('branch'));
    //     $segment = json_decode($this->input->get_post('segment'));
    //     $customer = json_decode($this->input->get_post('customer'));  
    //     $material_brand = json_decode($this->input->get_post('material_brand'));                
    //     $material_name = json_decode($this->input->get_post('material_name'));                
    //     $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
    //     $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));        
        
    //     $data["detail_customer_name"] = $this->overview_marketing->detail_customer_name($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        
    //     echo json_encode($data);
    // }

    // public function getDetailProductBrand()
    // {
    //     // $this->load->model('overview_marketing');
    //     $value = json_decode($this->input->get_post('value'));
    //     $start = json_decode($this->input->get_post('start'));
    //     $end = json_decode($this->input->get_post('end'));
    //     $branch = json_decode($this->input->get_post('branch'));
    //     $segment = json_decode($this->input->get_post('segment'));
    //     $customer = json_decode($this->input->get_post('customer'));  
    //     $material_brand = json_decode($this->input->get_post('material_brand'));                
    //     $material_name = json_decode($this->input->get_post('material_name'));                
    //     $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
    //     $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));        
        
    //     $data["detail_product_brand"] = $this->overview_marketing->detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        
    //     echo json_encode($data);
    // }

    // public function getDetailProductOnly()
    // {
    //     // $this->load->model('overview_marketing');
    //     $value = json_decode($this->input->get_post('value'));
    //     $start = json_decode($this->input->get_post('start'));
    //     $end = json_decode($this->input->get_post('end'));
    //     $branch = json_decode($this->input->get_post('branch'));
    //     $segment = json_decode($this->input->get_post('segment'));
    //     $customer = json_decode($this->input->get_post('customer'));  
    //     $material_brand = json_decode($this->input->get_post('material_brand'));                
    //     $material_name = json_decode($this->input->get_post('material_name'));                
    //     $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
    //     $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));        
    //     $data["detail_product"] = $this->overview_marketing->detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
    //     echo json_encode($data);
    // }

    public function getDetail_segment()
    {
        $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $start = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $branch = json_decode($this->input->get_post('branch'));
        $segment = json_decode($this->input->get_post('segment'));
        $customer = json_decode($this->input->get_post('customer'));  
        $material_brand = json_decode($this->input->get_post('material_brand'));                
        $material_name = json_decode($this->input->get_post('material_name'));                
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
        $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));  
        
        $data["detail_segment"] = $this->overview_marketing->detail_segment($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        // $data["detail_customer_name"] = $this->overview_marketing->detail_customer_name($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        // $data["detail_product_brand"] = $this->overview_marketing->detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        // $data["detail_product"] = $this->overview_marketing->detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
      

        echo json_encode($data);
    }

    public function getDetail_customer_name()
    {
        $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $start = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $branch = json_decode($this->input->get_post('branch'));
        $segment = json_decode($this->input->get_post('segment'));
        $customer = json_decode($this->input->get_post('customer'));  
        $material_brand = json_decode($this->input->get_post('material_brand'));                
        $material_name = json_decode($this->input->get_post('material_name'));                
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
        $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));  

        $data["detail_customer_name"] = $this->overview_marketing->detail_customer_name($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        
        echo json_encode($data);
    }

    public function getDetail_product_brand()
    {
        $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $start = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $branch = json_decode($this->input->get_post('branch'));
        $segment = json_decode($this->input->get_post('segment'));
        $customer = json_decode($this->input->get_post('customer'));  
        $material_brand = json_decode($this->input->get_post('material_brand'));                
        $material_name = json_decode($this->input->get_post('material_name'));                
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
        $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));  

        // $data["detail_product_brand"] = $this->overview_marketing->detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        $data["detail_product_brand"] = $this->overview_marketing->detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        

        echo json_encode($data);
    }

    public function getDetail_product()
    {
        $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $start = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $branch = json_decode($this->input->get_post('branch'));
        $segment = json_decode($this->input->get_post('segment'));
        $customer = json_decode($this->input->get_post('customer'));  
        $material_brand = json_decode($this->input->get_post('material_brand'));                
        $material_name = json_decode($this->input->get_post('material_name'));                
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));
        $layanan_name = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_name')));  

        // $data["detail_product"] = $this->overview_marketing->detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        $data["detail_product"] = $this->overview_marketing->detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer,$material_brand,$material_name);
        

        echo json_encode($data);
    }

    public function getSpesial()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));
        $forMTD = json_decode($this->input->get_post('forMTD'));
        $data["sales_branch_mtd"] = $this->overview_marketing->sales_branch_mtd($value, $Cend, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
        if (!$forMTD) {
            $data["sales_branch_ytd"] = $this->overview_marketing->sales_branch_ytd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
        }
        echo json_encode($data);
    }

    // public function getSummary()
    // {
    //     // $this->load->model('overview_marketing');
    //     $value = json_decode($this->input->get_post('value'));
    //     $star = json_decode($this->input->get_post('start'));
    //     $end = json_decode($this->input->get_post('end'));
    //     $Lstart = json_decode($this->input->get_post('Lstart'));
    //     $Lend = json_decode($this->input->get_post('Lend'));
    //     $mtd = json_decode($this->input->get_post('mtd'));
    //     $ytd = json_decode($this->input->get_post('ytd'));
    //     $lini = json_decode($this->input->get_post('lini'));
    //     $segment = json_decode($this->input->get_post('segment'));
    //     // $material_brand = json_decode($this->input->get_post('material_brand'));
    //     $brand = json_decode($this->input->get_post('brand'));
    //     $Cend = json_decode($this->input->get_post('Cend'));
    //     $order = json_decode($this->input->get_post('order'));
    //     $by = json_decode($this->input->get_post('by'));
    //     $map = json_decode($this->input->get_post('map'));

    //     $data["distributor_kftd"] = $this->overview_marketing->distributor_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["other_kftd"] = $this->overview_marketing->other_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["MTD"] = $this->overview_marketing->MTD($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["YTD"] = $this->overview_marketing->YTD($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["marketing_expense"] = $this->overview_marketing->marketing_expense($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["sales_contribution"] = $this->overview_marketing->sales_contribution($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["achievement_distribution"] = $this->overview_marketing->achievement_distribution($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["sales_branch_mtd"] = $this->overview_marketing->sales_branch_mtd($value, $Cend, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
    //     $data["sales_branch_ytd"] = $this->overview_marketing->sales_branch_ytd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
    //     $data["sales_lini"] = $this->overview_marketing->sales_lini($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["trend_sales"] = $this->overview_marketing->trend_sales($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["sales_segment"] = $this->overview_marketing->sales_segment($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["top_brand"] = $this->overview_marketing->top_brand($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     $data["sales_product"] = $this->overview_marketing->sales_product($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
    //     echo json_encode($data);        
    // }

    public function distributor_kftd(){
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        // $Cend = json_decode($this->input->get_post('Cend'));
        // $order = json_decode($this->input->get_post('order'));
        // $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["distributor_kftd"] = $this->overview_marketing->distributor_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data); 
    }

    public function other_kftd(){
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        // $Cend = json_decode($this->input->get_post('Cend'));
        // $order = json_decode($this->input->get_post('order'));
        // $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));
        
        $data["other_kftd"] = $this->overview_marketing->other_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data); 
    }

    public function MTD(){
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));
        
        $data["MTD"] = $this->overview_marketing->MTD($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data); 
    }

    public function YTD(){
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));
        
        $data["YTD"] = $this->overview_marketing->YTD($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);
    }

    public function marketing_expense()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        // $Cend = json_decode($this->input->get_post('Cend'));
        // $order = json_decode($this->input->get_post('order'));
        // $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["marketing_expense"] = $this->overview_marketing->marketing_expense($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);        
    }

    public function sales_contribution()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_contribution"] = $this->overview_marketing->sales_contribution($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        
        echo json_encode($data);        
    }

    public function achievement_distribution()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $map = json_decode($this->input->get_post('map'));

        $data["achievement_distribution"] = $this->overview_marketing->achievement_distribution($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        
        echo json_encode($data);        
    }

    public function sales_branch_mtd()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_branch_mtd"] = $this->overview_marketing->sales_branch_mtd($value, $Cend, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
        
        echo json_encode($data);        
    }

    public function sales_branch_ytd()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_branch_ytd"] = $this->overview_marketing->sales_branch_ytd($value, $star, $Cend, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map);
        echo json_encode($data);        
    }

    public function sales_lini()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_lini"] = $this->overview_marketing->sales_lini($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);        
    }

    public function trend_sales()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $map = json_decode($this->input->get_post('map'));

        $data["trend_sales"] = $this->overview_marketing->trend_sales($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        
        echo json_encode($data);        
    }

    public function sales_segment()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_segment"] = $this->overview_marketing->sales_segment($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);      
    }

    public function top_brand()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["top_brand"] = $this->overview_marketing->top_brand($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);        
    }

    public function sales_product()
    {
        // $this->load->model('overview_marketing');
        $value = json_decode($this->input->get_post('value'));
        $star = json_decode($this->input->get_post('start'));
        $end = json_decode($this->input->get_post('end'));
        $Lstart = json_decode($this->input->get_post('Lstart'));
        $Lend = json_decode($this->input->get_post('Lend'));
        $mtd = json_decode($this->input->get_post('mtd'));
        $ytd = json_decode($this->input->get_post('ytd'));
        $lini = json_decode($this->input->get_post('lini'));
        $segment = json_decode($this->input->get_post('segment'));
        // $material_brand = json_decode($this->input->get_post('material_brand'));
        $brand = json_decode($this->input->get_post('brand'));
        $Cend = json_decode($this->input->get_post('Cend'));
        $order = json_decode($this->input->get_post('order'));
        $by = json_decode($this->input->get_post('by'));
        $map = json_decode($this->input->get_post('map'));

        $data["sales_product"] = $this->overview_marketing->sales_product($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map);
        echo json_encode($data);        
    }

    function layanan_name(){      
        $this->load->model('overview_marketing');  
        $layanan_group = $this->overview_marketing->forIn(json_decode($this->input->get_post('layanan_group')));        
        $data = $this->overview_marketing->layanan($layanan_group);
        echo json_encode($data);
    }
}
