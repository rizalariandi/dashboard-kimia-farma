<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_b2b_level_rule extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('Crm_b2b_level_rule_model', 'dm');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
        // ini_set('memory_limit',-1);
	}

    public function index()
    {
        //ini_set('max_execution_time', 3600);
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";
 
        $data['title']	= 'B2B Level Rule'; 

        $year	    = $this->dm->getYear()->result(); 
        $temp = array();
        foreach($year as $row){
            $temp[] = $row->cct_year;
        }
        for($i=1;$i<=5;$i++){
            $temp[] = $temp[count($temp)-1]+1;
        }
        
        $data['year']	    = $temp; 
        $lini	    = $this->dm->getLini()->result(); 
        $layanan	= $this->dm->getLayanan()->result();  

        $linis  = array();          
        $linis['all'] = 'All Lini';
        foreach($lini as $row){
            $linis[$row->lini] = $row->lini;
        }

        $layanans  = array();          
        $layanans['all'] = 'All Layanan';
        foreach($layanan as $row){
            $layanans[$row->layanan] = $row->layanan;
        }
        $data['lini'] = $linis;
        $data['layanan'] = $layanans;
        $this->load->helper('form');
 

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_level_rule', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json()
	{
		if(!$this->input->is_ajax_request()) return false;

		$columns = array(
			array( 'db' => 'rule_month', 'dt' => 0 ),
			array( 'db' => 'rule_year', 'dt' => 1 ),
			array( 'db' => 'rule_model', 'dt' => 2 ),
			array( 'db' => 'rule_lini', 'dt' =>3 ),
			array( 'db' => 'rule_layanan', 'dt' =>4 ), 
			array( 'db' => 'rule_value', 'dt' =>5 ),
			array( 'db' => 'rule_freq', 'dt' =>6 )
		);
		
		$this->datatables->set_cols($columns);
        $param	 = $this->datatables->query(); 
        
		$periode = $this->input->post('periode');   
		$lini 	 = $this->input->post('lini');  
        $area    = $this->input->post('area'); 
        $channel = $this->input->post('channel'); 
        $layanan = $this->input->post('layanan'); 
        $area    = str_replace("_"," ",$area);
        $channel = str_replace("_"," ",$channel);
        $layanan = str_replace("_"," ",$layanan); 
        $lini    = str_replace("_"," ",$lini);
         

        $result = $this->dm->dtquery($param)->result();
		$filter = $this->dm->dtfiltered();
		$total	= $this->dm->dtcount();
		$output = $this->datatables->output($total, $filter);  

        $data['monthOption'] = array();
        $data['monthOption']['1'] = 'Januari';
        $data['monthOption']['2'] = 'Februari';
        $data['monthOption']['3'] = 'Maret';
        $data['monthOption']['4'] = 'April';
        $data['monthOption']['5'] = 'Mei';
        $data['monthOption']['6'] = 'Juni';
        $data['monthOption']['7'] = 'Juli';
        $data['monthOption']['8'] = 'Agustus';
        $data['monthOption']['9'] = 'September';
        $data['monthOption']['10'] = 'Oktober';
        $data['monthOption']['11'] = 'November';
        $data['monthOption']['12'] = 'Desember';

        foreach($result as $row)
		{
		    $b = (int) $row->rule_month;
            if(!empty($data['monthOption'][$b]))
                $bln = strtoupper($data['monthOption'][$b]);
            else
                $bln = '';

            $rows = array ( 
                $bln,
                $row->rule_year,
                ucwords(str_replace("_"," ",$row->rule_model)),
				strtoupper($row->rule_lini),
                strtoupper($row->rule_layanan), 
                strtoupper($row->rule_value),
                strtoupper($row->rule_freq),
                '<a href="javascript:edit('.$row->rule_month.','.$row->rule_year.',\''.$row->rule_lini.'\',\''.$row->rule_layanan.'\',\''.$row->rule_model.'\')" title="Edit Data" class="btn btn-xs btn-icon btn-primary"><i class="fa fa-edit"></i></a>'
			);
			
			$output['data'][] = $rows;
		} 
		
		echo json_encode( $output );
    }
    public function insert_inp()
    { 
        if(!$this->input->is_ajax_request()) return false;
        
        $value = $this->input->post('value');
        $freq = $this->input->post('freq');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $year = $this->input->post('year');
        $bisnis_model = $this->input->post('bisnis_model');

        if(!$this->dm->getbyyearmodel($year,$bisnis_model)->row()){
            $inp['rule_year'] =  $this->input->post('year');
            $inp['rule_model'] =  $this->input->post('bisnis_model');
            for($i=1;$i<=12;$i++){
                $inp['rule_month'] =  $i;
                foreach($value as $key => $val){
                    $inp['rule_lini'] = $lini[$key];
                    $inp['rule_layanan'] = $layanan[$key];
                    $inp['rule_value'] = $val;
                    $inp['rule_freq'] = $freq[$key];
                    $this->dm->add($inp);
                }
            }
            echo json_encode(array('status' => 'ok;', 'text' => ''));
        }
        else echo json_encode(array('status' => 'error;', 'text' => 'Data Bisnis Model di tahun yang dipilih sudah ada'));
    }

    public function edit_inp()
	{ 
        if(!$this->input->is_ajax_request()) return false;
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan'); 
        $models = $this->input->post('models'); 
		echo json_encode($this->dm->getbyall($month,$year,$lini,$layanan,$models)->row());
    }
    
    
    public function update_inp()
    { 
        if(!$this->input->is_ajax_request()) return false;
       
        
        $inp = $this->input->post('inp'); 
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $model = $this->input->post('model'); 

        if($this->dm->edit($month,$year,$lini,$layanan,$model,$inp)){ 
            echo json_encode(array('status' => 'ok;', 'text' => ''));
        }
        else echo json_encode(array('status' => 'error;', 'text' => 'Gagal update data'));
        
    }
      
    public function detail($id='')
    {
        if(empty($id))
            redirect('crm_b2b');
 
        $year   = date('Y');
        // $year   = '2018';
        $month  = date('n');
        
        $data['detail'] = $this->dm->b2b_detail($id);
        //$data['level'] = $this->dm->b2b_level($id)->result();

        //==================== GRAFIK LINI ========================================
        $lini_dbs = $this->db->query("SELECT cct_lini, cct_month, sum(rev) as jml FROM crm_b2b_cust_trans WHERE cct_cust_code='$id' and cct_year='$year' GROUP BY cct_lini, cct_month")->result();

        $lini = array();
        foreach($lini_dbs as $ld) {
            $lini[$ld->cct_lini][(int) $ld->cct_month] = $ld->jml;
        }

        $lini_array = array();
        if(!empty($lini)) {
            foreach($lini as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($lini[$name][$m]))
                        $month[] = (int) $lini[$name][$m];
                    else
                        $month[] = 0;
                }

                $lini_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['lini'] = $lini_array;
        //===========================================================================

        //==================== GRAFIK Produk ========================================
        /*$prod_dbs = $this->db->query("SELECT year, month, sum(rev) as jml FROM crm_b2b_cust_total_sku WHERE customer_code = '$id' and year = '$year' GROUP BY year, month")->result();

        $prod = array();
        foreach($prod_dbs as $ld) {
            $prod[$ld->cct_year][$ld->cct_month] = $ld->jml;
        }

        $prod_array = array();
        if(!empty($prod)) {
            foreach($prod as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($prod[$name][$m]))
                        $month[] = (int) $prod[$name][$m];
                    else
                        $month[] = 0;
                }

                $prod_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['prod'] = $prod_array;*/
        //========================================================================

        //============== Prediksi tipe konsumen ==================================
        $temp  = $this->db->query(" SELECT * FROM crm_b2b_cust_prediction WHERE cct_cust_code = '$id' and cct_year = '$year'")->result();
        $month = date('m');

        $level['regular'] = '1';
        $level['bronze'] = '3';
        $level['silver'] = '5';
        $level['gold'] = '8';

        $templini = array();
        foreach($temp as $tmp) {
            if($tmp->cct_month <= $month) {
                $templini[$tmp->cct_lini][$tmp->cct_month] = strtolower($tmp->level);
            } else {
                $templini[$tmp->cct_lini][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }

        $pred_array = array();
        if(!empty($templini)) {
            foreach($templini as $name => $value) {
                $monthx = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($templini[$name][$m]))
                        $monthx[] = !empty($level[$templini[$name][$m]]) ? (int) $level[$templini[$name][$m]] : (int) 1;
                    else
                        $monthx[] = (int) 1;
                }

                $pred_array[] = array('name' => $name, 'data' => $monthx);
            }
        }
        $data['prediksi'] = $pred_array;
        $data['level_point'] = $month-1;

        //============= LINI =========================
        $data['lini_level'] = $this->db->query("SELECT distinct cct_lini, level FROM crm_b2b_cust_lini_level WHERE cct_cust_code = '$id'")->result();

        $lini_rekomendasi = $this->db->query("SELECT * FROM crm_manual_input WHERE mi_model_business = 'B2B Relation' and month(mi_start_period) <= ".date('m')." and month(mi_end_period) >= ".date('m')." and year(mi_start_period) = ".date('Y'))->result();
        $data['lini_rec'] = array();
        foreach($lini_rekomendasi as $lr) {
            $data['lini_rec'][strtoupper($lr->mi_lini)][strtoupper($lr->mi_level)] = strtoupper($lr->mi_program);
        }
        //============================================

        $rev_sku = $this->db->query("SELECT month cct_month,year,total_sku,rev FROM crm_b2b_cust_total_sku WHERE customer_code = '$id' and year = '".date('Y')."'")->result();
        $revsku = array();
        foreach($rev_sku as $rs) {
            $revsku[$rs->cct_month] = array('rev' => $rs->rev, 'sku' => $rs->total_sku);
        }
        $data['bulan_rev'] = array();
        $data['bulan_sku'] = array();
        for($m=1; $m<=12; $m++) {
            $data['bulan_rev'][] = !empty($revsku[$m]['rev']) ? (int) $revsku[$m]['rev'] : 0;
            $data['bulan_sku'][] = !empty($revsku[$m]['sku']) ? (int) $revsku[$m]['sku'] : 0;
        }

        /*for($i=0;$i<=11;$i++){
            $lvl[$i] = 0;
        }

        foreach($temp as $row) {
            $ts1 = (int) $row->ccl_month-1;
            if($row->ccl_month<=$month){
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_level]) ? $level[$row->ccl_level] : '0' ;
            }
            else {
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_level_prediction]) ? $level[$row->ccl_level_prediction] : '0' ;
            }
        }

        $lvlx = array();
        foreach($lvl as $key => $value)
            $lvlx[$key+1] = $value;

        $data['level_point'] = $month-1;
        $data['level'] = json_encode($lvl);*/
        //================================================================

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/b2b_detail';
        $data['title']	= 'B2B Relation';
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_detail', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json_transaction()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_brand', 'dt' => 1 ),
			array( 'db' => 'cct_product', 'dt' => 2 ),
			array( 'db' => 'bcc_cabang', 'dt' => 3 ),
			array( 'db' => 'rev', 'dt' => 4 ) ,
			array( 'db' => 'qty', 'dt' => 5 ) ,
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (customer_code='".$id."' )";
        else $param['where'] .= " AND (customer_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_trans($param)->result();
		$filter = $this->dm->dtfiltered_detail_trans();
		$total	= $this->dm->dtcount_detail_trans();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				date('d/m/Y', strtotime($row->cct_faktur_date)),
				$row->cct_brand, 
				$row->cct_product, 
				$row->bcc_cabang,
                number_format($row->rev,0,",","."),
                $row->qty
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_reward()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_level', 'dt' => 1 ),
			array( 'db' => 'bcc_cabang', 'dt' => 2 ),
			array( 'db' => 'cct_total_reward', 'dt' => 3 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (cct_cust_code='".$id."' )";
        else $param['where'] .= "AND (cct_cust_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_trans($param)->result();
		$filter = $this->dm->dtfiltered_detail_trans();
		$total	= $this->dm->dtcount_detail_trans();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				$row->cct_faktur_date, 
				$row->cct_level, 
				$row->bcc_cabang, 
				$row->cct_total_reward
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_return()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_brand', 'dt' => 1 ),
			array( 'db' => 'cct_product', 'dt' => 2 ),
			array( 'db' => 'qty', 'dt' => 3 ) ,
			array( 'db' => 'bcc_cabang', 'dt' => 4 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (cct_cust_code='".$id."' )";
        else $param['where'] .= " AND (cct_cust_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_retur($param)->result();
		$filter = $this->dm->dtfiltered_detail_retur();
		$total	= $this->dm->dtcount_detail_retur();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
                date('d/m/Y', strtotime($row->cct_faktur_date)),
				$row->cct_brand, 
				$row->cct_product, 
				$row->qty,
                $row->bcc_cabang
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
	}
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
}

?>