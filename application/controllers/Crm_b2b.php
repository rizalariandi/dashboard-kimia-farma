<?php defined('BASEPATH') or exit('No direct script access allowed');

class Crm_b2b extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('Crm_b2b_model', 'dm');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
        // ini_set('memory_limit',-1);
    }

    public function index($start = '', $end = '', $lini = 'all', $lini_detail = 'all', $channel = 'all', $area = 'all', $layanan = 'all', $layanan_detail = 'all')
    {
        //ini_set('max_execution_time', 3600);
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['title']    = 'B2B Relation';

        //---------------------
        if (empty($start) and empty($end)) {
            // $start = '1-'.date('Y'); // initial filter from btp
            // $end = '12-'.date('Y'); // initial filter from btp

            $current_month = date('n');
            $three_monthsago = date('n', strtotime('-2 months'));

            $start = $three_monthsago . '-' . date('Y');
            $end = $current_month . '-' . date('Y');

            if($current_month == '1' || $current_month == '2'){
                $start = '1-' . date('Y');
            }
            // var_dump($start,$end);
            // die();
        } else if (empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-' . $y[1];
        } else if (!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-' . $y[1];
        }

        $data['var_start'] = $start;
        $data['var_end'] = $end;
        $data['var_lini'] = $lini;
        $data['var_layanan'] = $layanan;
        $data['var_layanan_detail'] = $layanan_detail;
        $data['lini_detail'] = $lini_detail;


        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1] . $s[0];
        $data['endx'] = $e[1] . $e[0];
        $data['start3'] = $blnx[$s[0]] . ' ' . $s[1];
        $data['end3'] = $blnx[$e[0]] . ' ' . $e[1];
        //---------------------

        //$data['year']    = $year;
        //$data['month']   = $month;
        $data['lini']    = $lini;
        // $data['brand']    = $brand;
        $data['area']    = $area;
        $data['channel'] = $channel;
        $data['layanan'] = $layanan;
        // $data['yearOption']  = $temp;
        $layanan             = str_replace("_", " ", $layanan);
        $channel             = str_replace("_", " ", $channel);
        $area                = str_replace("_", " ", $area);
        $lini                = str_replace("_", " ", $lini);
        // $brand                = str_replace("_"," ",$brand);
        $layanan_detail      = str_replace("_", " ", $layanan_detail);
        $lini_detail      = str_replace("_", " ", $lini_detail);

        // filter lini
        $dts     = $this->dm->getLini()->result();
        // var_dump($this->db->last_query());
        // die;

        $data['liniOption'] = array();
        foreach ($dts as $row) {
            $data['liniOption'][str_replace(" ", "_", $row->cct_lini)] = $row->cct_lini;
        }

        // penambahan brand
        // $dts     = $this->dm->getBrand()->result();
        // $data['brandOption'] = array();
        // foreach($dts as $row){
        //     $data['brandOption'][str_replace(" ","_",$row->brand)] = $row->brand;
        // }
        // penambahan brand

        //--- options ---
        $dts     = $this->dm->getChannel()->result();
        // var_dump($this->db->last_query());
        // die;

        $data['channelOption'] = array();
        foreach ($dts as $row) {
            $data['channelOption'][str_replace(" ", "_", $row->bcc_type_channel)] = $row->bcc_type_channel;
        }

        $dts     = $this->dm->getLayanan()->result();
        // var_dump($this->db->last_query());

        $data['layananOption'] = array();
        foreach ($dts as $row) {
            $data['layananOption'][str_replace(" ", "_", $row->cct_layanan)] = $row->cct_layanan;
        }

        $dts     = $this->dm->getArea()->result();
        // var_dump($this->db->last_query());

        $data['areaOption'] = array();
        foreach ($dts as $row) {
            $data['areaOption'][str_replace(" ", "_", $row->bcc_cabang)] = $row->bcc_cabang;
        }
        //--- options ---

        //scatter
        $where = "where rev > 0 and bcc_cust_name != '' and (cast(cct_month AS UNSIGNED INTEGER) BETWEEN '$s[0]' and '$e[0]') and cct_year = '$s[1]'";

        if (!empty($lini) and $lini != "all") $where .= " and lower(cct_lini)='" . strtolower($lini) . "'";
        //
        // if(!empty($brand) and $brand!="all") $where.=" and lower(brand)='".strtolower($brand)."'";
        //


        if (!empty($area) and $area != "all") $where .= " and bcc_cabang='$area'";
        if (!empty($channel) and $channel != "all") $where .= " and bcc_type_channel='$channel'";
        if (!empty($layanan) and $layanan != "all") $where .= " and cct_layanan='$layanan'";
        if (!empty($layanan_detail) and $layanan_detail != "all") $where .= " and layanan_name='$layanan_detail'";
        if (!empty($lini_detail) and $lini_detail != "all") $where .= " and brand='$lini_detail'";

        //$data['max_value'] = $max_value = $level_rule->value*2;
        //$data['max_freq'] = $max_freq = $level_rule->freq*2;

        //$data['median_value'] = $level_rule->value;
        //$data['median_freq'] = $level_rule->freq;

        $rule = $this->dm->get_level_rule($lini,  $layanan, $s[0], $e[0], $s[1], 'b2b');
        // var_dump($this->db->last_query());
        // die;

        $dts  = $this->dm->getLevelChart($where)->result();
        // die($this->db->last_query());

        $type = array();
        $c = array();
        foreach ($dts as $r) {
            /*if($r->rev < 0)
                $rev = 0;
            else if($r->rev > $max_value)
                $rev = $max_value;
            else*/
            $rev = (int) $r->rev;

            /*if($r->freq < 0)
                $freq = 0;
            else if($r->freq > $max_freq)
                $freq = $max_freq;
            else*/
            $freq = (int) $r->freq;

            $level = y_level_rule_range($r->rev, $r->freq, $rule);

            if (empty($c[strtolower($level)]))
                $c[strtolower($level)] = 1;
            else
                $c[strtolower($level)]++;

            $count = $c[strtolower($level)];

            if ($count <= 200) {
                $type['all'][strtolower($level)][] = array(
                    'name' => !empty($r->bcc_cust_name) ? $r->bcc_cust_name : 'null',
                    'x' => (int) $freq,
                    'y' => (int) $rev,
                    'label' => y_num_pad($r->rev)
                );
            }
        }
        $data['type'] = $type; //print_r($data['type']);


        $data['pie'] = array(
            array(
                'name' => 'Gold',
                'color' => y_cl('gold'),
                'y' => !empty($c['gold']) ? (int) $c['gold'] : 0
            ),
            array(
                'name' => 'Bronze',
                'color' => y_cl('bronze'),
                'y' => !empty($c['bronze']) ? (int) $c['bronze'] : 0
            ),
            array(
                'name' => 'Silver',
                'color' => y_cl('silver'),
                'y' => !empty($c['silver']) ? (int) $c['silver'] : 0
            ),
            array(
                'name' => 'Regular',
                'color' => y_cl('regular'),
                'y' => !empty($c['regular']) ? (int) $c['regular'] : 0
            )
        );
        // //=======================================================

        /*$pies = $this->dm->getLevelPie($where)->result();
        $pie = array();
        foreach($pies as $p)
            $pie[strtolower($p->level)] = $p->jml;

        $data['pie'] = array(
            array(
                'name' => 'Gold',
                'color' => y_cl('gold'),
                'y' => !empty($pie['gold']) ? (int) $pie['gold'] : 0
            ),
            array(
                'name' => 'Bronze',
                'color' => y_cl('bronze'),
                'y' => !empty($pie['bronze']) ? (int) $pie['bronze'] : 0
            ),
            array(
                'name' => 'Silver',
                'color' => y_cl('silver'),
                'y' => !empty($pie['silver']) ? (int) $pie['silver'] : 0
            ),
            array(
                'name' => 'Regular',
                'color' => y_cl('regular'),
                'y' => !empty($pie['regular']) ? (int) $pie['regular'] : 0
            )
        );*/

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json()
    {
        if (!$this->input->is_ajax_request()) return false;

        $columns = array(
            array('db' => 'bcc_cust_name', 'dt' => 0),
            array('db' => 'cct_cust_code', 'dt' => 1),
            array('db' => 'bcc_cabang', 'dt' => 2),
            array('db' => 'bcc_type_channel', 'dt' => 3),
            array('db' => 'layanan_name', 'dt' => 4),
            array('db' => 'rev', 'dt' => 5),
            array('db' => 'freq', 'dt' => 6),
            array('db' => 'revfreq', 'dt' => 7),
            array('db' => 'brand', 'dt' => 8),

        );

        $this->datatables->set_cols($columns);
        $param     = $this->datatables->query();

        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $lini      = $this->input->post('lini');
        $channel = $this->input->post('channel');
        $layanan = $this->input->post('layanan');
        $layanan_detail = $this->input->post('layanan_detail');
        $area    = $this->input->post('area');
        $lini_detail = $this->input->post('lini_detail');
        // $brand    = $this->input->post('brand');


        $url_var = $start . '/' . $end . '/' . $lini . '/' . $channel . '/' . $layanan;

        $area    = str_replace("_", " ", $area);
        $channel = str_replace("_", " ", $channel);
        $layanan = str_replace("_", " ", $layanan);
        $lini    = str_replace("_", " ", $lini);
        $layanan_detail = str_replace("_", " ", $layanan_detail);
        $lini_detail = str_replace("_", " ", $lini_detail);
        // $brand    = str_replace("_"," ",$brand);

        if (empty($param['where']))
            $param['where'] = "WHERE cct_year = '$s[1]' and (cast(cct_month AS UNSIGNED INTEGER) BETWEEN '$s[0]' and '$e[0]')";
        else
            $param['where'] .= " AND cct_year = '$s[1]' and (cast(cct_month AS UNSIGNED INTEGER) BETWEEN '$s[0]' and '$e[0]')";

        if ($lini != "all") {
            $param['where'] .= " AND (lower(cct_lini)='" . strtolower($lini) . "')";
        } 
        // else {
        //     $param['where'] .= " AND cct_lini != 'JANSSEN'";
        // }

        if ($lini_detail != "all") {
            $param['where'] .= " AND (lower(brand)='" . strtolower($lini_detail) . "')";
        }


        if ($area != "all") {
            $param['where'] .= " AND (lower(bcc_cabang)='" . strtolower($area) . "')";
        }

        if ($channel != "all") {
            $param['where'] .= " AND (lower(bcc_type_channel)='" . strtolower($channel) . "')";
        }

        if ($layanan != "all") {
            $param['where'] .= " AND (lower(cct_layanan)='" . strtolower($layanan) . "')";
        }

        if ($layanan_detail != "all") {
            $param['where'] .= " AND (lower(layanan_name)='" . strtolower($layanan_detail) . "')";
        }

        $rule = $this->dm->get_level_rule($lini, $layanan, $s[0], $e[0], $s[1], 'b2b');
        // var_dump($this->db->last_query());

        $result = $this->dm->dtquery($param)->result();
        // var_dump($this->db->last_query());
        // die;

        $total    = $this->dm->dtcount($param)->result()[0]->total;
        // var_dump($this->db->last_query());

        $filter = $total;
        $output = $this->datatables->output($total, $filter);
        // var_dump($this->db->last_query());

        /*$data['monthOption'] = array();
        $data['monthOption']['1'] = 'Januari';
        $data['monthOption']['2'] = 'Februari';
        $data['monthOption']['3'] = 'Maret';
        $data['monthOption']['4'] = 'April';
        $data['monthOption']['5'] = 'Mei';
        $data['monthOption']['6'] = 'Juni';
        $data['monthOption']['7'] = 'Juli';
        $data['monthOption']['8'] = 'Agustus';
        $data['monthOption']['9'] = 'September';
        $data['monthOption']['10'] = 'Oktober';
        $data['monthOption']['11'] = 'November';
        $data['monthOption']['12'] = 'Desember';*/

        foreach ($result as $row) {
            $level = y_level_rule_range($row->rev, $row->freq, $rule);

            $url = base_url() . 'index.php/crm_b2b/detail/' . $row->cct_cust_code . '/' . $url_var;

            $rows = array(
                '<a target="_blank" href="' . $url . '">' . $row->bcc_cust_name . '</a>',
                $row->cct_cust_code,
                strtoupper($row->bcc_cabang),
                strtoupper($row->bcc_type_channel),
                strtoupper($row->layanan_name),
                y_num_pad($row->rev),
                $row->freq,
                strtoupper($level),
                // $row->brand
                strtoupper($row->brand)
            );

            $output['data'][] = $rows;
        }

        echo json_encode($output);
    }

    public function detail($id = '', $start = '', $end = '', $lini = 'all', $channel = 'all', $layanan = 'all')
    {
        if (empty($id))
            redirect('crm_b2b');

        //---------------------
        if (empty($start) and empty($end)) {
            $start = '1-' . date('Y');
            $end = '12-' . date('Y');
        } else if (empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-' . $y[1];
        } else if (!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-' . $y[1];
        }

        $data['var_start'] = $start;
        $data['var_end'] = $end;

        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1] . $s[0];
        $data['endx'] = $e[1] . $e[0];
        $data['start3'] = $blnx[$s[0]] . ' ' . $s[1];
        $data['end3'] = $blnx[$e[0]] . ' ' . $e[1];
        //---------------------

        $data['lini']    = $lini;
        // $data['brand']    = $brand;

        $data['channel'] = $channel;
        $data['layanan'] = $layanan;

        $layanan   = str_replace("_", " ", $layanan);
        $channel   = str_replace("_", " ", $channel);
        $lini      = str_replace("_", " ", $lini);
        // $brand      = str_replace("_"," ",$brand);

        $data['detail'] = $this->dm->b2b_detail($id);
        // var_dump($this->db->last_query());
        // die;
        //$data['level'] = $this->dm->b2b_level($id)->result();

        //-------------- option ---------------------------------------------------
        $dts     = $this->dm->getLini()->result();
        // var_dump($this->db->last_query());
        // die;

        $data['liniOption'] = array();
        foreach ($dts as $row) {
            $data['liniOption'][str_replace(" ", "_", $row->cct_lini)] = $row->cct_lini;
        }

        // brand
        $dts     = $this->dm->getBrand()->result();
        // var_dump($this->db->last_query());

        $data['brandOption'] = array();
        foreach ($dts as $row) {
            $data['brandOption'][str_replace(" ", "_", $row->brand)] = $row->brand;
        }

        // brand
        $dts     = $this->dm->getChannel()->result();
        // var_dump($this->db->last_query());

        $data['channelOption'] = array();
        foreach ($dts as $row) {
            $data['channelOption'][str_replace(" ", "_", $row->bcc_type_channel)] = $row->bcc_type_channel;
        }

        $dts     = $this->dm->getLayanan()->result();
        // var_dump($this->db->last_query());

        $data['layananOption'] = array();
        foreach ($dts as $row) {
            $data['layananOption'][str_replace(" ", "_", $row->cct_layanan)] = $row->cct_layanan;
        }
        //-------- options ---


        $rule = $this->dm->get_level_rule($lini, $layanan, $s[0], $e[0], $s[1], 'b2b');
        // var_dump($this->db->last_query());
        // die;

        //==================== GRAFIK LINI ========================================
        $where = "WHERE cct_cust_code='$id' and cct_year='$s[1]' and (cast(cct_month AS UNSIGNED INTEGER) between '$s[0]' and '$e[0]')";
        if (!empty($lini) and $lini != "all") $where .= " and lower(cct_lini)='" . strtolower($lini) . "'";
        if (!empty($channel) and $channel != "all") $where .= " and bcc_type_channel='$channel'";
        if (!empty($layanan) and $layanan != "all") $where .= " and cct_layanan='$layanan'";

        $lini_dbs = $this->db->query("SELECT cct_lini, cct_month, sum(rev) as jml 
            FROM crm_b2b_cust_trans 
            $where
            GROUP BY cct_lini, cct_month")->result();
        // var_dump($this->db->last_query());
        // die;

        $linix = array();
        foreach ($lini_dbs as $ld) {
            $linix[$ld->cct_lini][(int) $ld->cct_month] = $ld->jml;
        }

        $lini_array = array();
        if (!empty($linix)) {
            foreach ($linix as $name => $value) {
                $month = array();
                for ($m = 1; $m <= 12; $m++) {
                    if (!empty($linix[$name][$m]))
                        $month[] = (int) $linix[$name][$m];
                    else
                        $month[] = 0;
                }

                $lini_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['graph_lini'] = $lini_array;
        //===========================================================================

        //============== Prediksi tipe konsumen =====================================
        $prediksi = $this->dm->get_prediction($id, $s[1], $s[0], $e[0])->result(); //print_r($prediksi);
        // var_dump($this->db->last_query());
        // die;

        $month = date('m');
        $monthx = array();
        $data['year'] = $s[1];

        $levela['regular'] = '1';
        $levela['bronze'] = '3';
        $levela['silver'] = '5';
        $levela['gold'] = '8';

        if ($s[1] < date('Y')) {
            $temp = array();
            if (!empty($prediksi)) {
                foreach ($prediksi as $tmp) {
                    $level = y_level_rule_range($tmp->rev, $tmp->freq, $rule);

                    $temp[$tmp->cct_month] = strtolower($level);
                }
            }

            $pred_array = array();
            for ($m = 1; $m <= 12; $m++) {
                if (!empty($temp[$m]))
                    $monthx[] = !empty($levela[$temp[$m]]) ? (int) $levela[$temp[$m]] : (int) 1;
                else
                    $monthx[] = (int) 1;
            }

            $pred_array[] = array('name' => 'Level', 'data' => $monthx);
            $data['prediksi'] = $pred_array;
            $data['level_point'] = 12;
        } else {
            $temp = array();
            $lr = $lf = 1;
            if (!empty($prediksi)) {
                foreach ($prediksi as $tmp) {
                    $level = y_level_rule_range($tmp->rev, $tmp->freq, $rule);

                    $lr = $tmp->rev;
                    $lf = $tmp->freq;

                    $temp[$tmp->cct_month] = strtolower($level);
                }
            }

            for ($m = 1; $m <= 12; $m++) {
                if ($m <= $month) {
                    if (!empty($temp[$m]))
                        $monthx[] = !empty($levela[$temp[$m]]) ? (int)$levela[$temp[$m]] : (int)1;
                    else
                        $monthx[] = (int)1;
                } else {
                    // RUMUS AWAL
                    // $var_rev = $lr + ($lr * $m * 0.1);
                    // $var_freq = $lf + ($lf * $m * 0.1);
                    // RUMUS AWAL

                    $var_rev = ($lr + ($lr * 0.1));
                    $var_freq = ($lf + ($lf * 0.1));                    
                    $level = y_level_rule_range($var_rev, $var_freq, $rule);
                    $level = strtolower($level);
                    $lr = $var_rev;
                    $lf = $var_freq;
                    $monthx[] = !empty($levela[$level]) ? (int)$levela[$level] : (int)1;
                }
            }

            $pred_array[] = array('name' => 'Level', 'data' => $monthx);
            $data['prediksi'] = $pred_array;
            $data['level_point'] = $month - 1;
        }

        /*$$lrev = $lfreq = 0;
        foreach($prediksi as $tmp) {
            //$linic = str_replace(' ', '_', strtolower($tmp->cct_lini));
            if($tmp->cct_month <= $month) {
                //$level = y_level_rule_range($tmp->revenue, $tmp->freq, $rule); //print_r($tmp); print '<br>'; print_r($level_rule); print $level.'<br><br>';

                //$templini[$linic][$tmp->cct_month] = strtolower($level);
            } else {
                //$templini[$linic][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }



        templini = array();
        foreach($prediksi as $tmp) {
            $linic = str_replace(' ', '_', strtolower($tmp->cct_lini));
            if($tmp->cct_month <= $month) {
                $level = y_level_rule_range($tmp->revenue, $tmp->freq, $rule); //print_r($tmp); print '<br>'; print_r($level_rule); print $level.'<br><br>';

                $templini[$linic][$tmp->cct_month] = strtolower($level);
            } else {
                $templini[$linic][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }
        //print_r($templini);

        $data['level_point'] = $month-1;*/
        //=========================================================================================================
        /*$where = "WHERE cct_cust_code='$id' and cct_year='$s[1]' and (cct_month between '$s[0]' and '$e[0]')";
        if(!empty($lini) and $lini!="all") $where.=" and lower(cct_lini)='".strtolower($lini)."'";
        //if(!empty($channel) and $channel!="all") $where.=" and bcc_type_channel='$channel'";
        // if(!empty($brand) and $brand!="all") $where.=" and brand='$brand'";
        if(!empty($layanan) and $layanan!="all") $where.=" and cct_layanan='$layanan'";

        //$temp  = $this->db->query(" SELECT * FROM crm_b2b_cust_prediction $where")->result();


        $prediksi = $this->dm->get_prediction($id, $s[1])->result();
        $month = date('Ym');

        $levela['regular'] = '1';
        $levela['bronze'] = '3';
        $levela['silver'] = '5';
        $levela['gold'] = '8';

        if($s[1] < date('Y'))
        {
            $temp = array();
            foreach($prediksi as $tmp) {
                $level = y_level_rule_range($tmp->revenue, $tmp->freq, $rule)

                //$templini[$linic][$tmp->cct_month] = strtolower($level);
            }
        }

        $lrev = $lfreq = 0;
        foreach($prediksi as $tmp) {
            //$linic = str_replace(' ', '_', strtolower($tmp->cct_lini));
            if($tmp->cct_month <= $month) {
                //$level = y_level_rule_range($tmp->revenue, $tmp->freq, $rule); //print_r($tmp); print '<br>'; print_r($level_rule); print $level.'<br><br>';

                //$templini[$linic][$tmp->cct_month] = strtolower($level);
            } else {
                //$templini[$linic][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }

        $pred_array = array();
        if(!empty($prediksi)) {
            foreach($prediksi as $value) {
                $monthx = array();
                //$name = str_replace(' ', '_', strtolower($name));
                for($m=1; $m<=12; $m++) {
                    if(!empty($templini[$name][$m]))
                        $monthx[] = !empty($levela[$templini[$name][$m]]) ? (int) $levela[$templini[$name][$m]] : (int) 1;
                    else
                        $monthx[] = (int) 1;
                }

                $pred_array[] = array('name' => $name, 'data' => $monthx);
            }
        }
        $data['prediksi'] = $pred_array;

        /*$templini = array();
        foreach($prediksi as $tmp) {
            $linic = str_replace(' ', '_', strtolower($tmp->cct_lini));
            if($tmp->cct_month <= $month) {
                $level = y_level_rule_range($tmp->revenue, $tmp->freq, $rule); //print_r($tmp); print '<br>'; print_r($level_rule); print $level.'<br><br>';

                $templini[$linic][$tmp->cct_month] = strtolower($level);
            } else {
                $templini[$linic][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }
        //print_r($templini);

        $data['level_point'] = $month-1;*/

        //============= LINI =========================
        /*$where = "WHERE cct_cust_codex = '$id' and cct_year='$s[1]' and (cct_month between '$s[0]' and '$e[0]')";
        if(!empty($lini) and $lini!="all") $where.=" and lower(cct_lini)='".strtolower($lini)."'";

        $data['lini_level'] = $this->db->query("SELECT distinct cct_lini, level FROM crm_b2b_cust_lini_level $where")->result();*/

        $lini_rekomendasi = $this->db->query("SELECT * FROM crm_manual_input WHERE mi_model_business = 'B2B Relation' and month(mi_start_period) <= " . date('m') . " and month(mi_end_period) >= " . date('m') . " and year(mi_start_period) = " . date('Y'))->result();
        $data['lini_rec'] = array();
        foreach ($lini_rekomendasi as $lr) {
            $data['lini_rec'][strtoupper($lr->mi_lini)][strtoupper($lr->mi_level)] = strtoupper($lr->mi_program);
        }

        $where = "where rev > 0 and cct_cust_code = '$id' and 
            (cast(cct_month AS UNSIGNED INTEGER) BETWEEN '$s[0]' and '$e[0]') and cct_year = '$s[1]'";

        if (!empty($lini) and $lini != "all") $where .= " and lower(cct_lini)='" . strtolower($lini) . "'";
        if (!empty($channel) and $channel != "all") $where .= " and bcc_type_channel='$channel'";
        if (!empty($layanan) and $layanan != "all") $where .= " and cct_layanan='$layanan'";

        $dts  = $this->dm->getLevelChartLini($where)->result();
        // var_dump($this->db->last_query());

        $data['dts'] = $this->dm->getLevelChartLini($where)->result();
        // var_dump($this->db->last_query());
        // die;

        $data['lini_level'] = array();
        foreach ($dts as $r) {
            $level = y_level_rule_range($r->rev, $r->freq, $rule);

            $data['lini_level'][$r->cct_lini] = $level;
        }
        //============================================



        //=============== grafik PRODUK =======================================
        $where = "WHERE bcc_cust_name = '" . $data['detail']->bcc_cust_name . "' and cct_year='$s[1]' and (cast(cct_month AS UNSIGNED INTEGER) between '$s[0]' and '$e[0]')";
        if (!empty($lini) and $lini != "all") $where .= " and lower(cct_lini)='" . strtolower($lini) . "'";
        if (!empty($channel) and $channel != "all") $where .= " and bcc_type_channel='$channel'";
        if (!empty($layanan) and $layanan != "all") $where .= " and cct_layanan='$layanan'";

        // ======================= initial code ==========================
        // $rev_sku = $this->db->query("SELECT cct_month,cct_year,count(brand) total_sku,sum(rev) as rev_akhir
        //     FROM crm_b2b_cust_trans $where GROUP BY cct_month")->result();
        // ===============================================================

        $rev_sku = $this->db->query("SELECT /* SQL_NO_CACHE */ cct_month,cct_year,sum(total_sku) total_sku,sum(rev_akhir) rev_akhir
            FROM crm_b2b_grafik_produk $where GROUP BY cct_month")->result();

        // die($this->db->last_query());
        // $revsku = array();

        foreach ($rev_sku as $rs) {
            if ($rs->cct_month < 10) {
                $revsku[str_replace('0', '', $rs->cct_month)] = array('rev_akhir' => $rs->rev_akhir, 'sku' => $rs->total_sku);
            } else {
                $revsku[$rs->cct_month] = array('rev_akhir' => $rs->rev_akhir, 'sku' => $rs->total_sku);
            }
        }
        $data['bulan_rev'] = array();
        $data['bulan_sku'] = array();
        for ($m = 1; $m <= 12; $m++) {
            $data['bulan_rev'][] = !empty($revsku[$m]['rev_akhir']) ? (int) $revsku[$m]['rev_akhir'] : 0;
            $data['bulan_sku'][] = !empty($revsku[$m]['sku']) ? (int) $revsku[$m]['sku'] : 0;
        }
        //================================================================

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view']     = 'crm/b2b_detail';
        $data['title']    = 'B2B Relation';
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_detail', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json_transaction()
    {
        if (!$this->input->is_ajax_request()) return false;

        $columns = array(
            array('db' => 'cct_faktur_date', 'dt' => 0),
            array('db' => 'cct_brand', 'dt' => 1),
            array('db' => 'total_sku', 'dt' => 2),
            array('db' => 'bcc_cabang', 'dt' => 3),
            array('db' => 'rev', 'dt' => 4),
            array('db' => 'qty', 'dt' => 5),
        );

        $this->datatables->set_cols($columns);
        $param    = $this->datatables->query();

        $id      = $this->input->post('id');
        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $lini    = $this->input->post('lini');
        $layanan    = $this->input->post('layanan');
        $channel    = $this->input->post('channel');
        $lini_replace = str_replace('_', ' ', $lini);

        if (empty($param['where']))     $param['where'] = "WHERE (customer_code='" . $id . "' )";
        else $param['where'] .= "AND (customer_code='" . $id . "')";
        if ($start && $end) {
            $s[0] = strlen($s[0]) > 1 ? $s[0] : '0' . $s[0];
            $e[0] = strlen($e[0]) > 1 ? $e[0] : '0' . $e[0];
            $start = $s[1] . '-' . $s[0] . '-01';
            $end =  $e[1] . '-' . $e[0] . '-31';
            $param['where'] .= "AND (cct_faktur_date BETWEEN '$start' and '$end')";
        }
        if ($lini != 'all') {
            if (empty($param['where']))     $param['where'] = "WHERE (cct_lini='" . $lini_replace . "' )";
            else $param['where'] .= "AND (cct_lini='" . $lini_replace . "')";
        }
        if ($layanan != 'all') {
            if (empty($param['where']))     $param['where'] = "WHERE (cct_layanan='" . $layanan . "' )";
            else $param['where'] .= "AND (cct_layanan='" . $layanan . "')";
        }
        if ($channel != 'all') {
            if (empty($param['where']))     $param['where'] = "WHERE (bcc_type_channel='" . $channel . "' )";
            else $param['where'] .= "AND (bcc_type_channel='" . $channel . "')";
        }
        $result = $this->dm->dtquery_detail_trans($param)->result();
        // die($this->db->last_query());

        $filter = $this->dm->dtfiltered_detail_trans();
        // var_dump($this->db->last_query());

        $total    = $this->dm->dtcount_detail_trans();
        // var_dump($this->db->last_query());
        
        $output = $this->datatables->output($total, $filter);

        foreach ($result as $row) {
            $rows = array(
                date('d/m/Y', strtotime($row->cct_faktur_date)),
                $row->cct_brand,
                $row->total_sku,
                $row->bcc_cabang,
                number_format($row->rev, 0, ",", "."),
                $row->qty
            );

            $output['data'][] = $rows;
        }

        echo json_encode($output);
    }

    public function json_reward()
    {
        if (!$this->input->is_ajax_request()) return false;

        $columns = array(
            array('db' => 'cbr_date', 'dt' => 0),
            array('db' => 'cbr_product_name', 'dt' => 1),
            array('db' => 'cbr_material', 'dt' => 2),
            array('db' => 'cbr_level', 'dt' => 3),
            array('db' => 'cbr_area', 'dt' => 4),
            array('db' => 'cbr_reward', 'dt' => 5)
        );

        $this->datatables->set_cols($columns);
        $param    = $this->datatables->query();

        $id     = $this->input->post('id');
        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);
        if (empty($param['where']))     $param['where'] = "WHERE (cbr_customer_code='" . $id . "' )";
        else $param['where'] .= "AND (cbr_customer_code='" . $id . "')";
        if ($start && $end) {
            $s[0] = strlen($s[0]) > 1 ? $s[0] : '0' . $s[0];
            $e[0] = strlen($e[0]) > 1 ? $e[0] : '0' . $e[0];
            $start = $s[1] . '-' . $s[0] . '-01';
            $end =  $e[1] . '-' . $e[0] . '-31';
            $param['where'] .= "AND (cbr_date BETWEEN '$start' and '$end')";
        }

        $result = $this->dm->dtquery_detail_reward($param)->result();
        // die($this->db->last_query());

        $filter = $this->dm->dtfiltered_detail_reward();
        // var_dump($this->db->last_query());

        $total    = $this->dm->dtcount_detail_reward();
        // var_dump($this->db->last_query());

        $output = $this->datatables->output($total, $filter);

        foreach ($result as $row) {
            $rows = array(
                date('d/m/Y', strtotime($row->cbr_date)),
                strtoupper($row->cbr_product_name),
                strtoupper($row->cbr_material),
                strtoupper($row->cbr_level),
                strtoupper($row->cbr_area),
                strtoupper($row->cbr_reward)
            );

            $output['data'][] = $rows;
        }
        echo json_encode($output);
    }

    public function json_return()
    {
        if (!$this->input->is_ajax_request()) return false;

        $columns = array(
            array('db' => 'cct_faktur_date', 'dt' => 0),
            array('db' => 'cct_brand', 'dt' => 1),
            array('db' => 'cct_product', 'dt' => 2),
            array('db' => 'qty', 'dt' => 3),
            array('db' => 'bcc_cabang', 'dt' => 4)
        );

        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $this->datatables->set_cols($columns);
        $param    = $this->datatables->query();

        $id     = $this->input->post('id');

        if (empty($param['where']))     $param['where'] = "WHERE (cct_cust_code='" . $id . "' )";
        else $param['where'] .= " AND (cct_cust_code='" . $id . "')";
        if ($start && $end) {
            $s[0] = strlen($s[0]) > 1 ? $s[0] : '0' . $s[0];
            $e[0] = strlen($e[0]) > 1 ? $e[0] : '0' . $e[0];
            $start = $s[1] . '-' . $s[0] . '-01';
            $end =  $e[1] . '-' . $e[0] . '-31';
            $param['where'] .= "AND (cct_faktur_date BETWEEN '$start' and '$end')";
        }
        $result = $this->dm->dtquery_detail_retur($param)->result();
        // die($this->db->last_query());
        

        $filter = $this->dm->dtfiltered_detail_retur();
        // var_dump($this->db->last_query());

        $total    = $this->dm->dtcount_detail_retur();
        // var_dump($this->db->last_query());

        $output = $this->datatables->output($total, $filter);

        foreach ($result as $row) {
            $rows = array(
                date('d/m/Y', strtotime($row->cct_faktur_date)),
                $row->cct_brand,
                $row->cct_product,
                $row->qty,
                $row->bcc_cabang
            );

            $output['data'][] = $rows;
        }

        echo json_encode($output);
    }

    /**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
     **/

    public function layanan_detail()
    {
        $id  = $this->input->post('id');
        $dbs = $this->db->query("SELECT layanan_name FROM crm_b2b_cust_trans WHERE lower(cct_layanan) = '" . strtolower($id) . "' GROUP BY layanan_name")->result();
        $result = array();

        foreach ($dbs as $db) {
            $result[] = array('id' => str_replace(' ', '_', $db->layanan_name), 'text' => $db->layanan_name);
        }

        echo json_encode($result);
    }

    public function lini_detail()
    {
        $id  = str_replace("_", " ", $this->input->post('id', TRUE));
        $dbs = $this->db->query("SELECT brand FROM crm_b2b_cust_trans WHERE lower(cct_lini) = '" . strtolower($id) . "' GROUP BY brand")->result();
        // $dbs = $this->db->select('brand')
        //         ->like('cct_lini', $id)
        //         ->group_by('brand')->get('crm_b2b_cust_trans')
        //         ->result();
        $result = array();

        foreach ($dbs as $db) {
            $result[] = array('id' => str_replace(' ', '_', $db->brand), 'text' => $db->brand);
        }

        echo json_encode($result);
    }
}
