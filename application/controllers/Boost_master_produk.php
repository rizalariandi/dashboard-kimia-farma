<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_master_produk extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->model('Boost_master_produk_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
    }

    public function index(){
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Master Produk',
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/produk/master_produk', $data);
        $this->load->view('templates/footer', $data);
	}

	function product_data(){
		$data=$this->Boost_master_produk_model->product_list();
		echo json_encode($data);
	}

	function save(){
		$data=$this->Boost_master_produk_model->save_product();
		echo json_encode($data);
	}

	function update(){
		$data=$this->Boost_master_produk_model->update_product();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->Boost_master_produk_model->delete_product();
		echo json_encode($data);
	}
    
}