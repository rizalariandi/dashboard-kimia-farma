<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_brand_area extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->model('Boost_brand_area_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{ 
		$data["title"] = 'Brand Area';
		$data["list_lini"] = $this->Boost_brand_area_model->get_list_lini();
		$data["list_layanan"] = $this->Boost_brand_area_model->get_list_layanan();
		$data["list_produk"] = $this->Boost_brand_area_model->get_list_produk();
		$data["list_brand"] = $this->Boost_brand_area_model->get_list_brand();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["data_brand"] = $this->input->post("brand", true);
		$data["data_produk"] = $this->input->post("produk", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
			
		}

		if($data["provinsi"] != ""){
			$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
		}
		// die($this->db->last_query());
		

		if($data["data_range"] != ''){
			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		} else {
			// $data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			// $data['startDate'] = "2017/01/01";
			// $data['endDate'] = date("Y/m/d");

			// ============ code wrote on 3 October 2021 ============
			$current_month = date('Y-m-d');
            $three_monthsago = date('Y-m-d', strtotime('-3 months'));
            
			$data["data_range"] = $three_monthsago.' / '.$current_month;
			$data['startDate'] = date('Y/m/d', strtotime('-3 months'));
			$data['endDate'] = date('Y/m/d');
		
			// ======================================================
			// var_dump($data_range);
			// die();
		}
		
		if($data["areaf"] == "brand-area"){
			$data["kftd"] = $this->Boost_brand_area_model->get_kftd();
			// die($this->db->last_query());
		} else {
			$brand = $this->input->post("brand", true);
			$produk = $this->input->post("produk", true);
			if($brand != "" || $produk != ""){
				$data["kftd"] = $this->Boost_brand_area_model->get_kftd_brand2();
				// die($this->db->last_query());
			} else {
				$data["kftd"] = $this->Boost_brand_area_model->get_kftd_brand();
				// die($this->db->last_query());

			}
		}

		$data['mapdata'] = $this->populate_provinsi();
		
		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/sales/kontribusi/brand_area', $data);
		$this->load->view('templates/footer', $data);
	}

	public function populate_provinsi(){
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'N ACEH DARUSSALAM', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		); 
		
		// =================== initial code =====================
		// $total = 0;
		// foreach($array_kode_iso as $key=>$val){
		// 	$total += $this->Boost_brand_area_model->get_revenue(strtoupper($val['name']));
		// }
		// ======================================================

		// ===================== edited on 26-10-2021 - fareza ==========================
		$total = $this->Boost_brand_area_model->get_revenue_total();
		// ==============================================================================
		// die($this->db->last_query());

		$array_datas = array();
		foreach($array_kode_iso as $key=>$val){
			if ($total != 0 ){
				$value = round(($this->Boost_brand_area_model->get_revenue(strtoupper($val['name'])) / $total)*100, 2);
			} else {
				$value = 0;
			}
			
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=> $value ));
		}

		return json_encode($array_datas);
	}

	function get_brand($table, $lini) {
        $this->db->distinct()->select('nama_brand');
        if (is_array($lini)) {
            foreach ($lini as $l) {
                $this->db->or_like('lini', $l);
            }
        } else {
            $this->db->where('lini', $lini);
        } 
        
        return $this->db->order_by('nama_brand','asc')->get($table)->result();
	}
	
	public function json_brand_area(){
		header('Content-Type: application/json');
        echo $this->Boost_brand_area_model->json_brand_area();
		// die($this->Boost_brand_area_model->json_brand_area());
	}

	public function json_area_brand(){	
		header('Content-Type: application/json');
		$data = str_replace("MARCKS'", "MARCKS", $this->Boost_brand_area_model->json_area_brand());
		echo str_replace("I.N.H'", "INH", $data);
		// die($this->Boost_brand_area_model->json_area_brand());
	}

	public function json_get_brand(){
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$lini = $this->input->post("lini", true);
		$brand_by_lini = $this->Boost_brand_area_model->get_brand($table, $lini);
		foreach ($brand_by_lini as $list) {
			echo '<option value="' . $list->nama_brand . '">' . $list->nama_brand . "</option>";
		}
	}

	public function json_get_sku(){
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$brand = $this->input->post("brand", true);
		$sku = $this->Boost_brand_area_model->get_sku($table, $brand);
		foreach ($sku as $list) {
			echo '<option value="' . $list->nama_produk . '">' . $list->nama_produk . "</option>";
		}
	}

}