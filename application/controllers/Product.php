<?php
class Product extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('product_model');
	}
	function index(){
        $data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Master Produk',
		);
        $this->load->view('templates/header', $data);
        $this->load->view('boosting/master_produk', $data);
        $this->load->view('templates/footer', $data);
	}

	function product_data(){
		$data=$this->product_model->product_list();
		// var_dump($this->db->last_query());
		// die;
		echo json_encode($data);
	}

	function save(){
		$data=$this->product_model->save_product();
		echo json_encode($data);
	}

	function update(){
		$data=$this->product_model->update_product();
		echo json_encode($data);
	}

	function delete(){
		$data=$this->product_model->delete_product();
		echo json_encode($data);
	}

}