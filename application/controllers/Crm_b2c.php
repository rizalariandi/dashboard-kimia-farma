<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_b2c extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('crm_model');
        $this->load->model('Crm_b2b_model', 'b2b');
        $this->load->model('Crm_b2c_end_model', 'dm');
        //ini_set('memory_limit',-1);

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}

    public function index($month="",$year="", $produk='all')
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";
        $data['title']	= 'B2C End User Relation';

        if(empty($month))
            // $month = date('m');
            $month = date('n');

        if(empty($year))
            $year = date('Y');

        $data['year']    = $year;
        $data['month']   = $month;
        $data['produk']  = $produk;
        $produk          = str_replace("_"," ",$produk);

        $dts = $this->crm_model->getProduk()->result();


        $data['produkOption'] = array();
        foreach($dts as $row){
            $linkencode = urlencode(base64_encode(str_replace(" ","_",$row->produk)));
            $data['produkOption'][] = array(
                'opsi' => base64_decode(urldecode($linkencode)),
                'value' =>$linkencode
            );

        }

        //produk
        $where = "where month = '$month' and year = '$year'";
        $where2 = "where year = '$year'";
        if($produk != "all") {
            $where.=' and name_produk = "'.str_replace("_"," ",base64_decode(urldecode($produk))).'"';
            $where2.=' and name_produk = "'.str_replace("_"," ",base64_decode(urldecode($produk))).'"';
        }
 
        $prod_dbs = $this->dm->history($where2)->result();
        // die($this->db->last_query());

        $prod = array();
        foreach($prod_dbs as $ld) {
            if ($ld->month < 10){
                $prod[str_replace('0','',$ld->month)] = $ld->rev;
            }else{
                $prod[$ld->month] = $ld->rev;
            }
        }

        for($m=1; $m<=12; $m++) {
            if(!empty($prod[$m]))
                $monthx[] = (int) $prod[$m];
            else
                $monthx[] = 0;
        }
        $prod_array[] = array('name' => $year, 'data' => $monthx);

        $data['prod'] = $prod_array;
        //=======================================================

        $pies = $this->dm->getLevelPie($where)->result();
        // die($this->db->last_query());

        $pie = array();
        foreach($pies as $p){
            $pie[strtolower($p->level)] = $p->jml;
        }
        // var_dump($pie);
        // die;

        $data['pie'] = array(
            array(
                'name' => 'Gold',
                'color' => y_cl('gold'),
                'y' => !empty($pie['gold']) ? (int) $pie['gold'] : 0
            ),
            array(
                'name' => 'Bronze',
                'color' => y_cl('bronze'),
                'y' => !empty($pie['bronze']) ? (int) $pie['bronze'] : 0
            ),
            array(
                'name' => 'Silver',
                'color' => y_cl('silver'),
                'y' => !empty($pie['silver']) ? (int) $pie['silver'] : 0
            ),
            array(
                'name' => 'Reguler',
                'color' => y_cl('regular'),
                'y' => !empty($pie['regular']) ? (int) $pie['regular'] : 0
            )
        );

        //=============================================================================================================

        $gender = $this->dm->getGender($where)->result();
        // die($this->db->last_query());

        $jk = array();
        foreach($gender as $gen) {
            $jk[strtolower($gen->gender)] = (int) $gen->jml;
        }
        $data['gender']['L'] = !empty($jk['l']) ? $jk['l'] : 0;
        $data['gender']['P'] = !empty($jk['p']) ? $jk['p'] : 0;

        $age = $this->dm->getAge($where)->result();
        // die($this->db->last_query());

        $data['age'] = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0);
        foreach($age as $a) {
            $umur = $a->age;
            if($umur >= 0 and $umur <= 9)
                $data['age']['0'] += $a->jml;
            else if($umur >= 10 and $umur <= 20)
                $data['age']['1'] += $a->jml;
            else if($umur >= 21 and $umur <= 30)
                $data['age']['2'] += $a->jml;
            else if($umur >= 31 and $umur <= 50)
                $data['age']['3'] += $a->jml;
            else
                $data['age']['4'] += $a->jml;
        }

        //=============================================================================================================

        $heatmaps = $this->dm->getHeatmaps($where)->result();
        // die($this->db->last_query());

        $data['heatmap'] = array();

        if(!empty($heatmaps)) {
            foreach($heatmaps as $h) {
                $data['heatmap'][] = array(
                    'name' => strtoupper($h->provinsi),
                    'value' => 1,
                    'colorValue' => (int) $h->jml
                );
            }
        } else {
            $heatmaps = $this->dm->getAllProvinsi()->result();
            foreach($heatmaps as $h) {
                $data['heatmap'][] = array(
                    'name' => strtoupper($h->provinsi),
                    'value' => 1,
                    'colorValue' => 0
                );
            }
        }

        //print_r($data['heatmap']);

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2c_enduser', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json()
	{
        
		if(!$this->input->is_ajax_request()) return false;

		$columns = array(
			array( 'db' => 'customer', 'dt' => 0 ),
			array( 'db' => 'address', 'dt' => 1 ),
			array( 'db' => 'phone', 'dt' => 2 ),
			array( 'db' => 'month', 'dt' =>3 ),
			array( 'db' => 'level', 'dt' =>4 )
		); 
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();
  
		// $lini 	= $this->input->post('lini');  
        $produk = $this->input->post('produk'); 
        $periode = $this->input->post('periode');  
    
        $p = explode('/',$periode);
        $tgl = strlen($p[0]) == 2 && substr($p[0],0,1) == '0' ? substr($p[0],1,1) : $p[0];

        if(empty($param['where'])){
            // $param['where'] = "WHERE year='$p[1]' and month = '$p[0]'";
            $param['where'] = "WHERE year='$p[1]' and month = '$tgl'";
        }else{
            // $param['where'] .= "AND year='$p[1]' and month = '$p[0]'";
            $param['where'] .= "AND year='$p[1]' and month = '$tgl'";
        }

        $produkParse = str_replace("_"," ",base64_decode(urldecode($produk)));
		if ($produk != "all"){
            // ============== initial query =============
		    // $param['where'] .= "AND (lower(name_produk) LIKE '".strtolower($produkParse)."%')";
            // ==========================================

            $param['where'] .= " AND (lower(name_produk) = '".strtolower($produkParse)."')";
        }
		// if ($lini!="0"){ 
		// 	if(empty($param['where'])) 	$param['where'] = "WHERE (lower(lini)='".strtolower($lini)."')";
		// 	else $param['where'] .= "AND (lower(lini)='".strtolower($lini)."')"; 
		// }   
            
		  
		$result = $this->dm->dtquery($param)->result();
        // $tes = $this->db->last_query();

		// $total	= $this->dm->dtcount($param)->result()[0]->total;
        $total	= $this->dm->dtcount($param);
        $filter = $total;
		$output = $this->datatables->output($total, $filter);

        // $data['monthOption'] = array();
        // $data['monthOption']['01'] = 'Januari';
        // $data['monthOption']['02'] = 'Februari';
        // $data['monthOption']['03'] = 'Maret';
        // $data['monthOption']['04'] = 'April';
        // $data['monthOption']['05'] = 'Mei';
        // $data['monthOption']['06'] = 'Juni';
        // $data['monthOption']['07'] = 'Juli';
        // $data['monthOption']['08'] = 'Agustus';
        // $data['monthOption']['09'] = 'September';
        // $data['monthOption']['10'] = 'Oktober';
        // $data['monthOption']['11'] = 'November';
        // $data['monthOption']['12'] = 'Desember';

        $data['monthOption'] = array();
        $data['monthOption']['1'] = 'Januari';
        $data['monthOption']['2'] = 'Februari';
        $data['monthOption']['3'] = 'Maret';
        $data['monthOption']['4'] = 'April';
        $data['monthOption']['5'] = 'Mei';
        $data['monthOption']['6'] = 'Juni';
        $data['monthOption']['7'] = 'Juli';
        $data['monthOption']['8'] = 'Agustus';
        $data['monthOption']['9'] = 'September';
        $data['monthOption']['10'] = 'Oktober';
        $data['monthOption']['11'] = 'November';
        $data['monthOption']['12'] = 'Desember';

        $rule = $this->b2b->get_level_rule('all', 'all', $p[0], $p[0], $p[1], 'b2c_end_user');

        foreach($result as $row)
		{
            $level = y_level_rule_range($row->rev, $row->freq, $rule);

            $rows = array (
				'<a target="_blank" href="'.base_url().'index.php/crm_b2c/detail/'.urlencode(base64_encode($row->customer)).'/'.$row->month.'/'.$row->year.'">'.$row->customer.'</a>',
				strtoupper($row->address),
				$row->phone,
                strtoupper($data['monthOption'][$row->month]).' '.$row->year,
                y_num_pad($row->rev),
                $row->freq,
                strtoupper($level)
			);
			
			$output['data'][] = $rows;
		} 
		
		echo json_encode( $output );
    }

    public function detail($id='', $month='', $year='')
    {
        if(empty($id))
            redirect('crm_b2c');

        $id = base64_decode(urldecode($id));

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/b2c';
        $data['title']	= 'B2C End User Relation'; 

        if(empty($year))
            $year = date('Y');

        if(empty($month))
            $month  = date('m');

        $data['detail'] = $this->dm->getProfile($id)->row();
        // die($this->db->last_query());

        $rule = $this->b2b->get_level_rule('all', 'all', $month, $month, $year, 'b2c_end_user');
        $level_trans = $this->dm->detail_trans($id, $year)->result();
        $data['level'] = array();
        //$data['level_rule'] = $rule;
        foreach($level_trans as $lt) {
            $data['level'][$lt->month] = y_level_rule_range($lt->rev, $lt->freq, $rule);
        }

        //================ REC ==========================
        $lini_rekomendasi = $this->db->query("SELECT * FROM crm_manual_input WHERE mi_model_business = 'B2C End User Relation' and month(mi_start_period) <= ".date('m')." and month(mi_end_period) >= ".date('m')." and year(mi_start_period) = ".date('Y'))->result();
        // die($this->db->last_query());

        $data['lini_rec'] = array();
        foreach($lini_rekomendasi as $lr) {
            $data['lini_rec'][strtoupper($lr->mi_level)] = strtoupper($lr->mi_program);
        }
        // var_dump($level_trans);
        // die;
        //===============================================

        //============== Prediksi Level ==================================
        $temp  = $this->db->query("SELECT level, month, prediction_1  from crm_b2c_end_prediction where customer='$id' and year='$year'")->result();
        // die($this->db->last_query());
        
        $month = date('m');

        $level['reguler'] = '1';
        $level['bronze'] = '3';
        $level['silver'] = '5';
        $level['gold'] = '8';

        $templini = array();
        foreach($temp as $tmp) {
            if($tmp->month <= $month) {
                $templini[$tmp->month] = strtolower($tmp->level);
            } else {
                $templini[$tmp->month] = strtolower($tmp->prediction_1);
            }
        }

        $pred_array = array();
        if(!empty($templini)) {
            foreach($templini as $name => $value) {
                $monthx = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($templini[$m]))
                        $monthx[] = !empty($level[$templini[$m]]) ? (int) $level[$templini[$m]] : (int) 1;
                    else
                        $monthx[] = (int) 1;
                }

                $pred_array[] = array('name' => $year, 'data' => $monthx);
            }
        }
        $data['prediksi'] = $pred_array;
        $data['level_point'] = $month-1;
        //================================================================
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2c_enduser_detail', $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function json_detail()
	{
		if(!$this->input->is_ajax_request()) return false;
		//name_produk
		$columns = array(
            array( 'db' => 'tanggal', 'dt' => 0 ),
			array( 'db' => 'name_produk', 'dt' => 1 ),
			array( 'db' => 'provinsi', 'dt' => 2 ),
			array( 'db' => 'payment_to_sentence', 'dt' => 3 ),
			array( 'db' => 'rev', 'dt' => 4 ),
			array( 'db' => 'freq', 'dt' => 5 )
		);
		 
        $id     = $this->input->post('id');  
        $year     = $this->input->post('year');  
        $month     = $this->input->post('month');

		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		  
        
        // var_dump($param['where']);
        // die; 

        if(empty($param['where'])) 
        {
            // ========== initial code =============
            // $param['where'] = "WHERE (customer='".$id."' )";
            // $param['where'] .= " AND (year='".$year."' )";
            // $param['where'] .= " AND (month='".$month."' )";
            // =====================================

            $tmp = array();
            if (!empty($id)){
                array_push($tmp,"(customer='".$id."')");
            }
            if (!empty($year)){
                array_push($tmp,"(year='".$year."')");
            }
            if (!empty($month)){
                array_push($tmp,"(month='".$month."')");
            }

            $param['where'] = "WHERE ".implode(' AND ', $tmp);
        }else {
            $param['where'] .= "AND (customer='".$id."')"; 
        }          
        
		$result = $this->dm->dtquery_detail($param)->result();
        // die($this->db->last_query());

		$filter = $this->dm->dtfiltered_detail($param);
		$total	= $this->dm->dtcount_detail();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{
       
			$rows = array (
                date('d/m/Y', strtotime($row->tanggal)),
                $row->name_produk,
				strtoupper($row->provinsi),
                strtoupper($row->payment_to_sentence),
				'Rp. '.number_format($row->rev,0,",","."),
				$row->freq
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_reward()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'created_at', 'dt' => 0 ),
			array( 'db' => 'level', 'dt' => 1 ),
			array( 'db' => 'provinsi', 'dt' => 2 ),
			array( 'db' => 'reward', 'dt' => 3 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (customer='".$id."' )";
        else $param['where'] .= "AND (customer='".$id."')";  
            
		$result = $this->crm_model->dtquery_detail($param)->result();
		$filter = $this->crm_model->dtfiltered_detail();
		$total	= $this->crm_model->dtcount_detail();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				$row->created_at, 
				$row->level, 
				$row->provinsi, 
				$row->reward
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
}

?>