<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_matrix_kfa extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boost_model');
		$this->load->model('Boost_matrix_kfa_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}

	public function index()
   	{
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$kompetitor = $this->input->post('kompetitor');
        $filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

        if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		
		if(empty($produk)){
			$produk[] = 'FITUNO';
			// var_dump($produk);
			// die;
		}

		if(empty($kompetitor)){
			$kompetitor[] = 'MAGASIDA';
		}else{
			// var_dump($kompetitor);
			// die;
		}

		if(empty($filter)){
			$filter = 'value';
        }
        $check = $this->Boost_matrix_kfa_model->check_data('usc_bst_product_mart', $area, $produk, $from, $to);
        // $check_kompetitor = $this->Boost_matrix_kfa_model->check_data_kompetitor('usc_bst_product_mart', $area, $kompetitor, $from, $to);
		// $checkbaru = $this->Boost_matrix_kfa_model->check_data_baru('usc_bst_product_mart', $area, $produk, $from, $to);
	
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}

        $bcg = $this->Boost_matrix_kfa_model->get_bcg_matrix_kompetitor('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		$bcg_rekomendasi = $this->Boost_matrix_kfa_model->get_bcg_rekomendasi_kompetitor('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		$bcg_detils = $this->Boost_matrix_kfa_model->get_bcg_matrix_detils_kompetitor('usc_bst_product_details_mart', $area, $produk, $kompetitor, $filter, $from, $to);
		$sell_out = $this->Boost_matrix_kfa_model->sell_out_kfa_kompetitor('usc_bst_product_mart_new', $area, $produk,$kompetitor, $filter, $from, $to);

        $data_raw = $this->Boost_matrix_kfa_model->data_sell_out('usc_bst_product_mart_new', $area, $produk, $filter, $from, $to);
		$chart_sell_out = json_decode($sell_out);
		$title_chart = '';
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}

		$export_product = $this->Boost_matrix_kfa_model->get_data_export('usc_bst_product_mart',$area, $data_produk,$data_kompetitor, $from, $to);
		$export_variant = $this->Boost_matrix_kfa_model->get_data_export('usc_bst_product_details_mart',$area, $data_produk,$data_kompetitor, $from, $to);
		// var_dump($export_variant);
		// die;
		$bcg_detils_rekomendasi = $this->Boost_matrix_kfa_model->rekomendasi_bcg_detils_kompetitor('usc_bst_product_details_mart', $area, $data_produk, $data_kompetitor, $filter, $from, $to);
		// var_dump($bcg_detils_rekomendasi);
		// die;
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boost_matrix_kfa_model->get_area('usc_bst_product_mart'),
			'produk' => $produk,
			'produks' => $this->Boost_matrix_kfa_model->get_produk('usc_bst_product_mart'),
			'kompetitor' => $kompetitor,
			'kompetitors' => $this->Boost_matrix_kfa_model->get_kompetitor('usc_bst_product_mart'),
			'bcg' => $bcg,			
			'bcg_rekomendasi' => $bcg_rekomendasi,
			'bcg_detils' => $bcg_detils,
			// 'bcg_detils_rekomendasi' => $this->Boost_matrix_kfa_model->rekomendasi_bcg_detils_kompetitor('usc_bst_product_details_mart', $area, $data_produk, $data_kompetitor, $filter, $from, $to),
			'bcg_detils_rekomendasi' => $bcg_detils_rekomendasi,
			'cat' => $chart_sell_out->categories,
			'data_sell_out' => $chart_sell_out->data,
			'title_sell_out' => $title_chart,
			'opsi' => $filter,
			'notif' => $notif,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'export_product' => $export_product,
			'export_variant' => $export_variant,
			'data_raw' => $data_raw
		);

		if ($filter == 'quantity') {
			// var_dump($produk);
			// die;
			$data['bcg_x'] = $this->Boost_matrix_kfa_model->get_bcg_x('usc_bst_product_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boost_matrix_kfa_model->get_bcg_y('usc_bst_product_quantity', $area, $produk);
			$data['bcg_detils_x'] = $this->Boost_matrix_kfa_model->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
			$data['bcg_detils_y'] = $this->Boost_matrix_kfa_model->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
		} else {
			// var_dump($produk);
			// die;
			$data['bcg_x'] = $this->Boost_matrix_kfa_model->get_bcg_x('usc_bst_product_value', $area, $produk);
			$data['bcg_y'] = $this->Boost_matrix_kfa_model->get_bcg_y('usc_bst_product_value', $area, $produk);
			$data['bcg_detils_x'] = $this->Boost_matrix_kfa_model->get_bcg_x('usc_bst_product_details_value', $area, $produk);
			$data['bcg_detils_y'] = $this->Boost_matrix_kfa_model->get_bcg_y('usc_bst_product_details_value', $area, $produk);
		}
        $this->load->view('templates/header', $data);
        $this->load->view('boosting/produk/matrix_kfa', $data);
        $this->load->view('templates/footer', $data);
	}
	
	
	
	

}
