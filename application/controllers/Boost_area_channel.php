<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_area_channel extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();
		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->model('Boost_area_channel_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{
		$data["title"] = "Area Channel";
		$data["list_lini"] = $this->Boost_area_channel_model->area_channel_get_list_lini();
		$data["list_layanan"] = $this->Boost_area_channel_model->area_channel_get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "channel-area";
		}

		if($data["provinsi"] != ""){
			$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_channel')->result();
			// die($this->db->last_query());
		}

		if($data["data_range"] != ''){

			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		} else {
			// $data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			// $data['startDate'] = "2017/01/01";
			// $data['endDate'] = date("Y/m/d");

			// ============ code wrote on 3 October 2021 ============
			$current_month = date('Y-m-d');
            $three_monthsago = date('Y-m-d', strtotime('-3 months'));
            
			$data["data_range"] = $three_monthsago.' / '.$current_month;
			$data['startDate'] = date('Y/m/d', strtotime('-3 months'));
			$data['endDate'] = date('Y/m/d');
		
			// ======================================================
			// var_dump($data_range);
			// die();
		}
	
		if($data["areaf"] == "channel-area"){
			$data["kftd"] = $this->Boost_area_channel_model->area_channel_get_kftd();
		} else {
			$data["kftd"] = $this->Boost_area_channel_model->area_channel_get_kftd_channel();
		}
		$data['mapdata_area_channel'] =$this->populate_provinsi_area_channel($data["data_lini"],$data["data_layanan"], $data['startDate'], $data['endDate']);

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/sales/kontribusi/area_channel', $data);
		$this->load->view('templates/footer', $data);
	}

	public function populate_provinsi_area_channel($lini,$layanan, $from, $to){
		
		$array_kode_iso = array(
			// array('iso'=>'ID-AC','name'=>'Aceh', 'code'=>11),
			array('iso'=>'ID-AC','name'=>'N Aceh Darussalam', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		); 
		
		// ========================= initial code (nama provinsi tidak valid) ==============================
		// $total = Array();
		// foreach($array_kode_iso as $key=>$val){
		// 	// $total += $this->Boost_area_channel_model->area_channel_get_revenue(strtoupper($val['name']), $from, $to, $lini, $layanan);
		// 	array_push($total,[
		// 		"prov" => $val['name'],
		// 		"res" => $this->Boost_area_channel_model->area_channel_get_revenue(strtoupper($val['name']), $from, $to, $lini, $layanan)
		// 	]);
		// }
		// die($this->db->last_query());
		// var_dump($total);
		// die;
		// ==============================================================================

		// ===================== edited on 01-11-2021 - fareza ==========================
		$total = $this->Boost_area_channel_model->area_channel_get_revenue_total($from, $to, $lini, $layanan);
		// ==============================================================================
		// die($this->db->last_query());
		// var_dump($total);
		// die;

		$array_datas = array();
		foreach($array_kode_iso as $key=>$val){
			if ($total != 0) {
				$value = round(($this->Boost_area_channel_model->area_channel_get_revenue(strtoupper($val['name']), $from, $to, $lini,$layanan) / $total)*100, 2);
			} else {
				$value = 0;
			}
			
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=> $value));
		}
		// die($this->db->last_query());

		return json_encode($array_datas);
	}

	public function json_channel_area(){
		header('Content-Type: application/json');
		// die($this->db->last_query());
		// dd();
		$a = $this->Boost_area_channel_model->json_channel_area();
		// die($a);
        echo $a;
	}
	
	public function json_area_channel(){
		header('Content-Type: application/json');
		// $data = $this->Boost_area_channel_model->json_channel_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boost_area_channel_model->json_area_channel());
		
		// die($this->Boost_area_channel_model->json_area_channel());

		echo str_replace("I.N.H'", "INH", $data);
	}

}