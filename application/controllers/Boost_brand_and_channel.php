<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_brand_and_channel extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boost_brand_and_channel_model');
		$this->load->library('Kf');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{ 	
		$data['data_range']=$this->input->post("range", true);
		$data["data_area"] = $this->input->post("provinsi", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_produk"] = $this->input->post("produk", true);

		$area = $this->input->post("provinsi", true);
		$lini = $this->input->post("lini", true);
		$produk = $this->input->post("produk", true);

		$data['title'] = 'Brand Channel';
		$data['lini_data']=$this->input->post("lini", true);

		$data['produk']=$this->input->post("produk", true);
		
		if($data['data_range'] != ''){
			$tanggal=str_replace(" ", "", $data['data_range']);
			var_dump($data['data_range']);
			die;
			$tanggal=str_replace("/", ",", $tanggal);
			$tgl=explode(",", $tanggal);
			$data['startDate']=$tgl[0];
			$data['endDate']=$tgl[1];
			
			$from = $tgl[0];
			$to = $tgl[1];

			$fromDate = explode("-", $tgl[0]);
			$toDate = explode("-", $tgl[1]);
			$fromYear = $fromDate[0];
			$fromMonth = $fromDate[1];
			$fromDay = $fromDate[2];
			$toYear = $toDate[0];
			$toMonth = $toDate[1];
			$toDay = $toDate[2];
		} else {
			$data["data_range"] = date("2019-m-d").'/'.date("Y-m-d");
			$tanggal = str_replace(" ","", $data['data_range']);
			$tanggal = str_replace("/",",", $tanggal);
			$tgl=explode(",", $tanggal);
			$data['startDate']=$tgl[0];
			$data['endDate']=$tgl[1];

			$from = $tgl[0];
			$to = $tgl[1];
			$fromDate = explode("-", $tgl[0]);
			$toDate = explode("-", $tgl[1]);
			$fromYear = $fromDate[0];
			$fromMonth = $fromDate[1];
			$fromDay = $fromDate[2];
			$toYear = $toDate[0];
			$toMonth = $toDate[1];
			$toDay = $toDate[2];
		}

		if (is_array($area)) {
			$data_area = $area;
		} else {
			$data_area = explode(',', $area);
		}

		if(is_array($lini)){	
			$lini_data = $lini;	
		} else {
			$lini_data = explode(',', $lini);
		}

		if(is_array($produk)){	
			$data_produk = $produk;	
		} else {
			$data_produk = explode(',', $produk);
		}

		// start first query
		$this->db->distinct()->select('channel');
		$this->db->group_start();
		foreach($data_produk as $prod){
			$this->db->or_like('nama_brand', $prod);
		}
		$this->db->group_end();
		$datachannel = $this->db->get('usc_bst_sales_brand_channel_coba')->result();
		
		$arrwarna = array();
		foreach($datachannel as $channel){
			$arrwarna[$channel->channel] = "#".$this->random_color();
		}

		$data['data_channel'] = $datachannel;
		$data['arrWarna'] = $arrwarna;
		// end first query
		
		// start second query
		$this->db->select("`channel`,`nama_kftd`, SUM(`revenue`) AS nowyear, SUM(`revenue_1`) AS lastyear");
	
		if (!empty($data_area)) {
			$this->db->group_start();
			if (sizeof($data_area) != 0) {
				foreach ( $data_area as $ar) {
					$this->db->or_like('nama_kftd', $ar);
				}
			}
			$this->db->group_end();
		}

		if (!empty($lini_data)) {
			$this->db->group_start();
			if(sizeof($lini_data) != 0){
				foreach ($lini_data as $lin) {
					$this->db->or_like('lini', $lin);
				}
			}	
			$this->db->group_end();
		}
		
		if (!empty($data_produk)) {
			$this->db->group_start();
			if(sizeof($data_produk) != 0){
				foreach ($data_produk as $dat) {
					$this->db->or_like('nama_brand', $dat);
				}
			}	
			$this->db->group_end();
		}
		
		// $this->db->where('tanggal >=', $from);
		// $this->db->where('tanggal <=', $to);

		$this->db->where('tahun = ', $fromYear);
		$this->db->where('tahun = ', $toYear);
		$this->db->where('bulan = ', $fromMonth);
		$this->db->where('bulan = ', $toMonth);
		$this->db->where('tanggal = ', $fromDay);
		$this->db->where('tanggal = ', $toDay);

		$this->db->group_by("channel");
		$datas = $this->db->get("usc_bst_sales_brand_channel_coba")->result();
		// end second query
		// var_dump($datas);
		// die();

		$kftds = $this->db->select("nama_kftd, SUM(revenue) AS total")->group_by("nama_kftd")->get("usc_bst_sales_brand_channel_coba")->result();
		
		$x=0;
		$y=0;
	
		$strseries = "";
		foreach($kftds as $kftd){
			foreach($datas as $datanya){
				if($datanya->nama_kftd == $kftd->nama_kftd){
					if($datanya->lastyear == 0 && $datanya->nowyear > 0){
						$x = 100;
					}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
						$x = -100;
					}else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
						$x = 0;
					}else{
						if($datanya->lastyear>0 && $datanya->nowyear>0){
						$x = round(( $datanya->nowyear - $datanya->lastyear ) / $datanya->lastyear, 3);
						}else{
							$x=0;
						}
					}

					if($datanya->nowyear == 0 && $datanya->lastyear == 0){
						$y = 0;
					}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
						$y= -100;
					}else if($datanya->lastyear == 0 && $datanya->nowyear>0){
						$y= 100;
					}
					else{
						if($datanya->lastyear>0 && $datanya->nowyear>0){
							// $y = round(($datanya->nowyear / $kftd->total) * 100, 3);
							$y = round(($datanya->nowyear / $datanya->lastyear) * 100, 3);
							// loss growth

							// if($y>100){
							// 	$y=100;
							// }else if($y<-100){
							// 	$y=-100;
							// }else{
							// 	$y = round(($datanya->nowyear / $kftd->total) * 100, 3);
							// }

						} else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
							$y=0;
						}

					}

					$strseries .= "{\n";
					$strseries .= "name : \"".$datanya->channel."\",";
					$strseries .= "showInLegend : false,";
					$strseries .= "marker : {\n";
					$strseries .= "symbol : \"circle\",\n";
					$strseries .= "},";
					// $strseries .= "labelFormat : \"".$datanya->channel."\",";
					$strseries .= "\ndata : [{\n";
					$strseries .= "x : ".$x.",\n";
					$strseries .= "y : ".$y.",\n";
					$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
					$strseries .= "}]";	
					$strseries .= "},";
							
					if(empty($datanya->channel)){
						$strseries .= "{\n";
							$strseries .= "name : \"".$datanya->channel." - ".$datanya->nama_customer."\",";
							$strseries .= "showInLegend : false,";
							$strseries .= "marker : {\n";
							$strseries .= "symbol : \"circle\",\n";
							$strseries .= "},";
							// $strseries .= "labelFormat : \"".$datanya->channel."\",";
							$strseries .= "\ndata : [{\n";
							$strseries .= "x : ".null.",\n";
							$strseries .= "y : ".null.",\n";
							$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
							$strseries .= "}]";	
							$strseries .= "},";
					}
				}
			}
		}
		// var_dump($strseries);
		// die();

		$data["scatter_series"] = substr_replace($strseries, "", -1);
		$data["list_lini"] = $this->Boost_brand_and_channel_model->get_lini_list();
		$data["list_area"] = $this->Boost_brand_and_channel_model->get_area_list();
		$data["list_produk"] = $this->Boost_brand_and_channel_model->get_produk_list();
		$data["channelf"] = $this->input->post("channel", true);
	
		if($data["channelf"] == ""){
			$data["channelf"] = "brand-channel";
		}

		if($data["channelf"] == "brand-channel"){
			$data["chan"] = $this->Boost_brand_and_channel_model->get_chan();
		} else {
			$data["chan"] = $this->Boost_brand_and_channel_model->get_chan_brand();
		}

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/sales/kontribusi/brand_and_channel', $data);
		$this->load->view('templates/footer', $data);
	}

	function random_color_part() {
		return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}
	
	function random_color() {
		return random_color_part() . random_color_part() . random_color_part();
	}

	public function json_brand_channel()
	{
		header('Content-Type: application/json');
		echo $this->Boost_brand_and_channel_model->json_brand_channel();
	}

	public function json_channel_brand()
	{
		header('Content-Type: application/json');
		$data = str_replace("MARCKS'", "MARCKS_", $this->Boost_brand_and_channel_model->json_channel_brand());
		echo str_replace("I.N.H'", "INH", $data);
	}

	public function json_get_brand()
	{
		$table = $this->input->post("table", true);
		$lini = $this->input->post("lini", true);
		$brand_by_lini = $this->Boost_brand_and_channel_model->get_brand($table, $lini);
		foreach ($brand_by_lini as $list) {
			echo '<option value="' . $list->nama_brand . '">' . $list->nama_brand . "</option>";
		}
	}

}