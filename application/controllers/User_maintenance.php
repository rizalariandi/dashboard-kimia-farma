<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_maintenance extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('User_maintenance/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'user_maintenance')
    {

        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['level'] = $this->user_model->getLevel();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }


    public function change()
    {

        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['level'] = $this->user_model->getLevel();
            $data['p'] = "";
            $data['title'] = "Change Password"; // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/' . 'f_change', $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getUser()
    {
        $data = $this->user_model->getUser();
        echo json_encode($data);
    }

    public function updateUser()
    {
        $nama = $this->input->get_post('nama');
        $username = $this->input->get_post('username');
        $email = $this->input->get_post('email');
        $password = $this->input->get_post('password');
        $hp = $this->input->get_post('hp');
        $unit = $this->input->get_post('unit');
        $level = $this->input->get_post('level');
        $jabatan = $this->input->get_post('jabatan');
        $nik = $this->input->get_post('nik');
        $active = $this->input->get_post('active');
        $entitas = json_decode($this->input->get_post('entitas'));
        $data = $this->user_model->updateUser($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active, $entitas, $this->session->userdata('nama'));
        echo json_encode($data);
    }

    public function DelUser()
    {
        $email = $this->input->get_post('email');
        $data = $this->user_model->DelUser($email);
        echo json_encode($data);
    }

    public function register()
    {
        $nama = $this->input->get_post('nama');
        $username = $this->input->get_post('username');
        $email = $this->input->get_post('email');
        $password = $this->input->get_post('password');
        $hp = $this->input->get_post('hp');
        $unit = $this->input->get_post('unit');
        $level = $this->input->get_post('level');
        $jabatan = $this->input->get_post('jabatan');
        $nik = $this->input->get_post('nik');
        $active = $this->input->get_post('active');
        $entitas = json_decode($this->input->get_post('entitas'));
        $data = $this->user_model->register($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active, $entitas, $this->session->userdata('nama'));
        echo json_encode($data);
    }

    public function change_password()
    {
        $current_password = $this->input->get_post('current_password');
        $new_password = $this->input->get_post('new_password');
        $retype_password = $this->input->get_post('retype_password');
        $data['user'] = $_SESSION['username'];
        $data['pass'] = $current_password;

        $this->load->model('user_model');

        $cek = $this->user_model->login($data,false);
        if (sizeof($cek) > 0) {
            $data['message'] = $this->user_model->update_password($current_password, $new_password, $retype_password, $_SESSION['email']);
            $data['status'] = "success";
        } else {
            $data['status'] = "error";
        }


        echo json_encode($data);
    }
}
