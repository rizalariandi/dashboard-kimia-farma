<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_b2b_reward extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('Crm_b2b_reward_model', 'dm');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
        // ini_set('memory_limit',-1);
	}

    public function index()
    {
        //ini_set('max_execution_time', 3600);
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";
 
        $data['title']	= 'B2B Reward';   
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_reward', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json()
	{
		if(!$this->input->is_ajax_request()) return false;

		$columns = array(
			array( 'db' => 'cbr_date', 'dt' => 0 ),
			array( 'db' => 'cbr_customer_code', 'dt' => 1 ),
			array( 'db' => 'cbr_customer_name', 'dt' =>2 ),
			array( 'db' => 'cbr_material', 'dt' =>3 ), 
			array( 'db' => 'cbr_product_name', 'dt' =>4 ),
			array( 'db' => 'cbr_level', 'dt' =>5 ),
			array( 'db' => 'cbr_area', 'dt' =>6 ),
			array( 'db' => 'cbr_reward', 'dt' =>7 ),
            array( 'db' => 'cbr_id', 'dt' =>8 )
		);
		
		$this->datatables->set_cols($columns);
        $param	 = $this->datatables->query(); 
        
		$periode = $this->input->post('periode');   
		$lini 	 = $this->input->post('lini');  
        $area    = $this->input->post('area'); 
        $channel = $this->input->post('channel'); 
        $layanan = $this->input->post('layanan'); 
        $area    = str_replace("_"," ",$area);
        $channel = str_replace("_"," ",$channel);
        $layanan = str_replace("_"," ",$layanan); 
        $lini    = str_replace("_"," ",$lini);
         

        $result = $this->dm->dtquery($param)->result();
		$filter = $this->dm->dtfiltered();
		$total	= $this->dm->dtcount();
		$output = $this->datatables->output($total, $filter);   

        foreach($result as $row)
		{ 
            $rows = array ( 
                $row->cbr_date,
                $row->cbr_customer_code,
				strtoupper($row->cbr_customer_name),
                strtoupper($row->cbr_material), 
                strtoupper($row->cbr_product_name),
                strtoupper($row->cbr_level),
                strtoupper($row->cbr_area),
                strtoupper($row->cbr_reward),
                '<a href="javascript:edit('.$row->cbr_id.')" title="Edit Data" class="btn btn-xs btn-icon btn-primary"><i class="fa fa-edit"></i></a>
                <a href="javascript:del('.$row->cbr_id.',\''.$row->cbr_date.'\',\''.$row->cbr_customer_code.'\')" title="Delete Data" class="btn btn-xs btn-icon btn-danger"><i class="fa fa-trash"></i></a>'
			);
			
			$output['data'][] = $rows;
		} 
		
		echo json_encode( $output );
    }

    public function create()
    {
        // $mark = 0;
        // if ($this->input->post()){
        //     $dt['cbr_date'] = $this->input->post('tanggal') != '' && !empty($this->input->post('tanggal')) ? trim($this->input->post('tanggal')) : $mark += 1;
        //     $dt['cbr_customer_code'] = trim($this->input->post('customer_code')) != '' && !empty(trim($this->input->post('customer_code'))) ? trim($this->input->post('customer_code')) : $mark += 1;
        //     $dt['cbr_customer_name'] = trim($this->input->post('nama_customer')) != '' && !empty(trim($this->input->post('nama_customer'))) ? trim($this->input->post('nama_customer')) : $mark += 1;
        //     $dt['cbr_material'] = trim($this->input->post('material')) != '' && !empty(trim($this->input->post('material'))) ? trim($this->input->post('material')) : $mark += 1;
        //     $dt['cbr_product_name'] = trim($this->input->post('nama_produk')) != '' && !empty(trim($this->input->post('nama_produk'))) ? trim($this->input->post('nama_produk')) : $mark += 1;
        //     $dt['cbr_level'] = trim($this->input->post('level')) != '' && !empty(trim($this->input->post('level'))) ? trim($this->input->post('level')) : $mark += 1;
        //     $dt['cbr_area'] = trim($this->input->post('area')) != '' && !empty(trim($this->input->post('area'))) ? trim($this->input->post('area')) : $mark += 1;
        //     $dt['cbr_reward'] = trim($this->input->post('reward')) != '' && !empty(trim($this->input->post('reward'))) ? trim($this->input->post('reward')) : $mark += 1;

        //     if ($mark == 8){
        //         $this->dm->add($dt);
        //         $this->session->set_flashdata('input_success', 'Data berhasil masuk!!');
        //         redirect('crm_b2b_reward');
        //     }else{
        //         $this->session->set_flashdata('input_error', 'Semua input field harus diisi!!');
        //     }
            
        // }
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['title']    = 'B2B Reward Add';
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_reward_create', $data);
        $this->load->view('templates/footer', $data);
    }

    function insert_reward(){
        $mark = 0;
        if ($this->input->post()){
            $dt['cbr_date'] = $this->input->post('tanggal') != '' && !empty($this->input->post('tanggal')) ? trim($this->input->post('tanggal')) : '';
            $dt['cbr_customer_code'] = trim($this->input->post('customer_code')) != '' && !empty(trim($this->input->post('customer_code'))) ? trim($this->input->post('customer_code')) : '';
            $dt['cbr_customer_name'] = trim($this->input->post('nama_customer')) != '' && !empty(trim($this->input->post('nama_customer'))) ? trim($this->input->post('nama_customer')) : '';
            $dt['cbr_material'] = trim($this->input->post('material')) != '' && !empty(trim($this->input->post('material'))) ? trim($this->input->post('material')) : '';
            $dt['cbr_product_name'] = trim($this->input->post('nama_produk')) != '' && !empty(trim($this->input->post('nama_produk'))) ? trim($this->input->post('nama_produk')) : '';
            $dt['cbr_level'] = trim($this->input->post('level')) != '' && !empty(trim($this->input->post('level'))) ? trim($this->input->post('level')) : '';
            $dt['cbr_area'] = trim($this->input->post('area')) != '' && !empty(trim($this->input->post('area'))) ? trim($this->input->post('area')) : '';
            $dt['cbr_reward'] = trim($this->input->post('reward')) != '' && !empty(trim($this->input->post('reward'))) ? trim($this->input->post('reward')) : '';

            if ($dt['cbr_date'] != ''){$mark += 1;}
            if ($dt['cbr_customer_code'] != ''){$mark += 1;}
            if ($dt['cbr_customer_name'] != ''){$mark += 1;}
            if ($dt['cbr_material'] != ''){$mark += 1;}
            if ($dt['cbr_product_name'] != ''){$mark += 1;}
            if ($dt['cbr_level'] != ''){$mark += 1;}
            if ($dt['cbr_area'] != ''){$mark += 1;}
            if ($dt['cbr_reward'] != ''){$mark += 1;}

            if ($mark == 8){
                // $this->session->set_flashdata('input_success', 'Data berhasil masuk!!');
                $this->dm->add($dt);
                echo json_encode(array(
                    "status" => "success",
                    "msg" => "Input Data Berhasil!"
                ));
            }else{
                // $this->session->set_flashdata('input_error', 'Semua input field harus diisi!!');
                
                // die("masuk else");
                echo json_encode(array(
                    "status" => "error",
                    "msg" => "Semua input field harus diisi!"
                ));
            }  
        }
    }

    function imports()
    { 
        // print_r($_FILES);
        // set_time_limit(0);
        // ini_set('memory_limit', '-1');
        $this->load->library('Excel');

        $objPHPExcel = new Excel();
        $inputFileName = $_FILES['csv_file']['tmp_name'];
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            $msg = 'Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage();
            echo json_encode(array('status'=> 'error;', 'msg' => $msg));
            return false;
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $warning = 0; $warningtxt = '';
        $no = 0;
 
        for ($row = 5; $row <= $highestRow; $row++)
        {
            $no++;
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':H' . $row, NULL, TRUE, FALSE);
            $rowData = $rowData[0];
            // print_r($rowData);
            /* Data Siswa */
            $dt = array();
            $temp = explode("-",$rowData[0]); 
          
            $dates = (array_key_exists('2',$temp)?$temp[2]:'').'-'.(array_key_exists('1',$temp)?$temp[1]:'').'-'.(array_key_exists('0',$temp)?$temp[0]:'');
            if($this->validateDate($dates))
            { 
                $dt['cbr_date'] = trim($dates);
                $dt['cbr_customer_code'] = trim($rowData[1]);
                $dt['cbr_customer_name'] = trim($rowData[2]);
                $dt['cbr_material'] = trim($rowData[3]);
                $dt['cbr_product_name'] = trim($rowData[4]);
                $dt['cbr_level'] = trim($rowData[5]);
                $dt['cbr_area'] = trim($rowData[6]);
                $dt['cbr_reward'] = trim($rowData[7]);

                if($this->dm->add($dt))
                {
                    $warningtxt .= '<span class="text-success">Data '.$no.'. '.trim($rowData[1].' '.$rowData[2]).' <strong>Berhasil</strong></span><br>';
                }
                else
                { 
                    $warningtxt .= '<span class="text-danger">Data '.$no.'. '.trim($rowData[1].' '.$rowData[2]).' <strong>Gagal</strong></span><br>';
                    $warning++;
                } 
            } 
            else  {
                $warning++;
                $warningtxt .= '<span class="text-danger">Data '.$no.'. '.trim($rowData[1].' '.$rowData[2]).' <strong>Gagal</strong></span><br>'; 
            }
        }

        $msg = "Data berhasil disimpan.
                <strong>Total Data : {$no}</strong> (<strong>".$warning." Gagal</strong>, <strong>".($no-$warning)." Berhasil</strong>)<br><br>".$warningtxt;

        echo json_encode(array('status'=> 'ok;', 'msg' => $msg));
    }

    function validateDate($date, $format = 'Y-m-d')
    { 
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function delete_inp()
    {
        if(!$this->input->is_ajax_request()) return false;
        if(!$this->input->post('id')) return false;

        $id = $this->input->post('id');

        if( $this->dm->delete($id) )
            echo json_encode(array('status' => 'ok;', 'text' => ''));
        else
            echo json_encode(array('status' => 'error;', 'text' => 'Gagal Menghapus Data'));
    }

    // public function insert_inp()
    // { 
    //     if(!$this->input->is_ajax_request()) return false;
        
    //     $value = $this->input->post('value');
    //     $freq = $this->input->post('freq');
    //     $lini = $this->input->post('lini');
    //     $layanan = $this->input->post('layanan');
    //     $year = $this->input->post('year');

    //     if(!$this->dm->getbyid($year)->row()){
    //         $inp['rule_year'] =  $this->input->post('year');
    //         for($i=1;$i<=12;$i++){
    //             $inp['rule_month'] =  $i;
    //             foreach($value as $key => $val){
    //                 $inp['rule_lini'] = $lini[$key];
    //                 $inp['rule_layanan'] = $layanan[$key];
    //                 $inp['rule_value'] = $val;
    //                 $inp['rule_freq'] = $freq[$key];
    //                 $this->dm->add($inp);
    //             }
    //         }
    //         echo json_encode(array('status' => 'ok;', 'text' => ''));
    //     }
    //     else echo json_encode(array('status' => 'error;', 'text' => 'Data tahun sudah ada'));
    // }

    public function edit_inp()
	{ 
        if(!$this->input->is_ajax_request()) return false;
        $id = $this->input->post('id');
		echo json_encode($this->dm->getbyid($id)->row());
    }
    
    
    public function update_inp()
    { 
        if(!$this->input->is_ajax_request()) return false;
       
        
        $inp = $this->input->post('inp'); 
        $id = $this->input->post('id'); 

        if($this->dm->edit($id,$inp)){ 
            echo json_encode(array('status' => 'ok;', 'text' => ''));
        }
        else echo json_encode(array('status' => 'error;', 'text' => 'Gagal update data'));
        
    }
      
    public function detail($id='')
    {
        if(empty($id))
            redirect('crm_b2b');
 
        $year   = date('Y');
        // $year   = '2018';
        $month  = date('n');
        
        $data['detail'] = $this->dm->b2b_detail($id);
        //$data['level'] = $this->dm->b2b_level($id)->result();

        //==================== GRAFIK LINI ========================================
        $lini_dbs = $this->db->query("SELECT cct_lini, cct_month, sum(rev) as jml FROM crm_b2b_cust_trans WHERE cct_cust_code='$id' and cct_year='$year' GROUP BY cct_lini, cct_month")->result();

        $lini = array();
        foreach($lini_dbs as $ld) {
            $lini[$ld->cct_lini][(int) $ld->cct_month] = $ld->jml;
        }

        $lini_array = array();
        if(!empty($lini)) {
            foreach($lini as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($lini[$name][$m]))
                        $month[] = (int) $lini[$name][$m];
                    else
                        $month[] = 0;
                }

                $lini_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['lini'] = $lini_array;
        //===========================================================================

        //==================== GRAFIK Produk ========================================
        /*$prod_dbs = $this->db->query("SELECT year, month, sum(rev) as jml FROM crm_b2b_cust_total_sku WHERE customer_code = '$id' and year = '$year' GROUP BY year, month")->result();

        $prod = array();
        foreach($prod_dbs as $ld) {
            $prod[$ld->cct_year][$ld->cct_month] = $ld->jml;
        }

        $prod_array = array();
        if(!empty($prod)) {
            foreach($prod as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($prod[$name][$m]))
                        $month[] = (int) $prod[$name][$m];
                    else
                        $month[] = 0;
                }

                $prod_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['prod'] = $prod_array;*/
        //========================================================================

        //============== Prediksi tipe konsumen ==================================
        $temp  = $this->db->query(" SELECT * FROM crm_b2b_cust_prediction WHERE cct_cust_code = '$id' and cct_year = '$year'")->result();
        $month = date('m');

        $level['regular'] = '1';
        $level['bronze'] = '3';
        $level['silver'] = '5';
        $level['gold'] = '8';

        $templini = array();
        foreach($temp as $tmp) {
            if($tmp->cct_month <= $month) {
                $templini[$tmp->cct_lini][$tmp->cct_month] = strtolower($tmp->level);
            } else {
                $templini[$tmp->cct_lini][$tmp->cct_month] = strtolower($tmp->prediction_1);
            }
        }

        $pred_array = array();
        if(!empty($templini)) {
            foreach($templini as $name => $value) {
                $monthx = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($templini[$name][$m]))
                        $monthx[] = !empty($level[$templini[$name][$m]]) ? (int) $level[$templini[$name][$m]] : (int) 1;
                    else
                        $monthx[] = (int) 1;
                }

                $pred_array[] = array('name' => $name, 'data' => $monthx);
            }
        }
        $data['prediksi'] = $pred_array;
        $data['level_point'] = $month-1;

        //============= LINI =========================
        $data['lini_level'] = $this->db->query("SELECT distinct cct_lini, level FROM crm_b2b_cust_lini_level WHERE cct_cust_code = '$id'")->result();

        $lini_rekomendasi = $this->db->query("SELECT * FROM crm_manual_input WHERE mi_model_business = 'B2B Relation' and month(mi_start_period) <= ".date('m')." and month(mi_end_period) >= ".date('m')." and year(mi_start_period) = ".date('Y'))->result();
        $data['lini_rec'] = array();
        foreach($lini_rekomendasi as $lr) {
            $data['lini_rec'][strtoupper($lr->mi_lini)][strtoupper($lr->mi_level)] = strtoupper($lr->mi_program);
        }
        //============================================

        $rev_sku = $this->db->query("SELECT month cct_month,year,total_sku,rev FROM crm_b2b_cust_total_sku WHERE customer_code = '$id' and year = '".date('Y')."'")->result();
        $revsku = array();
        foreach($rev_sku as $rs) {
            $revsku[$rs->cct_month] = array('rev' => $rs->rev, 'sku' => $rs->total_sku);
        }
        $data['bulan_rev'] = array();
        $data['bulan_sku'] = array();
        for($m=1; $m<=12; $m++) {
            $data['bulan_rev'][] = !empty($revsku[$m]['rev']) ? (int) $revsku[$m]['rev'] : 0;
            $data['bulan_sku'][] = !empty($revsku[$m]['sku']) ? (int) $revsku[$m]['sku'] : 0;
        }

        /*for($i=0;$i<=11;$i++){
            $lvl[$i] = 0;
        }

        foreach($temp as $row) {
            $ts1 = (int) $row->ccl_month-1;
            if($row->ccl_month<=$month){
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_level]) ? $level[$row->ccl_level] : '0' ;
            }
            else {
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_level_prediction]) ? $level[$row->ccl_level_prediction] : '0' ;
            }
        }

        $lvlx = array();
        foreach($lvl as $key => $value)
            $lvlx[$key+1] = $value;

        $data['level_point'] = $month-1;
        $data['level'] = json_encode($lvl);*/
        //================================================================

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/b2b_detail';
        $data['title']	= 'B2B Relation';
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2b_detail', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json_transaction()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_brand', 'dt' => 1 ),
			array( 'db' => 'cct_product', 'dt' => 2 ),
			array( 'db' => 'bcc_cabang', 'dt' => 3 ),
			array( 'db' => 'rev', 'dt' => 4 ) ,
			array( 'db' => 'qty', 'dt' => 5 ) ,
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (customer_code='".$id."' )";
        else $param['where'] .= " AND (customer_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_trans($param)->result();
		$filter = $this->dm->dtfiltered_detail_trans();
		$total	= $this->dm->dtcount_detail_trans();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				date('d/m/Y', strtotime($row->cct_faktur_date)),
				$row->cct_brand, 
				$row->cct_product, 
				$row->bcc_cabang,
                number_format($row->rev,0,",","."),
                $row->qty
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_reward()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_level', 'dt' => 1 ),
			array( 'db' => 'bcc_cabang', 'dt' => 2 ),
			array( 'db' => 'cct_total_reward', 'dt' => 3 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (cct_cust_code='".$id."' )";
        else $param['where'] .= "AND (cct_cust_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_trans($param)->result();
		$filter = $this->dm->dtfiltered_detail_trans();
		$total	= $this->dm->dtcount_detail_trans();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				$row->cct_faktur_date, 
				$row->cct_level, 
				$row->bcc_cabang, 
				$row->cct_total_reward
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_return()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'cct_faktur_date', 'dt' => 0 ),
			array( 'db' => 'cct_brand', 'dt' => 1 ),
			array( 'db' => 'cct_product', 'dt' => 2 ),
			array( 'db' => 'qty', 'dt' => 3 ) ,
			array( 'db' => 'bcc_cabang', 'dt' => 4 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (cct_cust_code='".$id."' )";
        else $param['where'] .= " AND (cct_cust_code='".$id."')";
            
		$result = $this->dm->dtquery_detail_retur($param)->result();
		$filter = $this->dm->dtfiltered_detail_retur();
		$total	= $this->dm->dtcount_detail_retur();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
                date('d/m/Y', strtotime($row->cct_faktur_date)),
				$row->cct_brand, 
				$row->cct_product, 
				$row->qty,
                $row->bcc_cabang
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
	}
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
}

?>