<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boost_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
    }

    public function bcg_matrix(){
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$kompetitor = $this->input->post('kompetitor');
        $filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

        if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}
		if(empty($kompetitor)){
			$kompetitor = 'imunos';
		}

		if(empty($filter)){
			$filter = 'value';
        }
        $check = $this->Boost_model->check_data('usc_bst_product_mart', $area, $produk, $from, $to);
        // $check_kompetitor = $this->Boost_model->check_data_kompetitor('usc_bst_product_mart', $area, $kompetitor, $from, $to);
		// $checkbaru = $this->Boost_model->check_data_baru('usc_bst_product_mart', $area, $produk, $from, $to);
		// var_dump($check);
		// die();
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}

        $bcg = $this->Boost_model->get_bcg_matrix('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		// var_dump($bcg);
		$bcg_rekomendasi = $this->Boost_model->get_bcg_rekomendasi('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		// var_dump($bcg_rekomendasi);
		$bcg_detils = $this->Boost_model->get_bcg_matrix_detils('usc_bst_product_details_mart', $area, $produk, $kompetitor, $filter, $from, $to);
		// var_dump($bcg_detils);

		$sell_out = $this->Boost_model->sell_out_kfa('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
        // var_dump($sell_out);
        
        $data_raw = $this->Boost_model->data_sell_out('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		$chart_sell_out = json_decode($sell_out);
		$title_chart = '';
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}

		$export_product = $this->Boost_model->get_data_export('usc_bst_product_mart',$area, $produk, $from, $to);
		$export_variant = $this->Boost_model->get_data_export('usc_bst_product_details_mart',$area, $produk, $from, $to);

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boost_model->get_area('usc_bst_product_mart'),
			'produk' => $produk,
			'produks' => $this->Boost_model->get_produk('usc_bst_product_mart'),
			'kompetitor' => $this->Boost_model->get_kompetitor('usc_bst_product_mart'),
			'bcg' => $bcg,			
			'bcg_rekomendasi' => $bcg_rekomendasi,
			'bcg_detils' => $bcg_detils,
			'bcg_detils_rekomendasi' => $this->Boost_model->rekomendasi_bcg_detils('usc_bst_product_details_mart', $area, $produk, $kompetitor, $filter, $from, $to),
			'cat' => $chart_sell_out->categories,
			'data_sell_out' => $chart_sell_out->data,
			'title_sell_out' => $title_chart,
			'opsi' => $filter,
			'notif' => $notif,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'export_product' => $export_product,
			'export_variant' => $export_variant,
			'data_raw' => $data_raw
		);

		// var_dump($data_raw);
		// die();

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boost_model->get_bcg_x('usc_bst_product_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boost_model->get_bcg_y('usc_bst_product_quantity', $area, $produk);
			$data['bcg_detils_x'] = $this->Boost_model->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
			$data['bcg_detils_y'] = $this->Boost_model->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boost_model->get_bcg_x('usc_bst_product_value', $area, $produk);
			$data['bcg_y'] = $this->Boost_model->get_bcg_y('usc_bst_product_value', $area, $produk);
			$data['bcg_detils_x'] = $this->Boost_model->get_bcg_x('usc_bst_product_details_value', $area, $produk);
			$data['bcg_detils_y'] = $this->Boost_model->get_bcg_y('usc_bst_product_details_value', $area, $produk);
		}
	
        // $data;
    
        $this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix_baru', $data);
        $this->load->view('templates/footer', $data);

	}
	public function bcg_matrix_channel()
	{
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

		if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}
        		
		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		if(empty($filter)){
			$filter = 'value';
		}

		// $bcg = $this->Boost_model->get_bcg_channel('usc_bst_channel_mart', $area, $produk);
		$check = $this->Boost_model->check_data('usc_bst_channel_mart', $area, $produk, $from, $to);
		
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}
		$bcg = $this->Boost_model->get_bcg_matrix_channel('usc_bst_channel_mart', $area, $produk, $filter, $from, $to);
		$export_channel = $this->Boost_model->get_data_export_channel('usc_bst_channel_mart',$area, $produk, $from, $to);
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFTD Channel',
			'area' => $area,
			'areas' => $this->Boost_model->get_area('usc_bst_channel_mart'),
			'produk' => $produk,
			'produks' => $this->Boost_model->get_produk_channel('usc_bst_channel_mart'),
			'bcg' => $bcg,
			'opsi' => $filter,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'notif' => $notif,
			'export_channel' => $export_channel
		);

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boost_model->get_bcg_x('usc_bst_channel_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boost_model->get_bcg_y('usc_bst_channel_quantity', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boost_model->get_bcg_x('usc_bst_channel_value', $area, $produk);
			$data['bcg_y'] = $this->Boost_model->get_bcg_y('usc_bst_channel_value', $area, $produk);
		}
			
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix_channel', $data);
        $this->load->view('templates/footer', $data);
	}
	public function outlet_otr()
	{
		$area = $this->input->post('area');
		$channel = $this->input->post('channel');

		$data["data_area"] = $this->input->post("area_table", true);
		$data["data_channel"] = $this->input->post("channel_table", true);
		$data["periode_table_outlet"] = $this->input->post("periode_table_outlet", true);
	
		if (empty($data["periode_table_outlet"])) {
			$data["periode_table_outlet"] = date('Y');
		}

		$data_otr = $this->Boost_model->outlet_otr($area, $channel);
		$tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');
		$json_data_otr = array();
		
		$nama_brand = $this->Boost_model->get_nama_brand('usc_bst_sales_outlet_channel');
		$list_brand = $this->Boost_model->get_brand_outlet('usc_bst_sales_outlet_channel');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		// $bulan = $this->db->distinct()->select('month(tanggal) as bulan')->where('year(tanggal)', $data["periode_table_outlet"])->get('usc_bst_sales_brand_channel')->result();
		$bulan = $this->db->distinct()->select('month(tanggal) as bulan')->get('usc_bst_sales_brand_channel')->result();
		
		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$outlet = $this->Boost_model->json_outlet_otr();
		$trend = $this->Boost_model->json_outlet_trend();
		$dec = json_decode($outlet);
		$decode_trend = json_decode($trend);
		$last_update = $this->Boost_model->get_last_update();
		$data_otr=json_encode($dec->data, JSON_NUMERIC_CHECK);
		$lini =$this->Boost_model->get_lini_otr('usc_bst_sales_brand_channel');
		$channels = $this->Boost_model->get_channels('usc_bst_sales_outlet_channel');
		
		// var_dump($channels);
		// var_dump($last_update[0]->tanggal);
		// die();
		// var_dump($dec->nama_brand);
		// die();
		// var_dump(implode(',',$dec->nama_brand));

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Outlet OTR',
			'bulan' => $bulan,
			'tahun' => $tahun,
			'tahun_otr' => $tahun_otr,
			'provinsi' => $this->Boost_model->get_provinsi('usc_bst_sales_outlet_channel'),
			'area' => $area,
			'areas' => $this->Boost_model->get_list_kftd('usc_bst_sales_outlet_channel'),
			'channel' => $channel,
			'channels' => $channels,
			'lini' => $lini,
			'linis' => $this->Boost_model->get_lini_otr('usc_bst_sales_outlet_channel'),
			'brand' => $this->Boost_model->get_brand_otr('usc_bst_sales_brand_channel'),
			'list_brand' => $list_brand,
			'nama_brand_otr' => json_encode($dec->nama_brand),
			'data_otr' => $data_otr,
			'trend_this_year' => json_encode($this->Boost_model->trend_outlet($area, $channel, $year = date("Y")),JSON_NUMERIC_CHECK),
			'trend_last_year' => json_encode($this->Boost_model->trend_outlet($area, $channel, $year - 1), JSON_NUMERIC_CHECK),
			'trend' => json_encode($decode_trend->data, JSON_NUMERIC_CHECK),
			'cat_month' => json_encode($decode_trend->bulan),
			'last_update' => $last_update[0]->tanggal
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/outlet_otr_boost', $data);
        $this->load->view('templates/footer', $data);
	}

	public function json_outlet_brand()
	{
		$this->load->model('Otr_model_boost');
		header('Content-Type: application/json');
		// echo $this->Boost_model->json_outlet_brand();
		echo $this->Otr_model_boost->json_outlet_brand();
	}

	public function json_outlet_area()
	{
		$this->load->model('Otr_model_boost');
		header('Content-Type: application/json');
		// echo $this->Boost_model->json_outlet_area();
		echo $this->Otr_model_boost->json_outlet_area();
	}

	public function json_outlet_otr()
	{
		header('Content-Type: application/json');
		echo $this->Boost_model->json_outlet_otr();
	}

	public function json_outlet_trend()
	{
		header('Content-Type: application/json');
		echo $this->Boost_model->json_outlet_trend();
	}

	public function json_outlet_product(){
		header('Content-Type: application/json');
		echo $this->Boost_model->json_outlet_product();
	}

	public function json_testing()
	{
		header('Content-Type: application/json');
		// echo json_encode($this->Boost_model->get_bcg_matrix('usc_bst_product_mart','JAWA BARAT','FITUNO'));	
		echo $this->Boost_model->get_bcg_matrix('usc_bst_product_mart','JAWA TENGAH','FITUNO');
	}

	public function growth_testing()
	{
		header('Content-Type: application/json');
	
		var_dump( linear_regression(array(7, 8, 9, 10), array(1045010, 21767950, 41470940, 5905033)) );
	}

	public function json_get_product()
	{
		header('Content-Type: application/json');
		$nama_brand = $this->input->post('nama_brand');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		$tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_transaksi_area');
		$json_data_otr = array();
		$data_otr= $this->Boost_model->get_product($nama_brand);

		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$json_data = array(
			'data' => $json_data_otr,
			'categories' => $this->Boost_model->get_list_product($nama_brand)
		);
		echo json_encode($json_data);
	}

	public function json_get_list_product()
	{
		header('Content-Type: application/json');
		echo $this->Boost_model->json_get_list_product();
	}

	public function json_get_growth_product()
	{
		header('Content-Type: application/json');
		echo $this->Boost_model->json_get_growth_product();
	}

	public function json_outlet_transaction()
	{
		header('Content-Type: application/json');
		echo $this->Boost_model->json_outlet_transaction();
	}

	// END OUTLET OTR

	public function json_get_sku()
	{
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$brand = $this->input->post("brand", true);
		$sku = $this->Boost_model->get_sku($table, $brand);
		foreach ($sku as $list) {
			echo '<option value="' . $list->nama_produk . '">' . $list->nama_produk . "</option>";
		}
	}

	public function json_get_brand()
	{
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$lini = $this->input->post("lini", true);
		$brand_by_lini = $this->Boost_model->get_brand($table, $lini);
		foreach ($brand_by_lini as $list) {
			echo '<option value="' . $list->nama_brand . '">' . $list->nama_brand . "</option>";
		}
	}


	function get_kompetitor(){
        $produk=$this->input->post('produk');
		// $data=$this->m_kategori->get_subkategori($id);
		$kompetitor = $this->Boost_model->get_kompetitor('usc_bst_product_mart',$produk);
		
        echo json_encode($kompetitor);
    }

	public function master_produk(){
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Master Produk',
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/master_produk', $data);
        $this->load->view('templates/footer', $data);
	}




public function json_scatter()
{
	header('Content-Type: application/json');
	$data['scatter_data']= $this->Boost_model->json_brand_channel_scatter();
	// var_dump($data);
}

function random_color_part() {
	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
	return random_color_part() . random_color_part() . random_color_part();
}
// ========
	// BOOSTING SALES - AREA CHANNEL
	public function area_channel_boost()
	{
		$data["list_lini"] = $this->Boost_model->area_channel_get_list_lini();
		$data["list_layanan"] = $this->Boost_model->area_channel_get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
		}

			if($data["provinsi"] != ""){
				$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
			}
		if($data["data_range"] != ''){

			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		}else{
			$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			$data['startDate'] = "2017/01/01";
			$data['endDate'] = date("Y/m/d");
		}
	
		if($data["areaf"] == "channel-area"){
			$data["kftd"] = $this->Boost_model->area_channel_get_kftd();
		}else{
			$data["kftd"] = $this->Boost_model->area_channel_get_kftd_channel();
		}
		$data['mapdata_area_channel'] =$this->populate_provinsi_area_channel($data["data_lini"],$data["data_layanan"]);

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/area_channel', $data);
		$this->load->view('templates/footer', $data);

	}
	public function populate_provinsi_area_channel($lini,$layanan){
		
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'Aceh', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		   ); 
		  
		   $array_datas = array();
		   foreach($array_kode_iso as $key=>$val){
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$this->Boost_model->area_channel_get_revenue(strtoupper($val['name']),$lini,$layanan)));
		   }
		   return json_encode($array_datas);
	}
	public function json_channel_area(){
		header('Content-Type: application/json');
        echo $this->Boost_model->json_channel_area();
	}
	public function json_area_channel(){
		header('Content-Type: application/json');
		$data = $this->Boost_model->json_channel_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boost_model->json_area_channel());
		echo str_replace("I.N.H'", "INH", $data);
	}
	// END AREA CHANNEL

}