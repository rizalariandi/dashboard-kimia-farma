<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insightful extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('user_model');
        $this->load->model('insightful_model', 'dm');
        $this->load->library('datatables');

        $this->load->helper('form');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function problem($start="",$end="",$lini='all',$layanan='all',$provinsi='all',$layanan2='all',$nama_kftd='all',$brand2='all',$nama_produk='all')
    {
        $layanan = urldecode($layanan);
        $provinsi = urldecode($provinsi);
        if(empty($start) and empty($end)) {
            // $start = '1-'.date('Y'); // initial filter from btp
            // $end = '12-'.date('Y'); // initial filter from btp

            // $current_month = date('m');
            // $current_month = date('n', strtotime('-1 months'));
            // $three_monthsago = date('n', strtotime('-3 months'));

            $current_month = date('n');
            $three_monthsago = date('n', strtotime('-2 months'));

            if($current_month = '1' || $current_month = '2'){
                $start = '1-'.date('Y');
            }else{
                $start = $three_monthsago.'-'.date('Y');
            }

            $end = $current_month.'-'.date('Y');

            // var_dump($start,$end);
            // die();
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $data['var_start'] = $start;
        $data['var_end'] = $end;
        $data['var_lini'] = $lini;
        $data['var_layanan'] = $layanan; 
        $data['var_provinsi'] = $provinsi; 
        $data['var_layanan2'] = $layanan2; 
        $data['var_kftd'] = $nama_kftd;
        $data['var_brand'] = $brand2;
        $data['var_produk2'] = $nama_produk;


        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];

        $lini_dbs = $this->dm->get_lini_target()->result();
        // $data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][strtolower($ld->lini)] = $ld->lini;
        }

        $klayanan_dbs = $this->dm->get_kelompok_layanan_target()->result();
        foreach($klayanan_dbs as $ld) {
            $data['klayanan_filter'][strtolower($ld->layanan)] = $ld->layanan;
        }

        $layanan_dbs = $this->dm->get_layanan_target("")->result();
        foreach($layanan_dbs as $ld) {
            $data['layanan_filter'][strtolower($ld->nama_layanan)] = $ld->nama_layanan;
        }

        $kftd = $this->dm->get_nama_ktfd("")->result();
        foreach($kftd as $ld) {
            $data['kftd_filter'][strtolower($ld->nama_kftd)] = $ld->nama_kftd;
        }

        
        $provinsi_dbs = $this->dm->get_provinsi("")->result();
        foreach($provinsi_dbs as $ld) {
            $id = $this->prov_id($ld->provinsi);
            if(!empty($id))
                $data['provinsi_filter'][strtolower($id)] = $ld->provinsi;
        }


        $brand = $this->dm->get_brand_target("")->result();
        foreach($brand as $ld) {
            $data['brand_filter'][strtolower($ld->nama_brand)] = $ld->nama_brand;
        }

        $produk = $this->dm->get_produk_target("")->result();
        foreach($produk as $ld) {
            $data['produk_filter'][strtolower($ld->nama_produk)] = $ld->nama_produk;
        }
        $data['col_filter'][''] = 'Value';
        $data['col_filter']['qty'] = 'Qty';
     
        // $layanan_dbs = $this->dm->get_layanan_target()->result();
        // // $data['layanan_filter']['all'] = 'Semua Layanan';
        // foreach($layanan_dbs as $ld) {
        //     $data['layanan_filter'][$ld->layanan] = $ld->layanan;
        // }

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful/problem_identification';
        $data['title']	= 'Problem Identification';

        $dbs = $this->dm->getCountDaerah($s[0], $e[0], $s[1], strtoupper($lini), $layanan,$provinsi,$layanan,$nama_kftd,$brand2,$nama_produk)->result();

        $array = array();
        $array_lini = array();
        foreach($dbs as $db) {
            $id = $this->prov_id($db->provinsi);

            if($db->target > 0)
                $persen = round(($db->realisasi / $db->target)*100);
            // else
            //     $persen = 100;

            $array[] = array($id, $persen);
        }

        $data['data_maps'] = json_encode($array);
        // die($data['data_maps']);
        $data['lini'] = $this->dm->getByLini($s[0], $e[0], $s[1], $lini, $layanan, $provinsi,$layanan,$nama_kftd,$brand2,$nama_produk)->result();
        
        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful/problem_identification', $data);
        $this->load->view('templates/footer', $data);
    }

    public function getLayanan()
    {  
        $id = $this->input->post('id');

        if(!empty($id)) {
            $layanan = "AND layanan IN ('".implode("','", $id)."')"; 
        } else {
            $layanan = '';
        }

        $layanan_dbs = $this->dm->get_layanan_target($layanan)->result();
        $html = "";
        foreach($layanan_dbs as $ld) {
            $html .='<option value="'.strtolower($ld->nama_layanan).'">'.$ld->nama_layanan.'</option>'; 
        }

        echo $html;
    }

    public function getBrand_Lini()
    {  
        $id = $this->input->post('id');

        if(!empty($id)) {
            $lini = "AND lini_desc1 IN ('".implode("','", $id)."')"; 
        } else {
            $lini = '';
        }

        $linifilter_dbs = $this->dm->getBrand_Lini($lini)->result();
        $html = "";
        foreach($linifilter_dbs as $ld) {
            $html .='<option value="'.$ld->nama_brand.'">'.$ld->nama_brand.'</option>'; 
        }

        echo $html;
    }

    public function getProduk()
    {  
        $id = $this->input->post('id');

        if(!empty($id)) {
            $produk = "AND nama_brand IN ('".implode("','", $id)."')"; 
        } else {
            $produk = '';
        }

        $produk = $this->dm->getProduk($produk)->result();
        $html = "";
        foreach($produk as $ld) {
            $html .='<option value="'.$ld->nama_produk.'">'.$ld->nama_produk.'</option>'; 
        }

        echo $html;
    }

    public function getKftd_provinsi()
    {  
        // ==================== initial code ============================
        // $res_name = $this->id_prov($data->$id);
        // echo $res_name;

        // if(!empty($id)) {
        //     $provinsi = "AND provinsi IN ('".implode("','", $id)."')"; 
        // } else {
        //     $provinsi = '';
        // }
        // =============================================================

        $converted = [];
        $id = $this->input->post('id');
        $arr_post = explode(",",implode(",", $id));

        foreach($arr_post as $res){
            $tmp = $this->id_prov($res);
            array_push($converted,$tmp);
        }
        
        // echo json_encode("converted = ". implode(",",$converted) .", post data = ". implode(",",$arr_post));
        
        if(sizeof($converted) > 0 && $converted[0] != '') {
            $provinsi = "provinsi IN ('".implode("','", $converted)."')"; 
            // echo "not empty";
        } else if(sizeof($converted) > 0 && $converted[0] == '') {
            $provinsi = 'null';
            // echo "empty";
        }
        // echo $provinsi;

        $layanan_dbs = $this->dm->getKftd_provinsi($provinsi)->result();

        $html = "";
        foreach($layanan_dbs as $ld) {
            $html .='<option value="'.$ld->nama_kftd.'">'.$ld->nama_kftd.'</option>'; 
        }

        echo $html;
    }


    public function getBrand_target()
    {  
        $id = $this->input->post('id');

        if(!empty($id)) {
            $kftd = "AND nama_kftd IN ('".implode("','", $id)."')"; 
        } else {
            $kftd = '';
        }

        $kftd_dbs = $this->dm->get_brand_target($kftd)->result();
        $html = "";
        foreach($kftd_dbs as $ld) {
            $html .='<option value="'.$ld->nama_brand.'">'.$ld->nama_brand.'</option>'; 
        }

        echo $html;
    }

    public function problem_kftd()
    {
        $id = $this->input->post('id');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');

        $s = explode('-', $start);
        $e = explode('-', $end);

        if(!empty($id)) {
            $wid = explode(',', $id);
            $area = array();
            foreach($wid as $wi) {
                $prov = $this->prov_id($wi);
                if(!empty($prov))
                    $area[] = "'".$prov."'";
            }
        } else {
            $area = array();
        }

        $data['kftd'] = $this->dm->getByKftd($area, $s[0], $e[0], $s[1], $lini, $layanan)->result();

        /*//$data['area'] = $id;
        $data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        foreach($charts as $chart) {
            $data['charts']['month'][$chart->bulan] = $bln[$chart->bulan];
            $data['charts']['target'][$chart->bulan] = (int) $chart->target;
            $data['charts']['realisasi'][$chart->bulan] = (int) $chart->realisasi;
            $data['charts']['stock'][$chart->bulan] = (int) $chart->stock;
        }

        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($data['charts']['month'][$b])) {
                $data['charts']['month'][$b] = $bln[$b];
                $data['charts']['target'][$b] = 0;
                $data['charts']['realisasi'][$b] = 0;
                $data['charts']['stock'][$b] = 0;
            }
        }*/

        echo $this->load->view('insightful/problem_kftd', $data, true);
    }

    public function problem_brand()
    {
        $id = urldecode($this->input->post('id'));
        $col = $this->input->post('col');

        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');

        $s = explode('-', $start);
        $e = explode('-', $end);

        if($col == 'lini')
        {
            $data[' '] = 'Lini '.$id;
            $data['ids']   = $id;
            $data['brand'] = $this->dm->getByLiniGroupBrand($id, $s[0], $e[0], $s[1])->result();
        }
        else
        {
            $data['title'] = 'Cabang '.$id;
            $data['ids']   = $id;
            $data['brand'] = $this->dm->getByKftdGroupBrand($id, $s[0], $e[0], $s[1])->result();
        }

        $data['var_col'] = $col;

        echo $this->load->view('insightful/problem_brand', $data, true);
    }

    public function problem_product()
    {
        $brand = $this->input->post('id');
        $col = $this->input->post('col');

        $start = $this->input->post('start');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $data['title'] = $brand;
        $data['ids']   = $brand;
        $data['col']  = $col;

        if($col == 'lini')
        {
            $data['product'] = $this->dm->getByBrandGroupProduct($brand, $s[0], $e[0], $s[1])->result();
        }
        else
        {
            $data['product'] = $this->dm->getByBrandGroupProduct($brand, $s[0], $e[0], $s[1])->result();
        }

        echo $this->load->view('insightful/problem_product', $data, true);
    }

    /*public function problem_product_detail()
    {
        /*$col = $this->input->post('col');
        $produk = $this->input->post('nama_produk');
        $brand = $this->input->post('nama_produkx');
        $cabang = $this->input->post('nama_cabang');
        $month = $this->input->post('month');
        $year = $this->input->post('year');*

        $realisasi = $this->input->post('realisasi');
        $target = $this->input->post('target');
        $me = $this->input->post('me');
        $cabang = $this->input->post('cabang');
        $brand = $this->input->post('brand');

        $data['col']  = $col;  
        
        $produk = $this->dm->getByBrandGroupProductById($cabang,$brand,$produk,$month,$year)->row();
        $dts = $this->dm->getBy_marketing_expenses($cabang,$brand,$month,$year)->row();
         
        if($realisasi < $target){
            if (($me >= 0) and ($me <= 30242353)  and ($cabang == 'KFTD Purwokerto')){
                $rekomendasi = "Increase marketing marketingexpense > 0";
                $change      = "42.9 %";
            }
            else {
                if ($brand == "FITUNO"){
                    $rekomendasi = "Increase marketing marketingexpense > Rp. 30.242.353";
                    $change      = " 83.9%";
                }
                else  if ($brand == "BATUGIN"){
                    $rekomendasi = "Increase marketing marketingexpense > Rp. 151.417.884";
                    $change      = " 97.7%";
                }
            }
            $data['message'] = "Performa produk $brand di $cabang di bulan $month tahun $year hanya mencapai ".number_format(((($produk->target-$produk->realisasi)/$produk->target)*100),2)."%.<br>
            <br>Saat ini Marketing Expense untuk $brand adalah sebesar Rp. ".number_format($dts->marketing_expenses, 0, ',', '.').". Jika Marketing Expense ditingkatkan menjadi $rekomendasi, maka terdapat kemungkinan $change performa produk ini mencapai target.";
        }
        else {
            $data['message'] = "Performa produk telah mencapai target.";
        } 

        echo $this->load->view('insightful/problem_product_detail', $data, true);
    }*/

    private function prov_id($prov='')
    {
        $array['N ACEH DARUSSALAM'] = 'id-ac';
        $array['KALIMANTAN TIMUR'] = 'id-ki';
        $array['JAWA TENGAH'] = 'id-jt';
        $array['BENGKULU'] = 'id-be';
        $array['BANTEN'] = 'id-bt';
        $array['KALIMANTAN BARAT'] = 'id-kb';
        $array['BANGKA BELITUNG'] = 'id-bb';
        $array['BALI'] = 'id-ba';
        $array['JAWA TIMUR'] = 'id-ji';
        $array['KALIMANTAN SELATAN'] = 'id-ks';
        $array['NUSA TENGGARA TIMUR'] = 'id-nt';
        $array['SULAWESI SELATAN'] = 'id-se';
        $array['KEPULAUAN RIAU'] = 'id-kr';
        $array['PAPUA BARAT'] = 'id-ib';
        $array['SUMATERA UTARA'] = 'id-su';
        $array['RIAU'] = 'id-ri';
        $array['SULAWESI UTARA'] = 'id-sw';
        $array['MALUKU UTARA'] = 'id-la';
        $array['SUMATERA BARAT'] = 'id-sb';
        $array['MALUKU'] = 'id-ma';
        $array['NUSA TENGGARA BARAT'] = 'id-nb';
        $array['SULAWESI TENGGARA'] = 'id-sg';
        $array['SULAWESI TENGAH'] = 'id-st';
        $array['PAPUA'] = 'id-pa';
        $array['JAWA BARAT'] = 'id-jr';
        $array['LAMPUNG'] = 'id-1024';
        $array['DKI JAKARTA'] = 'id-jk';
        $array['GORONTALO'] = 'id-go';
        $array['DI. YOGYAKARTA'] = 'id-yo';
        $array['KALIMANTAN TENGAH'] = 'id-kt';
        $array['SUMATERA SELATAN'] = 'id-sl';
        $array['SULAWESI BARAT'] = 'id-sr';
        $array['JAMBI'] = 'id-ja';

        /*foreach($array as $p => $id) {
            echo '$array['."'".strtoupper($p)."'] = '$id';<br>";
        }*/

        $prov = strtoupper($prov);

        if(!empty($array[$prov]))
            return $array[$prov];
        else
            return '';
    }

    private function id_prov($id='')
    {
        $array['id-ac'] = 'N ACEH DARUSSALAM';
        $array['id-ki'] = 'KALIMANTAN TIMUR';
        $array['id-kb'] = 'KALIMANTAN BARAT';
        $array['id-ks'] = 'KALIMANTAN SELATAN';
        $array['id-kt'] = 'KALIMANTAN TENGAH';
        $array['id-su'] = 'SUMATERA UTARA';
        $array['id-sb'] = 'SUMATERA BARAT';
        $array['id-sl'] = 'SUMATERA SELATAN';
        $array['id-jt'] = 'JAWA TENGAH';
        $array['id-ji'] = 'JAWA TIMUR';
        $array['id-jr'] = 'JAWA BARAT';
        $array['id-se'] = 'SULAWESI SELATAN';
        $array['id-sg'] = 'SULAWESI TENGGARA';
        $array['id-st'] = 'SULAWESI TENGAH';
        $array['id-sw'] = 'SULAWESI UTARA';
        $array['id-sr'] = 'SULAWESI BARAT';
        $array['id-be'] = 'BENGKULU';
        $array['id-bt'] = 'BANTEN';
        $array['id-bb'] = 'BANGKA BELITUNG';
        $array['id-ba'] = 'BALI';
        $array['id-nt'] = 'NUSA TENGGARA TIMUR';
        $array['id-nb'] = 'NUSA TENGGARA BARAT';
        $array['id-kr'] = 'KEPULAUAN RIAU';
        $array['id-ib'] = 'PAPUA BARAT';
        $array['id-ri'] = 'RIAU';
        $array['id-la'] = 'MALUKU UTARA';
        $array['id-ma'] = 'MALUKU';
        $array['id-pa'] = 'PAPUA';
        $array['id-1024'] = 'LAMPUNG';
        $array['id-jk'] = 'DKI JAKARTA';
        $array['id-go'] = 'GORONTALO';
        $array['id-yo'] = 'DI. YOGYAKARTA';
        $array['id-ja'] = 'JAMBI';

        if(!empty($array[$id]))
            return $array[$id];
        else
            return '';
    }
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
	private function achievement($val)
    {
        return ($val*100).' %';
    }
    private function persen($val)
    {
        return ($val*100).' %';
    }
    private function growth($realisasi, $realisasi_1)
    {
        if(empty($realisasi))
            return round(($realisasi - $realisasi_1) / $realisasi)*100;
        else
            return 0;
    }
    public function json_lini()
    { 
        if(!$this->input->is_ajax_request()) return false;
// $this->output->enable_profiler(true)
        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(
            array( 'db' => 'lini', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $this->datatables->set_cols($columns);
        $param	= $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_ins_union';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        // =================== initial code ======================
        $result = $this->dm->dtquery_lini($table, $param)->result();
        // die($this->db->last_query());

        $filter = $this->dm->dtfiltered_lini($table, $param);
        // die($this->db->last_query());
        $total	= $this->dm->dtcount_lini();
        $output = $this->datatables->output($total, $filter);
        // =======================================================

        // $get_result = $this->dm->dtquery_lini($table, $param);
        // $result = $get_result->result();
        // $filter = $get_result->num_rows();
        // $total	= $this->dm->dtcount_lini();
        // $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_lini_tahunlalu($table, $param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl){
            $ly[$thl->lini] = $thl;
        }
        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_lini_stock($param2)->result();
        $stok = array();
        foreach($stock as $s){
            $stok[$s->lini] = $s;
        }
        foreach($result as $row)
        {
            if(!empty($lini) and in_array(strtolower($row->lini), $lini)){
                $selected = 'checked';
            }else{
                $selected = '';
            }
            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->lini]) ? $ly[$row->lini]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->lini]) ? $ly[$row->lini]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;

            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_lini" value="'.strtolower($row->lini).'" '.$selected.'>',
                strtoupper($row->lini),
                y_num_pad($row->target_qty),
                y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),
                !empty($stok[$row->lini]) ? y_num_pad($stok[$row->lini]->qty_stok) : '0',
                !empty($stok[$row->lini]) ? y_num_pad($stok[$row->lini]->value_stok) : '0',
                $this->persen($row->achievement),
                $this->persen($row->achievement_qty),
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
                $this->persen($growth),
                $this->persen($growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_kftd()
    {
        if(!$this->input->is_ajax_request()) return false;

        $columns = array(
            array( 'db' => 'nama_kftd', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_insight_target_realisasi';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))." '";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);


        /*if(!empty($area)) {
            $wid = explode(',', $area);
            $area_array = array();
            foreach($wid as $wi) {
                $prov = $this->id_prov($wi);
                if(!empty($prov))
                    $area_array[] = "'".strtolower($prov)."'";
            }

            if(!empty($area_array))
                $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
        }  

        if (is_array($lini)){
            $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        }

        if (is_array($layanan)) {
            $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
            $table  = 'usc_insight_target_realisasi_produk_layanan';
        }

        $dlayanan = $this->input->post('dlayanan');
        if (is_array($dlayanan)) {
            $param['where'] .= " AND (lower(layanan_name) in ".str_replace("_"," ",strtolower("('".implode("','",$dlayanan)."')")).")";
            $table  = 'usc_insight_target_realisasi_produk_layanan_detail';
        }*/

        $result = $this->dm->dtquery_kftd($param)->result();
        $filter = $this->dm->dtfiltered_kftd($param);
        // die($this->db->last_query());

        $total	= $this->dm->dtcount_kftd();
        $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_kftd_tahunlalu($param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl)
            $ly[$thl->nama_kftd] = $thl;

        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_kftd_stock($param2)->result();
        $stok = array();
        foreach($stock as $s)
            $stok[$s->nama_kftd] = $s;

        foreach($result as $row)
        {
            if(!empty($kftd) and in_array(strtolower($row->nama_kftd), $kftd))
                $selected = 'checked';
            else
                $selected = '';

            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->nama_kftd]) ? $ly[$row->nama_kftd]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->nama_kftd]) ? $ly[$row->nama_kftd]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;

            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_kftd" value="'.strtolower($row->nama_kftd).'" '.$selected.'>',
                strtoupper($row->nama_kftd),
                y_num_pad($row->target_qty),
                y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),
                !empty($stok[$row->nama_kftd]) ? y_num_pad($stok[$row->nama_kftd]->qty_stok) : '0',
                !empty($stok[$row->nama_kftd]) ? y_num_pad($stok[$row->nama_kftd]->value_stok) : '0',
                $this->persen($row->achievement),
                $this->persen($row->achievement_qty),
                $this->persen($growth),
                $this->persen($growth_qty)
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_brand()
    {
        if(!$this->input->is_ajax_request()) return false;

        $columns = array(
            array( 'db' => 'nama_brand', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_insight_target_realisasi_produk';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $result = $this->dm->dtquery_brand($param)->result();
        $filter = $this->dm->dtfiltered_brand($param);
        $total	= $this->dm->dtcount_brand();
        $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_brand_tahunlalu($param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl)
            $ly[$thl->nama_brand] = $thl;

        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_brand_stock($param2)->result();
        $stok = array();
        foreach($stock as $s)
            $stok[$s->nama_brand] = $s;

        foreach($result as $row)
        {
            if(!empty($brand) and in_array(strtolower($row->nama_brand), $brand))
                $selected = 'checked';
            else
                $selected = '';

            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->nama_brand]) ? $ly[$row->nama_brand]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->nama_brand]) ? $ly[$row->nama_brand]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;

            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_brand" value="'.strtolower($row->nama_brand).'"  '.$selected.'>',
                strtoupper($row->nama_brand),
                y_num_pad($row->target_qty),
                y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),
                !empty($stok[$row->nama_brand]) ? y_num_pad($stok[$row->nama_brand]->qty_stok) : '0',
                !empty($stok[$row->nama_brand]) ? y_num_pad($stok[$row->nama_brand]->value_stok) : '0',
                $this->persen($row->achievement),
                $this->persen($row->achievement_qty),
                $this->persen($growth),
                $this->persen($growth_qty)
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_prod()
    {
        if(!$this->input->is_ajax_request()) return false;

        $columns = array(
            array( 'db' => 'nama_produk', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $lini = str_replace("_"," ",$lini);
        $layanan = str_replace("_"," ",$layanan);

        $s = explode('-', $start);
        $e = explode('-', $end);

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_insight_target_realisasi_produk';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $result = $this->dm->dtquery_prod($param)->result();
        $filter = $this->dm->dtfiltered_prod($param);
        $total	= $this->dm->dtcount_prod();
        $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_prod_tahunlalu($param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl)
            $ly[$thl->nama_produk] = $thl;

        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_prod_stock($param2)->result();
        $stok = array();
        foreach($stock as $s)
            $stok[$s->nama_produk] = $s;

        foreach($result as $row)
        {
            if(!empty($produk) and in_array(strtolower($row->nama_produk), $produk))
                $selected = 'checked';
            else
                $selected = '';

            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->nama_produk]) ? $ly[$row->nama_produk]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->nama_produk]) ? $ly[$row->nama_produk]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;

            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_prod" value="'.strtolower($row->nama_produk).'" '.$selected.'>',
                strtoupper($row->nama_produk),
                strtoupper($row->kode_barang),
                y_num_pad($row->target_qty),
                y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),
                !empty($stok[$row->nama_produk]) ? y_num_pad($stok[$row->nama_produk]->qty_stok) : '0',
                !empty($stok[$row->nama_produk]) ? y_num_pad($stok[$row->nama_produk]->value_stok) : '0',
                $this->persen($row->achievement),
                $this->persen($row->achievement_qty),
                $this->persen($growth),
                $this->persen($growth_qty)
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_group_layanan()
    {
        if(!$this->input->is_ajax_request()) return false;

        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(
            array( 'db' => 'layanan', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_insight_target_realisasi_produk_layanan';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $result = $this->dm->dtquery_layanan_group($param)->result();
        $filter = $this->dm->dtfiltered_layanan();
        $total	= $this->dm->dtcount_layanan();
        $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_layanan_group_tahunlalu($param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl)
            $ly[$thl->layanan] = $thl;

        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_layanan_group_stock($param2)->result();
        $stok = array();
        foreach($stock as $s)
            $stok[$s->layanan] = $s;

        foreach($result as $row)
        {
            if(!empty($layanan) and in_array(strtolower($row->layanan), $layanan))
                $selected = 'checked';
            else
                $selected = '';

            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->layanan]) ? $ly[$row->layanan]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->layanan]) ? $ly[$row->layanan]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;
            
            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_group_layanan" value="'.strtolower($row->layanan).'"  '.$selected.'>',
                strtoupper($row->layanan),
                y_num_pad($row->target_qty),
                y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),
                !empty($stok[$row->layanan]) ? y_num_pad($stok[$row->layanan]->qty_stok) : '0',
                !empty($stok[$row->layanan]) ? y_num_pad($stok[$row->layanan]->value_stok) : '0',
                $this->persen($row->achievement),
                $this->persen($row->achievement_qty),
                $this->persen($growth),
                $this->persen($growth_qty)
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_layanan()
    {
        $columns = array(
            array( 'db' => 'nama_layanan', 'dt' => 0 ),
            array( 'db' => 'target_qty', 'dt' => 1 ),
            array( 'db' => 'target', 'dt' => 2 ),
            array( 'db' => 'realisasi_qty', 'dt' => 3 ),
            array( 'db' => 'realisasi', 'dt' => 4 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
            array( 'db' => 'realisasi_1', 'dt' => 6 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
            array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
            array( 'db' => 'achievement', 'dt' => 9 ),
            array( 'db' => 'achievement', 'dt' => 10 ),
            array( 'db' => 'growth', 'dt' => 11 ),
            array( 'db' => 'growth', 'dt' => 12 )
        );

        $start = $this->input->post('startx');
        $end = $this->input->post('end');

        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('area');
        $layanan2 = $this->input->post('dlayanan');
        $brand = $this->input->post('brand');
        $produk = $this->input->post('produk');
        $kftd = $this->input->post('kftd');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();
        $param2 = $param;
        $table  = 'usc_insight_target_realisasi_produk_layanan_detail';

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        $w = "tahun = '".(!empty($s[1]) ? $s[1]-1 : date('Y')-1)."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param['where'] .= $this->parse_area('provinsi', $provinsi);
        $param['where'] .= $this->parse('lini', $lini);
        $param['where'] .= $this->parse('nama_brand', $brand);
        $param['where'] .= $this->parse('nama_produk', $produk);
        $param['where'] .= $this->parse('layanan', $layanan);
        $param['where'] .= $this->parse('nama_layanan', $layanan2);
        $param['where'] .= $this->parse('nama_kftd', $kftd);

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $result = $this->dm->dtquery_layanan($param)->result();
        $filter = $this->dm->dtfiltered_layanan_group($param);
        $total	= $this->dm->dtcount_layanan_group();
        $output = $this->datatables->output($total, $filter);

        $tahunlalu = $this->dm->dtquery_layanan_tahunlalu($param2)->result();
        $ly = array();
        foreach($tahunlalu as $thl)
            $ly[$thl->nama_layanan] = $thl;

        $w = "(tahun <= ".(!empty($e[1]) ? $e[1] : date('Y'))." and bulan <= ".(!empty($e[0]) ? $e[0] : date('m')).")";
        $param2 = array();
        if(empty($param2['where'])) {
            $param2['where'] = 'WHERE '.$w;
        } else {
            $param2['where'] .= ' AND '.$w;
        }

        $param2['where'] .= $this->parse_area('provinsi', $provinsi);
        $param2['where'] .= $this->parse('lini', $lini);
        $param2['where'] .= $this->parse('nama_brand', $brand);
        $param2['where'] .= $this->parse('nama_produk', $produk);
        $param2['where'] .= $this->parse('layanan', $layanan);
        $param2['where'] .= $this->parse('nama_layanan', $layanan2);
        $param2['where'] .= $this->parse('nama_kftd', $kftd);

        $stock = $this->dm->dtquery_layanan_stock($param2)->result();
        $stok = array();
        foreach($stock as $s)
            $stok[$s->nama_layanan] = $s;

        foreach($result as $row)
        {
            if(!empty($layanan2) and in_array(strtolower($row->nama_layanan), $layanan2))
                $selected = 'checked';
            else
                $selected = '';

            $r = !empty($row->realisasi) ? $row->realisasi : 0;
            $r1 = !empty($ly[$row->nama_layanan]) ? $ly[$row->nama_layanan]->realisasi : 0;
            $growth = $r > 0 ? ($r-$r1)/$r : 0;

            $rq = !empty($row->realisasi_qty) ? $row->realisasi_qty : 0;
            $rq1 = !empty($ly[$row->nama_layanan]) ? $ly[$row->nama_layanan]->realisasi_qty : 0;
            $growth_qty = $rq > 0 ? ($rq-$rq1)/$rq : 0;
          
            $rows = array (
                '<input type="checkbox" class="form-check-input" name="chk_layanan" value="'.strtolower($row->nama_layanan).'" '.$selected.'>',
                strtoupper($row->nama_layanan),
                0, //y_num_pad($row->target_qty),
                0, //y_num_pad($row->target),
                y_num_pad($row->realisasi_qty),
                y_num_pad($row->realisasi),
                y_num_pad($rq1),
                y_num_pad($r1),
                //y_num_pad($row->realisasi_qty_1),
                //y_num_pad($row->realisasi_1),

                !empty($stok[$row->nama_layanan]) ? y_num_pad($stok[$row->nama_layanan]->qty_stok) : '0',
                !empty($stok[$row->nama_layanan]) ? y_num_pad($stok[$row->nama_layanan]->value_stok) : '0',
                0, //$this->persen($row->achievement),
                0, //$this->persen($row->achievement_qty),
                $this->persen($growth),
                $this->persen($growth_qty)
                //$this->persen($row->growth),
                //$this->persen($row->growth_qty)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    //===================================================================================================================

    /*public function json_brand()
    {
        if(!$this->input->is_ajax_request()) return false;

        $var = strtolower(urldecode($this->input->post('var')));
        if(empty($var)) {
            $a = array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array());
            echo json_encode($a);
        } else {
            $columns = array(
                array( 'db' => 'nama_brand', 'dt' => 0 ),
                array( 'db' => 'target_qty', 'dt' => 1 ),
                array( 'db' => 'target', 'dt' => 2 ),
                array( 'db' => 'realisasi_qty', 'dt' => 3 ),
                array( 'db' => 'realisasi', 'dt' => 4 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
                array( 'db' => 'realisasi_1', 'dt' => 6 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
                array( 'db' => 'achievement', 'dt' => 9 ),
                array( 'db' => 'achievement', 'dt' => 10 ),
                array( 'db' => 'growth', 'dt' => 11 ),
                array( 'db' => 'growth', 'dt' => 12 )
            );

            $start = $this->input->post('startx');
            $end = $this->input->post('end');
            $lini 	 = $this->input->post('lini');
            $layanan = $this->input->post('layanan');
            $area = $this->input->post('area');
            $col = $this->input->post('col');

            $s = explode('-', $start);
            $e = explode('-', $end);

            $this->datatables->set_cols($columns);
            $param	 = $this->datatables->query();
            $table  = 'usc_insight_target_realisasi_produk';

            $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
            if(empty($param['where'])) {
                $param['where'] = 'WHERE '.$w;
            } else {
                $param['where'] .= ' AND '.$w;
            }

            if(!empty($area)) {
                $wid = explode(',', $area);
                $area_array = array();
                foreach($wid as $wi) {
                    $prov = $this->id_prov($wi);
                    if(!empty($prov))
                        $area_array[] = "'".strtolower($prov)."'";
                }

                if(!empty($area_array))
                    $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
            }

            if (is_array($layanan)) {
                $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
                $table  = 'usc_insight_target_realisasi_produk_layanan';
            }

            $dlayanan = $this->input->post('dlayanan');
            if (is_array($dlayanan)) {
                $param['where'] .= " AND (lower(layanan_name) in ".str_replace("_"," ",strtolower("('".implode("','",$dlayanan)."')")).")";
                $table  = 'usc_insight_target_realisasi_produk_layanan_detail';
            }

            if($col == 'lini') {
                $param['where'] .= " AND (lower(lini_desc1)='".strtolower($var)."')";
            } else {
                if (is_array($lini)){
                    $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
                }
                $param['where'] .= " AND (lower(nama_kftd)='".strtolower($var)."')";
            }

            $result = $this->dm->dtquery_brand($table, $param)->result();
            $filter = $this->dm->dtfiltered_brand();
            $total	= $this->dm->dtcount_brand();
            $output = $this->datatables->output($total, $filter);

            foreach($result as $row)
            {
                $rows = array (
                    strtoupper($row->nama_brand),
                    y_num_pad($row->target_qty),
                    y_num_pad($row->target),
                    y_num_pad($row->realisasi_qty),
                    y_num_pad($row->realisasi),
                    y_num_pad($row->realisasi_qty_1),
                    y_num_pad($row->realisasi_1),
                    0,
                    0,
                    $this->persen($row->achievement),
                    $this->persen($row->achievement_qty),
                    $this->persen($row->growth),
                    $this->persen($row->growth_qty)
                );

                $output['data'][] = $rows;
            }

            echo json_encode( $output );
        }
    }

    public function json_prod()
    {
        if(!$this->input->is_ajax_request()) return false;

        $var = strtolower(urldecode($this->input->post('var')));
        $var_brand = strtolower(urldecode($this->input->post('var_brand')));

        if(empty($var)) {
            $a = array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array());
            echo json_encode($a);
        } else {
            $columns = array(
                array( 'db' => 'nama_produk', 'dt' => 0 ),
                array( 'db' => 'target_qty', 'dt' => 1 ),
                array( 'db' => 'target', 'dt' => 2 ),
                array( 'db' => 'realisasi_qty', 'dt' => 3 ),
                array( 'db' => 'realisasi', 'dt' => 4 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 5 ),
                array( 'db' => 'realisasi_1', 'dt' => 6 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 7 ),
                array( 'db' => 'realisasi_qty_1', 'dt' => 8 ),
                array( 'db' => 'achievement', 'dt' => 9 ),
                array( 'db' => 'achievement', 'dt' => 10 ),
                array( 'db' => 'growth', 'dt' => 11 ),
                array( 'db' => 'growth', 'dt' => 12 )
            );

            $start = $this->input->post('startx');
            $end = $this->input->post('end');
            $lini 	 = $this->input->post('lini');
            $layanan = $this->input->post('layanan');
            $area = $this->input->post('area');
            $col = $this->input->post('col');

            $lini = str_replace("_"," ",$lini);
            $layanan = str_replace("_"," ",$layanan);

            $s = explode('-', $start);
            $e = explode('-', $end);

            $this->datatables->set_cols($columns);
            $param	 = $this->datatables->query();
            $table  = 'usc_insight_target_realisasi_produk';

            $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
            if(empty($param['where'])) {
                $param['where'] = 'WHERE '.$w;
            } else {
                $param['where'] .= ' AND '.$w;
            }

            if(!empty($area)) {
                $wid = explode(',', $area);
                $area_array = array();
                foreach($wid as $wi) {
                    $prov = $this->id_prov($wi);
                    if(!empty($prov))
                        $area_array[] = "'".strtolower($prov)."'";
                }

                if(!empty($area_array))
                    $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
            }

            if($col == 'lini') {
                $param['where'] .= " AND (lower(lini_desc1)='".strtolower($var_brand)."')";
            } else {
                if (is_array($lini)){
                    $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
                }
                $param['where'] .= " AND (lower(nama_kftd)='".strtolower($var_brand)."')";
            }

            if (is_array($layanan)) {
                $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
                $table  = 'usc_insight_target_realisasi_produk_layanan';
            }

            $dlayanan = $this->input->post('dlayanan');
            if (is_array($dlayanan)) {
                $param['where'] .= " AND (lower(layanan_name) in ".str_replace("_"," ",strtolower("('".implode("','",$dlayanan)."')")).")";
                $table  = 'usc_insight_target_realisasi_produk_layanan_detail';
            }

            $param['where'] .= " AND (lower(nama_brand)='".strtolower(str_replace("'","\'",$var))."')";

            $result = $this->dm->dtquery_prod($table, $param)->result();
            $filter = $this->dm->dtfiltered_prod();
            $total	= $this->dm->dtcount_prod();
            $output = $this->datatables->output($total, $filter);

            foreach($result as $row)
            {
                $rows = array (
                    strtoupper($row->nama_produk),
                    strtoupper($row->kode_barang),
                    y_num_pad($row->target_qty),
                    y_num_pad($row->target),
                    y_num_pad($row->realisasi_qty),
                    y_num_pad($row->realisasi),
                    y_num_pad($row->realisasi_qty_1),
                    y_num_pad($row->realisasi_1),
                    0,
                    0,
                    $this->persen($row->achievement),
                    $this->persen($row->achievement_qty),
                    $this->persen($row->growth),
                    $this->persen($row->growth_qty)
                );

                $output['data'][] = $rows;
            }

            echo json_encode( $output );
        }
    }*/

    //===================================================================================================================

    public function problem_area()
    {
        $area = $this->input->post('area');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('provinsi');
        $layanan2 = $this->input->post('layanan2');
        $nama_kftd = $this->input->post('kftd');
        $brand = $this->input->post('brand');
        $nama_produk = $this->input->post('produk');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $warea = '';
        if(!empty($area)) {
            $wid = explode(',', $area);
            $area_array = array();
            foreach($wid as $wi) {
                $prov = $this->id_prov($wi);
                if(!empty($prov))
                    $area_array[] = "'".strtolower($prov)."'";
            }

            if(!empty($area_array))
                $warea .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
        }

        $charts = $this->dm->getByAreaGroupMonth($warea, $s[0], $e[0], $s[1], $lini, $layanan,$provinsi,$layanan2,$nama_kftd,$brand,$nama_produk)->result();
        
        // die($this->db->last_query());
        // return ($this->db->last_query());

        $stock = $this->dm->getStockByAreaGroupMonth($warea, $s[0], $e[0], $e[1], $lini, $layanan,$provinsi,$layanan2,$nama_kftd,$brand,$nama_produk)->result();
        // die($this->db->last_query());
        
        $stok = array();
        foreach($stock as $sx)
            $stok[$sx->bulan] = $sx;

        $data['vyear'] = $s[1];
        $data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        foreach($charts as $chart) {
            $blnx = (int) $chart->bulan;

            $data['charts']['month'][$blnx] = $bln[$blnx];
            $data['charts']['realisasi_1'][$blnx] = (int) $chart->realisasi_1;
            $data['charts']['realisasi'][$blnx] = (int) $chart->realisasi;

            $data['charts']['realisasi_qty_1'][$blnx] = (int) $chart->qty1;
            $data['charts']['realisasi_qty'][$blnx] = (int) $chart->qty;

            $data['charts']['target'][$blnx] = (int) $chart->target;
            $data['charts']['target_qty'][$blnx] = (int) $chart->target_qty;

            $data['charts']['stock'][$blnx] = !empty($stok[$blnx]) ? (int) $stok[$blnx]->value_stok : 0;
            $data['charts']['stock_qty'][$blnx] = !empty($stok[$blnx]) ? (int) $stok[$blnx]->qty_stok : 0;
        }

        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($data['charts']['month'][$b])) {
                $data['charts']['month'][$b] = $bln[$b];

                $data['charts']['realisasi_1'][$b] = 0;
                $data['charts']['realisasi'][$b] = 0;

                $data['charts']['realisasi_qty_1'][$b] = 0;
                $data['charts']['realisasi_qty'][$b] = 0;

                $data['charts']['target'][$b] = 0;
                $data['charts']['target_qty'][$b] = 0;

                $data['charts']['stock'][$b] = 0;
                $data['charts']['stock_qty'][$b] = 0;
            }
        }

        echo $this->load->view('insightful/problem_area', $data, true);
    }

    private function parse($col, $array){
        $wlini ='';
        if($array != 'all' AND !empty($array)) {
            $wlini = 'AND (';
            foreach($array as $l){
                    $wlini .= $col." = '".strtoupper($l)."' OR ";
            }
            $wlini = substr($wlini,0,-4);
            $wlini .= ')';
        }
        return $wlini;
    }

    private function parse_area($col, $array){
        $wlini ='';
        if($array != 'all' AND !empty($array)) {
            $wlini .= 'AND (';
            foreach($array as $l){
                $prov = $this->id_prov($l);
                if(!empty($prov)) {
                    $wlini .= $col." = '".strtoupper($prov)."' OR ";
                }
            }
            $wlini = substr($wlini,0,-4);
            $wlini .= ')';
        }
        return $wlini;
    }

    public function problem_maps()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $provinsi = $this->input->post('provinsi');
        $layanan2 = $this->input->post('layanan2');
        $nama_kftd = $this->input->post('kftd');
        $brand = $this->input->post('brand');
        $nama_produk = $this->input->post('produk');
        

        $wlini = $this->parse('lini', $lini);
        $wlayanan = $this->parse('layanan', $layanan);
        $wprovinsi  = $this->parse_area('provinsi',$provinsi);
        $wlayanan2  = $this->parse('nama_layanan',$layanan2);
        $wbrand = $this->parse('nama_brand',$brand);
        $wkftd  = $this->parse('nama_kftd',$nama_kftd);
        $wproduk = $this->parse('nama_produk',$nama_produk);


        $s = explode('-', $start);
        $e = explode('-', $end);

        $dbs = $this->dm->getCountDaerah($s[0], $e[0], $s[1], $wlini, $wlayanan,$wprovinsi,$wlayanan2,$wkftd,$wbrand,$wproduk)->result();
        $array = array();
        foreach($dbs as $db) {
            $id = $this->prov_id($db->provinsi);

            if($db->target > 0)
                $persen = round(($db->realisasi / $db->target)*100);
            else
                $persen = 100;

            $array[] = array($id, $persen);
        }

        echo json_encode($array);
    }

   

    public function marketing_realisasi($start='', $end='', $lini='all')
    {
        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $s = explode('-', $start);
        $e = explode('-', $end);

        $dbs = $this->dm->get_realisasi_expenses($s[0], $e[0], $s[1], str_replace('_', ' ', $lini))->result();

        //$data['area'] = $id;
        //$data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $charts = array();
        $lini_array = array();
        foreach($dbs as $db) {
            if(empty($charts[(int) $db->bulan])) {
                $charts[(int) $db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
            } else {
                $charts[(int) $db->bulan]['expenses'] += (int) $db->expenses;
                $charts[(int) $db->bulan]['realisasi'] += (int) $db->realisasi;
            }

            $lini_array[$db->lini][(int) $db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
        }

        $array = array();
        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($charts[$b])) {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = 0;
                $array['realisasi'][$b] = 0;
                $array['ratio'][$b] = 0;
            } else {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = $charts[$b]['expenses'];
                $array['realisasi'][$b] = $charts[$b]['realisasi']/100;
                $array['ratio'][$b] = !empty($charts[$b]['realisasi']) ? round(($charts[$b]['expenses'] / $charts[$b]['realisasi'])*100, 2) : 0;
            }
        }

        $json[] = array('type' => 'column', 'name' => 'Marketing Expense', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#69d0ef', 'data' => array_values($array['expenses']));
        $json[] = array('type' => 'column', 'name' => 'Realisasi Sales', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#ab82c4', 'data' => array_values($array['realisasi']));
        $json[] = array('type' => 'spline', 'name' => 'Rasio Realisasi Expense', 'yAxis' => 1, 'tooltip' => array('valueSuffix' => ' %'), 'color' => '#db2347', 'data' => array_values($array['ratio']));

        $data['ex_data'] = json_encode($json);
        $data['ex_bln'] = json_encode(array_values($array['month']));
        $data['lini_data'] = $lini_array;
        $data['lini_bln'] = $array['month'];

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];

        $data['var_lini'] = $lini;

        $lini_dbs = $this->dm->get_lini_expanse()->result();
        $data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][str_replace(' ', '_', strtolower($ld->lini))] = strtoupper($ld->lini);
        }

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful/marketing_realisasi';
        $data['title']	= 'Marketing Expense vs Realisasi Sales';

        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful/marketing_realisasi', $data);
        $this->load->view('templates/footer', $data);
    }

    function evalDate(){
        $start = explode("-",$this->input->post('start'));
        $end = explode("-",$this->input->post('end'));
        $result = [];

        if($start[1] !== $end[1]){
            $result = [
                "result" => 0,
                "message" => "Tidak Bisa Lintas Tahun"
            ];
            
        }else{
            if($start[0] > $end[0]){
                $result = [
                    "result" => 0,
                    "message" => "Tanggal Start Lebih Dari Tanggal End"
                ];
            }else if($start[0] <= $end[0]){
                $result = [
                    "result" => 1,
                    "message" => "-"
                ];
            }else{
                $result = [
                    "result" => 0,
                    "message" => "Input Salah"
                ];
            }
        }

        echo json_encode($result);
    }
}

?>