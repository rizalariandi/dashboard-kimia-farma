<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boosting extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
    }
	
	// BOOSTING SALES - AREA CHANNEL
	
	public function area_channel()
	{
		$data["title"] = "Area Channel";
		$data["list_lini"] = $this->Boosting_model->area_channel_get_list_lini();
		$data["list_layanan"] = $this->Boosting_model->area_channel_get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
		}

			if($data["provinsi"] != ""){
				$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_channel')->result();
			}
		if($data["data_range"] != ''){

			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		}else{
			$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			$data['startDate'] = "2017/01/01";
			$data['endDate'] = date("Y/m/d");
		}
	
		if($data["areaf"] == "channel-area"){
			$data["kftd"] = $this->Boosting_model->area_channel_get_kftd();
		}else{
			$data["kftd"] = $this->Boosting_model->area_channel_get_kftd_channel();
		}
		$data['mapdata_area_channel'] =$this->populate_provinsi_area_channel($data["data_lini"],$data["data_layanan"]);

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/area_channel', $data);
		$this->load->view('templates/footer', $data);

	}
	public function populate_provinsi_area_channel($lini,$layanan){
		
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'Aceh', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		); 
		  
		$total = 0;
		foreach($array_kode_iso as $key=>$val){
			$total += $this->Boosting_model->area_channel_get_revenue(strtoupper($val['name']),$lini,$layanan);
		}

		$array_datas = array();
		foreach($array_kode_iso as $key=>$val){
			$value = round(($this->Boosting_model->area_channel_get_revenue(strtoupper($val['name']),$lini,$layanan) / $total)*100, 2);
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$value));
		}

		return json_encode($array_datas);
	}
	public function json_channel_area(){
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_channel_area();
	}
	public function json_area_channel(){
		header('Content-Type: application/json');
		$data = $this->Boosting_model->json_channel_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_area_channel());
		echo str_replace("I.N.H'", "INH", $data);
	}
	// END AREA CHANNEL

	// BOOSTING SALES - BRAND AREA -update
	public function brand_area()
	{ 
		$data["list_lini"] = $this->Boosting_model->get_list_lini();
		$data["list_layanan"] = $this->Boosting_model->get_list_layanan();
		$data["list_produk"] = $this->Boosting_model->get_list_produk();
		$data["list_brand"] = $this->Boosting_model->get_list_brand();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["data_brand"] = $this->input->post("brand", true);
		$data["data_produk"] = $this->input->post("produk", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
			
		}

			if($data["provinsi"] != ""){
				$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
			}
		

		if($data["data_range"] != ''){
			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		}else{
			$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			$data['startDate'] = "2017/01/01";
			$data['endDate'] = date("Y/m/d");
		}
		
		if($data["areaf"] == "brand-area"){
			$data["kftd"] = $this->Boosting_model->get_kftd();
		}else{
			$brand = $this->input->post("brand", true);
			$produk = $this->input->post("produk", true);
			if($brand != "" || $produk != ""){
				$data["kftd"] = $this->Boosting_model->get_kftd_brand2();
			}else{
				$data["kftd"] = $this->Boosting_model->get_kftd_brand();

			}
		}
		$data['mapdata'] = $this->populate_provinsi();
		
		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/brand_area', $data);
		$this->load->view('templates/footer', $data);
	}

	public function brand_area_excel(){
		// $header = $this->db->list_fields('usc_bst_sales_area_brand');
        // $header[0] = "No";
        // $this->tamaexcel->export_data('Brand - Area', $header, 'provinsi', $this->Boosting_model->brand_area_excel());
		$areaf = $this->input->post("area");
		if($areaf == ""){
			$areaf = "brand-area";
		}
		if($areaf == "brand-area"){
			$data = $this->Boosting_model->get_excel_brand_area();
		}else{
			$data = $this->Boosting_model->get_excel_area_brand();
		}
		// var_dump($data[0]);
		$header = array();
		foreach($data[0] as $key => $value){
			$header[] = $key;
		}
		// var_dump($header);
		// echo "=======================";
		
		// var_dump($data);
		// echo "Hello";
		// $i = 0;
		$arrtotal = array();
		foreach($data as $val){
			$total = 0;
			$i = 0;
            foreach($header as $head){
				// echo $val->$head."\n";
				if($i>0){
					$total += $val->$head;
				}
				$i++;
                // $values[] = $this->setpercen($i, $data, $val->$head);
			} 
			$arrtotal[] = $total;
		}
		var_dump($arrtotal);
		echo "============";
		var_dump($data);

		// $this->tamaexcel->exportexcel($header, $data, $arrtotal, $areaf);

	}

	
	public function populate_provinsi(){
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'N ACEH DARUSSALAM', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		   ); 
		  
		   $array_datas = array();
		   foreach($array_kode_iso as $key=>$val){
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$this->Boosting_model->get_revenue(strtoupper($val['name']))));
		   }
		   return json_encode($array_datas);
	}

	public function json_brand_area(){
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_brand_area();
	}

	public function json_area_brand(){	
		header('Content-Type: application/json');
		// $data = $this->Boosting_model->json_brand_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_area_brand());
		echo str_replace("I.N.H'", "INH", $data);
	}

	// END BRAND AREA

		// BOOSTING SALES - BRAND AREA2 -update
		public function brand_area2()
		{ 
			$data["list_lini"] = $this->Boosting_model->get_list_lini();
			$data["list_layanan"] = $this->Boosting_model->get_list_layanan();
			$data["list_produk"] = $this->Boosting_model->get_list_produk();
			$data["list_brand"] = $this->Boosting_model->get_list_brand();
			$data["data_range"] = $this->input->post("range", true);
			$data["data_lini"] = $this->input->post("lini", true);
			$data["data_layanan"] = $this->input->post("layanan", true);
			$data["data_brand"] = $this->input->post("brand", true);
			$data["data_produk"] = $this->input->post("produk", true);
			$data["areaf"] = $this->input->post("area", true);
			$data["provinsi"] = $this->input->post("provinsi", true);
			if($data["areaf"] == ""){
				$data["areaf"] = "brand-area";
				
			}
	
				if($data["provinsi"] != ""){
					$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
				}
			
	
			if($data["data_range"] != ''){
				$tanggal = str_replace(" ","",$data['data_range']);
				$tanggal = str_replace("/",",",$tanggal);
				$tgl =  explode(",", $tanggal);
				$data['startDate'] = $tgl[0];
				$data['endDate'] = $tgl[1];
			}else{
				$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
				$data['startDate'] = "2017/01/01";
				$data['endDate'] = date("Y/m/d");
			}
			
			if($data["areaf"] == "brand-area"){
				$data["kftd"] = $this->Boosting_model->get_kftd();
			}else{
				$brand = $this->input->post("brand", true);
				$produk = $this->input->post("produk", true);
				if($brand != "" || $produk != ""){
					$data["kftd"] = $this->Boosting_model->get_kftd_brand2();
				}else{
					$data["kftd"] = $this->Boosting_model->get_kftd_brand();
	
				}
			}
			$data['mapdata'] = $this->populate_provinsi();
			
			$data['menu'] = $this->user_model->getMenu();
			$this->load->view('templates/header', $data);
			$this->load->view('boosting/brand_area2', $data);
			$this->load->view('templates/footer', $data);
		}
	
		public function brand_area_excel2(){
			// $header = $this->db->list_fields('usc_bst_sales_area_brand');
			// $header[0] = "No";
			// $this->tamaexcel->export_data('Brand - Area', $header, 'provinsi', $this->Boosting_model->brand_area_excel());
			$areaf = $this->input->post("area");
			if($areaf == ""){
				$areaf = "brand-area";
			}
			if($areaf == "brand-area"){
				$data = $this->Boosting_model->get_excel_brand_area();
			}else{
				$data = $this->Boosting_model->get_excel_area_brand();
			}
			// var_dump($data[0]);
			$header = array();
			foreach($data[0] as $key => $value){
				$header[] = $key;
			}
			// var_dump($header);
			// echo "=======================";
			
			// var_dump($data);
			// echo "Hello";
			// $i = 0;
			$arrtotal = array();
			foreach($data as $val){
				$total = 0;
				$i = 0;
				foreach($header as $head){
					// echo $val->$head."\n";
					if($i>0){
						$total += $val->$head;
					}
					$i++;
					// $values[] = $this->setpercen($i, $data, $val->$head);
				} 
				$arrtotal[] = $total;
			}
			var_dump($arrtotal);
			echo "============";
			var_dump($data);
	
			// $this->tamaexcel->exportexcel($header, $data, $arrtotal, $areaf);
	
		}
	
		
		public function populate_provinsi2(){
			$array_kode_iso = array(
				array('iso'=>'ID-AC','name'=>'N ACEH DARUSSALAM', 'code'=>11),
				array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
				array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
				array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
				array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
				array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
				array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
				array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
				array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
				array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
				array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
				array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
				array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
				array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
				array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
				array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
				array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
				array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
				array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
				array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
				array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
				array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
				array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
				array('iso'=>'ID-KU','name'=>'Kalimantan Utara', 'code'=>65),
				array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
				array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
				array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
				array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
				array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
				array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
				array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
				array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
				array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
				array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
			   ); 
			  
			   $array_datas = array();
			   foreach($array_kode_iso as $key=>$val){
				array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$this->Boosting_model->get_revenue(strtoupper($val['name']))));
			   }
			   return json_encode($array_datas);
		}
	
		public function json_brand_area2(){
			header('Content-Type: application/json');
			echo $this->Boosting_model->json_brand_area();
		}
	
		public function json_area_brand2(){	
			header('Content-Type: application/json');
			// $data = $this->Boosting_model->json_brand_area();
			$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_area_brand());
			echo str_replace("I.N.H'", "INH", $data);
		}
	
		// END BRAND AREA

	
	
// BOOSTING SALES - BRAND CHANNEL
function random_color_part() {
	return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
	return random_color_part() . random_color_part() . random_color_part();
}

public function brand_channel()
{ 	

	$lini = $this->input->post("lini", true);
	$area = $this->input->post("provinsi", true);
	// if($area==""){
	// 	$area="DKI JAKARTA";
	// }

	$produk = $this->input->post("produk", true);
	$data['provinsi']=$this->input->post("provinsi", true);
	$data['produk_filter']=$this->input->post("produk", true);
	$data['lini_data']=$this->input->post("lini", true);
	$data['data_range']=$this->input->post("range", true);
	$data['produk']=$this->input->post("produk", true);

	// if($data["produk"] != ""){
	// 	$data["produk_bc"] = $this->db->distinct()->select('nama_brand')->like('provinsi', $data['provinsi'])->get('usc_bst_sales_brand_channel')->result();
	// }
	if($data['data_range'] != ''){
			$tanggal=str_replace(" ", "", $data['data_range']);
			$tanggal=str_replace("/", ",", $tanggal);
			$tgl=explode(",", $tanggal);
			$data['startDate']=$tgl[0];
			$data['endDate']=$tgl[1];
	}else{
		$data["data_range"]=date("2017-m-d").'/'.date("Y-m-d");
		$data['startDate']="2018/01/01";
		$data['endDate']="2019/12/30";
	}
	// if($lini != 0){
	// $data_lini=implode(' ', $lini);	
	if(is_array($lini)){	
		$lini_data=$lini;	
	}else{
		$lini_data=explode(',', $lini);
	}
	if(is_array($produk)){	
		$data_produk=$produk;	
	}else{
		$data_produk=explode(',', $produk);
	}
	// var_dump($lini_data);

	// }
	

	$arrwarna = array();
	$produk = $this->input->post("produk", true);

	$this->db->distinct()->select('channel');
	$this->db->group_start();
	$this->db->like('nama_brand', $produk);
	$this->db->group_end();
	$datachannel = $this->db->get('usc_bst_sales_brand_channel')->result();
	
	foreach($datachannel as $channel){
		$arrwarna[$channel->channel] = "#".$this->random_color();
	}
	$data['data_channel']=$datachannel;
	$data['arrWarna']=$arrwarna;
	
	$this->db->select("`channel`,`nama_kftd`, SUM(`revenue`) AS nowyear, SUM(`revenue_1`) AS lastyear");
	$this->db->group_start();
	if(sizeof($lini_data) != 0){
		// var_dump($lini_data);
		foreach ($lini_data as $lin) {
			$this->db->or_like('lini', $lin);
		}
	}	


	$this->db->group_end();

	$this->db->group_start();
	$this->db->like('nama_kftd', $area);
	$this->db->group_end();
	$this->db->group_start();
	if(sizeof($data_produk) != 0){
		foreach ($data_produk as $dat) {
			$this->db->or_like('nama_brand', $dat);
		}
	}	
	$this->db->group_end();
	
	$this->db->group_by("channel");
	// $this->db->order_by("nama_customer asc, channel asc");
	// $this->db->limit(3000);
	$datas = $this->db->get("usc_bst_sales_brand_channel")->result();

	// var_dump($datas);
	// die();

	$kftds = $this->db->select("nama_kftd, SUM(revenue) AS total")->group_by("nama_kftd")->get("usc_bst_sales_brand_channel")->result();


	// var_dump($datas);
	$myarr = array();
	$x=0;
	$y=0;
	
	$strseries = "";
	foreach($kftds as $kftd){
	foreach($datas as $datanya){
		if($datanya->nama_kftd == $kftd->nama_kftd){
			if($datanya->lastyear == 0 && $datanya->nowyear > 0){
				$x = 100;
			}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
				$x = -100;
			}else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
				$x = 0;
			}else{
				if($datanya->lastyear>0 && $datanya->nowyear>0){
				$x = round(( $datanya->nowyear - $datanya->lastyear ) / $datanya->lastyear, 3);
				}else{
					$x=0;
				}
			}

			if($datanya->nowyear == 0 && $datanya->lastyear == 0){
				$y = 0;
			}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
				$y= -100;
			}else if($datanya->lastyear == 0 && $datanya->nowyear>0){
				$y= 100;
			}
			else{
				if($datanya->lastyear>0 && $datanya->nowyear>0){
					$y = round(($datanya->nowyear / $kftd->total) * 100, 3);
					
					// if($y>100){
					// 	$y=100;
					// }else if($y<-100){
					// 	$y=-100;
					// }else{
					// 	$y = round(($datanya->nowyear / $kftd->total) * 100, 3);
					// }
				} else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
					$y=0;
				}

			}


				$strseries .= "{\n";
				$strseries .= "name : \"".$datanya->channel."\",";
				$strseries .= "showInLegend : false,";
				$strseries .= "marker : {\n";
				$strseries .= "symbol : \"circle\",\n";
				$strseries .= "},";
				// $strseries .= "labelFormat : \"".$datanya->channel."\",";
				$strseries .= "\ndata : [{\n";
				$strseries .= "x : ".$x.",\n";
				$strseries .= "y : ".$y.",\n";
				$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
				$strseries .= "}]";	
				$strseries .= "},";
						
			if(empty($datanya->channel)){
				$strseries .= "{\n";
					$strseries .= "name : \"".$datanya->channel." - ".$datanya->nama_customer."\",";
					$strseries .= "showInLegend : false,";
					$strseries .= "marker : {\n";
					$strseries .= "symbol : \"circle\",\n";
					$strseries .= "},";
					// $strseries .= "labelFormat : \"".$datanya->channel."\",";
					$strseries .= "\ndata : [{\n";
					$strseries .= "x : ".null.",\n";
					$strseries .= "y : ".null.",\n";
					$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
					$strseries .= "}]";	
					$strseries .= "},";
			}
		}
		// array_push($series, $temparr);

		}
	}

	$data["scatter_series"] = substr_replace($strseries, "", -1);
	// error_reporting(0);
	// var_dump($strseries);
	
	$data["list_lini"] = $this->Boosting_model->get_lini_list();
	$data["list_area"] = $this->Boosting_model->get_area_list();
	$data["list_produk"] = $this->Boosting_model->get_produk_list();
	// $data["list_layanan"] = $this->Boosting_model->get_list_layanan();
	$data["data_range"] = $this->input->post("range", true);
	$data["data_lini"] = $this->input->post("lini", true);
	$data["data_area"] = $this->input->post("provinsi", true);
	$data["data_produk"] = $this->input->post("produk", true);

	// $data["data_layanan"] = $this->input->post("layanan", true);
	$data["channelf"] = $this->input->post("channel", true);
	$data["cus_data"] = $this->Boosting_model->get_cus_brandc();
	$cus_data= $this->Boosting_model->get_cus_brandc();
	$channel= $this->Boosting_model->get_chan();

	$data["channel"] = $this->input->post("channel", true);
	if($data["channelf"] == ""){
		$data["channelf"] = "brand-channel";
	}
	$tanggal = str_replace(" ","",$data['data_range']);
	$tanggal = str_replace("/",",",$tanggal);
	$tgl =  explode(",", $tanggal);
	// var_dump($tgl);
	//   die();
	if($data["channelf"] == "brand-channel"){
		$data["chan"] = $this->Boosting_model->get_chan();
	}else{
		$data["chan"] = $this->Boosting_model->get_chan_brand();
	}
	// print_r($data['chan']);

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/brand_channel', $data);
		$this->load->view('templates/footer', $data);
}

public function json_brand_channel()
{



	header('Content-Type: application/json');
	// var_dump($this->Boosting_model->json_brand_channel());
	// die();
	echo $this->Boosting_model->json_brand_channel();
}

public function json_channel_brand()
{
	header('Content-Type: application/json');
	// $data = $this->Boosting_model->json_brand_channel();
	$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_channel_brand());
	echo str_replace("I.N.H'", "INH", $data);
}

public function json_scatter()
{
	header('Content-Type: application/json');
	$data['scatter_data']= $this->Boosting_model->json_brand_channel_scatter();
	// var_dump($data);
}

	//BOOSTING SALES - OUTLET OTR
	public function outlet_otr()
	{
		$area = $this->input->post('area');
		$channel = $this->input->post('channel');

		$data["data_area"] = $this->input->post("area_table", true);
		$data["data_channel"] = $this->input->post("channel_table", true);
		$data["periode_table_outlet"] = $this->input->post("periode_table_outlet", true);
	
		if (empty($data["periode_table_outlet"])) {
			$data["periode_table_outlet"] = date('Y');
		}

		$data_otr = $this->Boosting_model->outlet_otr($area, $channel);
		$tahun_otr = $this->Boosting_model->get_tahun_otr('usc_bst_sales_outlet_channel');
		$json_data_otr = array();
		
		$nama_brand = $this->Boosting_model->get_nama_brand('usc_bst_sales_outlet_channel');
		$list_brand = $this->Boosting_model->get_brand_outlet('usc_bst_sales_outlet_channel');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		// $bulan = $this->db->distinct()->select('month(tanggal) as bulan')->where('year(tanggal)', $data["periode_table_outlet"])->get('usc_bst_sales_brand_channel')->result();
		$bulan = $this->db->distinct()->select('month(tanggal) as bulan')->get('usc_bst_sales_brand_channel')->result();
		
		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$outlet = $this->Boosting_model->json_outlet_otr();
		$trend = $this->Boosting_model->json_outlet_trend();
		$dec = json_decode($outlet);
		$decode_trend = json_decode($trend);
		$last_update = $this->Boosting_model->get_last_update();
		$data_otr=json_encode($dec->data, JSON_NUMERIC_CHECK);
		$lini =$this->Boosting_model->get_lini_otr('usc_bst_sales_brand_channel');
		$channels = $this->Boosting_model->get_channels('usc_bst_sales_outlet_channel');
		// var_dump($last_update[0]->tanggal);
		// die();
		// var_dump($dec->nama_brand);
		// die();
		// var_dump(implode(',',$dec->nama_brand));

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Outlet OTR',
			'bulan' => $bulan,
			'tahun' => $tahun,
			'tahun_otr' => $tahun_otr,
			'provinsi' => $this->Boosting_model->get_provinsi('usc_bst_sales_outlet_channel'),
			'area' => $area,
			'areas' => $this->Boosting_model->get_list_kftd('usc_bst_sales_outlet_channel'),
			'channel' => $channel,
			'channels' => $channels,
			'lini' => $lini,
			'linis' => $this->Boosting_model->get_lini_otr('usc_bst_sales_outlet_channel'),
			'brand' => $this->Boosting_model->get_brand_otr('usc_bst_sales_brand_channel'),
			'list_brand' => $list_brand,
			'nama_brand_otr' => json_encode($dec->nama_brand),
			'data_otr' => $data_otr,
			'trend_this_year' => json_encode($this->Boosting_model->trend_outlet($area, $channel, $year = date("Y")),JSON_NUMERIC_CHECK),
			'trend_last_year' => json_encode($this->Boosting_model->trend_outlet($area, $channel, $year - 1), JSON_NUMERIC_CHECK),
			'trend' => json_encode($decode_trend->data, JSON_NUMERIC_CHECK),
			'cat_month' => json_encode($decode_trend->bulan),
			'last_update' => $last_update[0]->tanggal
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/outlet_otr', $data);
        $this->load->view('templates/footer', $data);
	}

	public function json_outlet_brand()
	{
		$this->load->model('Otr_model');
		header('Content-Type: application/json');
		// echo $this->Boosting_model->json_outlet_brand();
		echo $this->Otr_model->json_outlet_brand();
	}

	public function json_outlet_area()
	{
		$this->load->model('Otr_model');
		header('Content-Type: application/json');
		// echo $this->Boosting_model->json_outlet_area();
		echo $this->Otr_model->json_outlet_area();
	}

	public function json_outlet_otr()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_otr();
	}

	public function json_outlet_trend()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_trend();
	}

	public function json_outlet_product(){
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_product();
	}

	public function json_testing()
	{
		header('Content-Type: application/json');
		// echo json_encode($this->Boosting_model->get_bcg_matrix('usc_bst_product_mart','JAWA BARAT','FITUNO'));	
		echo $this->Boosting_model->get_bcg_matrix('usc_bst_product_mart','JAWA TENGAH','FITUNO');
	}

	public function growth_testing()
	{
		header('Content-Type: application/json');
	
		var_dump( linear_regression(array(7, 8, 9, 10), array(1045010, 21767950, 41470940, 5905033)) );
	}

	public function json_get_product()
	{
		header('Content-Type: application/json');
		$nama_brand = $this->input->post('nama_brand');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		$tahun_otr = $this->Boosting_model->get_tahun_otr('usc_bst_sales_transaksi_area');
		$json_data_otr = array();
		$data_otr= $this->Boosting_model->get_product($nama_brand);

		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$json_data = array(
			'data' => $json_data_otr,
			'categories' => $this->Boosting_model->get_list_product($nama_brand)
		);
		echo json_encode($json_data);
	}

	public function json_get_list_product()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_get_list_product();
	}

	public function json_get_growth_product()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_get_growth_product();
	}

	public function json_outlet_transaction()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_transaction();
	}

	// END OUTLET OTR

	public function json_get_sku()
	{
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$brand = $this->input->post("brand", true);
		$sku = $this->Boosting_model->get_sku($table, $brand);
		foreach ($sku as $list) {
			echo '<option value="' . $list->nama_produk . '">' . $list->nama_produk . "</option>";
		}
	}

	public function json_get_brand()
	{
		// header('Content-Type: application/json');
		$table = $this->input->post("table", true);
		$lini = $this->input->post("lini", true);
		$brand_by_lini = $this->Boosting_model->get_brand($table, $lini);
		foreach ($brand_by_lini as $list) {
			echo '<option value="' . $list->nama_brand . '">' . $list->nama_brand . "</option>";
		}
	}

	//BOOSTING PRODUCT
	public function bcg_matrix_channel()
	{
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

		if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}
        		
		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		if(empty($filter)){
			$filter = 'value';
		}

		// $bcg = $this->Boosting_model->get_bcg_channel('usc_bst_channel_mart', $area, $produk);
		$check = $this->Boosting_model->check_data('usc_bst_channel_mart', $area, $produk, $from, $to);
		
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}
		$bcg = $this->Boosting_model->get_bcg_matrix_channel('usc_bst_channel_mart', $area, $produk, $filter, $from, $to);
		$export_channel = $this->Boosting_model->get_data_export_channel('usc_bst_channel_mart',$area, $produk, $from, $to);
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFTD Channel',
			'area' => $area,
			'areas' => $this->Boosting_model->get_area('usc_bst_channel_mart'),
			'produk' => $produk,
			'produks' => $this->Boosting_model->get_produk_channel('usc_bst_channel_mart'),
			'bcg' => $bcg,
			'opsi' => $filter,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'notif' => $notif,
			'export_channel' => $export_channel
		);

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_channel_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_channel_quantity', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_channel_value', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_channel_value', $area, $produk);
		}
			
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix_channel', $data);
        $this->load->view('templates/footer', $data);
	}
	function get_kompetitor(){
        $produk=$this->input->post('produk');
		// $data=$this->m_kategori->get_subkategori($id);
		$kompetitor = $this->Boosting_model->get_kompetitor('usc_bst_product_mart',$produk);
		
        echo json_encode($kompetitor);
    }

	public function bcg_matrix(){
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$kompetitor = $this->input->post('kompetitor');
		$filter = $this->input->post('filter');

		$range = $this->input->post("period", true);
		if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}
		if(empty($kompetitor)){
			$kompetitor = 'imunos';
		}

		if(empty($filter)){
			$filter = 'value';
		}

		$check = $this->Boosting_model->check_data('usc_bst_product_mart', $area, $produk, $from, $to);
		// $checkbaru = $this->Boosting_model->check_data_baru('usc_bst_product_mart', $area, $produk, $from, $to);
		// var_dump($check);
		// die();
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}

		// $bcg = $this->Boosting_model->get_bcg('usc_bst_product', $area, $produk);

		$bcg = $this->Boosting_model->get_bcg_matrix('usc_bst_product_mart', $area, $produk, $filter, $from, $to);
		// var_dump($bcg);
		// die();
		$sell_out = $this->Boosting_model->sell_out_kfa('usc_bst_product_mart', $area, $produk, $filter, $from, $to);
		$data_raw = $this->Boosting_model->data_sell_out('usc_bst_product_mart', $area, $produk, $filter, $from, $to);
		$chart_sell_out = json_decode($sell_out);
		$title_chart = '';
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}

		$export_product = $this->Boosting_model->get_data_export('usc_bst_product_mart',$area, $produk, $from, $to);
		$export_variant = $this->Boosting_model->get_data_export('usc_bst_product_details_mart',$area, $produk, $from, $to);

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boosting_model->get_area('usc_bst_product_mart'),
			'produk' => $produk,
			'produks' => $this->Boosting_model->get_produk('usc_bst_product_mart'),
			'bcg' => $bcg,			
			'bcg_rekomendasi' => $this->Boosting_model->get_bcg_rekomendasi('usc_bst_product_mart', $area, $produk, $filter, $from, $to),
			'bcg_detils' => $this->Boosting_model->get_bcg_matrix('usc_bst_product_details_mart', $area, $produk, $filter, $from, $to),
			'bcg_detils_rekomendasi' => $this->Boosting_model->rekomendasi_bcg_detils('usc_bst_product_details_mart', $area, $produk, $filter, $from, $to),
			'cat' => $chart_sell_out->categories,
			'data_sell_out' => $chart_sell_out->data,
			'title_sell_out' => $title_chart,
			'opsi' => $filter,
			'notif' => $notif,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'export_product' => $export_product,
			'export_variant' => $export_variant,
			'data_raw' => $data_raw
		);

		// var_dump($data_raw);
		// die();

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_quantity', $area, $produk);
			$data['bcg_detils_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
			$data['bcg_detils_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_value', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_value', $area, $produk);
			$data['bcg_detils_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_details_value', $area, $produk);
			$data['bcg_detils_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_details_value', $area, $produk);
		}
		
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix', $data);
        $this->load->view('templates/footer', $data);
	}

	public function bcg_matrix_kompetitor()
   {
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$kompetitor = $this->input->post('kompetitor');
        $filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

        if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2019-m-d").' / '.date("Y-m-d");
			$startDate = "2019/07/01";
			$endDate = date("Y/m/d");
			$from = $startDate;
			$to = $endDate;
		}

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		if(empty($kompetitor)){
			$kompetitor = 'IMUNOS';
		}

		if(empty($filter)){
			$filter = 'value';
        }
        $check = $this->Boosting_model->check_data('usc_bst_product_mart', $area, $produk, $from, $to);
        // $check_kompetitor = $this->Boosting_model->check_data_kompetitor('usc_bst_product_mart', $area, $kompetitor, $from, $to);
		// $checkbaru = $this->Boosting_model->check_data_baru('usc_bst_product_mart', $area, $produk, $from, $to);
		// var_dump($check);
		// die();
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}

        $bcg = $this->Boosting_model->get_bcg_matrix_kompetitor('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		// var_dump($bcg);
		$bcg_rekomendasi = $this->Boosting_model->get_bcg_rekomendasi_kompetitor('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
		// var_dump($bcg_rekomendasi);
		$bcg_detils = $this->Boosting_model->get_bcg_matrix_detils_kompetitor('usc_bst_product_details_mart', $area, $produk, $kompetitor, $filter, $from, $to);
		// var_dump($bcg_detils);

		$sell_out = $this->Boosting_model->sell_out_kfa_kompetitor('usc_bst_product_mart', $area, $produk,$kompetitor, $filter, $from, $to);
        // var_dump($sell_out);
        
        $data_raw = $this->Boosting_model->data_sell_out('usc_bst_product_mart', $area, $produk, $filter, $from, $to);
		$chart_sell_out = json_decode($sell_out);
		$title_chart = '';
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}

		$export_product = $this->Boosting_model->get_data_export('usc_bst_product_mart',$area, $produk, $from, $to);
		$export_variant = $this->Boosting_model->get_data_export('usc_bst_product_details_mart',$area, $produk, $from, $to);

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boosting_model->get_area('usc_bst_product_mart'),
			'produk' => $produk,
			'produks' => $this->Boosting_model->get_produk('usc_bst_product_mart'),
			'kompetitor' => $kompetitor,
			'kompetitors' => $this->Boosting_model->get_kompetitor('usc_bst_product_mart'),
			'bcg' => $bcg,			
			'bcg_rekomendasi' => $bcg_rekomendasi,
			'bcg_detils' => $bcg_detils,
			'bcg_detils_rekomendasi' => $this->Boosting_model->rekomendasi_bcg_detils_kompetitor('usc_bst_product_details_mart', $area, $produk, $kompetitor, $filter, $from, $to),
			'cat' => $chart_sell_out->categories,
			'data_sell_out' => $chart_sell_out->data,
			'title_sell_out' => $title_chart,
			'opsi' => $filter,
			'notif' => $notif,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'export_product' => $export_product,
			'export_variant' => $export_variant,
			'data_raw' => $data_raw
		);

		// var_dump($data_raw);
		// die();

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_quantity', $area, $produk);
			$data['bcg_detils_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
			$data['bcg_detils_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_value', $area, $produk);
			$data['bcg_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_value', $area, $produk);
			$data['bcg_detils_x'] = $this->Boosting_model->get_bcg_x('usc_bst_product_details_value', $area, $produk);
			$data['bcg_detils_y'] = $this->Boosting_model->get_bcg_y('usc_bst_product_details_value', $area, $produk);
		}
	
        // $data;
    
        $this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix_kompetitor', $data);
        $this->load->view('templates/footer', $data);

    }

	public function master_produk(){
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Master Produk',
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/master_produk', $data);
        $this->load->view('templates/footer', $data);
	}


}



