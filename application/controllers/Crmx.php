<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('crm_model');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{
		//$data['view'] 	= 'backend/ms_prov/index';
		//$data['title']	= 'Master Data Provinsi';
		//$data['icon']	= 'icon-location4';
		
		//$this->load->helper('form');
		
		$this->load->view('crm/tpl');
    }

    public function channel_segmentation()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/channel_segmentation';
        $data['title']	= 'Channel Segmentation';

        
        $temp = $this->crm_model->getMarket();
        $market = array();

        foreach($temp as $row){
            $market[$row->cms_q][$row->cms_type]['total'] = $row->cms_total_sales;
            $market[$row->cms_q][$row->cms_type]['share'] = $row->cms_market_share;
            $market[$row->cms_q][$row->cms_type]['bonus'] = $row->cms_ratio_bonus;
        }
        $temp = array();
        for($i=1;$i<=4;$i++){
            if(!array_key_exists($i,$market)){ 
                $market[$i]['instansi']['total']    = 0;
                $market[$i]['instansi']['share']    = 0;
                $market[$i]['instansi']['bonus']    = 0;
                $market[$i]['swasta']['total']      = 0;
                $market[$i]['swasta']['share']      = 0;
                $market[$i]['swasta']['bonus']      = 0;
                $market[$i]['inter']['total']       = 0;
                $market[$i]['inter']['share']       = 0;
                $market[$i]['inter']['bonus']       = 0;
            }

            $temp['total'][] = (int) $market[$i]['instansi']['total'];
            $temp['total'][] = (int) $market[$i]['swasta']['total'];
            $temp['total'][] = (int) $market[$i]['inter']['total'];
            
            $temp['share'][] = (float) $market[$i]['instansi']['share']*100;
            $temp['share'][] = (float) $market[$i]['swasta']['share']*100;
            $temp['share'][] = (float) $market[$i]['inter']['share']*100;
            
            $temp['bonus'][] = (float) $market[$i]['instansi']['bonus']*100;
            $temp['bonus'][] = (float) $market[$i]['swasta']['bonus']*100;
            $temp['bonus'][] = (float) $market[$i]['inter']['bonus']*100;
        }
        
        $data['market']['total'] = json_encode($temp['total']);
        $data['market']['share'] = json_encode($temp['share']);
        $data['market']['bonus'] = json_encode($temp['bonus']);

        $temp = $this->crm_model->getMap();
        $map = array();
        foreach($temp as $key=> $row) {
            $map[$key][0] = $row->cms_id_prov;
            $map[$key][1] = (float) round($row->cms_market_share*100, 2);
        }

        $data['map'] = json_encode($map);

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/channel_segmentation', $data);
        $this->load->view('templates/footer', $data);
    }

    public function getDetailMap()
    {
        
        if(!$this->input->is_ajax_request()) return false;

        $id     = $this->input->post('id');
        $temp   = $this->crm_model->getMapDetail($id);
        
        $data['total_sales']    = $temp->cms_total_sales;
        $data['total_bonus']    = $temp->cms_total_bonus;
        $data['market_share']   = ($temp->cms_market_share*100).' %';
        $data['rasio_bonus']    = ($temp->cms_ratio_bonus*100).' %';

        echo json_encode($data);
    }

    public function customer_segmentation()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/customer_segmentation';
        $data['title']	= 'Customer Segmentation';

        //$data['pemerintah'] = $this->crm_model->getChannel('pemerintah')->row();
        //$data['swasta']     = $this->crm_model->getChannel('swasta')->row();
        //$data['intercompany'] = $this->crm_model->getChannel('intercompany')->row();

        $dbs = $this->crm_model->getChannel()->result();
        $data['channel'] = array();
        foreach($dbs as $db) {
            $data['ch'][strtolower($db->ct_channel)][strtolower($db->ct_level)] = $db->jml;
        }

        //$monetary       = $this->crm_model->getMonetary('\'apotek\',\'rumah sakit\',\'dokter\'')->result();

        /*$temp['apotek']['resensi']          = array();
        $temp['apotek']['frekuensi']        = array();
        $temp['dokter']['resensi']          = array();
        $temp$level['dokter']['frekuensi']        = array();
        $temp['rumah_sakit']['resensi']     = array();
        $temp['rumah_sakit']['frekuensi']   = array();

        foreach($monetary as $row){
            $type = str_replace(" ","_",$row->types);
            $temp[$type]['resensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_recency);
            $temp[$type]['frekuensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_freq);
        }

        $data['type'] = $temp;*/

        //$rules = $this->crm_model->getRulesRFM()->result();
        $rfm = $this->crm_model->getRFM()->result();

        $type = array();
        foreach($rfm as $r) {
            $type['all'][strtolower($r->ct_level)][] = array('name' => $r->ct_name.' ('.$r->ct_level.')', 'x' => (int) $r->cts_sales, 'y' => (int) $r->cts_freq);
        }
        $data['type'] = $type;

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/customer_segmentation', $data);
        $this->load->view('templates/footer', $data);
    }
    
    public function json()
	{
		if(!$this->input->is_ajax_request()) return false;

		$columns = array(
			array( 'db' => 'ct_name', 'dt' => 0 ),
			array( 'db' => 'ccs_sales', 'dt' => 1 ),
			array( 'db' => 'ccs_probability', 'dt' => 2 ),
			array( 'db' => 'ccs_step', 'dt' => 3 ),
			array( 'db' => 'cct_id', 'dt' =>4 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query(); 
        
		$city 	= $this->input->post('city');  
		$type 	= $this->input->post('type'); 
		$level 	= $this->input->post('level');  
		$status = $this->input->post('status'); 

		if ($city!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_city='".$city."')";
			else $param['where'] .= "AND (ct_city='".$city."')"; 
		} 
		if ($type!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_typex='".$type."')";
			else $param['where'] .= "AND (ct_typex='".$type."')"; 
		} 
		if ($level!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_level='".$level."')";
			else $param['where'] .= "AND (ct_level='".$level."')"; 
		} 
		if ($status!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_status='".$status."')";
			else $param['where'] .= "AND (ct_status='".$status."')"; 
		} 
        
        if(empty($param['where'])) {
            $param['step1'] = " WHERE ccs_step='1'";  
            $param['step2'] = " WHERE ccs_step='2'";  
            $param['step3'] = " WHERE ccs_step='3'";  
            $param['step4'] = " WHERE ccs_step='4'";  
            $param['step5'] = " WHERE ccs_step='5'";  
        } else {
            $param['step1'] = $param['where']." AND ccs_step='1'";   
            $param['step2'] = $param['where']." AND ccs_step='2'";  
            $param['step3'] = $param['where']." AND ccs_step='3'";  
            $param['step4'] = $param['where']." AND ccs_step='4'";  
            $param['step5'] = $param['where']." AND ccs_step='5'";  
        }
            
		  
		$result = $this->crm_model->dtquery($param)->result();
		$filter = $this->crm_model->dtfiltered();
		$total	= $this->crm_model->dtcount();
		$output = $this->datatables->output($total, $filter);
		
		
        $step[1] = $this->crm_model->step('step1',$param)->result(); 
		$step[2] = $this->crm_model->step('step2',$param)->result();
		$step[3] = $this->crm_model->step('step3',$param)->result();
		$step[4] = $this->crm_model->step('step4',$param)->result();
        $step[5] = $this->crm_model->step('step5',$param)->result(); 
        
        $count[1] = count($step[1]);
        $count[2] = count($step[2]);
        $count[3] = count($step[3]);
        $count[4] = count($step[4]);
        $count[5] = count($step[5]); 

        asort($count);
        
        $keys   = "";
        $values = "";

        foreach($count as $key=>$val){
            $keys=$key;
            $values=$val;
        }
        $output['recordsTotal']     =  $this->crm_model->getTotal();
        $output['recordsFiltered']  =  $this->crm_model->stepFiltered('step'.$keys,$param); 

        $temp = array();
        for($i=0;$i<$values;$i++)
		{   
            
            $rows[0] =  
            (array_key_exists($i,$step[1])?'<a target="_blank" href="'.base_url().'index.php/crm/consumer_journey_detail/'.$step[1][$i]->cct_id.'"><strong>'.$step[1][$i]->ct_name.'</strong><br>Rp. '.number_format($step[1][$i]->ccs_sales,0).'</a>' :'');
            $rows[1] =  
            (array_key_exists($i,$step[2])?'<a target="_blank" href="'.base_url().'index.php/crm/consumer_journey_detail/'.$step[2][$i]->cct_id.'"><strong>'.$step[2][$i]->ct_name.'</strong><br>Rp. '.number_format($step[2][$i]->ccs_sales,0).'</a>' :'');
            $rows[2] =  
            (array_key_exists($i,$step[3])?'<a target="_blank" href="'.base_url().'index.php/crm/consumer_journey_detail/'.$step[3][$i]->cct_id.'"><strong>'.$step[3][$i]->ct_name.'</strong><br>Rp. '.number_format($step[3][$i]->ccs_sales,0).'</a>' :'');
            $rows[3] =  
            (array_key_exists($i,$step[4])?'<a target="_blank" href="'.base_url().'index.php/crm/consumer_journey_detail/'.$step[4][$i]->cct_id.'"><strong>'.$step[4][$i]->ct_name.'</strong><br>Rp. '.number_format($step[4][$i]->ccs_sales,0).'<br><div class="badge badge-success badge">'.$step[4][$i]->ccs_probability.'&nbsp;</div></a>' :'');
            $rows[4] =  
            (array_key_exists($i,$step[5])?'<a target="_blank" href="'.base_url().'index.php/crm/consumer_journey_detail/'.$step[5][$i]->cct_id.'"><strong>'.$step[5][$i]->ct_name.'</strong><br>Rp. '.number_format($step[5][$i]->ccs_sales,0).'</a>' :'');
			$output['data'][] = $rows;
        }
 
		// foreach($result as $row)
		// {   
		// 	$rows = array (  
		// 		$row->ct_name, 
		// 		$row->ccs_sales,
		// 		$row->ccs_probability,
		// 		$row->ccs_step,
		// 		$row->cct_id,
		// 	); 
		// 	$output['data'][] = $rows;
		// }
		
		echo json_encode( $output );
    }
    
    public function json_detail()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'ccd_product', 'dt' => 0 ),
			array( 'db' => 'ccd_unit', 'dt' => 1 ),
			array( 'db' => 'ccd_qty', 'dt' => 2 ),
			array( 'db' => 'ccd_total', 'dt' => 3 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id = $this->input->post('id'); 
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (ccd_id_trans='".$id."')";
        else $param['where'] .= "AND (ccd_id_trans='".$id."')";  
            
		$result = $this->crm_model->dtquery_detail($param)->result();
		$filter = $this->crm_model->dtfiltered_detail();
		$total	= $this->crm_model->dtcount_detail();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{ 
			
			$rows = array (
				$row->ccd_product, 
				$row->ccd_unit, 
				number_format($row->ccd_qty,0), 
				'Rp. '.number_format($row->ccd_total,0), 
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
	}
	
	/*public function doctor()
	{
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/index');
        }

        $data['view'] 	= 'crm/doctor';
        $data['title']	= 'Doctor Analytic (RFM)';

        $this->load->view('crm/tpl', $data);
	}

    public function consumer()
    {
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/index');
        }

        $data['view'] 	= 'crm/consumer';
        $data['title']	= 'Consumer Analytic (RFM)';

        $this->load->view('crm/tpl', $data);
    }

    public function crm()
    {
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/index');
        }

        $data['view'] 	= 'crm/crm';
        $data['title']	= 'Consumer Journey';

        $this->load->view('crm/tpl', $data);
    }*/

    public function getTotal()
    {
        if(!$this->input->is_ajax_request()) return false; 
        
		$city 	= $this->input->post('city');  
		$type 	= $this->input->post('type'); 
		$level 	= $this->input->post('level');  
		$status = $this->input->post('status'); 
        $search = $this->input->post('search'); 

        $param['where'] = "";

        if($search!=""){
            $param['where'] = "WHERE (`ct_name` LIKE '%".$search."%' OR `ccs_sales` LIKE '%".$search."%' OR `ccs_probability` LIKE '%".$search."%' OR `ccs_step` LIKE '%".$search."%' OR `cct_id` LIKE '%".$search."%')";
        } 

		if ($city!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_city='".$city."')";
			else $param['where'] .= "AND (ct_city='".$city."')"; 
		} 
		if ($type!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_typex='".$type."')";
			else $param['where'] .= "AND (ct_typex='".$type."')"; 
		} 
		if ($level!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_level='".$level."')";
			else $param['where'] .= "AND (ct_level='".$level."')"; 
		} 
		if ($status!=""){ 
			if(empty($param['where'])) 	$param['where'] = "WHERE (ct_status='".$status."')";
			else $param['where'] .= "AND (ct_status='".$status."')"; 
		} 
        
        if(empty($param['where'])) {
            $param['step1'] = " WHERE ccs_step='1'";  
            $param['step2'] = " WHERE ccs_step='2'";  
            $param['step3'] = " WHERE ccs_step='3'";  
            $param['step4'] = " WHERE ccs_step='4'";  
            $param['step5'] = " WHERE ccs_step='5'";  
        } else {
            $param['step1'] = $param['where']." AND ccs_step='1'";   
            $param['step2'] = $param['where']." AND ccs_step='2'";  
            $param['step3'] = $param['where']." AND ccs_step='3'";  
            $param['step4'] = $param['where']." AND ccs_step='4'";  
            $param['step5'] = $param['where']." AND ccs_step='5'";  
        }

        $step['header1'] = $this->crm_model->stepSum('step1',$param)->row()->total; 
		$step['header2'] = $this->crm_model->stepSum('step2',$param)->row()->total; 
		$step['header3'] = $this->crm_model->stepSum('step3',$param)->row()->total; 
		$step['header4'] = $this->crm_model->stepSum('step4',$param)->row()->total; 
        $step['header5'] = $this->crm_model->stepSum('step5',$param)->row()->total;  

        echo json_encode($step);
    }

    public function consumer_journey()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	            = 'crm/consumer_journey';
        $data['title']	            = 'Customer Journey';
        $data['filter']['city']     = $this->crm_model->getFilter('ct_city');
        $data['filter']['type']     = $this->crm_model->getFilter('ct_typex');
        $data['filter']['level']    = $this->crm_model->getFilter('ct_level');
        $data['filter']['status']   = $this->crm_model->getFilter('ct_status');

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/consumer_journey', $data);
        $this->load->view('templates/footer', $data);
    }

    public function consumer_journey_detail($id)
    {
        $data['cct_id'] = $id;
        $data['menu']   = $this->user_model->getMenu();
        $data['p']      = "";
        $dt             = $this->crm_model->getData($id);
        $data['detail'] = $this->crm_model->getDetail($id);
        $data['via']    = $this->crm_model->getVia();
        $question       = $this->crm_model->getQUestion();
        $data['ask']    = array();
        foreach($question as $row){
            $data['ask'][$row->msq_step][$row->msq_order] = $row->msq_question;
        } 

        if(!$dt) redirect();
        $step  = array(); 

        
        $data['owner']['ct_name']       = $dt[0]->ct_name;
        $data['owner']['ct_type']       = $dt[0]->ct_type;
        $data['owner']['ct_address']    = $dt[0]->ct_address;
        $data['owner']['ct_phone']      = $dt[0]->ct_phone;
        $data['owner']['ct_email']      = $dt[0]->ct_email;
        $data['owner']['ct_level']      = $dt[0]->ct_level;
        $data['owner']['ct_city']       = $dt[0]->ct_city;
        $data['owner']['ct_status']     = $dt[0]->ct_status;
        // $data['owner']['ct_sales']      = $dt[0]->ct_sales;
        $data['owner']['ct_salesman']   = $dt[0]->cct_sales_name;
        $data['owner']['total_item']    = $dt[0]->cct_total_item;
        $data['owner']['total_sales']   = $dt[0]->cct_total_sales;

        foreach($dt as $row){
            $step[$row->ccs_step]['data'] = $row;
            $step[$row->ccs_step]['answer']['1'] = $row->ccs_q1;
            $step[$row->ccs_step]['answer']['2'] = $row->ccs_q2;
            $step[$row->ccs_step]['answer']['3'] = $row->ccs_q3;
            $step[$row->ccs_step]['answer']['4'] = $row->ccs_q4;
            $step[$row->ccs_step]['answer']['5'] = $row->ccs_q5;
            $step[$row->ccs_step]['answer']['6'] = $row->ccs_q6;
            $step[$row->ccs_step]['answer']['7'] = $row->ccs_q7;
            $step[$row->ccs_step]['answer']['8'] = $row->ccs_q8;
            $step[$row->ccs_step]['answer']['9'] = $row->ccs_q9;
            $step[$row->ccs_step]['answer']['10'] = $row->ccs_q10;
        }
        $data['data']   = $step;  

        $temp  = $this->crm_model->getPromo($dt[0]->ct_no);
        $promo = array();
        $promo_rec = array();
        foreach($temp as $row){
            $promo[$row->ccp_type] = $row->ccp_value;
            $promo_rec[] = $row->ccp_type;
        }

        if(!array_key_exists('Discount',$promo)) {
            $promo['Discount']  = 0; 
            $promo_rec[]        = 'Discount';
        }if(!array_key_exists('Giveaway',$promo)) {
            $promo['Giveaway']  = 0; 
            $promo_rec[]        = 'Giveaway';
        }
        if(!array_key_exists('Free Samples',$promo)) {
            $promo['Free Samples']  = 0; 
            $promo_rec[]            = 'Free Samples';
        }

        //print_r(array_keys($promo, max($promo)));

        $promo_width[$promo_rec[0]] = !empty($promo[$promo_rec[0]]) ? ($promo[$promo_rec[0]]/$promo[$promo_rec[0]]*100).'%' : '0%';
        $promo_width[$promo_rec[1]] = !empty($promo[$promo_rec[0]]) ? ($promo[$promo_rec[1]]/$promo[$promo_rec[0]]*100).'%' : '0%';
        $promo_width[$promo_rec[2]] = !empty($promo[$promo_rec[0]]) ? ($promo[$promo_rec[2]]/$promo[$promo_rec[0]]*100).'%' : '0%';
 
        $data['promo']          = $promo;  
        $data['promo_rec']      = implode( ',', array_keys($promo, max($promo)) );
        $data['promo_width']    = $promo_width;
        $data['comm']           = $this->crm_model->getComm($dt[0]->ct_no); 


        $temp  = $this->crm_model->getLevel($dt[0]->ct_no); //print_r($temp);
        $month = date('n');

        $level['Reguler'] = '1';
        $level['Silver'] = '3'; 
        $level['Gold'] = '5';
        $level['Platinum'] = '8';

        for($i=0;$i<=11;$i++){ 
           $lvl[$i] = 0; 
        }

        foreach($temp as $row){
            $ts1 = (int) $row->ccl_month-1;
            if($row->ccl_month<=$month){
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_real_level]) ? $level[$row->ccl_real_level] : '0' ;
            }
            else {
                if(isset($lvl[$ts1]))
                    $lvl[$ts1] = (int) !empty($level[$row->ccl_prediction_level]) ? $level[$row->ccl_prediction_level] : '0' ;
            }
        }

        /*$lvlx = array();
        foreach($lvl as $key => $value)
            $lvlx[$key+1] = $value;*/

        $data['level_point'] = $month-1;
        $data['level'] = json_encode($lvl);

        // print_r($data['level']);

        
        $data['log']  = $this->crm_model->getLog($id);

        $data['view'] 	= 'crm/consumer_journey';
        $data['title']	= 'Consumer Journey Detail'; 
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/consumer_journey_detail', $data);
        $this->load->view('templates/footer', $data);
    }

    public function save_log()
	{
		if(!$this->input->is_ajax_request()) return false;
		if(!$this->input->post('inp')) return false;
		
		$item = $this->input->post('inp');
        $item['ccl_date'] = date('Y-m-d H:i:s'); 
        
		if( $this->crm_model->save_log($item)){   
			echo json_encode(array('status' => 'ok;', 'text' => ''));
        }else
			echo json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data'));
	}

    public function save_step()
	{
		if(!$this->input->is_ajax_request()) return false;
		if(!$this->input->post('inp')) return false;
		
		$item = $this->input->post('inp');
        $item['ccs_close_date'] = date('Y-m-d');  
        
		if( $this->crm_model->save_step($item)){   
			echo json_encode(array('status' => 'ok;', 'text' => ''));
        }else
			echo json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data'));
    } 
	
	public function update_step()
	{
		if(!$this->input->is_ajax_request()) return false;
		if(!$this->input->post('inp')) return false;
		
		$item = $this->input->post('inp');
		$id   = $this->input->post('id');
        $max   = $this->input->post('max_pilih');
        
        for($i=1;$i<=$max;$i++){
            if(!array_key_exists('ccs_q'.$i,$item)) $item['ccs_q'.$i] = 0;
        }
		
		if( $this->crm_model->update_step($id, $item) )
			echo json_encode(array('status' => 'ok;', 'text' => ''));
		else
			echo json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data'));
    } 
    
    public function update_sales()
	{
		if(!$this->input->is_ajax_request()) return false;
		if(!$this->input->post('inp')) return false;
		
		$item = $this->input->post('inp');
		$id   = $this->input->post('id'); 
		
		if( $this->crm_model->update_sales($id, $item) )
			echo json_encode(array('status' => 'ok;', 'text' => ''));
		else
			echo json_encode(array('status' => 'error;', 'text' => 'Gagal Menyimpan Data'));
	} 

    public function clp()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/consumer_journey';
        $data['title']	= 'Consumer Loyalty Program';

        $this->load->view('templates/header', $data);
        $this->load->view('crm/clp', $data);
        $this->load->view('templates/footer', $data);
    }

    public function event()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/consumer_journey';
        $data['title']	= 'Marketing Event Analysis';

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/event', $data);
        $this->load->view('templates/footer', $data);
    }

    public function rfm()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/rfm';
        $data['title']	= 'Consumer Segmentation Farma'; 

        $data['dokter'] = $this->crm_model->getType('dokter')->row(); 
        $data['rs']     = $this->crm_model->getType('rumah sakit')->row(); 
        $data['apotek'] = $this->crm_model->getType('apotek')->row();

        $monetary       = $this->crm_model->getMonetary('\'apotek\',\'rumah sakit\',\'dokter\'')->result();
        
        $temp['apotek']['resensi']          = array();
        $temp['apotek']['frekuensi']        = array(); 
        $temp['dokter']['resensi']          = array();
        $temp['dokter']['frekuensi']        = array();
        $temp['rumah_sakit']['resensi']     = array();
        $temp['rumah_sakit']['frekuensi']   = array();

        foreach($monetary as $row){
            $type = str_replace(" ","_",$row->types);
            $temp[$type]['resensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_recency);
            $temp[$type]['frekuensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_freq);
        } 
        
        $data['type'] = $temp; 

        $this->load->view('templates/header', $data);
        $this->load->view('crm/rfm', $data);
        $this->load->view('templates/footer', $data);
    }

    public function rfm_non()
    {
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	        = 'crm/rfm';
        $data['title']	        = 'Consumer Segmentation Non Farma'; 

        $data['toko']           = $this->crm_model->getType('toko')->row();  
        $data['distributor']    = $this->crm_model->getType('distributor')->row();  
        

        $monetary       = $this->crm_model->getMonetary('\'toko\',\'distributor\'')->result();
        
        $temp['toko']['resensi']          = array();
        $temp['toko']['frekuensi']        = array(); 
        $temp['distributor']['resensi']   = array();
        $temp['distributor']['frekuensi'] = array(); 

        foreach($monetary as $row){
            $type = str_replace(" ","_",$row->types);
            $temp[$type]['resensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_recency);
            $temp[$type]['frekuensi'][] = array('name' => $row->ct_name, 'x' => (int) $row->cts_sales, 'y' => (int) $row->cts_freq);
        } 
        
        $data['type'] = $temp; 

        $this->load->view('templates/header', $data);
        $this->load->view('crm/rfm_non', $data);
        $this->load->view('templates/footer', $data);
    }
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
}

?>