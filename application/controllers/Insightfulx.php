<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insightful extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('user_model');
        $this->load->model('insightful_model', 'dm');
        $this->load->library('datatables');

        $this->load->helper('form');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function problem($start="",$end="",$lini='all',$layanan='all')
    {
        $layanan = urldecode($layanan);

        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $data['var_start'] = $start;
        $data['var_end'] = $end;
        $data['var_lini'] = $lini;
        $data['var_layanan'] = $layanan; 

        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];

        $lini_dbs = $this->dm->get_lini_target()->result();
        // $data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][strtolower($ld->lini)] = $ld->lini;
        }
        $data['col_filter'][''] = 'Value';
        $data['col_filter']['qty'] = 'Qty';
        // $layanan_dbs = $this->dm->get_layanan_target()->result();
        // // $data['layanan_filter']['all'] = 'Semua Layanan';
        // foreach($layanan_dbs as $ld) {
        //     $data['layanan_filter'][$ld->layanan] = $ld->layanan;
        // }

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful/problem_identification';
        $data['title']	= 'Problem Identification';

        $dbs = $this->dm->getCountDaerah($s[0], $e[0], $s[1], strtoupper($lini), $layanan)->result();
        $array = array();
        $array_lini = array();
        foreach($dbs as $db) {
            $id = $this->prov_id($db->provinsi);

            if($db->target > 0)
                $persen = round(($db->realisasi / $db->target)*100);
            else
                $persen = 100;

            $array[] = array($id, $persen);
        }

        $data['data_maps'] = json_encode($array);

        $data['lini'] = $this->dm->getByLini($s[0], $e[0], $s[1], $lini, $layanan)->result();

        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful/problem_identification', $data);
        $this->load->view('templates/footer', $data);
    }

    public function problem_kftd()
    {
        $id = $this->input->post('id');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');

        $s = explode('-', $start);
        $e = explode('-', $end);

        if(!empty($id)) {
            $wid = explode(',', $id);
            $area = array();
            foreach($wid as $wi) {
                $prov = $this->prov_id($wi);
                if(!empty($prov))
                    $area[] = "'".$prov."'";
            }
        } else {
            $area = array();
        }

        $data['kftd'] = $this->dm->getByKftd($area, $s[0], $e[0], $s[1], $lini, $layanan)->result();

        /*//$data['area'] = $id;
        $data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        foreach($charts as $chart) {
            $data['charts']['month'][$chart->bulan] = $bln[$chart->bulan];
            $data['charts']['target'][$chart->bulan] = (int) $chart->target;
            $data['charts']['realisasi'][$chart->bulan] = (int) $chart->realisasi;
            $data['charts']['stock'][$chart->bulan] = (int) $chart->stock;
        }

        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($data['charts']['month'][$b])) {
                $data['charts']['month'][$b] = $bln[$b];
                $data['charts']['target'][$b] = 0;
                $data['charts']['realisasi'][$b] = 0;
                $data['charts']['stock'][$b] = 0;
            }
        }*/

        echo $this->load->view('insightful/problem_kftd', $data, true);
    }

    public function problem_brand()
    {
        $id = urldecode($this->input->post('id'));
        $col = $this->input->post('col');

        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');

        $s = explode('-', $start);
        $e = explode('-', $end);

        if($col == 'lini')
        {
            $data['title'] = 'Lini '.$id;
            $data['ids']   = $id;
            $data['brand'] = $this->dm->getByLiniGroupBrand($id, $s[0], $e[0], $s[1])->result();
        }
        else
        {
            $data['title'] = 'Cabang '.$id;
            $data['ids']   = $id;
            $data['brand'] = $this->dm->getByKftdGroupBrand($id, $s[0], $e[0], $s[1])->result();
        }

        $data['var_col'] = $col;

        echo $this->load->view('insightful/problem_brand', $data, true);
    }

    public function problem_product()
    {
        $brand = $this->input->post('id');
        $col = $this->input->post('col');

        $start = $this->input->post('start');
        $end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $data['title'] = $brand;
        $data['ids']   = $brand;
        $data['col']  = $col;

        if($col == 'lini')
        {
            $data['product'] = $this->dm->getByBrandGroupProduct($brand, $s[0], $e[0], $s[1])->result();
        }
        else
        {
            $data['product'] = $this->dm->getByBrandGroupProduct($brand, $s[0], $e[0], $s[1])->result();
        }

        echo $this->load->view('insightful/problem_product', $data, true);
    }

    /*public function problem_product_detail()
    {
        /*$col = $this->input->post('col');
        $produk = $this->input->post('nama_produk');
        $brand = $this->input->post('nama_produkx');
        $cabang = $this->input->post('nama_cabang');
        $month = $this->input->post('month');
        $year = $this->input->post('year');*

        $realisasi = $this->input->post('realisasi');
        $target = $this->input->post('target');
        $me = $this->input->post('me');
        $cabang = $this->input->post('cabang');
        $brand = $this->input->post('brand');

        $data['col']  = $col;  
        
        $produk = $this->dm->getByBrandGroupProductById($cabang,$brand,$produk,$month,$year)->row();
        $dts = $this->dm->getBy_marketing_expenses($cabang,$brand,$month,$year)->row();
         
        if($realisasi < $target){
            if (($me >= 0) and ($me <= 30242353)  and ($cabang == 'KFTD Purwokerto')){
                $rekomendasi = "Increase marketing marketingexpense > 0";
                $change      = "42.9 %";
            }
            else {
                if ($brand == "FITUNO"){
                    $rekomendasi = "Increase marketing marketingexpense > Rp. 30.242.353";
                    $change      = " 83.9%";
                }
                else  if ($brand == "BATUGIN"){
                    $rekomendasi = "Increase marketing marketingexpense > Rp. 151.417.884";
                    $change      = " 97.7%";
                }
            }
            $data['message'] = "Performa produk $brand di $cabang di bulan $month tahun $year hanya mencapai ".number_format(((($produk->target-$produk->realisasi)/$produk->target)*100),2)."%.<br>
            <br>Saat ini Marketing Expense untuk $brand adalah sebesar Rp. ".number_format($dts->marketing_expenses, 0, ',', '.').". Jika Marketing Expense ditingkatkan menjadi $rekomendasi, maka terdapat kemungkinan $change performa produk ini mencapai target.";
        }
        else {
            $data['message'] = "Performa produk telah mencapai target.";
        } 

        echo $this->load->view('insightful/problem_product_detail', $data, true);
    }*/

    private function prov_id($prov='')
    {
        $array['ACEH'] = 'id-ac';
        $array['KALIMANTAN TIMUR'] = 'id-ki';
        $array['JAWA TENGAH'] = 'id-jt';
        $array['BENGKULU'] = 'id-be';
        $array['BANTEN'] = 'id-bt';
        $array['KALIMANTAN BARAT'] = 'id-kb';
        $array['BANGKA BELITUNG'] = 'id-bb';
        $array['BALI'] = 'id-ba';
        $array['JAWA TIMUR'] = 'id-ji';
        $array['KALIMANTAN SELATAN'] = 'id-ks';
        $array['NUSA TENGGARA TIMUR'] = 'id-nt';
        $array['SULAWESI SELATAN'] = 'id-se';
        $array['KEPULAUAN RIAU'] = 'id-kr';
        $array['PAPUA BARAT'] = 'id-ib';
        $array['SUMATERA UTARA'] = 'id-su';
        $array['RIAU'] = 'id-ri';
        $array['SULAWESI UTARA'] = 'id-sw';
        $array['MALUKU UTARA'] = 'id-la';
        $array['SUMATERA BARAT'] = 'id-sb';
        $array['MALUKU'] = 'id-ma';
        $array['NUSA TENGGARA BARAT'] = 'id-nb';
        $array['SULAWESI TENGGARA'] = 'id-sg';
        $array['SULAWESI TENGAH'] = 'id-st';
        $array['PAPUA'] = 'id-pa';
        $array['JAWA BARAT'] = 'id-jr';
        $array['LAMPUNG'] = 'id-1024';
        $array['DKI JAKARTA'] = 'id-jk';
        $array['GORONTALO'] = 'id-go';
        $array['DI. YOGYAKARTA'] = 'id-yo';
        $array['KALIMANTAN TENGAH'] = 'id-kt';
        $array['SUMATERA SELATAN'] = 'id-sl';
        $array['SULAWESI BARAT'] = 'id-sr';
        $array['JAMBI'] = 'id-ja';

        /*foreach($array as $p => $id) {
            echo '$array['."'".strtoupper($p)."'] = '$id';<br>";
        }*/

        $prov = strtoupper($prov);

        if(!empty($array[$prov]))
            return $array[$prov];
        else
            return '';
    }

    private function id_prov($id)
    {
        $array['id-ac'] = 'ACEH';
        $array['id-ki'] = 'KALIMANTAN TIMUR';
        $array['id-kb'] = 'KALIMANTAN BARAT';
        $array['id-ks'] = 'KALIMANTAN SELATAN';
        $array['id-kt'] = 'KALIMANTAN TENGAH';
        $array['id-su'] = 'SUMATERA UTARA';
        $array['id-sb'] = 'SUMATERA BARAT';
        $array['id-sl'] = 'SUMATERA SELATAN';
        $array['id-jt'] = 'JAWA TENGAH';
        $array['id-ji'] = 'JAWA TIMUR';
        $array['id-jr'] = 'JAWA BARAT';
        $array['id-se'] = 'SULAWESI SELATAN';
        $array['id-sg'] = 'SULAWESI TENGGARA';
        $array['id-st'] = 'SULAWESI TENGAH';
        $array['id-sw'] = 'SULAWESI UTARA';
        $array['id-sr'] = 'SULAWESI BARAT';
        $array['id-be'] = 'BENGKULU';
        $array['id-bt'] = 'BANTEN';
        $array['id-bb'] = 'BANGKA BELITUNG';
        $array['id-ba'] = 'BALI';
        $array['id-nt'] = 'NUSA TENGGARA TIMUR';
        $array['id-nb'] = 'NUSA TENGGARA BARAT';
        $array['id-kr'] = 'KEPULAUAN RIAU';
        $array['id-ib'] = 'PAPUA BARAT';
        $array['id-ri'] = 'RIAU';
        $array['id-la'] = 'MALUKU UTARA';
        $array['id-ma'] = 'MALUKU';
        $array['id-pa'] = 'PAPUA';
        $array['id-1024'] = 'LAMPUNG';
        $array['id-jk'] = 'DKI JAKARTA';
        $array['id-go'] = 'GORONTALO';
        $array['id-yo'] = 'DI. YOGYAKARTA';
        $array['id-ja'] = 'JAMBI';

        if(!empty($array[$id]))
            return $array[$id];
        else
            return '';
    }
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
	private function achievement($val)
    {
        return ($val*100).' %';
    }
    private function persen($val)
    {
        return ($val*100).' %';
    }
    private function growth($realisasi, $realisasi_1)
    {
        if(empty($realisasi))
            return round(($realisasi - $realisasi_1) / $realisasi)*100;
        else
            return 0;
    }
    public function json_lini()
    { 
        if(!$this->input->is_ajax_request()) return false;

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $area = $this->input->post('area'); 

        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(
            array( 'db' => 'lini', 'dt' => 0 ),
            array( 'db' => 'target', 'dt' => 1 ),
            array( 'db' => 'realisasi', 'dt' => 2 ),
            array( 'db' => 'realisasi_1', 'dt' => 3 ),
            array( 'db' => 'achievement', 'dt' => 4 ),
            array( 'db' => 'growth', 'dt' => 5 ),
            array( 'db' => 'stock', 'dt' => 6 )
        );

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        if(!empty($area)) {
            $wid = explode(',', $area);
            $area_array = array();
            foreach($wid as $wi) {
                $prov = $this->id_prov($wi);
                if(!empty($prov))
                    $area_array[] = "'".strtolower($prov)."'";
            }

            if(!empty($area_array))
                $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
        }

        

        if (is_array($lini)){
            $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        }

        // if (is_array($layanan)){
        //     $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
        // }

        $result = $this->dm->dtquery_lini($param)->result();
        $filter = $this->dm->dtfiltered_lini();
        $total	= $this->dm->dtcount_lini();
        $output = $this->datatables->output($total, $filter);

        foreach($result as $row)
        {
            $rows = array (
                strtoupper($row->lini),
                y_num_pad($row->target),
                y_num_pad($row->realisasi),
                y_num_pad($row->realisasi_1),
                $this->persen($row->achievement),
                $this->persen($row->growth),
                y_num_pad($row->stock)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_kftd()
    {
        if(!$this->input->is_ajax_request()) return false;

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $layanan = $this->input->post('layanan');
        $area = $this->input->post('area');
 

        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(
            array( 'db' => 'nama_kftd', 'dt' => 0 ),
            array( 'db' => 'target', 'dt' => 1 ),
            array( 'db' => 'realisasi', 'dt' => 2 ),
            array( 'db' => 'realisasi_1', 'dt' => 3 ),
            array( 'db' => 'achievement', 'dt' => 4 ),
            array( 'db' => 'growth', 'dt' => 5 ),
            array( 'db' => 'stock', 'dt' => 6 )
        );

        $this->datatables->set_cols($columns);
        $param	 = $this->datatables->query();

        $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        if(empty($param['where'])) {
            $param['where'] = 'WHERE '.$w;
        } else {
            $param['where'] .= ' AND '.$w;
        }

        if(!empty($area)) {
            $wid = explode(',', $area);
            $area_array = array();
            foreach($wid as $wi) {
                $prov = $this->id_prov($wi);
                if(!empty($prov))
                    $area_array[] = "'".strtolower($prov)."'";
            }

            if(!empty($area_array))
                $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
        }  

        if (is_array($lini)){
            $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        }

        // if (is_array($layanan)){
        //     $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
        // }

        $result = $this->dm->dtquery_kftd($param)->result();
        $filter = $this->dm->dtfiltered_kftd();
        $total	= $this->dm->dtcount_kftd();
        $output = $this->datatables->output($total, $filter);

        foreach($result as $row)
        {
            $rows = array (
                strtoupper($row->nama_kftd),
                y_num_pad($row->target),
                y_num_pad($row->realisasi),
                y_num_pad($row->realisasi_1),
                $this->persen($row->achievement),
                $this->persen($row->growth),
                y_num_pad($row->stock)
            );

            $output['data'][] = $rows;
        }

        echo json_encode( $output );
    }

    public function json_brand()
    {
        if(!$this->input->is_ajax_request()) return false;

        $var = strtolower(urldecode($this->input->post('var')));
        if(empty($var)) {
            $a = array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array());
            echo json_encode($a);
        } else {
            $columns = array(
                array( 'db' => 'nama_brand', 'dt' => 0 ),
                array( 'db' => 'target', 'dt' => 1 ),
                array( 'db' => 'realisasi', 'dt' => 2 ),
                array( 'db' => 'realisasi_1', 'dt' => 3 ),
                array( 'db' => 'achievement', 'dt' => 4 ),
                array( 'db' => 'growth', 'dt' => 5 ),
                array( 'db' => 'stock', 'dt' => 6 )
            );

            $start = $this->input->post('startx');
            $end = $this->input->post('end');
            $lini 	 = $this->input->post('lini');
            $layanan = $this->input->post('layanan');
            $area = $this->input->post('area');
            $col = $this->input->post('col');  

            $s = explode('-', $start);
            $e = explode('-', $end);

            $this->datatables->set_cols($columns);
            $param	 = $this->datatables->query();

            $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
            if(empty($param['where'])) {
                $param['where'] = 'WHERE '.$w;
            } else {
                $param['where'] .= ' AND '.$w;
            }

            if(!empty($area)) {
                $wid = explode(',', $area);
                $area_array = array();
                foreach($wid as $wi) {
                    $prov = $this->id_prov($wi);
                    if(!empty($prov))
                        $area_array[] = "'".strtolower($prov)."'";
                }

                if(!empty($area_array))
                    $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
            }


            // if (is_array($layanan)){
            //     $param['where'] .= " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
            // } 

            if($col == 'lini') {
                $param['where'] .= " AND (lower(lini_desc1)='".strtolower($var)."')";
            } else { 
                if (is_array($lini)){
                    $param['where'] .= " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
                }
                $param['where'] .= " AND (lower(nama_kftd)='".strtolower($var)."')";
            }

            $result = $this->dm->dtquery_brand($param)->result();
            $filter = $this->dm->dtfiltered_brand();
            $total	= $this->dm->dtcount_brand();
            $output = $this->datatables->output($total, $filter);

            foreach($result as $row)
            {
                $rows = array (
                    strtoupper($row->nama_brand),
                    y_num_pad($row->target),
                    y_num_pad($row->realisasi),
                    y_num_pad($row->realisasi_1),
                    $this->persen($row->achievement),
                    $this->persen($row->growth),
                    y_num_pad($row->stock)
                );

                $output['data'][] = $rows;
            }

            echo json_encode( $output );
        }
    }

    public function json_prod()
    {
        if(!$this->input->is_ajax_request()) return false;

        $var = strtolower(urldecode($this->input->post('var')));
        if(empty($var)) {
            $a = array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array());
            echo json_encode($a);
        } else {
            $columns = array(
                array( 'db' => 'nama_produk', 'dt' => 0 ),
                array( 'db' => 'target', 'dt' => 1 ),
                array( 'db' => 'realisasi', 'dt' => 2 ),
                array( 'db' => 'realisasi_1', 'dt' => 3 ),
                array( 'db' => 'achievement', 'dt' => 4 ),
                array( 'db' => 'growth', 'dt' => 5 ),
                array( 'db' => 'stock', 'dt' => 6 )
            );

            $start = $this->input->post('startx');
            $end = $this->input->post('end');
            $lini 	 = $this->input->post('lini');
            $layanan = $this->input->post('layanan');
            $area = $this->input->post('area');
            $col = $this->input->post('col');

            $lini = str_replace("_"," ",$lini);
            $layanan = str_replace("_"," ",$layanan);

            $s = explode('-', $start);
            $e = explode('-', $end);

            $this->datatables->set_cols($columns);
            $param	 = $this->datatables->query();

            $w = "tahun = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
            if(empty($param['where'])) {
                $param['where'] = 'WHERE '.$w;
            } else {
                $param['where'] .= ' AND '.$w;
            }

            if(!empty($area)) {
                $wid = explode(',', $area);
                $area_array = array();
                foreach($wid as $wi) {
                    $prov = $this->id_prov($wi);
                    if(!empty($prov))
                        $area_array[] = "'".strtolower($prov)."'";
                }

                if(!empty($area_array))
                    $param['where'] .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
            }

            // if ($layanan!="all") {
            //     $param['where'] .= " AND (lower(layanan)='".strtolower($layanan)."')";
            // }

            $param['where'] .= " AND (lower(nama_brand)='".strtolower(str_replace("'","\'",$var))."')";

            $result = $this->dm->dtquery_prod($param)->result();
            $filter = $this->dm->dtfiltered_prod();
            $total	= $this->dm->dtcount_prod();
            $output = $this->datatables->output($total, $filter);

            foreach($result as $row)
            {
                $rows = array (
                    strtoupper($row->nama_produk),
                    y_num_pad($row->target),
                    y_num_pad($row->realisasi),
                    y_num_pad($row->realisasi_1),
                    $this->persen($row->achievement),
                    $this->persen($row->growth),
                    y_num_pad($row->stock)
                );

                $output['data'][] = $rows;
            }

            echo json_encode( $output );
        }
    }

    //===================================================================================================================

    public function problem_area()
    {
        $area = $this->input->post('area');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $lini = $this->input->post('lini');
        $layanan = $this->input->post('layanan');

        $s = explode('-', $start);
        $e = explode('-', $end);

        $warea = '';
        if(!empty($area)) {
            $wid = explode(',', $area);
            $area_array = array();
            foreach($wid as $wi) {
                $prov = $this->id_prov($wi);
                if(!empty($prov))
                    $area_array[] = "'".strtolower($prov)."'";
            }

            if(!empty($area_array))
                $warea .= " AND lower(provinsi) IN (".implode(',', $area_array).")";
        }

        $charts = $this->dm->getByAreaGroupMonth($warea, $s[0], $e[0], $s[1], $lini, $layanan)->result();

        $data['vyear'] = $s[1];
        $data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        foreach($charts as $chart) {
            $data['charts']['month'][$chart->bulan] = $bln[$chart->bulan];
            $data['charts']['realisasi_1'][$chart->bulan] = (int) $chart->realisasi_1;
            $data['charts']['realisasi'][$chart->bulan] = (int) $chart->realisasi;
        }

        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($data['charts']['month'][$b])) {
                $data['charts']['month'][$b] = $bln[$b];
                $data['charts']['realisasi_1'][$b] = 0;
                $data['charts']['realisasi'][$b] = 0;
            }
        }

        echo $this->load->view('insightful/problem_area', $data, true);
    }

    public function marketing_realisasi($start='', $end='', $lini='all')
    {
        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $s = explode('-', $start);
        $e = explode('-', $end);

        $dbs = $this->dm->get_realisasi_expenses($s[0], $e[0], $s[1], str_replace('_', ' ', $lini))->result();

        //$data['area'] = $id;
        //$data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $charts = array();
        $lini_array = array();
        foreach($dbs as $db) {
            if(empty($charts[$db->bulan])) {
                $charts[$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
            } else {
                $charts[$db->bulan]['expenses'] += (int) $db->expenses;
                $charts[$db->bulan]['realisasi'] += (int) $db->realisasi;
            }

            $lini_array[$db->lini][$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
        }

        $array = array();
        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($charts[$b])) {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = 0;
                $array['realisasi'][$b] = 0;
                $array['ratio'][$b] = 0;
            } else {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = $charts[$b]['expenses'];
                $array['realisasi'][$b] = $charts[$b]['realisasi']/100;
                $array['ratio'][$b] = !empty($charts[$b]['realisasi']) ? round(($charts[$b]['expenses'] / $charts[$b]['realisasi'])*100, 2) : 0;
            }
        }

        $json[] = array('type' => 'column', 'name' => 'Marketing Expense', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#69d0ef', 'data' => array_values($array['expenses']));
        $json[] = array('type' => 'column', 'name' => 'Realisasi Sales', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#ab82c4', 'data' => array_values($array['realisasi']));
        $json[] = array('type' => 'spline', 'name' => 'Rasio Realisasi Expense', 'yAxis' => 1, 'tooltip' => array('valueSuffix' => ' %'), 'color' => '#db2347', 'data' => array_values($array['ratio']));

        $data['ex_data'] = json_encode($json);
        $data['ex_bln'] = json_encode(array_values($array['month']));
        $data['lini_data'] = $lini_array;
        $data['lini_bln'] = $array['month'];

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];

        $data['var_lini'] = $lini;

        $lini_dbs = $this->dm->get_lini_expanse()->result();
        $data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][str_replace(' ', '_', strtolower($ld->lini))] = strtoupper($ld->lini);
        }

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful/marketing_realisasi';
        $data['title']	= 'Marketing Expense vs Realisasi Sales';

        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful/marketing_realisasi', $data);
        $this->load->view('templates/footer', $data);
    }
}

?>