<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Import/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'i_cash_collection')
    {

        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/form/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getCashCollection()
    {
        $this->load->model('kf_import_model');
        $year = json_decode($this->input->get_post('year'));
        $month = json_decode($this->input->get_post('month'));
        $entitas = json_decode($this->input->get_post('entitas'));
        $week = json_decode($this->input->get_post('week'));

        $data = $this->kf_import_model->getCashCollection($week, $month, $year, $entitas);
        echo json_encode($data);
    }

    public function InsertDataCashCollection()
    {
        $this->load->model('kf_import_model');
        $datas = $this->input->get_post('data');
        $data = $this->kf_import_model->InsertDataCashCollection($datas,$this->session->userdata('nama'));
        echo json_encode($data);
    }

    public function UpdateDataCashCollection()
    {
        $this->load->model('kf_import_model');
        $datas = $this->input->get_post('data');    
        $data = $this->kf_import_model->UpdateDataCashCollection($datas,$this->session->userdata('nama'));
        echo json_encode($data);
    }    
    public function DeleteDataCashCollection()
    {
        $id = $this->input->get_post('id'); 
        $data = $this->kf_import_model->DeleteDataCashCollection($id);
        echo json_encode($data);
    }    
}
