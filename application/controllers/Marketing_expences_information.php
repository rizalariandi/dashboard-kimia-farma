<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Marketing_expences_information extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array("marketing_expences_information_model"));
	}
	public function index()
	{
		$data['url']				= base_url('index.php/marketing_expences_information');
		$data['mycontroller']		= 'marketing_expences_information';

		$this->template->template('marketing_expences_information/home', $data);
	}
	public function getList()
	{


		$record_total = 0;
		$record_filter_total = 0;
		$filter = array();
		$header = array();
		$order	= $this->input->get('order');
		$ordered = array("column" => $order[0]['column'], "dir" => $order[0]['dir']);

		parse_str($this->input->get("f_search"), $filter);


		parse_str($this->input->get("f_head"), $header);
		//print_r($filter);die();
		$header =  $header['head_filter'];
		$start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0;
		$limit = $this->input->get("length");

		if (strlen($filter['bulan_awal']) > 0 && strlen($filter['bulan_akhir']) > 0 && strlen($filter['tahun']) > 0 && strlen($header) > 0) {
			//echo $limit;die();
			$data = $this->marketing_expences_information_model->ordering($start, $limit, $filter, "data", $ordered, $header);
			$data_last = $this->marketing_expences_information_model->ordering_last($start, $limit, $filter, "data", $ordered, $header);
			//echo $data_last['data'][0]['cost_center'];
			$size_last = sizeof($data_last['data']);
			$size_data = sizeof($data['data']);
			//			echo "<pre>";print_r($data); "</pre>";die();
			$i = 0;
			if ($data != false) {
				foreach ($data['data'] as $datas) {

					if ($header == 'cost_center') {
						$result = array();
						//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
						$result[] = $datas['cost_center'];
						$result[] = $this->rupiahnormal($datas['anggaran']);
						$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
						$ly = 0;
						if ($size_last > 0) {
							foreach ($data_last['data'] as $datas_last) {

								if (strpos($datas['cost_center'], $datas_last['cost_center']) !== false) {

									$ly = $datas_last['realisasi_this_year'];
									break;
								}
							}
						}

						$growth = 0;
						if ($ly > 0) {
							$growth = (($datas['realisasi_this_year'] - $ly) / $ly) * 100;
						}

						$result[] = $this->rupiahnormal($ly);
						$result[] = $this->rupiah($datas['achievement']);
						$result[] = $this->rupiah($growth);
						$result[] = $this->rupiah($datas['share_realisasi_this_year']);
						$result[] = $this->rupiah($datas['share_realisasi_last_year']);
						$json_data[] = $result;
						$record_total 				= $size_data;
						$record_filter_total 	= $size_data;
					} else if ($header == 'biaya') {
						$result = array();
						//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;,&quot;'.$datas['biaya'].'&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
						$result[] = $datas['cost_center'];
						$result[] = $datas['biaya_desc'];
						$result[] = $this->rupiahnormal($datas['anggaran']);
						$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
						$ly = 0;
						if ($size_last > 0) {
							foreach ($data_last['data'] as $datas_last) {

								if (strpos($datas['cost_center'], $datas_last['cost_center']) !== false) {

									$ly = $datas_last['realisasi_this_year'];
									break;
								}
							}
						}

						$growth = 0;
						if ($ly > 0) {
							$growth = (($datas['realisasi_this_year'] - $ly) / $ly) * 100;
						}

						$result[] = $this->rupiahnormal($ly);
						$result[] = $this->rupiah($datas['achievement']);
						$result[] = $this->rupiah($growth);
						$result[] = $this->rupiah($datas['share_realisasi_this_year']);
						$result[] = $this->rupiah($datas['share_realisasi_last_year']);
						$json_data[] = $result;
						$record_total 				= $size_data;
						$record_filter_total 	= $size_data;
					} else if ($header == 'gl_account') {
						$result = array();
						//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;,&quot;'.$datas['biaya'].'&quot;,&quot;'.$datas['gl_account_code'].'&quot;,&quot;&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
						$result[] = $datas['cost_center'];
						$result[] = $datas['biaya_desc'];
						$result[] = $datas['gl_account'];
						$result[] = $this->rupiahnormal($datas['anggaran']);
						$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
						$ly = 0;
						if ($size_last > 0) {
							foreach ($data_last['data'] as $datas_last) {

								if (strpos($datas['cost_center'], $datas_last['cost_center']) !== false) {

									$ly = $datas_last['realisasi_this_year'];
									break;
								}
							}
						}
						$growth = 0;
						if ($ly > 0) {
							$growth = (($datas['realisasi_this_year'] - $ly) / $ly) * 100;
						}

						$result[] = $this->rupiahnormal($ly);
						$result[] = $this->rupiah($datas['achievement']);
						$result[] = $this->rupiah($growth);
						$result[] = $this->rupiah($datas['share_realisasi_this_year']);
						$result[] = $this->rupiah($datas['share_realisasi_last_year']);
						$json_data[] = $result;
						$record_total 				= $size_data;
						$record_filter_total 	= $size_data;
					} else if ($header == 'gpm_pm_code') {
						$result = array();
						//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;,&quot;'.$datas['biaya'].'&quot;,&quot;'.$datas['gl_account_code'].'&quot;,&quot;'.$datas['gpm_pm_code_code'].'&quot;,&quot;&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
						$result[] = $datas['cost_center'];
						$result[] = $datas['biaya_desc'];
						$result[] = $datas['gl_account'];
						$result[] = $datas['gpm_pm_code'];
						$result[] = $this->rupiahnormal($datas['anggaran']);
						$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
						$ly = 0;
						if ($size_last > 0) {
							foreach ($data_last['data'] as $datas_last) {

								if (strpos($datas['cost_center'], $datas_last['cost_center']) !== false) {

									$ly = $datas_last['realisasi_this_year'];
									break;
								}
							}
						}

						$growth = 0;
						if ($ly > 0) {
							$growth = (($datas['realisasi_this_year'] - $ly) / $ly) * 100;
						}

						$result[] = $this->rupiahnormal($ly);
						$result[] = $this->rupiah($datas['achievement']);
						$result[] = $this->rupiah($growth);
						$result[] = $this->rupiah($datas['share_realisasi_this_year']);
						$result[] = $this->rupiah($datas['share_realisasi_last_year']);
						$json_data[] = $result;
						$record_total 				= $size_data;
						$record_filter_total 	= $size_data;
					} else if ($header == 'rsm_shopper_code') {
						$result = array();
						//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;,&quot;'.$datas['biaya'].'&quot;,&quot;'.$datas['gl_account_code'].'&quot;,&quot;'.$datas['rsm_shopper_code_code'].'&quot;,&quot;filter&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
						$result[] = $datas['cost_center'];
						$result[] = $datas['biaya_desc'];
						$result[] = $datas['gl_account'];
						//$result[] = $datas['gpm_pm_code'];
						$result[] = $datas['rsm_shopper_code'];
						$result[] = $this->rupiahnormal($datas['anggaran']);
						$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
						$ly = 0;
						if ($size_last > 0) {
							foreach ($data_last['data'] as $datas_last) {

								if (strpos($datas['cost_center'], $datas_last['cost_center']) !== false) {

									$ly = $datas_last['realisasi_this_year'];
									break;
								}
							}
						}
						$growth = 0;
						if ($ly > 0) {
							$growth = (($datas['realisasi_this_year'] - $ly) / $ly) * 100;
						}
						$result[] = $this->rupiahnormal($ly);
						$result[] = $this->rupiah($datas['achievement']);
						$result[] = $this->rupiah($growth);
						$result[] = $this->rupiah($datas['share_realisasi_this_year']);
						$result[] = $this->rupiah($datas['share_realisasi_last_year']);
						$json_data[] = $result;
						$record_total 				= $size_data;
						$record_filter_total 	= $size_data;
					}
				}
			} else {
				$result = array();
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] =	'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$json_data[] = $result;
			}
		} else {
			$result = array();
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] =	'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$json_data[] = $result;
		}

		$output = array(
			"draw" => $_GET['draw'],
			"recordsTotal" => $record_total,
			"recordsFiltered" => $record_filter_total,
			"data" => $json_data
		);
		echo json_encode($output);
	}
	public function orderBY_($item1, $item2)
	{
		if ($item2 == 'gl_account_description') {
			$arry = array('cost_center', 'biaya', 'gl_account', 'anggaran', 'sisa_expense', 'realisasi', 'last_year', 'achievement', 'growth', 'share_realisasi_this_year', 'share_realisasi_last_year');
			return $arry[$item1['column']].' '.$item1['dir'];
		} else if ($item2 == 'nama_biaya') {
			$arry = array('cost_center', 'biaya', 'anggaran', 'sisa_expense', 'realisasi', 'last_year', 'achievement', 'growth', 'share_realisasi_this_year', 'share_realisasi_last_year');
			return $arry[$item1['column']].' '.$item1['dir'];
		} else {
			$arry = array('cost_center', 'anggaran', 'sisa_expense', 'realisasi', 'last_year', 'achievement', 'growth', 'share_realisasi_this_year', 'share_realisasi_last_year');
			return $arry[$item1['column']].' '.$item1['dir'];
		}
	}
	public function getListChart()
	{
		$filter_alone = array();
		$header = array();
		parse_str($this->input->get("f_head"), $header);
		$start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0;
		$limit = $this->input->get("length");
		$bulan_awal = $this->input->get("bulan_awal");
		$bulan_akhir = $this->input->get("bulan_akhir");
		$tahun = $this->input->get("tahun");
		$order	= $this->input->get('order');
		$ordered = array("column" => $order[0]['column'], "dir" => $order[0]['dir']);
		// parse_str($this->input->get("f_search"),$filter_alone);
		$filter_alone['bulan_awal'] = $bulan_awal;
		$filter_alone['bulan_akhir'] = $bulan_akhir;
		$filter_alone['tahun'] = $tahun;
		$data_chart = $this->marketing_expences_information_model->chartOrderingPeriod($start, $limit, $filter_alone, "data", $ordered, $header);
		echo json_encode($data_chart);
	}

	public function getListPeriod()
	{
		$record_total = 0;
		$record_filter_total = 0;
		$filter = array();
		$filter_alone = array();
		$header = array();		

		parse_str($this->input->get("f_search"), $filter);

		parse_str($this->input->get("f_head"), $header);
		// parse_str($this->input->get("f_head"),$filter);		
		$bulan_awal = $this->input->get("bulan_awal");
		$bulan_akhir = $this->input->get("bulan_akhir");
		$tahun = $this->input->get("tahun");
		//print_r($filter);die();		
		$header =  $header['head_filter'];
		$ordered	= $this->orderBY_($this->input->get('order')[0], $header);		
		$start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0;
		$limit = $this->input->get("length");
		$filter['bulan_awal'] = $bulan_awal;
		$filter['bulan_akhir'] = $bulan_akhir;
		$filter['tahun'] = $tahun;
		if (strlen($bulan_awal) > 0 && strlen($bulan_akhir) > 0 && strlen($tahun) > 0 && strlen($header) > 0) {
			//echo $limit;die();
			$data = $this->marketing_expences_information_model->orderingPeriod($start, $limit, $filter, "data", $ordered, $header);
			// $data_last = $this->marketing_expences_information_model->orderingPeriod_last($start,$limit,$filter,"data",$ordered,$header);
			//echo $data_last['data'][0]['cost_center'];
			// $size_last = sizeof($data_last['data']);
			$size_data = sizeof($data['data']);
			//			echo "<pre>";print_r($data); "</pre>";die();
			// print_r($data['grandtotal']);
			if ($data != false) {
				foreach ($data['data'] as $datas) {

					$result = array();
					//$result[] = '<label class="btn" onclick="dommodal(&quot;'.$datas['cost_center_code'].'&quot;,&quot;'.$datas['biaya'].'&quot;,&quot;'.$datas['gl_account_code'].'&quot;,&quot;&quot;)" ><i data-toggle="tooltip" data-placement="right" title="Detil">'.$datas['cost_center'].'</i></label>';
					$result[] = $datas['cost_center'];
					// die($datas['gl_account']);
					if ($header == "nama_biaya" || $header == "gl_account_description") {
						$result[] = $datas['biaya'];
					}
					if ($header == "gl_account_description") {
						$result[] = $datas['gl_account'];
					}
					$result[] = $this->rupiahnormal($datas['anggaran']);
					$result[] = $this->rupiahnormal($datas['sisa_expense']);
					$result[] = $this->rupiahnormal($datas['realisasi']);
					$result[] = $this->rupiahnormal($datas['last_year']);
					$result[] = $this->rupiahnormal($datas['achievement']);
					$result[] = $this->rupiahnormal($datas['growth']);
					$result[] = $this->rupiahnormal($datas['share_realisasi_this_year']);
					$result[] = $this->rupiahnormal($datas['share_realisasi_last_year']);
					$result[] = $datas['gl_account_code'];
					$result[] = $datas['bulan_tahun'];
					$result[] = $datas['cost_center_code'];
					$json_data[] = $result;
				}
				$result = array();
				$result[] = 'Grand Total';
				if ($header == "nama_biaya" || $header == "gl_account_description") {
					$result[] = '';
				};
				if ($header == "gl_account_description") {
					$result[] = '';
				};
				$result[] = $this->rupiahnormal($data['grandtotal']->anggaran);
				$result[] = $this->rupiahnormal($data['grandtotal']->sisa_expense);
				$result[] = $this->rupiahnormal($data['grandtotal']->realisasi_this_year);
				$result[] = $this->rupiahnormal($data['grandtotal']->realisasi_last_year);
				$result[] = $this->rupiahnormal($data['grandtotal']->achievement);
				$result[] = $this->rupiahnormal($data['grandtotal']->growth);
				$result[] = $this->rupiahnormal($data['grandtotal']->share_realisasi_this_year);
				$result[] = $this->rupiahnormal($data['grandtotal']->share_realisasi_last_year);
				$json_data[] = $result;
				$record_total = $size_data;
				$record_filter_total = $size_data;
			} else {
				$result = array();
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] =	'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$result[] = 'empty';
				$json_data[] = $result;
			}
		} else {
			$result = array();
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] =	'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$json_data[] = $result;
		}

		$output = array(
			"draw" => $_GET['draw'],
			"recordsTotal" => $record_total,
			"recordsFiltered" => $record_filter_total,
			"data" => $json_data,
		);
		echo json_encode($output);
	}

	public function getfiltervalue()
	{
		$id = $this->input->post('id');

		$data_tmp = $this->marketing_expences_information_model->getfiltervalue($id);
		$data = $data_tmp;
		echo json_encode($data);
	}



	public function getfiltervalue_biaya()
	{
		$id = $this->input->post('id');
		$lini = json_decode($this->input->post('param'));
		$data_tmp = $this->marketing_expences_information_model->getfiltervalue_biaya($id, $lini);
		$data = $data_tmp;
		echo json_encode($data);
	}

	public function getfiltervalue_uraian()
	{
		$id = $this->input->post('id');
		$biaya = json_decode($this->input->post('param'));
		$data_tmp = $this->marketing_expences_information_model->getfiltervalue_gl($id, $biaya);
		$data = $data_tmp;
		echo json_encode($data);
	}
	public function getListmodal()
	{
		//print_r($this->input->get('datam'));die();
		$record_total = 0;
		$record_filter_total = 0;
		$filter = array();
		$header = array();
		$order	= $this->input->get('order');
		$ordered = array("column" => $order[0]['column'], "dir" => $order[0]['dir']);

		parse_str($this->input->get("f_search"), $filter);


		parse_str($this->input->get("f_head"), $header);
		//	print_r($header);die();
		$filtering['tanggal_faktur_start'] 	= $filter['tanggal_faktur_start'];
		$filtering['tanggal_faktur_end'] 		= $filter['tanggal_faktur_end'];
		$filtering['cost_center'] 					= isset($this->input->get('datam')['cost_center']) ? $this->input->get('datam')['cost_center'] : '';
		$filtering['biaya'] 								= isset($this->input->get('datam')['biaya']) ? $this->input->get('datam')['biaya'] : '';
		$filtering['gl_account'] 						= isset($this->input->get('datam')['gl_account']) ? $this->input->get('datam')['gl_account'] : '';
		$filtering['gpm_pm_code'] 					= isset($this->input->get('datam')['gpm_pm_code']) ? $this->input->get('datam')['gpm_pm_code'] : '';
		$filtering['rsm_shopper_code'] 			= isset($this->input->get('datam')['rsm_shopper_code']) ? $this->input->get('datam')['rsm_shopper_code'] : '';
		//print_r($filtering);die();
		$header =  $header['head_filter'];
		$start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0;
		$limit = $this->input->get("length");
		if (strlen($filter['tanggal_faktur_start']) > 0 && strlen($filtering['tanggal_faktur_end']) > 0 && strlen($header) > 0) {
			$data = $this->marketing_expences_information_model->orderingmodal($start, $limit, $filtering, "data", $ordered, $header);
			//echo "<pre>";print_r($data);"</pre>";die();

			if ($data != false) {
				foreach ($data['data'] as $datas) {
					$result  = array();
					$result[] = $datas['TEXT'];
					$result[] = $this->rupiah($datas['totalanggaran']);
					$json_data[] = $result;
				}
				$record_total 				= $this->marketing_expences_information_model->orderingmodal($start, $limit, $filtering, "numrow", $header);
				$record_filter_total 	    = $this->marketing_expences_information_model->orderingmodal($start, $limit, $filtering, "allrow", $header);
			}
			$output = array(
				"draw" => $_GET['draw'],
				"recordsTotal" => $record_total,
				"recordsFiltered" => $record_filter_total,
				"data" => $json_data
			);
			echo json_encode($output);
		}
	}
	public function downloadexcel()
	{

		$objPHPExcel = new PHPExcel();
		//$objPHPExcel = new PHPExcel();
		$date_create = $this->input->post('filename');
		//$filename = $date_create."-".$action['function'].".xlsx";
		//$filename			= '../xampp/htdocs/kf'.$date_create.'.xls';
		$filename			= realpath(".base_url('').") . 'file/' . $date_create . '.xls';

		//$filename = $filename;

		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"), $filter);
		parse_str($this->input->post("f_head"), $header);
		//print_r($filter['value']);die();


		$header =  $header['head_filter'];

		//print_r($header);die();
		$data = $this->marketing_expences_information_model->get_limit_data(0, 0, $filter, "excel", $header, NULL);
		//$data = $this->sales_growth_information_model->get_limit_data($start,$limit,$filter,"data",$header);
		//echo json_encode($data);
		if ($data) {
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="' . $filename . '"');
			header('Cache-Control: max-age=0');
			$alphabet = array("A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2", "I2", "J2", "K2", "L2", "M2", "N2", "O2", "P2", "Q2", "R2", "S2", "T2", "U2", "V2", "W2", "X2", "Y2", "Z2");
			$alphabet = array_map('trim', $alphabet);
			if ($objPHPExcel) {
				$run  = "0";

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Judul :' . strtoupper($date_create . "/ Bulan awal :" . $filter['bulan_awal'] . '/ Bulan Akhir :' . $filter['bulan_akhir'] . '/ Tahun :' . $filter['tahun']));
				foreach ($data[0] as $key => $value) {
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$run], $key);
					$run++;
				}

				foreach ($data as $key => $value) {
					$listing[] = $value;
				}
				$rows_numb = 3;
				for ($number_each_for_value = 0; $number_each_for_value < $number_array = count($data); $number_each_for_value++) {
					$col = 'A';
					foreach ($listing[$number_each_for_value] as $key => $value) {
						if ($header['head-filter'] == 'Month' && $key == 'nama') {
							$objPHPExcel->getActiveSheet()->setCellValueExplicit($col . $rows_numb, ucwords($this->support->bulan($value)));
						} else {
							$objPHPExcel->getActiveSheet()->setCellValueExplicit($col . $rows_numb, $value);
							//$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
						}

						$col++;
					}
					$rows_numb++;
				}
				$rownumber = $rows_numb + 1;
				$lastrow = 'A' . $rownumber;

				$objPHPExcel->getActiveSheet()->setTitle('Data');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save($filename);
			} else {
				die("library not found");
			}
		}
	}
	public function rupiah($angka)
	{

		$hasil_rupiah = number_format($angka, 2, ',', '.');
		return $hasil_rupiah;
	}
	public function rupiahnormal($angka)
	{

		$hasil_rupiah = number_format($angka, 0, ',', '.');
		return $hasil_rupiah;
	}

	public function getDetailExpen()
	{
		$tgl_account_code	= $this->input->get('gl_account_code');
		$start	= $this->input->get('start');
		$end	= $this->input->get('end');
		$cost_center_code	= $this->input->get('cost_center_code');

		$result = $this->marketing_expences_information_model->getDetailExpen($tgl_account_code, $start, $end, $cost_center_code);
		if ($result != false) {
			foreach ($result as $datas) {
				$result_ = array();
				$result_["enter_document"] = $datas["enter_document"];
				$result_["cost_center"] = $datas["cost_center"];
				$result_["document_number"] = $datas["document_number"];
				$result_["line_item"] = $datas["line_item"];
				$result_["expense_ty"] = $datas["expense_ty"];
				$result_["text"] = $datas["text"];
				$json_data[] = $result_;
			}
		} else {
			$result_ = array();
			$result_["enter_document"] = 'empty';
			$result_["cost_center"] = 'empty';
			$result_["document_number"] = 'empty';
			$result_["line_item"] = 'empty';
			$result_["expense_ty"] = 'empty';
			$result_["text"] = 'empty';
			$json_data[] = $result_;
		}

		$output = array(
			"data" => $json_data
		);
		echo json_encode($output);
	}
}
