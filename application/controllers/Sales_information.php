<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_information extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array("Sales_information_model"));
    }
    public function index()
    {
        //echo 4;die();
        //$data['data'] 			= $this->getdatagrid();
        $data['url']                        = base_url('index.php/Sales_information');
        $data['mycontroller']        = 'Sales_information';
        $data['opration']            = true;
        $data['filter']['page']     = 'sales_information';

        $this->load->view('templates/header', $data);
        $this->load->view('sales_information/home', $data);
        $this->load->view('templates/footer_main', $data);
    }

    public function menudetail()
    {
        //echo 4;die();
        //$data['data'] 			= $this->getdatagrid();
        $data['url']                        = base_url('index.php/Sales_information');
        $data['mycontroller']        = 'Sales_information';
        echo json_encode($data);
    }
    public function getdatabar()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //print_r($filter);die();

        $header =  $header['head_filter']; //die();
        echo json_encode($this->Sales_information_model->getdatabar($filter, $header, $value));
    }
    
    public function getList()
    {

        ini_set('memory_limit', '-1');
        $record_total = 0;
        $totals = 0;
        $record_filter_total = 0;
        $filter = array();
        $header = array();
        $order    = $this->input->get('order');
        $ordered = array("column" => $order[0]['column'], "dir" => $order[0]['dir']);

        parse_str($this->input->get("f_search"), $filter);


        parse_str($this->input->get("f_head"), $header);
        $value = $filter['value'];
        //print_r($header);die();
        $header =  $header['head_filter'];
        $start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0;
        //$limit = $this->input->get("length");
        $distributor = "";
        // if (isset($filter['distributor_code'])){

        //     if (strlen($filter['distributor_code']) > 0){
        //         $distributor = $filter['distributor_code'];
        //     }
        // }
        $limit = -1;
        if (strlen($filter['tanggal_faktur_start']) > 0 && strlen($filter['tanggal_faktur_end']) > 0 && strlen($value) > 0  && strlen($header) > 0) {
            // $data_noo = $this->Sales_information_model->ordering_noo($start,$limit,$filter,"data",$ordered,$header,$value);
            //            die();
            $data = $this->Sales_information_model->ordering($start, $limit, $filter, "data", $ordered, $header, $value);            
            // $data_last = $this->Sales_information_model->ordering_last($start,$limit,$filter,"data",$ordered,$header,$value);

            //            print_r($data_noo);
            //            print_r($data);
            //            print_r($data_last);            
            $i = 0;
            $is_noo = 0;
            $is_growth = 0;
            // $noo_size = sizeof($data_noo['data']);
            $data_size = sizeof($data["data"]);
            // $data_last_size = sizeof($data_last["data"]);
            if ($data != false) {
                if ($data_size != 0) {
                    foreach ($data['data'] as $datas) {

                        $result = array();
                        $result['nama'] = $header == 'Month' ? ucwords($this->support->bulan($datas['nama'])) . " " . $datas['tahun'] : $datas['nama'];
                        // $result[] = $this->rupiahnormal($datas['target_qty_ty']);
                        // $result[] = $this->rupiahnormal($datas['target_amount_ty']);
                        $result["code"] = $datas['code'];
                        if ($value == 'HNA_TY' || $value == 'HJP_PTD_TY' || $value == 'HJP_TY' || $value == 'HPP_TY') {
                            $result["qty"] = (float)$this->rupiahnormal($datas['qty_hna']);
                            $result["target"] = (float)$this->rupiahnormal($datas['target_hna']);
                        } else if ($value == 'HJD_TY' || $value == 'HNA_PTD_TY') {
                            $result["qty"] = (float)$this->rupiahnormal($datas['qty_hna_nett']);
                            $result["target"] = (float)$this->rupiahnormal($datas['target_hna_nett']);
                        }
                        $result["realisasiqtyty"] = (float)$this->rupiahnormal($datas['realisasi_qty_ty']);
                        $result["realisasity"] = (float)$this->rupiahnormal($datas['realisasi_ty']);
                        $result["realisasiqtyly"] = (float)$this->rupiahnormal($datas['realisasi_qty_ly']);
                        $result["realisasily"] = (float)$this->rupiahnormal($datas['realisasi_ly']);
                        $result["ptd"] = (float)$this->rupiahnormal($datas['ptd']);                        
                        if ($value == 'HNA_TY' || $value == 'HJP_PTD_TY' || $value == 'HJP_TY' || $value == 'HPP_TY') {
                            $result["achievement"] = (float)$this->rupiah($datas['achievement_hna']);
                        } else if ($value == 'HJD_TY' || $value == 'HNA_PTD_TY') {
                            $result["achievement"] = (float)$this->rupiah($datas['achievement_hna_nett']);
                        }
                        $result["growth"] = (float)$this->rupiah($datas['growth']);
                        $result["share_realisasi_ty"] = (float)$this->rupiah($datas['share_realisasi_ty']);
                        $result["share_realisasi_ly"] = (float)$this->rupiah($datas['share_realisasi_ly']);
                        $result["margin_marketing_amount"] = (float)$this->rupiah($datas['margin_marketing_amount']);
                        $result["margin_marketing_persen"] = (float)$this->rupiah($datas['margin_marketing_persen']);
                        $result["margin_distributor_amount"] = (float)$this->rupiah($datas['margin_distributor_amount']);
                        $result["margin_distributor_persen"] = (float)$this->rupiah($datas['margin_distributor_persen']);
                        $result["noo"] = (float)$datas['noo'];
                        $result["npp"] = (float)$datas['npp'];
                        $result["otr"] = (float)$datas['otr'];
                        // $result[] = $this->rupiah($datas['margin_marketing_amount']);
                        // $result[] = $this->rupiah($datas['margin_marketing_persen']);
                        // $result[] = $this->rupiah($datas['margin_distributor_amount']);
                        // $result[] = $this->rupiah($datas['margin_distributor_persen']);
                        $json_data[] = $result;

                        $i++;
                    }



                    $total = array();
                    $total[] = 'Grand Total';
                    if ($value == 'HNA_TY' || $value == 'HJP_PTD_TY' || $value == 'HJP_TY' || $value == 'HPP_TY') {
                        $total[] = (float)$this->rupiah($data['grandtotal']->qty_hna);
                        $total[] = (float)$this->rupiah($data['grandtotal']->target_hna);
                    } else if ($value == 'HJD_TY' || $value == 'HNA_PTD_TY') {
                        $total[] = (float)$this->rupiah($data['grandtotal']->qty_hna_nett);
                        $total[] = (float)$this->rupiah($data['grandtotal']->target_hna_nett);
                    }
                    $total[] = (float)$this->rupiah($data['grandtotal']->realisasi_qty_ty);
                    $total[] = (float)$this->rupiah($data['grandtotal']->realisasi_ty);
                    $total[] = (float)$this->rupiah($data['grandtotal']->realisasi_qty_ly);
                    $total[] = (float)$this->rupiah($data['grandtotal']->realisasi_ly);
                    $total[] = (float)$this->rupiah($data['grandtotal']->ptd);
                    // $total[] = (float)$this->rupiah($data['grandtotal']->achievement);
                    if ($value == 'HNA_TY' || $value == 'HJP_PTD_TY' || $value == 'HJP_TY' || $value == 'HPP_TY') {
                        $total[] = (float)$this->rupiah($data['grandtotal']->achievement_hna);
                    } else if ($value == 'HJD_TY' || $value == 'HNA_PTD_TY') {
                        $total[] = (float)$this->rupiah($data['grandtotal']->achievement_hna_nett);
                    }
                    $total[] = (float)$this->rupiah($data['grandtotal']->growth);
                    $total[] = (float)$this->rupiah($data['grandtotal']->share_realisasi_ty);
                    $total[] = (float)$this->rupiah($data['grandtotal']->share_realisasi_ly);
                    $total[] = (float)$this->rupiah($data['grandtotal']->margin_marketing_amount);
                    $total[] = (float)$this->rupiah($data['grandtotal']->margin_marketing_persen);
                    $total[] = (float)$this->rupiah($data['grandtotal']->margin_distributor_amount);
                    $total[] = (float)$this->rupiah($data['grandtotal']->margin_distributor_persen);
                    $total[] = (float)$data['grandtotal']->noo;
                    $total[] = (float)$data['grandtotal']->npp;
                    $total[] = (float)$data['grandtotal']->otr;
                    $totals = $total;
                    $record_total                 = $data_size;
                    $record_filter_total     = $data_size;
                } else {
                    $result = array();
                    $result['nama'] = 'empty';
                    $result["code"] = 'empty';
                    $result["qty"] = 'empty';
                    $result["target"] = 'empty';
                    $result["realisasiqtyty"] = 'empty';
                    $result["realisasity"] = 'empty';
                    $result["realisasiqtyly"] = 'empty';
                    $result["realisasily"] = 'empty';
                    $result["ptd"] = 'empty';
                    $result["achievement"] = 'empty';
                    $result["growth"] = 'empty';
                    $result["share_realisasi_ty"] = 'empty';
                    $result["share_realisasi_ly"] = 'empty';
                    $result["margin_marketing_amount"] = 'empty';
                    $result["margin_marketing_persen"] = 'empty';
                    $result["margin_distributor_amount"] = 'empty';
                    $result["margin_distributor_persen"] = 'empty';
                    $result["noo"] = 'empty';
                    $result["npp"] = 'npp';
                    $result["otr"] = 'empty';


                    $json_data[] = $result;
                }
            } else {
                $result = array();
                $result['nama'] = 'empty';
                    $result["code"] = 'empty';
                    $result["qty"] = 'empty';
                    $result["target"] = 'empty';
                    $result["realisasiqtyty"] = 'empty';
                    $result["realisasity"] = 'empty';
                    $result["realisasiqtyly"] = 'empty';
                    $result["realisasily"] = 'empty';
                    $result["ptd"] = 'empty';
                    $result["achievement"] = 'empty';
                    $result["growth"] = 'empty';
                    $result["share_realisasi_ty"] = 'empty';
                    $result["share_realisasi_ly"] = 'empty';
                    $result["margin_marketing_amount"] = 'empty';
                    $result["margin_marketing_persen"] = 'empty';
                    $result["margin_distributor_amount"] = 'empty';
                    $result["margin_distributor_persen"] = 'empty';
                    $result["noo"] = 'empty';
                    $result["npp"] = 'empty';
                    $result["otr"] = 'empty';


                $json_data[] = $result;
            }
        } else {
            $result = array();
            $result['nama'] = 'empty';
            $result["code"] = 'empty';
            $result["qty"] = 'empty';
            $result["target"] = 'empty';
            $result["realisasiqtyty"] = 'empty';
            $result["realisasity"] = 'empty';
            $result["realisasiqtyly"] = 'empty';
            $result["realisasily"] = 'empty';
            $result["ptd"] = 'empty';
            $result["achievement"] = 'empty';
            $result["growth"] = 'empty';
            $result["share_realisasi_ty"] = 'empty';
            $result["share_realisasi_ly"] = 'empty';
            $result["margin_marketing_amount"] = 'empty';
            $result["margin_marketing_persen"] = 'empty';
            $result["margin_distributor_amount"] = 'empty';
            $result["margin_distributor_persen"] = 'empty';
            $result["noo"] = 'empty';
            $result["npp"] = 'empty';
            $result["otr"] = 'empty';
            $json_data[] = $result;
        }

        $output = array(
            // "draw" =>$_GET['draw'],
            "draw" => "1",
            "recordsTotal" => $record_total,
            "recordsFiltered" => $record_filter_total,            
            "Grand_Total" => $totals,
            "data" => $json_data
        );
        echo json_encode($output);
    }

    public function ordering($order, $header)
    {
        //print_r($header);die();
        $this->Sales_information_model->ordering($order, $header);
    }
    // public function downloadexcel(){
    //     $objPHPExcel = new PHPExcel();
    //     //$objPHPExcel = new PHPExcel();
    //     $date_create = $this->input->post('filename');
    //     //$filename = $date_create."-".$action['function'].".xlsx";
    //     //$filename			= '../xampp/htdocs/kf'.$date_create.'.xls';
    //     $filename			= realpath(".base_url('').").'file/'.$date_create.'.xls';

    //     //$filename = $filename;

    //     $filter = array();
    //     $header = array();
    //     parse_str($this->input->post("f_search"),$filter);
    //     parse_str($this->input->post("f_head"),$header);
    //     //print_r($filter['value']);die();


    //     $header =  $header['head_filter'];

    //     //print_r($header);die();
    //     $data = $this->Sales_information_model->get_limit_data(NULL , NULL , NULL,'excel',NULL,NULL,NULL);
    //     //$data = $this->Sales_information_model->get_limit_data($start,$limit,$filter,"data",$header);
    //     //echo json_encode($data);
    //     if($data){
    //         header('Content-Type: application/vnd.ms-excel');
    //         header('Content-Disposition: attachment;filename="'.$filename.'"');
    //         header('Cache-Control: max-age=0');
    //         $alphabet = array("A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1","U1","V1","W1","X1","Y1","Z1");
    //         $alphabet = array_map('trim', $alphabet);
    //         if($objPHPExcel){
    //             $run  = "0";
    //             foreach($data[0] as $key => $value){
    //                 $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$run], $key);
    //                 $run++;
    //             }

    //             foreach($data as $key =>$value){
    //                 $listing[] = $value;
    //             }
    //             $rows_numb = 2;
    //             for($number_each_for_value = 0;$number_each_for_value < $number_array = count($data);$number_each_for_value++){
    //                 $col = 'A';
    //                 foreach ($listing[$number_each_for_value] as $key => $value) {
    //                     if($header['head-filter']=='Month' && $key == 'nama'){
    //                         $objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
    //                     }else{
    //                         $objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,$value);
    //                         //$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
    //                     }

    //                     $col++;
    //                 }
    //                 $rows_numb++;
    //             }

    //             $objPHPExcel->getActiveSheet()->setTitle('Data');
    //             $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    //             $objWriter->save($filename);
    //         }else{
    //             die("library not found");
    //         }
    //     }

    // }
    public function downloadexcel()
    {
        //print_r($this->input->post());die();
        $objPHPExcel = new PHPExcel();
        //$objPHPExcel = new PHPExcel();
        $date_create = $this->input->post('filename');
        //$filename = $date_create."-".$action['function'].".xlsx";
        //$filename			= '../xampp/htdocs/kf'.$date_create.'.xls';	
        $filename            = realpath(".base_url('').") . 'file/' . $date_create . '.xls';

        //$filename = $filename;

        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        //print_r($filter['value']);die();


        $header =  $header['head_filter'];

        //print_r($header);die();
        $data = $this->Sales_information_model->get_limit_data(0, 0, $filter, "excel", $header, NULL);
        //$data = $this->sales_growth_information_model->get_limit_data($start,$limit,$filter,"data",$header);
        //echo json_encode($data);
        if ($data) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $alphabet = array("A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2", "I2", "J2", "K2", "L2", "M2", "N2", "O2", "P2", "Q2", "R2", "S2", "T2", "U2", "V2", "W2", "X2", "Y2", "Z2");
            $alphabet = array_map('trim', $alphabet);
            if ($objPHPExcel) {
                $run  = "0";

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Periode : " . $filter['tanggal_faktur_start'] . ' s/d ' . $filter['tanggal_faktur_start'] . '');
                foreach ($data[0] as $key => $value) {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$run], $key);
                    $run++;
                }

                foreach ($data as $key => $value) {
                    $listing[] = $value;
                }
                $rows_numb = 3;
                for ($number_each_for_value = 0; $number_each_for_value < $number_array = count($data); $number_each_for_value++) {
                    $col = 'A';
                    foreach ($listing[$number_each_for_value] as $key => $value) {
                        if ($header['head-filter'] == 'Month' && $key == 'nama') {
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit($col . $rows_numb, ucwords($this->support->bulan($value)));
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit($col . $rows_numb, $value);
                            //$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
                        }

                        $col++;
                    }
                    $rows_numb++;
                }
                $rownumber = $rows_numb + 1;
                $lastrow = 'A' . $rownumber;

                $objPHPExcel->getActiveSheet()->setTitle('Data');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save($filename);
            } else {
                die("library not found");
            }
        }
    }
    public function getdatagrid()
    {
        $data = $this->Sales_information_model->getdatagrid("month");
        return $data;
    }
    public function targetqty()
    {

        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        //print_r($header);die();
        $value = $filter['value'];
        //	echo $this->Sales_information_model->targetqty($filter,$header,$value);
        //echo ;die();
        //echo json_encode($this->formatnominal($this->Sales_information_model->targetqty($filter,$header,$value)));
        echo json_encode($this->rupiahnormal(round(round(($this->Sales_information_model->targetqty($filter, $header, $value)), 2))));
    }
    public function targetvalue()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //echo json_encode();
        echo json_encode("Rp." . $this->formatnominal($this->Sales_information_model->targetvalue($filter, $header, $value)));
    }
    public function realisasiqty()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];

        //echo json_encode($this->formatnominal($this->Sales_information_model->realisasiqty($filter,$header,$value)));
        //echo json_encode($this->rupiah(round(round(($this->Sales_information_model->targetqty($filter,$header,$value)) , 2))));
        echo json_encode($this->rupiahnormal(round(round(($this->Sales_information_model->realisasiqty($filter, $header, $value)), 2))));
    }
    public function realisasivalue()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        echo json_encode("Rp." . $this->formatnominal($this->Sales_information_model->realisasivalue($filter, $header, $value)));
    }
    public function lastyearqty()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];


        //echo json_encode($this->formatnominal($this->Sales_information_model->lastyearqty($filter,$header,$value)));
        //echo json_encode($this->rupiah(round(($this->Sales_information_model->lastyearqty($filter,$header,$value)) , 2)));
        echo json_encode($this->rupiahnormal(round(round(($this->Sales_information_model->lastyearqty($filter, $header, $value)), 2))));
    }
    public function lastyearvalue()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //print_r();die();
        //echo json_encode($this->formatnominal($this->Sales_information_model->lastyearvalue($filter,$header,$value)));
        echo json_encode("Rp." . $this->formatnominal($this->Sales_information_model->lastyearvalue($filter, $header, $value)));
    }
    public function ptd()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //echo json_encode($this->formatnominal($this->Sales_information_model->ptd($filter,$header,$value)));
        echo json_encode("Rp." . $this->formatnominal($this->Sales_information_model->ptd($filter, $header, $value)));
    }
    public function achievement()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //	echo $this->Sales_information_model->achievement($filter,$header,$value)."</br>";
        //	echo $this->formatnominal($this->Sales_information_model->achievement($filter,$header,$value));

        echo json_encode($this->rupiah(round(($this->Sales_information_model->achievement($filter, $header, $value)), 2)));
    }
    public function growth()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        echo json_encode($this->rupiah(round(($this->Sales_information_model->growth($filter, $header, $value)), 2)));
    }
    public function noo()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //echo json_encode($this->Sales_information_model->noo($filter,$header,$value));
        //echo json_encode($this->formatnominal($this->Sales_information_model->noo($filter,$header,$value)));
        //echo json_encode($this->rupiah(round(($this->Sales_information_model->noo($filter,$header,$value)) , 2)));
        echo json_encode($this->Sales_information_model->noo($filter, $header, $value));
    }
    public function otr()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //echo json_encode($this->Sales_information_model->otr($filter,$header,$value));
        //echo json_encode($this->formatnominal($this->Sales_information_model->otr($filter,$header,$value)));

        echo json_encode($this->Sales_information_model->otr($filter, $header, $value));
    }
    public function margin()
    {
        $filter = array();
        $header = array();
        parse_str($this->input->post("f_search"), $filter);
        parse_str($this->input->post("f_head"), $header);
        $value = $filter['value'];
        //echo json_encode($this->formatnominal($this->Sales_information_model->margin($filter,$header,$value)));
        echo json_encode("Rp." . $this->formatnominal($this->Sales_information_model->margin($filter, $header, $value)));
    }

    public function getfiltervalue()
    {
        $id = $this->input->post('id');
        $param = json_decode($this->input->post('param'));
        $name = $this->input->post('name');
        $data_tmp = $this->Sales_information_model->getfiltervalue($id, $param, $name);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_am_apm_asm()
    {
        $id = $this->input->post('id');
        $distributor = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_am_apm_asm($id, $distributor);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_branch()
    {
        $id = $this->input->post('id');
        $distributor = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_branch($id, $distributor);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_gm()
    {
        $id = $this->input->post('id');
        $gm = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_gm($id, $gm);
        $data = $data_tmp;
        echo json_encode($data);
    }
    public function getfiltervalue_group1()
    {
        $id = $this->input->post('id');
        $gm = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_group1($id, $gm);
        $data = $data_tmp;
        echo json_encode($data);
    }
    public function getfiltervalue_group2()
    {
        $id = $this->input->post('id');
        $gm = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_group2($id, $gm);
        $data = $data_tmp;
        echo json_encode($data);
    }
    public function getfiltervalue_group3()
    {
        $id = $this->input->post('id');
        $gm = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_group3($id, $gm);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_customer()
    {
        $id = $this->input->post('id');
        $segment = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_customer($id, $segment);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_layanan()
    {
        $id = $this->input->post('id');
        $customer = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_layanan($id, $customer);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_lini()
    {
        $id = $this->input->post('id');
        $lini = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_lini($id, $lini);
        $data = $data_tmp;
        echo json_encode($data);
    }


    public function getfiltervalue_product()
    {
        $id = $this->input->post('id');
        $brand = json_decode($this->input->post('param'));
        $data_tmp = $this->Sales_information_model->getfiltervalue_product($id, $brand);
        $data = $data_tmp;
        echo json_encode($data);
    }


    function delete_cache()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->cache->clean();
    }
    function info_cache()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        echo "<pre>";
        print_r($this->cache->cache_info());
        echo "</pre>";
    }
    public function rupiah($angka)
    {
        if ($angka === '-') {
            return $angka;
         } else {
            $hasil_rupiah = number_format($angka, 2, '.', '');
            return $hasil_rupiah;
        };
    }
    public function rupiahnormal($angka)
    {
        if ($angka === '-') {
            return $angka;
        } else {
            $hasil_rupiah = number_format($angka, 0, '.', '');
            return $hasil_rupiah;
        }
    }
    public function formatnominal($angka)
    {
        $angka = explode('.', $angka);
        $totalangka = strlen($angka[0]);
        $angka[0]; //die();
        switch ($totalangka) {
            case 2:
                return $this->rupiah($angka[0]);
                break;
            case 3:
                return $this->rupiah($angka[0]);
                break;
            case 4:
                return $this->rupiah($angka[0]);
                break;
            case 5:
                return $this->rupiah($angka[0]);
                break;
            case 6:
                return $this->rupiah($angka[0]);
                break;
            case 7:
                return substr($angka[0], 0, 1) . "," . substr($angka[0], 1, 2) . " Juta";
                break;
            case 8:
                return substr($angka[0], 0, 2) . "," . substr($angka[0], 2, 2) . " Juta";
                break;
            case 9:
                return substr($angka[0], 0, 3) . "," . substr($angka[0], 3, 2) . " Juta";
                break;
            case 10:
                return substr($angka[0], 0, 1) . "," . substr($angka[0], 1, 2) . " Milyar";
                break;
            case 11:
                return substr($angka[0], 0, 2) . "," . substr($angka[0], 2, 2) . " Milyar";
                break;
            case 12:
                return substr($angka[0], 0, 3) . " Milyar";
                break;
            case 13:
                return substr($angka[0], 0, 1) . " Triliun";
                break;
            case 14:
                return substr($angka[0], 0, 2) . " Triliun";
                break;
            case 15:
                return substr($angka[0], 0, 3) . " Triliun";
                break;

            default:
                return $angka[0] . ",00";
        }
        //return 	$totalangka;
    }
}
//21.813.274
//21.813.274
