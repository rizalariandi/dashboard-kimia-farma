<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Learning_growth extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Menu_management/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'demografi')
    {

        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['level1'] = $this->user_model->getLevel1();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getMenuLevel()
    {
        $data = $this->user_model->getMenuLevel();
        echo json_encode($data);
    }

    public function getMenuLevel2()
    {
        $level = json_decode($this->input->get_post('level1'));

        $data['data'] = $this->user_model->getLevel2($level);
        echo json_encode($data);
    }

    public function getMenuLevel3()
    {
        $level = json_decode($this->input->get_post('level2'));

        $data['data'] = $this->user_model->getLevel3($level);
        echo json_encode($data);
    }

    public function updateLevel()
    {
        $nama = $this->input->get_post('nama');
        $level = json_decode($this->input->get_post('level'));

        $data = $this->user_model->DelLevel($_SESSION['nama_level']);

        foreach ($level->level as $key => $value) {
            $id_menu = $this->user_model->getIdMenu($value);
            $data = $this->user_model->add_level($nama, $id_menu[0], $this->session->userdata('nama'));
        }

        echo json_encode($data);
    }

    public function getUpdateMenuLevel()
    {
        $nama = $this->input->get_post('nama');
        $_SESSION['nama_level'] = $nama;
        $id_menu = $this->user_model->getIdMenuUpdate($nama);

        $data['id_menu'] = $id_menu;
        $data['level1_update'] = $this->user_model->getUpdateLevel1($id_menu, $nama);
        $data['level2_update'] = $this->user_model->getUpdateLevel2($id_menu, $nama);
        $data['level3_update'] = $this->user_model->getUpdateLevel3($id_menu, $nama);
        $data['level1'] = $this->user_model->getLevel1Full($id_menu, $nama);
        $data['level2'] = $this->user_model->getLevel2Full($id_menu, $nama);
        $data['level3'] = $this->user_model->getLevel3Full($id_menu, $nama);

        echo json_encode($data);
    }

    public function DelLevel()
    {
        $level = $this->input->get_post('level');
        $data = $this->user_model->DelLevel($level);
        echo json_encode($data);
    }

    public function refresh_demografi()
    {
        ini_set('memory_limit', '-1');
        $this->load->model('demografi_model');
        $year = json_decode($this->input->get_post('year'));
        $month = json_decode($this->input->get_post('month'));
        $entitas = json_decode($this->input->get_post('entitas'));

        $data['sum_pendidikan'] = $this->demografi_model->sum_pendidikan($month, $year, $entitas);
        $data['sum_usia'] = $this->demografi_model->sum_usia($month, $year, $entitas);
        $data['sum_jabatan'] = $this->demografi_model->sum_jabatan($month, $year, $entitas);
        $data['sum_masakerja'] = $this->demografi_model->sum_masakerja($month, $year, $entitas);
        echo json_encode($data);
    }
    public function register()
    {
        $nama = $this->input->get_post('nama');
        $username = $this->input->get_post('username');
        $email = $this->input->get_post('email');
        $password = $this->input->get_post('password');
        $hp = $this->input->get_post('hp');
        $unit = $this->input->get_post('unit');
        $level = $this->input->get_post('level');
        $jabatan = $this->input->get_post('jabatan');
        $nik = $this->input->get_post('nik');
        $active = $this->input->get_post('active');
        $data = $this->user_model->register($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active);
        echo json_encode($data);
    }

    public function add_level()
    {
        $nama = $this->input->get_post('nama');
        $level = json_decode($this->input->get_post('level'));

        foreach ($level->level as $key => $value) {
            $id_menu = $this->user_model->getIdMenu($value);
            $data = $this->user_model->add_level($nama, $id_menu[0], $this->session->userdata('nama'));
        }

        echo json_encode($data);
    }
}
