<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insightful_Me extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->model('user_model');
        $this->load->model('insightful_model', 'dm');
        $this->load->model('insightful_me_model', 'imm');
        $this->load->library('datatables');

        $this->load->helper('form');

        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}

	public function chart()
    {

        $lini = $this->input->post('lini');
        $biaya = $this->input->post('biaya');
        $pos = $this->input->post('pos');
        $start = $this->input->post('start');
        $end = $this->input->post('end');

        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $s = explode('-', $start);
        $e = explode('-', $end);

        $dbs = $this->dm->get_realisasi_expenses($s[0], $e[0], $s[1], $lini)->result();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        $charts = array();
        $lini_array = array();
        foreach($dbs as $db) {
            if(empty($charts[$db->bulan])) {
                $charts[$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
            } else {
                $charts[$db->bulan]['expenses'] += (int) $db->expenses;
                $charts[$db->bulan]['realisasi'] += (int) $db->realisasi;
            }

            $lini_array[$db->lini][$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
        }

        $array = array();
        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($charts[$b])) {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = 0;
                $array['realisasi'][$b] = 0;
                $array['ratio'][$b] = 0;
            } else {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = $charts[$b]['expenses'];
                $array['realisasi'][$b] = $charts[$b]['realisasi'];
                $array['ratio'][$b] = !empty($charts[$b]['realisasi']) ? round(($charts[$b]['expenses'] / $charts[$b]['realisasi'])*100, 2) : 0;
            }
        }

        $json[] = array('type' => 'column', 'name' => 'Marketing Expense', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#69d0ef', 'data' => array_values($array['expenses']));
        $json[] = array('type' => 'column', 'name' => 'Realisasi Sales', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#ab82c4', 'data' => array_values($array['realisasi']));
        $json[] = array('type' => 'spline', 'name' => 'Rasio Realisasi Expense', 'yAxis' => 1, 'tooltip' => array('valueSuffix' => ' %'), 'color' => '#db2347', 'data' => array_values($array['ratio']));

        $data['ex_data'] = json_encode($json);
        $data['ex_bln'] = json_encode(array_values($array['month']));

        echo json_encode($data);
    }

    public function index($start='', $end='', $lini='all', $biaya='all', $pos='all')
    {
        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $s = explode('-', $start);
        $e = explode('-', $end);

        //$dbs = $this->dm->get_realisasi_expenses($s[0], $e[0], $s[1], str_replace('_', ' ', $lini))->result();
      
        //$data['area'] = $id;
        //$data['charts'] = array();

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        /*$charts = array();
        $lini_array = array();
        foreach($dbs as $db) {
            if(empty($charts[$db->bulan])) {
                $charts[$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
            } else {
                $charts[$db->bulan]['expenses'] += (int) $db->expenses;
                $charts[$db->bulan]['realisasi'] += (int) $db->realisasi;
            }

            $lini_array[$db->lini][$db->bulan] = array('expenses' => (int) $db->expenses, 'realisasi' => (int) $db->realisasi);
        }

        $array = array();
        for($b=(int) $s[0]; $b<=(int) $e[0]; $b++) {
            if(!isset($charts[$b])) {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = 0;
                $array['realisasi'][$b] = 0;
                $array['ratio'][$b] = 0;
            } else {
                $array['month'][$b] = $bln[$b];
                $array['expenses'][$b] = $charts[$b]['expenses'];
                $array['realisasi'][$b] = $charts[$b]['realisasi'];
                $array['ratio'][$b] = !empty($charts[$b]['realisasi']) ? round(($charts[$b]['expenses'] / $charts[$b]['realisasi'])*100, 2) : 0;
            }
        }

        $json[] = array('type' => 'column', 'name' => 'Marketing Expense', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#69d0ef', 'data' => array_values($array['expenses']));
        $json[] = array('type' => 'column', 'name' => 'Realisasi Sales', 'yAxis' => 0, 'tooltip' => array('valuePrefix' => 'Rp. '), 'color' => '#ab82c4', 'data' => array_values($array['realisasi']));
        $json[] = array('type' => 'spline', 'name' => 'Rasio Realisasi Expense', 'yAxis' => 1, 'tooltip' => array('valueSuffix' => ' %'), 'color' => '#db2347', 'data' => array_values($array['ratio']));

        $data['ex_data'] = json_encode($json);
        $data['ex_bln'] = json_encode(array_values($array['month']));


        $data['lini_data'] = $lini_array;
        $data['lini_bln'] = $array['month'];*/

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];

        $data['var_lini'] = $lini;
        $data['var_biaya'] = $biaya;
        $data['var_pos'] = $pos;
        // $data['var_lini2'] = $lini2;

        $lini_dbs = $this->imm->get_lini_expanse()->result();
        //$data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][str_replace(' ', '_', strtolower($ld->lini))] = strtoupper($ld->lini);
        }

        // $lini2_dbs = $this->imm->get_lini_me_info()->result();
        // $data['lini2_filter']['all'] = 'Semua Lini';
        // foreach($lini2_dbs as $ld2) {
        //     $data['lini2_filter'][str_replace(' ', '_', strtolower($ld2->lini))] = strtoupper($ld2->lini);
        // }

        $biaya_dbs = $this->db->query("SELECT beban_biaya FROM usc_insight_expense_info GROUP BY beban_biaya ORDER BY beban_biaya ASC")->result();
        //$data['biaya_filter']['all'] = 'Semua Beban Biaya';
        foreach($biaya_dbs as $bd) {
            $data['biaya_filter'][str_replace(' ', '_', strtolower($bd->beban_biaya))] = strtoupper($bd->beban_biaya);
        }

        $pos_dbs = $this->db->query("SELECT pos_rekening FROM usc_insight_expense_info GROUP BY pos_rekening ORDER BY pos_rekening ASC")->result();
        //$data['pos_filter']['all'] = 'Semua Pos Rekening';
        foreach($pos_dbs as $pd) {
            $data['pos_filter'][str_replace(' ', '_', strtolower($pd->pos_rekening))] = strtoupper($pd->pos_rekening);
        }
        $where = '';
        $biaya_where      = str_replace("_"," ",$biaya);
        $pos_where      = str_replace("_"," ",$pos);
      
        if(!empty($biaya) and $biaya!="all"){
         $where.="where beban_biaya='$biaya_where'";
         if(!empty($pos) and $pos!="all") $where.=" and pos_rekening='$pos_where'";
        } else {
        if(!empty($pos) and $pos!="all") $where.="where pos_rekening='$pos_where'";
        }

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful/marketing_realisasi';
        $data['title']	= 'Marketing Expense vs Realisasi Sales';

        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful_me/index2', $data);
        $this->load->view('templates/footer', $data);
    }

    public function profit_loss($start='', $end='', $lini='all')
    {
        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $s = explode('-', $start);
        $e = explode('-', $end);

        $bln['1'] = 'Januari';
        $bln['2'] = 'Februari';
        $bln['3'] = 'Maret';
        $bln['4'] = 'April';
        $bln['5'] = 'Mei';
        $bln['6'] = 'Juni';
        $bln['7'] = 'Juli';
        $bln['8'] = 'Agustus';
        $bln['9'] = 'September';
        $bln['10'] = 'Oktober';
        $bln['11'] = 'November';
        $bln['12'] = 'Desember';

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];
      
        $data['var_lini'] = $lini;

        $lini_dbs = $this->imm->get_lini_expanse()->result();
        $data['lini_filter']['all'] = 'Semua Lini';
        foreach($lini_dbs as $ld) {
            $data['lini_filter'][str_replace(' ', '_', strtolower($ld->lini))] = strtoupper($ld->lini);
        }



        $data['db'] = $this->imm->get_profit_loss($s[0], $e[0], $s[1], $lini);

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'insightful_me/profit_loss';
        $data['title']	= 'Profit and Loss';

        $this->load->view('templates/yheader', $data);
        $this->load->view('insightful_me/profit_loss', $data);
        $this->load->view('templates/footer', $data);
    }
    private function persen($val)
    {
        return  ( isset($val) ? $val : 0);
    }
    private function parse($col, $array){
        $wlini ='';
      
        if($array != 'all' AND !empty($array)) {
           
            $wlini = ' AND (';
            foreach($array as $l){
                    $replace = str_replace("_", " ",$l);
                    $wlini .= $col." = '".strtolower($replace)."' OR ";
            }
            $wlini = substr($wlini,0,-4);
            $wlini .= ')';
         

    }
        return $wlini;
    }

    public function json_me()
    {
        if(!$this->input->is_ajax_request()) return false;

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $biaya = $this->input->post('biaya');
        $pos = $this->input->post('pos');
        
        // $lini2	 = $this->input->post('lini2');
        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(
            array( 'db' => 'lini', 'dt' => 0 ),
            array( 'db' => 'pos_rekening', 'dt' => 1 ),           
            array( 'db' => 'beban_biaya', 'dt' => 2 ),            
            array( 'db' => 'budget', 'dt' => 3 ),
            array( 'db' => 'expense', 'dt' => 4 ),
            array( 'db' => 'sisa_expense', 'dt' => 5 ),
            array( 'db' => 'expense_2', 'dt' => 6),
            array( 'db' => 'achieve', 'dt' => 7),
            array( 'db' => 'goodgrowth', 'dt' => 8)
        );

        $this->datatables->set_cols($columns);
        $param	= $this->datatables->query();
        $table  = 'usc_insight_expense_info';

        $w = "tahun1 = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        // if(empty($param['where'])) {
        //     $param['where'] = 'WHERE '.$w;
        // } else {
        //     $param['where'] .= ' AND '.$w;
        // }
        if (empty($param['whereTahun'])){
            $param['whereTahun'] = 'WHERE '.$w;
        }else{
            $param['whereTahun'] = ' AND '.$w;
        }

        $param['tahun'] = (!empty($s[1]) ? $s[1] : date('Y'));
        $param['tahun_sebelum'] = (!empty($s[1]) ? $s[1]-1 : date('Y')-1);
        $param['s_bulan'] = (!empty($s[0]) ? $s[0] : date('m'));
        $param['e_bulan'] = (!empty($e[0]) ? $e[0] : date('m'));

        //if (is_array($lini)){
        if (!empty($lini)){
            //$param['where'] .= " AND (lower(lini) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
            $param['where'] .= "AND lower(lini) IN ".str_replace("_"," ",strtolower("('".implode("','", $lini)."')"));

        }

        // if ($lini2 != 'all'){
        //     //$param['where'] .= " AND (lower(lini) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        //     $param['where'] .= " AND (lower(lini) = '".str_replace("_"," ",strtolower($lini2))."')";
        // }

        if (!empty($biaya)) {
            $param['where'] .= " AND lower(beban_biaya) IN ".str_replace("_"," ",strtolower("('".implode("','", $biaya)."')"));
            //$param['where'] .= " AND (lower(beban_biaya) in ".str_replace("_"," ",strtolower("('".implode("','",$biaya)."')")).")";
        }

        if (!empty($pos)) {
            $param['where'] .= " AND lower(pos_rekening) IN ".str_replace("_"," ",strtolower("('".implode("','", $pos)."')"));
            //$param['where'] .= " AND (lower(beban_biaya) in ".str_replace("_"," ",strtolower("('".implode("','",$biaya)."')")).")";
        }

        //$param['where'] .= $this->parse('beban_biaya', $biaya);
        //$param['where'] .= $this->parse('pos_rekening', $pos);
        //$param['where'] .= $this->parse('lini', $lini);
        //print_r($param);

        $result = $this->imm->dtquery_me($param)->result();
        $last_query = $this->db->last_query();
        $filter = $this->imm->dtfiltered_me();
        $total	= $this->imm->dtcount_me();
        $output = $this->datatables->output($total, $filter);
       
        foreach($result as $row)
        {
            $rows = array (
                strtoupper($row->lini),
                strtoupper($row->pos_rekening),
                strtoupper($row->beban_biaya),
                //date('M Y', strtotime($row->tahun1.'-'.$row->bulan.'-01')),
                y_num_pad($row->budget),
                y_num_pad($row->expense),
                y_num_pad($row->sisa_expense),
                y_num_pad($row->expense_1),
                number_format($this->persen($row->achieve)*100,2,'.',' '),
                $row->expense_1 > 0 ? round( (($row->expense - $row->expense_1)/$row->expense_1)*100, 2 ) : '0',
            );

            $output['data'][] = $rows;
        }
        $output['query'] = $last_query;
        echo json_encode( $output );
    }

    public function json_me_bb()
    {
        if(!$this->input->is_ajax_request()) return false;

        $start = $this->input->post('startx');
        $end = $this->input->post('end');
        $lini 	 = $this->input->post('lini');
        $biaya = $this->input->post('biaya');
        $pos = $this->input->post('pos');
        // var_dump($lini,$biaya,$pos);
        // die;
        // $lini2	 = $this->input->post('lini2');
        $s = explode('-', $start);
        $e = explode('-', $end);

        $columns = array(     
            array( 'db' => 'beban_biaya', 'dt' => 0 ),            
            array( 'db' => 'budget', 'dt' => 1 ),
            array( 'db' => 'expense', 'dt' => 2 ),
            array( 'db' => 'sisa_expense', 'dt' => 3 ),
            array( 'db' => 'expense_2', 'dt' => 4),
            array( 'db' => 'achieve', 'dt' => 5),
            array( 'db' => 'goodgrowth', 'dt' => 6)
        );

        $this->datatables->set_cols($columns);
        $param	= $this->datatables->query();
        $table  = 'usc_insight_expense_info';

        $w = "tahun1 = '".(!empty($s[1]) ? $s[1] : date('Y'))."' and bulan BETWEEN '".(!empty($s[0]) ? $s[0] : date('m'))."' and '".(!empty($e[0]) ? $e[0] : date('m'))."'";
        // if(empty($param['where'])) {
        //     $param['where'] = 'WHERE '.$w;
        // } else {
        //     $param['where'] .= ' AND '.$w;
        // }

        $param['tahun'] = (!empty($s[1]) ? $s[1] : date('Y'));
        $param['tahun_sebelum'] = (!empty($s[1]) ? $s[1]-1 : date('Y')-1);
        $param['s_bulan'] = (!empty($s[0]) ? $s[0] : date('m'));
        $param['e_bulan'] = (!empty($e[0]) ? $e[0] : date('m'));

        if (empty($param['whereTahun'])){
            $param['whereTahun'] = 'WHERE '.$w;
        }else{
            $param['whereTahun'] = ' AND '.$w;
        }

        if (!empty($lini)){
            $param['where'] .= "AND lower(lini) IN ".str_replace("_"," ",strtolower("('".implode("','", $lini)."')"));
        }

        if (!empty($biaya)) {
            $param['where'] .= " AND lower(beban_biaya) IN ".str_replace("_"," ",strtolower("('".implode("','", $biaya)."')"));
        }

        if (!empty($pos)) {
            $param['where'] .= " AND lower(pos_rekening) IN ".str_replace("_"," ",strtolower("('".implode("','", $pos)."')"));
        }

        //$param['where'] .= $this->parse('beban_biaya', $biaya);
        //$param['where'] .= $this->parse('pos_rekening', $pos);
        //$param['where'] .= $this->parse('lini', $lini);

        $result = $this->imm->dtquery_me_bb($param)->result();
        $last_query = $this->db->last_query();
        $filter = $this->imm->dtfiltered_me_bb();
        $total	= $this->imm->dtcount_me_bb();
        $output = $this->datatables->output($total, $filter);
       
        
        foreach($result as $row)
        {
            $rows = array (
                strtoupper($row->beban_biaya),
                //date('M Y', strtotime($row->tahun1.'-'.$row->bulan.'-01')),
                y_num_pad($row->budget),
                y_num_pad($row->expense),
                y_num_pad($row->sisa_expense),
                y_num_pad($row->expense_1),
                number_format($this->persen($row->achieve)*100,2,'.',' '),
                $row->expense_1 > 0 ? round( (($row->expense-$row->expense_1)/$row->expense_1)*100, 2 ) : '0'
                //$row->goodgrowth,
            );

            $output['data'][] = $rows;
        }

        $output['query'] = $last_query;

        echo json_encode( $output );
    }
}

?>