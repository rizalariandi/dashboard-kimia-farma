<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Adj_balance_sheet extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Adj_balance_sheet/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'adj_balance_sheet')
    {
        
        if ($this->session->userdata('loged_in') !== null) :            					            
            $this->load->model('uraian_model');                              
            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $data['uraian_bs'] = $this->uraian_model->getUraianBS();     
            $this->load->view('templates/header', $data);
            $this->load->view('pages/form/' . $page, $data);
            $this->load->view('templates/footer_main', $data);          
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getAdjBS()
	{		

		$year = $this->input->get_post('year');
		$entitas = $this->input->get_post('entitas');
		$month = $this->input->get_post('month');

		$this->load->model('kf_import_model');
		$data= $this->kf_import_model->getAdjBS($year,$entitas,$month);	
		echo json_encode($data);
    }
    
    public function postAdjBS()
	{
		$this->load->model('kf_import_model');
		$id = $this->input->get_post('id');
		$adjusment = $this->input->get_post('adjusment');
		$entitas = $this->input->get_post('entitas');
		$f01 = $this->input->get_post('f01');
		$month = $this->input->get_post('month');
		$real = $this->input->get_post('real');
		$target = $this->input->get_post('target');
		$uraian = $this->input->get_post('uraian');
		$year = $this->input->get_post('year');
		$delete = $this->input->get_post('delete');
		$data['result'] = $this->kf_import_model->postAdjBS($id, $adjusment, $entitas, $f01, $month, $real, $target, $uraian, $year, $delete, $this->session->userdata('nama'));
		echo json_encode($data);
	}
      
}
