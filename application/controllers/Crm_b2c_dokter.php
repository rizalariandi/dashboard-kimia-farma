<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Crm_b2c_dokter extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

        $this->load->library('datatables');
        $this->load->model('user_model');
        $this->load->model('Crm_b2b_model', 'b2b');
        $this->load->model('Crm_b2c_dokter_model', 'dm');
   
        ini_set('memory_limit',-1);
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	} 

    public function index($start='',$end='',$area='all')
    {
        $var_max_scatter = 1200000;

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";
 
        $data['title']	= 'B2C Dokter Relation';

        
        if(empty($start) and empty($end)) {
            // $start = '1-'.date('Y'); // by btp
            // $end = '12-'.date('Y'); // by btp

            $current_month = date('n');
            $three_monthsago = date('n', strtotime('-2 months'));

            $start = $three_monthsago.'-'.date('Y');
            $end = $current_month.'-'.date('Y');

            if($current_month == '1' || $current_month == '2'){
                $start = '1-' . date('Y');
            }
            // var_dump($start,$end);
            // die();
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

         

        $data['var_start'] = $start;
        $data['var_end'] = $end; 
        $data['start'] = $start;
        $data['end'] = $end;  
        $data['sc_max_y']     = $var_max_scatter; 

        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];
        //---------------------
 

        $data['area']    = $area;
        $area            = str_replace("_"," ",$area); 

        $dts     = $this->dm->getArea2()->result();
        $data['areaOption'] = array();
        foreach($dts as $row){
            $data['areaOption'][str_replace(" ","_",$row->bcd_address)] = strtoupper($row->bcd_address);
        }

        //scatter

        // ============= initial code ===================
        // $where = "where rev > 0 and doctor != '' and 
        //     (month BETWEEN '$s[0]' and '$e[0]') and year = '$s[1]'";
        // ==============================================

        $where = "where (month BETWEEN $s[0] and $e[0]) and year = '$s[1]'";
 
        if($area != "all") $where.=" and lower(bcd_address) = '".strtolower($area)."'";
        
        $dts = $this->dm->getLevelChart2($where)->result();
        // die($this->db->last_query());

        $rule = $this->b2b->get_level_rule('all', 'all', $s[0], $e[0], $s[1], 'b2c_dokter');
        // die($this->db->last_query());

        //$data['max_value'] = $max_value = $level_rule->value*2;
        //$data['max_freq'] = $max_freq = $level_rule->freq*2;

        //$data['median_value'] = $level_rule->value;
        //$data['median_freq'] = $level_rule->freq;

        $type = array();
        $c = array();
        foreach($dts as $r) {
            /*if($r->rev < 0)
                $rev = 0;
            else if($r->rev > $var_max_scatter)
                $rev = $var_max_scatter;
            else*/
                $rev = (int) $r->rev;

                
            /*if($r->rev < 0)
                $rev = 0;
            else if($r->rev > $max_value)
                $rev = $max_value;
            else
                $rev = (int) $r->rev;*/

            /*if($r->freq < 0)
                $freq = 0;
            else if($r->freq > $max_freq)
                $freq = $max_freq;
            else*/
                $freq = (int) $r->freq;

            $level = y_level_rule_range($r->rev, $r->freq, $rule);

            if(empty($c[strtolower($level)]))
                $c[strtolower($level)] = 1;
            else
                $c[strtolower($level)]++;

            $count = $c[strtolower($level)];

            if($count <= 200) {
                $type['all'][strtolower($level)][] = array(
                    'name' => !empty($r->doctor) ? $r->doctor : 'null',
                    'x' => (int) $freq,
                    'y' => (int) $rev,
                    'label' => y_num_pad($r->rev)
                );
            }
        }
        $data['type'] = $type;

        // $pies = $this->dm->getLevelPie($where)->result();
        // $pie = array();
        // foreach($pies as $p)
        //     $pie[strtolower($p->level)] = $p->jml;

        $data['pie'] = array(
            array(
                'name' => 'Gold',
                'color' => y_cl('gold'),
                'y' => !empty($c['gold']) ? (int) $c['gold'] : 0
            ),
            array(
                'name' => 'Bronze',
                'color' => y_cl('bronze'),
                'y' => !empty($c['bronze']) ? (int) $c['bronze'] : 0
            ),
            array(
                'name' => 'Silver',
                'color' => y_cl('silver'),
                'y' => !empty($c['silver']) ? (int) $c['silver'] : 0
            ),
            array(
                'name' => 'Regular',
                'color' => y_cl('regular'),
                'y' => !empty($c['regular']) ? (int) $c['regular'] : 0
            )
        );

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2c_dokter', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json()
	{
		if(!$this->input->is_ajax_request()) return false;

		$columns = array(
			array( 'db' => 'doctor', 'dt' => 0 ),
			array( 'db' => 'bcd_hospital_name', 'dt' => 1 ),
			array( 'db' => 'bcd_spesialisasi', 'dt' => 2 ),
			array( 'db' => 'bcd_address', 'dt' => 3 ),
			array( 'db' => 'month', 'dt' =>4),
            array( 'db' => 'rev', 'dt' =>5 ),
            array( 'db' => 'freq', 'dt' =>6 )
			// array( 'db' => 'revfreq', 'dt' =>7 )
		); 
		
		$this->datatables->set_cols($columns);
        $param	    = $this->datatables->query(); 
         
		// $lini 	    = $this->input->post('lini');  
        $area       = $this->input->post('area');   
        $periode    = $this->input->post('periode');   
        $area       = str_replace("_"," ",$area); 
        
		$start = $this->input->post('startx');
		$end = $this->input->post('end');

        $s = explode('-', $start);
        $e = explode('-', $end);
        
        $p = explode('/',$periode);
         
       
        $month_start = $s[0];
        $month_end = $e[0];
        if ($s[0] < 10){
            $month_start = '0'.$s[0];
        }
        if ($e[0] < 10){
            $month_end = '0'.$e[0];
        }
        // ========== initial code ==============
        // if(empty($param['where'])){
        //     $param['where'] = "WHERE year = '$s[1]' and month BETWEEN '$month_start' and '$month_end'";
        // }else{
		//     $param['where'] .= " AND year = '$s[1]' and month BETWEEN '$month_start' and '$month_end'";
        // }
        // =====================================

        if(empty($param['where'])){
            $param['where'] = "WHERE year = '$s[1]' and month BETWEEN $month_start and $month_end";
        }else{
		    $param['where'] .= " AND year = '$s[1]' and month BETWEEN $month_start and $month_end";
        }

		if ($area != "all")
		    $param['where'] .= " AND (lower(bcd_address)='".strtolower($area)."')";

        $data['monthOption'] = array();
        $data['monthOption'][1] = 'Januari';
        $data['monthOption'][2] = 'Februari';
        $data['monthOption'][3] = 'Maret';
        $data['monthOption'][4] = 'April';
        $data['monthOption'][5] = 'Mei';
        $data['monthOption'][6] = 'Juni';
        $data['monthOption'][7] = 'Juli';
        $data['monthOption'][8] = 'Agustus';
        $data['monthOption'][9] = 'September';
        $data['monthOption'][10] = 'Oktober';
        $data['monthOption'][11] = 'November';
        $data['monthOption'][12] = 'Desember';

        $rule = $this->b2b->get_level_rule('all', 'all', $s[0], $e[0], $s[1], 'b2c_dokter');
        // die($this->db->last_query());
		  
        $result = $this->dm->dtquery($param)->result();
        // $query = $this->db->last_query();
        // die($query);
        
        // $filter = $this->dm->dtfiltered();
        $total	= $this->dm->dtcount($param)->result()[0]->total;
        // die($this->db->last_query());

        $filter = $total;
        $output = $this->datatables->output($total, $filter);
        // die($this->db->last_query());
        
        foreach($result as $row)
		{ 

            $level = y_level_rule_range($row->rev, $row->freq, $rule);
            $specialis = $row->bcd_spesialisasi;
            if (empty($specialis)){
                $specialis = 'undefined';
            }
            $rows = array (
				'<a target="_blank" href="'.base_url().'index.php/crm_b2c_dokter/detail/'.urlencode(base64_encode($row->doctor)).'/'.urlencode(base64_encode($specialis)).'/'.$start.'/'.$end.'/'.$this->input->post('area').'">'.strtoupper($row->doctor).'</a>',
                strtoupper($row->bcd_hospital_name),
                strtoupper($row->bcd_spesialisasi),
				strtoupper($row->bcd_address),
                strtoupper($data['monthOption'][$row->month]).' '.$row->year,
                y_num_pad($row->rev),
                $row->freq,
                strtoupper($level)
			);
			
			$output['data'][] = $rows;
            
		} 
		// $output['query'] = $query;
		echo json_encode( $output );
    }

    public function detail($id='', $spesialisasi='', $start='', $end='', $area='all')
    {
           
        if(empty($id))
            redirect('crm_b2b');

        //---------------------
        if(empty($start) and empty($end)) {
            $start = '1-'.date('Y');
            $end = '12-'.date('Y');
        } else if(empty($start) and !empty($end)) {
            $y = explode('-', $end);
            $start = '1-'.$y[1];
        } else if(!empty($start) and empty($end)) {
            $y = explode('-', $start);
            $end = '12-'.$y[1];
        }

        $data['var_start'] = $start;
        $data['var_end'] = $end;

        $s = explode('-', $start);
        $e = explode('-', $end);

        $blnx['1'] = 'Jan';
        $blnx['2'] = 'Feb';
        $blnx['3'] = 'Mar';
        $blnx['4'] = 'Apr';
        $blnx['5'] = 'May';
        $blnx['6'] = 'Jun';
        $blnx['7'] = 'Jul';
        $blnx['8'] = 'Aug';
        $blnx['9'] = 'Sep';
        $blnx['10'] = 'Okt';
        $blnx['11'] = 'Nov';
        $blnx['12'] = 'Des';

        $data['start'] = $start;
        $data['end'] = $end;
        $data['startx'] = $s[1].$s[0];
        $data['endx'] = $e[1].$e[0];
        $data['start3'] = $blnx[$s[0]].' '.$s[1];
        $data['end3'] = $blnx[$e[0]].' '.$e[1];
        //---------------------

        $id = base64_decode(urldecode($id));
        $spesialisasi = base64_decode(urldecode($spesialisasi));
        $year   = $s[1];
        $month  = $s[0];
        $month_end = $end[0];
        $id = str_replace('_',' ',$id);

        $data['month'] = $month;
        $data['year'] = $year;
        $data['detail'] = $this->dm->getProfile2($id,$spesialisasi)->row();
        // die($this->db->last_query());

        //$level_db = $this->dm->detail_level($id, $year)->result();
        $rule = $this->b2b->get_level_rule('all', 'all', $month, $month_end, $year, 'b2c_dokter');
        // die($this->db->last_query());

        $level_trans = $this->dm->detail_trans($id, $year)->result();
        // die($this->db->last_query());

        $data['level'] = array();
        //$data['level_rule'] = $rule;
        foreach($level_trans as $lt) {
            $data['level'][$lt->month] = y_level_rule_range($lt->rev, $lt->freq, $rule);
        } //print_r($data['level']);

        //================ REC ==========================
        $lini_rekomendasi = $this->db->query("SELECT * FROM crm_manual_input WHERE mi_model_business = 'B2C Dokter Relation' and month(mi_start_period) <= ".$month." and month(mi_end_period) >= ".$month_end." and year(mi_start_period) = ".$year)->result();
        // die($this->db->last_query());

        $data['lini_rec'] = array();
        foreach($lini_rekomendasi as $lr) {
            $data['lini_rec'][strtoupper($lr->mi_level)] = strtoupper($lr->mi_program);
        }
        //===============================================

        //==================== LINI chart (samping detail) ========================================
        $lini_dbs = $this->dm->purchase2($id,$year,$spesialisasi)->result();
        // die($this->db->last_query());
        // var_dump($lini_dbs);
        
        $lini = array();
        
        foreach($lini_dbs as $ld) {
            $lini['all'][$ld->month] = (int) $ld->jml;
        }

        $lini_array = array();
        if(!empty($lini)) {
            foreach($lini as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($lini[$name][$m]))
                        $month[] = (int) $lini[$name][$m];
                    else
                        $month[] = 0;
                }

                $lini_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['graph_lini'] = $lini_array;
     
        //==== LINI REC ========================================================
        $where = "where rev > 0 and doctor = '$id' and 
            (month BETWEEN '$s[0]' and '$e[0]') and year = '$s[1]'";

        //if(!empty($area) and $area!="all") $where.=" and lower(cct_lini)='".strtolower($lini)."'";

        $dts  = $this->dm->getLevelChartLini2($where)->result();
        // die($this->db->last_query());

        $data['lini_level'] = array();
        foreach($dts as $r) {
            $levelx = y_level_rule_range($r->rev, $r->freq, $rule);

            $data['lini_level'] = $levelx;
        }
        //====================================================================

        //==================== Produk ========================================
        /*$prod_dbs = $this->db->query("SELECT year,month, sum(total_transaction) as jml FROM b2c_crm_doctor_trans WHERE doctor = '$id' and year = '$year' GROUP BY year,month")->result();

        $prod = array();
        foreach($prod_dbs as $ld) {
            $prod[$ld->year][$ld->month] = $ld->jml;
        }

        $prod_array = array();
        if(!empty($prod)) {
            foreach($prod as $name => $value) {
                $month = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($prod[$name][$m]))
                        $month[] = (int) $prod[$name][$m];
                    else
                        $month[] = 0;
                }

                $prod_array[] = array('name' => $name, 'data' => $month);
            }
        }
        $data['prod'] = $prod_array;*/
        //==========================================================
        //============== Prediksi Level ================================== 
        $temp  = $this->db->query("SELECT level,CAST(`month` AS int) `month`,prediction_1 from crm_b2c_doc_prediction where doctor='$id' and year='$year' and `month` BETWEEN $s[0] 
        and $e[0] group by level,month,prediction_1")->result();
        // die($this->db->last_query());

        $month = date('n');
        
        $level['regular'] = '1';
        $level['bronze'] = '3';
        $level['silver'] = '5';
        $level['gold'] = '8';

        $templini = array();
        if(!empty($temp)) {
            foreach($temp as $tmp) {
                if($tmp->month <= $month) {
                    $templini[$tmp->month] = strtolower($tmp->level);
                } else {
                    $templini[$tmp->month] = strtolower($tmp->prediction_1);
                }
            }
        }


        $pred_array = array();
        if(!empty($templini)) {
            foreach($templini as $name => $value) {
                $monthx = array();
                for($m=1; $m<=12; $m++) {
                    if(!empty($templini[$m]))
                        $monthx[] = !empty($level[$templini[$m]]) ? (int) $level[$templini[$m]] : (int) 1;
                    else
                        $monthx[] = (int) 1;
                }

                $pred_array[] = array('name' => $year, 'data' => $monthx);
            }
        }
        $data['prediksi'] = $pred_array;
        $data['level_point'] = $month-1;
        //================================================================

        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['view'] 	= 'crm/b2b_detail';
        $data['title']	= 'B2C Dokter Relation';

        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2c_dokter_detail', $data);
        $this->load->view('templates/footer', $data);
    } 
    
    public function json_detail()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'tanggal', 'dt' => 0 ),
            array( 'db' => 'product_name', 'dt' => 1 ),
            array( 'db' => 'rev', 'dt' => 3),
            array( 'db' => 'freq', 'dt' => 4 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
         $spesialisasi     = $this->input->post('spesialisasi');
         $startx     = $this->input->post('startx');  
         $endx     = $this->input->post('endx'); 
         $e = strlen(explode('-', $startx)[0]) < 2 ? '0'.explode('-', $startx)[0]:explode('-', $startx)[0];
         $startx =  explode('-', $startx)[1].$e;
         $e = strlen(explode('-', $endx)[0]) < 2 ? '0'.explode('-', $endx)[0]:explode('-', $endx)[0];
         $endx =  explode('-', $endx)[1].$e;
        //  $e = explode('-', $end);

        if(empty($param['where'])) 	{
         $param['where'] = "WHERE (doctor='".$id."')";
         $param['where'] .= " and (bcd_spesialisasi='".$spesialisasi."')";
         $param['where'] .= " and DATE_FORMAT(tanggal, '%Y%m')  between '".$startx."' and '".$endx."' ";
        
        }
        else {$param['where'] .= "AND (doctor='".$id."')";   
        }
		$result = $this->dm->dtquery_detail($param)->result();
        // die($this->db->last_query());

		$filter = $this->dm->dtfiltered_detail($param);
        // die($filter);
		$total	= $this->dm->dtcount_detail();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{ 
            

			$rows = array (
                $row->tanggal,
                $row->product_name,
				'Rp. '.number_format($row->rev,0,",","."),
                $row->freq,
             
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }
    
    public function json_reward()
	{
		if(!$this->input->is_ajax_request()) return false;
		
		$columns = array(
			array( 'db' => 'created_at', 'dt' => 0 ),
			array( 'db' => 'level', 'dt' => 1 ),
			array( 'db' => 'bcd_address', 'dt' => 2 ),
			array( 'db' => 'reward', 'dt' => 3 )
		);
		
		$this->datatables->set_cols($columns);
        $param	= $this->datatables->query();		
        
        $id     = $this->input->post('id');  
 
        if(empty($param['where'])) 	$param['where'] = "WHERE (doctor='".$id."' )";
        else $param['where'] .= "AND (doctor='".$id."')";  
            
		$result = $this->dm->dtquery_detail($param)->result();
		$filter = $this->dm->dtfiltered_detail();
		$total	= $this->dm->dtcount_detail();
		$output = $this->datatables->output($total, $filter);
		
		foreach($result as $row)
		{  
			$rows = array (
				$row->created_at, 
				$row->level, 
				$row->bcd_address, 
				$row->reward
			);
			
			$output['data'][] = $rows;
		}
		
		echo json_encode( $output );
    }

    public function data()
    {
        // array( 'db' => 'doctor', 'dt' => 0 ),
		// 	array( 'db' => 'bcd_hospital_name', 'dt' => 1 ),
		// 	array( 'db' => 'bcd_spesialisasi', 'dt' => 2 ),
		// 	array( 'db' => 'bcd_address', 'dt' => 3 ),
		// 	array( 'db' => 'month', 'dt' =>4),
        //     array( 'db' => 'rev', 'dt' =>5 ),
        //     array( 'db' => 'freq', 'dt' =>6 )
        $data['menu'] = $this->user_model->getMenu();
        $data['p'] = "";

        $data['title']    = 'B2C Dokter Relation Datatables';
        $this->load->view('templates/yheader', $data);
        $this->load->view('crm/b2c_dokter_dt', $data);
        $this->load->view('templates/footer', $data);
    }

    public function json2()
    {
        // $this->output->enable_profiler();
        // $this->load->library('dss');
        $column_order = array(null, 'user_nama', 'user_email', 'user_alamat'); //field yang ada di table user
        $column_search = array('doctor', 'bcd_hospital_name', 'bcd_spesialisasi', 'bcd_address', 'month','rev','freq','level'); //field yang diizin untuk pencarian 
        // echo json_encode($this->input->post('columns'));
        // $this->dt->select('doctor,bcd_hospital_name,bcd_spesialisasi,bcd_address,month,rev,freq,level')->from('crm_b2c_doct_trans');
        // echo $this->dt->generate();
        $this->load->library('dss', array(
            'table' => 'crm_b2c_doct_trans',
            'primary_key' => 'doctor',
            'columns' => array('doctor', 'bcd_hospital_name', 'bcd_spesialisasi', 'bcd_address', 'month', 'rev', 'freq', 'level'),
            'where' => array()
        ));

        $this->dss->process();
    }
	
	/**		FOR ADDITONAL FUNCTION
			Untuk Menambah function baru silahkan letakkan di bawah ini.
	**/
}

?>