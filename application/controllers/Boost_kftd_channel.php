<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_kftd_channel extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boost_model');
		$this->load->model('Boost_kftd_channel_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}
	
	public function index()
	{
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		$filter = $this->input->post('filter');
		$range = $this->input->post("period", true);

		if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			// $data_range = date("2019-m-d").' / '.date("Y-m-d");
			// $startDate = "2019/07/01";
			// $endDate = date("Y/m/d");
			// $from = $startDate;
			// $to = $endDate;

			// ============ code wrote on 3 October 2021 ============
			$current_month = date('Y-m-d');
            $three_monthsago = date('Y-m-d', strtotime('-3 months'));
            
			$data_range = $three_monthsago.' / '.$current_month;
			$startDate = $three_monthsago;
			$endDate = $current_month;
			$from = $three_monthsago;
			$to = $current_month;
			// ======================================================
			// var_dump($data_range);
			// die();
		}
        		
		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		if(empty($filter)){
			$filter = 'value';
		}

		// $bcg = $this->Boost_kftd_channel_model->get_bcg_channel('usc_bst_channel_mart', $area, $produk);
		$check = $this->Boost_kftd_channel_model->check_data('usc_bst_channel_mart', $area, $produk, $from, $to);
		
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
		}
		$bcg = $this->Boost_kftd_channel_model->get_bcg_matrix_channel('usc_bst_channel_mart', $area, $produk, $filter, $from, $to);
		$export_channel = $this->Boost_kftd_channel_model->get_data_export_channel('usc_bst_channel_mart',$area, $produk, $from, $to);
		// die($export_channel);
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFTD Channel',
			'area' => $area,
			'areas' => $this->Boost_kftd_channel_model->get_area('usc_bst_channel_mart'),
			'produk' => $produk,
			'produks' => $this->Boost_kftd_channel_model->get_produk_channel('usc_bst_channel_mart'),
			'bcg' => $bcg,
			'opsi' => $filter,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'notif' => $notif,
			'export_channel' => $export_channel
		);

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boost_kftd_channel_model->get_bcg_x('usc_bst_channel_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boost_kftd_channel_model->get_bcg_y('usc_bst_channel_quantity', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boost_kftd_channel_model->get_bcg_x('usc_bst_channel_value', $area, $produk);
			// die($this->db->last_query());

			$data['bcg_y'] = $this->Boost_kftd_channel_model->get_bcg_y('usc_bst_channel_value', $area, $produk);
			// die($this->db->last_query());
		}
			
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/produk/matrix_kftd_channel', $data);
        $this->load->view('templates/footer', $data);
	}

}