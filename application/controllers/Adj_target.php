<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Adj_target extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('kf_import_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Adj_target/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'adj_target')
    {

        if ($this->session->userdata('loged_in') !== null) :
            $this->load->model('kf_import_model');
            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter

            $data['branch']= $this->kf_import_model->getBranch();
            $data['lini']= $this->kf_import_model->getLini();

            $this->load->view('templates/header', $data);
            $this->load->view('pages/form/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getAdjBS()
    {

        $year = $this->input->get_post('year');
        $entitas = $this->input->get_post('entitas');
        $month = $this->input->get_post('month');

        $this->load->model('kf_import_model');
        $data= $this->kf_import_model->getAdjBS($year,$entitas,$month);
        echo json_encode($data);
    }

    public function getAdjTarget()
    {

        $year = $this->input->get_post('year');
        $entitas = $this->input->get_post('entitas');
        $month = $this->input->get_post('month');

        $this->load->model('kf_import_model');
        $data= $this->kf_import_model->getAdjTarget($year,$entitas,$month);
        echo json_encode($data);
    }

    public function postAdjBS()
    {
        $this->load->model('kf_import_model');
        $id = $this->input->get_post('id');
        $adjusment = $this->input->get_post('adjusment');
        $entitas = $this->input->get_post('entitas');
        $f01 = $this->input->get_post('f01');
        $month = $this->input->get_post('month');
        $real = $this->input->get_post('real');
        $target = $this->input->get_post('target');
        $uraian = $this->input->get_post('uraian');
        $year = $this->input->get_post('year');
        $delete = $this->input->get_post('delete');
        $data['result'] = $this->kf_import_model->postAdjBS($id, $adjusment, $entitas, $f01, $month, $real, $target, $uraian, $year, $delete, $this->session->userdata('nama'));
        echo json_encode($data);
    }

    public function postAdjTarget()
    {
        $this->load->model('kf_import_model');
        $id = $this->input->get_post('id');
        $branch_code = $this->input->get_post('branch_code');
        $branch_name = $this->input->get_post('branch_name');
        $distributor_code = $this->input->get_post('distributor_code');
        $lini_code = $this->input->get_post('lini_code');
        $lini_name = $this->input->get_post('lini_name');
        $month = $this->input->get_post('month');
        $qty = $this->input->get_post('qty');
        $target_hjd = $this->input->get_post('target_hjd');
        $target_hjp = $this->input->get_post('target_hjp');
        $target_hna = $this->input->get_post('target_hna');
        $target_hna_nett = $this->input->get_post('target_hna_nett');
        $target_hpp = $this->input->get_post('target_hpp');
        $year = $this->input->get_post('year');
        $delete = $this->input->get_post('delete');
        $data['result'] = $this->kf_import_model->postAdjTarget($id, $branch_code, $branch_name, $distributor_code, $lini_code, $lini_name, $month, $qty, $target_hjd, $target_hjp,$target_hna,$target_hna_nett,$target_hpp,$year,$delete, $this->session->userdata('nama'));
        echo json_encode($data);
    }

}
