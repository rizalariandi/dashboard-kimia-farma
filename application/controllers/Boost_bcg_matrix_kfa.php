<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boost_bcg_matrix_kfa extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('user_model');
		$this->load->model('Boost_bcg_matrix_kfa_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
	}

	public function index()
   	{
		// var_dump($this->Boost_bcg_matrix_kfa_model->get_areas('ms_provinsi'));
		// die;
        $range = $this->input->post("period", true);
		$data['data_area'] = $area = $this->input->post('area');
		$data['data_produk'] = $produk = $this->input->post('produk');
		$data['data_kompetitor'] = $kompetitor = $this->input->post('kompetitor');
        $filter = $this->input->post('filter');

        if($range != ''){
			$tanggal = str_replace(" ","",$range);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;

			// var_dump($data_range);
			// die();
		} else {
			// $data_range = date("Y-02-01").' / '.date("Y-03-01");
			// $startDate = date("Y-02-01");
			// $endDate = date("Y-03-01");
			// $from = $startDate;
			// $to = $endDate;

			// ============ code wrote on 3 October 2021 ============
			$current_month = date('Y-m-d');
            $three_monthsago = date('Y-m-d', strtotime('-3 months'));
            
			$data_range = $three_monthsago.' / '.$current_month;
			$startDate = $three_monthsago;
			$endDate = $current_month;
			$from = $three_monthsago;
			$to = $current_month;
			// ======================================================
			// var_dump($data_range);
			// die();
		}

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		// var_dump($produk);
		// die;
		if(empty($kompetitor)){
			$kompetitor = 'IMUNOS';
		}

		if(empty($filter)){
			$filter = 'value';
        }

        $title_chart = '';
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
        }
        
        if (is_array($area)) {
			$data['$data_area'] = $area;
		} else {
			$data['$data_area'] = explode(',', $area);
        }
        
        if (is_array($produk)) {
			$data['data_produk'] = $produk;
		} else {
			$data['data_produk'] = explode(',', $produk);
        }
        
        if (is_array($kompetitor)) {
			$data['data_kompetitor'] = $kompetitor;
		} else {
			$data['data_kompetitor'] = explode(',', $kompetitor);
        }
        
        $check = $this->Boost_bcg_matrix_kfa_model->check_data('usc_bst_product_mart_new', $area, $produk, $from, $to);
	
		if (empty($check)){
			$notif = 'Data tidak tersedia';
		} else {
			$notif = '';
        }
        
		$bcg = $this->Boost_bcg_matrix_kfa_model->get_bcg_matrix_kompetitor('usc_bst_product_mart_new', $area, $produk, $kompetitor, $filter, $from, $to);
		// die($bcg);
		// var_dump($bcg);
		// die;

        $bcg_rekomendasi = $this->Boost_bcg_matrix_kfa_model->get_bcg_rekomendasi_kompetitor('usc_bst_product_mart_new', $area, $produk, $kompetitor, $filter, $from, $to); 
        $bcg_detils = $this->Boost_bcg_matrix_kfa_model->get_bcg_matrix_detils_kompetitor('usc_bst_product_details_mart_new', $area, $produk, $kompetitor, $filter, $from, $to);
        
		// die($bcg_detils);
		
		$bcg_detils_rekomendasi = $this->Boost_bcg_matrix_kfa_model->get_bcg_detils_rekomendasi_kompetitor('usc_bst_product_details_mart_new', $area, $produk, $kompetitor, $filter, $from, $to);
        $sell_out = $this->Boost_bcg_matrix_kfa_model->sell_out_kfa_kompetitor('usc_bst_product_mart_new', $area, $produk,$kompetitor, $filter, $from, $to);

		// die($sell_out);

        $data_raw = $this->Boost_bcg_matrix_kfa_model->data_sell_out('usc_bst_product_mart_new', $area, $produk,$kompetitor, $filter, $from, $to);
		$chart_sell_out = json_decode($sell_out);
		$export_product = $this->Boost_bcg_matrix_kfa_model->get_data_export('usc_bst_product_mart_new',$area, $produk, $kompetitor, $from, $to);
		$export_variant = $this->Boost_bcg_matrix_kfa_model->get_data_export('usc_bst_product_details_mart_new',$area, $produk, $kompetitor, $from, $to);

		
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boost_bcg_matrix_kfa_model->get_areas('ms_provinsi'),
			'produk' => $produk,
			'produks' => array_values(array_filter($this->Boost_bcg_matrix_kfa_model->get_produk('usc_bst_product_mart_new'),function($value) { return !is_null($value) && $value !== ''; } )),
			'kompetitor' => $kompetitor,
			// 'kompetitors' => $this->Boost_bcg_matrix_kfa_model->get_kompetitor('usc_bst_product_mart_new'),
			'bcg' => $bcg,			
			'bcg_rekomendasi' => $bcg_rekomendasi,
			'bcg_detils' => $bcg_detils,
			'bcg_detils_rekomendasi' => $bcg_detils_rekomendasi,
			'cat' => $chart_sell_out->categories,
			'data_sell_out' => $chart_sell_out->data,
            'title_sell_out' => $title_chart,
            'data_raw' => $data_raw,
			'opsi' => $filter,
			'notif' => $notif,
			'startDate' => $startDate,
			'endDate' => $endDate,
			'data_range' => $data_range,
			'export_product' => $export_product,
			'export_variant' => $export_variant,
		);

		if ($filter == 'quantity') {
			$data['bcg_x'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_x('usc_bst_product_quantity', $area, $produk);
			$data['bcg_y'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_y('usc_bst_product_quantity', $area, $produk);
            $data['bcg_detils_x'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
			
			// die($this->db->last_query());

			$data['bcg_detils_y'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
		} else {
			$data['bcg_x'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_x('usc_bst_product_value', $area, $produk);
			// die($this->db->last_query());
			$data['bcg_y'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_y('usc_bst_product_value', $area, $produk);
			// die($this->db->last_query());
			$data['bcg_detils_x'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_x('usc_bst_product_details_value', $area, $produk);
			// die($this->db->last_query());
            $data['bcg_detils_y'] = $this->Boost_bcg_matrix_kfa_model->get_bcg_y('usc_bst_product_details_value', $area, $produk);
			// die($this->db->last_query());
		}

		// var_dump($data['bcg_x']);
		// var_dump($data['bcg_y']);
		// var_dump($data['bcg_detils_x']);
		// var_dump($data['bcg_detils_y']);
		// print_r(array_filter($linksArray, fn($value) => !is_null($value) && $value !== ''));
		// var_dump(array_values(array_filter($data['produks'], function($value) { return !is_null($value) && $value !== ''; })));
		// die;

		// foreach($data['produks'] as $key=>$value) {
		// 	if ($value->produk == NULL) {
		// 		unset($data['produks'][$key]);
		// 	}
		// }
		// foreach($data['kompetitors'] as $key=>$value) {
		// 	if ($value->produk == NULL) {
		// 		unset($data['kompetitors'][$key]);
		// 	}
		// }
		// var_dump($data['produks']);
		// die;
	    
        $this->load->view('templates/header', $data);
        $this->load->view('boosting/produk/bcg_matrix_kfa', $data);
        $this->load->view('templates/footer', $data);
	}

	public function json_get_matrix_kfa()
	{
		header('Content-Type: application/json');
		$period = $this->input->post("period", true);
        $area = $this->input->post("area", true);
        $produk = $this->input->post("produk", true);
        $kompetitor = $this->input->post("brand", true);

        if($period != ''){
			$tanggal = str_replace(" ","",$period);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2020-m-d").' / '.date("2020-m-d");
			$startDate = date("Y-02-01");
			$endDate = date("Y-03-01");	
			$from = $startDate;
			$to = $endDate;
		}

		echo json_encode($this->Boost_bcg_matrix_kfa_model->get_bcg_matrix_kompetitor('usc_bst_product_mart_new', 'Jawa Barat', 'FITUNO', 'IMBOOST', $filter = 'value', $startDate, $endDate), JSON_PRETTY_PRINT);
	}

	public function json_get_bcg_x()
	{
		header('Content-Type: application/json');
		echo json_encode($this->Boost_bcg_matrix_kfa_model->get_bcg_x('usc_bst_product_quantity', 'Jawa Barat', 'FITUNO'), JSON_PRETTY_PRINT);
	}

	public function json_get_bcg_y()
	{
		header('Content-Type: application/json');
		echo json_encode($this->Boost_bcg_matrix_kfa_model->get_bcg_y('usc_bst_product_quantity', 'Jawa Barat', 'FITUNO'), JSON_PRETTY_PRINT);
	}


	public function json_get_data_sellout()
	{
		header('Content-Type: application/json');

		$period = $this->input->post("period", true);
        $area = $this->input->post("area", true);
        $produk = $this->input->post("produk", true);
		$kompetitor = $this->input->post("brand", true);
		$filter = $this->input->post('filter');

		if (empty($area)) {
			$area = 'Jawa Barat';
		}
		
		if(empty($produk)){
			$produk = 'FITUNO';
		}

		if(empty($kompetitor)){
			$kompetitor = 'IMUNOS';
		}

		if(empty($filter)){
			$filter = 'value';
        }
		
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}
		
		if($period != ''){
			$tanggal = str_replace(" ","",$period);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2020-m-d").' / '.date("2020-m-d");
			$startDate = date("Y-02-01");
			$endDate = date("Y-03-01");	
			$from = $startDate;
			$to = $endDate;
		}

		$data_raw = $this->Boost_bcg_matrix_kfa_model->data_sell_out('usc_bst_product_mart_new', $area, $produk, $kompetitor, $filter, $from, $to);
		echo json_encode($data_raw, JSON_PRETTY_PRINT);
	}

	public function json_get_data_matrix_kfa()
	{
		header('Content-Type: application/json');

		$period = $this->input->post("period", true);
        $area = $this->input->post("area", true);
        $produk = $this->input->post("produk", true);
		$kompetitor = $this->input->post("brand", true);
		$filter = $this->input->post('filter');

		
		if ($filter == 'value') {
			$title_chart = 'Total Sales KFA';
		} else {
			$title_chart = 'Total Quantity KFA';
		}
		
		if($period != ''){
			$tanggal = str_replace(" ","",$period);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$startDate = $tgl[0];
			$endDate = $tgl[1];
			$from = $tgl[0];
			$to = $tgl[1];
			$data_range = $from . ' / ' . $to;
		} else {
			$data_range = date("2020-m-d").' / '.date("2020-m-d");
			$startDate = date("Y-02-01");
			$endDate = date("Y-03-01");	
			$from = $startDate;
			$to = $endDate;
		}

		$export_product = $this->Boost_bcg_matrix_kfa_model->get_data_export('usc_bst_product_mart_new',$area, $produk, $kompetitor, $from, $to);

		echo json_encode($export_product, JSON_PRETTY_PRINT);

	}

	
}