<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import_apl extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');
        $this->load->model('Kf_import_apl_model');
    }


    public function index()
    {
        if ($this->session->userdata('loged_in') !== null) {
            redirect(base_url('Import/view'));
        } else {
            $this->load->view('pages/login');
        }
    }

    public function view($page = 'apl_code')
    {

        if ($this->session->userdata('loged_in') !== null) :

            $data = array();
            $data['menu'] = $this->user_model->getMenu();
            $data['p'] = "";
            $data['title'] = ucfirst($page); // Capitalize the first letter
            $this->load->view('templates/header', $data);
            $this->load->view('pages/form/' . $page, $data);
            $this->load->view('templates/footer_main', $data);
        else :
            redirect(base_url('pages/login'));
        endif;
    }

    public function getDataAPL()
    {
        ini_set('memory_limit', '512M');
        $this->load->model('Kf_import_apl_model');
        $data['apl'] = $this->Kf_import_apl_model->getDataAPL();
        $data['kode'] = $this->Kf_import_apl_model->getDataKodeCustomer();
        echo json_encode($data);
    }

    public function getDataKodeCustomer()
    {
        $this->load->model('Kf_import_apl_model');
        $data = $this->Kf_import_apl_model->getDataKodeCustomer();
        echo json_encode($data);
    }

    public function InsertDataAPL()
    {
        $this->load->model('Kf_import_apl_model');
        $datas = $this->input->get_post('data');
        $data = $this->Kf_import_apl_model->InsertDataAPL($datas);
        echo json_encode($data);
    }

    public function InsertDataKodeCustomer()
    {
        $this->load->model('Kf_import_apl_model');        
        $datas = $this->input->get_post('data');
        $data = $this->Kf_import_apl_model->InsertDataKodeCustomer($datas);
        echo json_encode($data);
    }

    public function UpdateDataAPL()
    {
        $this->load->model('Kf_import_apl_model');
        $datas = $this->input->get_post('data');
        $data = $this->Kf_import_apl_model->UpdateDataAPL($datas);
        echo json_encode($data);
    }

    public function UpdateKodeCustomer()
    {
        $this->load->model('Kf_import_apl_model');
        $datas = $this->input->get_post('data');
        $data = $this->Kf_import_apl_model->UpdateKodeCustomer($datas);
        echo json_encode($data);
    }

    public function DeleteAPL()
    {
        $id = $this->input->get_post('id');
        $data = $this->Kf_import_apl_model->DeleteAPL($id);
        echo json_encode($data);
    }

    public function DeleteKodeCustomer()
    {
        $id = $this->input->get_post('id');
        $data = $this->Kf_import_apl_model->DeleteKodeCustomer($id);
        echo json_encode($data);
    }
}
