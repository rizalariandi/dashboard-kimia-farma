<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Report_sales_margin_summary extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model(array("Report_sales_margin_summary_model"));
		set_time_limit(0); 
	}
	public function index()
	{
		//$data['data'] 			= $this->getdatagrid();
		$data['url']						= base_url('index.php/Report_sales_margin_summary');
		$data['mycontroller']		= 'Report_sales_margin_summary';
		
		$this->template->template('Report_sales_margin_summary/home',$data);
	}
	public function getdatabar(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//print_r($filter);die();

		$header =  $header['head_filter'];//die();
		echo json_encode($this->Report_sales_margin_summary_model->getdatabar($filter,$header,$value));

	}
	public function getList(){
		

		$record_total = 0;
		$record_filter_total = 0;
		$filter = array();
		$header = array();
		$order	= $this->input->get('order');
		$ordered= array("column"=> $order[0]['column'], "dir" => $order[0]['dir'] );

		parse_str($this->input->get("f_search"),$filter);
		
		
		parse_str($this->input->get("f_head"),$header);
		//$value = $filter['value'];
		$value = "";
		//print_r($header);die();
		$header =  $header['head_filter'];
		$start = $this->input->get("start") != '' || !empty($this->input->get("start")) ? $this->input->get("start") : 0 ;
    $limit = $this->input->get("length");
		
	//if( strlen($filter['tanggal_faktur_start']) > 0 && strlen($filter['tanggal_faktur_end']) > 0 && strlen($value) > 0  && strlen($header) > 0 ){
		if( strlen($filter['tanggal_faktur_start']) > 0 && strlen($filter['tanggal_faktur_end']) > 0   && strlen($header) > 0 ){
		
			$data = $this->Report_sales_margin_summary_model->ordering($start,$limit,$filter,"data",$ordered,$header,$value);//die();
			// die($this->db->last_query());

            $data_last = $this->Report_sales_margin_summary_model->ordering_last($start,$limit,$filter,"data",$ordered,$header,$value);
			//echo "<pre>";print_r($data);echo "</pre>";die();
            $size_data = sizeof($data['data']);
            $size_last = sizeof($data_last['data']);

			$i = 0;			
			// die(!$size_data);
			if($size_data){
				foreach($data['data'] as $datas){

					$result = array();
//					if($i == 0){
//						$result[] = $header == 'Month' ? ucwords($this->support->bulan($datas['nama']))." ".$datas['tahun'] :  $datas['nama'];
//					}else{
//						$result[] = $datas['nama'];
//					}
                    $result[] = $datas['nama'];
					$result[] = $datas['description'];
					$result[] = $this->rupiahnormal($datas['target']);
					$result[] = $this->rupiahnormal($datas['realisasi_this_year']);
					$achiev = 0;
					if ($datas['target'] > 0){
					    $achiev = $datas['realisasi_this_year'] / $datas['target'];
                    }
                    $ly = 0;
                    if ($size_last > 0){
                        foreach($data_last['data'] as $datas_last){

                            if (strpos($datas['description'],$datas_last['description']) !== false){

                                $ly = $datas_last['realisasi_this_year'];
                                break;
                            }
                        }
                    }
                    $growth = 0;
                    if ($ly > 0){
                        $growth = (($datas['realisasi_this_year'] - $ly)/$ly)*100;
                    }
					$result[] = $this->rupiahnormal($datas['realisasi_last_year']);
					$result[] = $this->rupiah($achiev);
					$result[] = $this->rupiah($growth);
					
					$json_data[] = $result;
					$i++;
				}
			//	echo "<pre>";print_r($json_data);echo "</pre>";die();
				$record_total 				= $size_data;
				$record_filter_total 	= $size_data;
			}else{				
				$result = array();
					$result[] = 'empty';
					$result[] = 'empty';
					$result[] = 'empty';
					$result[] = 'empty';
					$result[] = 'empty';
					$result[] = 'empty';
					$result[] = 'empty';
				
					
					$json_data[] = $result;
			}
		}else{
			$result = array();
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
			$result[] = 'empty';
		
				$json_data[] = $result;
		}

		$output = array(
            "draw" =>$_GET['draw'],
            "recordsTotal" => $record_total,
            "recordsFiltered" => $record_filter_total,
            "data" => $json_data
		);

		// die($this->db->last_query());
		echo json_encode($output);
	}
	public function ordering($order,$header){
	//print_r($header);die();
		$this->Report_sales_margin_summary_model->ordering($order,$header);
	}
// 	public function downloadexcel(){
// 	$objPHPExcel = new PHPExcel();
// 	//$objPHPExcel = new PHPExcel();
// 	$date_create = $this->input->post('filename');
// 	//$filename = $date_create."-".$action['function'].".xlsx";
// 	//$filename			= '../xampp/htdocs/kf'.$date_create.'.xls';	
// 	$filename			= realpath(".base_url('').").'file/'.$date_create.'.xls';	
	
// 	//$filename = $filename;
	
// 	$filter = array();
// 	$header = array();
// 	parse_str($this->input->post("f_search"),$filter);
// 	parse_str($this->input->post("f_head"),$header);
// 	//print_r($filter['value']);die();
	
	
// 	$header =  $header['head_filter'];
	
// 	//print_r($header);die();
// 		$data = $this->Report_sales_margin_summary_model->ordering(0,0,$filter,"excel",$header,$filter['value']);
// 		//$data = $this->Report_sales_margin_summary_model->get_limit_data($start,$limit,$filter,"data",$header);
// 	//echo json_encode($data);
// //	echo "<pre>";
// 	//print_r($data);
// 	//echo "</pre>";die();
// 	  if($data){
// 		header('Content-Type: application/vnd.ms-excel');
// 		header('Content-Disposition: attachment;filename="'.$filename.'"');
// 		header('Cache-Control: max-age=0');
// 		$alphabet = array("A1","B1","C1","D1","E1","F1","G1","H1","I1","J1","K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1","U1","V1","W1","X1","Y1","Z1");
// 		$alphabet = array_map('trim', $alphabet);
// 	    if($objPHPExcel){
// 	    $run  = "0";
// 	    foreach($data[0] as $key => $value){
// 	      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$run], $key);
// 	      $run++;
// 	    }

// 	            foreach($data as $key =>$value){
// 	              $listing[] = $value;
// 	            }
// 	            $rows_numb = 2;
// 	            for($number_each_for_value = 0;$number_each_for_value < $number_array = count($data);$number_each_for_value++){
// 	              $col = 'A';
// 	              foreach ($listing[$number_each_for_value] as $key => $value) {
// 					  if($header['head-filter']=='Month' && $key == 'nama'){
// 						$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
// 					  }else{
// 						$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,$value);
// 						//$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
// 					  }
	                
// 	                $col++;
// 	              }
// 	              $rows_numb++;
// 	            }

// 	            $objPHPExcel->getActiveSheet()->setTitle('Data');
// 	            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// 	          	$objWriter->save($filename);
// 	    }else{
// 	      die("library not found");
// 	    }
//   }
  
// 	}

    public function getfiltervalue_rsm(){
        $id = $this->input->post('id');
        $gpm = json_decode($this->input->post('param'));
        $data_tmp = $this->Report_sales_margin_summary_model->getfiltervalue_rsm($id,$gpm);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_shopper(){
        $id = $this->input->post('id');
        $rsm = json_decode($this->input->post('param'));
        $data_tmp = $this->Report_sales_margin_summary_model->getfiltervalue_shopper($id,$rsm);
        $data = $data_tmp;
        echo json_encode($data);
    }

    public function getfiltervalue_brand(){
        $id = $this->input->post('id');
        $lini = json_decode($this->input->post('param'));
        $data_tmp = $this->Report_sales_margin_summary_model->getfiltervalue_brand($id,$lini);
        $data = $data_tmp;
        echo json_encode($data);
    }

public function downloadexcel(){
	//print_r($this->input->post());die();
	$objPHPExcel = new PHPExcel();
	//$objPHPExcel = new PHPExcel();
	$date_create = $this->input->post('filename');
	//$filename = $date_create."-".$action['function'].".xlsx";
	//$filename			= '../xampp/htdocs/kf'.$date_create.'.xls';	
	$filename			= realpath(".base_url('').").'file/'.$date_create.'.xls';	
	
	//$filename = $filename;
	
	$filter = array();
	$header = array();
	parse_str($this->input->post("f_search"),$filter);
	parse_str($this->input->post("f_head"),$header);
	//print_r($filter['value']);die();
	
	
	$header =  $header['head_filter'];
	
	//print_r($header);die();
		$data = $this->Report_sales_margin_summary_model->ordering(0,0,$filter,"excel",$header,NULL);
		//$data = $this->sales_growth_information_model->get_limit_data($start,$limit,$filter,"data",$header);
	//echo json_encode($data);
	  if($data){
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$alphabet = array("A2","B2","C2","D2","E2","F2","G2","H2","I2","J2","K2","L2","M2","N2","O2","P2","Q2","R2","S2","T2","U2","V2","W2","X2","Y2","Z2");
		$alphabet = array_map('trim', $alphabet);
		if($objPHPExcel){
		$run  = "0";
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Periode : ".$filter['tanggal_faktur_start'].' s/d '.$filter['tanggal_faktur_start'].'');
		foreach($data[0] as $key => $value){
		  $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alphabet[$run], $key);
		  $run++;
		}

				foreach($data as $key =>$value){
				  $listing[] = $value;
				}
				$rows_numb = 3;
				for($number_each_for_value = 0;$number_each_for_value < $number_array = count($data);$number_each_for_value++){
				  $col = 'A';
				  foreach ($listing[$number_each_for_value] as $key => $value) {
					  if($header['head-filter']=='Month' && $key == 'nama'){
						$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
					  }else{
						$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,$value);
						//$objPHPExcel->getActiveSheet()->setCellValueExplicit($col.$rows_numb,ucwords($this->support->bulan($value)));
					  }
					
					$col++;
				  }
				  $rows_numb++;
				}
				$rownumber = $rows_numb + 1;
				$lastrow = 'A'.$rownumber;
				
				$objPHPExcel->getActiveSheet()->setTitle('Data');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				  $objWriter->save($filename);
		}else{
		  die("library not found");
		}
  }
}
	public function getdatagrid(){
		$data = $this->Report_sales_margin_summary_model->getdatagrid("month");
		return $data;
	}
	public function targetqty(){
	
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		//print_r($header);die();
		$value = $filter['value'];
	//	echo $this->Report_sales_margin_summary_model->targetqty($filter,$header,$value);
//echo ;die();
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->targetqty($filter,$header,$value)));
		echo json_encode($this->rupiahnormal(round(round(($this->Report_sales_margin_summary_model->targetqty($filter,$header,$value)) , 2))));
	}
	public function targetvalue(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->targetvalue($filter,$header,$value)));
	}
	public function realisasiqty(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->realisasiqty($filter,$header,$value)));
		//echo json_encode($this->rupiah(round(round(($this->Report_sales_margin_summary_model->targetqty($filter,$header,$value)) , 2))));
		echo json_encode($this->rupiahnormal(round(round(($this->Report_sales_margin_summary_model->realisasiqty($filter,$header,$value)) , 2))));
		
	}
	public function realisasivalue(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		
		echo json_encode("Rp.".$this->formatnominal($this->Report_sales_margin_summary_model->realisasivalue($filter,$header,$value)));
	}
	public function lastyearqty(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		
		
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->lastyearqty($filter,$header,$value)));
		//echo json_encode($this->rupiah(round(($this->Report_sales_margin_summary_model->lastyearqty($filter,$header,$value)) , 2)));
		echo json_encode($this->rupiahnormal(round(round(($this->Report_sales_margin_summary_model->lastyearqty($filter,$header,$value)) , 2))));
	}
	public function lastyearvalue(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//print_r();die();
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->lastyearvalue($filter,$header,$value)));
		echo json_encode("Rp.".$this->formatnominal($this->Report_sales_margin_summary_model->lastyearvalue($filter,$header,$value)));
		
	}
	public function ptd(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->ptd($filter,$header,$value)));
		echo json_encode("Rp.".$this->formatnominal($this->Report_sales_margin_summary_model->ptd($filter,$header,$value)));
	}
	public function achievement(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
	//	echo $this->Report_sales_margin_summary_model->achievement($filter,$header,$value)."</br>";
	//	echo $this->formatnominal($this->Report_sales_margin_summary_model->achievement($filter,$header,$value));
	
		echo json_encode($this->rupiah(round(($this->Report_sales_margin_summary_model->achievement($filter,$header,$value)) , 2)));
	}
	public function growth(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];	
		echo json_encode($this->rupiah(round(($this->Report_sales_margin_summary_model->growth($filter,$header,$value)) , 2)));
	}
	public function noo(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//echo json_encode($this->Report_sales_margin_summary_model->noo($filter,$header,$value));
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->noo($filter,$header,$value)));
		//echo json_encode($this->rupiah(round(($this->Report_sales_margin_summary_model->noo($filter,$header,$value)) , 2)));
		echo json_encode($this->Report_sales_margin_summary_model->noo($filter,$header,$value));
	}
	public function otr(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//echo json_encode($this->Report_sales_margin_summary_model->otr($filter,$header,$value));
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->otr($filter,$header,$value)));
		
		echo json_encode($this->Report_sales_margin_summary_model->otr($filter,$header,$value));
	}
	public function margin(){
		$filter = array();
		$header = array();
		parse_str($this->input->post("f_search"),$filter);
		parse_str($this->input->post("f_head"),$header);
		$value = $filter['value'];
		//echo json_encode($this->formatnominal($this->Report_sales_margin_summary_model->margin($filter,$header,$value)));
		echo json_encode("Rp.".$this->formatnominal($this->Report_sales_margin_summary_model->margin($filter,$header,$value)));
		
	}

		public function getfiltervalue(){
			$id = $this->input->post('id');
			$data_tmp = $this->Report_sales_margin_summary_model->getfiltervalue($id);
			$data = $data_tmp;
			echo json_encode($data);
		}	

	
		function delete_cache(){
			$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
			$this->cache->clean();
			
		}
		function info_cache(){
			$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
			echo "<pre>";
			print_r($this->cache->cache_info());
			echo "</pre>";
		}
	public function rupiah($angka){
	
		$hasil_rupiah = number_format($angka,2,',','.');
		return $hasil_rupiah;
	 
	}
	public function rupiahnormal($angka){
	
		$hasil_rupiah = number_format($angka,2,',','.');
		return $hasil_rupiah;
	 
	}
	public function formatnominal($angka){
		$angka = explode('.',$angka);
		$totalangka = strlen($angka[0]);
		$angka[0];//die();
		switch ($totalangka) {
				case 2:
					return $this->rupiah($angka[0]);
					break;
				case 3:
					return $this->rupiah($angka[0]);
						break;
				case 4:
					return $this->rupiah($angka[0]);
						break;
				case 5:
					return $this->rupiah($angka[0]);
					break;	
				case 6:
					return $this->rupiah($angka[0]);
					break;
				case 7:
					return substr($angka[0], 0,1).",".substr($angka[0], 1,2)." Juta";
					break;
				case 8:
					return substr($angka[0], 0,2).",".substr($angka[0], 2,2)." Juta";
				break;
				case 9:
				return substr($angka[0], 0,3).",".substr($angka[0], 3,2)." Juta";
					break;
				case 10:
					return substr($angka[0], 0,1).",".substr($angka[0], 1,2)." Milyar";
						break;
				case 11:
				return substr($angka[0], 0,2).",".substr($angka[0], 2,2)." Milyar";
					break;		
				case 12:
				return substr($angka[0], 0,3)." Milyar";
					break;
					case 13:
					return substr($angka[0], 0,1)." Triliun";
						break;
				case 14:
				return substr($angka[0], 0,2)." Triliun";
					break;		
				case 15:
				return substr($angka[0], 0,3)." Triliun";
					break;				
																											
				default:
						return $angka[0].",00";
		}
		//return 	$totalangka;
	}
}
//21.813.274
//21.813.274
