<?php
class Marketing extends CI_Controller
{
	var $marketingMenu =  array(
		'sales_information',
		'marketing_expences_information',
		'report_sales_margin_summary',
		'report_sales_summary'
	);

	public function view($page = 'dashboard')
	{
		error_reporting(0);
		if ($this->session->userdata('loged_in') == null) {
			redirect('page/login');
		}
		//echo APPPATH.'views/'.$page.'';die;
		if (!file_exists(APPPATH . 'views/pages/marketing/' . $page . '.php') &&  !file_exists(APPPATH . 'views/marketing/' . $page)) {
			// Whoops, we don't have a page for that!
			show_404();
		}

		$this->load->model('user_model');
		$this->load->model('kf_model');

		$data['menu'] = $this->user_model->getMenu();
		$data['p'] = "";
		$data['title'] = ucfirst($page); // Capitalize the first letter
		if (!empty($_POST)) {
			foreach ($_POST as $k => $v) {
				$_SESSION[$k] = $v;
			}
		}
		$_SESSION['period'] = isset($_SESSION['period']) ? $_SESSION['period'] : 'YearToDate';
		$_SESSION['entitas'] = isset($_SESSION['entitas']) ? $_SESSION['entitas'] : 'KFHO';
		$_SESSION['year'] = isset($_SESSION['year']) ? $_SESSION['year'] : '2018';
		$_SESSION['month'] = isset($_SESSION['month']) ? $_SESSION['month'] : '1';
		$_SESSION['week'] = isset($_SESSION['week']) ? $_SESSION['week'] : '1';
		$_SESSION['day'] = isset($_SESSION['day']) ? $_SESSION['day'] : '1';
		switch ($page) {
			case 'f_income':
				$this->load->model('uraian_model');
				$data['uraian_is'] = $this->uraian_model->getUraianIS();
				break;
			case 'balance_sheet':
				$data['bs'] = $this->kf_model->balance_sheet();
				break;
			case 'kftd_balance_sheet':
				$data['bs'] = $this->kf_model->balance_sheet();
				break;
			case 'income_s':
				$data['max'] = $this->kf_model->max_lastupdate();
				$max_lastupdate = $data['max'][0];
				$data['is'] = $this->kf_model->income_statement($max_lastupdate);
				break;
			case 'kftd_income_s':
				$data['max'] = $this->kf_model->max_lastupdate();
				$max_lastupdate = $data['max'][0];
				$data['is'] = $this->kf_model->income_statement($max_lastupdate);
				break;
			case 'cash_bank':
				$data['profit_center'] = $this->kf_model->get_profit_center();
				$data['cb'] = $this->kf_model->cash_bank();
				break;
			case 'kftd_cash_bank':
				$data['profit_center'] = $this->kf_model->get_profit_center_cb_kftd();
				$data['status'] = $this->kf_model->get_status_kftd();
				$data['cb'] = $this->kf_model->cash_bank();

				break;
			case 'user_management':
				$data['menus'] = $this->user_model->getAllMenu();
				$data['levels'] = $this->user_model->getAllLevel();
				$data['usermenu'] = $this->user_model->getAllUserMenu();
				break;
			case 'ap_ar':
				$data['menus'] = $this->user_model->getAllMenu();
				$data['levels'] = $this->user_model->getAllLevel();
				$data['usermenu'] = $this->user_model->getAllUserMenu();
				$data['ap_data'] = $this->kf_model->ap_data();
				$data['ar_data'] = $this->kf_model->ar_data();
				$data['ar_data_aging'] = $this->kf_model->ar_data_aging();
				break;
			case 'kftd_ar':

				$data['daops'] = $this->kf_model->get_daops();
				$data['plant'] = $this->kf_model->get_plant();
				$data['segment_1'] = $this->kf_model->get_segment1();
				$data['segment_3'] = $this->kf_model->get_segment3();
				break;
			case 'kftd_ar_pem':

				$data['cust_cat'] = $this->kf_model->get_cust_cat();
				$data['plant'] = $this->kf_model->get_plant_pem();
				$data['segment_1'] = $this->kf_model->get_segment1_pem();
				$data['segment_3'] = $this->kf_model->get_segment3_pem();
				break;
			case 'kftd_ap':

				$data['daops'] = $this->kf_model->get_daops_ap();
				$data['plant'] = $this->kf_model->get_plant_ap();
				$data['segment_1'] = $this->kf_model->get_segment1_ap();
				break;
			case 'dtp':
				$data['form'] = (!empty($this->input->post())) ? $this->input->post() : "";

				if (!empty($data['form'])) {
					$this->dtp_model->inputDtp($data['form']);
					$data['p'] = "<div class='alert alert-success alert-dismissable'>
					<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> Berhasil disimpan </div>";
					$msg = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><title>Digital Touchpoint</title></head><body style='margin:0px; background: #f8f8f8; '><div width='100%' style='background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;'><div style='max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px'>
					<table border='0' cellpadding='0' cellspacing='0' style='width: 100%; margin-bottom: 20px'>
					<tbody>
					</tbody>
					</table>
					<div style='padding: 40px; background: #fff;'>
					<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'>
					<tbody>
					<tr>
					<td>Hi, <b>$_SESSION[nama]</b>!
					<p>Request anda tentang <b>$_POST[subject]</b></p>
					<p>detail pertanyaan/permintaan : $_POST[isi]</p>
					<p>Sudah kami terima dan akan segera kami proses.</p>
					<p>Terima Kasih</p>
					<b>- Thanks (Admin team)</b> </td>
					</tr>
					</tbody>
					</table>
					</div><div style='text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px'><p> Digital Touchpoint Data Scientist Group <br></div></div></div></body></html>";
					$this->sendMail($_SESSION['username'], 'Request anda sudah kami terima!', $msg);
				}
				break;
			case 'summary':
				$data['table'] = $this->dtp_model->getData();
				break;
			case 'progress':
				$id = isset($_GET['id']) ? $_GET['id'] : "";
				if (!empty($_POST)) {
					$this->dtp_model->saveProcess($id, $_POST);
					$this->sendingMail($id);
				}
				$data['table'] = $this->dtp_model->getProcess($id);
				$data['form'] = $this->dtp_model->getDataId($id);
				$data['table2'] = $this->dtp_model->getPIC();
				break;
			case 'follow_up':
				$id = isset($_GET['id']) ? $_GET['id'] : "";
				$data['table'] = $this->dtp_model->getProcess($id);
				$data['form'] = $this->dtp_model->getDataId($id);
				break;
		}

		$conditionPage = !in_array($page, $this->marketingMenu) ? true : false;
		if ($conditionPage === true) {
			$data['opration'] = false;
			$this->load->view('templates/header', $data);
			$this->load->view('pages/marketing/' . $page, $data);
			$this->load->view('templates/footer_main', $data);
		} else {
			$data['opration'] = $page == 'marketing_expences_information' ? false : true;
			$data['filter'] = array('page' => $page . '/filter', 'data' => $data);
			$data['javascript'] = array(base_url('assets/js/custom/' . $page . '.js'));
			//print_r($data);die();

			$this->load->view('templates/header_opt', $data);
			$this->load->view($page . '/home', $data);
			$this->load->view('templates/footer_main', $data);
		}
	}

	public function index()
	{
		if ($this->session->userdata('loged_in') !== null) {
			redirect('page/view');
		} else {
			$this->load->view('pages/login');
		}
	}

	public function getdata_marketing_performance()
	{ 
		$value = $this->input->get_post('value');
		$month= $this->input->get_post('month');
		$year = $this->input->get_post('year');
		$top_filter = $this->input->get_post('top_filter');
		$top_struktural = $this->input->get_post('top_struktural');
		$lini_name = $this->input->get_post('lini_name');		
		ini_set('memory_limit', '512M');
        $this->load->model('MarketingPerformance');
        $data = $this->MarketingPerformance->getDataMarketingPerformance($value,$month,$year,$top_filter,$top_struktural,$lini_name);        
        echo json_encode($data);

	}
}
