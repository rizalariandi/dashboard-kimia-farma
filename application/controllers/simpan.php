<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Boosting extends CI_Controller 
{
    function __construct()
	{
		parent::__construct();
	
		$this->load->database();

		$this->load->model('user_model');
		$this->load->model('Boosting_model');
		$this->load->library('Kf');
		$this->load->library('Tamaexcel');
		$this->load->library('WriteCSV');
        if ($this->session->userdata('loged_in') == null) {
            redirect('page/login');
        }
    }
	
	// BOOSTING SALES - AREA CHANNEL
	public function area_channel()
	{
		$data["list_lini"] = $this->Boosting_model->area_channel_get_list_lini();
		$data["list_layanan"] = $this->Boosting_model->area_channel_get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
		}

			if($data["provinsi"] != ""){
				$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
			}
		if($data["data_range"] != ''){

			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		}else{
			$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			$data['startDate'] = "2017/01/01";
			$data['endDate'] = date("Y/m/d");
		}
	
		if($data["areaf"] == "channel-area"){
			$data["kftd"] = $this->Boosting_model->area_channel_get_kftd();
		}else{
			$data["kftd"] = $this->Boosting_model->area_channel_get_kftd_channel();
		}
		$data['mapdata_area_channel'] =$this->populate_provinsi_area_channel($data["data_lini"],$data["data_layanan"]);

		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/area_channel', $data);
		$this->load->view('templates/footer', $data);

	}
	public function populate_provinsi_area_channel($lini,$layanan){
		
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'Aceh', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KI','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		   ); 
		  
		   $array_datas = array();
		   foreach($array_kode_iso as $key=>$val){
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$this->Boosting_model->area_channel_get_revenue(strtoupper($val['name']),$lini,$layanan)));
		   }
		   return json_encode($array_datas);
	}
	public function json_channel_area(){
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_channel_area();
	}
	public function json_area_channel(){
		header('Content-Type: application/json');
		$data = $this->Boosting_model->json_channel_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_area_channel());
		echo str_replace("I.N.H'", "INH", $data);
	}
	// END AREA CHANNEL

	// BOOSTING SALES - BRAND AREA
	public function brand_area()
	{ 
		$data["list_lini"] = $this->Boosting_model->get_list_lini();
		$data["list_layanan"] = $this->Boosting_model->get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_layanan"] = $this->input->post("layanan", true);
		$data["areaf"] = $this->input->post("area", true);
		$data["provinsi"] = $this->input->post("provinsi", true);
		if($data["areaf"] == ""){
			$data["areaf"] = "brand-area";
			
		}

			if($data["provinsi"] != ""){
				$data["kftd_brand_area"] = $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($data['provinsi']))->get('usc_bst_sales_area_brand')->result();
			}
		

		if($data["data_range"] != ''){
			$tanggal = str_replace(" ","",$data['data_range']);
			$tanggal = str_replace("/",",",$tanggal);
			$tgl =  explode(",", $tanggal);
			$data['startDate'] = $tgl[0];
			$data['endDate'] = $tgl[1];
		}else{
			$data["data_range"] = date("2017-m-d").' / '.date("Y-m-d");
			$data['startDate'] = "2017/01/01";
			$data['endDate'] = date("Y/m/d");
		}
		
		if($data["areaf"] == "brand-area"){
			$data["kftd"] = $this->Boosting_model->get_kftd();
		}else{
			$data["kftd"] = $this->Boosting_model->get_kftd_brand();
		}
		$data['mapdata'] = $this->populate_provinsi();
		
		$data['menu'] = $this->user_model->getMenu();
		$this->load->view('templates/header', $data);
		$this->load->view('boosting/brand_area', $data);
		$this->load->view('templates/footer', $data);
	}

	public function brand_area_excel(){
		// $header = $this->db->list_fields('usc_bst_sales_area_brand');
        // $header[0] = "No";
        // $this->tamaexcel->export_data('Brand - Area', $header, 'provinsi', $this->Boosting_model->brand_area_excel());
		$areaf = $this->input->post("area");
		if($areaf == ""){
			$areaf = "brand-area";
		}
		if($areaf == "brand-area"){
			$data = $this->Boosting_model->get_excel_brand_area();
		}else{
			$data = $this->Boosting_model->get_excel_area_brand();
		}
		// var_dump($data[0]);
		$header = array();
		foreach($data[0] as $key => $value){
			$header[] = $key;
		}
		// var_dump($header);
		// echo "=======================";
		
		// var_dump($data);
		// echo "Hello";
		// $i = 0;
		$arrtotal = array();
		foreach($data as $val){
			$total = 0;
			$i = 0;
            foreach($header as $head){
				// echo $val->$head."\n";
				if($i>0){
					$total += $val->$head;
				}
				$i++;
                // $values[] = $this->setpercen($i, $data, $val->$head);
			} 
			$arrtotal[] = $total;
		}
		$this->tamaexcel->exportexcel($header, $data, $arrtotal, $areaf);

	}

	
	public function populate_provinsi(){
		$array_kode_iso = array(
			array('iso'=>'ID-AC','name'=>'N ACEH DARUSSALAM', 'code'=>11),
			array('iso'=>'ID-SU','name'=>'Sumatera Utara', 'code'=>12),
			array('iso'=>'ID-SB','name'=>'Sumatera Barat', 'code'=>13),
			array('iso'=>'ID-RI','name'=>'Riau', 'code'=>14),
			array('iso'=>'ID-JA','name'=>'Jambi', 'code'=>15),
			array('iso'=>'ID-SL','name'=>'Sumatera Selatan', 'code'=>16),
			array('iso'=>'ID-BE','name'=>'Bengkulu', 'code'=>17),
			array('iso'=>'ID-1024','name'=>'Lampung', 'code'=>18),
			array('iso'=>'ID-BB','name'=>'Kepulauan Bangka Belitung', 'code'=>19),
			array('iso'=>'ID-KR','name'=>'Kepulauan Riau', 'code'=>21),
			array('iso'=>'ID-JK','name'=>'DKI Jakarta', 'code'=>31),
			array('iso'=>'ID-JR','name'=>'Jawa Barat', 'code'=>32),
			array('iso'=>'ID-JT','name'=>'Jawa Tengah', 'code'=>33),
			array('iso'=>'ID-YO','name'=>'DI. Yogyakarta', 'code'=>34),
			array('iso'=>'ID-JI','name'=>'Jawa Timur', 'code'=>35),
			array('iso'=>'ID-BT','name'=>'Banten', 'code'=>36),
			array('iso'=>'ID-BA','name'=>'Bali', 'code'=>51),
			array('iso'=>'ID-NB','name'=>'Nusa Tenggara Barat', 'code'=>52),
			array('iso'=>'ID-NT','name'=>'Nusa Tenggara Timur', 'code'=>53),
			array('iso'=>'ID-KB','name'=>'Kalimantan Barat', 'code'=>61),
			array('iso'=>'ID-KT','name'=>'Kalimantan Tengah', 'code'=>62),
			array('iso'=>'ID-KS','name'=>'Kalimantan Selatan', 'code'=>63),
			array('iso'=>'ID-KI','name'=>'Kalimantan Timur', 'code'=>64),
			array('iso'=>'ID-KI','name'=>'Kalimantan Utara', 'code'=>65),
			array('iso'=>'ID-SW','name'=>'Sulawesi Utara', 'code'=>71),
			array('iso'=>'ID-ST','name'=>'Sulawesi Tengah', 'code'=>72),
			array('iso'=>'ID-SE','name'=>'Sulawesi Selatan', 'code'=>73),
			array('iso'=>'ID-SG','name'=>'Sulawesi Tenggara', 'code'=>74),
			array('iso'=>'ID-GO','name'=>'Gorontalo', 'code'=>75),
			array('iso'=>'ID-SR','name'=>'Sulawesi Barat', 'code'=>76),
			array('iso'=>'ID-MA','name'=>'Maluku', 'code'=>81),
			array('iso'=>'ID-LA','name'=>'Maluku Utara', 'code'=>82),
			array('iso'=>'ID-IB','name'=>'Papua Barat', 'code'=>91),
			array('iso'=>'ID-PA','name'=>'Papua', 'code'=>94)
		   ); 
		  
		   $array_datas = array();
		   foreach($array_kode_iso as $key=>$val){
			array_push($array_datas, array('hc-key'=>strtolower($val['iso']), 'name'=>$val['name'], 'value'=>$this->Boosting_model->get_revenue(strtoupper($val['name']))));
		   }
		   return json_encode($array_datas);
	}

	public function json_brand_area(){
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_brand_area();
	}

	public function json_area_brand(){	
		header('Content-Type: application/json');
		$data = $this->Boosting_model->json_brand_area();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_area_brand());
		echo str_replace("I.N.H'", "INH", $data);
	}
	// END BRAND AREA

	
	
	// BOOSTING SALES - BRAND CHANNEL
	function random_color_part() {
		return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}
	
	function random_color() {
		return random_color_part() . random_color_part() . random_color_part();
	}

	public function brand_channel()
	{ 	

		$lini = $this->input->post("lini", true);
		$area = $this->input->post("provinsi", true);
		if($area==""){
			$area="DKI JAKARTA";
		}

		$produk = $this->input->post("produk", true);
		$data['provinsi']=$this->input->post("provinsi", true);
		$data['produk_filter']=$this->input->post("produk", true);
		$data['lini_data']=$this->input->post("lini", true);
		$data['data_range']=$this->input->post("range", true);
		$data['produk']=$this->input->post("produk", true);

		// if($data["produk"] != ""){
		// 	$data["produk_bc"] = $this->db->distinct()->select('nama_brand')->like('provinsi', $data['provinsi'])->get('usc_bst_sales_brand_channel')->result();
		// }
		if($data['data_range'] != ''){
				$tanggal=str_replace(" ", "", $data['data_range']);
				$tanggal=str_replace("/", ",", $tanggal);
				$tgl=explode(",", $tanggal);
				$data['startDate']=$tgl[0];
				$data['endDate']=$tgl[1];
		}else{
			$data["data_range"]=date("2017-m-d").'/'.date("Y-m-d");
			$data['startDate']="2018/01/01";
			$data['endDate']="2019/12/30";
		}
		// if($lini != 0){
		// $data_lini=implode(' ', $lini);	
		if(is_array($lini)){	
			$lini_data=$lini;	
		}else{
			$lini_data=explode(',', $lini);
		}
		if(is_array($produk)){	
			$data_produk=$produk;	
		}else{
			$data_produk=explode(',', $produk);
		}
		// var_dump($lini_data);

		// }
		

		$arrwarna = array();
		$datachannel = $this->db->distinct()->select('channel')->get('usc_bst_sales_brand_channel')->result();
		
		foreach($datachannel as $channel){
			$arrwarna[$channel->channel] = "#".$this->random_color();
		}
		$data['data_channel']=$datachannel;
		$data['arrWarna']=$arrwarna;
		
		$this->db->select("`nama_customer`, `channel`,`nama_kftd`, SUM(`revenue`) AS nowyear, SUM(`revenue_1`) AS lastyear");
		$this->db->group_start();
		if(sizeof($lini_data) != 0){
			// var_dump($lini_data);
			foreach ($lini_data as $lin) {
				$this->db->or_like('lini', $lin);
			}
		}	


		$this->db->group_end();

		$this->db->group_start();
		$this->db->like('provinsi', $area);
		$this->db->group_end();
		$this->db->group_start();
		if(sizeof($data_produk) != 0){
			foreach ($data_produk as $dat) {
				$this->db->or_like('nama_brand', $dat);
			}
		}	
		$this->db->group_end();
		
		$this->db->group_by("nama_customer, channel, nama_kftd");
		// $this->db->order_by("nama_customer asc, channel asc");
		// $this->db->limit(3000);
		$datas = $this->db->get("usc_bst_sales_brand_channel")->result();
		$kftds = $this->db->select("nama_kftd, SUM(revenue) AS total")->group_by("nama_kftd")->get("usc_bst_sales_brand_channel")->result();


		// var_dump($datas);
		$myarr = array();
		$x=0;
		$y=0;
		
		$strseries = "";
		foreach($kftds as $kftd){
		foreach($datas as $datanya){
			if($datanya->nama_kftd == $kftd->nama_kftd){
				if($datanya->lastyear == 0 && $datanya->nowyear > 0){
					$x = 100;
				}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
					$x = -100;
				}else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
					$x = 0;
				}else{
					if($datanya->lastyear>0 && $datanya->nowyear>0){
					$x = round(( $datanya->nowyear - $datanya->lastyear ) / $datanya->lastyear, 3);
					}else{
						$x=0;
					}
				}

				if($datanya->nowyear == 0 && $datanya->lastyear == 0){
					$y = 0;
				}else if($datanya->nowyear == 0 && $datanya->lastyear > 0){
					$y= -100;
				}else if($datanya->lastyear == 0 && $datanya->nowyear>0){
					$y= 100;
				}
				else{
					if($datanya->lastyear>0 && $datanya->nowyear>0){
					$y = round(($datanya->nowyear / $kftd->total) * 100, 3);
					}else if($datanya->nowyear == 0 && $datanya->lastyear == 0){
						$y=0;
					}

				}


					$strseries .= "{\n";
					$strseries .= "name : \"".$datanya->channel." - ".$datanya->nama_customer."\",";
					$strseries .= "showInLegend : false,";
					$strseries .= "marker : {\n";
					$strseries .= "symbol : \"circle\",\n";
					$strseries .= "},";
					// $strseries .= "labelFormat : \"".$datanya->channel."\",";
					$strseries .= "\ndata : [{\n";
					$strseries .= "x : ".$x.",\n";
					$strseries .= "y : ".$y.",\n";
					$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
					$strseries .= "}]";	
					$strseries .= "},";
							
				if(empty($datanya->channel)){
					$strseries .= "{\n";
						$strseries .= "name : \"".$datanya->channel." - ".$datanya->nama_customer."\",";
						$strseries .= "showInLegend : false,";
						$strseries .= "marker : {\n";
						$strseries .= "symbol : \"circle\",\n";
						$strseries .= "},";
						// $strseries .= "labelFormat : \"".$datanya->channel."\",";
						$strseries .= "\ndata : [{\n";
						$strseries .= "x : ".null.",\n";
						$strseries .= "y : ".null.",\n";
						$strseries .= "color : \"".$arrwarna[$datanya->channel]."\"\n";			
						$strseries .= "}]";	
						$strseries .= "},";
				}
			}
			// array_push($series, $temparr);

		}
	}

		$data["scatter_series"] = substr_replace($strseries, "", -1);
		// error_reporting(0);
		// var_dump($strseries);
		
		$data["list_lini"] = $this->Boosting_model->get_lini_list();
		$data["list_area"] = $this->Boosting_model->get_area_list();
		$data["list_produk"] = $this->Boosting_model->get_produk_list();
		// $data["list_layanan"] = $this->Boosting_model->get_list_layanan();
		$data["data_range"] = $this->input->post("range", true);
		$data["data_lini"] = $this->input->post("lini", true);
		$data["data_area"] = $this->input->post("provinsi", true);
		$data["data_produk"] = $this->input->post("produk", true);

		// $data["data_layanan"] = $this->input->post("layanan", true);
		$data["channelf"] = $this->input->post("channel", true);
		$data["cus_data"] = $this->Boosting_model->get_cus_brandc();
		$cus_data= $this->Boosting_model->get_cus_brandc();
		$channel= $this->Boosting_model->get_chan();

		$data["channel"] = $this->input->post("channel", true);
		if($data["channelf"] == ""){
			$data["channelf"] = "brand-channel";
		}
		$tanggal = str_replace(" ","",$data['data_range']);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
		// var_dump($tgl);
		//   die();
		if($data["channelf"] == "brand-channel"){
			$data["chan"] = $this->Boosting_model->get_chan();
		}else{
			$data["chan"] = $this->Boosting_model->get_chan_brand();
		}
		// print_r($data['chan']);

			$data['menu'] = $this->user_model->getMenu();
			$this->load->view('templates/header', $data);
			$this->load->view('boosting/brand_channel', $data);
			$this->load->view('templates/footer', $data);
			
	}

	

	public function json_brand_channel(){
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_brand_channel();
	}

	public function json_channel_brand(){
		header('Content-Type: application/json');
		// $data = $this->Boosting_model->json_brand_channel();
		$data = str_replace("MARCKS'", "MARCKS", $this->Boosting_model->json_channel_brand());
		echo str_replace("I.N.H'", "INH", $data);
	}

	public function json_scatter(){
		header('Content-Type: application/json');
		$data['scatter_data']= $this->Boosting_model->json_brand_channel_scatter();
		// var_dump($data);
	}

	//BOOSTING SALES - OUTLET OTR
	public function outlet_otr()

	{
		$area = $this->input->post('area');
		$channel = $this->input->post('channel');

		$data["data_area"] = $this->input->post("area_table", true);
		$data["data_channel"] = $this->input->post("channel_table", true);

		$data_otr = $this->Boosting_model->outlet_otr($area, $channel);
		$tahun_otr = $this->Boosting_model->get_tahun_otr('usc_bst_sales_transaksi_area');
		$json_data_otr = array();
		
		$nama_brand = $this->Boosting_model->get_nama_brand('usc_bst_sales_transaksi_area');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];

		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Outlet OTR',
			'provinsi' => $this->Boosting_model->get_provinsi('usc_bst_sales_transaksi_area'),
			'area' => $area,
			'areas' => $this->Boosting_model->get_list_kftd('usc_bst_sales_transaksi_area'),
			'channel' => $channel,
			'channels' => $this->Boosting_model->get_channels('usc_bst_sales_transaksi_area'),
			'nama_brand_otr' => $nama_brand,
			'data_otr' => json_encode($json_data_otr, JSON_NUMERIC_CHECK),
			'trend_this_year' => json_encode($this->Boosting_model->trend_outlet($area, $channel, $year = date("Y")),JSON_NUMERIC_CHECK),
			'trend_last_year' => json_encode($this->Boosting_model->trend_outlet($area, $channel, $year - 1), JSON_NUMERIC_CHECK),
			'tahun' => $tahun
		);

		$this->load->view('templates/header', $data);
        $this->load->view('boosting/outlet_otr', $data);
        $this->load->view('templates/footer', $data);
	}

	public function json_outlet_brand()
	{
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_outlet_brand();
	}

	public function json_outlet_area()
	{
		header('Content-Type: application/json');
        echo $this->Boosting_model->json_outlet_area();
	}

	public function json_outlet_otr()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_otr();
	}

	public function json_outlet_trend()
	{
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_trend();
	}

	public function json_outlet_product(){
		header('Content-Type: application/json');
		echo $this->Boosting_model->json_outlet_product();
	}

	public function json_get_product()
	{
		header('Content-Type: application/json');
		$nama_brand = $this->input->post('nama_brand');
		$tahun = [date("Y",strtotime("-1 year")), date("Y")];
		$tahun_otr = $this->Boosting_model->get_tahun_otr('usc_bst_sales_transaksi_area');
		$json_data_otr = array();
		$data_otr= $this->Boosting_model->get_product($nama_brand);

		foreach ($tahun_otr as $key=>$value) {
			$json_data_otr[$key]['name'] = $value->tahun;
			$jml_outlet = array();
			foreach ($data_otr as $otr){
				if ($otr->tahun == $value->tahun){
					 $jml_outlet[] = (int)$otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
			$json_data_otr[$key]['color'] = '#' . random_color();
		}

		$json_data = array(
			'data' => $json_data_otr,
			'categories' => $this->Boosting_model->get_list_product($nama_brand)
		);
		echo json_encode($json_data);
	}

	// END OUTLET OTR

	//BOOSTING PRODUCT
	public function bcg_matrix_channel(){
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		if (empty($area)) {
			$area = 'Nasional';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}
		$bcg = $this->Boosting_model->get_bcg_channel('usc_bst_channel', $area, $produk);
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFTD Channel',
			'area' => $area,
			'areas' => $this->Boosting_model->get_area('usc_bst_channel'),
			'produk' => $produk,
			'produks' => $this->Boosting_model->get_produk('usc_bst_channel'),
			'bcg' => $bcg,
			'bcg_x' => $this->Boosting_model->get_bcg_x('usc_bst_channel', $area, $produk),
			'bcg_y' => $this->Boosting_model->get_bcg_y('usc_bst_channel', $area, $produk),
		);
			
		
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix_channel', $data);
        $this->load->view('templates/footer', $data);
	}

	public function bcg_matrix(){
		$area = $this->input->post('area');
		$produk = $this->input->post('produk');
		if (empty($area)) {
			$area = 'Nasional';
		}
		if(empty($produk)){
			$produk = 'FITUNO';
		}
		// $produk = str_replace(' ', '', $produk);
		$bcg = $this->Boosting_model->get_bcg('usc_bst_product', $area, $produk);
		// var_dump($bcg);
		$data = array(
			'menu' => $this->user_model->getMenu(),
			'title'	=> 'Matrix KFA',
			'area' => $area,
			'areas' => $this->Boosting_model->get_area('usc_bst_product'),
			'produk' => $produk,
			'produks' => $this->Boosting_model->get_produk('usc_bst_product'),
			'bcg' => $bcg,
			'bcg_x' => $this->Boosting_model->get_bcg_x('usc_bst_product', $area, $produk),
			'bcg_y' => $this->Boosting_model->get_bcg_y('usc_bst_product', $area, $produk),
			'bcg_rekomendasi' => $this->Boosting_model->get_bcg_rekomendasi('usc_bst_product', $area, $produk),
			'bcg_detils' => $this->Boosting_model->get_bcg('usc_bst_product_details', $area, $produk),
			'bcg_detils_x' => $this->Boosting_model->get_bcg_x('usc_bst_product_details', $area, $produk),
			'bcg_detils_y' => $this->Boosting_model->get_bcg_y('usc_bst_product_details', $area, $produk),
			'bcg_detils_rekomendasi' => $this->Boosting_model->rekomendasi_bcg_detils('usc_bst_product_details', $area, $produk),
		);
			
		
		$this->load->view('templates/header', $data);
        $this->load->view('boosting/bcg_matrix', $data);
        $this->load->view('templates/footer', $data);

	}
}



