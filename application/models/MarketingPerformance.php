<?php
class MarketingPerformance extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getDataMarketingPerformance($value, $month, $year, $top_filter, $top_struktural, $lini_name)
	{

		// Achievement
		if ($top_filter === '% Achievement') {
			$query = $this->db->query("select REPLACE(A.person_name,"."','".","."''".") as person_name,A.lini_name, 
		ROUND((sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN ". $value." END)/
		sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN target_hna END))*100,2) value_number,
		CONCAT(ROUND((sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN ". $value." END)/
		sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN B.target_hna END))*100,2),'%') value
		from  datamart_kf.exe_db4_all as A
		LEFT JOIN datamart_kf.kf_target_branch as B on A.branch_code = B.branch_code AND A.bulan_tahun = B.bulan_tahun
		WHERE A.ket = ". $this->db->escape($top_struktural)." AND A.lini_name = ". $this->db->escape($lini_name)." GROUP by A.person_name 
		order by Value_number DESC limit 5
		");
		}
		// % Growth
		else {
			$query = $this->db->query("select REPLACE(A.person_name,"."','".","."''".") as person_name,A.lini_name, 
		ROUND(((sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN ". $value." END) - sum(case when A.bulan_tahun = ". $this->db->escape($month.($year-1))." THEN ". $value." END))/
		sum(case when A.bulan_tahun = ". $this->db->escape($month.($year-1))." THEN ". $value." END))*100,2) value_number,
		CONCAT(ROUND(((sum(case when A.bulan_tahun = ". $this->db->escape($month.$year)." THEN ". $value." END) - sum(case when A.bulan_tahun = ". $this->db->escape($month.($year-1))." THEN ". $value." END))/
		sum(case when A.bulan_tahun = ". $this->db->escape($month.($year-1))." THEN A.". $value." END))*100,2),'%') value
		from  datamart_kf.exe_db4_all as A
		LEFT JOIN datamart_kf.kf_target_branch as B on A.branch_code = B.branch_code AND A.bulan_tahun = B.bulan_tahun
		WHERE A.ket = ". $this->db->escape($top_struktural)." AND A.lini_name = ". $this->db->escape($lini_name)." GROUP by A.person_name 
		order by Value_number DESC limit 5");
		}

		return $query->result_array();
	}
}
