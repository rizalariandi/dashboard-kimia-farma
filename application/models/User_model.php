<?php
class User_model extends CI_Model
{

    public function __construct()
    {
        // $this->db->db_debug = FALSE;
        $this->load->database();
    }

    public function login($data, $info)
    {        
        $query = $this->db->query("select nama,username,hp,email,level,unit,entitas from user where active=1 and username=? and password=md5(?);", array($data['user'], $data['pass']));        
        if ($info && count($query->row_array()) > 0) {
            $this->db->query(
                "insert into user_log(name, time_log, username) values(?,now(),?)",
                array($query->row_array()["nama"], $data['user'])             
            );
        }    
        return $query->row_array();
    }
    public function register($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active, $entitas, $username_)
    {
        $cek = $this->checkuser($username);
        if (count($cek) < 1) {
            $query = $this->db->query(
                "insert into user(nama,username, email, password, hp, unit, `level`,jabatan, NIK, active, entitas,last_update,create_by) values(?,?,?,md5(?),?,?,?,?,?,?,?,now(),?)",
                array($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active, implode(",", $entitas), $username_)
            );
            if (!$query) {
                return $this->db->error();
            } else {
                return $query;
            }
        } else {
            return array(
                'success' => false,
                'message' => 'Username have existed'
            );
        }
    }

    public function update_password($password, $new_password, $retype_password, $email)
    {
        $pass = $new_password ? "password=md5(" . $this->db->escape($new_password) . ") " : '';
        $query = $this->db->query("UPDATE user SET " . $pass . " WHERE email='" . $email . "'");
        return $query;
    }

    public function add_level($nama, $id_menu, $username)
    {
        $query = $this->db->query(
            "insert into user_menu(level,id_menu,last_update,create_by) values(?,?,now(),?)",
            array($nama, $id_menu, $username)
        );
        if (!$query) {
            return $this->db->error();
        } else {
            return $query;
        }
    }

    public function updateUser($nama, $username, $email, $password, $hp, $unit, $level, $jabatan, $nik, $active, $entitas, $username_)
    {
        $cek = $this->checkuser($username);
        $cekemail = $this->checkemail($email);
        if (count($cek) < 1) {
            $pass = $password ? "password=md5(" . $this->db->escape($password) . ")," : '';
            $query = $this->db->query("UPDATE user SET nama=" . $this->db->escape($nama) . "," . "username=" . $this->db->escape($username) . "," . $pass . " hp=" . $this->db->escape($hp) . ", unit=" . $this->db->escape($unit) . ", `level`=" . $this->db->escape($level) . ", active=" . $this->db->escape($active) . ",jabatan=" . $this->db->escape($jabatan) . ", NIK=" . $this->db->escape($nik) . ", entitas=" . $this->db->escape(implode(",", $entitas)) . ", last_update= now()" . ", update_by=" . $this->db->escape($username_) . " WHERE email=" . $this->db->escape($email));
            return $query;
        } elseif ($cek[0]['username'] == $cekemail[0]['username']) {
            $pass = $password ? "password=md5(" . $this->db->escape($password) . ")," : '';
            $query = $this->db->query("UPDATE user SET nama=" . $this->db->escape($nama) . "," . "username=" . $this->db->escape($username) . "," . $pass . " hp=" . $this->db->escape($hp) . ", unit=" . $this->db->escape($unit) . ", `level`=" . $this->db->escape($level) . ", active=" . $this->db->escape($active) . ",jabatan=" . $this->db->escape($jabatan) . ", NIK=" . $this->db->escape($nik) . ", entitas=" . $this->db->escape(implode(",", $entitas)) . ", last_update= now()" . ", update_by=" . $this->db->escape($username_) . " WHERE email=" . $this->db->escape($email));
            return $query;
        } else {
            return array(
                'success' => false,
                'message' => 'Username have existed'
            );
        }
    }

    public function checkuser($username)
    {
        $query = $this->db->query("select * from user where username=" .  $this->db->escape($username));
        return $query->result_array();
    }

    public function checkemail($email)
    {
        $query = $this->db->query("select * from user where email=" .  $this->db->escape($email));
        return $query->result_array();
    }

    public function getUser()
    {
        $query = $this->db->query("SELECT nama, email, hp, unit, `level`, active,username,jabatan,NIK,entitas FROM user");
        return $query->result_array();
    }

    public function getMenuLevel()
    {
        $query = $this->db->query("SELECT user_menu.id_menu as id,user_menu.level as level,GROUP_CONCAT(menu.name) as menu
from user_menu left join menu on user_menu.id_menu = menu.id_menu WHERE CHAR_LENGTH(menu.level) = 1 GROUP BY user_menu.level");
        return $query->result_array();
    }

    public function getLevel()
    {
        $query = $this->db->query("select level from user_menu group by level order by level ASC");
        return $query->result_array();
    }

    public function getLevel1()
    {
        $query = $this->db->query("SELECT id_menu,level,name from menu where level NOT LIKE '%.%'");
        return $query->result_array();
    }

    public function getLevel1Full()
    {
        $query = $this->db->query("SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 1");
        return $query->result_array();
    }

    public function getIdMenu($level)
    {

        $query = $this->db->query("select id_menu from menu where level ='" . $level . "'");
        return $query->result_array();
    }

    public function getIdMenuUpdate($level)
    {

        $query = $this->db->query("select id_menu from user_menu where level ='" . $level . "'");
        return $query->result_array();
    }



    public function getLevel2($level)
    {
        $level_code = "";
        $level_parent = "";
        $i = 0;
        if (sizeof(($level)) !== 0) {

            foreach ($level as $key => $value) {

                if (empty($level_code)) {
                    $level_code = " level Like '" . $value . "%'";
                } else {
                    $level_code = $level_code . " OR level Like '" . $value . "%'";
                }

                if ($i === sizeof($level) - 1) {
                    $level_parent .= "'" . $value . "'";
                } else {
                    $level_parent .= "'" . $value . "',";
                }
                $i++;
            }


            $query = $this->db->query('SELECT id_menu,level as lv,name,(select name from menu where level = SUBSTRING(lv,1,1)) as parent_level from menu where CHAR_LENGTH(level) = 3 AND (' . $level_code . ') Order By lv ASC');
        } else {
            $query = $this->db->query('SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 3 AND level is null');
        }


        return $query->result_array();;
    }

    public function getLevel2Full($level)
    {

        $query = $this->db->query('SELECT id_menu,level as lv,name,(select name from menu where level = SUBSTRING(lv,1,1)) as parent_level from menu where CHAR_LENGTH(level) = 3 Order By lv ASC');

        return $query->result_array();
    }

    public function getLevel3Full($level)
    {

        $query = $this->db->query('SELECT id_menu,level as lv,name,(select name from menu where level = SUBSTRING(lv,1,3)) as parent_level from menu where CHAR_LENGTH(level) = 5 Order By lv ASC');

        return $query->result_array();
    }


    public function getUpdateLevel1($id_menu, $nama)
    {
        $level_code = "";
        $i = 0;

        if (sizeof(($id_menu)) !== 0) {
            for ($i = 0; $i < sizeof($id_menu); $i++) {

                if ($i === sizeof($id_menu) - 1) {
                    $level_code .= $id_menu[$i]['id_menu'];
                } else {
                    $level_code .= $id_menu[$i]['id_menu'] . ",";
                }
            }

            $query = $this->db->query('SELECT a.level as nama, b.level as level FROM datamart_kf.user_menu a
left join menu as b on a.id_menu=b.id_menu where a.id_menu in (' . $level_code . ') AND a.level="' . $nama . '" AND CHAR_LENGTH(b.level)=1');
        } else {
            $query = $this->db->query('SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 3 AND level is null');
        }


        return $query->result_array();;
    }

    public function getUpdateLevel2($id_menu, $nama)
    {
        $level_code = "";
        $i = 0;
        if (sizeof(($id_menu)) !== 0) {

            for ($i = 0; $i < sizeof($id_menu); $i++) {

                if ($i === sizeof($id_menu) - 1) {
                    $level_code .= $id_menu[$i]['id_menu'];
                } else {
                    $level_code .= $id_menu[$i]['id_menu'] . ",";
                }
            }
            $query = $this->db->query('SELECT a.level as nama, b.level as level FROM datamart_kf.user_menu a
left join menu as b on a.id_menu=b.id_menu where a.id_menu in (' . $level_code . ') AND a.level="' . $nama . '" AND CHAR_LENGTH(b.level)=3');
        } else {
            $query = $this->db->query('SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 3 AND level is null');
        }


        return $query->result_array();;
    }

    public function getUpdateLevel3($id_menu, $nama)
    {
        $level_code = "";
        $i = 0;
        if (sizeof(($id_menu)) !== 0) {

            for ($i = 0; $i < sizeof($id_menu); $i++) {

                if ($i === sizeof($id_menu) - 1) {
                    $level_code .= $id_menu[$i]['id_menu'];
                } else {
                    $level_code .= $id_menu[$i]['id_menu'] . ",";
                }
            }
            $query = $this->db->query('SELECT a.level as nama, b.level as level FROM datamart_kf.user_menu a
left join menu as b on a.id_menu=b.id_menu where a.id_menu in (' . $level_code . ') AND a.level="' . $nama . '" AND CHAR_LENGTH(b.level)=5');
        } else {
            $query = $this->db->query('SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 3 AND level is null');
        }


        return $query->result_array();;
    }

    public function getLevel3($level)
    {
        $level_code = "";
        $i = 0;
        if (sizeof(($level)) !== 0) {
            for ($i = 0; $i <= sizeof($level); $i++) { }
            foreach ($level as $key => $value) {
                $i++;
                if (empty($level_code)) {
                    $level_code = " level Like '" . $value . "%'";
                } else {
                    $level_code = $level_code . " OR level Like '" . $value . "%'";
                }
            }
            $query = $this->db->query('SELECT id_menu,level as lv,name,(select name from menu where level = SUBSTRING(lv,1,3)) as parent_level from menu where CHAR_LENGTH(level) = 5 AND (' . $level_code . ')');
        } else {
            $query = $this->db->query('SELECT id_menu,level,name from menu where CHAR_LENGTH(level) = 5 AND level is null');
        }


        return $query->result_array();;
    }
    public function DelUser($email)
    {
        $query = $this->db->query("DELETE FROM user WHERE email=" . $this->db->escape($email));
        return $query;
    }

    public function DelLevel($level)
    {
        $query = $this->db->query("DELETE FROM user_menu WHERE level=" . $this->db->escape($level));
        return $query;
    }

    public function getMenu()
    {
        $query = $this->db->query("SELECT t0.* FROM menu t0 LEFT JOIN user_menu t1 USING(id_menu) WHERE t1.level=? order by t0.level", array($_SESSION['level']));
        return $query->result_array();
    }
    public function getAllMenu()
    {
        $query = $this->db->query("select * from menu order by level;");
        return $query->result_array();
    }
    public function getAllLevel()
    {
        $query = $this->db->query("SELECT DISTINCT level FROM user;");
        return $query->result_array();
    }
    public function getAllUserMenu()
    {
        $query = $this->db->query("SELECT level,GROUP_CONCAT(id_menu ORDER BY 1)levels FROM user_menu GROUP BY 1;");
        return $query->result_array();
    }
    public function saveUserMenu($q)
    {
        $this->db->query("truncate user_menu;");
        $this->db->query($q);
    }
}
