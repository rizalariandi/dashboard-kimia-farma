<?php
class Crm_b2c_dokter_model extends CI_Model {
	
	private $table = 'crm_b2c_doct_trans';
	private $table2 = 'crm_b2c_doc_table';
	private $id    = 'customer_name';

	public function __construct()
	{
	    $this->load->database();
	}
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{  
		// ======================== initial query ========================
		// return $this->db->query("select bcd_hospital_name, bcd_spesialisasi, bcd_address, bcd_no_tlpn, trim(doctor) doctor, month, year, sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq
		// FROM ".$this->table2." $param[where] 
		// group by bcd_hospital_name, bcd_spesialisasi, bcd_address, bcd_no_tlpn, trim(doctor), month, year $param[order] $param[limit]");
		// ===============================================================

		return $this->db->query("SELECT /* SQL_NO_CACHE */ bcd_hospital_name, bcd_spesialisasi, bcd_address, bcd_no_tlpn, doctor, month, year, rev, freq, revfreq
		FROM crm_b2b_table_customer_segmentation $param[where] 
		$param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	} 
	
	function dtcount($param)
	{
		return  $this->db->query("select count(*) total from ( select * from ".$this->table2." $param[where] group by bcd_hospital_name, bcd_spesialisasi, bcd_address, bcd_no_tlpn, trim(doctor), month, year) x"); 
		// return $this->db->count_all($this->table2);
	}

    function dtquery_detail($param)
    {
		// ================ initial query ==================
        // return $this->db->query("select SQL_CALC_FOUND_ROWS tanggal,product_name,rev,freq 
        //     from crm_b2c_doct_history 
        //     $param[where]  $param[order] $param[limit]");

		// return $this->db->query("select tanggal,product_name,rev,freq 
        //     from crm_b2c_doct_history 
        //     $param[where] $param[order] $param[limit]");
		// =================================================

		return $this->db->query("select tanggal,product_name,rev,freq 
            from crm_b2c_doct_history_v1 
            $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail($param)
    {
		// ================ initial query ==================
        // $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		// =====================================
		// ========== after revamp =============
		// $result = $this->db->query('
		// select count(*) as jumlah
		// from (
		// 	select tanggal,product_name,rev,freq 
		// 	from crm_b2c_doct_history 
		// 	'.$param["where"].' '.$param["order"].') 
		// a')->row();
		// =================================================

		$result = $this->db->query('
		select count(*) as jumlah
		from (
			select tanggal,product_name,rev,freq 
			from crm_b2c_doct_history_v1 
			'.$param["where"].' '.$param["order"].') 
		a')->row();

        return $result->jumlah;
    }

    function dtcount_detail()
    {
        return $this->db->count_all('crm_b2c_doct_history');
    }
	//---------------------------------------------------------------------------------------------------------------------

    function getLevelChart($where)
    {
				return $this->db->query("SELECT doctor,sum(rev)rev, sum(freq)freq from ".$this->table." $where
				group by doctor");
    }

	function getLevelChart2($where)
    {
		// ======== initial code ==========
		// $new_table = "crm_b2c_doc_chart";

		// return $this->db->query("SELECT trim(doctor) doctor,sum(rev)rev, sum(freq)freq 
		// from ".$new_table." 
		// $where
		// group by trim(doctor)")
		// ================================

		$new_table = "b2c_scatter_plot";
		return $this->db->query("SELECT /* SQL_NO_CACHE */ doctor, sum(rev)rev, sum(freq)freq 
		from ".$new_table." 
		$where
		group by doctor");
    }

    function getLevelPie($where)
    {
        return $this->db->query("SELECT /* SQL_NO_CACHE */ level, count(*) as jml from ".$this->table." $where group by level");
    }

    function getArea()
    {
        return $this->db->query("SELECT bcd_address from ".$this->table." where bcd_address != ''
					group by bcd_address order by bcd_address");
    }

	function getArea2()
    {
		$new_table = 'crm_b2c_doc_bcd_address';
        return $this->db->query("SELECT bcd_address from ".$new_table." where bcd_address is not null");
    }

    function getProfile($id,$spesialisasi)
	{
		$andWhere = " and bcd_spesialisasi = '$spesialisasi'";
		if ($spesialisasi == 'undefined'){
			$andWhere = '';
		}    
	    return $this->db->query("select * from ".$this->table." where doctor = '$id' $andWhere limit 0, 1");
	}

	function getProfile2($id,$spesialisasi)
	{
		$andWhere = " and bcd_spesialisasi = '$spesialisasi'";
		if ($spesialisasi == 'undefined'){
			$andWhere = '';
		}    
	    return $this->db->query("select * from crm_b2c_doc_table where doctor = '$id' $andWhere limit 0, 1");
	}

    public function detail_level($id, $year)
    {
        return $this->db->query("SELECT distinct level, month FROM crm_b2c_doct_lini_level WHERE doctor = '$id' and year = '$year' order by month desc");
    }

    public function detail_trans($id, $year)
    {
        return $this->db->query("SELECT doctor, month, sum(rev) rev, sum(freq) freq
            FROM crm_b2c_doct_trans 
            WHERE doctor = '$id' and year = '$year'
            GROUP BY doctor, month            
            order by month desc");
    }

    public function purchase($id,$year,$spesialisasi)
    {
		$andWhere = " and bcd_spesialisasi = '$spesialisasi'";
		if ($spesialisasi == 'undefined'){
			$andWhere = '';
		}
		return $this->db->query("SELECT month,bcd_spesialisasi, year, sum(rev) as jml FROM ".$this->table." WHERE doctor = '$id' and year = '$year' $andWhere GROUP BY month, year");
	
    }

	public function purchase2($id,$year,$spesialisasi)
    {
		$andWhere = " and bcd_spesialisasi = '$spesialisasi'";
		if ($spesialisasi == 'undefined'){
			$andWhere = '';
		}
		// =============== initial query ===============
		// return $this->db->query("SELECT month,bcd_spesialisasi, year, sum(rev) as jml 
		// FROM crm_b2c_doc_table WHERE doctor = '$id' and year = '$year' $andWhere GROUP BY month, year");
		// =============================================
		return $this->db->query("SELECT month, year, jml 
		FROM b2c_dokter_detail_grafik_purch_hist 
		WHERE doctor = '$id' and year = '$year' $andWhere GROUP BY month, year");
	
    }

	public function b2b_detail($id)
    {
		$query = $this->db->query(" SELECT * FROM b2c_crm_doctor WHERE bcd_doctor_name = '$id'");
		return $query->row();
	}

    public function b2c_level($id, $month, $year)
    {
        return $this->db->query(" SELECT * FROM b2c_cust_lini WHERE ccl_id_customer = '$id' and ccl_month = '$month' and ccl_year = '$year'");
    }

	
	function getYear()
	{
			return $this->db->query("SELECT ccl_year from b2c_cust_lini  
					group by ccl_year order by ccl_year");
	}
	
	function getLini()
	{
			return $this->db->query("SELECT lini from b2c_crm_doctor_trans  
					group by lini order by lini");
	}
	
	function getLayanan()
	{
			return $this->db->query("SELECT bcd_spesialisasi from b2c_crm_doctor where bcd_spesialisasi!='' 
					group by bcd_spesialisasi order by bcd_spesialisasi");
	}
	
	function getChannel()
	{
			return $this->db->query("SELECT ct_channel from b2c_customers  
					group by ct_channel order by ct_channel");
	}

	function getLevels($level,$start,$end,$area)
	{   
		$where = "where created_at BETWEEN '".$start[2]."-".$start[1]."-".$start[0]."' and '".$end[2]."-".$end[1]."-".$end[0]."'";
		if($level!="") $where .=" and lower(level)='$level'"; 
		// if($lini!="0") $where .=" and lini	='$lini'"; 
		if($area!="0") $where .=" and bcd_address	='$area'";     

		// echo "select count(*) total from (SELECT distinct level,doctor from b2c_crm_doctor left join b2c_crm_doctor_trans on doctor=bcd_doctor_name $where limit 0,100)a";
		return $this->db->query("select count(*) total from (SELECT distinct level,doctor from b2c_crm_doctor left join b2c_crm_doctor_trans on doctor=bcd_doctor_name $where limit 0,100)a");
	} 


	//---------------------------------------------------------------------------------------------------------------------

    function getLevelChartLini($where)
    {
        return $this->db->query("SELECT doctor, sum(rev)rev, sum(freq)freq 
            from ".$this->table." 
            $where 
            group by doctor");
    }

	function getLevelChartLini2($where)
    {
        return $this->db->query("SELECT doctor, sum(rev)rev, sum(freq)freq 
            from crm_b2c_doc_table  
            $where 
            group by doctor");
    }

}
