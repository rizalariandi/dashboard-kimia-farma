<?php
class Insightful_model extends CI_Model {
	
	private $table = 'usc_insight_target_realisasi';
    private $table_product = 'usc_insight_target_realisasi_produk';
	private $id    = 'cct_id';
	private $table_mix = 'usc_ins_union';

	public function __construct()
	{
					$this->load->database();
	} 
	
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{
		return $this->db->query("SELECT SQL_CALC_FOUND_ROWS ct_name,ccs_sales,ccs_probability,ccs_step,cct_id FROM ".$this->table."   
								left join crm_cust_stepx on cct_id=ccs_id_trans
								left join crm_customersx on cct_id_customer=ct_id 
								 $param[where] $param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	}
	
	function dtcount()
	{
		return $this->db->count_all($this->table);
	}
	//---------------------------------------------------------------------------------------------------------------------

    function getYear()
    {
        return $this->db->query("SELECT tahun
            from insightful_kftd_produk
            group by tahun order by tahun");
    }
    function getCountDaerah($start,$end,$year,$lini,$layanan,$provinsi,$layanan2,$kftd,$brand,$produk)
     {   
        if($lini != 'ALL' and !empty($lini))
        $lini;
       else
        $lini = '';
        

        if($layanan != 'all' and !empty($layanan))
        $layanan;
       else
        $layanan = '';

        if($provinsi != 'all' and !empty($provinsi))
        $provinsi;
       else
        $provinsi = '';

        if($layanan2 != 'all' and !empty($layanan2))
        $layanan2;
       else
        $layanan2 = '';

        
        if($kftd != 'all' and !empty($kftd))
        $kftd;
       else
        $kftd = '';

        if($brand != 'all' and !empty($brand))
        $brand;
       else
        $brand = '';

        if($produk != 'all' and !empty($produk))
        $produk;
       else
        $produk = '';

        return $this->db->query("SELECT provinsi, sum(target_value) as target, sum(realisasi) as realisasi
            from usc_ins_union where tahun = '$year' and (bulan BETWEEN '".(int) $start."' and '".(int) $end."') $lini $layanan $provinsi $layanan2 $kftd $brand $produk
            group by provinsi");
    }

    function getByAreaGroupMonth($warea,$start,$end,$year,$lini,$layanan,$provinsi,$layanan2,$nama_kftd,$brand,$nama_produk)
    {
        // if($lini != 'all')
        //     $wlini = "AND lini_desc1 = '".strtoupper($lini)."'";
        // else
        //     $wlini = '';

        // if($layanan != 'all')
        //     $wlayanan = "AND layanan = '$layanan'";
        // else
        // $wlayanan = '';

        if (is_array($lini)){
            $wlini = " AND (lower(lini) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        } 
        else
        $wlini = '';

        if (is_array($layanan)){
            $wlayanan = " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
        } 
        else
        $wlayanan = '';

        if (is_array($provinsi)){
            $wprovinsi = " AND (lower(provinsi) in ".str_replace("_"," ",strtolower("('".implode("','",$provinsi)."')")).")";
        } 
        else
        $wprovinsi = '';

        if (is_array($layanan2)){
            $wlayanan2 = " AND (lower(nama_layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan2)."')")).")";
        } 
        else
        $wlayanan2 = '';

        if (is_array($nama_kftd)){
            $nama_kftd = " AND (lower(nama_kftd) in ".str_replace("_"," ",strtolower("('".implode("','",$nama_kftd)."')")).")";
        } 
        else
        $nama_kftd = '';

        if (is_array($brand)){
            $brand = " AND (lower(nama_brand) in ".str_replace("_"," ",strtolower("('".implode("','",$brand)."')")).")";
        } 
        else
        $brand = '';

        if (is_array($nama_produk)){
            $nama_produk = " AND (lower(nama_produk) in ".str_replace("_"," ",strtolower("('".implode("','",$nama_produk)."')")).")";
        } 
        else
        $nama_produk = '';

        return $this->db->query("SELECT bulan, 
                sum(realisasi) as realisasi, sum(realisasi_1) as realisasi_1,
                sum(qty) as qty, sum(qty1) as qty1,
                sum(target_qty) as target_qty, sum(qty_stok) as stock_qty,
                sum(target_value) as target, sum(value_stok) as stock
                from usc_ins_union
                where tahun = '$year' and (cast(bulan AS UNSIGNED INTEGER) BETWEEN '".(int) $start."' and '".(int) $end."') $wlini $wlayanan $warea $wlayanan2 $nama_kftd $brand $nama_produk
                group by bulan
                order by bulan asc");
    }

    function getStockByAreaGroupMonth($warea,$start,$end,$year,$lini,$layanan,$provinsi,$layanan2,$nama_kftd,$brand,$nama_produk)
    {
        if (is_array($lini)){
            $wlini = " AND (lower(lini) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        }
        else
            $wlini = '';

        if (is_array($layanan)){
            $wlayanan = " AND (lower(layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan)."')")).")";
        }
        else
            $wlayanan = '';

        if (is_array($provinsi)){
            $wprovinsi = " AND (lower(provinsi) in ".str_replace("_"," ",strtolower("('".implode("','",$provinsi)."')")).")";
        }
        else
            $wprovinsi = '';

        if (is_array($layanan2)){
            $wlayanan2 = " AND (lower(nama_layanan) in ".str_replace("_"," ",strtolower("('".implode("','",$layanan2)."')")).")";
        }
        else
            $wlayanan2 = '';

        if (is_array($nama_kftd)){
            $nama_kftd = " AND (lower(nama_kftd) in ".str_replace("_"," ",strtolower("('".implode("','",$nama_kftd)."')")).")";
        }
        else
            $nama_kftd = '';

        if (is_array($brand)){
            $brand = " AND (lower(nama_brand) in ".str_replace("_"," ",strtolower("('".implode("','",$brand)."')")).")";
        }
        else
            $brand = '';

        if (is_array($nama_produk)){
            $nama_produk = " AND (lower(nama_produk) in ".str_replace("_"," ",strtolower("('".implode("','",$nama_produk)."')")).")";
        }
        else
            $nama_produk = '';

        return $this->db->query("SELECT bulan, sum(qty_stok) as qty_stok, sum(value_stok) as value_stok
                from usc_ins_union
                where (tahun <= $year and bulan <= ".(int) $end.") $wlini $wlayanan $warea $wlayanan2 $nama_kftd $brand $nama_produk
                group by bulan
                order by bulan asc");
    }

    function getByKftdGroupMonth($area,$month,$year)
    {
        return $this->db->query("SELECT nama_cabang, sum(target_amount) as target, 
                sum(realisasi_total_penjualan) as realisasi, sum(stock) as stock
                from insightful_kftd_produk 
                where tahun = '$year' and bulanx='$month' and provinsi = '$area'
                group by nama_cabang
                order by realisasi desc
                limit 0, 5");
    }

    function getByLini($start,$end,$year,$lini,$layanan,$provinsi)
    {
        if($lini != 'all')
            $wlini = "AND a.lini = '$lini'";
        else
            $wlini = '';

            if($provinsi != 'all')
            $wprovinsi = "AND a.provinsi = '$provinsi'";
        else
            $wprovinsi = '';

        if($layanan != 'all') {
            $wlayanan = "AND a.layanan = '$layanan'";
            $wlayanan_b = "AND b.layanan = '$layanan'";
        } else {
            $wlayanan = $wlayanan_b = '';
        }


        return $this->db->query("SELECT a.lini as lini, provinsi, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.lini_desc1 = a.lini $wlayanan_b) as realisasi_past
                from usc_ins_union a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') $wlini $wlayanan $wprovinsi
                group by a.lini
                order by realisasi desc
                limit 0, 5");
    }

    function getByKftd($area, $start,$end,$year,$lini,$layanan)
    {
        if($lini != 'all') {
            $wlini = "AND a.lini_desc1 = '$lini'";
            $wlini_b = "AND b.lini_desc1 = '$lini'";
        } else {
            $wlini = $wlini_b = '';
        }

        if($layanan != 'all') {
            $wlayanan = "AND a.layanan = '$layanan'";
            $wlayanan_b = "AND b.layanan = '$layanan'";
        } else {
            $wlayanan = $wlayanan_b = '';
        }

        if(!empty($area)) {
            $warea = "AND a.provinsi IN (".implode(',', $area).")";
            $warea_b = "AND b.provinsi IN (".implode(',', $area).")";
        } else {
            $warea = $warea_b = '';
        }

        return $this->db->query("SELECT a.nama_kftd, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_kftd = a.nama_kftd $warea_b $wlini_b $wlayanan_b) as realisasi_past
                from usc_insight_target_realisasi a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') $warea $wlini $wlayanan
                group by a.nama_kftd
                order by realisasi desc
                limit 0, 5");
    }

    function getByLiniGroupBrand($lini,$start,$end,$year)
    {
        /*if($lini != 'all')
            $wlini = "AND kode_lini = '$lini'";
        else
            $wlini = '';

        if($layanan != 'all')
            $wlayanan = "AND kode_lini = '$layanan'";
        else
            $wlayanan = '';*/

        return $this->db->query("SELECT a.nama_brand, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.lini_desc1 = '$lini') as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and lini_desc1 = '$lini'
                group by a.nama_brand
                order by realisasi desc
                limit 0, 5");
    }

    function getByKftdGroupBrand($kftd,$start,$end,$year)
    {
        return $this->db->query("SELECT a.nama_brand, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.nama_kftd = '$kftd') as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and a.nama_kftd = '$kftd'
                group by a.nama_brand
                order by realisasi desc
                limit 0, 5");
    }

    function getByBrandGroupProduct($brand,$start,$end,$year)
    {
        return $this->db->query("SELECT a.nama_produk, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.nama_produk = a.nama_produk) as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and nama_brand = '$brand'
                group by a.nama_produk
                order by realisasi desc
                limit 0, 5");
    }

    function getByBrandGroupProductById($cabang,$brand,$month,$year)
    {  
        return $this->db->query("SELECT sum(realisasi_total_penjualan)realisasi,sum(target_amount)target
                from insightful_kftd_produk 
                where tahun = '$year' and bulanx='$month' and nama_produkx = '$brand' and nama_cabang='$cabang'");
    }

    function getBy_marketing_expenses($cabang,$brand,$month,$year)
    { 
        return $this->db->query("SELECT *
                from insightful_marketing_expenses 
                where tahun = '$year' and bulanx='$month' and nama_produk = '$brand' and nama_cabang = '$cabang' 
                order by nama_produk desc");
    }

    function get_realisasi_expenses($start,$end,$year,$lini)
    {
        if(empty($lini) or $lini == 'all')
        {
            $wlini = '';
        }
        else
        {
            $wlini = "and lower(lini_desc1) IN ".str_replace("_"," ",strtolower("('".implode("','", $lini)."')"));
        }

        return $this->db->query("SELECT bulan, lini_desc1 as lini, sum(expenses) as expenses, sum(realisasi) as realisasi
                from usc_insight_realisasi_expenses 
                where tahun = '$year' and (bulan BETWEEN $start and $end) $wlini 
                group by bulan, lini_desc1
                order by bulan asc, lini_desc1 asc");
    }

    function get_lini_target()
    {
        return $this->db->query("SELECT lini_desc1 as lini
                from usc_insight_target_realisasi 
                group by lini_desc1
                order by lini_desc1 asc");
    }
    function get_layanan_target($layanan)
    { 
        return $this->db->query("SELECT nama_layanan
                from usc_ins_union
                where nama_layanan is not null and nama_layanan!='' $layanan
                group by nama_layanan
                order by nama_layanan asc");
    }

    function getBrand_Lini($lini)
    { 
        return $this->db->query("SELECT nama_brand
                from usc_insight_target_realisasi_produk_layanan_detail
                where nama_brand is not null and nama_brand!='' $lini
                group by nama_brand
                order by nama_brand asc");
    }


    function get_brand_target($kftd)
    { 
        return $this->db->query("SELECT nama_brand
        from usc_insight_target_realisasi_produk
        where nama_brand is not null and nama_kftd!='' $kftd 
        group by nama_brand order by nama_brand");
    }

    
    function get_produk_target()
    { 
        return $this->db->query("SELECT nama_produk
        from usc_ins_union
        where nama_produk is not null and nama_produk!=''
        group by nama_produk order by nama_produk");
    }

    function getProduk($produk)
    { 
        return $this->db->query("SELECT nama_produk
        from usc_ins_union
        where nama_produk is not null and nama_produk!='' $produk 
        group by nama_produk order by nama_produk");
    }


    function getKftd_provinsi($provinsi)
    { 
        return $this->db->query("SELECT nama_kftd
        from usc_insight_target_realisasi
        where  nama_kftd is not null  $provinsi 
        group by nama_kftd order by nama_kftd asc");
    }

    function get_kelompok_layanan_target()
    {
        return $this->db->query("SELECT layanan
                from usc_ins_union
                where layanan is not null and layanan != 'stok'
                group by layanan
                order by layanan asc");
    }

    function get_nama_ktfd()
    {
        return $this->db->query("SELECT nama_kftd
                from usc_insight_target_realisasi
                where nama_kftd is not null
                group by nama_kftd
                order by nama_kftd asc");
    }
    function get_provinsi()
    {
        return $this->db->query("SELECT provinsi
                from usc_insight_target_realisasi
                where provinsi is not null
                group by provinsi
                order by provinsi asc");
    }
    function get_nama_brand()
    {
      
        return $this->db->query("SELECT nama_brand
        from usc_insight_target_realisasi_produk
where nama_brand is not null
group by nama_brand order by nama_brand");
    }




    //============================================
    function dtquery_lini($table_mix, $param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            lini as lini, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            
            FROM $table_mix  
            
			$param[where] group by lini $param[order] $param[limit]");
    }

    function dtquery_lini_stock($param)
    {
        return $this->db->query("SELECT lini, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY lini");
    }

    function dtquery_lini_tahunlalu($table_mix, $param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            lini as lini, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM $table_mix  
            
			$param[where] group by lini $param[limit]");
    }

    function dtfiltered_lini()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_lini()
    {
        return '1000';
    }
    //============================================
    function dtquery_kftd($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_kftd, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi_1)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            
            FROM ".$this->table_mix."   
			
            $param[where] group by nama_kftd $param[order] $param[limit]");
    }

    function dtquery_kftd_stock($param)
    {
        return $this->db->query("SELECT nama_kftd, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY nama_kftd");
    }

    function dtquery_kftd_tahunlalu($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_kftd, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM ".$this->table_mix."  
            
			$param[where] group by nama_kftd $param[limit]");
    }

    function dtfiltered_kftd()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_kftd()
    {
        return '1000';
    }
    //============================================
    function dtquery_brand($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_brand, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi_1)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            FROM ".$this->table_mix."   
			$param[where] group by nama_brand $param[order] $param[limit]");
    }

    function dtquery_brand_stock($param)
    {
        return $this->db->query("SELECT nama_brand, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY nama_brand");
    }

    function dtquery_brand_tahunlalu($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_brand, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM ".$this->table_mix."  
            
			$param[where] group by nama_brand $param[limit]");
    }

    function dtfiltered_brand()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_brand()
    {
        return '1000';
    }
    //============================================
    function dtquery_prod($param)
    {  
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_produk, kode_barang, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi_1)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            
            FROM ".$this->table_mix."   
			$param[where] group by nama_produk, kode_barang $param[order] $param[limit]");
    }

    function dtquery_prod_tahunlalu($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_produk, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM ".$this->table_mix."  
            
			$param[where] group by nama_produk $param[limit]");
    }

    function dtquery_prod_stock($param)
    {
        return $this->db->query("SELECT nama_produk, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY nama_produk");
    }

    function dtfiltered_prod()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_prod()
    {
        return '1000';
    }
    //============================================
    function dtquery_layanan($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_layanan, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi_1)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            
            FROM ".$this->table_mix."   
			
			$param[where] group by nama_layanan $param[order] $param[limit]");
    }

    function dtquery_layanan_stock($param)
    {
        return $this->db->query("SELECT nama_layanan, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY nama_layanan");
    }

    function dtquery_layanan_tahunlalu($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_layanan, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM ".$this->table_mix."  
            
			$param[where] group by nama_layanan $param[limit]");
    }

    function dtfiltered_layanan()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_layanan()
    {
        return '1000';
    }
    //============================================
    function dtquery_layanan_group($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            layanan, sum(target_value) AS target, sum(target_qty) AS target_qty, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok, 
            sum(realisasi) AS realisasi, sum(realisasi_1) AS realisasi_1, 
            sum(qty) AS realisasi_qty, sum(qty1) AS realisasi_qty_1,
            (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi_1)) as growth,            
            (sum(qty)/sum(target_qty)) as achievement_qty, (((sum(qty)-sum(qty1)))/sum(qty)) as growth_qty
            
            FROM ".$this->table_mix."   
			
			$param[where] group by layanan $param[order] $param[limit]");
    }

    function dtquery_layanan_group_stock($param)
    {
        return $this->db->query("SELECT layanan, sum(qty_stok) AS qty_stok, sum(value_stok) AS value_stok
            FROM ".$this->table_mix."  
            $param[where]
			GROUP BY layanan");
    }

    function dtquery_layanan_group_tahunlalu($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            layanan, sum(target_value) AS target, sum(target_qty) AS target_qty, 
            sum(realisasi) AS realisasi, sum(qty) AS realisasi_qty
            
            FROM ".$this->table_mix."  
            
			$param[where] group by layanan $param[limit]");
    }

    function dtfiltered_layanan_group()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_layanan_group()
    {
        return '1000';
    }
}
