<?php
class Boost_brand_area_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_list_lini()
    {
        return $this->db->distinct()->select('lini')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_layanan()
    {
        return $this->db->distinct()->select('layanan')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_brand()
    {
        return $this->db->distinct()->select('nama_brand')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_produk()
    {
        return $this->db->distinct()->select('nama_produk')->get('usc_bst_sales_area_brand')->result();
    }

    function get_kftd()
    {
        $provinsi = $this->input->post("provinsi");

        // ======== initial code ========
        // return $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($provinsi))->get('usc_bst_sales_area_brand')->result();
        // ==============================
        
        // ======== for memsql =========
        return $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($provinsi), 'both', false)->get('usc_bst_sales_area_brand')->result();
        // =============================
    }

    function get_kftd_brand()
    {
        // ====== initial code ======
        // $a = $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS", REPLACE(nama_brand, ".", "")) as nama_brand ')->get('usc_bst_sales_area_brand')->result();
        // ==========================
        $pattern = "'";
        $pattern2 = ".";
        $a = $this->db->distinct()->select(
            '(case
            when locate("'.$pattern.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern.'","_")
            when locate("'.$pattern2.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern2.'","")
            else nama_brand end) as nama_brand')->get('usc_bst_sales_area_brand')->result();
        // $b = $this->db->last_query();
        return $a;
    }

    function get_kftd_brand2(){
        $brand = $this->input->post("brand");
        $produk = $this->input->post("produk");

        if(!is_array($brand)){
            $data_brand = explode(",", $brand);
        }else{
            $data_brand =$brand;
        }

        if(!is_array($produk)){
            $data_produk = explode(",", $produk);
        }else{
            $data_produk = $produk;
        }

        // ======== initial code =========
        // $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS", REPLACE(nama_brand, ".", "")) as nama_brand ');
        // ===============================
        
        $pattern = "'";
        $pattern2 = ".";
        $this->db->distinct()->select(
            '(case
            when locate("'.$pattern.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern.'","_")
            when locate("'.$pattern2.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern2.'","")
            else nama_brand end) as nama_brand'
        );
        $this->db->from('usc_bst_sales_area_brand');

        if($brand != null){
            $this->db->group_start();
            // foreach ($data_brand as $brand) {
            //     $this->db->or_like('nama_brand', $brand);
            // }
            
            $this->db->where_in('nama_brand', $data_brand);
            $this->db->group_end();
        }

        if($produk != null){
            $this->db->group_start();
            // foreach ($data_produk as $produk) {
            //     $this->db->or_like('nama_produk', $produk);
            // }
            
            $this->db->where_in('nama_produk', $data_produk);
            $this->db->group_end();
        }
        return $this->db->get()->result();
    }

    // ===================== added on 26-10-2021 ===================================
    function get_revenue_total(){
        // SELECT sum(revenue) as total FROM datamart_kf.usc_bst_sales_area_brand
		// WHERE `tanggal` >= '2021-07-25' AND `tanggal` <= '2021-10-25' 
		// AND ( `lini` LIKE '%%' ESCAPE '!' ) AND ( `layanan` LIKE '%%' ESCAPE '!' )
        
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);

        // ======== initial code ========
        // if(!is_array($lini)){
        //    $data_lini = explode(",", $lini);
        // }else{
        //     $data_lini = $lini;
        // }

        // if(!is_array($layanan)){
        //     $data_layanan = explode(",", $layanan);
        // }else{
        //     $data_layanan = $layanan;
        // }
        // ==============================
       
        $this->db->select("sum(revenue) as total");
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        
        if(is_array($lini)){
            if(!empty($lini) && $lini[0] != ""){
                // $this->db->group_start();
                // foreach ($data_lini as $lin) {
                //     $this->db->or_like('lini', $lin);
                // }
                
                $this->db->where_in('lini',$lini);
                // $this->db->group_end();
            }
        }else{
            $data_lini = explode(",", $lini);

            if(!empty($data_lini) && $data_lini[0] != ""){
                // $this->db->group_start();
                // foreach ($data_lini as $lin) {
                //     $this->db->or_like('lini', $lin);
                // }
                
                $this->db->where_in('lini',$lini);
                // $this->db->group_end();
            }
        }

        if(is_array($layanan)){
            if(!empty($layanan) && $layanan[0] != ""){
                // $this->db->group_start();
                // foreach ($data_layanan as $lay){
                //     $this->kf->or_like('layanan', $lay);
                // }
                
                $this->db->where_in('layanan',$layanan);
                // $this->db->group_end();
            }
        }else{
            $data_layanan = explode(",", $layanan);
            if(!empty($data_layanan) && $data_layanan[0] != ""){
                // $this->db->group_start();
                // foreach ($data_layanan as $lay){
                //     $this->kf->or_like('layanan', $lay);
                // }
                
                $this->db->where_in('layanan',$data_layanan);
                // $this->db->group_end();
            }
        }
       
        $data = $this->db->get('usc_bst_sales_area_brand');

        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }

    }
    // =========================================================================

    function get_revenue($provinsi){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);

        // if(!is_array($lini)){
        //    $data_lini = explode(",", $lini);
        // }else{
        //     $data_lini = $lini;
        // }
        // if(!is_array($layanan)){
        // $data_layanan = explode(",", $layanan);

        // }else{
        //     $data_layanan = $layanan;
        // }
       
        $this->db->select("provinsi, sum(revenue) as total");
        $this->db->where("provinsi", strtoupper($provinsi));
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        
        if(is_array($lini)){
            if(!empty($lini) && $lini[0] != ""){
                // $this->db->group_start();
                // foreach ($data_lini as $lin) {
                //     $this->db->or_like('lini', $lin);
                // }
                
                $this->db->where_in('lini',$lini);
                // $this->db->group_end();
            }
        }else{
            $data_lini = explode(",", $lini);

            if(!empty($data_lini) && $data_lini[0] != ""){
                // $this->db->group_start();
                // foreach ($data_lini as $lin) {
                //     $this->db->or_like('lini', $lin);
                // }
                
                $this->db->where_in('lini',$lini);
                // $this->db->group_end();
            }
        }

        if(is_array($layanan)){
            if(!empty($layanan) && $layanan[0] != ""){
                // $this->db->group_start();
                // foreach ($data_layanan as $lay){
                //     $this->kf->or_like('layanan', $lay);
                // }
                
                $this->db->where_in('layanan',$layanan);
                // $this->db->group_end();
            }
        }else{
            $data_layanan = explode(",", $layanan);
            if(!empty($data_layanan) && $data_layanan[0] != ""){
                // $this->db->group_start();
                // foreach ($data_layanan as $lay){
                //     $this->kf->or_like('layanan', $lay);
                // }
                
                $this->db->where_in('layanan',$data_layanan);
                // $this->db->group_end();
            }
        }
       
        $this->db->group_by("provinsi");
        $data = $this->db->get('usc_bst_sales_area_brand');

        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }
    }

    function json_brand_area()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $brand = $this->input->post("brand", true);
        $produk = $this->input->post("produk", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $data_brand = explode(",", $brand);
        $data_produk = explode(",", $produk);

        $kftd = $this->get_kftd();
        // die($this->db->last_query());

        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        // ====== initial code =======
        // $this->kf->select('nama_brand');
        // ===========================

        $pattern = "'";
        $pattern2 = ".";
        $this->kf->select('(case
        when locate("'.$pattern.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern.'","_")
        when locate("'.$pattern2.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern2.'","")
        else nama_brand end) as nama_brand');
        $this->kf->from('usc_bst_sales_area_brand');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);

        // $this->kf->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->kf->group_end();
        
        // $this->kf->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->kf->or_like('layanan', $lay);
        // }
        // $this->kf->group_end();

        // $this->kf->group_start();
        // foreach ($data_brand as $brand){
        //     $this->kf->or_like('nama_brand', $brand);
        // }
        // $this->kf->group_end();

        // $this->kf->group_start();
        // foreach ($data_produk as $produk){
        //     $this->kf->or_like('nama_produk', $produk);
        // }
        // $this->kf->group_end();
        
        if(!empty($data_layanan) && $data_layanan[0] != ""){
            $this->kf->where_in('layanan', $data_layanan);
        }
        if(!empty($data_lini) && $data_lini[0] != ""){
            $this->kf->where_in('lini', $data_lini);
        }
        if(!empty($data_brand) && $data_brand[0] != ""){
            $this->kf->where_in('nama_brand', $data_brand);
        }
        if(!empty($data_produk) && $data_produk[0] != ""){
            $this->kf->where_in('nama_produk', $data_produk);
        }
       
        // $this->kf->group_start();
        //     $this->kf->like('provinsi', strtoupper($provinsi));
        // $this->kf->group_end();
        $this->kf->group_by('nama_brand');
        $temp =  $this->kf->generate();
        // die($this->db->last_query());

        $temp2 = json_decode($temp);
        $i = 0;
		foreach($temp2->data as $data){
            $total = array_sum((array)$data);
            foreach ($kftd as $kf){
                if($total != 0){
                    $in = $kf->nama_kftd;
                    $value = $temp2->data[$i]->$in;
                    $rev = $value / $total * 100;
                }else{
                    $rev = 0;
                }
                $revval = number_format((float)$rev, 2, '.', '');
               $temp2->data[$i]->$in = $revval."%";
            }
			$i++;
        }
        return json_encode($temp2);
    }
    

    function json_area_brand()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }

        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $produk = $this->input->post("produk", true);
        $layanan = $this->input->post("layanan", true);

        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        
        // ======== initial code =========
        // if(!is_array($brand)){
        //     $data_brand = explode(",", $brand);
        //     $data_produk = explode(",", $produk);
        // } else {
        //     $data_brand =$brand;
        //     $data_produk = $produk;
        // }
        // ===============================

        if(!is_array($brand)){
            $data_brand = explode(",", $brand);
        } else {
            $data_brand =$brand;
        }

        if(!is_array($produk)){
            $data_produk = explode(",", $produk);
        } else {
            $data_produk = $produk;
        }
        
        if($brand != "" || $produk != ""){
            // $kftd = $this->Boosting_model->get_kftd_brand2();
            $kftd = $this->get_kftd_brand2();
        } else {
            // $kftd = $this->Boosting_model->get_kftd_brand();
            $kftd = $this->get_kftd_brand();
        }

        
        $pattern = "'";
        $pattern2 = ".";
        foreach ($kftd as $kf) {
            // $this->kf->select(
            //     'SUM( IF( nama_brand = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"'
            // );

            $this->kf->select(
                'SUM( IF( 
                    (case
                    when locate("'.$pattern.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern.'","_")
                    when locate("'.$pattern2.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern2.'","")
                    else nama_brand end) = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"'
            );
        }

        $this->kf->select('nama_kftd as area');
        $this->kf->from('usc_bst_sales_area_brand');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);

        // $this->kf->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->kf->group_end();
        
        // $this->kf->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->kf->or_like('layanan', $lay);
        // }

        // $this->kf->group_end();
        
        // if($brand != null){
        //     $this->kf->group_start();
        //     foreach ($data_brand as $brand) {
        //         $this->kf->or_like('nama_brand', $brand);
        //     }
        //     $this->kf->group_end();
        // }

        // if($produk != null){
        //     $this->kf->group_start();
        //     foreach ($data_produk as $produk) {
        //         $this->kf->or_like('nama_produk', $produk);
        //     }
        //     $this->kf->group_end();
        // }

        if(!empty($data_layanan) && $data_layanan[0] != ""){
            $this->kf->where_in('layanan', $data_layanan);
        }
        if(!empty($data_lini) && $data_lini[0] != ""){
            $this->kf->where_in('lini', $data_lini);
        }
        if(!empty($data_brand) && $data_brand[0] != ""){
            $this->kf->where_in('nama_brand', $data_brand);
        }
        if(!empty($data_produk) && $data_produk[0] != ""){
            $this->kf->where_in('nama_produk', $data_produk);
        }
        
        // ======== initial code ========
        // $this->kf->group_start();
        // $this->kf->like('provinsi', strtoupper($provinsi));
        // $this->kf->group_end();
        // ==============================
        if($provinsi != null){
            $this->kf->where('provinsi', strtoupper($provinsi));
        }

        $this->kf->group_by('nama_kftd');
        $temp =  $this->kf->generate();

        // die($this->db->last_query());

        $temp2 = json_decode($temp);
        $i = 0;
		foreach($temp2->data as $data){
            $total = array_sum((array)$data);
            foreach ($kftd as $kf){
                if($total != 0){
                    $in = $kf->nama_brand;
                    if($in != ""){
                        $value = $temp2->data[$i]->$in;
                    }else{
                        $tempval = (array)$temp2->data[$i];
                        $value = $tempval[""];
                    }
                    $rev = $value / $total * 100;
                }else{
                    $rev = 0;
                }
                 $revval = number_format((float)$rev, 2, '.', '');
               $temp2->data[$i]->$in = $revval."%";
            }
			$i++;
        }
        return json_encode($temp2);
    }

    function get_sku($table, $brand) {
        $this->db->distinct()->select('nama_produk');
        if (is_array($brand)) {
            // foreach ($brand as $b) {
            //     $this->db->or_like('nama_brand', $b);
            // }
            $this->db->where_in('nama_brand', $brand);
        } else {
            $this->db->where('nama_brand', $brand);
        }

        return $this->db->order_by('nama_produk','asc')->get($table)->result();
    }

    
    function get_brand($table, $lini) {
        $this->db->distinct()->select('nama_brand');

        if (is_array($lini)) {
            // foreach ($lini as $l) {
            //     $this->db->or_like('lini', $l);
            // }
            if(!empty($lini) && $lini[0] != ""){
                $this->db->where_in('lini', $lini);
            }
        } else {
            $this->db->where('lini', $lini);
        }
        
        return $this->db->order_by('nama_brand','asc')->get($table)->result();
    }


}
