<?php
class Boost_brand_channel_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('Kf');
    }

    function get_lini_list()
    {
        // $area = $this->input->post("provinsi", true);
        // $produk = $this->input->post("produk", true);

        // $data_area = explode(',', $area);
        // $data_produk = explode(',', $produk);

        $this->db->distinct()->select('lini');
        // $this->db->group_start();
        // foreach($data_area as $a) {
        //     $this->db->or_where('nama_kftd', $a);
        // }
        // $this->db->group_end();

        // $this->db->group_start();
        // foreach($data_produk as $p) {
        //     $this->db->like('nama_brand', $p);
        // }
        // $this->db->group_end();
        return $this->db->get('usc_bst_sales_brand_channel')->result();
    }

    function get_area_list()
    {
    //  return $this->db->distinct()->select('provinsi')->get('usc_bst_sales_brand_channel')->result();
        // $produk = $this->input->post("produk", true);

        $this->db->distinct()->select('nama_kftd');
        // $this->db->group_start();
        // $this->db->like('nama_brand', $produk);
        // $this->db->group_end();
        return $this->db->get('usc_bst_sales_brand_channel')->result();
    }

    function get_produk_list()
    {
        // $area = $this->input->post("provinsi", true);
        $this->db->distinct()->select('nama_brand');
        // $this->db->group_start();
        // $this->db->like('provinsi', $area);
        // $this->db->group_end();
        // $this->db->group_start();
        // $this->db->like('nama_brand', $produk);
        // $this->db->group_end();
        return $this->db->get('usc_bst_sales_brand_channel')->result();
    }

    function get_cus_brandc()
    {
        $area = $this->input->post("provinsi", true);
        // if($area == ""){
        //     $area = "DKI JAKARTA";
        // }
        $produk = $this->input->post("produk", true);
        $lini = $this->input->post("lini", true);
        if(is_array($lini)){	
            $data_lini=$lini;	
        } else {
            $data_lini=explode(',', $lini);
        }

        $this->db->distinct()->select('nama_customer');
        $this->db->group_start();
        $this->db->like('nama_kftd', $area);
        $this->db->group_end();
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        $this->db->group_start();
        $this->db->like('nama_brand', $produk);
        $this->db->group_end();
        return $this->db->get('usc_bst_sales_brand_channel')->result();
    }

    function get_chan()
    {
        // $area = $this->input->post("provinsi", true);
        // $lini = $this->input->post("lini", true);
        // $produk = $this->input->post("produk", true);

        // if(is_array($area)) {
        //     $data_area = $area;
        // } else {
        //     $data_area = explode(',', $area);
        // }

        // if(is_array($lini)){	
        //    $data_lini=$lini;	
        // } else {
        //    $data_lini=explode(',', $lini);
        // }

        // if (is_array($produk)) {
        //     $data_produk = $produk;
        // } else {
        //     $data_produk = explode(',', $produk);
        // }
 
        $this->db->distinct()->select("channel, CASE WHEN channel !='Marketing' THEN 0 ELSE 1 END AS ordering");
        
        // $this->db->group_start();
        // foreach($data_area as $ar) {
        //     $this->db->or_where('nama_kftd', $ar);
        // }
        // $this->db->group_end();
        
        // $this->db->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->db->or_like('lini', $lin);
        // }
        // $this->db->group_end();

        // $this->db->group_start();
        // foreach ($data_produk as $prod) {
        //     $this->db->or_like('nama_brand', $prod);
        // }
        // $this->db->group_end();

        // $this->db->group_start();
        // $this->db->like('nama_brand', $produk);
        // $this->db->group_end();
        $this->db->order_by('ordering');
        $this->db->order_by('channel');
        return $this->db->get('usc_bst_sales_brand_channel')->result();
    }

    function get_chan_brand()
    {
        // $area = $this->input->post("provinsi", true);
        // $lini = $this->input->post("lini", true);
        // $produk = $this->input->post("produk", true);

        // if(is_array($area)) {
        //     $data_area = $area;
        // } else {
        //     $data_area = explode(',', $area);
        // }

        // if(is_array($lini)){	
        //     $data_lini=$lini;	
        // } else {
        //     $data_lini=explode(',', $lini);
        // }

        // if (is_array($produk)) {
        //     $data_produk = $produk;
        // } else {
        //     $data_produk = explode(',', $produk);
        // }
        
        // ========== initial code ============
        // $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS_", REPLACE(nama_brand, ".", "")) as nama_brand ');
        // $this->db->distinct()->select('REGEXP_REPLACE(nama_brand, "'. "['.]" .'","") as nama_brand ');
        // ====================================

        // $this->db->distinct()->select('nama_brand');

        // $this->db->group_start();
        // foreach($data_area as $ar) {
        //     $this->db->or_where('nama_kftd', $ar);
        // }
        // $this->db->group_end();
        
        // $this->db->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->db->or_like('lini', $lin);
        // }
        // $this->db->group_end();

        // $this->db->group_start();
        // foreach ($data_produk as $prod) {
        //     $this->db->or_like('nama_brand', $prod);
        // }
        // $this->db->group_end();
        
        // ============ initial code ==============
        // $a = $this->db->get('usc_bst_sales_brand_channel')->result();
        // ========================================

        // die($this->db->last_query());
        // $b = 0;
        
        $pattern = "'";
        $pattern2 = ".";
        $subquery_ = 'LOCATE("'.$pattern.'", nama_brand)>0 then REPLACE(nama_brand, "'.$pattern.'","_")';
        $subquery_2 = 'LOCATE("'.$pattern2.'", nama_brand)>0 then REPLACE(nama_brand, "'.$pattern2.'","_")';
        $a = $this->db->query('
            select distinct a.clean as nama_brand
            from (
                SELECT
                Case
                When '. $subquery_ .'
                when '. $subquery_2 .'
                else nama_brand
                End AS clean 
                FROM usc_bst_sales_brand_channel
            ) a')->result();
        
        // $a = $this->db->get();
        return $a;
    }

    function json_brand_channel()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2019-01-01 / 2019-12-30";
        }
        $area = $this->input->post("area", true);
        $lini = $this->input->post("lini", true);
        $produk = $this->input->post("produk", true);

        $tanggal = str_replace(" ","",$range);
        $tanggal = str_replace("/",",",$tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $data_area = explode(",", $area);
        $data_lini = explode(",", $lini);
        $data_produk = explode(",", $produk);

        $chan = $this->get_chan();
        // die($this->db->last_query());

        foreach ($chan as $kf) {
            $this->kf->select('SUM( IF( channel = "' . $kf->channel . '", revenue, 0) ) AS "' . $kf->channel . '"');
        }
        $this->kf->select('nama_brand');
        $this->kf->from('usc_bst_sales_brand_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        
        if (!empty($area)) {
            $this->kf->group_start();
            foreach($data_area as $ar) {
                $this->kf->or_where('nama_kftd', $ar);
            }
            $this->kf->group_end();
        }

        if(!empty($lini)) {
            $this->kf->group_start();
            foreach ($data_lini as $lin) {
                $this->kf->or_like('lini', $lin);
            }
            $this->kf->group_end();
        }
        
        if (!empty($produk)) {
            $this->kf->group_start();
            foreach($data_produk as $prod) {
                $this->kf->or_like('nama_brand', $prod);
            }
            $this->kf->group_end();    
        }

        $this->kf->group_by('nama_brand');
        $temp=$this->kf->generate();
        // die($this->db->last_query());
        // var_dump($temp);
        // die();

        $temps = json_decode($temp);
        $i = 0;

        foreach($temps->data as $data){
            $total = abs(array_sum((array)$data));
            foreach ($chan as $kf){
                $in = $kf->channel;
                $value = $temps->data[$i]->$in;
                if($total == 0){
                    $rev=0;
                }else{
                $rev = $value/$total*100;
                }
                $revvl = round($rev, 3);
                $temps->data[$i]->$in = $revvl."%";
            }
            $i++;
        }
        return json_encode($temps);
    }

    function json_channel_brand()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2019-01-01 / 2019-12-30";
        }
        $area = $this->input->post("area", true);
        $lini = $this->input->post("lini", true);
        $produk = $this->input->post("produk", true);

        $tanggal = str_replace(" ","",$range);
        $tanggal = str_replace("/",",",$tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $data_area = explode(",", $area);
        $data_lini = explode(",", $lini);
        $data_produk = explode(",", $produk);

        $chan = $this->get_chan_brand();
        // die($this->db->last_query());
        // var_dump($chan);
        // die();

        $pattern = "'";
        $pattern2 = ".";
        foreach ($chan as $kf) {

            // ============= initial code ==============
            // $this->kf->select('SUM( IF( nama_brand = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
            // =========================================

            // $this->kf->select('SUM( IF( if(locate("'.$pattern.'",nama_brand) > 0, REPLACE(nama_brand, "'.$pattern.'","_"), nama_brand) = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
            // $regex_ = 'REGEXP_REPLACE(nama_brand, "'. "'" .'","_")';
            // $this->kf->select("SUM(IF(". $regex_ ." = '" . $kf->nama_brand . "', revenue, 0) ) AS '" . $kf->nama_brand . "' ");

            $this->kf->select('
                SUM( IF( 
                    (case
                    when locate("'.$pattern.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern.'","_")
                    when locate("'.$pattern2.'",nama_brand) > 0 then REPLACE(nama_brand, "'.$pattern2.'"," ")
                    else nama_brand end) = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
        }
        $this->kf->select('channel');
        // $this->kf->select('(select sum(`revenue`) group by `channel`) as total_rev');
        $this->kf->from('usc_bst_sales_brand_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        
        if (!empty($area)) {
            $this->kf->group_start();
            foreach($data_area as $ar) {
                $this->kf->or_where('nama_kftd', $ar);
            }
            $this->kf->group_end();
        }

        if(!empty($lini)) {
            $this->kf->group_start();
            foreach ($data_lini as $lin) {
                $this->kf->or_like('lini', $lin);
            }
            $this->kf->group_end();
        }
        
        if (!empty($produk)) {
            $this->kf->group_start();
            foreach($data_produk as $prod) {
                $this->kf->or_like('nama_brand', $prod);
            }
            $this->kf->group_end();    
        }

        $this->kf->group_by('channel');
        // $this->db->order_by('channel');
        $temp=$this->kf->generate();
        // die($this->db->last_query());

        $temp2 = json_decode($temp);
        $i = 0;

        // var_dump($temp);
        // die();

        foreach($temp2->data as $data){
            $total = abs(array_sum((array)$data));
            foreach ($chan as $kf){
                $in = $kf->nama_brand;
                // $in = preg_replace("/'/i","_",$kf->nama_brand);
                $value = $temp2->data[$i]->$in;
                if($total == 0){
                    $rev=0;
                }else{
                    $rev = $value/$total*100;
                }
                $revvl = round($rev, 3);
                $temp2->data[$i]->$in = $revvl."%";
            }
            $i++;
        }

        return json_encode($temp2);
    }

    function get_brand($table, $lini) {
        $this->db->distinct()->select('nama_brand');
        if (is_array($lini)) {
            foreach ($lini as $l) {
                $this->db->or_like('lini', $l);
            }
        } else {
            $this->db->where('lini', $lini);
        } 
        
        return $this->db->order_by('nama_brand','asc')->get($table)->result();
    }
}
