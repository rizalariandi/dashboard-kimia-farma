<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_sales_margin_summary_model extends MY_Model
{
    public function querys($head, $value, $where = NULL)
    {
        $sql = "
        SELECT 
          " . $this->filterheader($head) . " 
        
        FROM datamart_kf.dm_market_operation1
        WHERE 1=1 
        ";
        // echo nl2br($sql);die();
        return $sql;
    }

    public function getfiltervalue_rsm($id, $gpm)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($gpm)) !== 0) {
            foreach ($gpm as $key => $value) {
                $i++;

                if ($i === sizeof($gpm)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT rsm_code,rsm_name FROM datamart_kf.dm_market_operation1 where gpm_pm_code in (' . $cat . ') AND rsm_code IS NOT NULL group by rsm_code,rsm_name order by rsm_name ASC;';
        } else {
            $query = 'SELECT rsm_code,rsm_name FROM datamart_kf.dm_market_operation1 where rsm_code IS NOT NULL group by rsm_code,rsm_name order by rsm_name ASC;';
        }

        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_shopper($id, $rsm)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($rsm)) !== 0) {
            foreach ($rsm as $key => $value) {
                $i++;

                if ($i === sizeof($rsm)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT shopper_code,shopper_name FROM datamart_kf.dm_market_operation1 where rsm_code in (' . $cat . ') AND shopper_code IS NOT NULL group by shopper_code,shopper_name order by shopper_name ASC;';
        } else {
            $query = 'SELECT shopper_code,shopper_name FROM datamart_kf.dm_market_operation1 where shopper_code IS NOT NULL group by shopper_code,shopper_name order by rsm_name ASC;';
        }

        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_brand($id, $lini)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT brand,brand FROM datamart_kf.dm_market_operation1 where lini_code in (' . $cat . ') AND brand IS NOT NULL group by brand order by brand ASC;';
        } else {
            $query = 'SELECT brand,brand FROM datamart_kf.dm_market_operation1 where brand IS NOT NULL group by brand order by brand ASC;';
        }

        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function querysleft($queryss, $head, $value, $where = NULL, $i = NULL)
    {
        $sql = "";
        $sql .= "select Z.nama,Z.description,Z.counter,X.target,Z.realisasi_this_year, Z.realisasi_last_year, Z.achievement, Z.growth from($queryss) Z left join ";
        $sql .= "(
			select 
			'SALES - PTD' description, 
            '1' counter,
            sum(TARGET_AMOUNT_HJP) target ,
            $head
            from datamart_kf.dm_market_operation1
            where 1=1
             " . $this->condition($where, false) . "             
            group by               
            $head
            UNION ALL 
            
        select 'PTD' description,  
        '2' counter, 
        0 target, $head
        from datamart_kf.dm_market_operation1
            where 1=1
             " . $this->condition($where, false) . "             
            group by              
            $head
            
		UNION ALL             
        select 'SALES' description,
        '3' counter,
        sum(TARGET_AMOUNT_HJP) target,
        $head
          from datamart_kf.dm_market_operation1
          where 1=1
             " . $this->condition($where, false) . "             
          group by
          $head
		UNION ALL         
	      select 'COGS' description, 
	       '4' counter,
	       sum(target_quantity)*ROUND(SUM(HPP_TY)/SUM(QTY_TY)) target,
	       $head
			from datamart_kf.dm_market_operation1
		        where 1=1 and brand = 'ALBUMIN' 
                " . $this->condition($where, false) . "
			  group by
          	$head
           
		UNION ALL         	
			SELECT  'COGS %' description,
            '5' counter,
            sum(target_quantity)*(ROUND(SUM(HPP_TY)/SUM(QTY_TY))/ ROUND(SUM(HJP_TY)/SUM(QTY_TY))) * 100 target,
            $head
              from datamart_kf.dm_market_operation1
            where 1=1
                " . $this->condition($where, false) . "
            group by
            $head
            
            UNION ALL 
            SELECT            
              'MARKETING EXPENSE' description,
            '6' counter,
            0 target,
            $head           
            from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition($where, false) . "
            group by
            $head
            
            UNION  ALL 
            SELECT            
              'MARKETING EXPENSE %' description,
            '7' counter,     
            0 target,       
            $head           
            from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition($where, false) . "
            group by
            $head
            
            UNION  ALL 
            SELECT      
            'GROSS_MARGIN' description,
            '8' counter,
            sum(TARGET_AMOUNT_HJP)-SUM(HPP_TY) target,
            $head
                from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition($where, false) . "
            group by
            $head
            
            UNION ALL 
            SELECT            
            'GROSS MARGIN %' description,
            '9' counter,
            sum(target_quantity)*( ROUND(SUM(HPP_TY)/SUM(QTY_TY))/ ROUND(SUM(HJP_TY)/SUM(QTY_TY))) target,
            $head
             from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition($where, false) . "
            group by
            $head
            ) X";
        $sql .= " on Z.$head = X.$head and Z.description = X.description";
        return $sql;
    }

    public function querysdata($head, $value, $where = NULL, $i = NULL)
    {
        $sql = "";
        // $sql .= "
        // select"        
        //     .$this->filterheader($head).",
        //     $head,
        //     'SALES - PTD' description, 
        //     '1' counter,
        //     SUM(HJP_TY-PTD_TY) realisasi_this_year,
        //     0 realisasi_last_year,
        // 0 target,
        //     0 achievement,
        //     0 growth
        //     from datamart_kf.dm_market_operation1_rsm
        //     where 1=1
        //     ".$this->condition($where,false)."            
        //     ".$this->groupingdata($head)."";



        // $sql .= " UNION ALL
        // select 
        // ".$this->filterheader($head).", 
        // $head,
        // 'PTD' description, 
        // '2' counter,        
        // SUM(PTD_TY) realisasi_this_year,
        // SUM(PTD_LY) realisasi_last_year,
        // 0 target,
        // 0 achievement,
        // 0 growth
        // from datamart_kf.dm_market_operation1_rsm
        // where 1=1
        // ".$this->condition($where,false)."        
        // ".$this->groupingdata($head)."";

        $sql .= " 
        select 
        " . $this->filterheader($head) . ", 
        $head,
        'SALES' description,
        '1' counter,        
        SUM(HNA_TY) realisasi_this_year,
        0 realisasi_last_year,
        0 target,        
        0 achievement,
        0 growth
        from datamart_kf.dm_market_operation1_rsm
                where 1=1
                   " . $this->condition($where, false) . "                  
                 " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'HJP (Baseline)' description, 
            '2' counter,        
            SUM(HJP_TY) realisasi_this_year,
            SUM(HJP_LY) realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 1=1
            " . $this->condition($where, false) . "        
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'HJP - Diskon' description, 
            '3' counter,        
            SUM(HJP_TY-((CASE WHEN lini_code != '103' THEN  disc_2_matriks_amt else 0 end) + (CASE WHEN remark_is_ed = 1 THEN disc_4_ptd_amt else 0 end))) realisasi_this_year,
            SUM(HJP_LY) realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 1=1
            " . $this->condition($where, false) . "        
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'COGS' description, 
            '4' counter,                    
            SUM(HPP_TY) realisasi_this_year,
            SUM(HPP_LY) realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "                    
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'COGS %' description,
            '5' counter,            
            (SUM(HPP_TY) / (SUM(HJP_TY)))*100 realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'Diskon' description,
            '6' counter,            
            SUM(CASE WHEN lini_code != '103' THEN  disc_2_matriks_amt else 0 end) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'Diskon %' description,
            '7' counter,            
            (SUM(CASE WHEN lini_code != '103' THEN  disc_2_matriks_amt else 0 end) / (SUM(HJP_TY)))*100 realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'PTD' description,
            '8' counter,            
            SUM(CASE WHEN remark_is_ed = 1 THEN disc_4_ptd_amt else 0 end) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ", 
            $head,
            'PTD %' description,
            '9' counter,            
            SUM(CASE WHEN remark_is_ed = 1 THEN disc_4_ptd_amt else 0 end)/SUM(HJP_TY)*100 realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'GROSS MARGIN' description,
            '10' counter,           
            SUM(HJP_TY-HPP_TY) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 1=1
             " . $this->condition($where, false) . "              
             " . $this->groupingdata($head) . "
            ";


        $sql .= "
             UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'GROSS MARGIN %' description,
            '11' counter,            
            SUM(HPP_TY/HJP_TY) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1_rsm
            where 
            1=1
            " . $this->condition($where, false) . "            
            " . $this->groupingdata($head) . "
            ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'MARKETING EXPENSE' description,
            '12' counter,            
            SUM(total_expense_ty) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
            ";

        $sql .= " UNION ALL
                select 
                " . $this->filterheader($head) . ",
                $head,
                'MARKETING EXPENSE %' description,
                '13' counter,                
                SUM(total_expense_ty)/sum(hjp_ty) *100 realisasi_this_year,
                0 realisasi_last_year,
                0 target,
                0 achievement,
                0 growth
                from (
                    (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                    INNER JOIN
                    ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                    as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
                    where 1=1           
                " . $this->groupingdata($head) . "
            ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'SALES EXPENSE' description,
            '14' counter,                
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            else 
            0
            end            
            realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'SALES EXPENSE %' description,
            '15' counter,                
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))/sum (hjp_ty) *100
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))/sum (hjp_ty) *100
            else 
            0
            end            
            realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";


        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'TOTAL MARKETING EXPENSE' description,
            '16' counter,                
            (SUM(total_expense_ty) + 
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            else 
            0
            end            
            ) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";


        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'TOTAL MARKETING EXPENSE %' description,
            '17' counter,                
            (SUM(total_expense_ty) + 
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            else 
            0
            end            
            )/ sum (hjp_ty) *100  realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'MARGIN MARKETING' description,
            '18' counter,                
            (SUM(HJP_TY-HPP_TY) - (SUM(total_expense_ty) + 
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            else 
            0
            end            
            )) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty,sum(hpp_ty) hpp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            $head,
            'MARGIN MARKETING %' description,
            '19' counter,                
            (SUM(HJP_TY-HPP_TY) - (SUM(total_expense_ty) + 
            case
            when lini_name = 'OTC & HERBAL' then
            (sum(case when lini_name = 'OTC & HERBAL' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            when lini_name = 'KOSMETIK' then
            (sum(case when lini_name = 'KOSMETIK' then hjp_ty else 0 end)/(select sum(hjp_ty) from datamart_kf.dm_market_operation1_rsm where lini_name IN  ('OTC & HERBAL', 'KOSMETIK')) * SUM(total_expense_ty))
            else 
            0
            end            
            ))/sum(hjp_ty) realisasi_this_year,
            0 realisasi_last_year,
            0 target,
            0 achievement,
            0 growth
            from (
                (SELECT lini_name,lini_code ,bulan_faktur ,sum(hjp_ty) hjp_ty,sum(hpp_ty) hpp_ty from datamart_kf.dm_market_operation1_rsm  where 1=1 " . $this->condition($where, false) . " group by lini_name , bulan_faktur ) as A
                INNER JOIN
                ( select lini_name as lini_name_join,bulan_tahun,sum(total_expense_ty) total_expense_ty FROM datamart_kf.operasional_dm_expense_rsm  where 1=1 " . $this->condition($where, true) . "  group by lini_name,bulan_tahun)
                as B on A.lini_name = B.lini_name_join  and A.bulan_faktur = B.bulan_tahun )
            where 1=1             
            " . $this->groupingdata($head) . "
        ";

        //echo nl2br($sql);       
        return $sql;
    }

    public function querysdata_last($head, $value, $where = NULL, $i = NULL)
    {
        $sql = "";
        $sql .= "
        select 
            " . $this->filterheader($head) . ",
            
            'SALES - PTD' description, 
            '1' counter,
            0 target,
            SUM(HJP_TY-PTD_TY) realisasi_this_year,
            0 realisasi_last_year,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition_last($where) . "
            and TARGET_AMOUNT_HJP is not null
            " . $this->groupingdata($head) . "";



        $sql .= " UNION ALL
        select 
        " . $this->filterheader($head) . ",
         
        'PTD' description, 
        '2' counter,
        0 target,
        SUM(PTD_TY) realisasi_this_year,
        SUM(PTD_LY) realisasi_last_year,
        0 achievement,
        0 growth
        from datamart_kf.dm_market_operation1
        where 1=1
        " . $this->condition_last($where) . "
        and TARGET_AMOUNT_HJP is not null
        " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
        select 
        " . $this->filterheader($head) . ",
         
        'SALES' description,
        '3' counter,
        0 target,
        SUM(HJP_TY) realisasi_this_year,
        0 realisasi_last_year,
        0 achievement,
        0 growth
        from datamart_kf.dm_market_operation1
                where 1=1
                 " . $this->condition_last($where) . "
                  and TARGET_AMOUNT_HJP is not null
                 " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
                    select 
                    " . $this->filterheader($head) . ",
                     
                    'COGS' description, 
                    '4' counter,
                        0 target,
                    SUM(HPP_TY) realisasi_this_year,
                    SUM(HPP_LY) realisasi_last_year,
                    0 achievement,
                    0 growth
                    from datamart_kf.dm_market_operation1
                    where 
                    1=1
                    " . $this->condition_last($where) . "
                    and TARGET_AMOUNT_HJP is not null
                    " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
             
            'COGS %' description,
            '5' counter,
            0 target,
            ( (SUM(HPP_TY)/SUM(HJP_TY)) / (SUM(HJP_TY)/SUM(HJP_TY)) )*100 realisasi_this_year,
            0 relisasi_las_year,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1
            where 
            1=1
            " . $this->condition_last($where) . "
            and TARGET_AMOUNT_HJP is not null
            " . $this->groupingdata($head) . "";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            
            'MARKETING EXPENSE' description,
            '6' counter,
            0 target,
            0 realisasi_this_year,
            0 relisasi_las_year,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1
            where 1=1
            " . $this->condition_last($where) . "
            and TARGET_AMOUNT_HJP is not null
            " . $this->groupingdata($head) . "
            ";

        $sql .= " UNION ALL
                select 
                " . $this->filterheader($head) . ",
                
                'MARKETING EXPENSE %' description,
                '7' counter,
                0 target,
                0 realisasi_this_year,
                0 relisasi_las_year,
                0 achievement,
                0 growth
                from datamart_kf.dm_market_operation1
                where 1=1
                " . $this->condition_last($where) . "
                and TARGET_AMOUNT_HJP is not null
                " . $this->groupingdata($head) . "
            ";

        $sql .= " UNION ALL
            select 
            " . $this->filterheader($head) . ",
            
            'GROSS_MARGIN' description,
            '8' counter,
            0 target,
            SUM(HJP_TY-HPP_TY) realisasi_this_year,
            0 realisasi_last_year,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1
            where 1=1
             " . $this->condition_last($where) . "
              and TARGET_AMOUNT_HJP is not null
             " . $this->groupingdata($head) . "
            ";

        $sql .= "
             UNION ALL
            select 
            " . $this->filterheader($head) . ",
            
            'GROSS MARGIN %' description,
            '9' counter,
            0 target,
            ( 1- (SUM(HPP_TY)/SUM(HJP_TY)) )*100 realisasi_this_year,
            0 realisasi_last_year,
            0 achievement,
            0 growth
            from datamart_kf.dm_market_operation1
            where 
            1=1
            " . $this->condition_last($where) . "
            and TARGET_AMOUNT_HJP is not null
            " . $this->groupingdata($head) . "
            ";
        //echo nl2br($sql);die();

        return $sql;
    }
    public function groupingdata($head)
    {
        $head = $head =='lini_code' ? 'lini_name' :'lini_name';
        $query = "";
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
            } else {

                $query .= " GROUP BY  " . $head;
            }
        }
        return $query;
    }
    public function queryrow($head, $value)
    {
        $sql = "
        SELECT 
        SUM(" . $value . ") realisasi_ty
        FROM dm_marketing_operation
        WHERE 1=1 
        ";
        //echo nl2br($sql);die();
        return $sql;
    }
    public function querylimit()
    {
        $query = "SELECT
        '' nama,
        '' description,
        '' target,
        '' realisasi_this_year,
        '' realisasi_last_year,
        '' achievement,
        '' growth
        ";

        return $query;
    }
    public function querydata($where = NULL, $head = NULL, $value = NULL)
    {
        // var_dump($head);die();
        $query = $this->querys($head, $value, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition($where, false);
        }
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
            } else {

                $query .= " GROUP BY  " . $head;
            }
        }


        // echo nl2br($query);die();
        return $query;
    }
    public function get_limit_data($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $header = NULL, $value = NULL)
    {
        // echo $header;die();
        $query  = $this->querydata($where, $header, $value);
        //echo $value;die();
        //echo nl2br($query);die();
        if ($type == "data") {

            $query .= " LIMIT " . $start . "," . $limit . "";
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        } else if ($type == "numrow") {

            $query .= " LIMIT " . $start . "," . $limit . "";
            return $this->db->query($query)->num_rows();
        } else if ($type == "allrow") {

            return $this->db->query($query)->num_rows();
        } else if ($type == "excel") {
            //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        }
        // echo nl2br($query);die();

    }

    public function ordering($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }
        $query  = "";

        //echo nl2br($querys);die();


        if ($type == "data") {
            if ($start == 0) {
                //                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin");
                //                $create_tmp_data = "
                //                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration_margin(
                //                    SELECT * FROM (
                //                        ".$querys.")X)";
                //
                //                   $this->db->query($create_tmp_data);//die();

                //$count_query = $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin")->num_rows();
                // $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin_data");
                //           $queryss = "
                //           CREATE  TABLE  datamart_admedika.temp_dm_marketing_opration_margin_data(
                //               SELECT * FROM (";
                $queryss = $this->querysdata($header, $value, $where, "");
                $leftjoin  = $this->querysleft($queryss, $header, $value, $where, "");
                // die($queryss);

            } //die();

            //  echo nl2br($queryss);die();
            $query = 'SELECT * FROM (' . $queryss;
            if ($limit == '-1') {
                $query .= " ) X  ORDER BY nama " . $ordered['dir'] . " ,  counter ASC ";
            } else {
                $query .= ") X  ORDER BY nama " . $ordered['dir'] . " ,  counter ASC LIMIT " . $start . "," . $limit;
            }
                // die($query);
            //$query .= " ORDER BY nama ".$ordered['dir']." ,  counter ASC LIMIT ".$start.",".$limit.") X ";
            //  echo nl2br($query);die();

            // echo nl2br($query);
            //echo nl2br($querylimit_total);die();
            // die();
            //    die($query);

            //echo nl2br($querytotal);die();

            $data['data'] = $this->db->query($query)->result_array();
            // die($query);
            return $data;
            //$data['total'] = $this->db->query


        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }
            if ($limit == '-1') {
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data LIMIT " . $start . "," . $limit . "";
            }

            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = '';
            $queryrow = "
            SELECT 
                description,
                counter,
                target,
                realisasi_this_year,
                realisasi_last_year,
                achievement,
                growth
            FROM datamart_admedika.temp_dm_marketing_opration_margin_data;

            ";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }

    public function ordering_last($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }
        $query  = "";

        //echo nl2br($querys);die();


        if ($type == "data") {
            if ($start == 0) {
                //                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin");
                //                $create_tmp_data = "
                //                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration_margin(
                //                    SELECT * FROM (
                //                        ".$querys.")X)";
                //
                //                   $this->db->query($create_tmp_data);//die();

                //$count_query = $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin")->num_rows();
                // $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin_data");
                //           $queryss = "
                //           CREATE  TABLE  datamart_admedika.temp_dm_marketing_opration_margin_data(
                //               SELECT * FROM (";
                $queryss = $this->querysdata_last($header, $value, $where, "");
            } //die();

            //  echo nl2br($queryss);die();
            $query = 'SELECT * FROM (' . $queryss;
            if ($limit == '-1') {
                $query .= " ORDER BY nama " . $ordered['dir'] . " ,  counter ASC ) X ";
            } else {
                $query .= " ORDER BY nama " . $ordered['dir'] . " ,  counter ASC LIMIT " . $start . "," . $limit . ") X ";
            }
            //$query .= " ORDER BY nama ".$ordered['dir']." ,  counter ASC LIMIT ".$start.",".$limit.") X ";
            //  echo nl2br($query);die();

            // echo nl2br($query);
            //echo nl2br($querylimit_total);die();
            // die();


            //echo nl2br($querytotal);die();

            $data['data'] = $this->db->query($query)->result_array();

            return $data;
            //$data['total'] = $this->db->query


        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }
            if ($limit == '-1') {
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data LIMIT " . $start . "," . $limit . "";
            }

            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = '';
            $queryrow = "
            SELECT 
                description,
                counter,
                target,
                realisasi_this_year,
                realisasi_last_year,
                achievement,
                growth
            FROM datamart_admedika.temp_dm_marketing_opration_margin_data;

            ";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }


    public function gettarget($id = NULL)
    {
        if ($id == 'HPP_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HJP_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HJP_PTD_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HNA_TY') {
            $id = 'TARGET_AMOUNT_HNA';
        } else if ($id == 'HJD_TY') {
            $id = 'TARGET_AMOUNT_HNA';
        }
        return $id;
    }

    public function getly($id = NULL)
    {
        if ($id == 'HPP_TY') {
            $id = 'HPP_LY';
        } else if ($id == 'HJP_TY') {
            $id = 'HJP_LY';
        } else if ($id == 'HJP_PTD_TY') {
            $id = 'HJP_PTD_LY';
        } else if ($id == 'HNA_TY') {
            $id = 'HNA_LY';
        } else if ($id == 'HJD_TY') {
            $id = 'HJD_LY';
        }
        return $id;
    }

    public function getfiltervalue($id)
    {
        $lini_code = $id === 'lini_code' ? " where lini_code != 104" : "";
        $query = "
        SELECT * from datamart_admedika." . $id . $lini_code;
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function f_tahun()
    {
        $query = "        
            SELECT RIGHT(BULAN_TAHUN,4) f_tahun 
            FROM datamart_kf.dm_market_operation1
            GROUP BY RIGHT(BULAN_TAHUN,4)
        ";
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }
    public function f_bulan()
    {
        $query = "        
            SELECT LEFT(BULAN_TAHUN,1) f_bulan
            FROM datamart_kf.dm_market_operation1
            GROUP BY LEFT(BULAN_TAHUN,1)
        ";
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function condition($cond = NULL, $for)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {
                if ($key == 'tanggal_faktur_start') {
                    if (!$for) {
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $dateed =  date('Y-m-d', strtotime($date));

                        $where .= " AND tanggal_faktur between '" . $dateed . "'";
                    } else {
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $dateed =  date('Ym', strtotime($date));
                        $where .= " AND bulan_tahun >= '" . $dateed . "'";
                    }
                } else if ($key == 'tanggal_faktur_end') {
                    if (!$for) {
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $date_end =  date('Y-m-d', strtotime($date));
                        $where .= " AND '" . $date_end . "'";
                    } else {
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $date_end =  date('Ym', strtotime($date));
                        $where .= " AND  bulan_tahun <='" . $date_end . "'";
                    }
                } else if ($key == 'value') {
                    $where .= "";
                } else {
                    $where .= "AND IFNULL(" . $key . ",'') in (";
                    for ($i = 0; $i < count($val); $i++) {
                        $val_where = $val[$i] == 'null' || $val[$i] == ''  ? '' : $val[$i];
                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }
        return $where;
    }



    public function condition_last($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);

                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $dateed =  date('Y-m-d', strtotime($date));


                    $where .= " AND tanggal_faktur between '" . $dateed . "'";
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);
                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $date_end =  date('Y-m-d', strtotime($date));
                    $where .= " AND '" . $date_end . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {
                    $where .= "AND IFNULL(" . $key . ",'') in (";
                    for ($i = 0; $i < count($val); $i++) {
                        $val_where = $val[$i] == 'null' || $val[$i] == ''  ? '' : $val[$i];
                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }
        return $where;
    }
    public function filterheader($head)
    {
        $headname = "";
        if ($head == 'Month') {
            $headname =  "date_format(tanggal_faktur,'%m') as nama";
        } else if ($head == 'distributor_code') {
            $headname = "CONCAT(CONCAT(distributor_name,'-'),distributor_code) AS nama";
        } else if ($head == 'branch_code') {
            $headname = "CONCAT(CONCAT(branch_name,'-'),branch_code) AS nama";
        } else if ($head == 'gpm_pm_code') {
            $headname = "CONCAT(CONCAT(gpm_pm_name,'-'),gpm_pm_code) AS nama";
        } else if ($head == 'rsm_code') {
            $headname = "CONCAT(CONCAT(rsm_name,'-'),rsm_code) AS nama";
        } else if ($head == 'shopper_code') {
            $headname = "CONCAT(CONCAT(shopper_name,'-'),shopper_code) AS nama";
        } else if ($head == 'am_apm_asm_code') {
            $headname = "CONCAT(CONCAT(am_apm_asm_name,'-'),am_apm_asm_code) AS nama";
        } else if ($head == 'msr_md_se_code') {
            $headname = "CONCAT(CONCAT(msr_md_se_name,'-'),msr_md_se_code) AS nama";
        } else if ($head == 'segment') {
            $headname = "SEGMENT AS nama";
        } else if ($head == 'customer_code') {
            $headname = "CONCAT(CONCAT(customer_name,'-'),customer_code) AS nama";
        } else if ($head == 'layanan_code') {
            $headname = "CONCAT(CONCAT(layanan_name,'-'),layanan_code) AS nama";
        } else if ($head == 'lini_code') {
            $headname = "CONCAT(CONCAT(lini_name,'-'),lini_code) AS nama";
        } else if ($head == 'material_group1_code') {
            $headname = "CONCAT(CONCAT(material_group1_name,'-'),material_group1_code) AS nama";
        } else if ($head == 'material_group2_code') {
            $headname = "CONCAT(CONCAT(material_group2_name,'-'),material_group2_code) AS nama";
        } else if ($head == 'material_group3_code') {
            $headname = "CONCAT(CONCAT(material_group3_name,'-'),material_group3_code) AS nama";
        } else if ($head == 'brand') {
            $headname = "brand AS nama";
        } else if ($head == 'material_code') {
            $headname = " CONCAT(CONCAT(CONCAT(CONCAT(material_material_name,'-'),kemasan),'-'), material_code ) AS nama";
        }
        //  echo $headname;die();
        return $headname;
    }
    function groupbyheadfilter($header = NULL)
    {
        $query = "";
        if (strlen($header['head_filter']) > 0) {
            if ($header['head_filter'] == 'Month') {
                $query .= " GROUP BY  date_format(tanggal_faktur,'%m') )x";
            } else {

                $query .= " GROUP BY " . $header['head_filter'] . ")x";
            }
        } else {
            $query = ")x";
        }
        return $query;
    }
}
