<?php
class Demografi_model extends CI_Model
{

    public function __construct()
    {
        // $this->db->db_debug = FALSE;
        $this->load->database();
    }

    public function sum_pendidikan($month, $year, $entitas)
    {
        $query = $this->db->query("SELECT pendidikan,SUM(vol) vol FROM datamart_kf.demografi_edu where pendidikan != '' AND year=? AND month = ? AND entitas = ? GROUP BY pendidikan;", array($year, $month, $entitas));

        return $query->result_array();
    }

    public function sum_usia($month, $year, $entitas)
    {
        $query = $this->db->query("SELECT usia,SUM(vol) vol FROM datamart_kf.demografi_age where usia != '' AND  year=? AND month = ? AND entitas = ? GROUP BY usia;", array($year, $month, $entitas));

        return $query->result_array();
    }

    public function sum_jabatan($month, $year, $entitas)
    {
        $query = $this->db->query("SELECT level,SUM(vol) vol FROM datamart_kf.demografi_job where level != '' AND  year=? AND month = ? AND entitas = ? GROUP BY level;", array($year, $month, $entitas));

        return $query->result_array();
    }

    public function sum_masakerja($month, $year, $entitas)
    {
        $query = $this->db->query("SELECT masa_kerja,SUM(vol) vol FROM datamart_kf.demografi_work where masa_kerja != '' AND  year=? AND month =? AND entitas =? GROUP BY masa_kerja;", array($year, $month, $entitas));

        return $query->result_array();
    }

}
