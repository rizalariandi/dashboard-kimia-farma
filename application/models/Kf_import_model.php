<?php
class Kf_import_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getCashCollection($week, $month, $year, $entitas)
	{
		$query = $this->db->query("SELECT *
			FROM f_cashcollection where year='" . $year . "' AND month='" . $month . "' AND week= '" . $week . "' AND entitas='" . $entitas . "' 
			AND datacollection ='CASH COLLECTION'");

		return $query->result_array();
	}

	public function InsertDataCashCollection($datas, $username)
	{
		$json =  json_decode($datas, true);
		foreach ($json as &$value) {
			$query = "INSERT INTO f_cashcollection
			(datacollection, cakupan, target, `real`, achievement, acctarget, accreal, accachievement, entitas, `year`, `month`, week,last_update,create_by)
			VALUES(" . $this->db->escape($value["datacollection"]) . ", " . $this->db->escape($value["cakupan"]) . ", " . $this->db->escape($value["target"]) . ", " . $this->db->escape($value["real"]) . ", " . $this->db->escape($value["achievement"]) . ", " . $this->db->escape($value["acctarget"]) . ", " . $this->db->escape($value["accreal"]) . ", " . $this->db->escape($value["accachievement"]) . ", " . $this->db->escape($value["entitas"]) . ", " . $this->db->escape($value["year"]) . ", " . $this->db->escape($value["month"]) . ", " . $this->db->escape($value["week"])  . "," . "now()" . "," . $this->db->escape($username) . ");";
			$this->db->query($query);
		}
		$return_result = "Insert successful";
		return $return_result;
	}

	public function UpdateDataCashCollection($datas, $username)
	{
		$value =  json_decode($datas, true);
		$query = "UPDATE f_cashcollection
			SET datacollection =" . $this->db->escape($value["datacollection"]) . ", 
			cakupan=" . $this->db->escape($value["cakupan"]) . ",
			target= " . $this->db->escape($value["target"]) . ",
			`real`=" . $this->db->escape($value["real"]) . ",
			achievement= " . $this->db->escape($value["achievement"]) . ",
			acctarget=" . $this->db->escape($value["acctarget"]) . ",
			accreal=" . $this->db->escape($value["accreal"]) . ",
			accachievement= " . $this->db->escape($value["accachievement"]) . ",
			entitas=" . $this->db->escape($value["entitas"]) . ",
			year=" . $this->db->escape($value["year"]) . ", 
			month= " . $this->db->escape($value["month"]) . ", 
			last_update= now()" . ", 
			update_by= " . $this->db->escape($username) . ", 
			week=" . $this->db->escape($value["week"])  . "
			WHERE Id=" . $this->db->escape($value["id"]);
		$this->db->query($query);
		$return_result = "Update successful";
		return $return_result;
	}

	public function DeleteDataCashCollection($id)
	{
		$query = "DELETE FROM f_cashcollection WHERE Id =" . $this->db->escape($id);
		$this->db->query($query);
		$return_result = "Delete successful";
		return $return_result;
	}

	// -------------------------------------------------------------------------------------


	public function getRealisasiInvestas($month, $year, $entitas)
	{
		$query = $this->db->query("SELECT *
			FROM fi_investasi_month where year='" . $year . "' AND month='" . $month . "' AND entitas='" . $entitas . "' 
			");

		return $query->result_array();
	}

	public function InsertRealisasiInvestas($datas, $username)
	{
		$json =  json_decode($datas, true);
		foreach ($json as &$value) {
			$query = "INSERT INTO fi_investasi_month
			(entitas, uraian, target, realisasi, `month`, `year`, achievement,last_update,create_by)
			VALUES(" . $this->db->escape($value["entitas"]) . ",
			 " . $this->db->escape($value["uraian"]) . ", 
			 " . $this->db->escape($value["target"]) . ", 
			 " . $this->db->escape($value["realisasi"]) . ",
			  " . $this->db->escape($value["month"]) . ",
			   " . $this->db->escape($value["year"]) . ", 			   
			   " . $this->db->escape($value["achievement"]) . ",
			   " . "now()" . ",
			   " . $this->db->escape($username) . ");";
			$this->db->query($query);
		}
		$return_result = "Insert successful";
		return $return_result;
	}

	public function UpdateRealisasiInvestas($datas, $username)
	{
		$value =  json_decode($datas, true);
		$query = "UPDATE fi_investasi_month
			SET entitas =" . $this->db->escape($value["entitas"]) . ", 
			uraian=" . $this->db->escape($value["uraian"]) . ",
			target= " . $this->db->escape($value["target"]) . ",
			realisasi=" . $this->db->escape($value["realisasi"]) . ",
			`month`= " . $this->db->escape($value["month"]) . ",
			`year`=" . $this->db->escape($value["year"]) . ",
			last_update= now()" . ", 
			update_by= " . $this->db->escape($username) . ", 
			achievement=" . $this->db->escape($value["achievement"]) . "			
			WHERE Id=" . $this->db->escape($value["id"]);
		$this->db->query($query);
		$return_result = "Update successful";
		return $return_result;
	}

	public function DeleteRealisasiInvestas($id)
	{
		$query = "DELETE FROM fi_investasi_month WHERE Id =" . $this->db->escape($id);
		$this->db->query($query);
		$return_result = "Delete successful";
		return $return_result;
	}

	// --------------------------------------------------------------------------------------------

	public function getAdjBS($year, $entitas, $month)
	{
		$year = $year ? "and year = '" . $year   . "'" : '';
		$entitas = $entitas ? " and entitas = '" . $entitas   . "'" : '';
		$month = $month ? " and month = '" . $month   . "'" : '';
		$query = $this->db->query("SELECT * from kf_target_adj_BS  WHERE Id is not null "
			. $year
			. $entitas
			. $month
			. "order by Id ASC");
		return $query->result_array();
	}

    public function getAdjTarget($year, $entitas, $month)
    {
        $year = $year ? "and year = '" . $year   . "'" : '';
        $entitas = $entitas ? " and distributor_code = '" . $entitas   . "'" : '';
        $month = $month ? " and month = '" . $month   . "'" : '';
        $query = $this->db->query("SELECT branch_code,branch_name,distributor_code,id,lini_code,lini_name,
 month, IFNULL(qty, 0) qty, IFNULL(target_hjd,0) target_hjd, IFNULL(target_hjp,0) target_hjp, IFNULL(target_hna,0) target_hna, 
 IFNULL(target_hna_nett,0) target_hna_nett, IFNULL(target_hpp,0) target_hpp, `year`,last_update from kf_target_branch  WHERE Id is not null "
            . $year
            . $entitas
            . $month
            . "order by Id ASC");
        return $query->result_array();
    }

	public function postAdjBS($id, $adjusment, $entitas, $f01, $month, $real, $target, $uraian, $year, $delete, $nameuser)
	{

		$return_result = array(
			'status' => FALSE,
			'message' => 'Uraian have exited'
		);
		$return_check = 0;
		$whereId = $id ? ' and id !=' . $this->db->escape($id) : '';
		if ($entitas && $uraian && $year && $month) {
			$sqlcheck = $this->db->query('SELECT * from kf_target_adj_BS WHERE entitas=' . $this->db->escape($entitas) . $whereId . ' and `month`=' . $this->db->escape($month) . ' and `year`=' . $this->db->escape($year) . ' and uraian=' . $this->db->escape($uraian));
			$return_check = $sqlcheck->num_rows();
		}


		// add
		if (!$id) {
			if ($return_check == 0) {
				$sql = "INSERT INTO kf_target_adj_BS (`month`, entitas, uraian, target, `real`, f01, adjusment, `year`, last_update, create_by) 
			VALUES (" . $this->db->escape($month) .
					", " . $this->db->escape($entitas) .
					"," . $this->db->escape($uraian) .
					"," . $this->db->escape($target) .
					"," . $this->db->escape($real) .
					"," . $this->db->escape($f01) .
					"," . $this->db->escape($adjusment) .
					"," . $this->db->escape($year) .
					", now() 
					," . $this->db->escape($nameuser) .
					") ";
				$this->db->query($sql);
				if ($this->db->affected_rows() > 0) {
					$return_result = "Insert successful";
				} else {
					$return_result = "Failed to insert record";
				}
			}
		} elseif ($id && $delete) {
			// delete
			$sql = "DELETE FROM kf_target_adj_BS WHERE Id = $id";
			$return_result = $this->db->query($sql);
		} else {
			// update 		
			if ($return_check == 0) {
				$sql = "UPDATE kf_target_adj_BS
			SET `month`=" . $this->db->escape($month) . ",
			entitas=" . $this->db->escape($entitas) . ", 
			uraian=" . $this->db->escape($uraian) . ",
			target=" . $this->db->escape($target) . ",		
			f01=" . $this->db->escape($f01) . ",
			adjusment=" . $this->db->escape($adjusment) . ",
			`year`=" . $this->db->escape($year) . ",
			update_by =" . $this->db->escape($nameuser) . ",
			last_update= now() 		 
			WHERE Id =" . $this->db->escape($id);
				$return_result = $this->db->query($sql);
			}
		}
		$this->cronJobBS();
		return $return_result;
	}

    public function getBranch()
    {
        $query = $this->db->query("select branch_code,branch_name from datamart_kf.kf_target_branch group by branch_code,branch_name order by branch_name ASC;");
        $arr = $query->result_array();

        return $arr;
    }

    public function getLini()
    {
        $query = $this->db->query("select lini_code,lini_name from datamart_kf.kf_target_branch where lini_name != '' group by lini_code,lini_name order by lini_name ASC;");
        $arr = $query->result_array();

        return $arr;
    }

    public function postAdjTarget($id, $branch_code, $branch_name, $distributor_code, $lini_code, $lini_name, $month, $qty, $target_hjd,$target_hjp,$target_hna,$target_hna_nett,$target_hpp,$year, $delete, $nameuser)
    {

        $return_result = array(
            'status' => FALSE,
            'message' => 'Uraian have exited'
        );
        $return_check = 0;
//        $whereId = $id ? ' and id !=' . $this->db->escape($id) : '';
//        if ($entitas && $uraian && $year && $month) {
//            $sqlcheck = $this->db->query('SELECT * from kf_target_branch WHERE entitas=' . $this->db->escape($entitas) . $whereId . ' and `month`=' . $this->db->escape($month) . ' and `year`=' . $this->db->escape($year) . ' and uraian=' . $this->db->escape($uraian));
//            $return_check = $sqlcheck->num_rows();
//        }


        // add

        if (sizeof($month) == 1){
            $bulan_tahun = "0".$month.$year;
        }else{
            $bulan_tahun = $month.$year;
        }

        if (!$id) {
            if ($return_check == 0) {
                $sql = "INSERT INTO kf_target_branch (bulan_tahun,branch_code, branch_name, distributor_code, lini_code, lini_name,`month`, qty,target_hjd,target_hjp,target_hna, target_hna_nett,target_hpp,`year`, last_update, create_by) 
			VALUES (" . $this->db->escape($bulan_tahun) .
                    ", " . $this->db->escape($branch_name) .
                    ", " . $this->db->escape($branch_name) .
                    "," . $this->db->escape($distributor_code) .
                    "," . $this->db->escape($lini_code) .
                    "," . $this->db->escape($lini_name) .
                    "," . $this->db->escape($month) .
                    "," . $this->db->escape($qty) .
                    "," . $this->db->escape($target_hjd) .
                    "," . $this->db->escape($target_hjp) .
                    "," . $this->db->escape($target_hna) .
                    "," . $this->db->escape($target_hna_nett) .
                    "," . $this->db->escape($target_hpp) .
                    "," . $this->db->escape($year) .
                    ", now() 
					," . $this->db->escape($nameuser) .
                    ") ";
                $this->db->query($sql);
                if ($this->db->affected_rows() > 0) {
                    $return_result = "Insert successful";
                } else {
                    $return_result = "Failed to insert record";
                }
            }
        } elseif ($id && $delete) {
            // delete
            $sql = "DELETE FROM kf_target_branch WHERE Id = $id";
            $return_result = $this->db->query($sql);
        } else {
            // update
            if ($return_check == 0) {
                $sql = "UPDATE kf_target_branch
			SET bulan_tahun=" . $this->db->escape($bulan_tahun) . ",
			branch_code=" . $this->db->escape($branch_code) . ",
			branch_name=" . $this->db->escape($branch_name) . ", 
			distributor_code=" . $this->db->escape($distributor_code) . ",
			lini_code=" . $this->db->escape($lini_code) . ",		
			lini_name=" . $this->db->escape($lini_name) . ",
			`month`=" . $this->db->escape($month) . ",
			qty=" . $this->db->escape($qty) . ",
			target_hjd=" . $this->db->escape($target_hjd) . ",
			target_hjp=" . $this->db->escape($target_hjp) . ",
			target_hna=" . $this->db->escape($target_hna) . ",
			target_hna_nett=" . $this->db->escape($target_hna_nett) . ",
			target_hpp=" . $this->db->escape($target_hpp) . ",
			`year`=" . $this->db->escape($year) . ",
			create_by =" . $this->db->escape($nameuser) . ",
			last_update= now() 		 
			WHERE Id =" . $this->db->escape($id);
                $return_result = $this->db->query($sql);
            }
        }
        $this->cronJobBS();
        return $return_result;
    }

    public function cronJobBS()
	{
		$delBS	 = $this->db->query("DELETE FROM `datamart_kf`.`FI_BS_MONTH_CRON`
		WHERE LAST_UPDATE=
		(
		SELECT DISTINCT last_update
		FROM
		(
		SELECT LAST_UPDATE FROM `datamart_kf`.`FI_BS_MONTH_CRON`
		)t1
		ORDER BY last_update DESC
		LIMIT 1
		)");
		if ($delBS) {
			$delBSR	 = $this->db->query("DELETE FROM `datamart_kf`.`FI_BS_RASIO_MONTH_CRON`
			WHERE LAST_UPDATE=
			(
			SELECT DISTINCT last_update
			FROM
			(
			SELECT LAST_UPDATE FROM `datamart_kf`.`FI_BS_RASIO_MONTH_CRON`
			)t1
			ORDER BY last_update DESC
			LIMIT 1
			)");
			if ($delBSR) {
				$createBS = $this->db->query("INSERT INTO datamart_kf.FI_BS_MONTH_CRON (`key`,balance_sheet,jenis,faktor,`real`,
				target,amount,amount_ytd,adjusment,amount_ytd1,pengurang,ket,`year`,`month`,entitas,last_update)
				
				SELECT 
				A.`key`,
				A.balance_sheet,
				A.jenis,
				A.faktor,
				ROUND(COALESCE(B.REAL,0)) AS `real`,
				ROUND(COALESCE(B.TARGET,0)) AS target,
				ROUND(COALESCE(A.amount_,0)) AS amount,
				ROUND(COALESCE(A.amount_ytd_,0)) AS amount_ytd,
				ROUND(COALESCE(B.Adjusment,0)) AS adjusment,
				
				
				ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN COALESCE(B.REAL,0)
					ELSE COALESCE(A.amount_ytd_,0)+COALESCE(B.Adjusment,0) END) AS amount_ytd1,
					
				ROUND(CASE WHEN A.amount_ytd_ IS NULL THEN ROUND(COALESCE(B.REAL,0))
					ELSE (A.amount_ytd_+B.Adjusment) END)-ROUND(COALESCE(B.REAL,0)) AS pengurang,
					
						
				CASE WHEN B.Adjusment IS NULL THEN 'not yet adjusted' 
					 ELSE 'adjusted' END AS ket,
				A.year,
				A.month,
				A.entitas,
				A.last_update
				
				FROM
				(
				  SELECT * FROM datamart_kf.fi_balance_month_ori
				) AS A LEFT JOIN
				(
				SELECT * FROM datamart_kf.kf_target_adj_BS
				) AS B ON A.month=B.month AND A.year=B.year AND A.faktor=B.uraian AND A.entitas=B.entitas
				WHERE A.last_update IN ( SELECT MAX(LAST_UPDATE) FROM datamart_kf.fi_balance_month_ori) 
				ORDER BY A.key,A.balance_sheet");
				if ($createBS) {
					$this->db->query("INSERT INTO datamart_kf.FI_BS_RASIO_MONTH_CRON (WORKING_CAPITAL,WORKING_CAPITAL_YTD,CURRENT_RATIO,CURRENT_RATIO_YTD, 
					ACID_TEST_RASIO,ACID_TEST_RASIO_YTD,CASH_RASIO,CASH_RASIO_YTD,DER_INTEREST_BEARING,DER_INTEREST_BEARING_YTD,DEBT_RASIO,DEBT_RASIO_YTD, 
					EQUITY_RASIO,EQUITY_RASIO_YTD,ENTITAS,`YEAR`,`MONTH`,last_update)
					
					SELECT 
					((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))-
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END))) AS WORKING_CAPITAL,
					((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))-
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))) AS WORKING_CAPITAL_YTD,
					
					((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS CURRENT_RATIO,
					((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS CURRENT_RATIO_YTD,
					
					(((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount ELSE 0 END))-
					(SUM(CASE WHEN faktor='PERSEDIAAN' THEN amount ELSE 0 END)))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS ACID_TEST_RASIO,
					
					(((SUM(CASE WHEN (balance_sheet='ASET' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END))-
					(SUM(CASE WHEN faktor='PERSEDIAAN' THEN amount_ytd1 ELSE 0 END)))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS ACID_TEST_RASIO_YTD,
					
					((SUM(CASE WHEN faktor='KAS DAN SETARA KAS' THEN amount ELSE 0 END))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount ELSE 0 END)))*100 AS CASH_RASIO,
					
					((SUM(CASE WHEN faktor='KAS DAN SETARA KAS' THEN amount_ytd1 ELSE 0 END))/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS' AND JENIS='LANCAR') THEN amount_ytd1 ELSE 0 END)))*100 AS CASH_RASIO_YTD,
					
					(((SUM(CASE WHEN faktor='UTANG BANK' THEN amount ELSE 0 END))+
					(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN - JANGKA PENDEK' THEN amount ELSE 0 END))+
					(SUM(CASE WHEN faktor='PINJAMAN JANGKA MENENGAH (MTN)' THEN amount ELSE 0 END))+
					(SUM(CASE WHEN faktor='PINJAMAN JANGKA PANJANG' THEN amount ELSE 0 END))+
					(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN JANGKA PANJANG' THEN amount ELSE 0 END)))/
					(SUM(CASE WHEN balance_sheet='EKUITAS' THEN amount ELSE 0 END)))*100 AS DER_INTEREST_BEARING,
					
					
					(((SUM(CASE WHEN faktor='UTANG BANK' THEN amount_ytd1 ELSE 0 END))+
					(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN - JANGKA PENDEK' THEN amount_ytd1 ELSE 0 END))+
					(SUM(CASE WHEN faktor='PINJAMAN JANGKA MENENGAH (MTN)' THEN amount_ytd1 ELSE 0 END))+
					(SUM(CASE WHEN faktor='PINJAMAN JANGKA PANJANG' THEN amount_ytd1 ELSE 0 END))+
					(SUM(CASE WHEN faktor='LIABILITAS SEWA PEMBIAYAAN JANGKA PANJANG' THEN amount_ytd1 ELSE 0 END)))/
					(SUM(CASE WHEN balance_sheet='EKUITAS' THEN amount_ytd1 ELSE 0 END)))*100 AS DER_INTEREST_BEARING_YTD,
					
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)))*100 AS DEBT_RASIO,
					
					
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)))*100
					AS DEBT_RASIO_YTD,
					
					
					
					(SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount ELSE 0 END)))*100 AS EQUITY_RASIO,
					
					(SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)/
					(SUM(CASE WHEN (balance_sheet='LIABILITAS') THEN amount_ytd1 ELSE 0 END)+SUM(CASE WHEN (balance_sheet='EKUITAS') THEN amount_ytd1 ELSE 0 END)))*100 AS EQUITY_RASIO_YTD,
					
					
					ENTITAS,
					YEAR,
					MONTH,
					last_update
					FROM datamart_kf.FI_BS_MONTH_CRON
					WHERE last_update IN (SELECT MAX(LAST_UPDATE) FROM datamart_kf.FI_BS_MONTH_CRON)
					GROUP BY 
					ENTITAS,
					YEAR,
					MONTH,
					last_update");
				}
			}
		}
	}
}
