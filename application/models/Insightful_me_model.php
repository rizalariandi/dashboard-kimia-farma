<?php
class Insightful_me_model extends CI_Model {
	
	private $table = 'usc_insight_expense_info';

	public function __construct()
	{
		$this->load->database();
	}

    function dtquery_me($param)
    {
        $q = "SELECT SQL_CALC_FOUND_ROWS 
            pos_rekening,lini,beban_biaya, sum(budget) AS budget, sum(expense) AS expense, sum(sisa_expense) AS sisa_expense, (sum(expense)/sum(budget)) as achieve,
            (select sum(b.expense) from usc_insight_expense_info b WHERE 
            b.bulan = a.bulan and b.tahun1 = a.tahun1-1 and b.lini = a.lini and b.beban_biaya = a.beban_biaya and b.pos_rekening = a.pos_rekening) as expense_1
            FROM ".$this->table." a
            $param[where] group by pos_rekening $param[order] $param[limit]";

        $q2 = "select * from
                (
                select a.lini, a.beban_biaya, a.pos_rekening, a.budget, a.expense, a.sisa_expense,
                    b.expense_1, a.achieve, CAST(((a.expense-b.expense_1)/b.expense_1)as decimal(10,2)) as growth
                from
                (
                SELECT  	lini,pos_rekening,beban_biaya,
                            sum(budget) AS budget, sum(expense) AS expense, sum(sisa_expense) AS sisa_expense,
                            CAST((sum(expense)/sum(budget)) as decimal(10,2)) as achieve
                FROM datamart_kf.usc_insight_expense_info a
                WHERE tahun1 = '$param[tahun]' and bulan BETWEEN '$param[s_bulan]' and '$param[e_bulan]'
                group by lini,pos_rekening,beban_biaya
                )a
                left join
                (
                select lini, pos_rekening, beban_biaya,
                    sum(expense) expense_1
                from datamart_kf.usc_insight_expense_info
                WHERE tahun1 = '$param[tahun_sebelum]' and bulan BETWEEN '$param[s_bulan]' and '$param[e_bulan]'
                group by lini, pos_rekening, beban_biaya
                )b
                on a.lini = b.lini and
                a.beban_biaya = b.beban_biaya  and
                a.pos_rekening = b.pos_rekening
                )aa
                WHERE 1=1 $param[where] $param[order] $param[limit]
        
                ";

        return $this->db->query($q2);
    }

 

    function dtfiltered_me()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_me()
    {
        return $this->db->count_all($this->table);
    }

    function dtquery_me_bb($param)
    {
        // return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
        //     pos_rekening,lini,beban_biaya, sum(budget) AS budget, sum(expense) AS expense, sum(sisa_expense) AS sisa_expense, (sum(expense)/sum(budget)) as achieve,
        //     (select sum(b.expense) from usc_insight_expense_info b WHERE 
        //     b.bulan = a.bulan and b.tahun1 = a.tahun1-1 and b.lini = a.lini and b.beban_biaya = a.beban_biaya and b.pos_rekening = a.pos_rekening) as expense_1
        //     FROM ".$this->table." a
        //     $param[where] group by beban_biaya $param[order] $param[limit]");
        $q2 = "SELECT * FROM (
                    select a.lini,a.beban_biaya,a.pos_rekening,
                        sum(a.budget) as budget,
                        sum(a.expense) as expense,
                        sum(a.sisa_expense) as sisa_expense,
                        sum(b.expense_1) as expense_1,
                        CAST((sum(a.expense)/sum(a.budget)*100) as decimal(10,2))as achieve,
                        CAST(((a.expense-b.expense_1)/b.expense_1)*100 as decimal(10,2)) as growth
                from
                (
                SELECT  	lini,pos_rekening,beban_biaya,
                            sum(budget) AS budget, sum(expense) AS expense, sum(sisa_expense) AS sisa_expense
                FROM datamart_kf.usc_insight_expense_info a
                WHERE tahun1 = '$param[tahun]' and bulan BETWEEN '$param[s_bulan]' and '$param[e_bulan]'
                group by lini,pos_rekening,beban_biaya
                )a
                left join
                (
                select lini, pos_rekening, beban_biaya,
                        sum(expense) expense_1
                from datamart_kf.usc_insight_expense_info
                WHERE tahun1 = '$param[tahun_sebelum]' and bulan BETWEEN '$param[s_bulan]' and '$param[e_bulan]'
                group by lini, pos_rekening, beban_biaya
                )b
                on a.lini = b.lini and
                    a.beban_biaya = b.beban_biaya  and
                    a.pos_rekening = b.pos_rekening
                    GROUP BY a.beban_biaya
                )ab
                WHERE 1=1 $param[where]
                group by beban_biaya $param[order] $param[limit]
                ";
        return $this->db->query($q2);
            
    }
    

    function dtfiltered_me_bb()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_me_bb()
    {
        return $this->db->count_all($this->table);
    }
	
    function get_profit_loss($start,$end,$year,$lini)
    {

        /*if($lini != 'all') {
            $wlini = "AND a.lini_desc1 = '$lini'";
            $wlini_b = "AND b.lini_desc1 = '$lini'";
        } else {*/
            //$wlini = $wlini_b = '';
        //}
        $wlini = '';
        $w = "where tahun = '$year' and (bulan BETWEEN '".(int) $start."' and '".(int) $end."') $wlini";

        $q = "SELECT 
                SUM(cogs) as cogs,
                SUM(gross_margin) as gross_margin,
                SUM(expense) as expense,
                SUM(margin_mkt) as margin_mkt,
                SUM(sales) as sales,
                SUM(sales_ly) as sales_ly,
                SUM(cogs_ly) as cogs_ly,
                SUM(gross_margin_ly) as gross_margin_ly,
                SUM(expense_ly) as expense_ly,
                SUM(margin_mkt_ly) as margin_mkt_ly,
                SUM(cogs_persen) as cogs_persen,
                SUM(cogs_persen_ly) as cogs_persen_ly,
                SUM(gross_margin_persen) as gross_margin_persen,
                SUM(gross_margin_persen_ly) as gross_margin_persen_ly,
                SUM(expense_persen) as expense_persen,
                SUM(expense_persen_ly) as expense_persen_ly,
                SUM(margin_mkt_persen) as margin_mkt_persen,
                SUM(margin_mkt_persen_ly) as margin_mkt_persen_ly,
                SUM(target_sales) as target_sales,
                SUM(target_cogs) as target_cogs,
                SUM(target_cogs_persen) as target_cogs_persen,
                SUM(target_gross_margin) as target_gross_margin,
                SUM(target_gross_margin_persen) as target_gross_margin_persen,
                SUM(target_expense) as target_expense,
                SUM(target_expense_persen) as target_expense_persen,
                SUM(target_margin_mkt) as target_margin_mkt,
                SUM(target_margin_mkt_persen) as target_margin_mkt_persen
              FROM usc_insight_profit_loss $w";


        return $this->db->query($q)->row();
    }

    function get_lini_expanse()
    {
        return $this->db->query("SELECT lini_desc1 as lini
                from usc_insight_realisasi_expenses 
                group by lini_desc1
                order by lini_desc1 asc");
    }

    function get_lini_me_info()
    {
        return $this->db->query("SELECT lini
                from usc_insight_expense_info 
                group by lini
                order by lini asc");
    }
}
