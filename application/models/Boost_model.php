<?php
class Boost_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    // BOOSTING PRODUCT

    function get_bcg($table, $area, $produk)
    {
        $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        $this->db->where('produk_id', $produk);
        $this->db->where('area', $area);
        return json_encode($this->db->get($table)->result(), JSON_NUMERIC_CHECK);
    }

    function get_bcg_product($table, $area, $produk,$kompetitor) {
        return $this->db->distinct()->select('produk')->where("(produk='$produk' OR produk='$kompetitor')")->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
    }

    function get_bcg_product_detils($table, $area, $produk,$kompetitor) {
        return $this->db->distinct()->select('produk')->like('produk',$produk)->or_like('produk',$kompetitor)->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
    }

    function get_list_channel($table, $area, $produk) {
        return $this->db->distinct()->select('channel')->where('produk',$produk)->where('provinsi', $area)->order_by('channel','asc')->get($table)->result();
    }

    function get_varian_product($table, $area, $produk) {
        return $this->db->distinct()->select('produk')->where('produk_id',$produk)->where('provinsi', $area)->where('kategori',1)->order_by('produk','asc')->get($table)->result();
    }

    function get_sku($table, $brand) {
        $this->db->distinct()->select('nama_produk');
        if (is_array($brand)) {
            foreach ($brand as $b) {
                $this->db->or_like('nama_brand', $b);
            }
        } else {
            $this->db->where('nama_brand', $brand);
        }

        return $this->db->order_by('nama_produk','asc')->get($table)->result();
    }

    function get_brand($table, $lini) {
        $this->db->distinct()->select('nama_brand');
        if (is_array($lini)) {
            foreach ($lini as $l) {
                $this->db->or_like('lini', $l);
            }
        } else {
            $this->db->where('lini', $lini);
        } 
        
        return $this->db->order_by('nama_brand','asc')->get($table)->result();
    }

    function check_data($table, $area, $produk, $from='', $to='')
    {
        if(($table == 'usc_bst_product_mart') or ($table == 'usc_bst_product_details_mart') ){
            $this->db->where('produk_id', $produk);
        } else {
            $this->db->where('produk', $produk);
        }
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        return $this->db->get($table)->result();
        
    }

    function get_data_export($table, $area, $produk, $from, $to) {
        $this->db->select('produk, provinsi, bulan, SUM(total_sales) as sales, SUM(quantity) as quantity');
        $this->db->where('provinsi', $area);
        $this->db->where('produk_id', $produk);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("produk","bulan"));

        return $this->db->get($table)->result_array();
    }

    function get_data_export_channel($table, $area, $produk, $from, $to) {
        $this->db->select('produk, channel, provinsi, bulan, SUM(sales) as sales, SUM(quantity) as quantity');
        $this->db->where('provinsi', $area);
        $this->db->where('produk', $produk);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("channel","bulan"));

        return $this->db->get($table)->result_array();
    }

    function get_bcg_matrix($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        $list_produk = $this->get_bcg_product($table, $area, $produk, $kompetitor);
        // var_dump($produk);
        // var_dump($kompetitor);
        // var_dump($list_produk);
        //get market share
        if ($filter == 'value'){
            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk .'", total_sales, 0)) / SUM( IF( produk_id= "' . $produk .'", total_sales, 0)) AS DECIMAL(10,2)) * 100 as "' . $p->produk . '"');
            }
        } else {
            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk .'", quantity, 0)) / SUM( IF( produk_id= "' . $produk .'", quantity, 0)) AS DECIMAL(10,2)) * 100 as "' . $p->produk . '"');
            }
        }
        $this->kf->from($table);
        $this->kf->where('provinsi', $area);
        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $output = $this->kf->generate();

        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');
            $this->db->where('produk_id', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');
            $this->db->where('produk_id', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();
        $growth = array();
        $bulan = array();
        $json = array();
        $marketshare = json_decode($output);

        foreach($list_produk as $p=>$val) {
            $json[$p]["y"] = 0;                            

            foreach($output_growth as $key=>$value) {
                $growth[$value->produk][] = 0;
                $bulan[$value->produk][] = 0;
                if ($val->produk == $value->produk) {
                    $growth[$value->produk][] = $value->total;
                    $bulan[$value->produk][] = $value->bulan;
                }
            }
            if (empty($output_growth)) {
                $json[$p]["y"] = 0;                            
            } else {
                foreach($output_growth as $key=>$value) {
                    if ($val->produk == $value->produk) {
                        $m = linear_regression($bulan[$val->produk], $growth[$val->produk]);
                        $market_growth[$val->produk] = $m["m"];
                        $json[$p]["y"] = $market_growth[$val->produk];                            
                    }
                }
            }
        }

        $varian_produk = $this->get_varian_product($table, $area, $produk);
        foreach ($list_produk as $key=>$value){
            foreach($marketshare->data as $market)
            {
                $in = $value->produk;
                $json[$key]["x"] = $marketshare->data[0]->$in;
            }

            $json[$key]["name"] = $value->produk;
            if ($value->produk == $produk) {
                $json[$key]["color"] = '#FFA351FF';
            }
            foreach ($varian_produk as $vp){
                if ($value->produk == $vp->produk) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }

        // var_dump($json);
        // die();
        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }

    function get_bcg_matrix_detils($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        $list_produk = $this->get_bcg_product_detils($table, $area, $produk, $kompetitor);
        // var_dump($produk);
        // var_dump($kompetitor);
        // var_dump($list_produk);
        //get market share
        if ($filter == 'value'){
            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk .'", total_sales, 0)) / SUM( IF( produk_id= "' . $produk .'", total_sales, 0)) AS DECIMAL(10,2)) * 100 as "' . $p->produk . '"');
            }
        } else {
            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk .'", quantity, 0)) / SUM( IF( produk_id= "' . $produk .'", quantity, 0)) AS DECIMAL(10,2)) * 100 as "' . $p->produk . '"');
            }
        }
        $this->kf->from($table);
        $this->kf->where('provinsi', $area);
        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $output = $this->kf->generate();

        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');
            $this->db->where('produk_id', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');
            $this->db->where('produk_id', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();
        $growth = array();
        $bulan = array();
        $json = array();
        $marketshare = json_decode($output);

        foreach($list_produk as $p=>$val) {
            $json[$p]["y"] = 0;                            

            foreach($output_growth as $key=>$value) {
                $growth[$value->produk][] = 0;
                $bulan[$value->produk][] = 0;
                if ($val->produk == $value->produk) {
                    $growth[$value->produk][] = $value->total;
                    $bulan[$value->produk][] = $value->bulan;
                }
            }
            if (empty($output_growth)) {
                $json[$p]["y"] = 0;                            
            } else {
                foreach($output_growth as $key=>$value) {
                    if ($val->produk == $value->produk) {
                        $m = linear_regression($bulan[$val->produk], $growth[$val->produk]);
                        $market_growth[$val->produk] = $m["m"];
                        $json[$p]["y"] = $market_growth[$val->produk];                            
                    }
                }
            }
        }

        $varian_produk = $this->get_varian_product($table, $area, $produk);
        foreach ($list_produk as $key=>$value){
            foreach($marketshare->data as $market)
            {
                $in = $value->produk;
                $json[$key]["x"] = $marketshare->data[0]->$in;
            }

            $json[$key]["name"] = $value->produk;
            if ($value->produk == $produk) {
                $json[$key]["color"] = '#FFA351FF';
            }
            foreach ($varian_produk as $vp){
                if ($value->produk == $vp->produk) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }

        // var_dump($json);
        // die();
        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }

    function total_all_by_produk($area, $produk, $filter = 'value', $from='', $to='') {
        if ($filter == 'value') {
            $this->db->select('SUM(sales) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
        } else {
            $this->db->select('SUM(quantity) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
        }
        $total_sales = $this->db->get('usc_bst_channel_mart')->result();
        return (int)$total_sales[0]->total;
    }

    function get_bcg_matrix_channel($table, $area, $produk, $filter = 'value', $from='', $to='')
    {
        $list_produk = $this->get_list_channel($table, $area, $produk);
        $total_all_by_produk = $this->total_all_by_produk($area, $produk, $filter, $from, $to);

        //get market share
        if ($filter == 'value') {
            $this->db->select('channel, SUM(sales) / ' . $total_all_by_produk . ' as market_share');
        } else {
            $this->db->select('channel, SUM(quantity) / ' . $total_all_by_produk . ' as market_share');
        }
        $this->db->where('produk', $produk);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("channel"));
        $output_market = $this->db->get($table)->result();
    
        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('channel, bulan, SUM(sales) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("channel","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('channel, bulan, SUM(quantity) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("channel","bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();

        $growth = array();

        foreach($list_produk as $p=>$val) {
            $realisasi_awal = -1;
            $realisasi_akhir = 0;    

            foreach($output_growth as $key=>$value) {
                if ($val->channel == $value->channel) {
                    if ($value->channel == "") {
                        continue;
                    }
                    // $growth[$p][] = number_format((float) ($value->total / $total_growth[$p]->total), 2, '.', '' ) * 100 ;
                    if( $value->total == 0 and $realisasi_awal == -1) {
                        $realisasi_awal = 0;
                    } else if($realisasi_awal == -1){
                        $realisasi_awal = $value->total;
                    } 
                    
                    // echo $realisasi_awal ."\n";
                    $realisasi_akhir = $realisasi_akhir + $value->total; 
                   
                    if ($realisasi_awal == 0  ) {
                        $growth[$val->channel] = 100;
                    } else {
                        $growth[$val->channel] = number_format((float) (($realisasi_akhir - $realisasi_awal) / $realisasi_awal),2,'.','' )* 100;
                    }
                }
            }
        }
        // var_dump($growth);
        // die();

        $json = array();
        foreach ($list_produk as $key=>$value){
            $json[$key]["name"] = $value->channel;
            $market_share = 0;
            foreach($output_market as $market)
            {
                if ($value->channel == "")
                {
                    continue;
                } 
                if( $market->channel == $value->channel)
                {
                    $market_share = $market->market_share * 100;
                    $json[$key]["x"] = $market_share;
                }
            }
  
            foreach($output_growth as $g){
                if ($value->channel == "")
                {
                    continue;
                }
                if( $g->channel == $value->channel) {
                    $json[$key]["y"] = $growth[$value->channel];
                }
            }
            $json[$key]["color"] = color_channel($value->channel);
        }
        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    function get_bcg_channel($table, $area, $produk)
    {
        $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, channel as name');
        $this->db->where('produk_id', $produk);
        $this->db->where('area', $area);
        return json_encode($this->db->get($table)->result(), JSON_NUMERIC_CHECK);
    }

    function get_bcg_x($table, $area, $produk)
    {
        $data = $this->db->select('batasan_market_share')
            ->where('area', $area)
            ->where('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error            
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_share;
        }
    }

    function get_bcg_y($table, $area, $produk)
    {
        $data = $this->db->select('batasan_market_growth')
            ->where('area', $area)
            ->where('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_growth;
        }
    }

    function get_area($table)
    {
        // return $this->db->distinct()->select('area')->get($table)->result();
        return $this->db->distinct()->select('provinsi')->get($table)->result();
    }

    // function get_channels(){
    //    return $this->db->distinct()->select('nama_channel')->get('mart_bcg_channel')->result();
    // }

    function get_produk($table)
    {
        return $this->db->distinct()->select('produk_id')->get($table)->result();
    }
    function get_kompetitor($table)
    {
        return $this->db->distinct()->select('produk')->get($table)->result();
    }

    function get_produk_channel($table)
    {
        return $this->db->distinct()->select('produk')->get($table)->result();
    }

    function get_bcg_rekomendasi($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';

        // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        // $this->db->where('nama_produk', $produk);
        // $this->db->where('area', $area);
        // $data = $this->db->get($table)->row();
        $bcg = $this->Boost_model->get_bcg_matrix($table, $area, $produk, $filter, $kompetitor,  $from, $to);
        $data = json_decode($bcg);
        // var_dump($data);
        // die();
        $arr_data = array();
        foreach ($data as $d) {
            if ($d->name == $produk) {
                $arr_data["x"] = $d->x;
                $arr_data["y"] = $d->y;
            }
        }
        // var_dump($arr_data);
        // die();
        $check = $this->check_data($table, $area, $produk, $from, $to);

        if (empty($data) or empty($check)) {
            $rekomendasi = 'Data market share dan market growth untuk produk ' .$produk. ' tidak ada';
        } else {
            $marketvalue = $this->get_marketvalue($table, $area, $produk, $arr_data["x"], $arr_data["y"], $filter);
            if (($marketvalue['share'] == 'High' and $marketvalue['growth'] == 'High') or ($marketvalue['share'] == 'Low' and $marketvalue['growth'] == 'High')){
                $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
            } 
            else {
                $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
                $rekomendasi .= $this->rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to);
            }
            $rekomendasi = str_replace("[FITUNO/BATUGIN]", $produk, $rekomendasi);
            $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '.$produk, $rekomendasi);
            $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);
        }
        return $rekomendasi;
    }

    function rekomendasi_bcg_detils($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';
        // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        // $this->db->like('LOWER(nama_produk)', strtolower($produk));
        // $this->db->where('area', $area);
        // $datas = $this->db->get($table)->result();
        $bcg = $this->Boost_model->get_bcg_matrix($table, $area, $produk,$kompetitor, $filter,  $from, $to);
        // var_dump($bcg);
        // die();
        $datas = json_decode($bcg);

        $check = $this->check_data($table, $area, $produk, $from, $to);
        // var_dump($datas);
        // die();

        if (empty($datas)  or empty($check)) {
            $rekomendasi = 'Data market share dan market growth untuk produk ' .$produk. ' tidak ada';
        }else {
            foreach ($datas as $data) {
                if(strpos($data->name, $produk) !== false) {    
                    $marketvalue = $this->get_marketvalue($table, $area, $produk, $data->x, $data->y, $filter);
                    $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
                    $rekomendasi = str_replace("[VARIAN PRODUK]", $data->name, $rekomendasi);
                }
            }
        }
        
        $rekomendasi = str_replace("[FITUNO/BATUGIN]", $produk, $rekomendasi);
        $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '.$produk, $rekomendasi);
        $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);
        return $rekomendasi;
    }

    function rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';
        // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        // $this->db->where('produk_id', $produk);
        // $this->db->where('nama_produk <>', $produk);
        // $this->db->where('area', $area);
        // $datas = $this->db->get($table)->result();

        $bcg = $this->Boost_model->get_bcg_matrix($table, $area, $produk, $filter, $kompetitor,  $from, $to);
        $datas = json_decode($bcg);
        // var_dump($datas);
        // die();

        foreach ($datas as $data) {
            if ($data->name != $produk){
                $marketvalue = $this->get_marketvalue($table, $area, $produk, $data->x, $data->y, $filter);
                $rekomendasi .= '<li> Perspektif : Kompetitor - ' . $data->name . ' ' . $this->rekomendasi('Kompetitor', $marketvalue['share'], $marketvalue['growth']) . '</li>';    
            }
        }

        // var_dump($rekomendasi);
        // die();
        return $rekomendasi;
    }

    function get_marketvalue($table, $area, $produk, $xproduk, $yproduk, $filter)
    {
        if ($table == 'usc_bst_product_mart') {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_quantity', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_quantity', $area, $produk);
            }
        } else {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_details_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
            }
        }

        if ($xproduk >= $x) {
            $data['share'] = 'High';
        } else {
            $data['share'] = 'Low';
        }
        if ($yproduk >= $y) {
            $data['growth'] = 'High';
        } else {
            $data['growth'] = 'Low';
        }
        return $data;
    }

    function rekomendasi($table, $market_share, $market_growth)
    {
        if ($table == 'usc_bst_product_mart') {
            $perspektif = 'BRAND KF';
        } else if ($table == 'usc_bst_product_details_mart') {
            $perspektif = 'Varian Produk KF';
        } else {
            $perspektif = 'Kompetitor';
        }

        $rekomendasi = $this->db->select('rekomendasi')
            ->where('perspektif', $perspektif)
            ->where('market_share', $market_share)
            ->where('product_growth', $market_growth)
            ->get('usc_bst_product_recommendation')->row()->rekomendasi;
        return $rekomendasi;
    }

    function sell_out_kfa($table, $area, $produk,$kompetitor, $filter = 'value', $from='', $to='')
    {
        if ($filter == 'value') {
            $this->db->select("produk,nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("produk,nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }
        $this->db->where("(produk='$produk' OR produk='$kompetitor')");
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result();

        $this->db->distinct()->select('nama_kfa');
        $this->db->where("(produk='$produk' OR produk='$kompetitor')");
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->order_by('nama_kfa', 'asc');
        $kfa = $this->db->get($table)->result();
        
        $this->db->distinct()->select("date_format(tanggal, '%M-%y') as bulan");
        $this->db->where("(produk='$produk' OR produk='$kompetitor')");
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by('bulan');
        $bulan = $this->db->get($table)->result();
        // var_dump($produk);
        // var_dump($kompetitor);
        // var_dump($datas);
        // die();
        $json = array();
        $cat = array();

        foreach($bulan as $b){
            $cat[] = $b->bulan;
        }

        foreach($kfa as $key=>$kf) {
            $json[$key]["name"] = $kf->nama_kfa;
            $total_sales = array();
            foreach($datas as $k=>$data) {
                if ($kf->nama_kfa == $data->nama_kfa){
                    $total_sales[] = $data->total_sales;
                }
            }    
            $json[$key]["data"] = $total_sales;
        }

        $output = array(
            'categories' => json_encode($cat),
            'data' => json_encode($json, JSON_NUMERIC_CHECK)
        );

        return json_encode($output);
    }

    public function data_sell_out($table, $area, $produk, $filter = 'value', $from='', $to='') {
        if ($filter == 'value') {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }
        $this->db->where('produk_id', $produk);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result_array();
        return $datas;
    }

    // BRAND AREA
    function json_brand_area()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $brand = $this->input->post("brand", true);
        $produk = $this->input->post("produk", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $data_brand = explode(",", $brand);
        $data_produk = explode(",", $produk);
        $kftd = $this->get_kftd();
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        $this->kf->select('nama_brand');
        $this->kf->from('usc_bst_sales_area_brand');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();

        $this->kf->group_start();
        foreach ($data_brand as $brand){
            $this->kf->or_like('nama_brand', $brand);
        }
        $this->kf->group_end();

        $this->kf->group_start();
        foreach ($data_produk as $produk){
            $this->kf->or_like('nama_produk', $produk);
        }
        $this->kf->group_end();
        // $this->kf->group_start();
        //     $this->kf->like('provinsi', strtoupper($provinsi));
        // $this->kf->group_end();
        $this->kf->group_by('nama_brand');
        $temp =  $this->kf->generate();
        $temp2 = json_decode($temp);
        $i = 0;
		foreach($temp2->data as $data){
            $total = array_sum((array)$data);
            foreach ($kftd as $kf){
                if($total != 0){
                    $in = $kf->nama_kftd;
                    $value = $temp2->data[$i]->$in;
                    $rev = $value / $total * 100;
                }else{
                    $rev = 0;
                }
                $revval = number_format((float)$rev, 2, '.', '');
               $temp2->data[$i]->$in = $revval."%";
            }
			$i++;
        }
        return json_encode($temp2);
    }
    

    function json_area_brand()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $produk = $this->input->post("produk", true);

        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        if(!is_array($brand)){
            $data_brand = explode(",", $brand);
            $data_produk = explode(",", $produk);
        }else{
            $data_brand =$brand;
            $data_produk = $produk;
        }
        
        if($brand != "" || $produk != ""){
            $kftd = $this->Boost_model->get_kftd_brand2();
        }else{
            $kftd = $this->Boost_model->get_kftd_brand();

        }
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_brand = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
        }
        $this->kf->select('nama_kftd as area');
        $this->kf->from('usc_bst_sales_area_brand');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();
        
        if($brand != null){
            $this->kf->group_start();
            foreach ($data_brand as $brand) {
                $this->kf->or_like('nama_brand', $brand);
            }
            $this->kf->group_end();
        }
        if($produk != null){
        

        $this->kf->group_start();
        foreach ($data_produk as $produk) {
            $this->kf->or_like('nama_produk', $produk);
        }
        $this->kf->group_end();
    }
        $this->kf->group_start();
        $this->kf->like('provinsi', strtoupper($provinsi));
        $this->kf->group_end();
        $this->kf->group_by('nama_kftd');
        $temp =  $this->kf->generate();
        $temp2 = json_decode($temp);
        $i = 0;
		foreach($temp2->data as $data){
            $total = array_sum((array)$data);
            foreach ($kftd as $kf){
                if($total != 0){
                    $in = $kf->nama_brand;
                    if($in != ""){
                        $value = $temp2->data[$i]->$in;
                    }else{
                        $tempval = (array)$temp2->data[$i];
                        $value = $tempval[""];
                    }
                    $rev = $value / $total * 100;
                }else{
                    $rev = 0;
                }
                 $revval = number_format((float)$rev, 2, '.', '');
               $temp2->data[$i]->$in = $revval."%";
            }
			$i++;
        }
        return json_encode($temp2);
    }


    function get_excel_brand_area(){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
		$keyword = $this->input->post("keyword", true);

        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->get_kftd();
        $this->db->select('nama_brand');
        foreach ($kftd as $kf) {
            $this->db->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        $this->db->from('usc_bst_sales_area_brand');
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        $this->db->like("nama_brand", $keyword);
        $this->db->group_start();
        foreach ($data_lini as $lin) {
            $this->db->or_like('lini', $lin);
        }
        $this->db->group_end();
        
        $this->db->group_start();
        foreach ($data_layanan as $lay){
            $this->db->or_like('layanan', $lay);
        }
        $this->db->group_end();
        // $this->db->group_start();
        //     $this->db->like('provinsi', strtoupper($provinsi));
        // $this->db->group_end();
        $this->db->group_by('nama_brand');
        return $this->db->get()->result();
    }

    function get_excel_area_brand(){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
		$keyword = $this->input->post("keyword", true);
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->get_kftd_brand2();
        $this->db->select('nama_kftd as area');
        foreach ($kftd as $kf) {
            $this->db->select('SUM( IF( nama_brand = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
        }
        $this->db->from('usc_bst_sales_area_brand');
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        $this->db->like('nama_kftd', $keyword);
        $this->db->group_start();
        foreach ($data_lini as $lin) {
            $this->db->or_like('lini', $lin);
        }
        $this->db->group_end();
        
        $this->db->group_start();
        foreach ($data_layanan as $lay){
            $this->db->or_like('layanan', $lay);
        }
        $this->db->group_end();

        $this->db->group_start();
        $this->db->like('provinsi', strtoupper($provinsi));
        $this->db->group_end();
        $this->db->group_by('nama_kftd');
        return $this->db->get()->result();
    }

    function get_kftd_brand()
    {
        return $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS", REPLACE(nama_brand, ".", "")) as nama_brand ')->get('usc_bst_sales_area_brand')->result();
    }

    function get_kftd_brand2(){
        $brand = $this->input->post("brand");
        $produk = $this->input->post("produk");
        if(!is_array($brand)){
            $data_brand = explode(",", $brand);
            $data_produk = explode(",", $produk);
        }else{
            $data_brand =$brand;
            $data_produk = $produk;
        }
        // die();
        

    
        $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS", REPLACE(nama_brand, ".", "")) as nama_brand ');
        $this->db->from('usc_bst_sales_area_brand');
        if($brand != null){
            $this->db->group_start();
            foreach ($data_brand as $brand) {
                $this->db->or_like('nama_brand', $brand);
            }
            $this->db->group_end();
        }
        if($produk != null){
        

        $this->db->group_start();
        foreach ($data_produk as $produk) {
            $this->db->or_like('nama_produk', $produk);
        }
        $this->db->group_end();
    }
        return $this->db->get()->result();
    }
    function get_kftd()
    {
        $provinsi = $this->input->post("provinsi");
        return $this->db->distinct()->select('nama_kftd')->like('provinsi', strtoupper($provinsi))->get('usc_bst_sales_area_brand')->result();

    }

    function get_list_lini()
    {
        return $this->db->distinct()->select('lini')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_layanan()
    {
        return $this->db->distinct()->select('layanan')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_brand()
    {
        return $this->db->distinct()->select('nama_brand')->get('usc_bst_sales_area_brand')->result();
    }

    function get_list_produk()
    {
        return $this->db->distinct()->select('nama_produk')->get('usc_bst_sales_area_brand')->result();
    }


    function get_revenue($provinsi){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        if(!is_array($lini)){
           $data_lini = explode(",", $lini);
        }else{
            $data_lini = $lini;
        }
        if(!is_array($layanan)){
        $data_layanan = explode(",", $layanan);

        }else{
            $data_layanan = $layanan;
        }
       
        $this->db->select("provinsi, sum(revenue) as total");
        $this->db->where("provinsi", strtoupper($provinsi));
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
     
            $this->db->group_start();
            foreach ($data_lini as $lin) {
                $this->db->or_like('lini', $lin);
            }
            $this->db->group_end();
       
        
            $this->db->group_start();
            foreach ($data_layanan as $lay){
                $this->kf->or_like('layanan', $lay);
            }
            $this->db->group_end();
       
        $this->db->group_by("provinsi");
        $data = $this->db->get('usc_bst_sales_area_brand');
        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }
    }
  
     // Brand channel
     function get_cus_brandc()
     {
         $area = $this->input->post("provinsi", true);
         // if($area == ""){
         //     $area = "DKI JAKARTA";
         // }
         $produk = $this->input->post("produk", true);
         $lini = $this->input->post("lini", true);
         if(is_array($lini)){	
            $data_lini=$lini;	
        }else{
            $data_lini=explode(',', $lini);
        }
         $this->db->distinct()->select('nama_customer');
         $this->db->group_start();
         $this->db->like('provinsi', $area);
         $this->db->group_end();
         $this->kf->group_start();
         foreach ($data_lini as $lin) {
             $this->kf->or_like('lini', $lin);
         }
         $this->kf->group_end();
         $this->db->group_start();
         $this->db->like('nama_brand', $produk);
         $this->db->group_end();
         return $this->db->get('usc_bst_sales_brand_channel')->result();
     }
    //  function get_tahun_brandc(){
    //      $area = $this->input->post("provinsi", true);
    //      $produk = $this->input->post("produk", true);
    //      $this->db->distinct()->select("year(str_to_date(tanggal, '%Y-%m-%d')) as tahun");
    //      $this->db->group_start();
    //      $this->db->like('provinsi', $area);
    //      $this->db->group_end();
    //      $this->db->group_start();
    //      $this->db->like('nama_brand', $produk);
    //      $this->db->group_end();
    //      return $this->db->get('usc_bst_sales_brand_channel')->result();
    //  }
     function get_chan_brand()
     {
         $area = $this->input->post("provinsi", true);
         $produk = $this->input->post("produk", true);
         $lini = $this->input->post("lini", true);
         if(is_array($lini)){	
            $data_lini=$lini;	
        }else{
            $data_lini=explode(',', $lini);
        }


         $this->db->distinct()->select('IF(nama_brand = "MARCKS\'", "MARCKS", REPLACE(nama_brand, ".", "")) as nama_brand ');
         $this->db->group_start();
         $this->db->like('nama_kftd', $area);
         $this->db->group_end();
         $this->kf->group_start();
         foreach ($data_lini as $lin) {
             $this->kf->or_like('lini', $lin);
         }
         $this->kf->group_end();
         $this->db->group_start();
         $this->db->like('nama_brand', $produk);
         $this->db->group_end();
         return $this->db->get('usc_bst_sales_brand_channel')->result();
 
     }
  
     function get_chan()
     {
        $area = $this->input->post("provinsi", true);
        $produk = $this->input->post("produk", true);
        $lini = $this->input->post("lini", true);
        if(is_array($lini)){	
           $data_lini=$lini;	
       }else{
           $data_lini=explode(',', $lini);
       }
 
         $this->db->distinct()->select("channel, CASE WHEN channel !='Marketing' THEN 0 ELSE 1 END AS ordering");
         $this->db->group_start();
         $this->db->like('nama_kftd', $area);
         $this->db->group_end();
         $this->kf->group_start();
         foreach ($data_lini as $lin) {
             $this->kf->or_like('lini', $lin);
         }
         $this->kf->group_end();
         $this->db->group_start();
         $this->db->like('nama_brand', $produk);
         $this->db->group_end();
         $this->db->order_by('ordering');
         $this->db->order_by('channel');
         return $this->db->get('usc_bst_sales_brand_channel')->result();
     }
 
     function get_lini_list()
     {
         $area = $this->input->post("provinsi", true);
         $produk = $this->input->post("produk", true);
 
         $this->db->distinct()->select('lini');
         $this->db->group_start();
         $this->db->like('nama_kftd', $area);
         $this->db->group_end();
         $this->db->group_start();
         $this->db->like('nama_brand', $produk);
         $this->db->group_end();
         return $this->db->get('usc_bst_sales_brand_channel')->result();
 
     }
     function get_area_list()
     {
 
        //  return $this->db->distinct()->select('provinsi')->get('usc_bst_sales_brand_channel')->result();
         $produk = $this->input->post("produk", true);
 
         $this->db->distinct()->select('nama_kftd');
         $this->db->group_start();
         $this->db->like('nama_brand', $produk);
         $this->db->group_end();
         return $this->db->get('usc_bst_sales_brand_channel')->result();
     }
     function get_produk_list()
     {
         $area = $this->input->post("provinsi", true);
         $this->db->distinct()->select('nama_brand');
         $this->db->group_start();
         $this->db->like('provinsi', $area);
         $this->db->group_end();
         // $this->db->group_start();
         // $this->db->like('nama_brand', $produk);
         // $this->db->group_end();
         return $this->db->get('usc_bst_sales_brand_channel')->result();
     }
     function random_color_part() {
         return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
     }
     
     function random_color() {
         return random_color_part() . random_color_part() . random_color_part();
     }
     
     function json_brand_channel()
     {
         $range = $this->input->post("range", true);
         if($range == ""){
             $range = "2018-01-01 / 2019-12-30";
         }
         $area = $this->input->post("provinsi", true);
 
         $produk = $this->input->post("produk", true);
         $lini = $this->input->post("lini", true);
         $tanggal = str_replace(" ","",$range);
         $tanggal = str_replace("/",",",$tanggal);
         $tgl =  explode(",", $tanggal);
         $from = $tgl[0];
         $to = $tgl[1];
         // $layanan = $this->input->post("layanan", true);
         $data_lini = explode(",", $lini);
         // $data_layanan = explode(",", $layanan);
         $chan = $this->get_chan();
         foreach ($chan as $kf) {
             $this->kf->select('SUM( IF( channel = "' . $kf->channel . '", revenue, 0) ) AS "' . $kf->channel . '"');
         }
         $this->kf->select('nama_brand');
         $this->kf->from('usc_bst_sales_brand_channel');
         $this->kf->where("date_format(str_to_date(tanggal, '%Y-%m-%d'), '%Y-%m-%d') >=", $from);
         $this->kf->where("date_format(str_to_date(tanggal, '%Y-%m-%d'), '%Y-%m-%d') <=", $to);
         $this->kf->group_start();
         foreach ($data_lini as $lin) {
             $this->kf->or_like('lini', $lin);
         }
         $this->kf->group_end();
     
         $this->kf->group_start();
             $this->kf->like('nama_kftd', $area);
         $this->kf->group_end();
         $this->kf->group_start();
         $this->kf->like('nama_brand', $produk);
         $this->kf->group_end();
         $this->kf->group_by('nama_brand');
         $temp=$this->kf->generate();
        //  var_dump($temp);
        //  die();

         $temps = json_decode($temp);
        //  $data = $temps->data;

        //  $temp=$this->kf->generate();
        //  $temp2 = json_decode($temp);
         $i = 0;
 
         foreach($temps->data as $data){
             $total = abs(array_sum((array)$data));
             foreach ($chan as $kf){
                 $in = $kf->channel;
                 $value = $temps->data[$i]->$in;
                 if($total == 0){
                     $rev=0;
                 }else{
                 $rev = $value/$total*100;
                 }
                 $revvl = round($rev, 3);
                 $temps->data[$i]->$in = $revvl."%";
             }
             $i++;
         }
        return json_encode($temps);
     }
 
     function json_channel_brand()
     {
         $range = $this->input->post("range", true);
         if($range == ""){
             $range = "2018-01-01 / 2019-12-30";
         }
         $area = $this->input->post("provinsi", true);
 
         $produk = $this->input->post("produk", true);
         $lini = $this->input->post("lini", true);
         $tanggal = str_replace(" ","",$range);
         $tanggal = str_replace("/",",",$tanggal);
         $tgl =  explode(",", $tanggal);
         $from = $tgl[0];
         $to = $tgl[1];
         // $layanan = $this->input->post("layanan", true);
         $data_lini = explode(",", $lini);
         // $data_layanan = explode(",", $layanan);
         $chan = $this->get_chan_brand();
 
         foreach ($chan as $kf) {
             $this->kf->select('SUM( IF( nama_brand = "' . $kf->nama_brand . '", revenue, 0) ) AS "' . $kf->nama_brand . '"');
         }
         $this->kf->select('channel');
         // $this->kf->select('(select sum(`revenue`) group by `channel`) as total_rev');
         $this->kf->from('usc_bst_sales_brand_channel');
         $this->kf->where("tanggal >=", $from);
         $this->kf->where("tanggal <=", $to);
         $this->kf->group_start();
         foreach ($data_lini as $lin) {
             $this->kf->or_like('lini', $lin);
         }
         $this->kf->group_end();
         $this->kf->group_start();
         $this->kf->like('nama_kftd', $area);
         $this->kf->group_end();
         $this->kf->group_start();
         $this->kf->like('nama_brand', $produk);
         $this->kf->group_end();
         $this->kf->group_by('channel');
         // $this->db->order_by('channel');
         $temp=$this->kf->generate();
         $temp2 = json_decode($temp);
         $i = 0;
 
         foreach($temp2->data as $data){
             $total = abs(array_sum((array)$data));
             foreach ($chan as $kf){
                 $in = $kf->nama_brand;
                 $value = $temp2->data[$i]->$in;
                 if($total == 0){
                     $rev=0;
                 }else{
                 $rev = $value/$total*100;
                 }
                 $revvl = round($rev, 3);
                 $temp2->data[$i]->$in = $revvl."%";
             }
             $i++;
         }
        return json_encode($temp2);
     }
     
 
     // END BRAND CHANNEL

    // CHANNEL AREA
    // Area-Channel START 
    function json_channel_area()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
       
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd();
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        $this->kf->select('channel');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();
        $this->kf->group_start();
            $this->kf->like('provinsi', strtoupper($provinsi));
        $this->kf->group_end();
        $this->kf->group_by('channel');
        $temp =$this->kf->generate();
        $temp2 =json_decode($temp); 
        // ?var_dump($temp2->data);
        $data =$temp2->data; 
        // ?exit();
        
         $this->db->distinct();
         $this->db->select('SUM(`revenue`) AS `revenue`,`channel`');
         $this->db->where("tanggal >=", $from)->where("tanggal <=", $to);
         $this->db->group_by('channel');
         $this->db->like('provinsi', strtoupper($provinsi));
         $this->db->group_start();
        foreach ($data_layanan as $lay){
            $this->db->or_like('layanan', $lay);
        }
        $this->db->group_end();
        $this->db->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->db->group_end();
         
         $arr_total_per_channel = $this->db->get('usc_bst_sales_area_channel')->result();
        // var_dump($arr_total_per_channel);
        $arr_channel= array();
        foreach($arr_total_per_channel as $total_per_channel){
            $arr_channel[$total_per_channel->channel]=$total_per_channel->revenue;
        }
        
        $arr_hasil= array();
        $arr_kftd=$this->area_channel_get_kftd();
        
        $arr_hasil_akhir= array();
        foreach($data as $key => $temp ){
            foreach($arr_kftd as $kftd_temp){
            $pembagi=$arr_channel[$temp->channel];
           
            $yg_dibagi=$data[$key]->{$kftd_temp->nama_kftd};
            if($pembagi==0){
                $temp5="0%";
            }else{
                $temp5 =  round(((int)$yg_dibagi/(int)$pembagi)*100,2)." %";
            }
            $arr_hasil_akhir[$key][$kftd_temp->nama_kftd]=str_replace(' ','',$temp5);;
        }
        $arr_hasil_akhir[$key]["channel"]=$temp->channel;
        
               
        }
        $std_obj = new stdClass();
        $std_obj->data=  $arr_hasil_akhir;
        
        return  json_encode($std_obj);
    }

    function json_area_channel()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd_channel();
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( channel = "' . $kf->channel . '", revenue, 0) ) AS "' . $kf->channel . '"');
        }
        $this->kf->select('nama_kftd as area');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();

        $this->kf->group_start();
            $this->kf->like('provinsi',strtoupper($provinsi));
        $this->kf->group_end();
        $this->kf->group_by('nama_kftd');
        $temp =$this->kf->generate();
        $temp2 =json_decode($temp); 
        // ?var_dump($temp2->data);
        $data =$temp2->data; 
         
         $this->db->distinct();
         $this->db->select('SUM(`revenue`) AS `revenue`,`nama_kftd`');
         $this->db->where("tanggal >=", $from)->where("tanggal <=", $to);
         $this->db->like('provinsi', strtoupper($provinsi));
         $this->db->group_start();
        foreach ($data_layanan as $lay){
            $this->db->or_like('layanan', $lay);
        }
        $this->db->group_end();
        $this->db->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->db->group_end();
         $arr_total_per_kftd = $this->db->group_by('nama_kftd')->get('usc_bst_sales_area_channel')->result();
        $arr_kftd= array();
        foreach($arr_total_per_kftd as $total_per_kftd){
            $arr_kftd[$total_per_kftd->nama_kftd]=$total_per_kftd->revenue;
        }


        $arr_hasil= array();
        $arr_channel=$this->area_channel_get_kftd_channel();
        $arr_hasil_akhir= array();
        foreach($data as $key => $temp ){
            foreach($arr_channel as $channel_temp){
                
            $pembagi=$arr_kftd[$temp->area];
            // $pembagi=$arr_kftd[$temp->nama_kftd];
            $yg_dibagi=$data[$key]->{$channel_temp->channel};
            if($pembagi==0){
                $temp5="0%";
            }else{
                $temp5 = round(((int)$yg_dibagi/(int)$pembagi)*100,2)."%";
            }
            $arr_hasil_akhir[$key][$channel_temp->channel]=str_replace(' ','',$temp5);
        }
        $arr_hasil_akhir[$key]["area"]=$temp->area;
        
               
        }
        $std_obj = new stdClass();
        $std_obj->data=  $arr_hasil_akhir;
        
        return  json_encode($std_obj);

    }

    function area_channel_get_kftd_channel()
    {
        return $this->db->distinct()->select('IF(channel = "MARCKS\'", "MARCKS", REPLACE(channel, ".", "")) as channel ')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_kftd()
    {
        return $this->db->distinct()->select('nama_kftd')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_list_lini()
    {
        return $this->db->distinct()->select('lini')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_list_layanan()
    {
        return $this->db->distinct()->select('layanan')->get('usc_bst_sales_area_channel')->result();
    }


    function area_channel_get_revenue($provinsi){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        // $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        
        if(!is_array($lini)){
            $data_lini = explode(",", $lini);
         }else{
             $data_lini = $lini;
         }
         if(!is_array($layanan)){
         $data_layanan = explode(",", $layanan);
 
         }else{
             $data_layanan = $layanan;
         }
        // $data_lini = (object) $lini;
        // $data_layanan = (object) $layanan;
        // var_dump($data_layanan);
        // $data_lini = explode(",", $lini);
        // $data_layanan = explode(",", $layanan);
        
        
        $this->db->select("provinsi, sum(revenue) as total");
        $this->db->like("provinsi", $provinsi);
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);

        // if(sizeof($lini) != 0){
            $this->db->group_start();
            foreach ($data_lini as $lin) {
                $this->db->or_like('lini', $lin);
            }
            $this->db->group_end();
        // }
       
        
        // if(sizeof($layanan) != 0){
            $this->db->group_start();
            foreach ($data_layanan as $lay){
                $this->kf->or_like('layanan', $lay);
            }
            $this->db->group_end();
        // }
       
        $this->db->group_by("provinsi");
        $data = $this->db->get('usc_bst_sales_area_channel');
        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }
    
    }
    function channel_area_get_ex_channel_area(){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
       
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd();
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        $this->kf->select('channel');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();
        $this->kf->group_start();
            $this->kf->like('provinsi', strtoupper($provinsi));
        $this->kf->group_end();
        $this->kf->group_by('channel');
        return $this->kf->generate();
       
    }
    function channel_area_get_ex_area_channel(){
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd_channel();
        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( channel = "' . $kf->channel . '", revenue, 0) ) AS "' . $kf->channel . '"');
        }
        $this->kf->select('nama_kftd as area');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);
        $this->kf->group_start();
        foreach ($data_lini as $lin) {
            $this->kf->or_like('lini', $lin);
        }
        $this->kf->group_end();
        
        $this->kf->group_start();
        foreach ($data_layanan as $lay){
            $this->kf->or_like('layanan', $lay);
        }
        $this->kf->group_end();

        $this->kf->group_start();
            $this->kf->like('provinsi',strtoupper($provinsi));
        $this->kf->group_end();
        $this->kf->group_by('nama_kftd');
        return $this->kf->generate();
    }
    
    //BEGIN BOOSTING SALES - OUTLET OTR
    function get_list_kftd($table){
        return $this->db->distinct()->select('nama_kftd')->get($table)->result();
    }

    function get_channels($table){
         $periode = $this->input->post("periode", true);
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $brand = $this->input->post("brand", true);
        $lini = $this->input->post("lini", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-30";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');

        $this->db->select("count(distinct(nama_customer)) as jumlah_outlet, nama_brand, year(tanggal) as tahun");
            if (!empty($brand)) {
                $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
                $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));
                
                $this->db->group_start();
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);
                $this->db->or_where("tanggal >=", $last_from);
                $this->db->where("tanggal <=", $last_to);
                $this->db->group_end();
            } else {
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);    
            }
        if (!empty($brand)){
            $this->db->group_by(array("nama_brand","year(tanggal)"));
        } else {
            $this->db->group_by(array("nama_brand"));
        }
        
        $this->db->order_by("jumlah_outlet","DESC");    
        
        if(!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_like("nama_kftd",$k);
            }
            $this->db->group_end();
        }
        
        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            $this->db->or_like("lini",$lini);
            $this->db->group_end();
        }
        

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_like("nama_brand", $b);
            }
            $this->db->group_end();
        } else {
            $this->db->limit(20);    
        }
        
        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();
        return $this->db->distinct()->select("channel, CASE WHEN channel != 'Marketing' THEN 0 ELSE 1 END AS ordering")->order_by('ordering')->order_by('channel')->get($table)->result();
     }

    function get_nama_brand($table){
        $nama_brand = [];
        $brands = $this->db->distinct()->select('nama_brand')->order_by('nama_brand', 'ASC')->get($table)->result();
        foreach ($brands as $b){
            $nama_brand[] = "'" . str_replace("'","-",$b->nama_brand) . "'";
        }
        return implode(',',$nama_brand);
    }

    function get_sum_all_outlet(){
        $sum_outlets = $this->db->select('SUM(jumlah_customer) as jumlah_outlet, nama_brand')->get('usc_bst_sales_outlet_channel')->group_by('nama_brand')->result();
    }

    function get_nama_produk($nama_brand){
        return $this->db->select('nama_produk')
        ->where('nama_brand', $nama_brand)
        ->get('usc_bst_sales_outlet_channel')->result();
    }

    //usc_bst_sales_outlet_channel table
    //jumlah_customer
    //tanggal
    //nama_kftd
    //channel
    //lini
    //nama_brand
    //nama_produk

    function outlet_otr($start = '', $end='', $area='', $channel=''){
        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_brand, year(tanggal) as tahun");
        $this->db->group_by(array("nama_brand","year(tanggal)"));
        if ( empty($start) && empty($end) ){
            $this->db->where("tanggal >=", '2018-01-01');
            $this->db->where("tanggal <=", '2019-12-31');    
        } else {
            $this->db->where("tanggal >=", $start);
            $this->db->where("tanggal <=", $end);
        }
        if (empty($area) && empty($channel)) {
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (empty($area)){
            $this->db->where_in('channel', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (empty($channel)){
            $this->db->where_in('nama_kftd', $area);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else {
            $this->db->where_in('nama_kftd', $area);
            $this->db->where_in('channel', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        }
        return $data;
    }

    function json_outlet_otr(){
        $periode = $this->input->post("periode", true);
        // $provinsi = $this->input->post("provinsi", true);
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $brand = $this->input->post("brand", true);
        $lini = $this->input->post("lini", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-30";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');

        $this->db->select("count(distinct(nama_customer)) as jumlah_outlet, nama_brand, year(tanggal) as tahun");
        // if($dateFrom->format("Y") == $dateTo->format("Y"))
        // {
            if (!empty($brand)) {
                // $last_from = strtotime("last year", strtotime($from));
                // $last_to = strtotime("last year", strtotime($to));
                $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
                $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));
                
                $this->db->group_start();
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);
                $this->db->or_where("tanggal >=", $last_from);
                $this->db->where("tanggal <=", $last_to);
                $this->db->group_end();

                // $compare = date('Y', $last_from);
                // foreach($tahun_otr as $key=>$value) {
                //     if ($value->tahun == $compare+1)
                //     {
                //         if(isset($tahun_otr[$key+1])){
                //             unset($tahun_otr[$key+1]);
                //         }
                //     }
                // }
            } else {
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);    
            }

        // } else {
        //     $this->db->where("tanggal >=", $from);
        //     $this->db->where("tanggal <=", $to);
        // }

        if (!empty($brand)){
            $this->db->group_by(array("nama_brand","year(tanggal)"));
        } else {
            $this->db->group_by(array("nama_brand"));
        }
        
        $this->db->order_by("jumlah_outlet","DESC");    
        
        // if(!empty($provinsi)) {
        //     $this->db->group_start();
        //     foreach ($provinsi as $prov) {
        //         $this->db->or_like("provinsi",$prov);
        //     }
        //     $this->db->group_end();
        // }

        if(!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_like("nama_kftd",$k);
            }
            $this->db->group_end();
        }
        
        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            $this->db->or_like("lini",$lini);
            $this->db->group_end();
        }
        

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_like("nama_brand", $b);
            }
            $this->db->group_end();
        } else {
            $this->db->limit(20);    
        }
        
        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();

		$json_data_otr = array();
        $nama_brand = $this->Boost_model->get_nama_brand('usc_bst_sales_outlet_channel');

        if (empty($brand)) {
            $json_data_otr[0]['name'] = 'Top 20 Brand';
            $jml_outlet = array();
            $json_nama_brand = array();

            foreach ($data as $otr){
                // if ($otr->tahun == $value->tahun){
                    $json_nama_brand[] = $otr->nama_brand;
                    $jml_outlet[] = (int)$otr->jumlah_outlet;
                // }
            }
            $json_data_otr[0]['data'] = $jml_outlet;
            $json_data_otr[0]['color'] = "#4285F4";
        } else {
            foreach ($tahun_otr as $key=>$value) {
                $json_data_otr[$key]['name'] = $value->tahun;
                $jml_outlet = array();
                $json_nama_brand = array();
    
                foreach ($data as $otr){
                    if ($otr->tahun == $value->tahun){
                        $json_nama_brand[] = $otr->nama_brand;
                        $jml_outlet[] = (int)$otr->jumlah_outlet;
                    }
                }
                $json_data_otr[$key]['data'] = $jml_outlet;
                if ($key == 0) {
                    $json_data_otr[$key]['color'] =  "#A3D4EF";
                } else {
                    $json_data_otr[$key]['color'] = "#4285F4";
                }
            }
        }
        
        $json = array(
            'raw' => $data,
            'nama_brand' => $json_nama_brand,
            'data' => $json_data_otr
        );

        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    function json_get_list_product(){
        $periode = $this->input->post("periode", true);
        // $provinsi = $this->input->post("provinsi", true);
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $nama_brand = $this->input->post("nama_brand", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-30";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');

        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_produk, year(tanggal) as tahun");
        
        // if($dateFrom->format("Y") == $dateTo->format("Y"))
        // {
            // $last_from = strtotime("last year", strtotime($from));
            // $last_to = strtotime("last year", strtotime($to));

            $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
            $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));
            
            $this->db->group_start();
            $this->db->where("tanggal >=", $from);
            $this->db->where("tanggal <=", $to);
            $this->db->or_where("tanggal >=", $last_from);
            $this->db->where("tanggal <=", $last_to);
            $this->db->group_end();
           
            // $compare = date('Y', $last_from);
            // foreach($tahun_otr as $key=>$value) {
            //     if ($value->tahun == $compare+1)
            //     {
            //         if(isset($tahun_otr[$key+1])){
            //             unset($tahun_otr[$key+1]);
            //         }
            //     }
            // }

        // } else {
        //     $this->db->where("tanggal >=", $from);
        //     $this->db->where("tanggal <=", $to);
        // }

        $this->db->group_by(array("nama_produk","year(tanggal)"));
        $this->db->like('nama_brand', $nama_brand);    
        $this->db->order_by("jumlah_outlet","DESC");
        
        // if(!empty($provinsi)) {
        //     $this->db->group_start();
        //     foreach ($provinsi as $prov) {
        //         $this->db->or_like("provinsi",$prov);
        //     }
        //     $this->db->group_end();
        // }

        if(!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_like("nama_kftd",$k);
            }
            $this->db->group_end();
        }
        
        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }
        
        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();

        // $this->db->select('SUM(IF(YEAR(`tanggal`)="2018", `jumlah_outlet`,0)) as "lastyear", SUM(IF(YEAR(`tanggal`)="2019", `jumlah_outlet`,0)) as "nowyear", `nama_produk`');
		// $this->db->group_by('nama_produk');
        // $data_tahunan = $this->db->get('usc_bst_sales_outlet_channel')->result();
        /*$this->db->select('((SELECT SUM(IF(YEAR(`tanggal`)="2019", `jumlah_outlet`,0)))-(SELECT SUM(IF(YEAR(`tanggal`)="2018", `jumlah_outlet`,0))))/(SELECT SUM(IF(YEAR(`tanggal`)="2018", `jumlah_outlet`,0)))*100 AS "growth", `nama_produk`');
		$this->db->group_by('nama_produk');
        $growth=$this->db->get('`usc_bst_sales_outlet_channel`')->result();*/
        
        $cate = $this->Boost_model->get_list_product($nama_brand);

        // foreach($cate as $key=>$value){
        //     $json_data_growth[$key]['name'] = $value->nama_produk;
        //     $growth=array();
        //     foreach($data_tahunan as $gr){
        //         if($gr->nama_produk == $value->nama_produk){
        //         $growth[]=((int)$gr->nowyear-(int)$gr->lastyear)/(int)$gr->lastyear;
        //         }
        //     }
        //     $json_data_growth['data'] = $growth;
        //     $json_data_growth['color'] = '#' . random_color();
        //     $json_data_growth['type'] = "spline";
        // }
        /*foreach($cate as $key=>$value){
            // $json_data_growth[$key]['name'] = $value->nama_produk;
            $growth_data=array(); 
         foreach($growth as $gr){
            //  $preg = array_slice($gr->nama_produk, 0, 3);
             if(substr($gr->nama_produk, 0, 7) == substr($value->nama_produk, 0, 7)) {
                $growth_data[]=(int)$gr->growth;
             }
         }
         $json_data_growth[$key]['data']= $growth_data;
         $json_data_growth[$key]['color']= '#' . random_color();
         $json_data_growth[$key]['type'] = "spline";
        $json_data_growth[$key]['yAxis'] = 1;
            $json_data_growth[$key]['name'] = 'Growth';
        $json_data_growth[$key]['showInLegend'] = false;

         
        }*/
        $d = array();
        foreach ($tahun_otr as $key=>$value) {
            $json_data_otr[$key]['name'] = $value->tahun;
            $jml_outlet = array();
			foreach ($data as $k => $otr){
				if ($otr->tahun == $value->tahun) {
				    $np = str_replace(' ', '_', $otr->nama_produk);
                    $d[$np][$otr->tahun] = (int) $otr->jumlah_outlet;
				    $jml_outlet[] = (int) $otr->jumlah_outlet;
				}
			}
			$json_data_otr[$key]['data'] = $jml_outlet;
            $json_data_otr[$key]['color'] = '#' . random_color();
            $json_data_otr[$key]['type'] = "column";
            $json_data_otr[$key]['yAxis'] = 0;
        }

        //print_r($d);
        $growth_data = array();
        foreach($d as $key => $growth_tahun) {
            if(empty(array_values($growth_tahun)[0]))
                $i0 = 0;
            else
                $i0 = array_values($growth_tahun)[0];

            if(empty(array_values($growth_tahun)[1]))
                $i1 = 0;
            else
                $i1 = array_values($growth_tahun)[1];

            /*if($i1 - $i0 == 0)
                $grfix = (($i1 - $i0) / $i1)*100;
            else
                $grfix = 0;*/

            $growth_data[] = (int) $i1 - (int) $i0;
        }

        $key++;
        $json_data_growth[$key]['data']= $growth_data;
        $json_data_growth[$key]['color']= '#' . random_color();
        $json_data_growth[$key]['type'] = "spline";
        $json_data_growth[$key]['yAxis'] = 1;
        $json_data_growth[$key]['name'] = 'Growth';
        $json_data_growth[$key]['showInLegend'] = false;
        
        $json_data2 =[];
        foreach($json_data_otr as $otr){
            $json_data2[]=$otr;
        }
        foreach($json_data_growth as $gr){
                
            $json_data2[]=$gr;

        }
        
		$json_data = array(
            'raw' => $data,
            'data' => $json_data2,
            // 'data2'=>$preg,
			'categories' => $this->Boost_model->get_list_product($nama_brand)
		);
		echo json_encode($json_data);
    }

    function json_get_growth_product(){
        $periode = $this->input->post("periode", true);
        $provinsi = $this->input->post("provinsi", true);
        $channel = $this->input->post("channel", true);
        $nama_produk = $this->input->post("nama_produk", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-30";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');
    }

    function json_outlet_product(){
        $periode = $this->input->post("periode", true);
        $cabang = $this->input->post("cabang", true);
        $channel = $this->input->post("channel", true);
        $brand = $this->input->post("brand", true);
        $lini = $this->input->post("lini", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-31";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_transaksi_area');

        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_brand, year(tanggal) as tahun");

        if($dateFrom->format("Y") == $dateTo->format("Y"))
        {
            if (!empty($brand)) {
                $last_from = strtotime("last year", strtotime($from));
                $last_to = strtotime("last year", strtotime($to));
                
                $this->db->group_start();
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);
                $this->db->or_where("tanggal >=", $last_from);
                $this->db->where("tanggal <=", $last_to);
                $this->db->group_end();

                $compare = date('Y', $last_from);
                foreach($tahun_otr as $key=>$value) {
                    if ($value->tahun == $compare+1)
                    {
                        if(isset($tahun_otr[$key+1])){
                            unset($tahun_otr[$key+1]);
                        }
                    }
                }
            } else {
                $this->db->where("tanggal >=", $from);
                $this->db->where("tanggal <=", $to);    
            }
            
        } else {
            $this->db->where("tanggal >=", $from);
            $this->db->where("tanggal <=", $to);
        }

        if (!empty($brand)){
            $this->db->group_by(array("nama_brand","year(tanggal)"));
        } else {
            $this->db->group_by(array("nama_brand"));
        }
        
        $this->db->order_by("jumlah_outlet","DESC");

        if (!empty($cabang)) {
            $this->db->group_start();
            foreach ($cabang as $kftd) {
                $this->db->or_like("nama_kftd",$kftd);
            }
            $this->db->group_end();    
        }
        
        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            foreach ($lini as $l) {
                $this->db->or_like("lini", $l);
            }
            $this->db->group_end();
        }

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_like("nama_brand", $b);
            }
            $this->db->group_end();
        } else {
            $this->db->limit(20);    
        }

        $data = $this->db->get('usc_bst_sales_transaksi_area')->result();

        $json_data_otr = array();
		$nama_brand = $this->Boost_model->get_nama_brand('usc_bst_sales_transaksi_area');
        $json_nama_brand = array();

        foreach ($tahun_otr as $key=>$value) {
            if (empty($brand)) {
                $json_data_otr[$key]['name'] = 'Top 20 Brand';
                $jml_outlet = array();
                foreach ($data as $otr){
                    if ($otr->tahun == $value->tahun){
                        $json_nama_brand[] = $otr->nama_brand;
                        $jml_outlet[] = (int)$otr->jumlah_outlet;
                    }
                }
                $json_data_otr[$key]['data'] = $jml_outlet;
                $json_data_otr[$key]['color'] = "#4285F4";
            break;
            } else {
                $json_data_otr[$key]['name'] = $value->tahun;
                $jml_outlet = array();
                foreach ($data as $otr){
                    if ($otr->tahun == $value->tahun){
                        $json_nama_brand[] = $otr->nama_brand;
                        $jml_outlet[] = (int)$otr->jumlah_outlet;
                    }
                }
                $json_data_otr[$key]['data'] = $jml_outlet;
                if ($key == 0) {
                    $json_data_otr[$key]['color'] =  "#4A72B8";
                } else {
                    $json_data_otr[$key]['color'] = "#D8DADD";
                }
            }
        }

        $json = array(
            'raw' => $data,
            'nama_brand' => $json_nama_brand,
            'data' => $json_data_otr
        );

        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    function json_outlet_trend(){
        $periode = $this->input->post("periode", true);
        // $provinsi = $this->input->post("provinsi", true);
        $brand = $this->input->post("brand",true);
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);

        if($periode == ""){
            $periode = "2018-01-01 / 2019-12-30";
        }
        $tanggal = str_replace(" ","",$periode);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_outlet_channel');

        $this->db->select("COUNT(DISTINCT(nama_customer)) as jumlah_outlet, month(tanggal) as bulan, year(tanggal) as tahun");


        // if($dateFrom->format("Y") == $dateTo->format("Y"))
        // {
            // $last_from = strtotime("last year", strtotime($from));
            // $last_to = strtotime("last year", strtotime($to));

            $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
            $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));
            
            $this->db->group_start();
            $this->db->where("tanggal >=", $from);
            $this->db->where("tanggal <=", $to);
            $this->db->or_where("tanggal >=", $last_from);
            $this->db->where("tanggal <=", $last_to);
            $this->db->group_end();

            // $compare = date('Y', $last_from);
            // var_dump($compare);
            // die();
            // foreach($tahun_otr as $key=>$value) {
            //     if ($value->tahun == $compare+1)
            //     {
            //         if(isset($tahun_otr[$key+1])){
            //             unset($tahun_otr[$key+1]);
            //         }
            //     }
            // }

        // } else {
        //     $this->db->where("tanggal >=", $from);
        //     $this->db->where("tanggal <=", $to);
        // }
        $this->db->group_by(array("month(tanggal)","year(tanggal)"));    
        
        // if (!empty($provinsi)) {
        //     $this->db->group_start();
        //     foreach ($provinsi as $prov) {
        //         $this->db->or_like("provinsi",$prov);
        //     }
        //     $this->db->group_end();
        // }

        if (!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_like("nama_kftd",$k);
            }
            $this->db->group_end();
        }
        
        $this->db->group_start();
        $this->db->or_like("lini",$lini);
        $this->db->group_end();

        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_like("nama_brand", $b);
            }
            $this->db->group_end();
        }
        
        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();
        // var_dump($data);
        // die();
        
        // get diagram batang realisasi 
        $this->db->select("SUM(revenue) as revenue, month(tanggal) as bulan, year(tanggal) as tahun");

        if (!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_like("nama_kftd",$k);
            }
            $this->db->group_end();
        }
        
        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_like("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            $this->db->or_like("lini",$lini);
            $this->db->group_end();    
        }

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_like("nama_brand", $b);
            }
            $this->db->group_end();
        }

        // if($dateFrom->format("Y") == $dateTo->format("Y"))
        // {
            // $last_from = strtotime("last year", strtotime($from));
            // $last_to = strtotime("last year", strtotime($to));
            $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
            $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));
            // var_dump($last_from);
            // die();
            
            $this->db->group_start();
            $this->db->where("tanggal >=", $from);
            $this->db->where("tanggal <=", $to);
            $this->db->or_where("tanggal >=", $last_from);
            $this->db->where("tanggal <=", $last_to);
            $this->db->group_end();
        // } else {
        //     $this->db->where("tanggal >=", $from);
        //     $this->db->where("tanggal <=", $to);
        // }

        $this->db->group_by(array("month(tanggal)","year(tanggal)"));    

        $batang = $this->db->get('usc_bst_sales_area_brand')->result();
        //end get diagram batang realisasi
        // var_dump($batang);
        // die();

        $json_trend_otr = array();
        $json_realisasi_otr = array();
        $json_bulan = array();
        $json = array();
       
        foreach ($tahun_otr as $key=>$value) {
            $json_trend_otr[$key]['name'] = 'OTR tahun '. $value->tahun;

            if ($key == 0) {
                $json_trend_otr[$key]['color'] = '#F04040';
            } else {
                $json_trend_otr[$key]['color'] = '#1F90FA';
            }  
			$trend_otr = array();
			foreach ($data as $p=>$otr){
				if ($otr->tahun == $value->tahun){
                    $trend_otr[$p]['y'] = (int)$otr->jumlah_outlet;
                    if (in_array(date('F', mktime(0, 0, 0, $otr->bulan, 10)),$json_bulan)) {
                        continue;
                    } else {
                        $json_bulan[] = date('F', mktime(0, 0, 0, $otr->bulan, 10));
                    }
				}
			}
            $batang_otr = array();
            foreach ($batang as $k=>$b) {
                if ($b->tahun == $value->tahun) {
                    $trend_otr[$k]['value'] =  $b->revenue;
                }
            }
            $json_trend_otr[$key]['data'] = array_values($trend_otr);
            $json_trend_otr[$key]['yAxis'] = 0;
            $json_trend_otr[$key]['showInLegend'] = false;
        }

        foreach ($tahun_otr as $key=>$value) {
            $json_realisasi_otr[$key]['name'] = 'Value tahun ' . $value->tahun;
            $value_otr = array();
            foreach ($batang as $k=>$val) {
                if ($val->tahun == $value->tahun){
                    $value_otr[] = $val->revenue;
                }
            }
            $json_realisasi_otr[$key]['data'] = $value_otr;
            $json_realisasi_otr[$key]['type'] = 'column';
            $json_realisasi_otr[$key]['yAxis'] = 1;
            $json_realisasi_otr[$key]['showInLegend'] = false;

        }
        
        foreach($json_trend_otr as $otr){
            $json['data'][] = $otr;
        }
        foreach($json_realisasi_otr as $val){
            $json['data'][] = $val;
        }
        $json['bulan'] = $json_bulan;

        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    function get_product($nama_brand){
        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_produk, year(tanggal) as tahun");
        $this->db->group_by(array("nama_produk","year(tanggal)"));
        $this->db->like('nama_brand',$nama_brand);
        $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        return $data;
    }

    function get_list_product($nama_brand){
        return $this->db->distinct()->select('nama_produk')->like('nama_brand',$nama_brand)->order_by('nama_produk', 'asc')->get('usc_bst_sales_transaksi_area')->result();
    }

    function get_tahun_otr($table){
        return $this->db->distinct()->select("year(tanggal) as tahun")->get($table)->result();
    }

    function get_provinsi($table){
        return $this->db->distinct()->select("provinsi")->get($table)->result();
    }

    function get_lini_otr($table){
        return $this->db->distinct()->select("lini")->get($table)->result();
    }

    function get_brand_otr($table){
        return $this->db->distinct()->select('nama_brand')->get($table)->result();
    }

    // usc_bst_sales_trend_outlet table
    // jumlah_customer
    // tanggal
    // nama_kftd
    // channel
    // lini

    function trend_outlet($area=null, $channel=null, $year ){
        $trend_otr = array();
        $this->db->reconnect();
        $sql = "SELECT COUNT(DISTINCT(nama_customer)) as jumlah_outlet, MONTH(tanggal) as bulan FROM usc_bst_sales_outlet_channel WHERE YEAR(tanggal)=".$year;
        if(!empty($area)) $sql.="AND WHERE nama_kftd IN " . implode(',',$area);
        if(!empty($channel)) $sql.="AND WHERE channel IN " . implode(',',$channel);
        $sql.=" GROUP BY MONTH(tanggal)";
        
        $query = $this->db->query($sql);
        
        foreach($query->result_array() as $trend){
            $trend_otr[] = $trend['jumlah_outlet'];
        }
        
        return $trend_otr;

    }

    function get_brand_outlet($table)
    {
        return $this->db->distinct()->select('nama_brand')->get($table)->result();
    }

    function outlet_product($start = '', $end='', $area='', $channel=''){
        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_brand, year(tanggal) as tahun");
        $this->db->group_by(array("nama_brand", "year(tanggal)"));

        if (empty($area) && empty($channel)) {
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (!empty($area)){
            $this->db->where_in('provinsi', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (!empty($channel)){
            $this->db->where_in('channel', $area);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else {
            $this->db->where_in('provinsi', $area);
            $this->db->where_in('channel', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        }
        return $data;
    }

    function json_outlet_brand(){
        $area = $this->input->post("area_table", true);
        $chan = $this->input->post("channel_table", true);
        $data_area = explode(",", $area);
        $data_channel = explode(",",$chan);
        $channels = $this->get_channels('usc_bst_sales_outlet_channel');
        $tahun = [date("Y",strtotime("-1 year")), date("Y")];
        foreach ( $channels as $ch ) {
            foreach ($tahun as $key=>$value) {
                $this->kf->select('CASE WHEN COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $value .', nama_customer, 0) ) ) = 1 THEN 0 ELSE COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $value .', nama_customer, 0) ) ) - 1  END AS "'. $ch->channel . '-' . $value . '"');
            }
        }
        $this->kf->select("nama_brand");
        $this->kf->from('usc_bst_sales_outlet_channel');
        $this->kf->where("YEAR(tanggal) BETWEEN YEAR(CURDATE())-1 AND YEAR(CURDATE())");
        // var_dump($area);
        // die();

        if (!empty($area)) {
            $this->kf->group_start();
            foreach ($data_area as $a) {
            // foreach($area as $a){
                // $this->kf->or_like('nama_kftd', $a);
                $this->kf->or_where('nama_kftd', $a);
            }
            $this->kf->group_end();                                                                                                                           
        }

        // var_dump($data_channel);
        // die();
        if (!empty($chan)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
            // foreach ($chan as $c) {
                // $this->kf->or_like('channel', $c);
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();    
        }
        $this->kf->group_by("nama_brand");


        return $this->kf->generate();
    }

    function json_outlet_area(){
        $brand = $this->input->post("brand_table",true);
        $area = $this->input->post("area_table", true);
        $chan = $this->input->post("channel_table", true);
        $data_area = explode(",", $area);
        $data_channel = explode(",",$chan);

        $channels = $this->get_channels('usc_bst_sales_outlet_channel');
        $tahun = [date("Y",strtotime("-1 year")), date("Y")];
        foreach ( $channels as $ch ) {
            foreach ($tahun as $key=>$value) {
            // $this->kf->select('SUM(IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $value .', jumlah_outlet, 0) ) AS "'. $ch->channel . '-' . $value . '"');
            $this->kf->select('CASE WHEN COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $value .', nama_customer, 0) ) ) = 1 THEN 0 ELSE COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $value .', nama_customer, 0) ) ) - 1  END AS "'. $ch->channel . '-' . $value . '"');
            }
        }
        $this->kf->select("nama_kftd");
        $this->kf->from('usc_bst_sales_outlet_channel');
        $this->kf->where("YEAR(tanggal) BETWEEN YEAR(CURDATE())-1 AND YEAR(CURDATE())");

        if (!empty($area)) {
            $this->kf->group_start();

            foreach ($data_area as $a) {
                $this->kf->or_where('nama_kftd', $a);
            }
            $this->kf->group_end();    
        }
        
        if (!empty($chan)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();

        }

        $this->kf->group_by("nama_kftd");        
        return $this->kf->generate();
    }

    function json_outlet_transaction(){
        $lini = $this->input->post("lini_table", true);
        $channel = $this->input->post("channel_table_outlet", true);
        $periode = $this->input->post("periode_table_outlet", true);

        $brand = $this->input->post("brand_table_outlet",true);
        $kftd = $this->input->post("area_table_outlet", true);

        $data_lini = explode(",", $lini);
        $data_channel = explode(",",$channel);
        $data_brand = explode(",",$brand);

        // if($periode == ""){
        //     $periode = date('Y');
        // }
        // $tanggal = str_replace("+","",$periode);
		// $tanggal = str_replace("/",",",$tanggal);
		// $tgl =  explode(",", $tanggal);
        // $from = $tgl[0];
        // $to = $tgl[1];
        // $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        // $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        // $tahun_otr = $this->Boost_model->get_tahun_otr('usc_bst_sales_brand_channel');


        if (empty($periode)) {
            $periode = date('Y');
        }

        $channels = $this->get_channels('usc_bst_sales_outlet_channel');
        $tahun = [date("Y",strtotime("-1 year")), date("Y")];
        $isi = array('transaksi','value');
        $bulan = $this->db->distinct()->select('month(tanggal) as bulan')->where('year(tanggal)', $periode)->get('usc_bst_sales_brand_channel')->result();
        

        for($m=1; $m<=12; $m++) {
           
            $this->kf->select('SUM(IF( month(tanggal) = "' . $m . '" AND YEAR(tanggal) = ' . $periode . ', quantity, 0) ) AS "'. date('M', mktime(0,0,0,$m, 10 )). '-quantity",SUM(IF( month(tanggal) = "' . $m . '" AND YEAR(tanggal) = ' . $periode . ', revenue, 0) ) AS "'. date('M', mktime(0,0,0,$m, 10 )). '-revenue"');
            
        }

        $this->kf->select("nama_brand");
        $this->kf->from('usc_bst_sales_brand_channel');
        $this->kf->where('year(tanggal)', $periode);
        $this->kf->group_by("nama_brand");
        
        $this->kf->group_start();
        foreach ($data_brand as $b) {
            $this->kf->or_like('nama_brand', $b);
        }
        $this->kf->group_end();

        $this->kf->group_start();
        foreach ($data_lini as $a) {
            $this->kf->or_like('lini', $a);
        }
        $this->kf->group_end();

        $this->kf->group_start();
        foreach ($data_channel as $c) {
            $this->kf->or_like('channel', $c);
        }
        $this->kf->group_end();

        return $this->kf->generate();
    }

    function get_last_update() {
        return $this->db->select('tanggal')->order_by('tanggal','DESC')->limit(1)->get('usc_bst_sales_outlet_channel')->result();
    }

    
    //END OF BOOSTING SALES - OUTLET OTR
}
