<?php
class Crm_b2b_model extends CI_Model {
	
	private $table = 'crm_b2b_cust_trans';
	private $id    = 'cct_id';

	public function __construct()
	{
	    $this->load->database();
	}
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{
        // ====================== initial code ===========================
        // return $this->db->query("select 
        // cct_cust_code, bcc_cust_name, max(bcc_cabang) as bcc_cabang, max(bcc_type_channel) as bcc_type_channel, max(layanan_name) as layanan_name, sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq, brand
        // from ".$this->table." 
        // $param[where] 
        // group by cct_cust_code, bcc_cust_name
        // $param[order] $param[limit]");
        // ===============================================================

        return $this->db->query("SELECT /* SQL_NO_CACHE */
        cct_cust_code, bcc_cust_name, bcc_cabang, bcc_type_channel, 
        layanan_name, sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq, brand
        from b2b_relation_cust_segment 
        $param[where] 
        group by cct_cust_code, bcc_cust_name, brand
        $param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	} 
	
	function dtcount($param)
	{
        return  $this->db->query("select count(*) total from (select * from ".$this->table." $param[where] group by cct_cust_code, bcc_cust_name) x"); 

		// return $this->db->count_all($this->table);
	}

    function dtquery_detail_trans($param)
    {
        // ========== initial query ===========
        // return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history $param[where] $param[order] $param[limit]");
        // ====================================

        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history_v1 $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_trans()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_trans()
    {
        // ======== initial query ==========
        // return $this->db->count_all('crm_b2b_cust_total_history');
        // =================================

        return $this->db->count_all('crm_b2b_cust_total_history_v1');
    }

    function dtquery_detail_retur($param)
    {
        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_retur $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_retur()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_retur()
    {
        return $this->db->count_all('crm_b2b_cust_total_retur');
    }

    //----
    function dtquery_detail_reward($param)
    {
        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_reward $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_reward()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_reward()
    {
        return $this->db->count_all('crm_b2b_reward');
    }
	//---------------------------------------------------------------------------------------------------------------------

    public function b2b_detail($id)
    {
        $query = $this->db->query(" SELECT * FROM ".$this->table." WHERE cct_cust_code = '$id'");
        return $query->row();
    }

    // public function b2b_level($id)
    // {
    //     return $this->db->query("SELECT distinct level, month FROM crm_b2c_end_lini_level WHERE customer = '$id' and year = '$year' order by month desc");
    // }

    function getArea()
    {
        return $this->db->query("SELECT bcc_cabang from ".$this->table." group by bcc_cabang order by bcc_cabang asc");
    }

    function getChannel()
    {
        return $this->db->query("SELECT bcc_type_channel from ".$this->table." group by bcc_type_channel order by bcc_type_channel asc");
    }

    function getLayanan()
    {
        return $this->db->query("SELECT cct_layanan from ".$this->table." where cct_layanan is not NULL  group by cct_layanan order by cct_layanan");
    }

    function getLini()
    {
        return $this->db->query("SELECT cct_lini from ".$this->table." group by cct_lini order by cct_lini");
    }
    // blablabla
    function getBrand()
    {

        return $this->db->query("SELECT brand from ".$this->table." group by brand order by brand");

    }
    // blablabla

    function getLevelChart($where)
    {
        /*echo "SELECT bcc_cust_name, sum(rev)rev, sum(freq)freq
            from crm_b2b_cust_trans 
            $where 
            group by bcc_cust_name
            ORDER BY sum(rev)*sum(freq) DESC";*/

        // =========================== initial code =======================================
        // return $this->db->query("SELECT bcc_cust_name, sum(rev)rev, sum(freq)freq 
        //     from crm_b2b_cust_trans 
        //     $where 
        //     group by bcc_cust_name
        //     ORDER BY sum(rev)*sum(freq) DESC");
        // ============================================================================

        return $this->db->query("SELECT /* SQL_NO_CACHE */ bcc_cust_name, sum(rev)rev, sum(freq)freq 
        FROM b2b_scatter_plot
        $where
        group by bcc_cust_name");
    }

    function getLevelChartLini($where)
    {
        return $this->db->query("SELECT cct_lini, sum(rev)rev, sum(freq)freq 
            from crm_b2b_cust_trans 
            $where 
            group by cct_lini");
    }

    function getLevelPie($where)
    {
        // ============== initial query ============
        return $this->db->query("SELECT level, count(*) as jml from crm_b2b_cust_trans $where group by level");
        // =========================================
    }

    public function b2c_level($id, $month, $year)
    {  
        return $this->db->query(" SELECT * FROM b2c_crm_end_user_trans WHERE customer = '$id' and month = '$month' and year = '$year'");
    }

	
	function getYear()
	{
			return $this->db->query("SELECT ccl_year from b2b_cust_lini  
					group by ccl_year order by ccl_year");
	}
	
	function getProduk()
	{
			return $this->db->query("SELECT cct_brand from b2b_crm_cust_trans  
					group by cct_brand order by cct_brand");
	}


	
	function getLevels($level,$lini,$start,$end,$area,$channel,$layanan)
	{   
		$where = "where cct_faktur_date BETWEEN '".$start[2]."-".$start[1]."-".$start[0]."' and '".$end[2]."-".$end[1]."-".$end[0]."'";
		if($level!="") $where .=" and lower(cct_level)='$level'"; 
		if($lini!="0") $where .=" and cct_lini	='$lini'"; 
		if($area!="0") $where .=" and bcc_city	='$area'"; 
		if($channel!="0") $where .=" and bcc_type_channel	='$channel'";  
		if($layanan!="0") $where .=" and cct_layanan	='$layanan'";  
 
		return $this->db->query("select count(*) total from (SELECT distinct bcc_type_channel from b2b_crm_cust_trans left join b2b_crm_cust on bcc_cust_code=cct_cust_code $where limit 0,100)a");
	}

	function get_level_rule($lini, $layanan, $month_start, $month_end, $year, $model)
    {
        // $level_rule = $this->db->query("SELECT rule_level, sum(rule_value_min) as rule_value_min, sum(rule_value_max) as rule_value_max,
        //     sum(rule_freq_min) as rule_freq_min, sum(rule_freq_max) as rule_freq_max
        //     FROM crm_b2b_level_rule_range
        //     WHERE lower(rule_layanan) = '".strtolower($layanan)."' and lower(rule_lini) = '".strtolower($lini)."' and 
        //     (rule_month BETWEEN '$month_start' and '$month_end') and rule_year = '$year' and rule_model = '$model'
        //     group by rule_layanan, rule_lini, rule_level")->result();

        $level_rule = $this->db->query("SELECT /* SQL_NO_CACHE */ a.rule_id, a.rule_level,a.rule_month,a.rule_year, a.rule_value_min,a.rule_value_max,
        a.rule_freq_min, a.rule_freq_max
        FROM crm_b2b_level_rule_range a        
        WHERE a.rule_id in (select max(rule_id) rule_id from crm_b2b_level_rule_range where rule_model = '$model' group by rule_level  )")->result();

        $ra = array(
            'gold' => array('rev_min' => 0, 'rev_max' => 0, 'freq_min' => 0, 'freq_max' => 0),
            'silver' => array('rev_min' => 0, 'rev_max' => 0, 'freq_min' => 0, 'freq_max' => 0),
            'bronze' => array('rev_min' => 0, 'rev_max' => 0, 'freq_min' => 0, 'freq_max' => 0),
            'regular' => array('rev_min' => 0, 'rev_max' => 0, 'freq_min' => 0, 'freq_max' => 0)
        );

        if(!empty($level_rule))
        {
            foreach($level_rule as $rule)
            {
                $ra[strtolower($rule->rule_level)] = array(
                    'rev_min' => $rule->rule_value_min,
                    'rev_max' => $rule->rule_value_max,
                    'freq_min' => $rule->rule_freq_min,
                    'freq_max' => $rule->rule_freq_max,
                );
            }
        }

        return $ra;
    }

    function get_prediction($id, $year,$start,$end)
    {
        // ======================= initial code ==========================
        // $q = "SELECT cct_month, sum(rev)rev, sum(freq)freq 
        //     FROM crm_b2b_cust_trans 
        //     WHERE cct_cust_code = '$id' and cct_year = '$year'
        //     and CAST(cct_month AS int) BETWEEN $start and $end
        //     GROUP BY cct_month
        //     ORDER BY cct_month ASC";
        // ===============================================================

        $q = "SELECT /* SQL_NO_CACHE */ cct_month, sum(rev)rev, sum(freq)freq 
            FROM crm_b2b_prediksi_tipe_konsumen 
            WHERE cct_cust_code = '$id' and cct_year = '$year'
            and CAST(cct_month AS int) BETWEEN $start and $end
            GROUP BY cct_month
            ORDER BY cct_month ASC";

        return $this->db->query($q);
    }
}
