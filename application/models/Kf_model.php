<?php
class Kf_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function ap_data()
	{
		$query = $this->db->query(" SELECT vendor_desc,currency,total,aging,aging_category,aging_status,
			CASE 
			WHEN aging BETWEEN 0 AND 30 THEN '0-30'
			WHEN aging BETWEEN 31 AND 60 THEN '31-60' 
			WHEN aging BETWEEN 61 AND 90 THEN '61-90' 
			WHEN aging BETWEEN 91 AND 120 THEN '91-120' 
			WHEN aging BETWEEN 121 AND 150 THEN '121-150'
			WHEN aging BETWEEN 151 AND 360 THEN '151-360'  
			ELSE '>360' END age
			FROM `f_kfho_ap` WHERE YEAR=? AND MONTH=? AND DAY=? AND entitas=?;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['day'], $_SESSION['entitas']));
		return $query->result_array();
	}
	public function ar_data()
	{
		$query = $this->db->query(" SELECT customer_desc,currency,total,aging,aging_category,aging_status,
			CASE 
			WHEN aging BETWEEN 0 AND 30 THEN '0-30'
			WHEN aging BETWEEN 31 AND 60 THEN '31-60' 
			WHEN aging BETWEEN 61 AND 90 THEN '61-90' 
			WHEN aging BETWEEN 91 AND 120 THEN '91-120' 
			WHEN aging BETWEEN 121 AND 150 THEN '121-150'
			WHEN aging BETWEEN 151 AND 360 THEN '151-360'  
			ELSE '>360' END age
			FROM `f_kfho_ar` WHERE YEAR=? AND MONTH=? AND DAY=? AND entitas=?;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['day'], $_SESSION['entitas']));
		return $query->result_array();
	}
	public function ar_data_aging()
	{
		$query = $this->db->query(" SELECT count(aging) as aging, CASE 
			WHEN aging BETWEEN 0 AND 30 THEN '0-30'
			WHEN aging BETWEEN 31 AND 60 THEN '31-60' 
			WHEN aging BETWEEN 61 AND 90 THEN '61-90' 
			WHEN aging BETWEEN 91 AND 120 THEN '91-120' 
			WHEN aging BETWEEN 121 AND 150 THEN '121-150'
			WHEN aging BETWEEN 151 AND 360 THEN '151-360'  
			ELSE '>360' END age
			FROM `f_kfho_ar` WHERE YEAR=? AND MONTH=? AND DAY=? AND entitas=? group by age;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['day'], $_SESSION['entitas']));
		return $query->result_array();
	}
	public function income_statement($max_lastupdate)
	{
		$max_lastupdate = str_replace("-", "", $max_lastupdate);
		$query = $this->db->query("SELECT key_,month,income_statement,ket,prognosa as sum_prognosa, target as sum_target,growth as sum_growth,achievement as sum_achievement,realisasi as sum_realisasi,realisasi_ytd as sum_realisasi_ytd,last_update as last_update,MAX(last_update) FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND last_update=? AND income_statement NOT IN ('NULL','COGM') GROUP BY income_statement;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['entitas'], $max_lastupdate));
		return $query->result_array();
	}

	public function max_lastupdate()
	{
		$query = $this->db->query("SELECT MAX(STR_TO_DATE(last_update, '%Y%m%d')) max_lastupdate FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL','COGM');", array($_SESSION['year'], $_SESSION['month'], $_SESSION['entitas']));
		return $query->result_array();
	}



	public function max_lastupdate_growth()
	{
		$query = $this->db->query("SELECT MAX(STR_TO_DATE(last_update, '%Y%m%d')) max_lastupdate FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL','COGM');", array($_SESSION['year'] - 1, $_SESSION['month'], $_SESSION['entitas']));
		return $query->result_array();
	}

	public function sum_income_statement($month, $year, $entitas, $max_lastupdate, $period)
	{
		if ($period === 'YearToDate') {
			$query = $this->db->query("SELECT key_,month,income_statement,ket,prognosa as sum_prognosa, target as sum_target, growth as sum_growth, achievement as sum_achievement, realisasi as sum_realisasi, realisasi_ytd as sum_realisasi_ytd, last_update as last_update FROM FI_IS_MONTH_CRON WHERE YEAR = ? AND MONTH=? AND entitas=? AND income_statement NOT IN ('COGM','NULL') Order By last_update DESC, key_ ASC Limit 10;", array($year, $month, $entitas));
		} elseif ($period === 'Monthly') {
			if ($month == 1) {
				$query = $this->db->query("SELECT key_,month,income_statement,ket,prognosa as sum_prognosa, target as sum_target_monthly, growth as sum_growth, achievement as sum_achievement, realisasi_ytd as sum_realisasi, realisasi_ytd as sum_realisasi_ytd, last_update as last_update FROM FI_IS_MONTH_CRON WHERE YEAR = ? AND MONTH=? AND entitas=? AND income_statement NOT IN ('COGM','NULL') Order By last_update DESC, key_ ASC Limit 10;", array($year, $month, $entitas));
			} else {
				$query = $this->db->query("SELECT A.key_,A.month,A.income_statement AS income_statement,A.ket AS ket,A.sum_prognosa AS sum_prognosa, A.sum_target AS sum_target, A.sum_growth AS sum_growth,A.sum_achievement AS sum_achievement, A.sum_realisasi_ytd AS sum_realisasi_ytd , A.sum_realisasi_ytd-B.sum_realisasi_ytd AS sum_realisasi,A.last_update, A.sum_target-B.sum_target AS sum_target_monthly FROM ( 
					(
					SELECT key_,MONTH,income_statement,ket,prognosa AS sum_prognosa, target AS sum_target, growth AS sum_growth,achievement AS sum_achievement, realisasi AS sum_realisasi, realisasi_ytd AS sum_realisasi_ytd, last_update
					FROM FI_IS_MONTH_CRON WHERE YEAR =? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND TARGET<>0 GROUP BY income_statement) AS A
					LEFT JOIN  (
					SELECT income_statement,ket,prognosa AS sum_prognosa, target AS sum_target, growth AS sum_growth,achievement AS sum_achievement, realisasi AS sum_realisasi, realisasi_ytd AS sum_realisasi_ytd
					FROM FI_IS_MONTH_CRON WHERE YEAR =? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND TARGET<>0  GROUP BY income_statement) AS B ON A.income_statement = B.income_statement
					)", array($year, $month, $entitas, $year, ($month - 1), $entitas));
			}
		}
		return $query->result_array();
	}

	public function sum_income_statement_daily($month, $year, $entitas, $day)
	{
		$query = $this->db->query("SELECT income_statement,ket,prognosa as sum_prognosa, target as sum_target, growth as sum_growth,achievement as sum_achievement, realisasi as sum_realisasi, realisasi_ytd as sum_realisasi_ytd FROM FI_IS_MONTH_CRON WHERE YEAR = ? AND MONTH=? AND day=? AND entitas=? AND income_statement NOT IN ('NULL') GROUP BY income_statement;", array($year, $month, $day, $entitas));
		return $query->result_array();
	}

	public function is_cogm($month, $year, $entitas)
	{

		$query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('COGM') ORDER BY MONTH ASC;", array($year, $entitas));

		return $query->result_array();
	}

	public function is_cogm_lastmonth($month, $year, $entitas)
	{

		$last_month = $month;
		$last_year = $year - 1;
		$query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('COGM') ORDER BY MONTH ASC;", array($last_year, $last_month, $entitas));

		return $query->result_array();
	}


	public function is_cogm_5($month, $year, $entitas)
	{

		$last_month = $month;
		$last_year = $year - 1;
		$query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('COGM') ORDER BY MONTH ASC;", array($last_year, $last_month, $entitas));

		return $query->result_array();
	}

	public function sum_all($startdate, $enddate, $entitas)
	{

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $startdate . "' AND '" . $enddate . "');");
		return $query->result_array();
	}

	public function sum_kftd($startdate, $enddate, $entitas)
	{
		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $startdate . "' AND '" . $enddate . "') AND distributor_code_82 IN ('KFTD');");
		return $query->result_array();
	}

	public function sum_other($startdate, $enddate, $entitas)
	{

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $startdate . "' AND '" . $enddate . "') AND distributor_code_82 NOT IN ('KFTD');");

		return $query->result_array();
	}

	public function sum_today($startdate, $enddate, $entitas)
	{

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur = '" . $startdate . "');");

		return $query->result_array();
	}

	public function sum_today_last_year($startdate, $enddate, $entitas)
	{

		$newDate = strtotime($startdate);
		$oldDate = strtotime("last year", $newDate);
		$oldDate = strtotime(date('l', $newDate), $oldDate);

		$dateFormat = 'Y-m-d';

		$last_year = date($dateFormat, $oldDate);
		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur = '" . $last_year . "');");

		return $query->result_array();
	}

	public function sum_wtd($startdate, $enddate, $entitas)
	{
		$cur_date = strtotime($startdate); // Change to whatever date you need
		// Get the day of the week: Sunday = 0 to Saturday = 6
		$dotw = date('w', $cur_date);
		if ($dotw > 1) {
			$pre_monday  =  $cur_date - (($dotw - 1) * 24 * 60 * 60);
			$next_sunday = $cur_date + ((7 - $dotw) * 24 * 60 * 60);
		} else if ($dotw == 1) {
			$pre_monday  = $cur_date;
			$next_sunday =  $cur_date + ((7 - $dotw) * 24 * 60 * 60);
		} else if ($dotw == 0) {
			$pre_monday  = $cur_date - (6 * 24 * 60 * 60);;
			$next_sunday = $cur_date;
		}

		$date_array =   array();
		$date_array["start_date_of_week"] = $pre_monday;
		$date_array["end_date_of_week"] = $next_sunday;
		// $date_array = getfirst_last_date($startdate);

		$week_start = date("Y-m-d", $date_array["start_date_of_week"]);
		$week_end = date("Y-m-d", $date_array["end_date_of_week"]);

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $week_start . "' AND '" . $week_end . "');");

		return $query->result_array();
	}

	public function sum_mtd($startdate, $enddate, $entitas)
	{
		// $start_date = date("Y-m-d",strtotime("-30 days"));
		// $end_date = date("Y-m-d");

		// $first_second = date("Y-m-01", $startdate);
		// $last_second  = date("Y-m-t", $startdate);
		$first_day_this_month = date("Y-m-01", strtotime($startdate)); // hard-coded '01' for first day
		$last_day_this_month  = date("Y-m-t", strtotime($startdate));

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $first_day_this_month . "' AND '" . $last_day_this_month . "') ");

		return $query->result_array();
	}

	public function sum_ytd($startdate, $enddate, $entitas)
	{

		$current_year = date("Y");
		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where MONTH(tanggal_faktur) =" . $current_year);

		return $query->result_array();
	}

	public function sum_last_year($startdate, $enddate, $entitas)
	{

		$last_year = date("Y") - 1;
		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where MONTH(tanggal_faktur) =" . $last_year);

		return $query->result_array();
	}

	public function sum_kf($startdate, $enddate, $entitas)
	{

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $startdate . "' AND '" . $enddate . "') AND unit IN ('KFA');");

		return $query->result_array();
	}

	public function sum_nonkf($startdate, $enddate, $entitas)
	{

		$query = $this->db->query("SELECT sum(" . $entitas . ") as total FROM datamart_admedika.table_mart_overview_ex_marketing_4
			where (tanggal_faktur BETWEEN '" . $startdate . "' AND '" . $enddate . "') AND unit IN ('NON_KFA');");

		return $query->result_array();
	}


	public function is_cogs($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('COGS') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('COGS') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function is_cogs_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('COGS') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('COGS') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_gross_profit($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('GROSS PROFIT') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('GROSS PROFIT') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}


	public function get_cash_collection($week, $month, $year, $entitas)
	{

		$query = $this->db->query("SELECT * FROM datamart_kf.f_cashcollection WHERE week=? AND month=? AND year=? AND entitas=? ;", array($week, $month, $year, $entitas));

		return $query->result_array();
	}

	public function chart_cash_collection($year, $entitas,$cakupan){		
		$query = $this->db->query("SELECT SUM(REPLACE(target, ',', '')) as target,SUM(REPLACE(`real`, ',', '')) as `real` ,SUM(REPLACE(acctarget, ',', '')) as acctarget,SUM(REPLACE(accreal, ',', '')) as accreal,`month` FROM datamart_kf.f_cashcollection WHERE year=? AND entitas=? AND cakupan=? GROUP BY `month`;", array($year, $entitas,$cakupan));	
		return $query->result_array();
	}

	public function is_gross_profit_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('GROSS PROFIT') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('GROSS PROFIT') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_net_income($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function is_net_income_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET INCOME') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET INCOME') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_net_operating($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET OPERATING INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET OPERATING INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}


	public function is_net_operating_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET OPERATING INCOME') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET OPERATING INCOME') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_net_sales($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}

		return $result_array1;
	}

	public function is_net_sales_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}


	public function is_operating_expenses($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('OPERATING EXPENSES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('OPERATING EXPENSES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function sum_marketing_statement($month, $year, $entitas)
	{
		$query = $this->db->query("select 
SUM(HJP_TY) realisasi_current_year,
SUM(HJP_LY) realisasi_last_year,
(
select sum(c.TARGET_AMOUNT_HJP) from
(
  select 
    BULAN_TAHUN
  , BRANCH_CODE
  , LAYANAN_CODE
  , DISTRIBUTOR_CODE
  , MATERIAL_CODE
  , TARGET_AMOUNT_HJP
  from datamart_admedika.dm_marketing_operation
  where TARGET_AMOUNT_HJP is not null
  group by
    BULAN_TAHUN
  , BRANCH_CODE
  , LAYANAN_CODE
  , DISTRIBUTOR_CODE
  , MATERIAL_CODE
  , TARGET_AMOUNT_HJP
) c  
) target_current_year,
( sum(hjp_ty) / (
select sum(c.TARGET_AMOUNT_HJP) from
(
  select 
    BULAN_TAHUN
  , BRANCH_CODE
  , LAYANAN_CODE
  , DISTRIBUTOR_CODE
  , MATERIAL_CODE
  , TARGET_AMOUNT_HJP
  from datamart_admedika.dm_marketing_operation
  where TARGET_AMOUNT_HJP is not null
  group by
    BULAN_TAHUN
  , BRANCH_CODE
  , LAYANAN_CODE
  , DISTRIBUTOR_CODE
  , MATERIAL_CODE
  , TARGET_AMOUNT_HJP
) c  
) ) *100 achievement,
ifnull((sum(HJP_TY)/SUM(HJP_LY))*100,0) growth
from datamart_admedika.dm_marketing_operation
where TARGET_AMOUNT_HJP is not null
and year(tanggal_faktur) = ?", array($year));
		return $query->result_array();
	}


	public function is_operating_expenses_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('OPERATING EXPENSES') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('OPERATING EXPENSES') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_other_income($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('OTHER INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('OTHER INCOME') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function is_interest_expense($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('INTEREST EXPENSE') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('INTEREST EXPENSE') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function is_income_before($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('INCOME BEFORE TAXES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('INCOME BEFORE TAXES') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function is_tax_expense($month, $year, $entitas, $max_lastupdate)
	{
		$month = (int) $month;
		$query1 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('TAX EXPENSE') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, $month, $month));
		$query2 = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND entitas=? AND (month between 1 AND ?)  AND income_statement NOT IN ('NULL') AND income_statement IN ('TAX EXPENSE') ORDER BY last_update DESC, month ASC Limit ?;", array($year, $entitas, ($month - 1), ($month - 1)));
		$result_array1 =  $query1->result_array();
		$result_array2 =  $query2->result_array();
		for ($x = count($result_array1); $x > 0; $x--) {
			$result_array1[$x - 1]['TARGETMONTHLY'] =  $result_array1[$x - 1]['TARGET'];
			$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'];
			for ($y = count($result_array2); $y > 0; $y--) {
				if ($x - 1 == $y) {
					$result_array1[$x - 1]['realisasi'] = $result_array1[$x - 1]['REALISASI_YTD'] - $result_array1[$y - 1]['REALISASI_YTD'];
					$result_array1[$x - 1]['TARGETMONTHLY'] = $result_array1[$x - 1]['TARGET'] - $result_array1[$y - 1]['TARGET'];
				}
			}
		}
		return $result_array1;
	}

	public function get_profit_center()
	{
		$query = $this->db->query('SELECT profit_center FROM datamart_kf.f_kfho_cashbank GROUP BY profit_center ORDER BY profit_center ASC');

		return $query->result_array();
	}

	public function get_profit_category($entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_cashbank";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_cashbank";
		}
		$query = $this->db->query('SELECT profit_center FROM ' . $table . ' GROUP BY profit_center ORDER BY profit_center ASC');

		return $query->result_array();
	}

	public function is_other_income_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('OTHER INCOME') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('OTHER INCOME') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_tax_expense_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('TAX EXPENSE') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('TAX EXPENSE') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_interest_expense_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('INTEREST EXPENSE') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('INTEREST EXPENSE') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function is_income_before_lastmonth($month, $year, $entitas, $max_lastupdate)
	{
		$last_month = $month;
		$last_year = $year - 1;
		// $query = $this->db->query("SELECT * FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('NET SALES') ORDER BY last_update DESC Limit 1;", array($last_year, $last_month, $entitas));
		$query = $this->db->query("SELECT A.ACHIEVEMENT,A.Adjusment_,A.F01,A.GROWTH,A.Ket,A.PROGNOSA,A.REAL,A.REALISASI_YTD,A.TARGET,A.entitas,A.income_statement,A.key_,A.last_update,A.month,A.pengurang,(A.realisasi_ytd-B.realisasi_ytd) as realisasi,A.year from
		(SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('INCOME BEFORE TAXES') ORDER BY last_update
		) as A
		LEFT join  (SELECT *
		FROM FI_IS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=? AND income_statement NOT IN ('NULL') AND income_statement IN ('INCOME BEFORE TAXES') ORDER BY last_update
		) as B on A.income_statement = B.income_statement", array($last_year, $month, $entitas, $last_year, ($month - 1), $entitas));

		return $query->result_array();
	}

	public function cash_bank()
	{

		if ($_SESSION['entitas'] === "KFHO") {
			$table = "datamart_kf.f_kfho_cashbank";
		} else if ($_SESSION['entitas'] === "KFTD") {
			$table = "datamart_kf.f_kftd_cashbank";
		}

		$query = $this->db->query("SELECT * FROM " . $table . " WHERE YEAR=? AND MONTH=? and entitas=?;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['entitas']));
		return $query->result_array();
	}

	public function cash_bank_filter($profit_center, $year, $month, $entitas)
	{
		$profit_cat = "";
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_cashbank";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_cashbank";
		}
		if (sizeof(($profit_center)) !== 0) {
			foreach ($profit_center as $key => $value) {
				$i++;

				if ($i === sizeof($profit_center)) {
					$profit_cat .= "'" . $value . "'";
				} else {
					$profit_cat .= "'" . $value . "',";
				}
			}
			$query = $this->db->query('SELECT * FROM ' . $table . ' WHERE YEAR=' + $year + ' AND MONTH=' + $month + ' and entitas=' + $entitas + ' AND profit_center in (' + $profit_cat + ');');
		} else {
			$query = $this->db->query('SELECT * FROM ' . $table . ' WHERE YEAR=' + $year + ' AND MONTH=' + $month + ' and entitas=' + $entitas + ' ;');
		}
	}
	public function balance_sheet()
	{
		$query = $this->db->query("SELECT jenis,balance_sheet,faktor,amount,amount_ytd1,ket FROM `FI_BS_MONTH_CRON` WHERE YEAR=? AND MONTH=? AND entitas=? ORDER BY `key` ASC;", array($_SESSION['year'], $_SESSION['month'], $_SESSION['entitas']));
		return $query->result_array();
	}

	public function max_lastupdate_balance_sheet($year, $month, $entitas)
	{
		$query = $this->db->query("SELECT MAX(STR_TO_DATE(last_update, '%Y%m%d')) max_lastupdate FROM FI_BS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=?", array($year, $month, $entitas));
		return $query->result_array();
	}

	public function balance_sheet_refresh($day, $month, $year, $entitas, $period, $max_lastupdate, $max_onebeforeupdate)
	{
		// $max_onebeforeupdate = ($this->db->query("SELECT MAX(STR_TO_DATE(last_update, '%Y%m%d')) max_lastupdate FROM FI_BS_MONTH_CRON WHERE YEAR=? AND MONTH=? AND entitas=?", array(($year-1), $month, $entitas)))->result_array();	
		// $max_onebeforeupdate = str_replace("-", "", $max_onebeforeupdate[0]);

		if ($period === 'Daily') {
			$query = $this->db->query("SELECT jenis,balance_sheet,faktor,amount,amount_ytd1,ket,key FROM `FI_BS_MONTH_CRON` WHERE YEAR=? AND MONTH=? AND entitas=? AND last_update=? ORDER BY `key` ASC", array($year, $month, $entitas, $day));
			return $query->result_array();
		} else if ($period === 'Monthly') {

			if ($month == 1) {
				$query = $this->db->query("select DISTINCT
			a.`key`,a.jenis,
			a.balance_sheet,
			a.faktor,
			COALESCE(a.target,0) AS target,
			COALESCE(a.amount_ytd1,0) AS amount,
			COALESCE(c.amount_ytd1,0) AS amount_2018,
			(COALESCE(a.amount_ytd1,0)-COALESCE(c.amount_ytd1,0))/(COALESCE(c.amount_ytd1,0))*100 AS growth,
			((a.amount_ytd1-c.amount_ytd1)/c.amount_ytd1)*100 AS growth_old,
			a.ket,
			a.last_update 			
			from (SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,		
			amount,	
			amount_ytd1,
			ket,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . "
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month) . "  AND entitas=" . $this->db->escape($entitas) . ")) as a
			left join(SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,
			amount,
			amount_ytd1,
			ket,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year - 1) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . "  
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year - 1) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . ")
			) AS c ON a.faktor=c.faktor
			");
			} else {
				$query = $this->db->query("SELECT DISTINCT a.`key`,a.jenis,
			a.balance_sheet,
			a.faktor,
			(COALESCE(a.target,0)-COALESCE(b.target,0)) AS target,
			(COALESCE(a.amount_ytd1,0)-COALESCE(b.amount_ytd1,0)) AS amount,
			(COALESCE(c.amount_ytd1,0)-COALESCE(d.amount_ytd1,0)) AS amount_2018,
			((COALESCE(a.amount_ytd1,0)-COALESCE(b.amount_ytd1,0))-(COALESCE(c.amount_ytd1,0)-COALESCE(d.amount_ytd1,0)))/(COALESCE(c.amount_ytd1,0)-COALESCE(d.amount_ytd1,0))*100 AS growth,
			((a.amount_ytd1-b.amount_ytd1)/b.amount_ytd1)*100 AS growth_old,
			a.ket,
			a.last_update
			FROM (SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,
			amount,
			amount_ytd1,
			ket,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . "
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month) . "  AND entitas=" . $this->db->escape($entitas) . ")
			) AS a LEFT JOIN
			(SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,
			amount,
			amount_ytd1,
			ket,
			`month`,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year) . " AND MONTH=" . $this->db->escape($month - 1) . " AND entitas=" . $this->db->escape($entitas) . " )
			) AS b ON a.faktor=b.faktor LEFT JOIN
			(SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,
			amount,
			amount_ytd1,
			ket,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year - 1) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . "  
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year - 1) . " AND MONTH=" . $this->db->escape($month) . " AND entitas=" . $this->db->escape($entitas) . ")
			) AS c ON b.faktor=c.faktor LEFT JOIN
			(SELECT 
			`key`,
			jenis,
			balance_sheet,
			target,
			faktor,
			amount,
			amount_ytd1,
			ket,
			`YEAR`,			
			last_update
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =" . $this->db->escape($year - 1) . " AND MONTH=" . ($month - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
			AND last_update=(SELECT MAX(last_update) FROM FI_BS_MONTH_CRON WHERE YEAR=" . $this->db->escape($year - 1) . " AND MONTH=" . ($month - 1) . " AND entitas=" . $this->db->escape($entitas) . ")
			) AS d ON c.faktor=d.faktor ");

				// $query = $this->db->query("SELECT a.`key`,a.jenis,
				// a.balance_sheet,
				// a.faktor,
				// (a.amount - b.amount) as amount,
				// a.amount_ytd1,
				// ((a.amount - b.amount)/b.amount)*100 as growth,a.ket,
				// a.last_update
				// from (SELECT `key`,jenis,balance_sheet,
				// faktor,
				// amount,
				// amount_ytd1,
				// ket,
				// YEAR,			
				// last_update
				//  FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=? )
				// as a left JOIN
				// (SELECT `key`,jenis,balance_sheet,
				// faktor,
				// amount,
				// amount_ytd1,
				// ket,			
				// YEAR FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=?) as b
				// on a.faktor= b.faktor
				// GROUP by 
				// a.faktor,
				// a.year ,
				// b.year
				// ORDER BY a.`key` ASC
				// ", array($year, $month, $entitas, $max_lastupdate, ($year - 1), $month, $entitas, $max_onebeforeupdate));
			}
			return $query->result_array();
		} else {

			// backup
			// $query = $this->db->query("SELECT jenis,balance_sheet,faktor,amount,amount_ytd1,ket FROM `FI_BS_MONTH_CRON` WHERE YEAR=? AND MONTH=? AND entitas=? AND last_update=? ORDER BY `key` ASC", array($year, $month, $entitas, $max_lastupdate));
			// return $query->result_array();
			// end backup	

			$query = $this->db->query("SELECT a.`key`,a.jenis
			,a.balance_sheet
			,a.faktor,
			a.amount,
			a.amount_ytd1,
			((a.amount_ytd1-b.amount_ytd1)/b.amount_ytd1)*100 as growth,
			a.ket,
		    a.last_update
			from (SELECT `key`,jenis,balance_sheet,
			faktor,
			amount,
			amount_ytd1,
			last_update,
			ket,
			YEAR
			FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=? )
			as a left JOIN
			(SELECT `key`,jenis,balance_sheet,
			faktor,
			amount,
			amount_ytd1,
			ket,YEAR FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=?) as b
			on a.faktor= b.faktor
			GROUP by 
			a.faktor,
			a.year ,
			b.year
			ORDER BY a.`key` ASC
			", array($year, $month, $entitas, $max_lastupdate, ($year - 1), $month, $entitas, $max_onebeforeupdate));
			return $query->result_array();
		}
	}


	public function balance_sheet_total($day, $month, $year, $entitas, $period, $max_lastupdate, $max_onebeforeupdate)
	{


		$query = $this->db->query("SELECT a.`key`,a.jenis
				,a.balance_sheet,
				IFNULL(a.amount,0) amount,
				IFNULL(b.amount,0) amount_last,
				IFNULL(a.amount_ytd1,0) amount_ytd1,
				IFNULL(b.amount_ytd1,0)  amount_ytd_last,
				IFNULL(((a.amount_ytd1-b.amount_ytd1)/b.amount_ytd1)*100,0) as growth,
				IFNULL(((a.amount-b.amount)/b.amount)*100,0) as growth_monthly,
				a.ket,
				a.last_update
				from (SELECT `key`,jenis,balance_sheet,
				faktor,
				sum(amount) amount,
				sum(amount_ytd1) amount_ytd1,
				last_update,
				ket,
				YEAR
				FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=? group by balance_sheet,jenis)
				as a left JOIN
				(SELECT `key`,jenis,balance_sheet,
				sum(amount) amount,
				sum(amount_ytd1) amount_ytd1,
				ket,YEAR FROM `FI_BS_MONTH_CRON` WHERE YEAR =? AND MONTH=? AND entitas=? AND last_update=? group by balance_sheet,jenis) as b
				on (a.jenis = b.jenis AND a.balance_sheet = b.balance_sheet)
				group by
				balance_sheet,jenis", array($year, $month, $entitas, $max_lastupdate, ($year - 1), $month, $entitas, $max_onebeforeupdate));
		return $query->result_array();
	}

	public function realisasi_investasi_chart($year, $entitas)
	{
		$query =  $this->db->query("SELECT sum(target) as target, sum(realisasi) as realisasi FROM `fi_investasi_month` as a WHERE YEAR=? AND entitas=? GROUP by MONTH ", array($year, $entitas));
		return $query->result_array();
	}

	public function realisasi_investasi_data($month, $year, $entitas)
	{
		$this->db->query("set @row_num = 0;");
		$query = $this->db->query("SELECT @row_num := @row_num + 1 as num, a.* FROM `fi_investasi_month` as a WHERE YEAR=? AND MONTH=? AND entitas=? ORDER BY 1,2;", array($year, $month, $entitas));
		return $query->result_array();
	}

	public function balance_sheet_tittle($day, $month, $year, $entitas, $period)
	{
		if ($period === 'Daily') {
			$query = $this->db->query("SELECT working_capital,current_ratio,cash_rasio,acid_test_rasio,der_interest_bearing,debt_rasio,equity_rasio FROM `FI_BS_RASIO_MONTH_CRON` WHERE YEAR=? AND MONTH=? AND entitas=? AND last_update=? ORDER BY 1,2;", array($year, $month, $entitas, $day));
			return $query->result_array();
		} else {
			if ($month == 1) {

				$query = $this->db->query("SELECT 
			 COALESCE(A.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_A'
			,COALESCE(B.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_B'
			,COALESCE(C.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_C'
			
			,COALESCE(A.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_MTD_A'
			,COALESCE(B.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_MTD_B'
			,COALESCE(C.WORKING_CAPITAL_YTD,0)  AS 'WORKING_CAPITAL_MTD_C'
			
			
			
			,COALESCE(A.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_A'
			,COALESCE(B.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_B'
			,COALESCE(C.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_C'
			
			,COALESCE(A.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_MTD_A'
			,COALESCE(B.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_MTD_B'
			,COALESCE(C.CURRENT_RATIO_YTD,0)  AS 'CURRENT_RATIO_MTD_C'
			
			,COALESCE(A.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_A'
			,COALESCE(B.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_B'
			,COALESCE(C.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_C'
			
			,COALESCE(A.CASH_RASIO_YTD,0) AS 'CASH_RASIO_MTD_A'
			,COALESCE(B.CASH_RASIO_YTD,0) AS 'CASH_RASIO_MTD_B'
			,COALESCE(C.CASH_RASIO_YTD,0) AS 'CASH_RASIO_MTD_C'
			
			
			,COALESCE(A.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_A'
			,COALESCE(B.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_B'
			,COALESCE(C.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_C'
			
			,COALESCE(A.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_MTD_A'
			,COALESCE(B.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_MTD_B'
			,COALESCE(C.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_MTD_C'
			
			
			,COALESCE(A.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_A'
			,COALESCE(B.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_B'
			,COALESCE(C.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_C'
			
			
			,COALESCE(A.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_MTD_A'
			,COALESCE(B.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_MTD_B'
			,COALESCE(C.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_MTD_C'
			
			,COALESCE(A.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_A'
			,COALESCE(B.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_B'
			,COALESCE(C.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_C'
			
			,COALESCE(A.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_MTD_A'
			,COALESCE(B.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_MTD_B'
			,COALESCE(C.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_MTD_C'
			
			,COALESCE(A.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_A'
			,COALESCE(B.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_B'
			,COALESCE(C.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_C'
			
			,COALESCE(A.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_MTD_A'
			,COALESCE(B.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_MTD_B'
			,COALESCE(C.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_MTD_C'
			
				
				FROM
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 	
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . ")
				) AS A LEFT JOIN
					
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 	
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " )
				) AS B ON A.MONTH=B.MONTH AND A.entitas=B.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=12 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=12 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS C ON A.entitas=C.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS D ON A.entitas=D.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS E ON A.entitas=E.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=11 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=11 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS F ON A.entitas=F.entitas");
			} else {

				$query = $this->db->query("SELECT 
			COALESCE(A.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_A'
			,COALESCE(B.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_B'
			,COALESCE(C.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_YTD_C'
			
			,COALESCE(A.WORKING_CAPITAL_YTD,0)-COALESCE(D.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_MTD_A'
			,COALESCE(B.WORKING_CAPITAL_YTD,0)-COALESCE(E.WORKING_CAPITAL_YTD,0) AS 'WORKING_CAPITAL_MTD_B'
			,COALESCE(C.WORKING_CAPITAL_YTD,0)-COALESCE(F.WORKING_CAPITAL_YTD,0)  AS 'WORKING_CAPITAL_MTD_C'
			
			
			
			,COALESCE(A.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_A'
			,COALESCE(B.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_B'
			,COALESCE(C.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_YTD_C'
			
			,COALESCE(A.CURRENT_RATIO_YTD,0)-COALESCE(D.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_MTD_A'
			,COALESCE(B.CURRENT_RATIO_YTD,0)-COALESCE(E.CURRENT_RATIO_YTD,0) AS 'CURRENT_RATIO_MTD_B'
			,COALESCE(C.CURRENT_RATIO_YTD,0)-COALESCE(F.CURRENT_RATIO_YTD,0)  AS 'CURRENT_RATIO_MTD_C'
			
			,COALESCE(A.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_A'
			,COALESCE(B.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_B'
			,COALESCE(C.CASH_RASIO_YTD,0) AS 'CASH_RASIO_YTD_C'
			
			,COALESCE(A.CASH_RASIO_YTD,0)-COALESCE(D.CASH_RASIO_YTD,0) AS 'CASH_RASIO_MTD_A'
			,COALESCE(B.CASH_RASIO_YTD,0)-COALESCE(E.CASH_RASIO_YTD,0) AS 'CASH_RASIO_MTD_B'
			,COALESCE(C.CASH_RASIO_YTD,0)-COALESCE(F.CASH_RASIO_YTD,0)  AS 'CASH_RASIO_MTD_C'
			
			
			,COALESCE(A.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_A'
			,COALESCE(B.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_B'
			,COALESCE(C.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_YTD_C'
			
			,COALESCE(A.ACID_TEST_RASIO_YTD,0)-COALESCE(D.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_MTD_A'
			,COALESCE(B.ACID_TEST_RASIO_YTD,0)-COALESCE(E.ACID_TEST_RASIO_YTD,0) AS 'ACID_TEST_RASIO_MTD_B'
			,COALESCE(C.ACID_TEST_RASIO_YTD,0)-COALESCE(F.ACID_TEST_RASIO_YTD,0)  AS 'ACID_TEST_RASIO_MTD_C'
			
			
			,COALESCE(A.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_A'
			,COALESCE(B.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_B'
			,COALESCE(C.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_YTD_C'
			
			
			,COALESCE(A.DER_INTEREST_BEARING_YTD,0)-COALESCE(D.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_MTD_A'
			,COALESCE(B.DER_INTEREST_BEARING_YTD,0)-COALESCE(E.DER_INTEREST_BEARING_YTD,0) AS 'DER_INTEREST_BEARING_MTD_B'
			,COALESCE(C.DER_INTEREST_BEARING_YTD,0)-COALESCE(F.DER_INTEREST_BEARING_YTD,0)  AS 'DER_INTEREST_BEARING_MTD_C'
			
			,COALESCE(A.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_A'
			,COALESCE(B.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_B'
			,COALESCE(C.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_YTD_C'
			
			,COALESCE(A.DEBT_RASIO_YTD,0)-COALESCE(D.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_MTD_A'
			,COALESCE(B.DEBT_RASIO_YTD,0)-COALESCE(E.DEBT_RASIO_YTD,0) AS 'DEBT_RASIO_MTD_B'
			,COALESCE(C.DEBT_RASIO_YTD,0)-COALESCE(F.DEBT_RASIO_YTD,0)  AS 'DEBT_RASIO_MTD_C'
			
			,COALESCE(A.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_A'
			,COALESCE(B.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_B'
			,COALESCE(C.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_YTD_C'
			
			,COALESCE(A.EQUITY_RASIO_YTD,0)-COALESCE(D.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_MTD_A'
			,COALESCE(B.EQUITY_RASIO_YTD,0)-COALESCE(E.EQUITY_RASIO_YTD,0) AS 'EQUITY_RASIO_MTD_B'
			,COALESCE(C.EQUITY_RASIO_YTD,0)-COALESCE(F.EQUITY_RASIO_YTD,0)  AS 'EQUITY_RASIO_MTD_C'
			
				
				FROM
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 	
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . ")
				) AS A LEFT JOIN
					
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 	
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " )
				) AS B ON A.MONTH=B.MONTH AND A.entitas=B.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=12 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=12 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS C ON A.entitas=C.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS D ON A.entitas=D.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=" . $this->db->escape($month - 1) . " AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS E ON A.entitas=E.entitas LEFT JOIN
				(
				SELECT 	
				`WORKING_CAPITAL`, 
				`WORKING_CAPITAL_YTD`, 
				`CURRENT_RATIO`, 
				`CURRENT_RATIO_YTD`, 
				`ACID_TEST_RASIO`, 
				`ACID_TEST_RASIO_YTD`, 
				`CASH_RASIO`, 
				`CASH_RASIO_YTD`, 
				`DER_INTEREST_BEARING`, 
				`DER_INTEREST_BEARING_YTD`, 
				`DEBT_RASIO`, 
				`DEBT_RASIO_YTD`, 
				`EQUITY_RASIO`, 
				`EQUITY_RASIO_YTD`, 
				`ENTITAS`, 
				`YEAR`, 
				`MONTH`, 
				`last_update`
				FROM 
				`datamart_kf`.`FI_BS_RASIO_MONTH_CRON` 
				WHERE MONTH=11 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " 
				AND last_update=(SELECT MAX(last_update) FROM FI_BS_RASIO_MONTH_CRON WHERE MONTH=11 AND YEAR=" . $this->db->escape($year - 1) . " AND entitas=" . $this->db->escape($entitas) . " ) 
				) AS F ON A.entitas=F.entitas");
			}
			return $query->result_array();
		}
	}


	public function get_ap_sum($month, $year, $day, $entitas)
	{
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ap_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ap_v2";
		} else {
			$table = "datamart_kf.f_kfho_ap_v3";
		}

		$query = $this->db->query("SELECT vendor_category,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? GROUP BY vendor_category", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ar_sum($month, $year, $day, $entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ar_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ar_v2";
		} else {
			$table = 'datamart_kf.f_kfho_ar_v3';
		}


		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT customer_category,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? GROUP BY customer_category", array($filter_date, $entitas));
		return $query->result_array();
	}


	public function get_ar_idr($month, $year, $day, $entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ar_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ar_v2";
		} else {
			$table = 'datamart_kf.f_kfho_ar_v3';
		}
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND currency in('IDR')", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ar_nonidr($month, $year, $day, $entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ar_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ar_v2";
		} else {
			$table = 'datamart_kf.f_kfho_ar_v3';
		}
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND currency not in('IDR')", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ap_idr($month, $year, $day, $entitas)
	{
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ap_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ap_v2";
		} else {
			$table = "datamart_kf.f_kfho_ap_v3";
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND currency in('IDR')", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ap_nonidr($month, $year, $day, $entitas)
	{
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ap_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ap_v2";
		} else {
			$table = "datamart_kf.f_kfho_ap_v3";
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND currency not in('IDR')", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ap_currency_ketiga($month, $year, $day, $entitas)
	{
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ap_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ap_v2";
		} else {
			$table = "datamart_kf.f_kfho_ap_v3";
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT currency,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND vendor_category = 'PIHAK KETIGA' GROUP BY currency ORDER BY currency ASC", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ap_currency_berelasi($month, $year, $day, $entitas)
	{
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ap_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ap_v2";
		} else {
			$table = "datamart_kf.f_kfho_ap_v3";
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT currency,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND vendor_category = 'PIHAK BERELASI' GROUP BY currency ORDER BY currency ASC", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ar_currency_ketiga($month, $year, $day, $entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ar_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ar_v2";
		} else {
			$table = "datamart_kf.f_kfho_ar_v3";
		}
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT currency,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND customer_category = 'PIHAK KETIGA' GROUP BY currency ORDER BY currency ASC", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function get_ar_currency_berelasi($month, $year, $day, $entitas)
	{
		if ($entitas === "KFHO") {
			$table = "datamart_kf.f_kfho_ar_v3";
		} else if ($entitas === "KFTD") {
			$table = "datamart_kf.f_kftd_ar_v2";
		} else {
			$table = "datamart_kf.f_kfho_ar_v3";
		}
		if (strlen($month) == 1) {
			$month = "0" . $month;
		}
		if (strlen($day) == 1) {
			$day = "0" . $day;
		}
		$filter_date = $year . $month . $day;
		$query = $this->db->query("SELECT currency,SUM(total) as total FROM " . $table . " WHERE STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE(?,'%Y%m%d') AND entitas=? AND customer_category = 'PIHAK BERELASI' GROUP BY currency ORDER BY currency ASC", array($filter_date, $entitas));
		return $query->result_array();
	}

	public function getAdjusmentIncomeStatement($year, $entitas, $month)
	{
		$year = $year ? "and year = '" . $year   . "'" : '';
		$entitas = $entitas ? " and entitas = '" . $entitas   . "'" : '';
		$month = $month ? " and month = '" . $month   . "'" : '';
		$query = $this->db->query("SELECT * from kf_target_adj_income  WHERE Id is not null "
			. $year
			. $entitas
			. $month
			. "order by Id ASC");
		return $query->result_array();
	}



	public function balance_sheet_aging($filter_date, $entitas, $table)
	{
		// $table = $table == 'ap'?'f_kfho_ap_v3' :'f_kfho_ar_v3';
		// $group_desc = $table == 'ap'?'vendor_desc' :'customer_desc';
		if ($table == 'ar') {
			$query =  $this->db->query("select Id as Id,customer_desc as customer_desc,text_prc as text_prc ,text_gl as text_gl, 
		sum(total) as total, 
		sum(0_30) as 0_30,
		sum(31_60) as 31_60,
		sum(61_90) as 61_90,
		sum(91_120) as 91_120,
		sum(121_150) as 121_150,
		sum(151_360) as 151_360,
		sum(360_) as 360_
		from f_kfho_ar_v3 where STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE('" . $filter_date . "','%Y%m%d') AND entitas='" . $entitas . "' 
		group by customer_desc,text_prc ,text_gl");
		} else {
			$query = $this->db->query("select Id as Id,vendor_desc as vendor_desc,text_prc as text_prc ,text_gl as text_gl, 
			sum(total) as total, 
			sum(0_30) as 0_30,
			sum(31_60) as 31_60,
			sum(61_90) as 61_90,
			sum(91_120) as 91_120,
			sum(121_150) as 121_150,
			sum(151_360) as 151_360,
			sum(360_) as 360_
			from f_kfho_ap_v3 where STR_TO_DATE(pstng_date, '%Y%m%d') <= STR_TO_DATE('" . $filter_date . "','%Y%m%d') AND entitas='" . $entitas . "' 
			group by vendor_desc,text_prc ,text_gl");
		}

		return $query->result_array();
	}

	public function postAdjustment($id, $adjusment, $entitas, $f01, $month, $real, $target, $uraian, $year, $delete, $nameuser)
	{
		$return_result = array(
			'status' => FALSE,
			'message' => 'Uraian have exited'
		);

		$return_check = 0;
		if ($entitas && $uraian && $year && $month) {
			$whereId = $id ? ' and id !=' . $this->db->escape($id) : '';
			$sqlcheck =  $this->db->query('SELECT * from kf_target_adj_income  WHERE entitas=' . $this->db->escape($entitas) . $whereId . ' and `month`=' . $this->db->escape($month) . ' and `year`=' . $this->db->escape($year) . ' and uraian=' . $this->db->escape($uraian));
			$return_check = $sqlcheck->num_rows();
		}
		// add		
		if (!$id) {
			if ($return_check == 0) {
				$sql = "INSERT INTO kf_target_adj_income (`month`, entitas, uraian, target, `real`, f01, adjusment, `year`, last_update, create_by) 
			VALUES (" . $this->db->escape($month) .
					", " . $this->db->escape($entitas) .
					"," . $this->db->escape($uraian) .
					"," . $this->db->escape($target) .
					"," . $this->db->escape($real) .
					"," . $this->db->escape($f01) .
					"," . $this->db->escape($adjusment) .
					"," . $this->db->escape($year) .
					", now() 
					," . $this->db->escape($nameuser) .
					") ";
				$this->db->query($sql);
				if ($this->db->affected_rows() > 0) {
					$return_result = "Insert successful";
				} else {
					$return_result = "Failed to insert record";
				}
			}
		} elseif ($id && $delete) {
			// delete
			$sql = "DELETE FROM kf_target_adj_income WHERE Id = $id";
			$return_result = $this->db->query($sql);
		} else {
			// update 				
			if ($return_check == 0) {
				$sql = "UPDATE kf_target_adj_income
			SET `month`=" . $this->db->escape($month) . ",
			entitas=" . $this->db->escape($entitas) . ", 
			uraian=" . $this->db->escape($uraian) . ",
			target=" . $this->db->escape($target) . ",
			f01=" . $this->db->escape($f01) . ",
			adjusment=" . $this->db->escape($adjusment) . ",
			`year`=" . $this->db->escape($year) . ",
			update_by =" . $this->db->escape($nameuser) . ",
			last_update= now() 		 		 
			WHERE Id =" . $this->db->escape($id);
				$return_result = $this->db->query($sql);
			}
		}
		$this->cronJobBS();
		return $return_result;
	}

	public function cronJobBS()
	{
		$delIS	 = $this->db->query("DELETE FROM `datamart_kf`.`FI_IS_MONTH_CRON`
		WHERE LAST_UPDATE=
		(
		SELECT DISTINCT last_update
		FROM
		(
		SELECT LAST_UPDATE FROM `datamart_kf`.`FI_IS_MONTH_CRON`
		)t1
		ORDER BY last_update DESC
		LIMIT 1
		)");
		if ($delIS) {
			$this->db->query("INSERT INTO datamart_kf.FI_IS_MONTH_CRON (REALISASI,F01,Adjusment_,TARGET,`REAL`,PROGNOSA,GROWTH,REALISASI_YTD,pengurang,Ket,ACHIEVEMENT,
			income_statement,
			KEY_,
			ENTITAS,
			`YEAR`,
			`MONTH`,
			LAST_UPDATE)
			
			SELECT 
			A.REALISASI,
			A.F01,
			COALESCE(C.Adjusment,0) AS Adjusment_,
			COALESCE(C.TARGET,0) AS TARGET, 
			COALESCE(C.REAL,0) AS `REAL`,
			CASE WHEN (A.REALISASI_YTD-C.Adjusment) IS NULL THEN COALESCE(C.REAL,0)
				  ELSE A.REALISASI_YTD-C.Adjusment END AS PROGNOSA,
			
			(A.REALISASI_YTD/A.REALISASI) AS GROWTH, 
			
			
			ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
				 ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END) AS REALISASI_YTD,
				 
					   
				 
			ROUND(COALESCE(C.REAL,0)-ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
				 ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)) AS pengurang,
				 
			CASE WHEN C.Adjusment IS NULL THEN 'not yet adjusted' 
				 ELSE 'adjusted' END AS Ket,
			
			  
			
			ROUND(CASE WHEN A.REALISASI_YTD IS NULL THEN COALESCE(C.REAL,0)
				 ELSE A.REALISASI_YTD-COALESCE(C.Adjusment,0) END)/C.TARGET AS ACHIEVEMENT,
			
			A.income_statement,
			A.KEY_,
			A.ENTITAS,
			A.YEAR,
			A.MONTH,
			A.LAST_UPDATE
			
			FROM 
			(
			SELECT * FROM datamart_kf.fi_is_month_ori
			) AS A LEFT JOIN
			(
			 SELECT * FROM datamart_kf.kf_target_adj_income
			)AS C ON A.month=C.month AND A.year=C.year AND A.entitas=C.entitas AND A.income_statement=C.uraian
			WHERE A.last_update IN (SELECT MAX(LAST_UPDATE) FROM datamart_kf.fi_is_month_ori) 
			ORDER BY A.key_,A.MONTH");
		}
	}

	public function getCashCollection($week, $month, $year, $entitas)
	{
		$query = $this->db->query("SELECT *
			FROM datamart_kf.f_cashcollection where year='" . $year . "' AND month='" . $month . "' AND week= '" . $week . "' AND entitas='" . $entitas . "' 
			");

		return $query->result_array();
	}

	public function InsertDataCashCollection($datas)
	{
		$json =  json_decode($datas, true);
		foreach ($json as &$value) {
			$query = "INSERT INTO datamart_kf.f_cashcollection
			(datacollection, cakupan, target, `real`, achievement, acctarget, accreal, accachievement, entitas, `year`, `month`, week)
			VALUES(" . $this->db->escape($value["datacollection"]) . ", " . $this->db->escape($value["cakupan"]) . ", " . $this->db->escape($value["target"]) . ", " . $this->db->escape($value["real"]) . ", " . $this->db->escape($value["achievement"]) . ", " . $this->db->escape($value["acctarget"]) . ", " . $this->db->escape($value["accreal"]) . ", " . $this->db->escape($value["accachievement"]) . ", " . $this->db->escape($value["entitas"]) . ", " . $this->db->escape($value["year"]) . ", " . $this->db->escape($value["month"]) . ", " . $this->db->escape($value["week"])  . ");";
			$this->db->query($query);
		}
		$return_result = "Insert successful";
		return $return_result;
	}

	public function refresh_cashbank($year, $entitas, $month, $profit_center)
	{

		$i = 0;
		$profit_cat = '';
		if (sizeof(($profit_center)) !== 0) {
			foreach ($profit_center as $key => $value) {
				$i++;

				if ($i === sizeof($profit_center)) {
					$profit_cat .= "'" . $value . "'";
				} else {
					$profit_cat .= "'" . $value . "',";
				}
			}
		}
		$profit_center = $profit_center ? 'AND profit_center in (' . $profit_cat . ')' : " AND profit_center in('')";
		$query = $this->db->query("SELECT * FROM f_kfho_cashbank WHERE gl_account not in ('xxxxxx') and year='" . $year . "' AND month='" . $month . "'" . $profit_center . "AND entitas='" . $entitas . "'");
		return $query->result_array();
	}
}
