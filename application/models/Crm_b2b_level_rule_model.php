<?php
class Crm_b2b_level_rule_model extends CI_Model {
	
	private $table = 'crm_b2b_level_rule';
	private $id    = 'rule_year';

	public function __construct()
	{
	    $this->load->database();
	}

	 
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{
		return $this->db->query("select SQL_CALC_FOUND_ROWS *, '' adds
            from ".$this->table." 
            $param[where] $param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	} 
	
	function dtcount()
	{
		return $this->db->count_all($this->table);
	}

    function dtquery_detail_trans($param)
    {
        // ============ initial query ============
        // return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history $param[where] $param[order] $param[limit]");
        // =======================================

        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history_v1 $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_trans()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_trans()
    {
        // ======== initial query =========
        // return $this->db->count_all('crm_b2b_cust_total_history');
        // ================================
        return $this->db->count_all('crm_b2b_cust_total_history_v1');
    }

    function dtquery_detail_retur($param)
    {
        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_retur $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_retur()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_retur()
    {
        return $this->db->count_all('crm_b2b_cust_total_retur');
    }
	//---------------------------------------------------------------------------------------------------------------------
   
    function getLini()
    {
        return $this->db->query("SELECT cct_lini lini from crm_b2b_cust_trans group by cct_lini order by cct_lini asc");
    } 

    function getLayanan()
    {
        return $this->db->query("SELECT cct_layanan layanan  from crm_b2b_cust_trans where cct_layanan is not null group by cct_layanan order by cct_layanan asc");
    }  

    function getYear()
    {
        return $this->db->query("SELECT cct_year  from b2b_crm_cust_trans group by cct_year order by cct_year asc");
    }  

	function add($item)
	{
		$this->db->insert($this->table, $item);
		return $this->db->insert_id();
    }
     
	function getbyid($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table);
	}
     
	function getbyall($month,$year,$lini,$layanan,$models)
	{ 
        // echo "SELECT * from crm_b2b_level_rule where rule_month='$month' and rule_year='$year' and rule_lini='$lini' and rule_layanan='$layanan' and rule_model='$models'";
        return $this->db->query("SELECT * from crm_b2b_level_rule where rule_month='$month' and rule_year='$year' and rule_lini='$lini' and rule_layanan='$layanan' and rule_model='$models'");
    } 
     
	function getbyyearmodel($year,$models)
	{ 
        return $this->db->query("SELECT * from crm_b2b_level_rule where rule_year='$year' and rule_model='$models'");
    } 
	
	function edit($month,$year,$lini,$layanan,$models,$item)
	{
		$this->db->where('rule_month', $month);
		$this->db->where('rule_year', $year);
		$this->db->where('rule_lini', $lini);
		$this->db->where('rule_layanan', $layanan);
		$this->db->where('rule_model', $models);
		return $this->db->update($this->table, $item);
	}
}
