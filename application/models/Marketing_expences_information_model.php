<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Marketing_expences_information_model extends MY_Model
{

    public function querys($head, $where = NULL)
    {
        $headname = $this->filterheader($head);

        $sql = "
            
            SELECT
            " . $this->filterheader($head) . "
            ,SUM(a.total_budget_ty)  										anggaran
            ,SUM(a.total_expense_ty) 										realisasi_this_year 
            ,0 										realisasi_last_year
            ,ROUND((SUM(a.total_expense_ty)/SUM(a.total_budget_ty))*100,2) 	achievement
            ,0 growth
            ,ROUND(
                (SUM(a.total_expense_ty) / 
                (
                    SELECT SUM(b.total_expense_ty) 
                    FROM datamart_kf.operasional_dm_expense b 
                    WHERE 1=1 " . $this->condition($where) . "
                )
            )*100 ,2) share_realisasi_this_year
            ,ROUND
                ( 
                    (SUM(a.total_expense_ly) /
                        (
                            SELECT SUM(b.total_expense_ly) 
                            FROM datamart_kf.operasional_dm_expense b 
                            WHERE 1=1 " . $this->condition($where) . "
                        )
                    )*100,2 
                ) share_realisasi_last_year		

            FROM datamart_kf.operasional_dm_expense a
        WHERE 1=1 
            ";
        // echo nl2br($sql);die();

        return $sql;
    }

    public function querys_last($head, $where = NULL)
    {
        $headname = $this->filterheader($head);

        $sql = "
            
            SELECT
            " . $this->filterheader($head) . "
            ,SUM(a.total_budget_ty)  										anggaran
            ,SUM(a.total_expense_ty) 										realisasi_this_year 
            ,0 										realisasi_last_year
            ,0 	achievement
            ,0 growth
            ,0 share_realisasi_this_year
            ,0 share_realisasi_last_year		

            FROM datamart_kf.operasional_dm_expense a
        WHERE 1=1 
            ";
        // echo nl2br($sql);die();

        return $sql;
    }
    public function querysPeriod_last($head, $where = NULL)
    {
        $headname = $this->filterheader($head);

        $sql = "
            
            SELECT
            " . $this->filterheaderPeriod($head) . "
            ,SUM(a.total_budget_ty)  										anggaran
            ,SUM(a.total_expense_ty) 										realisasi_this_year 
            ,SUM(a.total_expense_ly) 										realisasi_last_year
            ,ROUND((SUM(a.total_expense_ty)/SUM(a.total_budget_ty))*100,2) 	achievement
            ,ROUND(( (SUM(a.total_expense_ty)-SUM(a.total_expense_ly))/SUM(total_expense_ly) )*100,2) growth
            ,ROUND(
                (SUM(a.total_expense_ty) / 
                (
                    SELECT SUM(b.total_expense_ty) 
                    FROM datamart_kf.operasional_dm_expense b 
                    WHERE 1=1 " . $this->conditionPeriod_last($where) . "
                )
            )*100 ,2) share_realisasi_this_year
            ,ROUND
                ( 
                    (SUM(a.total_expense_ly) /
                        (
                            SELECT SUM(b.total_expense_ly) 
                            FROM datamart_kf.operasional_dm_expense b 
                            WHERE 1=1 " . $this->conditionPeriod_last($where) . "
                        )
                    )*100,2 
                ) share_realisasi_last_year		

            FROM datamart_kf.operasional_dm_expense a
        WHERE 1=1 
            ";
        return $sql;
    }
    public function querysPeriod($head, $where = NULL)
    {
        $headname = $this->filterheader($head);

        $sql = "
            
            SELECT
            " . $this->filterheaderPeriod($head) . "
            ,SUM(a.total_budget_ty)  										anggaran
            ,SUM(a.total_expense_ty) 										realisasi_this_year 
            ,SUM(a.total_expense_ly) 										realisasi_last_year
            ,ROUND((SUM(a.total_expense_ty)/SUM(a.total_budget_ty))*100,2) 	achievement
            ,ROUND(( (SUM(a.total_expense_ty)-SUM(a.total_expense_ly))/SUM(total_expense_ly) )*100,2) growth
            ,ROUND(
                (SUM(a.total_expense_ty) / 
                (
                    SELECT SUM(b.total_expense_ty) 
                    FROM datamart_kf.operasional_dm_expense b 
                    WHERE 1=1 " . $this->conditionPeriod($where) . "
                )
            )*100 ,2) share_realisasi_this_year
            ,ROUND
                ( 
                    (SUM(a.total_expense_ly) /
                        (
                            SELECT SUM(b.total_expense_ly) 
                            FROM datamart_kf.operasional_dm_expense b 
                            WHERE 1=1 " . $this->conditionPeriod($where) . "
                        )
                    )*100,2 
                ) share_realisasi_last_year		

            FROM datamart_kf.operasional_dm_expense a
        WHERE 1=1 
            ";

        return $sql;
    }
    public function querydata($where = NULL, $head)
    {
        // var_dump($head);die();
        $query = $this->querys($head, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition($where);
        }
        $query .= " GROUP BY ";
        switch ($head) {
            case 'cost_center';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center)";
                break;
            case 'biaya';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya";
                break;
            case 'gl_account';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya, CONCAT(CONCAT(gl_account_description,'-'),gl_account) ";
                break;
            case 'gpm_pm_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(gpm_pm_code,'-'),gpm_pm_name) ";
                break;
            case 'rsm_shopper_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(rsm_shopper_code,'-'),rsm_shopper_name) ";
                break;
        }

        return $query;
    }

    public function querydata_last($where = NULL, $head)
    {
        // var_dump($head);die();
        $query = $this->querys_last($head, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition_last($where);
        }
        $query .= " GROUP BY ";
        switch ($head) {
            case 'cost_center';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center)";
                break;
            case 'biaya';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya";
                break;
            case 'gl_account';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya, CONCAT(CONCAT(gl_account_description,'-'),gl_account) ";
                break;
            case 'gpm_pm_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(gpm_pm_code,'-'),gpm_pm_name) ";
                break;
            case 'rsm_shopper_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(rsm_shopper_code,'-'),rsm_shopper_name) ";
                break;
        }

        return $query;
    }

    public function getfiltervalue_biaya($id, $lini)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT nama_biaya,kode_biaya, FROM datamart_kf.operasional_dm_expense where cost_center_description in (' . $cat . ')  group by nama_biaya order by nama_biaya ASC;';
        } else {
            $query = 'SELECT nama_biaya,kode_biaya FROM datamart_kf.operasional_dm_expense group by nama_biaya order by nama_biaya ASC;';
        }

        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_gl($id, $biaya)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($biaya)) !== 0) {
            foreach ($biaya as $key => $value) {
                $i++;

                if ($i === sizeof($biaya)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT gl_account,gl_account_description FROM datamart_kf.operasional_dm_expense where nama_biaya in (' . $cat . ') AND gl_account IS NOT NULL group by gl_account,gl_account_description order by gl_account_description ASC;';
        } else {
            $query = 'SELECT gl_account,gl_account_description FROM datamart_kf.operasional_dm_expense where gl_account IS NOT NULL group by gl_account,gl_account_description order by gl_account_description ASC;';
        }
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function querydataPeriod($where = NULL, $head)
    {
        // var_dump($head);die();
        $query = $this->querysPeriod($head, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->conditionPeriod($where);
        }
        $query .= " GROUP BY ";
        switch ($head) {
            case 'cost_center';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center)";
                break;
            case 'biaya';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya";
                break;
            case 'gl_account';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya, CONCAT(CONCAT(gl_account_description,'-'),gl_account) ";
                break;
            case 'gpm_pm_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(gpm_pm_code,'-'),gpm_pm_name) ";
                break;
            case 'rsm_shopper_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(rsm_shopper_code,'-'),rsm_shopper_name) ";
                break;
        }

        return $query;
    }

    public function querydataPeriod_last($where = NULL, $head)
    {
        // var_dump($head);die();
        $query = $this->querysPeriod_last($head, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->conditionPeriod_last($where);
        }
        $query .= " GROUP BY ";
        switch ($head) {
            case 'cost_center';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center)";
                break;
            case 'biaya';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya";
                break;
            case 'gl_account';
                $query .= " CONCAT(CONCAT(cost_center_description,'-'),cost_center),biaya, CONCAT(CONCAT(gl_account_description,'-'),gl_account) ";
                break;
            case 'gpm_pm_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(gpm_pm_code,'-'),gpm_pm_name) ";
                break;
            case 'rsm_shopper_code';
                $query .= " CONCAT(CONCAT(cost_center,'-'),cost_center_description),biaya, CONCAT(CONCAT(gl_account,'-'),gl_account_description), CONCAT(CONCAT(rsm_shopper_code,'-'),rsm_shopper_name) ";
                break;
        }

        return $query;
    }

    public function get_limit_data($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $header = NULL, $value = NULL)
    {
        // echo $header;die();
        $query  = $this->querydata($where, $header, $value);
        //echo $value;die();
        //echo nl2br($query);die();
        if ($type == "data") {
            if ($limit == '-1') {
                $query .= " ";
            } else {
                $query .= " LIMIT " . $start . "," . $limit . "";
            }

            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        } else if ($type == "numrow") {

            if ($limit == '-1') {
                $query .= " ";
            } else {
                $query .= " LIMIT " . $start . "," . $limit . "";
            }
            return $this->db->query($query)->num_rows();
        } else if ($type == "allrow") {

            return $this->db->query($query)->num_rows();
        } else if ($type == "excel") {
            //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        }
        // echo nl2br($query);die();

    }

    public function get_limit_data_period($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $header = NULL, $value = NULL)
    {
        // echo $header;die();
        $query  = $this->querydataPeriod($where, $header, $value);
        //echo $value;die();
        //echo nl2br($query);die();
        if ($type == "data") {
            if ($limit == '-1') {
                $query .= " ";
            } else {
                $query .= " LIMIT " . $start . "," . $limit . "";
            }

            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        } else if ($type == "numrow") {

            if ($limit == '-1') {
                $query .= " ";
            } else {
                $query .= " LIMIT " . $start . "," . $limit . "";
            }
            return $this->db->query($query)->num_rows();
        } else if ($type == "allrow") {

            return $this->db->query($query)->num_rows();
        } else if ($type == "excel") {
            //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        }
        // echo nl2br($query);die();

    }
    public function getfiltervalue($id)
    {
        $query = "
        SELECT " . $id . " from datamart_kf.operasional_dm_expense group by " . $id . " order by " . $id . " ASC
        ";
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }
    public function ordering($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {
        //print_r( $header );die();

        $query = $this->querylimit($header);
        $query .= " LIMIT 1";
        //    / echo nl2br($query);die();
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydata($where, $header, $value);
        //echo nl2br($querys);die();

        if ($type == "data") {
            //if($start == 0 ){
            $this->db->query("DROP TABLE IF EXISTS datamart_admedika.temp_operasional_dm_expense"); //die();
            $create_tmp_data = "
                        CREATE TABLE IF NOT EXISTS datamart_admedika.temp_operasional_dm_expense (
                        SELECT * FROM (
                            " . $querys . ")X);";
            //echo nl2br($create_tmp_data);die();
            $this->db->query($create_tmp_data); //die();
            //}
            //echo $limit;die();
            $query = 'SELECT * FROM ( 
                        SELECT * FROM datamart_admedika.temp_operasional_dm_expense';
            if ($limit == '-1') {
                //echo $limit;die();
                $query .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            } else {
                //echo 20;die();
                $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            }
            //$query .= " LIMIT ".$start.",".$limit.") X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir'];

            $querylimit_total = 'SELECT * FROM (
                        SELECT * FROM datamart_admedika.temp_operasional_dm_expense';
            $querylimit_total .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            //echo nl2br($querylimit_total);die();
            //S  echo nl2br($querylimit_total);die();
            // die();

            if ($this->db->query($querylimit_total)->num_rows()) {

                $querytotal = "
                       
                            SELECT
                            " . $this->grandtotal($header) . "
                            FROM (" . $querylimit_total . ") X;";
                //echo nl2br($query);die();
                $data['data'] = $this->db->query($query)->result_array();
                $data['grandtotal'] = $this->db->query($querytotal)->row();
                //echo 2;die();
                return $data;
                // echo $limit;die();
                //$data['total'] = $this->db->query

            } else {
                return false;
            }
        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where);
            }
            if ($limit == '-1') {

                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense LIMIT " . $start . "," . $limit . "";
            }

            //echo nl2br($query);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense";
            return $this->db->query($queryrow)->num_rows();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();


    }

    public function ordering_last($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {
        //print_r( $header );die();

        $query = $this->querylimit($header);
        $query .= " LIMIT 1";
        //    / echo nl2br($query);die();
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydata_last($where, $header, $value);
        //echo nl2br($querys);die();

        if ($type == "data") {


            $query = 'SELECT * FROM ( ' . $querys;
            if ($limit == '-1') {
                //echo $limit;die();
                $query .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            } else {
                //echo 20;die();
                $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            }
            //$query .= " LIMIT ".$start.",".$limit.") X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir'];

            //echo nl2br($query);die();
            $data['data'] = $this->db->query($query)->result_array();
            $data['grandtotal'] = [];
            //echo 2;die();
            return $data;
            // echo $limit;die();
            //$data['total'] = $this->db->query


        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where);
            }
            if ($limit == '-1') {

                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense LIMIT " . $start . "," . $limit . "";
            }

            //echo nl2br($query);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense";
            return $this->db->query($queryrow)->num_rows();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();


    }

    public function orderingPeriod_last($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {
        //print_r( $header );die();

        $query = $this->querylimitPeriod($header);
        $query .= " LIMIT 1";
        //    / echo nl2br($query);die();
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydataPeriod_last($where, $header, $value);
        //echo nl2br($querys);die();


        if ($type == "data") {

            $query = 'SELECT * FROM ( ' . $querys;
            if ($limit == '-1') {
                //echo $limit;die();
                $query .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            } else {
                //echo 20;die();
                $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            }
            //$query .= " LIMIT ".$start.",".$limit.") X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir'];


            $data['data'] = $this->db->query($query)->result_array();
            $data['grandtotal'] = [];
            //echo 2;die();
            return $data;
            // echo $limit;die();
            //$data['total'] = $this->db->query


        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->conditionPeriod($where);
            }
            if ($limit == '-1') {

                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense LIMIT " . $start . "," . $limit . "";
            }


            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense";
            return $this->db->query($queryrow)->num_rows();
        }
    }


    public function groupBy($header = NULL)
    {
        if ($header == 'cost_center')
            return "cost_center";
        else {
            return $header . ",cost_center";
        }
    }

    public function OnBy($header = NULL)
    {
        if ($header != 'cost_center') {
            return "and A.cost_center=B.cost_center";
        } else {
            return "";
        }
    }
    public function select($header = NULL)
    {
        if ($header != 'cost_center') {
            return ",cost_center";
        } else {
            return "";
        }
    }


    public function orderingPeriod($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {                
        //print_r( $header );die();
        if ($type == "data") {
            //if($start == 0 ){
            $querys = "select A.cost_center_description cost_center,A.cost_center cost_center_code,
            CONCAT(CONCAT(A.nama_biaya,'-'),A.kode_biaya) AS biaya_desc 
            ,A.realisasi as realisasi,
            B.realisasi as last_year,
            A.anggaran as anggaran,
            (A.anggaran-A.realisasi) as sisa_expense,
            A.nama_biaya AS biaya ,
            CONCAT(CONCAT(A.gl_account_description,'-'),gl_account) gl_account,
            A.gl_account gl_account_code,
            CONCAT(CONCAT(A.rsm_shopper_name,'-'),A.rsm_shopper_code) rsm_shopper_code,
            A.rsm_shopper_code rsm_shopper_code_code ,
            A.bulan_tahun,
            ROUND((A.realisasi/A.anggaran),2) * 100 as achievement,
            ROUND(((A.realisasi-B.realisasi)/B.realisasi),2) * 100 as growth,
            ROUND((A.realisasi/(select sum(total_expense_ty) from  datamart_kf.operasional_dm_expense WHERE 1=1  " . $this->conditionPeriod($where, false) . ")),2) * 100  as share_realisasi_this_year,
            ROUND((B.realisasi/(select sum(total_expense_ty) from  datamart_kf.operasional_dm_expense WHERE 1=1  " . $this->conditionPeriod($where, true) . ")),2) * 100 as share_realisasi_last_year
            from (select sum(total_budget_ty)as anggaran,sum(total_expense_ty) as realisasi,cost_center_description,
            cost_center,nama_biaya,gl_account_description,kode_biaya,rsm_shopper_code,rsm_shopper_name,gl_account,bulan_tahun 
            from datamart_kf.operasional_dm_expense WHERE 1=1  " . $this->conditionPeriod($where, false) . " GROUP by " . $this->groupBy($header) . ") as A
            left JOIN (select sum(total_budget_ty)as anggaran,sum(total_expense_ty) as realisasi,cost_center_description,cost_center,nama_biaya,gl_account_description from
            datamart_kf.operasional_dm_expense where 1=1  " . $this->conditionPeriod($where, true) . " GROUP by " . $this->groupBy($header) . ") as B
            on A." . $header . " = B." . $header . " " . $this->OnBy($header) ." order by ".$ordered;
            
            // die($querys);
            
            $querytotal = "
                   
            SELECT
            " . $this->grandtotal($header) . "
            FROM (" . $querys . ") X;";
            // $data['grandtotal'] = $this->db->query($querytotal)->row();

            if ($this->db->query($querys)->num_rows()) {
                //echo nl2br($query);die();
                $data['data'] = $this->db->query($querys)->result_array();
                $data['grandtotal'] = $this->db->query($querytotal)->row();
                //echo 2;die();
                return $data;
                // echo $limit;die();
                //$data['total'] = $this->db->query

            } else {
                return false;
            }
        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->conditionPeriod($where);
            }
            if ($limit == '-1') {

                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense ";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense LIMIT " . $start . "," . $limit . "";
            }


            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense";
            return $this->db->query($queryrow)->num_rows();
        }
    }

    public function condition($cond = NULL)
    {
        //echo "<pre>"; print_r($cond); "</pre>";die();
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "'";
                } else {
                    $val_where = $val == 'null' ? '' : $val;

                    //if(strlen($val) > 0 ){
                    //$where .= " AND ".$key." = '".$val."' ";
                    $val_where = $val == 'null' ? '' : $val;
                    $where .= "AND IFNULL(" . $key . ",'') = '" . $val_where . "' ";
                    //}

                }
            }
            // echo nl2br($where);die();
            return $where;
        }
    }

    public function condition_last($cond = NULL)
    {
        //echo "<pre>"; print_r($cond); "</pre>";die();
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "'";
                } else {
                    $val_where = $val == 'null' ? '' : $val;

                    //if(strlen($val) > 0 ){
                    //$where .= " AND ".$key." = '".$val."' ";
                    $val_where = $val == 'null' ? '' : $val;
                    if ($key == 'tahun') {
                        $val_where = $val_where - 1;
                        $where .= "AND IFNULL(" . $key . ",'') = '" . $val_where . "' ";
                    } else {
                        $where .= "AND IFNULL(" . $key . ",'') = '" . $val_where . "' ";
                    }



                    //}

                }
            }
            // echo nl2br($where);die();
            return $where;
        }
    }

    public function conditionPeriod($cond = NULL, $last_year = null)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);

            foreach ($cond as $key => $val) {


                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND (bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "')";
                } else if ($key == 'tahun') {
                    $var = $last_year ? $val - 1 : $val;

                    $where .= " AND tahun = '" . $var . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {

                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";
                    for ($i = 0; $i < count($val); $i++) {
                        $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }
        // print_r($where);
        return $where;
    }

    public function conditionPeriodChart($cond = NULL, $last_year = null)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);

            foreach ($cond as $key => $val) {


                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND (bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "')";
                } else if ($key == 'tahun') {
                    $var = $last_year ? $val : $val;

                    $where .= " AND tahun = '" . $var . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {
                    $key = $key == 'nama_biaya' ? 'filter_biaya': ($key == 'gl_account_description' ? 'uraian ' :  'lini_desc1');
                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";
                    for ($i = 0; $i < count($val); $i++) {
                        $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }
        // print_r($where);
        return $where;        
    }

    public function conditionPeriod_last($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND (bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "')";
                } else if ($key == 'tahun') {
                    $var = $val - 1;

                    $where .= " AND tahun = '" . $var . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {


                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";
                    for ($i = 0; $i < count($val); $i++) {
                        $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }
        return $where;
    }


    public function condition_modal($cond = NULL)
    {
        // echo "<pre>"; print_r($cond); "</pre>";die();
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'bulan_awal') {
                    $var = $val;


                    $where .= " AND bulan between '" . $var . "'";
                } else if ($key == 'bulan_akhir') {
                    $var = $val;

                    $where .= " AND '" . $var . "'";
                } else {
                    $val_where = $val == 'null' ? '' : $val;

                    //if(strlen($val) > 0 ){
                    //$where .= " AND ".$key." = '".$val."' ";
                    $val_where = $val == 'null' ? '' : $val;
                    $where .= "AND IFNULL(" . $key . ",'') = '" . $val_where . "' ";
                    //}

                }
            }
            //echo nl2br($where);die();
            return $where;
        }
    }
    public function filterheader($head)
    {
        //print_r($head);die();
        $headname = "";
        if ($head == 'cost_center') {
            $headname =  "CONCAT(CONCAT(cost_center_description,'-'),cost_center) AS cost_center,cost_center cost_center_code";
        } else if ($head == 'biaya') {
            $headname = "CONCAT(CONCAT(cost_center_description,'-'),cost_center) AS cost_center,cost_center cost_center_code,CONCAT(CONCAT(nama_biaya,'-'),kode_biaya) AS biaya_desc  , kode_biaya AS biaya";
        } else if ($head == 'gl_account') {
            $headname = "CONCAT(CONCAT(cost_center_description,'-'),cost_center) AS cost_center,cost_center cost_center_code,CONCAT(CONCAT(nama_biaya,'-'),kode_biaya) AS biaya_desc ,kode_biaya AS biaya , CONCAT(CONCAT(gl_account_description,'-'),gl_account) gl_account , gl_account gl_account_code";
        } else if ($head == 'gpm_pm_code') {
            $headname = "CONCAT(CONCAT(cost_center_description,'-'),cost_center) AS cost_center,cost_center cost_center_code,CONCAT(CONCAT(nama_biaya,'-'),kode_biaya) AS biaya_desc ,kode_biaya AS biaya , CONCAT(CONCAT(gl_account_description,'-'),gl_account) gl_account , gl_account gl_account_code, CONCAT(CONCAT(gpm_pm_name,'-'),gpm_pm_code) gpm_pm_code, gpm_pm_code gpm_pm_code_code";
        } else if ($head == 'rsm_shopper_code') {
            $headname = "CONCAT(CONCAT(cost_center_description,'-'),cost_center_description) AS cost_center,cost_center cost_center_code,CONCAT(CONCAT(nama_biaya,'-'),kode_biaya) AS biaya_desc ,kode_biaya AS biaya , CONCAT(CONCAT(gl_account_description,'-'),gl_account) gl_account , gl_account gl_account_code, CONCAT(CONCAT(rsm_shopper_name,'-'),rsm_shopper_code) rsm_shopper_code,rsm_shopper_code rsm_shopper_code_code";
        }
        //echo $headname;die();
        return $headname;
    }
    public function filterheaderPeriod($head)
    {
        //print_r($head);die();
        $headname = "";

        $headname = "CONCAT(CONCAT(cost_center_description,'-'),cost_center_description) AS cost_center,cost_center cost_center_code,CONCAT(CONCAT(nama_biaya,'-'),kode_biaya) AS biaya_desc ,kode_biaya AS biaya , CONCAT(CONCAT(gl_account_description,'-'),gl_account) gl_account , gl_account gl_account_code, CONCAT(CONCAT(rsm_shopper_name,'-'),rsm_shopper_code) rsm_shopper_code,rsm_shopper_code rsm_shopper_code_code";

        //echo $headname;die();
        return $headname;
    }
    function groupbyheadfilter($header = NULL)
    {
        $query = "";
        if (strlen($header['head_filter']) > 0) {
            if ($header['head_filter'] == 'Month') {
                $query .= " GROUP BY  date_format(tanggal_faktur,'%m') )x";
            } else {

                $query .= " GROUP BY " . $header['head_filter'] . ")x";
            }
        } else {
            $query = ")x";
        }
        return $query;
    }
    public function querylimit($header)
    {
        if ($header == 'cost_center') {
            $query = "
                SELECT
                '' cost_center,
                '' anggaran,
                ''  realisasi_this_year,
                ''  realisasi_last_year,
                ''  achievement,
                ''  growth,
                ''  share_realisasi_this_year,
                ''  share_realisasi_last_year
                ";
        } else if ($header == 'biaya') {
            $query = "
                SELECT
                '' cost_center,
                '' biaya,
                '' anggaran,
                ''  realisasi_this_year,
                ''  realisasi_last_year,
                ''  achievement,
                ''  growth,
                ''  share_realisasi_this_year,
                ''  share_realisasi_last_year
                ";
        } else if ($header == 'gl_account') {
            $query = "
            SELECT
            '' cost_center,
            '' biaya,
            '' gl_account,
            '' anggaran,
            ''  realisasi_this_year,
            ''  realisasi_last_year,
            ''  achievement,
            ''  growth,
            ''  share_realisasi_this_year,
            ''  share_realisasi_last_year
            ";
        } else if ($header == 'gpm_pm_code') {
            $query = "
            SELECT
            '' cost_center,
            '' biaya,
            '' gl_account,
            '' gpm_pm_code,
            '' anggaran,
            ''  realisasi_this_year,
            ''  realisasi_last_year,
            ''  achievement,
            ''  growth,
            ''  share_realisasi_this_year,
            ''  share_realisasi_last_year
            ";
        } else if ($header == 'rsm_shopper_code') {
            $query = "
            SELECT
            '' cost_center,
            '' biaya,
            '' gl_account,
          
            '' rsm_shopper_code,
            '' anggaran,
            ''  realisasi_this_year,
            ''  realisasi_last_year,
            ''  achievement,
            ''  growth,
            ''  share_realisasi_this_year,
            ''  share_realisasi_last_year
            ";
        }

        return $query;
    }

    public function querylimitPeriod($header)
    {

        $query = "
            SELECT
            '' cost_center,
            '' biaya,
            '' gl_account,
          
            '' rsm_shopper_code,
            '' anggaran,
            ''  realisasi_this_year,
            ''  realisasi_last_year,
            ''  achievement,
            ''  growth,
            ''  share_realisasi_this_year,
            ''  share_realisasi_last_year
            ";



        return $query;
    }
    public function grandtotalPeriod($header)
    {
        $query = "
        'GrandTotal' cost_center,
        sum(anggaran) anggaran,
        count(biaya) biaya,
        count(gl_account) gl_account,
        sum(realisasi_this_year) realisasi_this_year,
        sum(realisasi_last_year) realisasi_last_year,
        sum(achievement) achievement,
        sum(growth) growth,
        sum(share_realisasi_this_year) share_realisasi_this_year,
        sum(share_realisasi_last_year) share_realisasi_last_year
        ";

        return $query;
    }
    public function grandtotal($header)
    {
        if ($header == 'cost_center') {
            $query = "
            
            'GrandTotal' cost_center,
            sum(anggaran) anggaran,
            sum(realisasi) realisasi_this_year,
            sum(last_year) realisasi_last_year,
            sum(anggaran) -  sum(realisasi) as sisa_expense,
            sum(achievement) achievement,
            sum(growth) growth,
            sum(share_realisasi_this_year) share_realisasi_this_year,
            sum(share_realisasi_last_year) share_realisasi_last_year
            ";
        } else if ($header == 'nama_biaya') {
            $query = "
            'GrandTotal' cost_center,
            sum(anggaran) anggaran,
            count(biaya) biaya,
            sum(realisasi) realisasi_this_year,            
            sum(last_year) realisasi_last_year,
            sum(anggaran) -  sum(realisasi) as sisa_expense,
            sum(achievement) achievement,
            sum(growth) growth,
            sum(share_realisasi_this_year) share_realisasi_this_year,
            sum(share_realisasi_last_year) share_realisasi_last_year
            ";
        } else if ($header == 'gl_account_description') {
            $query = "
        'GrandTotal' cost_center,
        sum(anggaran) anggaran,
        count(biaya) biaya,
        count(gl_account) gl_account,
        sum(realisasi) realisasi_this_year,
        sum(last_year) realisasi_last_year,
        sum(anggaran) -  sum(realisasi) as sisa_expense,
        sum(achievement) achievement,
        sum(growth) growth,
        sum(share_realisasi_this_year) share_realisasi_this_year,
        sum(share_realisasi_last_year) share_realisasi_last_year
        ";
        } else if ($header == 'gpm_pm_code') {
            $query = "
        'GrandTotal' cost_center,
        sum(anggaran) anggaran,
        count(biaya) biaya,
        count(gl_account) gl_account,
        count(gpm_pm_code) gpm_pm_code,
        sum(realisasi_this_year) realisasi_this_year,
        sum(realisasi_last_year) realisasi_last_year,
        sum(achievement) achievement,
        sum(growth) growth,
        sum(share_realisasi_this_year) share_realisasi_this_year,
        sum(share_realisasi_last_year) share_realisasi_last_year
        ";
        } else if ($header == 'rsm_shopper_code') {
            $query = "
        'GrandTotal' cost_center,
        sum(anggaran) anggaran,
        count(biaya) biaya,
        count(gl_account) gl_account,
       
        count(rsm_shopper_code) rsm_shopper_code,
        sum(realisasi_this_year) realisasi_this_year,
        sum(realisasi_last_year) realisasi_last_year,
        sum(achievement) achievement,
        sum(growth) growth,
        sum(share_realisasi_this_year) share_realisasi_this_year,
        sum(share_realisasi_last_year) share_realisasi_last_year
        ";
        }
        return $query;
    }



    ///////////MODAL

    public function orderingmodal($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {
        $query = $this->querylimitmodal($header);
        $query .= " LIMIT 1";
        //echo nl2br($query);die();
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = "
        SELECT TEXT,SUM(total_amount_ty) totalanggaran 
        FROM operasional_dm_expense a
        WHERE 1=1
        " . $this->condition_modal($where) . "";
        $querys .= " GROUP BY TEXT";
        //echo nl2br($querys);die();


        if ($type == "data") {
            $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_operasional_dm_expense_modal"); //die();
            $create_tmp_data = "
            CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_operasional_dm_expense_modal (
            SELECT * FROM (
                " . $querys . ")X)
            ;";
            //testing
            //  echo nl2br($create_tmp_data);die();
            $this->db->query($create_tmp_data); //die();
            $query = 'SELECT * FROM ( 
                SELECT * FROM datamart_admedika.temp_operasional_dm_expense_modal';
            if ($limit == '-1') {
                echo 1;
                die();
                // $query .= " ) X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir'];
            } else {
                echo 2;
                die();
                //$query .= " LIMIT ".$start.",".$limit.") X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir'];
            }

            //echo nl2br($query);die();

            //echo nl2br($querylimit_total);die();
            //S  echo nl2br($querylimit_total);die();
            // die();
            if ($this->db->query($query)->num_rows()) {


                // echo nl2br($querytotal);die();
                $data['data'] = $this->db->query($query)->result_array();
                return $data;
            } else {
                return false;
            }
        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition_modal($where);
            }
            if ($limit == '-1') {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense_modal";
            } else {
                $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense_modal LIMIT " . $start . "," . $limit . "";
            }

            //echo nl2br($query);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense_modal";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = "";
            $queryrow = "SELECT * FROM datamart_admedika.temp_operasional_dm_expense_modal";
            return $this->db->query($queryrow)->result_array();
        }
    }
    public function querylimitmodal($header)
    {

        $query = "
            SELECT
            '' TEXT,
            '' totalanggaran
            ";
        return $query;
    }

    public function getDetailExpen($tgl_account_code, $start, $end, $cost_center_code)
    {
        $queryrow = "select * from datamart_kf.operasional_dm_exp_text WHERE gl_account
         ='" . $tgl_account_code . "' and bulan_tahun >='" . $start . "' and bulan_tahun <='" . $end . "' and cost_center ='" . $cost_center_code . "'";
        // die($queryrow);
        // die($this->db->query($queryrow)->num_rows());
        if ($this->db->query($queryrow)->num_rows() > 0) {
            $data = $this->db->query($queryrow)->result_array();
            return $data;
        } else {
            return false;
        }
    }

    public function chartOrderingPeriod($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {        
        $query = $this->db->query("SELECT monthname(str_to_date(bulan,'%m')) as bulan_name,bulan, sum(expenses) as expenses, sum(realisasi) as realisasi,
		CAST((sum(expenses)/sum(realisasi))*100 as decimal(8,2)) as ratio
		from datamart_kf.usc_insight_realisasi_expenses 
        WHERE 1=1 ".$this->conditionPeriodChart($where, true)
        // ."and cost_centre_description = '' -- filter lini
        // and nama_biaya = '' -- filter_biaya
        // and gl_account_description = '' -- filter uraian 
        ."group by bulan
        order by bulan asc");
        // die($this->db->last_query());
        return $query->result_array();
    }
}
