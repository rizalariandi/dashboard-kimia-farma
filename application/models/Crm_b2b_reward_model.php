<?php
class Crm_b2b_reward_model extends CI_Model {
	
	private $table = 'crm_b2b_reward';
	private $id    = 'cbr_id';

	public function __construct()
	{
	    $this->load->database();
	}

	 
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{
		return $this->db->query("select SQL_CALC_FOUND_ROWS *, '' adds
            from ".$this->table." 
            $param[where] $param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	} 
	
	function dtcount()
	{
		return $this->db->count_all($this->table);
	}

    function dtquery_detail_trans($param)
    {
		// ======== initial code ==========
        // return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history $param[where] $param[order] $param[limit]");
		// ================================
		return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_history_v1 $param[where] $param[order] $param[limit]");
	}

    function dtfiltered_detail_trans()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_trans()
    {
        // ============ initial code =============
		// return $this->db->count_all('crm_b2b_cust_total_history');
		// =======================================

		return $this->db->count_all('crm_b2b_cust_total_history_v1');
    }

    function dtquery_detail_retur($param)
    {
        return $this->db->query("select SQL_CALC_FOUND_ROWS * from crm_b2b_cust_total_retur $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail_retur()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_detail_retur()
    {
        return $this->db->count_all('crm_b2b_cust_total_retur');
    }
	//---------------------------------------------------------------------------------------------------------------------
    

	function add($item)
	{
		$this->db->insert($this->table, $item);
		return $this->db->insert_id();
    }
     
	function getbyid($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->get($this->table);
	} 
	
	function edit($id,$item)
	{
		$this->db->where($this->id, $id);
		return $this->db->update($this->table, $item);
    } 
	
	function delete($id)
	{
		$this->db->where($this->id, $id);
		return $this->db->delete($this->table);
	}
}
