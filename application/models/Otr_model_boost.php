<?php
class Otr_model_boost extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_channels($table){
        return $this->db->distinct()->select("channel, CASE WHEN channel != 'Marketing' THEN 0 ELSE 1 END AS ordering")->order_by('ordering')->order_by('channel')->get($table)->result();
    }

    function get_channels_where($table,$channel){
        return $this->db->distinct()->select("channel, CASE WHEN channel != 'Marketing' THEN 0 ELSE 1 END AS ordering")->where('channel', $channel)->order_by('ordering')->order_by('channel')->get($table)->result();
    }

    function get_kftd($table) {
        return $this->db->distinct()->select("nama_kftd")->get($table)->result();
    }

    function get_brand($table) {
        return $this->db->distinct()->select("nama_brand")->get($table)->result();
    }

    function json_outlet_brand(){
        $brand_table = $this->input->post("brand_table",true);

        $area = $this->input->post("area_table", true);
        $chan = $this->input->post("channel_table", true);
        $data_area = explode(",", $area);
        $data_channel = explode(",",$chan);
        $data_brand = explode(",",$brand_table);
        $channels = $this->get_channels('usc_bst_sales_otr_brand');
        $brand = $this->get_brand('usc_bst_sales_otr_brand');
        // var_dump($brand);
        $tahun = [date("Y",strtotime("-1 year")), date("Y")];
        
        $this->kf->select("*");
        $this->kf->from('usc_bst_sales_otr_brand');
    
        if (!empty($brand_table)) {
            $this->kf->group_start();
            foreach ($data_brand as $a) {
            // foreach($area as $a){
                // $this->kf->or_like('nama_kftd', $a);
                $this->kf->or_where('nama_brand', $a);
            }
            $this->kf->group_end();                                                                                                                           
        }

        if (!empty($chan)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
            // foreach ($chan as $c) {
                // $this->kf->or_like('channel', $c);
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();    
        }
        $this->kf->group_by(array('nama_brand','channel'));       

        $data = $this->kf->generate();
        
        // return $data; 
        $raw = json_decode($data);
        // var_dump($raw->data);
        // die();
        $temp = array();
        
        foreach ($brand as $in=>$kf){
            foreach ($channels as $ch){
                $temp[$in][$ch->channel . '-' . date("Y",strtotime("-1 year"))] = "0";
                $temp[$in][$ch->channel . '-' . date("Y")] = "0";
                foreach ($raw->data as $key=>$value) {
                    if($value->nama_brand != ""){
                    if($value->nama_brand == $kf->nama_brand){
                        if ($ch->channel == $value->channel) {
                            $temp[$in][$ch->channel . '-' . date("Y",strtotime("-1 year"))] = $value->total_outlet_ly;
                            $temp[$in][$ch->channel . '-' . date("Y")] = $value->total_outlet_ty;
                        }
                        $temp[$in]['nama_brand'] = $value->nama_brand;

                    }
                }
                }
            }
        }
        $temp2 = array();
        foreach ($temp as $v){
            if(array_key_exists("nama_brand", $v)){
                $temp2[] = $v;
            }
        }
        $raw->data = $temp2;
        return json_encode($raw);
    }

    function json_outlet_area(){
        $area = $this->input->post("area_table", true);
        $chan = $this->input->post("channel_table", true);
        $data_area = explode(",", $area);
        $data_channel = explode(",",$chan);

        $channels = $this->get_channels('usc_bst_sales_otr_cabang');
        $kftd = $this->get_kftd('usc_bst_sales_otr_cabang');
        $tahun = [date("Y",strtotime("-1 year")), date("Y")];
        
        $this->kf->select("*");
        $this->kf->from('usc_bst_sales_otr_cabang');

        if (!empty($area)) {
            $this->kf->group_start();

            foreach ($data_area as $a) {
                $this->kf->or_where('nama_kftd', $a);
            }
            $this->kf->group_end();    
        }
        
        if (!empty($chan)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();

        }

        $this->kf->group_by(array("nama_kftd","channel"));       
        $data = $this->kf->generate();
        // return $data; 
        $raw = json_decode($data);
        // var_dump($raw->data);
        // die();
        $temp = array();
        foreach ($kftd as $in=>$kf){
            foreach ($channels as $ch){
                $temp[$in][$ch->channel . '-' . date("Y",strtotime("-1 year"))] = "0";
                $temp[$in][$ch->channel . '-' . date("Y")] = "0";
                foreach ($raw->data as $key=>$value) {
                    if($value->nama_kftd == $kf->nama_kftd){
                        $temp[$in]['nama_kftd'] = $value->nama_kftd;
                        if ($ch->channel == $value->channel) {
                            $temp[$in][$ch->channel . '-' . date("Y",strtotime("-1 year"))] = $value->total_outlet_ly;
                            $temp[$in][$ch->channel . '-' . date("Y")] = $value->total_outlet_ty;
                        }
                        
                    }
                }
            }
        }
        
        $temp2 = array();
        foreach ($temp as $v){
            if(array_key_exists("nama_kftd", $v)){
                $temp2[] = $v;
            }
        }
        $raw->data = $temp2;
        return json_encode($raw);
        // return json_encode($this->db->get('usc_bst_sales_otr_cabang')->result());
    }

}