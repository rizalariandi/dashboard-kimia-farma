<?php
class Boost_master_produk_model extends CI_Model{

	function product_list(){
		$hasil=$this->db->get('kf_master_produk_kompetitor');
		return $hasil->result();
	}

	function get_product($produk_id, $nama_produk) {
		$this->db->where('produk_id', $produk_id);
		$this->db->where('nama_produk', $nama_produk);
		$hasil = $this->db->get('kf_master_produk_kompetitor')->result();
		return $hasil;
	}

	function get_product_by_id($id) {
		$this->db->where('id', $id);
		return $this->db->get('kf_master_produk_kompetitor')->result();
	}

	function save_product(){
		$data = array(
			'produk_id' 	=> $this->input->post('produk_id'), 
			'nama_produk' 	=> $this->input->post('nama_produk'), 
		);

		if ($data['produk_id'] == $data['nama_produk']) {
			$data['kategori'] = 1;
		} else {
			$data['kategori'] = 0;
		}
		
		$result=$this->db->insert('kf_master_produk_kompetitor',$data);
		return $result;
	}

	function update_product(){
		$id = $this->input->post('id');
		$produk_id = $this->input->post('produk_id');
		$nama_produk =$this->input->post('nama_produk');

		$this->db->set('produk_id', $produk_id);
		$this->db->set('nama_produk', $nama_produk);
		$this->db->where('id', $id);
		$result=$this->db->update('kf_master_produk_kompetitor');
		return $result;
	}

	function delete_product(){
		$id=$this->input->post('id');
		$this->db->where('id', $id);
		$result=$this->db->delete('kf_master_produk_kompetitor');
		return $result;
	}
	
}