<?php
class Dtp_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
		public function email($id){
			$query = $this->db->query("SELECT id_dtp,a.nama,b.nama nama1,detail,pic,due_date,a.status,email,b.subject FROM process a LEFT JOIN dtp b ON b.id=a.id_dtp WHERE id_dtp=? ORDER BY a.id DESC LIMIT 1;",array($id));
			return $query->row();
		}

		public function getData(){
			if($_SESSION['level']=='admin'){
				$query = $this->db->query("SELECT id,email,isi,subject,timestamp,status FROM dtp order by timestamp desc;");
			}else{
				$query = $this->db->query("SELECT id,isi,subject,timestamp,ifnull(status,'Waiting Response...')status FROM dtp where email=?  order by timestamp desc;",array($_SESSION['username']));
			}
			return $query->result_array();
		}
		public function saveProcess($d,$p){
			$this->db->query("INSERT INTO process(id_dtp, nama,detail, pic, due_date,status,timestamp) values(?,?,?,?,?,?,now())",array($d,$p['process'],$p['detail'],$p['pic'],$p['due_date'],$p['status']));
			$this->db->query("update dtp set status=? where id=?",array($p['status'],$d));
		}

		public function getDataId($id){
      $query = $this->db->query("SELECT isi,subject,id FROM dtp where id=?",array($id));
			return $query->row();
		}

    public function getPIC(){
      $query = $this->db->query("SELECT * FROM member");
      $temp = array();
      for($i = 0 ;$i < $query->num_rows();$i++){
        array_push($temp, $query->row($i));
      }
      return $temp;
    }

		public function getProcess($id){
			$query = $this->db->query("select * from process where id_dtp=?;",array($id));
			return $query->result_array();
		}
		public function inputDtp($d){
			$query = $this->db->query("INSERT INTO dtp(email,subject,isi,useragent,ip,timestamp) values(?,?,?,?,?,now())",array($_SESSION['username'],$d['subject'],$d['isi'],$_SERVER['HTTP_USER_AGENT'],$_SERVER['REMOTE_ADDR']));
		}
}
