<?php

class Boost_kftd_channel_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function check_data($table, $area, $produk, $from='', $to='')
    {
        if(($table == 'usc_bst_product_mart') or ($table == 'usc_bst_product_details_mart') ){
            $this->db->where('produk_id', $produk);
        } else {
            $this->db->where('produk', $produk);
        }
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        return $this->db->get($table)->result();   
    }

    function get_data_export_channel($table, $area, $produk, $from, $to) {
        $this->db->select('produk, channel, provinsi, bulan, SUM(sales) as sales, SUM(quantity) as quantity');
        $this->db->where('provinsi', $area);
        $this->db->where('produk', $produk);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("channel","bulan"));

        $a = $this->db->get($table)->result_array();
        // die($this->db->last_query());
        return $a;
    }

    function get_area($table)
    {
        return $this->db->distinct()->select('provinsi')->get($table)->result();
    }

    function get_produk_channel($table)
    {
        return $this->db->distinct()->select('produk')->get($table)->result();
    }

    function get_list_channel($table, $area, $produk) {
        return $this->db->distinct()->select('channel')->where('produk',$produk)->where('provinsi', $area)->order_by('channel','asc')->get($table)->result();
    }

    function total_all_by_produk($area, $produk, $filter = 'value', $from='', $to='') {
        if ($filter == 'value') {
            $this->db->select('SUM(sales) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
        } else {
            $this->db->select('SUM(quantity) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
        }
        $total_sales = $this->db->get('usc_bst_channel_mart')->result();
        return (int)$total_sales[0]->total;
    }

    function get_bcg_x($table, $area, $produk)
    {
        $data = $this->db->select('batasan_market_share')
            ->where('area', $area)
            ->where('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error            
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_share;
        }
    }

    function get_bcg_y($table, $area, $produk)
    {
        $data = $this->db->select('batasan_market_growth')
            ->where('area', $area)
            ->where('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_growth;
        }
    }

    function get_bcg_matrix_channel($table, $area, $produk, $filter = 'value', $from='', $to='')
    {
        $list_produk = $this->get_list_channel($table, $area, $produk);
        // die($this->db->last_query());

        $total_all_by_produk = $this->total_all_by_produk($area, $produk, $filter, $from, $to);
        // die($this->db->last_query());

        //get market share
        if ($filter == 'value') {
            $this->db->select('channel, SUM(sales) / ' . $total_all_by_produk . ' as market_share');
        } else {
            $this->db->select('channel, SUM(quantity) / ' . $total_all_by_produk . ' as market_share');
        }
        $this->db->where('produk', $produk);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("channel"));
        $output_market = $this->db->get($table)->result();
        // die($this->db->last_query());
    
        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('channel, bulan, SUM(sales) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("channel","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('channel, bulan, SUM(quantity) as total');
            $this->db->where('produk', $produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("channel","bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();
        // die($this->db->last_query());

        $growth = array();

        foreach($list_produk as $p=>$val) {
            $realisasi_awal = -1;
            $realisasi_akhir = 0;    

            foreach($output_growth as $key=>$value) {
                if ($val->channel == $value->channel) {
                    if ($value->channel == "") {
                        continue;
                    }
                    // $growth[$p][] = number_format((float) ($value->total / $total_growth[$p]->total), 2, '.', '' ) * 100 ;
                    if( $value->total == 0 and $realisasi_awal == -1) {
                        $realisasi_awal = 0;
                    } else if($realisasi_awal == -1){
                        $realisasi_awal = $value->total;
                    } 
                    
                    // echo $realisasi_awal ."\n";
                    $realisasi_akhir = $realisasi_akhir + $value->total; 
                   
                    if ($realisasi_awal == 0  ) {
                        $growth[$val->channel] = 100;
                    } else {
                        $growth[$val->channel] = number_format((float) (($realisasi_akhir - $realisasi_awal) / $realisasi_awal),2,'.','' )* 100;
                    }
                }
            }
        }

        $json = array();
        foreach ($list_produk as $key=>$value){
            $json[$key]["name"] = $value->channel;
            $market_share = 0;
            foreach($output_market as $market)
            {
                if ($value->channel == "")
                {
                    continue;
                } 
                if( $market->channel == $value->channel)
                {
                    $market_share = $market->market_share * 100;
                    $json[$key]["x"] = $market_share;
                }
            }
  
            foreach($output_growth as $g){
                if ($value->channel == "")
                {
                    continue;
                }
                if( $g->channel == $value->channel) {
                    $json[$key]["y"] = $growth[$value->channel];
                }
            }
            $json[$key]["color"] = color_channel($value->channel);
        }
        return json_encode($json, JSON_NUMERIC_CHECK);
    }

}