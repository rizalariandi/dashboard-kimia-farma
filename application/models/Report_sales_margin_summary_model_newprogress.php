<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Report_sales_margin_summary_model_newprogress extends MY_Model{
    public function querys($head,$value,$where = NULL){
        $sql = "
        SELECT 
          ".$this->filterheader($head)." 
        
        FROM datamart_kf.dm_market_operation1
        WHERE 1=1 
        ";
       // echo nl2br($sql);die();
        return $sql;
    }

    public function getfiltervalue_rsm($id,$gpm){
        $i = 0;
        $cat = "";

        if (sizeof(($gpm)) !== 0) {
            foreach ($gpm as $key => $value) {
                $i++;

                if ($i === sizeof($gpm)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT rsm_code,rsm_name FROM datamart_kf.dm_market_operation1 where gpm_pm_code in ('.$cat.') AND rsm_code IS NOT NULL group by rsm_code,rsm_name order by rsm_name ASC;';
        } else {
            $query = 'SELECT rsm_code,rsm_name FROM datamart_kf.dm_market_operation1 where rsm_code IS NOT NULL group by rsm_code,rsm_name order by rsm_name ASC;';
        }

        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }

    public function getfiltervalue_shopper($id,$rsm){
    $i = 0;
    $cat = "";

    if (sizeof(($rsm)) !== 0) {
        foreach ($rsm as $key => $value) {
            $i++;

            if ($i === sizeof($rsm)) {
                $cat .= "'" . $value . "'";
            } else {
                $cat .= "'" . $value . "',";
            }
        }
        $query = 'SELECT shopper_code,shopper_name FROM datamart_kf.dm_market_operation1 where rsm_code in ('.$cat.') AND shopper_code IS NOT NULL group by shopper_code,shopper_name order by shopper_name ASC;';
    } else {
        $query = 'SELECT shopper_code,shopper_name FROM datamart_kf.dm_market_operation1 where shopper_code IS NOT NULL group by shopper_code,shopper_name order by rsm_name ASC;';
    }

    if($this->db->query($query)->num_rows() > 0 ){
        return $this->db->query($query)->result();
    }else{
        return false;
    }
}

    public function getfiltervalue_brand($id,$lini){
        $i = 0;
        $cat = "";

        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT brand,brand FROM datamart_kf.dm_market_operation1 where lini_code in ('.$cat.') AND brand IS NOT NULL group by brand order by brand ASC;';
        } else {
            $query = 'SELECT brand,brand FROM datamart_kf.dm_market_operation1 where brand IS NOT NULL group by brand order by brand ASC;';
        }

        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }
    public function querysdata($head,$value,$where = NULL,$i = NULL){
        $sql = "";
        $sql .= "
        select ".$head." as nama , description , sum(target) as target, sum(realisasi) as realisasi, sum(last_year) as last_year, 
        avg(achieve) as achieve, avg(growth) as growth from dm_market_operation1_hardcode where group_layanan != 'TOTAL'".$this->groupingdata($head)."";
       //echo nl2br($sql);die();
        
        return $sql;
    }

    public function querysdata_last($head,$value,$where = NULL,$i = NULL){
        $sql = "";
        $sql .= "
        select ".$head." as nama , description , sum(target) as target, sum(realisasi) as realisasi, sum(last_year) as last_year, 
        avg(achieve) as achieve, avg(growth) as growth from dm_market_operation1_hardcode where group_layanan != 'TOTAL'".$this->groupingdata($head)."";
       //echo nl2br($sql);die();
        

        return $sql;
    }
   public function groupingdata($head){
    $query = "";
    if(strlen($head) > 0 ){
        if($head == 'Month'){
            $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
        }else{
            
             $query .= " GROUP BY description, ".$head;
        }
    }
    return $query;
   }
    public function queryrow($head,$value){
        $sql = "
        SELECT 
        SUM(".$value.") realisasi_ty
        FROM dm_marketing_operation
        WHERE 1=1 
        ";
        //echo nl2br($sql);die();
        return $sql;
    }
    public function querylimit(){
        $query = "SELECT
        '' nama,
        '' description,
        '' target,
        '' realisasi_this_year,
        '' realisasi_last_year,
        '' achievement,
        '' growth
        ";

return $query;
}
    public function querydata($where = NULL,$head = NULL,$value = NULL){
      // var_dump($head);die();
            $query = $this->querys($head,$value,$where);
            //echo nl2br($query);die();
            if($where != NULL){
                $query .= $this->condition($where);
            }
            if(strlen($head) > 0 ){
                if($head == 'Month'){
                    $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
                }else{
                    
                     $query .= " GROUP BY  ".$head;
                }
            }
            
           
           // echo nl2br($query);die();
            return $query;
        
    }
    public function get_limit_data($start = NULL , $limit = NULL , $where = NULL, $type = NULL,$header= NULL,$value = NULL){
       // echo $header;die();
        $query  = $this->querydata($where,$header,$value); 
        //echo $value;die();
        //echo nl2br($query);die();
        if($type == "data"){

            $query .= " LIMIT ".$start.",".$limit."";
            if($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            }else{
                return false;
            }
            

        }else if($type == "numrow"){

            $query .= " LIMIT ".$start.",".$limit."";
            return $this->db->query($query)->num_rows();

        }else if($type == "allrow"){

            return $this->db->query($query)->num_rows();
               
        }else if($type == "excel"){
           //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            }else{
                return false;
            }
        }
       // echo nl2br($query);die();
        
    }
 
    public function ordering($start = NULL , $limit = NULL , $where = NULL, $type = NULL,$ordered = NULL,$header = NULL,$value = NULL){
       
        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();
       
        foreach($setdataorder[0] as $rowdataorder => $val){
            $rows[] =  $rowdataorder;
        }
        $query  = "";

       //echo nl2br($querys);die();
       
        
        if($type == "data"){
            if($start == 0 ){
//                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin");
//                $create_tmp_data = "
//                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration_margin(
//                    SELECT * FROM (
//                        ".$querys.")X)";
//
//                   $this->db->query($create_tmp_data);//die();

           //$count_query = $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin")->num_rows();
          // $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin_data");
//           $queryss = "
//           CREATE  TABLE  datamart_admedika.temp_dm_marketing_opration_margin_data(
//               SELECT * FROM (";
            $queryss = $this->querysdata($header,$value,$where,"");
            // die($queryss);

            }//die();
            
          //  echo nl2br($queryss);die();
            $query = 'SELECT * FROM ('.$queryss;
            if($limit == '-1'){
                $query .= " ) X ";
            }   else{
                $query .= " LIMIT ".$start.",".$limit.") X ";
            } 
            //$query .= " ORDER BY nama ".$ordered['dir']." ,  counter ASC LIMIT ".$start.",".$limit.") X ";
          //  echo nl2br($query);die();
            
           // echo nl2br($query);
            //echo nl2br($querylimit_total);die();
           // die();
        //    die($query);

               //echo nl2br($querytotal);die();

                $data['data'] = $this->db->query($query)->result_array();
                // die($query);
                return $data;
                //$data['total'] = $this->db->query
                

        }else if($type == "numrow"){
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if($where != NULL){
                $where_c .= $this->condition($where);
            }
            if($limit == '-1'){
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data ";
            }else{
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data LIMIT ".$start.",".$limit."";
            }
            
           // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();

        }else if($type == "allrow"){
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data" ;
            return $this->db->query($queryrow)->num_rows();
        }else if($type == "excel"){
            $queryrow = '';
            $queryrow = "
            SELECT 
                description,
                counter,
                target,
                realisasi_this_year,
                realisasi_last_year,
                achievement,
                growth
            FROM datamart_admedika.temp_dm_marketing_opration_margin_data;

            " ;          
            return $this->db->query($queryrow)->result_array();
         }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }

    public function ordering_last($start = NULL , $limit = NULL , $where = NULL, $type = NULL,$ordered = NULL,$header = NULL,$value = NULL){

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach($setdataorder[0] as $rowdataorder => $val){
            $rows[] =  $rowdataorder;
        }
        $query  = "";

        //echo nl2br($querys);die();


        if($type == "data"){
            if($start == 0 ){
//                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin");
//                $create_tmp_data = "
//                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration_margin(
//                    SELECT * FROM (
//                        ".$querys.")X)";
//
//                   $this->db->query($create_tmp_data);//die();

                //$count_query = $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin")->num_rows();
                // $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin_data");
//           $queryss = "
//           CREATE  TABLE  datamart_admedika.temp_dm_marketing_opration_margin_data(
//               SELECT * FROM (";
                $queryss = $this->querysdata_last($header,$value,$where,"");


            }//die();

            //  echo nl2br($queryss);die();
            $query = 'SELECT * FROM ('.$queryss;
            if($limit == '-1'){
                $query .= " ) X ";
            }   else{
                $query .= " LIMIT ".$start.",".$limit.") X ";
            }
            //$query .= " ORDER BY nama ".$ordered['dir']." ,  counter ASC LIMIT ".$start.",".$limit.") X ";
            //  echo nl2br($query);die();

            // echo nl2br($query);
            //echo nl2br($querylimit_total);die();
            // die();


            //echo nl2br($querytotal);die();

            $data['data'] = $this->db->query($query)->result_array();

            return $data;
            //$data['total'] = $this->db->query


        }else if($type == "numrow"){
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if($where != NULL){
                $where_c .= $this->condition($where);
            }
            if($limit == '-1'){
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data ";
            }else{
                $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data LIMIT ".$start.",".$limit."";
            }

            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();

        }else if($type == "allrow"){
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data" ;
            return $this->db->query($queryrow)->num_rows();
        }else if($type == "excel"){
            $queryrow = '';
            $queryrow = "
            SELECT 
                description,
                counter,
                target,
                realisasi_this_year,
                realisasi_last_year,
                achievement,
                growth
            FROM datamart_admedika.temp_dm_marketing_opration_margin_data;

            " ;
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }
    

    public function gettarget($id = NULL){
        if($id == 'HPP_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HJP_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HJP_PTD_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HNA_TY'){
            $id = 'TARGET_AMOUNT_HNA';
        }else if($id == 'HJD_TY'){
            $id = 'TARGET_AMOUNT_HNA';
        }
        return $id;
    }

  public function getly($id= NULL){
    if($id == 'HPP_TY'){
        $id = 'HPP_LY';
    }else if($id == 'HJP_TY'){
        $id = 'HJP_LY';
    }else if($id == 'HJP_PTD_TY'){
        $id = 'HJP_PTD_LY';
    }else if($id == 'HNA_TY'){
        $id = 'HNA_LY';
    }else if($id == 'HJD_TY'){
        $id = 'HJD_LY';
    }
    return $id;
  }
    
    public function getfiltervalue($id){
        $lini_code = $id === 'lini_code' ? " where lini_code != 104" : "";
        $query = "
        SELECT * from datamart_admedika.".$id.$lini_code;
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }
    
    public function f_tahun(){
        $query = "        
            SELECT RIGHT(BULAN_TAHUN,4) f_tahun 
            FROM datamart_kf.dm_market_operation1
            GROUP BY RIGHT(BULAN_TAHUN,4)
        ";
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }
    public function f_bulan(){
        $query = "        
            SELECT LEFT(BULAN_TAHUN,1) f_bulan
            FROM datamart_kf.dm_market_operation1
            GROUP BY LEFT(BULAN_TAHUN,1)
        ";
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }

       public function condition($cond = NULL){
        if($cond != NULL){
            $where = '';
            $count_filter = count($cond);
            foreach($cond as $key => $val){
                
                    if($key == 'tanggal_faktur_start'){
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $dateed=  date('Y-m-d', strtotime($date));
                        
                        $where .= " AND tanggal_faktur between '".$dateed."'";
                    }else if($key == 'tanggal_faktur_end'){
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $date_end =  date('Y-m-d', strtotime($date));
                        $where .= " AND '".$date_end."'";
                    }else if($key == 'value'){
                        $where .= "";
                    }else{
                            $where .= "AND IFNULL(".$key.",'') in (";
                            for($i = 0 ;  $i < count($val) ; $i++){
                                $val_where = $val[$i] == 'null' || $val[$i] == ''  ? '' : $val[$i];
                                if(count($val) - $i > 1 ){
                                    $where .= " '".$val_where."' , ";
                                }else{
                                    $where .= " '".$val_where."'  ";
                                }  
                            }
                            $where .= ")";
                           
                        }
                        
                        
                    }
                
            }
            return $where;
        
        
    }



    public function condition_last($cond = NULL){
        if($cond != NULL){
            $where = '';
            $count_filter = count($cond);
            foreach($cond as $key => $val){

                if($key == 'tanggal_faktur_start'){
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);

                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $dateed =  date('Y-m-d', strtotime($date));


                    $where .= " AND tanggal_faktur between '".$dateed."'";
                }else if($key == 'tanggal_faktur_end'){
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);
                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $date_end =  date('Y-m-d', strtotime($date));
                    $where .= " AND '".$date_end."'";
                }else if($key == 'value'){
                    $where .= "";
                }else{
                    $where .= "AND IFNULL(".$key.",'') in (";
                    for($i = 0 ;  $i < count($val) ; $i++){
                        $val_where = $val[$i] == 'null' || $val[$i] == ''  ? '' : $val[$i];
                        if(count($val) - $i > 1 ){
                            $where .= " '".$val_where."' , ";
                        }else{
                            $where .= " '".$val_where."'  ";
                        }
                    }
                    $where .= ")";

                }


            }

        }
        return $where;


    }
    public function filterheader($head){
        $headname = "";
        if($head == 'Month'){
            $headname =  "date_format(tanggal_faktur,'%m') as nama";
        }else if($head == 'distributor_code'){
            $headname = "CONCAT(CONCAT(distributor_name,'-'),distributor_code) AS nama";
        }else if($head == 'branch_code'){
            $headname = "CONCAT(CONCAT(branch_name,'-'),branch_code) AS nama";
        }else if($head == 'gpm_pm_code'){
            $headname = "CONCAT(CONCAT(gpm_pm_name,'-'),gpm_pm_code) AS nama";
        }else if($head == 'rsm_code'){
            $headname = "CONCAT(CONCAT(rsm_name,'-'),rsm_code) AS nama";
        }else if($head == 'shopper_code'){
            $headname = "CONCAT(CONCAT(shopper_name,'-'),shopper_code) AS nama";
        }else if($head == 'am_apm_asm_code'){
            $headname = "CONCAT(CONCAT(am_apm_asm_name,'-'),am_apm_asm_code) AS nama";
        }else if($head == 'msr_md_se_code'){
            $headname = "CONCAT(CONCAT(msr_md_se_name,'-'),msr_md_se_code) AS nama";
        }else if($head == 'segment'){
            $headname = "SEGMENT AS nama";
        }else if($head == 'customer_code'){
            $headname = "CONCAT(CONCAT(customer_name,'-'),customer_code) AS nama";
        }else if($head == 'layanan_code'){
            $headname = "CONCAT(CONCAT(layanan_name,'-'),layanan_code) AS nama";
        }else if($head == 'lini_code'){
            $headname = "CONCAT(CONCAT(lini_name,'-'),lini_code) AS nama";
        }else if($head == 'material_group1_code'){
            $headname = "CONCAT(CONCAT(material_group1_name,'-'),material_group1_code) AS nama";
        }else if($head == 'material_group2_code'){
            $headname = "CONCAT(CONCAT(material_group2_name,'-'),material_group2_code) AS nama";
        }else if($head == 'material_group3_code'){
            $headname = "CONCAT(CONCAT(material_group3_name,'-'),material_group3_code) AS nama";
        }else if($head == 'brand'){
            $headname = "brand AS nama";
        }else if($head == 'material_code'){
            $headname = " CONCAT(CONCAT(CONCAT(CONCAT(material_material_name,'-'),kemasan),'-'), material_code ) AS nama";
        }
      //  echo $headname;die();
        return $headname;

    }
    function groupbyheadfilter($header = NULL){
        $query = "";
        if(strlen($header['head_filter' ]) > 0  ){
            if($header['head_filter'] == 'Month'){
                $query .= " GROUP BY  date_format(tanggal_faktur,'%m') )x";
            }else{
                
                 $query .= " GROUP BY ".$header['head_filter'].")x" ;
            }
        }else{
            $query = ")x";
        }
        return $query;
    }
    
    
   

}



?>