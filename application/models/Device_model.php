<?php
class Device_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
		
		public function getDevicePerfomance(){
			$query = $this->db->query("SELECT DISTINCT device FROM performance WHERE device NOT LIKE '1%';");
			return $query->result_array();
		}
		
		public function getPerformance($data){
			$query = $this->db->query("SELECT time,device,
			ifnull(temperature,0)temperature,
			ifnull(temperatureMin,0)temperatureMin,
			ifnull(temperatureMax,0)temperatureMax,
			ifnull(cpu,0)cpu,
			ifnull(cpuMin,0)cpuMin,
			ifnull(cpuMax,0)cpuMax,
			ifnull(memory,0)memory,
			ifnull(memoryMin,0)memoryMin,
			ifnull(memoryMax,0)memoryMax,
			ifnull(port,0)port,
			ifnull(portMin,0)portMin,
			ifnull(portMax,0)portMax
			from performanceMinMax where device='$data[hostname]' order by 1;");
			return $query->result_array();
		}
		public function getOccupancy($dt){
			if(!empty($dt)){
				$query = $this->db->query("SELECT $dt[w] w,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.0 AND 0.3 THEN 1 ELSE 0 END)red,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.4 AND 0.7 THEN 1 ELSE 0 END)yellow,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.8 AND 1 THEN 1 ELSE 0 END)green,
					COUNT(*)jml FROM odp_occ where witel='$dt[n]' or reg='Regional $dt[n]' GROUP BY 1;");
			}else{
				$query = $this->db->query("SELECT reg w,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.0 AND 0.3 THEN 1 ELSE 0 END)red,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.4 AND 0.7 THEN 1 ELSE 0 END)yellow,
					SUM(CASE WHEN round(available/kapasitas,1)BETWEEN 0.8 AND 1 THEN 1 ELSE 0 END)green,
					COUNT(*)jml FROM odp_occ GROUP BY 1;");
			}
			return $query->result_array();
		}
		public function getOccChart($dt){
			if(!empty($dt)){
				$query = $this->db->query("SELECT $dt[w] w,SUM(service)used,SUM(blocking)blok,
				SUM(reserve)reser,SUM(available)avail FROM odp_occ
				where witel='$dt[n]' or reg='Regional $dt[n]' GROUP BY 1;");
			}else{
				$query = $this->db->query("SELECT reg w,SUM(service)used,SUM(blocking)blok,
				SUM(reserve)reser,SUM(available)avail FROM odp_occ GROUP BY 1;");
			}
			return $query->result_array();
		}
		
		public function getWitel(){
			$query = $this->db->query("SELECT distinct witel FROM odp_occ order by 1;");
			return $query->result_array();
		}
		public function getSTO(){
			$query = $this->db->query("SELECT distinct sto FROM odp_occ order by 1;");
			return $query->result_array();
		}
		public function getUserActivity(){
			$query = $this->db->query("SELECT distinct ds,acs,source_ip,des_ip,user FROM user_activity limit 200;");
			return $query->result_array();
		}
}