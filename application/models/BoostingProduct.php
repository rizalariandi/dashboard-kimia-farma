<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class BoostingProduct extends CI_Model {

    public function __construct()
	{
		$this->load->database();
    }
    
    public function retrieve_bcg($area, $kategori)
    {
        $query = $this->db->query("SELECT * FROM `boost_bcg` 
            WHERE AREA=? AND KATEGORI=?;", array($area, $kategori));
		return $query->result_array();
    }
}