<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Report_sales_margin_model extends MY_Model{
    public function querys($head,$value,$where = NULL){
        $sql = "
        SELECT 
          ".$this->filterheader($head)." 
        
        FROM datamart_kf.dm_market_operation
        WHERE 1=1 
        ";
       // echo nl2br($sql);die();
        return $sql;
    }
     
    public function querysdata($head,$value,$where = NULL,$i = NULL){
        $sql = "
        select 
        ".$this->filterheader($head).",
        'SALES - PTD' description, 
        '1' counter,
        (
        select sum(c.target-PTD_TY) from
        (
          select 
              BULAN_TAHUN
          , BRANCH_CODE
          , LAYANAN_CODE
          , DISTRIBUTOR_CODE
          , MATERIAL_CODE
          ,  ".$this->gettarget($value)." target
          , SUM(PTD_TY) AS PTD_TY
          from datamart_kf.dm_market_operation
          where 1=1
          
          ".$this->condition($where)."
          group by
              BULAN_TAHUN
          , BRANCH_CODE
          , LAYANAN_CODE
          , DISTRIBUTOR_CODE
          , MATERIAL_CODE
          , ".$this->gettarget($value)."
        ) c  
        ) target,
        SUM(".$value."-PTD_TY) realisasi_this_year,
        SUM(".$value."-PTD_LY) realisasi_last_year,
        (
        SUM(".$value."-PTD_TY)/
        (
        select sum(c.target_quantity) from
        (
          select 
              BULAN_TAHUN
          , BRANCH_CODE
          , LAYANAN_CODE
          , DISTRIBUTOR_CODE
          , MATERIAL_CODE
          , target_quantity
          from datamart_kf.dm_market_operation
          where 1=1
         
          ".$this->condition($where)."
          group by
              BULAN_TAHUN
          , BRANCH_CODE
          , LAYANAN_CODE
          , DISTRIBUTOR_CODE
          , MATERIAL_CODE
          , target_quantity
        ) c  
        )
        ) achievement,
        ((SUM(".$value."-PTD_TY)/SUM(".$this->getly($value)."-PTD_LY))-1)*100 growth
        from datamart_kf.dm_market_operation
        where 1=1
       
        ".$this->condition($where)."
        ".$this->groupingdata($head)."

        UNION ALL
        select 
        ".$this->filterheader($head).", 
        'PTD' description, 
        '2' counter,
        0 target,
        SUM(PTD_TY) realisasi_this_year,
        SUM(PTD_LY) realisasi_last_year,
        0 achievement,
        ((SUM(PTD_TY)/SUM(PTD_LY))-1)*100 growth
        from datamart_kf.dm_market_operation
        where 1=1
        
        ".$this->condition($where)."
        ".$this->groupingdata($head)."

        UNION ALL
        select 
        ".$this->filterheader($head).", 
        'SALES' description,
        '3' counter,
        (
        select sum(c.target) from
        (
        select 
            BULAN_TAHUN
        , BRANCH_CODE
        , LAYANAN_CODE
        , DISTRIBUTOR_CODE
        , MATERIAL_CODE
        , ".$this->gettarget($value)." target
        from datamart_kf.dm_market_operation
        where 1=1
       
        ".$this->condition($where)."
        group by
            BULAN_TAHUN
        , BRANCH_CODE
        , LAYANAN_CODE
        , DISTRIBUTOR_CODE
        , MATERIAL_CODE
        , ".$this->gettarget($value)."
        ) c  
        ) target,
        SUM(".$value.") realisasi_this_year,
        SUM(".$this->getly($value).") realisasi_last_year,
        (
        SUM((QTY_TY))/
        (
        select sum(c.target_quantity) from
        (
        select 
            BULAN_TAHUN
        , BRANCH_CODE
        , LAYANAN_CODE
        , DISTRIBUTOR_CODE
        , MATERIAL_CODE
        , target_quantity
        from datamart_kf.dm_market_operation
        where 1=1
       
        ".$this->condition($where)."
        group by
            BULAN_TAHUN
        , BRANCH_CODE
        , LAYANAN_CODE
        , DISTRIBUTOR_CODE
        , MATERIAL_CODE
        , target_quantity
        ) c  
        )
        )*SUM(".$value.") achievement,
        ((SUM(".$value.")/SUM(".$this->getly($value)."))-1)*100 growth
        from datamart_kf.dm_market_operation
        where 1=1
       
        ".$this->condition($where)."
        ".$this->groupingdata($head)."
        
        UNION ALL
        Select 
            ".$this->filterheader($head).", 
            'COGS' description, 
            '4' counter,
            (
            select sum(c.target_quantity*c.HPP_TY) from
            (
            select 
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY) AS HPP_TY
            from datamart_kf.dm_market_operation
            where 1=1
            
            ".$this->condition($where)."
            group by
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY)
            ) c  
            ) target,
            SUM(HPP_TY) realisasi_this_year,
            SUM(HPP_LY) realisasi_last_year,
            (
            SUM((HPP_TY))/
            (
            select sum(c.target_quantity) from
            (
            select 
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            from datamart_kf.dm_market_operation
            where 1=1
            
            ".$this->condition($where)."
            group by
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            ) c  
            )
            ) achievement,
            ((SUM(HPP_TY)/SUM(HPP_LY))-1)*100 growth
            from datamart_kf.dm_market_operation
            where 1=1
            
            ".$this->condition($where)."
            ".$this->groupingdata($head)."

            UNION ALL
            select 
            ".$this->filterheader($head).", 
            'COGS_PERSEN' description,
            '5' counter,
            (
            select sum(c.target_quantity*( c.HPP_TY/ c.HNA_TY )) from
            (
            select 
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY) AS HPP_TY
            , ROUND(".$value."/QTY_TY) AS HNA_TY
            from datamart_kf.dm_market_operation
            where 1=1
            
            ".$this->condition($where)."
            group by
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY)
            , ROUND(HNA_TY/QTY_TY)
            ) c  
            ) * 100 target,
            ( (SUM(HPP_TY)/SUM(".$value.")) / (SUM(".$value.")/SUM(".$value.")) )*100 realisasi_this_year,
            ( (SUM(HPP_LY)/SUM(".$this->getly($value).")) / (SUM(".$this->getly($value).")/SUM(".$this->getly($value).")) )*100 relisasi_las_year,
            (
            (SUM(HPP_TY)/SUM(".$value."))/
            ( 
            select sum(c.target_quantity*( c.HPP_TY/ c.HNA_TY )) from
            (
            select 
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY) AS HPP_TY
            , ROUND(".$value."/QTY_TY) AS HNA_TY
            from datamart_kf.dm_market_operation
            where 1=1
            
            ".$this->condition($where)."
            group by
                BULAN_TAHUN
            , BRANCH_CODE
            , LAYANAN_CODE
            , DISTRIBUTOR_CODE
            , MATERIAL_CODE
            , target_quantity
            , ROUND(HPP_TY/QTY_TY)
            , ROUND(".$value."/QTY_TY)
            ) c  
            )
            )*100 achivement,
            ( ( (SUM(HPP_TY)/SUM(".$value.")) / (SUM(HPP_LY)/SUM(".$this->getly($head).")) )-1)*100 growth
            from datamart_kf.dm_market_operation
            where 1=1
           
            ".$this->condition($where)."
            ".$this->groupingdata($head)."

            UNION ALL
            select 
                ".$this->filterheader($head).", 
                'GROSS_MARGIN' description,
                '6' counter,
                (
                select sum(c.TARGET-HPP_TY) from
                (
                select 
                    BULAN_TAHUN
                , BRANCH_CODE
                , LAYANAN_CODE
                , DISTRIBUTOR_CODE
                , MATERIAL_CODE
                , ".$this->gettarget($value)." TARGET
                , SUM(HPP_TY) AS HPP_TY
                from datamart_kf.dm_market_operation
                where 1=1
                
                ".$this->condition($where)."
                group by
                    BULAN_TAHUN
                , BRANCH_CODE
                , LAYANAN_CODE
                , DISTRIBUTOR_CODE
                , MATERIAL_CODE
                , ".$this->gettarget($value)."
                ) c  
                ) target,
                SUM(".$value."-HPP_TY) realisasi_this_year,
                SUM(".$this->getly($value)."-HPP_LY) realisasi_last_year,
                (
                SUM(".$value."-HPP_TY)/
                (
                select sum(c.TARGET-HPP_TY) from
                (
                select 
                    BULAN_TAHUN
                , BRANCH_CODE
                , LAYANAN_CODE
                , DISTRIBUTOR_CODE
                , MATERIAL_CODE
                , ".$this->gettarget($value)." TARGET
                , SUM(HPP_TY) AS HPP_TY
                from datamart_kf.dm_market_operation
                where 1=1
                ".$this->condition($where)."
                group by
                    BULAN_TAHUN
                , BRANCH_CODE
                , LAYANAN_CODE
                , DISTRIBUTOR_CODE
                , MATERIAL_CODE
                , ".$this->gettarget($value)."
                ) c   
                )
                ) achievement,
                ((SUM(".$value."-HPP_TY)/SUM(".$this->getly($value)."-HPP_LY))-1)*100 growth
                from datamart_kf.dm_market_operation
                where 1=1
               
                ".$this->condition($where)."
                ".$this->groupingdata($head)."
                
                UNION ALL
                select 
                    ".$this->filterheader($head).", 
                    'GROSS_MARGIN_PERSEN' description,
                    '7' counter,
                    (
                    1-
                    (
                    select sum(c.target_quantity*( c.HPP_TY/ c.HNA_TY )) from
                    (
                    select 
                        BULAN_TAHUN
                    , BRANCH_CODE
                    , LAYANAN_CODE
                    , DISTRIBUTOR_CODE
                    , MATERIAL_CODE
                    , target_quantity
                    , ROUND(HPP_TY/QTY_TY) AS HPP_TY
                    , ROUND(".$value."/QTY_TY) AS HNA_TY
                    from datamart_kf.dm_market_operation
                    where 1=1
                    
                    ".$this->condition($where)."
                    group by
                        BULAN_TAHUN
                    , BRANCH_CODE
                    , LAYANAN_CODE
                    , DISTRIBUTOR_CODE
                    , MATERIAL_CODE
                    , target_quantity
                    , ROUND(HPP_TY/QTY_TY)
                    , ROUND(".$value."/QTY_TY)
                    ) c  
                    )
                    )*100 target,
                    ( 1-( (SUM(HPP_TY)/SUM(".$value.")) / (SUM(".$value.")/SUM(".$value.")) ) )*100 realisasi_this_year,
                    ( 1-( (SUM(HPP_LY)/SUM(".$this->getly($value).")) / (SUM(".$this->getly($value).")/SUM(".$this->getly($value).")) ) )*100 realisasi_last_year,
                    0 achievement,
                    0 growth
                    from datamart_kf.dm_market_operation
                    where 1=1
                   
                    ".$this->condition($where)."
                    ".$this->groupingdata($head)."

               
                ";
        //echo nl2br($sql);die();
        return $sql;
    }
   public function groupingdata($head){
    $query = "";
    if(strlen($head) > 0 ){
        if($head == 'Month'){
            $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
        }else{
            
             $query .= " GROUP BY  ".$head;
        }
    }
    return $query;
   }
    public function queryrow($head,$value){
        $sql = "
        SELECT 
        SUM(".$value.") realisasi_ty
        FROM dm_marketing_operation
        WHERE 1=1 
        ";
        //echo nl2br($sql);die();
        return $sql;
    }
    public function querylimit(){
        $query = "SELECT
        '' nama,
        '' description,
        '' target,
        '' realisasi_this_year,
        '' realisasi_last_year,
        '' achievement,
        '' growth
        ";

return $query;
}
    public function querydata($where = NULL,$head = NULL,$value = NULL){
      // var_dump($head);die();
            $query = $this->querys($head,$value,$where);
            //echo nl2br($query);die();
            if($where != NULL){
                $query .= $this->condition($where);
            }
            if(strlen($head) > 0 ){
                if($head == 'Month'){
                    $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
                }else{
                    
                     $query .= " GROUP BY  ".$head;
                }
            }
            
           
           // echo nl2br($query);die();
            return $query;
        
    }
    public function get_limit_data($start = NULL , $limit = NULL , $where = NULL, $type = NULL,$header= NULL,$value = NULL){
       // echo $header;die();
        $query  = $this->querydata($where,$header,$value); 
        //echo $value;die();
        //echo nl2br($query);die();
        if($type == "data"){

            $query .= " LIMIT ".$start.",".$limit."";
            if($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            }else{
                return false;
            }
            

        }else if($type == "numrow"){

            $query .= " LIMIT ".$start.",".$limit."";
            return $this->db->query($query)->num_rows();

        }else if($type == "allrow"){

            return $this->db->query($query)->num_rows();
               
        }else if($type == "excel"){
           //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            }else{
                return false;
            }
        }
       // echo nl2br($query);die();
        
    }
 
    public function ordering($start = NULL , $limit = NULL , $where = NULL, $type = NULL,$ordered = NULL,$header = NULL,$value = NULL){
       
        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();
       
        foreach($setdataorder[0] as $rowdataorder => $val){
            $rows[] =  $rowdataorder;
        }
        $query  = "";
        $querys  = $this->querydata($where,$header,$value); 
      
       
        
        if($type == "data"){
            if($start == 0 ){
                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin");
                $create_tmp_data = "
                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration_margin(
                    SELECT * FROM (
                        ".$querys.")X)";
                   //echo nl2br($create_tmp_data);die();     
                   $this->db->query($create_tmp_data);//die();
                   // echo nl2br($create_tmp_data);die();     
                
        //echo $count_query;die();
           $count_query = $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin")->num_rows();
           $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration_margin_data");
           $queryss = "
           CREATE  TABLE  datamart_admedika.temp_dm_marketing_opration_margin_data(
               SELECT * FROM (";
            $queryss .= $this->querysdata($header,$value,$where,"");   
            $queryss .= ")X ORDER BY nama,counter);";
            //echo nl2br($queryss);die();
            $this->db->query($queryss);//die();
            }//die();
            
          //  echo nl2br($queryss);die();
            $query = 'SELECT * FROM ( 
                SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data';
            $query .= " LIMIT ".$start.",".$limit.") X ORDER BY nama,".$rows[$ordered['column']]." ".$ordered['dir'];
            //echo nl2br($query);die();
            
           // echo nl2br($query);
            //echo nl2br($querylimit_total);die();
           // die();
            if($this->db->query($query)->num_rows()) {
                
               
               //echo nl2br($querytotal);die();   
                $data['data'] = $this->db->query($query)->result_array();
                return $data;
                //$data['total'] = $this->db->query
                
            }else{
                return false;
            }
            
        }else if($type == "numrow"){
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if($where != NULL){
                $where_c .= $this->condition($where);
            }
          
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data LIMIT ".$start.",".$limit."";
           // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();

        }else if($type == "allrow"){
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data" ;
            return $this->db->query($queryrow)->num_rows();
        }else if($type == "excel"){
            //echo nl2br($query);die();
             //$query .= " LIMIT ".$start.",".$limit."";
             if($this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data")->num_rows() > 0) {
                 return $this->db->query("SELECT * FROM datamart_admedika.temp_dm_marketing_opration_margin_data")->result_array();
             }else{
                 return false;
             }
         }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }
    

    public function gettarget($id = NULL){
        if($id == 'HPP_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HJP_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HJP_PTD_TY'){
            $id = 'TARGET_AMOUNT_HJP';
        }else if($id == 'HNA_TY'){
            $id = 'TARGET_AMOUNT_HNA';
        }else if($id == 'HJD'){
            $id = 'TARGET_AMOUNT_HNA';
        }
        return $id;
    }

  public function getly($id= NULL){
    if($id == 'HPP_TY'){
        $id = 'HPP_LY';
    }else if($id == 'HJP_TY'){
        $id = 'HJP_LY';
    }else if($id == 'HJP_PTD_TY'){
        $id = 'HJP_PTD_LY';
    }else if($id == 'HNA_TY'){
        $id = 'HNA_LY';
    }else if($id == 'HJD'){
        $id = 'HJD_LY';
    }
    return $id;
  }
    
    public function getfiltervalue($id){
        $query = "
        SELECT * from datamart_admedika.".$id."
        ";
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }
    
    public function f_tahun(){
        $query = "        
            SELECT RIGHT(BULAN_TAHUN,4) f_tahun 
            FROM datamart_kf.dm_market_operation
            GROUP BY RIGHT(BULAN_TAHUN,4)
        ";
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }
    public function f_bulan(){
        $query = "        
            SELECT LEFT(BULAN_TAHUN,1) f_bulan
            FROM datamart_kf.dm_market_operation
            GROUP BY LEFT(BULAN_TAHUN,1)
        ";
        if($this->db->query($query)->num_rows() > 0 ){
            return $this->db->query($query)->result();
        }else{
            return false;
        }
    }

       public function condition($cond = NULL){
        if($cond != NULL){
            $where = '';
            $count_filter = count($cond);
            foreach($cond as $key => $val){
                
                    if($key == 'tanggal_faktur_start'){
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $dateed=  date('Y-m-d 00:00:00', strtotime($date));
                        
                        $where .= " AND tanggal_faktur between '".$dateed."'";
                    }else if($key == 'tanggal_faktur_end'){
                        $var = $val;
                        $date = str_replace('/', '-', $var);
                        $date_end =  date('Y-m-d 00:59:59', strtotime($date));
                        $where .= " AND '".$date_end."'";
                    }else if($key == 'value'){
                        $where .= "";
                    }else{
                            $where .= "AND IFNULL(".$key.",'') in (";
                            for($i = 0 ;  $i < count($val) ; $i++){
                                $val_where = $val[$i] == 'null' || $val[$i] == ''  ? '' : $val[$i];
                                if(count($val) - $i > 1 ){
                                    $where .= " '".$val_where."' , ";
                                }else{
                                    $where .= " '".$val_where."'  ";
                                }  
                            }
                            $where .= ")";
                           
                        }
                        
                        
                    }
                
            }
            return $where;
        
        
    }
   
    public function filterheader($head){
        $headname = "";
        if($head == 'Month'){
            $headname =  "date_format(tanggal_faktur,'%m') as nama";
        }else if($head == 'distributor_code'){
            $headname = "CONCAT(CONCAT(distributor_code,'-'),distributor_name) AS nama";
        }else if($head == 'branch_code'){
            $headname = "CONCAT(CONCAT(branch_code,'-'),branch_name) AS nama";
        }else if($head == 'gpm_pm_code'){
            $headname = "CONCAT(CONCAT(gpm_pm_code,'-'),gpm_pm_code) AS nama";
        }else if($head == 'rsm_code'){
            $headname = "CONCAT(CONCAT(rsm_code,'-'),rsm_name) AS nama";
        }else if($head == 'shopper_code'){
            $headname = "CONCAT(CONCAT(shopper_code,'-'),shopper_name) AS nama";
        }else if($head == 'am_apm_asm_code'){
            $headname = "CONCAT(CONCAT(am_apm_asm_code,'-'),am_apm_asm_name) AS nama";
        }else if($head == 'msr_md_se_code'){
            $headname = "CONCAT(CONCAT(msr_md_se_code,'-'),msr_md_se_name) AS nama";
        }else if($head == 'segment'){
            $headname = "SEGMENT AS nama";
        }else if($head == 'customer_code'){
            $headname = "CONCAT(CONCAT(customer_code,'-'),customer_name) AS nama";
        }else if($head == 'layanan_code'){
            $headname = "CONCAT(CONCAT(layanan_code,'-'),layanan_name) AS nama";
        }else if($head == 'lini_code'){
            $headname = "CONCAT(CONCAT(lini_code,'-'),lini_name) AS nama";
        }else if($head == 'material_group1_code'){
            $headname = "CONCAT(CONCAT(material_group1_code,'-'),material_group1_name) AS nama";
        }else if($head == 'material_group2_code'){
            $headname = "CONCAT(CONCAT(material_group2_code,'-'),material_group2_name) AS nama";
        }else if($head == 'material_group3_code'){
            $headname = "CONCAT(CONCAT(material_group3_code,'-'),material_group3_name) AS nama";
        }else if($head == 'brand'){
            $headname = "brand AS nama";
        }else if($head == 'material_code'){
            $headname = " CONCAT(CONCAT(CONCAT(CONCAT(material_code,'-'),kemasan),'-'), material_name ) AS nama";
        }
      //  echo $headname;die();
        return $headname;

    }
    function groupbyheadfilter($header = NULL){
        $query = "";
        if(strlen($header['head_filter' ]) > 0  ){
            if($header['head_filter'] == 'Month'){
                $query .= " GROUP BY  date_format(tanggal_faktur,'%m') )x";
            }else{
                
                 $query .= " GROUP BY ".$header['head_filter'].")x" ;
            }
        }else{
            $query = ")x";
        }
        return $query;
    }
    
    
   

}



?>