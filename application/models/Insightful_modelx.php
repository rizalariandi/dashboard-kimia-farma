<?php
class Insightful_model extends CI_Model {
	
	private $table = 'usc_insight_target_realisasi';
    private $table_product = 'usc_insight_target_realisasi_produk';
	private $id    = 'cct_id';

	public function __construct()
	{
					$this->load->database();
	} 
	
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{
		return $this->db->query("SELECT SQL_CALC_FOUND_ROWS ct_name,ccs_sales,ccs_probability,ccs_step,cct_id FROM ".$this->table."   
								left join crm_cust_stepx on cct_id=ccs_id_trans
								left join crm_customersx on cct_id_customer=ct_id 
								 $param[where] $param[order] $param[limit]");
	}
	
	function dtfiltered()
	{
		$result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
		
		return $result->jumlah;
	}
	
	function dtcount()
	{
		return $this->db->count_all($this->table);
	}
	//---------------------------------------------------------------------------------------------------------------------

    function getYear()
    {
        return $this->db->query("SELECT tahun
            from insightful_kftd_produk
            group by tahun order by tahun");
    }
    function getCountDaerah($start,$end,$year,$lini,$layanan)
    {
        if($lini != 'ALL')
            $wlini = "AND lini_desc1 = '$lini'";
        else
            $wlini = '';

        if($layanan != 'all')
            $wlayanan = "AND layanan = '$layanan'";
        else
            $wlayanan = '';

        return $this->db->query("SELECT provinsi, sum(target_value) as target, sum(realisasi) as realisasi
            from usc_insight_target_realisasi where tahun = '$year' and (bulan BETWEEN '".(int) $start."' and '".(int) $end."') $wlini $wlayanan
            group by provinsi");
    }

    function getByAreaGroupMonth($warea,$start,$end,$year,$lini,$layanan)
    {
        // if($lini != 'all')
        //     $wlini = "AND lini_desc1 = '".strtoupper($lini)."'";
        // else
        //     $wlini = '';

        // if($layanan != 'all')
        //     $wlayanan = "AND layanan = '$layanan'";
        // else
        $wlayanan = '';

        if (is_array($lini)){
            $wlini = " AND (lower(lini_desc1) in ".str_replace("_"," ",strtolower("('".implode("','",$lini)."')")).")";
        } 
        else
        $wlini = '';

        return $this->db->query("SELECT bulan, sum(realisasi) as realisasi, 
                sum(realisasi_1) as realisasi_1
                from usc_insight_target_realisasi 
                where tahun = '$year' and (bulan BETWEEN '".(int) $start."' and '".(int) $end."') $wlini $wlayanan $warea 
                group by bulan
                order by bulan asc");
    }

    function getByKftdGroupMonth($area,$month,$year)
    {
        return $this->db->query("SELECT nama_cabang, sum(target_amount) as target, 
                sum(realisasi_total_penjualan) as realisasi, sum(stock) as stock
                from insightful_kftd_produk 
                where tahun = '$year' and bulanx='$month' and provinsi = '$area'
                group by nama_cabang
                order by realisasi desc
                limit 0, 5");
    }

    function getByLini($start,$end,$year,$lini,$layanan)
    {
        if($lini != 'all')
            $wlini = "AND a.lini_desc1 = '$lini'";
        else
            $wlini = '';

        if($layanan != 'all') {
            $wlayanan = "AND a.layanan = '$layanan'";
            $wlayanan_b = "AND b.layanan = '$layanan'";
        } else {
            $wlayanan = $wlayanan_b = '';
        }


        return $this->db->query("SELECT a.lini_desc1 as lini, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.lini_desc1 = a.lini_desc1 $wlayanan_b) as realisasi_past
                from usc_insight_target_realisasi a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') $wlini $wlayanan
                group by a.lini_desc1
                order by realisasi desc
                limit 0, 5");
    }

    function getByKftd($area, $start,$end,$year,$lini,$layanan)
    {
        if($lini != 'all') {
            $wlini = "AND a.lini_desc1 = '$lini'";
            $wlini_b = "AND b.lini_desc1 = '$lini'";
        } else {
            $wlini = $wlini_b = '';
        }

        if($layanan != 'all') {
            $wlayanan = "AND a.layanan = '$layanan'";
            $wlayanan_b = "AND b.layanan = '$layanan'";
        } else {
            $wlayanan = $wlayanan_b = '';
        }

        if(!empty($area)) {
            $warea = "AND a.provinsi IN (".implode(',', $area).")";
            $warea_b = "AND b.provinsi IN (".implode(',', $area).")";
        } else {
            $warea = $warea_b = '';
        }

        return $this->db->query("SELECT a.nama_kftd, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_kftd = a.nama_kftd $warea_b $wlini_b $wlayanan_b) as realisasi_past
                from usc_insight_target_realisasi a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') $warea $wlini $wlayanan
                group by a.nama_kftd
                order by realisasi desc
                limit 0, 5");
    }

    function getByLiniGroupBrand($lini,$start,$end,$year)
    {
        /*if($lini != 'all')
            $wlini = "AND kode_lini = '$lini'";
        else
            $wlini = '';

        if($layanan != 'all')
            $wlayanan = "AND kode_lini = '$layanan'";
        else
            $wlayanan = '';*/

        return $this->db->query("SELECT a.nama_brand, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.lini_desc1 = '$lini') as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and lini_desc1 = '$lini'
                group by a.nama_brand
                order by realisasi desc
                limit 0, 5");
    }

    function getByKftdGroupBrand($kftd,$start,$end,$year)
    {
        return $this->db->query("SELECT a.nama_brand, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.nama_kftd = '$kftd') as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and a.nama_kftd = '$kftd'
                group by a.nama_brand
                order by realisasi desc
                limit 0, 5");
    }

    function getByBrandGroupProduct($brand,$start,$end,$year)
    {
        return $this->db->query("SELECT a.nama_produk, sum(a.target_value) as target, 
                sum(a.realisasi) as realisasi, sum(a.value_stok) as stock,
                (select sum(b.realisasi) as rx from usc_insight_target_realisasi_produk b
                where b.tahun = '".($year-1)."' and (b.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and b.nama_brand = a.nama_brand and b.nama_produk = a.nama_produk) as realisasi_past
                from usc_insight_target_realisasi_produk a
                where a.tahun = '$year' and (a.bulan BETWEEN '".(int) $start."' and '".(int) $end."') and nama_brand = '$brand'
                group by a.nama_produk
                order by realisasi desc
                limit 0, 5");
    }

    function getByBrandGroupProductById($cabang,$brand,$month,$year)
    {  
        return $this->db->query("SELECT sum(realisasi_total_penjualan)realisasi,sum(target_amount)target
                from insightful_kftd_produk 
                where tahun = '$year' and bulanx='$month' and nama_produkx = '$brand' and nama_cabang='$cabang'");
    }

    function getBy_marketing_expenses($cabang,$brand,$month,$year)
    { 
        return $this->db->query("SELECT *
                from insightful_marketing_expenses 
                where tahun = '$year' and bulanx='$month' and nama_produk = '$brand' and nama_cabang = '$cabang' 
                order by nama_produk desc");
    }

    function get_realisasi_expenses($start,$end,$year,$lini)
    {
        if($lini == 'all')
            $wlini = '';
        else
            $wlini = "and lower(lini_desc1) = '$lini'";

        return $this->db->query("SELECT bulan, lini_desc1 as lini, sum(expenses) as expenses, sum(realisasi) as realisasi
                from usc_insight_realisasi_expenses 
                where tahun = '$year' and (bulan BETWEEN $start and $end) $wlini 
                group by bulan, lini_desc1
                order by bulan asc, lini_desc1 asc");
    }

    function get_lini_expanse()
    {
        return $this->db->query("SELECT lini_desc1 as lini
                from usc_insight_filter_lini 
                group by lini_desc1
                order by lini_desc1 asc");
    }

    function get_lini_target()
    {
        return $this->db->query("SELECT lini lini_desc1,lini 
        from usc_insight_filter_lini_2 
        group by lini
        order by lini asc");
    }
    function get_layanan_target()
    {
        return $this->db->query("SELECT layanan
                from usc_insight_target_realisasi 
                group by layanan
                order by layanan asc");
    }

    //============================================
    function dtquery_lini($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            lini_desc1 as lini, sum(target_value) AS target, sum(realisasi) AS realisasi, sum(value_stok) AS stock, sum(realisasi_1) AS realisasi_1, (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi)) as growth
            FROM ".$this->table."   
			$param[where] group by lini_desc1 $param[order] $param[limit]");
    }

    function dtfiltered_lini()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_lini()
    {
        return '1000';
    }
    //============================================
    function dtquery_kftd($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_kftd, sum(target_value) AS target, sum(realisasi) AS realisasi, sum(value_stok) AS stock, sum(realisasi_1) AS realisasi_1, (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi)) as growth
            FROM ".$this->table."   
			$param[where] group by nama_kftd $param[order] $param[limit]");
    }

    function dtfiltered_kftd()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_kftd()
    {
        return '1000';
    }
    //============================================
    function dtquery_brand($param)
    {
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_brand, sum(target_value) AS target, sum(realisasi) AS realisasi, sum(value_stok) AS stock, sum(realisasi_1) AS realisasi_1, 
                (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi)) as growth
            FROM ".$this->table_product."   
			$param[where] group by nama_brand $param[order] $param[limit]");
    }

    function dtfiltered_brand()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_brand()
    {
        return '1000';
    }
    //============================================
    function dtquery_prod($param)
    {  
        return $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
            nama_produk, sum(target_value) AS target, sum(realisasi) AS realisasi, sum(value_stok) AS stock, sum(realisasi_1) AS realisasi_1, 
                (sum(realisasi)/sum(target_value)) as achievement, (((sum(realisasi)-sum(realisasi_1)))/sum(realisasi)) as growth
            FROM ".$this->table_product."   
			$param[where] group by nama_produk $param[order] $param[limit]");
    }

    function dtfiltered_prod()
    {
        $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();

        return $result->jumlah;
    }

    function dtcount_prod()
    {
        return '1000';
    }
}
