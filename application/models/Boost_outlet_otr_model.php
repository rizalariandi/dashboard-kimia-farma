<?php
class Boost_outlet_otr_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_channels($table)
    {
        return $this->db->distinct()->select("CASE WHEN `channel` ='HOREKA' THEN 'Horeka' WHEN `channel` ='Rumah sakit' THEN 'Rumah Sakit' ELSE `channel` END as channel, CASE WHEN channel != 'Marketing' THEN 0 ELSE 1 END AS ordering")->order_by('ordering')->order_by('channel')->get($table)->result();

    }

    function get_tahun_otr($table)
    {
        $a = $this->db->distinct()->select("year(tanggal) as tahun")->get($table)->result();
        return $a;
    }

    function get_nama_brand($table)
    {
        $nama_brand = [];
        $brands = $this->db->distinct()->select('nama_brand')->order_by('nama_brand', 'ASC')->get($table)->result();
        foreach ($brands as $b) {
            $nama_brand[] = "'" . str_replace("'", "-", $b->nama_brand) . "'";
        }
        return implode(',', $nama_brand);
    }

    function get_list_product($nama_brand)
    {
        return $this->db->distinct()->select('nama_produk')->like('nama_brand', $nama_brand)->order_by('nama_produk', 'asc')->get('usc_bst_sales_outlet_channel')->result();
    }

    function get_last_update()
    {
        return $this->db->select('tanggal')->order_by('tanggal', 'DESC')->limit(1)->get('usc_bst_sales_outlet_channel')->result();
    }

    function get_brand_outlet($table)
    {
        // die($this->db->distinct()->select('nama_brand')->get($table));
        return $this->db->distinct()->select('nama_brand')->get($table)->result();
    }

    function get_lini_otr($table)
    {
        return $this->db->distinct()->select("lini")->get($table)->result();
    }

    function get_brand_otr($table)
    {
        return $this->db->distinct()->select('nama_brand')->get($table)->result();
    }

    function get_provinsi($table)
    {
        return $this->db->distinct()->select("provinsi")->get($table)->result();
    }

    function get_list_kftd($table)
    {
        return $this->db->distinct()->select('nama_kftd')->get($table)->result();
    }

    function get_brand($table, $lini)
    {
        $this->db->distinct()->select('nama_brand');
        if (is_array($lini)) {
            foreach ($lini as $l) {
                $this->db->or_like('lini', $l);
            }
        } else {
            $this->db->where('lini', $lini);
        }

        return $this->db->order_by('nama_brand', 'asc')->get($table)->result();
    }

    function outlet_otr($start = '', $end = '', $area = '', $channel = '')
    {
        $this->db->select("SUM(jumlah_outlet) as jumlah_outlet, nama_brand, year(tanggal) as tahun");
        $this->db->group_by(array("nama_brand", "year(tanggal)"));
        if (empty($start) && empty($end)) {
            $this->db->where("tanggal >=", '2018-01-01');
            $this->db->where("tanggal <=", '2019-12-31');
        } else {
            $this->db->where("tanggal >=", $start);
            $this->db->where("tanggal <=", $end);
        }
        if (empty($area) && empty($channel)) {
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (empty($area)) {
            $this->db->where_in('channel', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else if (empty($channel)) {
            $this->db->where_in('nama_kftd', $area);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        } else {
            $this->db->where_in('nama_kftd', $area);
            $this->db->where_in('channel', $channel);
            $data = $this->db->get('usc_bst_sales_transaksi_area')->result();
        }
        // die($this->db->last_query());
        return $data;
    }

    // tadinya -> function trend_outlet($area='', $channel='', $year )
    function trend_outlet($area = null, $channel = null, $year)
    {
        $trend_otr = array();
        $this->db->reconnect();
        $sql = "SELECT COUNT(DISTINCT(nama_customer)) as jumlah_outlet, MONTH(tanggal) as bulan FROM usc_bst_sales_outlet_channel WHERE YEAR(tanggal)=" . $year;
        if (!empty($area)) $sql .= "AND WHERE nama_kftd IN " . implode(',', $area);
        if (!empty($channel)) $sql .= "AND WHERE channel IN " . implode(',', $channel);
        $sql .= " GROUP BY MONTH(tanggal)";

        $query = $this->db->query($sql);

        foreach ($query->result_array() as $trend) {
            $trend_otr[] = $trend['jumlah_outlet'];
        }

        // die($this->db->last_query());
        return $trend_otr;
    }

    // start json for chart
    function json_outlet_otr($periode, $kftd, $channel, $lini, $brand)
    {
        // =================== initial code =========================
        // $kftd = $this->input->post("kftd", true);
        // $channel = $this->input->post("channel", true);
        // $lini = $this->input->post("lini", true);
        // $brand = $this->input->post("brand", true);
        // $periode = $this->input->post("periode", true);
        // ==========================================================

        $start = '2021'.'-'.'01'.'-01';
        $end = '2021'.'-'.'12'.'-31';
        if($periode == ""){
            $periode = $start." / ".$end ;
        }

        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $tahun_otr = $this->get_tahun_otr('usc_bst_sales_outlet_channel');
        // die($this->db->last_query());

        $this->db->select("count(distinct(nama_customer)) as jumlah_outlet, nama_brand, year(tanggal) as tahun");

        if (!empty($brand)) {
            $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
            $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

            $this->db->group_start();
            // ========== initial code ===========
            // $this->db->where("tanggal >=", $from);
            // $this->db->where("tanggal <=", $to);
            // $this->db->or_where("tanggal >=", $last_from);
            // $this->db->where("tanggal <=", $last_to);
            // ===================================

            $this->db->where("tanggal between '" . $from . "' and '" . $to . "'");
            $this->db->or_where("tanggal between '" . $last_from . "' and '" . $last_to . "'");
            $this->db->group_end();
        } else {
            $this->db->where("tanggal >=", $from);
            $this->db->where("tanggal <=", $to);
        }

        if (!empty($brand)) {
            $this->db->group_by(array("nama_brand", "year(tanggal)"));
        } else {
            $this->db->group_by(array("nama_brand"));
        }

        $this->db->order_by("jumlah_outlet", "DESC");

        if (!empty($kftd)) {
            $this->db->group_start();
            // =========== initial code ============
            // foreach ($kftd as $k) {
            //     $this->db->or_where("nama_kftd",$k);
            // }
            // =====================================

            $this->db->where_in("nama_kftd", $kftd);
            $this->db->group_end();
        }

        if (!empty($channel)) {
            $this->db->group_start();
            // ========== initial code ============
            // foreach ($channel as $c) {
            //     $this->db->or_where("channel", $c);
            // }
            // ====================================

            $this->db->where_in("channel", $channel);
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            // ======== initial code =========
            // foreach ($lini as $l) {
            //     $this->db->or_where("lini", $l);
            // }
            // ===============================
            $this->db->where_in("lini", $lini);
            $this->db->group_end();
        }

        if (!empty($brand)) {
            $this->db->group_start();
            // ========== initial code ==========
            // foreach ($brand as $b) {
            //     $this->db->or_where("nama_brand", $b);
            // }
            // ==================================

            $this->db->where_in("nama_brand", $brand);
            $this->db->group_end();
        } else {
            $this->db->limit(20);
        }

        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();
        // die($this->db->last_query());

        $json_data_otr = array();

        if (empty($brand)) {
            $json_data_otr[0]['name'] = 'Top 20 Brand';
            $jml_outlet = array();
            $json_nama_brand = array();
            foreach ($data as $otr) {
                $json_nama_brand[] = $otr->nama_brand;
                $jml_outlet[] = (int)$otr->jumlah_outlet;
            }
            $json_data_otr[0]['data'] = $jml_outlet;
            $json_data_otr[0]['color'] = "#4285F4";
        } else {
            // print_r($data);
            // die;
            $tmp_thn = [];

            $json_nama_brand = array();
            foreach ($tahun_otr as $key => $value) {
                $json_data_otr[$key]['name'] = $value->tahun;
                $jml_outlet = array();

                // ========= intial code =============
                // $json_nama_brand = array();
                // ===================================

                foreach ($data as $otr) {
                    if ($otr->tahun == $value->tahun) {
                        $json_nama_brand[] = $otr->nama_brand;
                        $jml_outlet[] = (int)$otr->jumlah_outlet;

                        // array_push($tmp_thn,[
                        //     "value_tahun"=>$value->tahun,
                        //     "otr_tahun"=>$otr->tahun,
                        //     "truth_val" => $otr->tahun == $value->tahun,
                        //     "brand" => $otr->nama_brand
                        // ]);
                    }
                }
                $json_data_otr[$key]['data'] = $jml_outlet;
                if ($key == 0) {
                    $json_data_otr[$key]['color'] =  "#A3D4EF";
                } else {
                    $json_data_otr[$key]['color'] = "#4285F4";
                }
            }

            // print_r($tmp_thn);
            // print_r($json_nama_brand);
            // die;
        }

        $json = array(
            'raw' => $data,
            'nama_brand' => $json_nama_brand,
            'data' => $json_data_otr
        );
        // die($this->db->last_query());
        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    function json_get_list_product()
    {
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);

        $start = date("Y").'-'.'01'.'-01';
        $end = date("Y").'-'.'12'.'-31';
        if($periode == ""){
            $periode = $start." / ".$end ;
        }
        
        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $dateFrom = DateTime::createFromFormat("Y-m-d", $from);
        $dateTo = DateTime::createFromFormat("Y-m-d", $to);
        $tahun_otr = $this->get_tahun_otr('usc_bst_sales_outlet_channel');
        // die($this->db->last_query());

        $this->db->select("count(distinct(nama_customer)) as jumlah_outlet, nama_produk, year(tanggal) as tahun");

        $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        $this->db->like('nama_brand', $brand);

        $this->db->group_start();
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        $this->db->or_where("tanggal >=", $last_from);
        $this->db->where("tanggal <=", $last_to);
        $this->db->group_end();

        $this->db->group_by(array("nama_produk", "year(tanggal)"));

        if (!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_where("nama_kftd", $k);
            }
            $this->db->group_end();
        }

        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_where("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            foreach ($lini as $l) {
                $this->db->or_where("lini", $l);
            }
            $this->db->group_end();
        }

        $this->db->order_by("jumlah_outlet", "DESC");

        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();
        // die($this->db->last_query());

        // $cate = $this->get_list_product($brand);

        $d = array();
        foreach ($tahun_otr as $key => $value) {
            $json_data_otr[$key]['name'] = $value->tahun;
            $jml_outlet = array();
            foreach ($data as $k => $otr) {
                if ($otr->tahun == $value->tahun) {
                    $np = str_replace(' ', '_', $otr->nama_produk);
                    $d[$np][$otr->tahun] = (int) $otr->jumlah_outlet;
                    $jml_outlet[] = (int) $otr->jumlah_outlet;
                }
            }
            $json_data_otr[$key]['data'] = $jml_outlet;
            $json_data_otr[$key]['color'] = '#' . random_color();
            $json_data_otr[$key]['type'] = "column";
            $json_data_otr[$key]['yAxis'] = 0;
        }

        $growth_data = array();
        foreach ($d as $key => $growth_tahun) {
            if (empty(array_values($growth_tahun)[0]))
                $i0 = 0;
            else
                $i0 = array_values($growth_tahun)[0];

            if (empty(array_values($growth_tahun)[1]))
                $i1 = 0;
            else
                $i1 = array_values($growth_tahun)[1];

            $growth_data[] = (int) $i1 - (int) $i0;
        }

        $key++;
        $json_data_growth[$key]['data'] = $growth_data;
        $json_data_growth[$key]['color'] = '#' . random_color();
        $json_data_growth[$key]['type'] = "spline";
        $json_data_growth[$key]['yAxis'] = 1;
        $json_data_growth[$key]['name'] = 'Growth';
        $json_data_growth[$key]['showInLegend'] = false;

        $json_data2 = [];
        foreach ($json_data_otr as $otr) {
            $json_data2[] = $otr;
        }
        foreach ($json_data_growth as $gr) {
            $json_data2[] = $gr;
        }

        $json_data = array(
            'raw' => $data,
            'data' => $json_data2,
            'categories' => $this->get_list_product($brand)
        );
        // die($this->db->last_query());

        echo json_encode($json_data);
    }

    function json_outlet_trend()
    {
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);
        $start = date("Y") . '-' . '01' . '-01';
        $end = date("Y") . '-' . '12' . '-31';
        if ($periode == "") {
            $periode = $start . " / " . $end;
        }
        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $tahun_otr = $this->get_tahun_otr('usc_bst_sales_outlet_channel');
        // $query_tahun = $this->db->last_query();

        $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        $this->db->select("COUNT(DISTINCT(nama_customer)) as jumlah_outlet, month(tanggal) as bulan, year(tanggal) as tahun");

        $this->db->group_start();
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        $this->db->group_end();

        $this->db->or_group_start();
        // $this->db->or_where("tanggal >=", $last_from);
        $this->db->where("tanggal >=", $last_from);
        $this->db->where("tanggal <=", $last_to);
        $this->db->group_end();
        $this->db->group_by(array("month(tanggal)", "year(tanggal)"));

        if (!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_where("nama_kftd", $k);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            foreach ($lini as $l) {
                $this->db->or_where("lini", $l);
            }
            $this->db->group_end();
        }

        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_where("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_where("nama_brand", $b);
            }
            $this->db->group_end();
        }

        $data = $this->db->get('usc_bst_sales_outlet_channel')->result();
        // $query_data = ($this->db->last_query());

        // get bar chart value
        $this->db->select("SUM(revenue) as revenue, month(tanggal) as bulan, year(tanggal) as tahun");

        if (!empty($kftd)) {
            $this->db->group_start();
            foreach ($kftd as $k) {
                $this->db->or_where("nama_kftd", $k);
            }
            $this->db->group_end();
        }

        if (!empty($channel)) {
            $this->db->group_start();
            foreach ($channel as $c) {
                $this->db->or_where("channel", $c);
            }
            $this->db->group_end();
        }

        if (!empty($lini)) {
            $this->db->group_start();
            foreach ($lini as $l) {
                $this->db->or_where("lini", $l);
            }
            $this->db->group_end();
        }

        if (!empty($brand)) {
            $this->db->group_start();
            foreach ($brand as $b) {
                $this->db->or_where("nama_brand", $b);
            }
            $this->db->group_end();
        }

        // $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        // $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        $this->db->group_start();
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        $this->db->group_end();

        $this->db->or_group_start();
        // $this->db->or_where("tanggal >=", $last_from);
        $this->db->where("tanggal >=", $last_from);
        $this->db->where("tanggal <=", $last_to);
        $this->db->group_end();

        $this->db->group_by(array("month(tanggal)", "year(tanggal)"));

        $batang = $this->db->get('usc_bst_sales_area_brand')->result();
        $batang_query = $this->db->last_query();
        // die($batang);
        //end get bar chart value 

        $json_trend_otr = array();
        $json_realisasi_otr = array();
        $json_bulan = array();
        $json = array();

        foreach ($tahun_otr as $key => $value) {
            $json_realisasi_otr[$key]['name'] = 'Value tahun ' . $value->tahun;
            $value_otr = array();
            foreach ($batang as $k => $val) {
                if ($val->tahun == $value->tahun) {
                    $value_otr[] = $val->revenue;
                }
            }
            $json_realisasi_otr[$key]['data'] = $value_otr;
            $json_realisasi_otr[$key]['type'] = 'column';
            $json_realisasi_otr[$key]['yAxis'] = 1;
            $json_realisasi_otr[$key]['showInLegend'] = false;
        }

        foreach ($tahun_otr as $key => $value) {
            $json_trend_otr[$key]['name'] = 'OTR tahun ' . $value->tahun;

            if ($key == 0) {
                $json_trend_otr[$key]['color'] = '#F04040';
            } else {
                $json_trend_otr[$key]['color'] = '#1F90FA';
            }
            $trend_otr = array();
            foreach ($data as $p => $otr) {
                if ($otr->tahun == $value->tahun) {
                    $trend_otr[$p]['y'] = (int)$otr->jumlah_outlet;
                    if (in_array(date('F', mktime(0, 0, 0, $otr->bulan, 10)), $json_bulan)) {
                        continue;
                    } else {
                        $json_bulan[] = date('F', mktime(0, 0, 0, $otr->bulan, 10));
                    }
                }
            }
            $batang_otr = array();
            foreach ($batang as $k => $b) {
                if ($b->tahun == $value->tahun) {
                    $trend_otr[$k]['value'] =  $b->revenue;
                }
            }
            $json_trend_otr[$key]['data'] = array_values($trend_otr);
            $json_trend_otr[$key]['yAxis'] = 0;
            $json_trend_otr[$key]['showInLegend'] = false;
        }

        foreach ($json_trend_otr as $otr) {
            $json['data'][] = $otr;
        }
        foreach ($json_realisasi_otr as $val) {
            $json['data'][] = $val;
        }
        $json['bulan'] = $json_bulan;

        // die($this->db->last_query());
        // die($batang);
        return json_encode($json, JSON_NUMERIC_CHECK);
    }

    // end json for chart

    // start json for datatable

    // json for datatable outlet transaction by quantity
    function json_outlet_transaction_quantity()
    {
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);

        $data_kftd = explode(",", $kftd);
        $data_channel = explode(",", $channel);
        $data_lini = explode(",", $lini);
        $data_brand = explode(",", $brand);


        $start = date("Y") . '-' . '01' . '-01';
        $end = date("Y") . '-' . '12' . '-31';
        if ($periode == "") {
            $periode = $start . " / " . $end;
        }

        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        for ($m = 1; $m <= 12; $m++) {
            $this->kf->select('SUM(IF( month(tanggal) = "' . $m . '" , quantity, 0) ) AS "' . date('M', mktime(0, 0, 0, $m, 10)) . '-quantity",SUM(IF( month(tanggal) = "' . $m . '" , revenue, 0) ) AS "' . date('M', mktime(0, 0, 0, $m, 10)) . '-revenue"');
        }

        $this->kf->select("nama_brand");
        $this->kf->from('usc_bst_sales_trans_outlet');

        $this->kf->group_start();
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $this->kf->or_where('tanggal >=', $last_from);
        $this->kf->where('tanggal <=', $last_to);
        $this->kf->group_end();
        $this->kf->group_by("nama_brand");

        if (!empty($kftd)) {
            $this->kf->group_start();
            foreach ($data_kftd as $k) {
                $this->kf->or_where('nama_kftd', $k);
            }
            $this->kf->group_end();
        }

        if (!empty($channel)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();
        }

        if (!empty($lini)) {
            $this->kf->group_start();
            foreach ($data_lini as $l) {
                $this->kf->or_where('lini', $l);
            }
            $this->kf->group_end();
        }

        if (!empty($brand)) {
            $this->kf->group_start();
            foreach ($data_brand as $b) {
                $this->kf->or_where('nama_brand', $b);
            }
            $this->kf->group_end();
        }

        $a = $this->kf->generate();
        // die($this->kf->last_query());
        return $a;
    }

    function json_outlet_transaction_brand()
    {
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);

        $data_kftd = explode(",", $kftd);
        $data_channel = explode(",", $channel);
        $data_lini = explode(",", $lini);
        $data_brand = explode(",", $brand);

        $start = date("Y") . '-' . '01' . '-01';
        $end = date("Y") . '-' . '12' . '-31';
        if ($periode == "") {
            $periode = $start . " / " . $end;
        }

        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        $channels = $this->get_channels('usc_bst_sales_outlet');
        // die($this->db->last_query());

        $tahun =  $this->db->distinct()->select("year(tanggal) as tahun")->order_by('tahun', 'DESC')->limit(2)->get('usc_bst_sales_outlet')->result();
        // die($this->db->last_query());

        foreach ($channels as $ch) {
            foreach ($tahun as $th) {
                $this->kf->select('CASE WHEN COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $th->tahun . ', nama_customer, 0) ) ) = 1 THEN 0 ELSE COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $th->tahun . ' , nama_customer, 0) ) ) - 1  END AS "' . $ch->channel . '-' . $th->tahun . '"');
            }
        }
        $this->kf->select("nama_brand");
        $this->kf->from('usc_bst_sales_outlet');

        $this->kf->group_start();
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $this->kf->or_where('tanggal >=', $last_from);
        $this->kf->where('tanggal <=', $last_to);
        $this->kf->group_end();

        if (!empty($kftd)) {
            $this->kf->group_start();
            foreach ($data_kftd as $a) {
                $this->kf->or_where('nama_kftd', $a);
            }
            $this->kf->group_end();
        }

        if (!empty($channel)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();
        }

        if (!empty($lini)) {
            $this->kf->group_start();
            foreach ($data_lini as $l) {
                $this->kf->or_where('lini', $l);
            }
            $this->kf->group_end();
        }

        if (!empty($brand)) {
            $this->kf->group_start();
            foreach ($data_brand as $b) {
                $this->kf->or_where('nama_brand', $b);
            }
            $this->kf->group_end();
        }

        $this->kf->group_by("nama_brand");

        $a = $this->kf->generate();
        // die($a);
        return $a;
    }

    function json_outlet_transaction_area()
    {
        $kftd = $this->input->post("kftd", true);
        $channel = $this->input->post("channel", true);
        $lini = $this->input->post("lini", true);
        $brand = $this->input->post("brand", true);
        $periode = $this->input->post("periode", true);

        $data_kftd = explode(",", $kftd);
        $data_channel = explode(",", $channel);
        $data_lini = explode(",", $lini);
        $data_brand = explode(",", $brand);

        $start = date("Y") . '-' . '01' . '-01';
        $end = date("Y") . '-' . '12' . '-31';
        if ($periode == "") {
            $periode = $start . " / " . $end;
        }

        $tanggal = str_replace(" ", "", $periode);
        $tanggal = str_replace("/", ",", $tanggal);
        $tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];

        $last_from = date("Y-m-d", strtotime("-1 year", strtotime($from)));
        $last_to = date("Y-m-d", strtotime("-1 year", strtotime($to)));

        $channels = $this->get_channels('usc_bst_sales_outlet');
        // die($this->db->last_query());

        $tahun =  $this->db->distinct()->select("year(tanggal) as tahun")->order_by('tahun', 'DESC')->limit(2)->get('usc_bst_sales_outlet')->result();
        // die($this->db->last_query());

        foreach ($channels as $ch) {
            foreach ($tahun as $th) {
                $this->kf->select('CASE WHEN COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $th->tahun . ', nama_customer, 0) ) ) = 1 THEN 0 ELSE COUNT( DISTINCT( IF( channel = "' . $ch->channel . '" AND YEAR(tanggal) = ' . $th->tahun . ', nama_customer, 0) ) ) - 1  END AS "' . $ch->channel . '-' . $th->tahun . '"');
            }
        }
        $this->kf->select("nama_kftd");
        $this->kf->from('usc_bst_sales_outlet');

        $this->kf->group_start();
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $this->kf->or_where('tanggal >=', $last_from);
        $this->kf->where('tanggal <=', $last_to);
        $this->kf->group_end();

        if (!empty($kftd)) {
            $this->kf->group_start();
            foreach ($data_kftd as $k) {
                $this->kf->or_where('nama_kftd', $k);
            }
            $this->kf->group_end();
        }

        if (!empty($channel)) {
            $this->kf->group_start();
            foreach ($data_channel as $c) {
                $this->kf->or_where('channel', $c);
            }
            $this->kf->group_end();
        }

        if (!empty($lini)) {
            $this->kf->group_start();
            foreach ($data_lini as $l) {
                $this->kf->or_where('lini', $l);
            }
            $this->kf->group_end();
        }

        if (!empty($brand)) {
            $this->kf->group_start();
            foreach ($data_brand as $b) {
                $this->kf->or_where('nama_brand', $b);
            }
            $this->kf->group_end();
        }

        $this->kf->group_by("nama_kftd");

        $a = $this->kf->generate();
        // die($this->db->last_query());
        return $a;
    }
    // end json for datatable

}
