<?php
class Kf_import_apl_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}


	public function getDataAPL()
	{
		$query = $this->db->query("SELECT * FROM data_apl");

		return $query->result_array();
	}

	public function InsertDataAPL($datas)
	{
		$json =  json_decode($datas, true);
		foreach ($json as &$value) {			
			$query = "INSERT INTO data_apl (principal_year, period_no, principal, business_division_1, business_division_2, invoice_no, zp_item_code, sap, item_name, lini, branch, cust_group_1_code, channel, customer_type, customer_code, customer_name, list_price_a, sales_unit, net_sales, discount_principal, sales_value, bonus_value, bonus_unit, transaction_date) VALUES(" . $this->db->escape($value["principal_year"]). "," . $this->db->escape($value["period_no"]). "," .$this->db->escape($value["principal"]). "," .$this->db->escape($value["business_division_1"]). "," .$this->db->escape($value["business_division_2"]). "," .$this->db->escape($value["invoice_no"]). "," .$this->db->escape($value["zp_item_code"]). "," .$this->db->escape($value["sap"]). "," .$this->db->escape($value["item_name"]). "," .$this->db->escape($value["lini"]). "," .$this->db->escape($value["branch"]). "," .$this->db->escape($value["cust_group_1_code"]). "," .$this->db->escape($value["channel"]). "," .$this->db->escape($value["customer_type"]). "," .$this->db->escape($value["customer_code"]). "," .$this->db->escape($value["customer_name"]). "," .$this->db->escape($value["list_price_a"]). "," .$this->db->escape($value["sales_unit"]). "," .$this->db->escape($value["net_sales"]). "," .$this->db->escape($value["discount_principal"]). "," .$this->db->escape($value["sales_value"]). "," .$this->db->escape($value["bonus_value"]). "," .$this->db->escape($value["bonus_unit"]). "," .$this->db->escape($value["transaction_date"]). ");";
			$this->db->query($query);
		}
		$return_result = "Insert successful";
		return $return_result;
	}

	public function UpdateDataAPL($datas)
	{
		$value =  json_decode($datas, true);
		$query = "UPDATE data_apl SET principal_year=" . $this->db->escape($value["principal_year"]) 
		. ", period_no=". $this->db->escape($value["period_no"]).", principal=". $this->db->escape($value["principal"])
		.", business_division_1=".$this->db->escape($value["business_division_1"]).", business_division_2="
		. $this->db->escape($value["business_division_2"]).", invoice_no=". $this->db->escape($value["invoice_no"]).", zp_item_code="
		. $this->db->escape($value["zp_item_code"]).", sap=". $this->db->escape($value["sap"]).", item_name="
		. $this->db->escape($value["item_name"]).", lini=". $this->db->escape($value["lini"]).", branch="
		. $this->db->escape($value["branch"]).", cust_group_1_code=". $this->db->escape($value["cust_group_1_code"])
		.", channel=". $this->db->escape($value["channel"]).", customer_type=". $this->db->escape($value["customer_type"])
		.", customer_code=". $this->db->escape($value["customer_code"]).", customer_name=". $this->db->escape($value["customer_name"])
		.", list_price_a=". $this->db->escape($value["list_price_a"]).", sales_unit=". $this->db->escape($value["sales_unit"])
		.", net_sales=". $this->db->escape($value["net_sales"]).", discount_principal=". $this->db->escape($value["discount_principal"])
		.", sales_value=". $this->db->escape($value["sales_value"]).", bonus_value=". $this->db->escape($value["bonus_value"])
		.", bonus_unit=". $this->db->escape($value["bonus_unit"]).", transaction_date=". $this->db->escape($value["transaction_date"]).";";
		$this->db->query($query);
		$return_result = "Insert successful";
		return $return_result;
	}

	public function DeleteAPL($id)
	{
		$query = "DELETE FROM data_apl  WHERE Id =" . $this->db->escape($id);
		$this->db->query($query);
		$return_result = "Delete successful";
		return $return_result;
	}

	// ----------------------------------------------------------------------------------------------------------------------------

	public function getDataKodeCustomer()
	{
		$query = $this->db->query("SELECT * FROM data_kode_customer");

		return $query->result_array();
	}

	public function InsertDataKodeCustomer($datas)
	{
		$json =  json_decode($datas, true);
		foreach ($json as &$value) {
			$query = "INSERT INTO data_kode_customer (kode_customer) VALUES(" . $this->db->escape($value["kode_customer"]) . ");";
			$this->db->query($query);
		}
		$return_result = "Insert successful";
		return $return_result;
	}

	public function DeleteKodeCustomer($id)
	{
		$query = "DELETE FROM data_kode_customer  WHERE Id =" . $this->db->escape($id);
		$this->db->query($query);
		$return_result = "Delete successful";
		return $return_result;
	}

	public function UpdateKodeCustomer($datas)
	{
		$value =  json_decode($datas, true);
		$query = "UPDATE data_kode_customer SET kode_customer=" . $this->db->escape($value["kode_customer"])."WHERE id=" . $this->db->escape($value["id"]);
		$this->db->query($query);
		$return_result = "Update successful";
		return $return_result;
	}
}
