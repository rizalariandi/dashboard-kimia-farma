<?php
class Overview_marketing extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function distributor_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{						
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');		
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');										
		$map = $map ? 'and plant_province_name =' . json_encode($map) : '';
		$query = $this->db->query("select ((select SUM($value) from datamart_kf.stagging_final_all_finish where 
		bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map
		and  distributor_code ='KFTD')/ ( select SUM($value) from datamart_kf.stagging_final_all_finish where 
		bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map)) *100 as sum
		");
		return array_values($query->result_array())[0]['sum'];
	}

	public function other_kftd($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$query = $this->db->query("select ((select SUM($value) from datamart_kf.stagging_final_all_finish where 
		bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map
		and  distributor_code !='KFTD')/ ( select SUM($value) from datamart_kf.stagging_final_all_finish where 
		bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map)) *100 as sum
		");

		return array_values($query->result_array())[0]['sum'];
	}
	public function MTD($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'HNA-PTD' || $value == 'HJD' ? 'target_nett' : 'target_hna';

		// ================== initial query ===================
		// $query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $end $mtd $ytd $lini $segment $brand $map");
		// $result['MTD'] = array_values($query->result_array())[0]['sum'];
		// $query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $Lend $mtd $ytd $lini $segment $brand $map");
		// $result['LMTD'] = array_values($query->result_array())[0]['sum'];
		// $query = $this->db->query("select SUM($target) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $end $mtd $ytd $lini $segment $brand $map");
		// $result['TMTD'] = array_values($query->result_array())[0]['sum'];
		// $query = $this->db->query("select (select ((select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $end $mtd $ytd $lini $segment $brand $map) - (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $Lend $mtd $ytd $lini $segment $brand $map)) /
		// (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $Lend $mtd $ytd $lini $segment $brand $map)) * 100 as sum");
		// $result['GMTD'] = array_values($query->result_array())[0]['sum'];
		// $query = $this->db->query("select (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $end $mtd $ytd $lini $segment $brand $map)/
		// (select SUM($target) from datamart_kf.stagging_final_all_finish where bulan_faktur_final = $end $mtd $ytd $lini $segment $brand $map) * 100 as sum ");
		// $result['AMTD'] = array_values($query->result_array())[0]['sum'];
		// ====================================================

		$query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map");
		$result['MTD'] = array_values($query->result_array())[0]['sum'];
		// $a1 = $this->db->last_query();

		$query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map");
		$result['LMTD'] = array_values($query->result_array())[0]['sum'];
		// $a2 = $this->db->last_query();
		
		$query = $this->db->query("select SUM($target) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map");
		$result['TMTD'] = array_values($query->result_array())[0]['sum'];
		// $a3 = $this->db->last_query();

		$query = $this->db->query("select (select ((select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map) - (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map)) /
		(select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map)) * 100 as sum");
		$result['GMTD'] = array_values($query->result_array())[0]['sum'];
		// $a4 = $this->db->last_query();

		$query = $this->db->query("select (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map)/
		(select SUM($target) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map) * 100 as sum ");
		$result['AMTD'] = array_values($query->result_array())[0]['sum'];
		// $a5 = $this->db->last_query();

		$tmp = "";
		return $result;
	}
	public function YTD($value, $start, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';

		$query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map");
		$result['YTD'] = array_values($query->result_array())[0]['sum'];
		// $a1 = $this->db->last_query();

		$query = $this->db->query("select SUM($value) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map");
		$result['LYTD'] = array_values($query->result_array())[0]['sum'];
		// $a2 = $this->db->last_query();

		$query = $this->db->query("select SUM($target) as sum from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map");
		$result['TYTD'] = array_values($query->result_array())[0]['sum'];
		// $a3 = $this->db->last_query();

		$query = $this->db->query("select (select ((select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map) - (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map)) /
		(select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and bulan_faktur_final <= $Lend $mtd $ytd $lini $segment $brand $map)) * 100 as sum");
		$result['GYTD'] = array_values($query->result_array())[0]['sum'];
		// $a4 = $this->db->last_query();

		$query = $this->db->query("select (select SUM($value) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map)/
		(select SUM($target) from datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map) * 100 as sum");
		$result['AYTD'] = array_values($query->result_array())[0]['sum'];
		// $a5 = $this->db->last_query();

		$tmp = "";
		return $result;
	}
	public function marketing_expense($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$query = $this->db->query("select (select SUM(total_expense) as sum from datamart_kf.exe_db1_expense where bulan_faktur = $end) / (select SUM(total_budget) as sum from datamart_kf.exe_db1_expense where bulan_faktur = $end) * 100 as sum ");
		$result['MTD'] = array_values($query->result_array())[0]['sum'];
		$query = $this->db->query("select (select SUM(total_expense) as sum from datamart_kf.exe_db1_expense where bulan_faktur >= $star and bulan_faktur <= $end)/(select SUM(total_budget) as sum from datamart_kf.exe_db1_expense where bulan_faktur >= $star and bulan_faktur <= $end ) * 100 as sum");
		$result['YTD'] = array_values($query->result_array())[0]['sum'];

		return $result;
	}
	public function sales_contribution($value, $start, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_name is null' : ($segment ? 'and segment_name =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$query = $this->db->query("select ((select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit ='KF' $mtd $ytd $lini $segment $brand $map) / ((select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit ='KF' $mtd $ytd $lini $segment $brand $map) +
		(select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit !='KF' $mtd $ytd $lini $segment $brand $map))) * 100 as sum");
		$result['kf'] = array_values($query->result_array())[0]['sum'];
		$query = $this->db->query("select ((select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit !='KF' $mtd $ytd $lini $segment $brand $map) / ((select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit ='KF' $mtd $ytd $lini $segment $brand $map) +
		(select SUM($value) from datamart_kf.exe_db1_unit1 where bulan_faktur >= $start and bulan_faktur <= $end and unit !='KF' $mtd $ytd $lini $segment $brand $map))) * 100 as sum");
		$result['non_kf'] = array_values($query->result_array())[0]['sum'];

		return $result;
	}
	public function achievement_distribution($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$query = $this->db->query("select A.province_A , CASE	   
		WHEN A.plant_province_code = '11' THEN	'id-jk'
		WHEN A.plant_province_code = '13' THEN	'id-jr'
		WHEN A.plant_province_code = '14' THEN	'id-jt'
		WHEN A.plant_province_code = '16' THEN	'id-ji'
		WHEN A.plant_province_code = '1' THEN	'id-ac'
		WHEN A.plant_province_code = '2' THEN	'id-su'
		WHEN A.plant_province_code = '3' THEN	'id-sb'
		WHEN A.plant_province_code = '4' THEN	'id-ri'
		WHEN A.plant_province_code = '5' THEN	'id-kr'
		WHEN A.plant_province_code = '6' THEN	'id-ja'
		WHEN A.plant_province_code = '7' THEN	'id-sl'
		WHEN A.plant_province_code = '8' THEN	'id-bb'
		WHEN A.plant_province_code = '9' THEN	'id-be'
		WHEN A.plant_province_code = '10' THEN  'id-1024'
		WHEN A.plant_province_code = '12' THEN	'id-bt'
		WHEN A.plant_province_code = '15' THEN	'id-yo'
		WHEN A.plant_province_code = '17' THEN	'id-ba'
		WHEN A.plant_province_code = '18' THEN	'id-nb'
		WHEN A.plant_province_code = '19' THEN	'id-nt'
		WHEN A.plant_province_code = '20' THEN	'id-kb'
		WHEN A.plant_province_code = '21' THEN	'id-kt'
		WHEN A.plant_province_code = '22' THEN	'id-ks'
		WHEN A.plant_province_code = '23' THEN	'id-ki'
		WHEN A.plant_province_code = '24' THEN	'id-ku'
		WHEN A.plant_province_code = '25' THEN	'id-sw'
		WHEN A.plant_province_code = '26' THEN	'id-sr'
		WHEN A.plant_province_code = '28' THEN	'id-st'
		WHEN A.plant_province_code = '29' THEN	'id-sg'
		WHEN A.plant_province_code = '30' THEN	'id-go'
		WHEN A.plant_province_code = '31' THEN	'id-ma'
		WHEN A.plant_province_code = '32' THEN	'id-la'
		WHEN A.plant_province_code = '33' THEN	'id-ib'
		WHEN A.plant_province_code = '34' THEN	'id-pa'
	   END as id_map ,
	   A.ty/ A.target * 100 as Achievement
		from (select plant_province_name province_A, plant_province_code, sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		bulan_faktur_final  <= $end $mtd $ytd $lini $segment $brand $map group by plant_province_code) as A		
		order by A.ty DESC;
		");
		// INNER join (select plant_province_name province_B, plant_province_code,
		// sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		// bulan_faktur_final  <= $Lend $mtd $ytd $lini $segment $brand $map group by plant_province_code) as B
		// on A.plant_province_code = B.plant_province_code

		return $query->result_array();
	}
	public function sales_branch_mtd($value, $Cend, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$by = $by == 'sales_this_year' ? 'ty' : ($by == 'sales_last_year' ? 'ly' : ($by == 'target' ? 'target' : ($by == 'growth' ? 'growth' : 'achievement')));
		
		$query = $this->db->query("select A.branch_A, 
		case when A.ty > 0 then A.ty else 0 end ty , case when B.ly > 0 then B.ly else 0 end ly,
		 (A.ty - B.ly)/B.ly *100 as Growth,A.target, (A.ty/ A.target) * 100 as Achievement
		from (select branch_name branch_A,sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish where
		bulan_faktur_final  = $Cend $mtd $ytd $lini $segment $brand $map and branch_name is not null group by branch_name) as A
		left join (select branch_name branch_B,
		sum($value) as ly FROM datamart_kf.stagging_final_all_finish where  
		bulan_faktur_final  = $Lend $mtd $ytd $lini $segment $brand $map and branch_name is not null group by branch_name) as B
		on A.branch_A = B.branch_B
		order by $by $order");

		// $a1 = $this->db->last_query();
		return $query->result_array();
	}
	public function sales_branch_ytd($value, $star, $Cend, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $order, $by, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$by = $by == 'sales_this_year' ? 'ty' : ($by == 'sales_last_year' ? 'ly' : ($by == 'target' ? 'target' : ($by == 'growth' ? 'growth' : 'achievement')));

		// $query = $this->db->query("select A.branch_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as growth,A.target, (A.ty/ A.target) * 100 as achievement
		// from (select branch_name branch_A,sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		// bulan_faktur_final  <= $end $mtd $ytd $lini $segment $brand $map group by branch_name) as A
		// left join (select branch_name branch_B,
		// sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		// bulan_faktur_final  <= $Lend $mtd $ytd $lini $segment $brand $map group by branch_name) as B
		// on A.branch_A = B.branch_B
		// order by $by $order");

		$query = $this->db->query("select A.branch_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as growth,A.target, (A.ty/ A.target) * 100 as achievement
		from (select branch_name branch_A,sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		bulan_faktur_final  <= $Cend $mtd $ytd $lini $segment $brand $map and branch_name is not null group by branch_name) as A
		left join (select branch_name branch_B,
		sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		bulan_faktur_final  <= $Lend $mtd $ytd $lini $segment $brand $map and branch_name is not null group by branch_name) as B
		on A.branch_A = B.branch_B
		order by $by $order");

		// $a = $this->db->last_query();
		return $query->result_array();
	}
	public function sales_lini($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$query = $this->db->query("select A.lini_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as Growth,A.target, (A.ty/ A.target) * 100 as Achievement
		from (select lini_name lini_A,sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish 
		where bulan_faktur_final >= $star and bulan_faktur_final  <= $end 
		AND lini_name is not null
		AND lini_name != 'JANSSEN'
		$mtd $ytd $lini $segment $brand $map group by lini_name) as A
		left join (select lini_name lini_B,
		sum($value) as ly FROM datamart_kf.stagging_final_all_finish 
		where bulan_faktur_final >= $Lstart and bulan_faktur_final  <= $Lend 
		AND lini_name is not null
		AND lini_name != 'JANSSEN'
		$mtd $ytd $lini $segment $brand $map group by lini_name) as B
		on A.lini_A = B.lini_B
		order by A.ty DESC;");

		return $query->result_array();
	}
	public function trend_sales($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$query = $this->db->query("select A.bulan_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as Growth,A.target, (A.ty/ A.target) * 100 as Achievement
		from (select SUBSTRING_INDEX(bulan_tahun_name , ' ', 1) as bulan_A,sum($value) as ty, sum($target) as target,bulan_faktur_final  FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		bulan_faktur_final  <= $end $mtd $ytd $lini $segment $brand $map group by bulan_tahun_name) as A
		left join (select SUBSTRING_INDEX(bulan_tahun_name , ' ', 1) as bulan_B,
		sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		bulan_faktur_final  <= $Lend $mtd $ytd $lini $segment $brand $map group by bulan_tahun_name) as B
		on A.bulan_A = B.bulan_B
		order by A.bulan_faktur_final ASC;
		");

		return $query->result_array();
	}
	public function sales_segment($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$query = $this->db->query("select A.segment_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as Growth
		from (select segment_final segment_A,sum($value) as ty FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		bulan_faktur_final  <= $end 
		and segment_final is not null
		$mtd $ytd $lini $segment $brand $map group by segment_final) as A
		left join (select segment_final segment_B,sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		bulan_faktur_final  <= $Lend 
		and segment_final is not null
		$mtd $ytd $lini $segment $brand $map group by segment_final) as B
		on A.segment_A = B.segment_B where A.segment_A != 'Apotek'
		order by A.ty DESC 				
		");

		return $query->result_array();
	}
	public function top_brand($value, $star, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$target = $value == 'hna_ptd_ty' || $value == 'hjd_ty' ? 'target_nett' : 'target_hna';
		$query = $this->db->query("select A.brand_A, A.ty, B.ly, (A.ty - B.ly)/B.ly *100 as Growth,A.target, (A.ty/ A.target) * 100 as Achievement
		from (select CASE
			WHEN material_brand is null THEN 'Null'
			ELSE material_brand 
		END AS brand_A,sum($value) as ty, sum($target) as target  FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $star and
		bulan_faktur_final  <= $end $mtd $ytd $lini $segment $brand $map group by material_brand) as A
		left join (select CASE
			WHEN material_brand is null THEN 'Null'
			ELSE material_brand 
		END AS brand_B,sum($value) as ly FROM datamart_kf.stagging_final_all_finish where bulan_faktur_final >= $Lstart and 
		bulan_faktur_final  <= $Lend $mtd $ytd $lini $segment $brand $map group by material_brand) as B
		on A.brand_A = B.brand_B
		order by A.ty DESC 
		limit 10;");

		return $query->result_array();
	}
	public function sales_product($value, $start, $end, $Lstart, $Lend, $mtd, $ytd, $lini, $segment, $brand, $map)
	{		
		$mtd = $mtd === 'null' ? 'and branch_name is null' : ($mtd ? 'and branch_name =' . json_encode($mtd) : '');		
		$ytd = $ytd === 'null' ? 'and branch_name is null' : ($ytd ? 'and branch_name =' . json_encode($ytd) : '');		
		$lini = $lini === 'null' ? 'and lini_name is null' : ($lini ? 'and lini_name =' . json_encode($lini) : '');		
		$segment = $segment === 'null' ? 'and segment_final is null' : ($segment ? 'and segment_final =' . json_encode($segment) : '');
		$brand = strtolower($brand) === 'null' ? 'and material_brand is null' : ($brand ? 'and material_brand =' . json_encode($brand) : '');		
		$map = $map ? 'and plant_province_name=' . json_encode($map) : '';
		$query = $this->db->query("select material_brand ,qty_ty, COALESCE(sum($value),0) as value from datamart_kf.stagging_final_all_finish 
		where bulan_faktur_final >= $start and bulan_faktur_final <= $end $mtd $ytd $lini $segment $brand $map and material_brand is not null
		GROUP by material_brand");

		// die($this->db->last_query());

		return $query->result_array();
	}

	public function detail_segment($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer, $material_brand, $material_name)
	{
		$branch = $branch ? 'and branch_name =' . json_encode($branch) : '';
		$layanan_group = $layanan_group ? 'and layanan_group in(' . $layanan_group . ')' : '';
		$layanan_name = $layanan_name ? 'and layanan_name in (' . $layanan_name . ')' : '';
		$segment = $segment === 'null' ? 'and segment_name is null' : ($segment ? 'and segment_name =' . json_encode($segment) : '');
		$customer = $customer === 'null' ? 'and customer_name is null' : ($customer ? 'and customer_name =' . json_encode($customer) : '');
		$material_brand = $material_brand === 'null' ? 'and material_brand is null' : ($material_brand ? 'and material_brand =' . json_encode($material_brand) : '');
		$material_name = $material_name === 'null' ? 'and material_name is null' : ($material_name ? 'and material_name =' . json_encode($material_name) : '');		
		$query = $this->db->query("
		select segment_name, sum($value) as value from datamart_kf.exe_db2 
		where bulan_faktur >= $start and bulan_faktur  <= $end
		and segment_name is not null
		$layanan_group $layanan_name $branch $segment $customer $material_brand $material_name group by segment_name order by value DESC");
		return $query->result_array();
	}

	public function detail_customer_name($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer, $material_brand, $material_name)
	{
		$branch = $branch ? 'and branch_name =' . json_encode($branch) : '';
		$layanan_group = $layanan_group ? 'and layanan_group in(' . $layanan_group . ')' : '';
		$layanan_name = $layanan_name ? 'and layanan_name in (' . $layanan_name . ')' : '';
		$segment = $segment === 'null' ? 'and segment_name is null' : ($segment ? 'and segment_name =' . json_encode($segment) : '');
		$customer = $customer === 'null' ? 'and customer_name is null' : ($customer ? 'and customer_name =' . json_encode($customer) : '');
		$material_brand = $material_brand === 'null' ? 'and material_brand is null' : ($material_brand ? 'and material_brand =' . json_encode($material_brand) : '');
		$material_name = $material_name === 'null' ? 'and material_name is null' : ($material_name ? 'and material_name =' . json_encode($material_name) : '');		
		// die($customer);
		$query = $this->db->query("
		select customer_name, sum($value) as value from datamart_kf.exe_db2 
		where bulan_faktur >= $start and bulan_faktur  <= $end 
		and customer_name is not null
		$layanan_group $layanan_name $branch $segment $customer $material_brand $material_name group by customer_name order by value DESC");
		return $query->result_array();
	}

	public function detail_product_brand($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer, $material_brand, $material_name)
	{
		$branch = $branch ? 'and branch_name =' . json_encode($branch) : '';
		$layanan_group = $layanan_group ? 'and layanan_group in(' . $layanan_group . ')' : '';
		$layanan_name = $layanan_name ? 'and layanan_name in (' . $layanan_name . ')' : '';		
		$segment = $segment === 'null' ? 'and segment_name is null' : ($segment ? 'and segment_name =' . json_encode($segment) : '');
		$customer = $customer === 'null' ? 'and customer_name is null' : ($customer ? 'and customer_name =' . json_encode($customer) : '');
		$material_brand = $material_brand === 'null' ? 'and material_brand is null' : ($material_brand ? 'and material_brand =' . json_encode($material_brand) : '');		
		$material_name = $material_name === 'null' ? 'and material_name is null' : ($material_name ? 'and material_name =' . json_encode($material_name) : '');				
		$query = $this->db->query("
		select  material_brand, sum($value) as value from datamart_kf.exe_db2 where bulan_faktur >= $start and bulan_faktur  <= $end
		$layanan_group $layanan_name $branch $segment $customer $material_brand $material_name
		group by material_brand");
		return $query->result_array();
	}

	public function detail_product($value, $start, $end, $branch, $layanan_group, $layanan_name, $segment, $customer, $material_brand, $material_name)
	{
		$branch = $branch ? 'and branch_name =' . json_encode($branch) : '';
		$layanan_group = $layanan_group ? 'and layanan_group in(' . $layanan_group . ')' : '';
		$layanan_name = $layanan_name ? 'and layanan_name in (' . $layanan_name . ')' : '';
		$segment = $segment === 'null' ? 'and segment_name is null' : ($segment ? 'and segment_name =' . json_encode($segment) : '');
		$customer = $customer === 'null' ? 'and customer_name is null' : ($customer ? 'and customer_name =' . json_encode($customer) : '');
		$material_brand = $material_brand === 'null' ? 'and material_brand is null' : ($material_brand ? 'and material_brand =' . json_encode($material_brand) : '');
		$material_name = $material_name === 'null' ? 'and material_name is null' : ($material_name ? 'and material_name =' . json_encode($material_name) : '');		
		$query = $this->db->query("
		select  material_name,sum(qty_ty) qty, sum($value) as value from datamart_kf.exe_db2 where bulan_faktur >= $start and bulan_faktur  <= $end 
		$layanan_group $layanan_name $branch $segment $customer $material_brand $material_name group by material_name ");
		return $query->result_array();
	}

	public function layanan($layanan_group)
	{
		$group_layanan = $layanan_group ? $layanan_group : '';
		$group_layanan = $group_layanan ? 'AND layanan_group in(' . $group_layanan . ')' : '';
		$query = $this->db->query("SELECT DISTINCT layanan_name from datamart_kf.exe_db2 where 
		layanan_name is not null
		$group_layanan");
		return $query->result_array();
	}

	public function group_layanan()
	{
		$query = $this->db->query("SELECT DISTINCT layanan_group 
		from datamart_kf.exe_db2
		where layanan_group is not null
		");
		return $query->result_array();
	}

	public  function forIn($itmes)
	{
		if ($itmes) {
			$i = 0;
			$isi = "";
			foreach ($itmes as $p => $a) {
				$i++;

				if ($i != count($itmes)) {
					$isi .= "'$a',";
				} else if ($i == count($itmes)) {
					$isi .= "'$a'";
				}
			}
		}
		return $isi;
	}
}
