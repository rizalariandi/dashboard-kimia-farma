<?php
class Boost_bcg_matrix_kfa_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_area($table)
    {
        return $this->db->distinct()->select('provinsi')->get($table)->result();
    }

    function get_areas($table)
    {
        return $this->db->select('nama_provinsi')->get($table)->result();
    }

    function get_produk($table)
    {
        // return $this->db->distinct()->select('produk_id')->get($table)->result();
        return $this->db->distinct()->select('produk')->order_by('produk', 'asc')->get($table)->result();
    }

    function get_kompetitor($table)
    {
        return $this->db->distinct()->select('produk')->order_by('produk', 'asc')->get($table)->result();
    }

    function get_varian_product($table, $area, $produk) {

        // var_dump($produk);
        // die;
        $this->db->distinct()->select('produk');

        if(is_array($produk)) {
            $this->db->group_start();
                foreach($produk as $p) {
                    $this->db->or_like('produk', $p);
                }
            $this->db->group_end();
        } else {
            $this->db->like('produk', $produk);
        }

        if (is_array($area)) {
            $this->db->group_start();
                foreach($area as $a) {
                    $this->db->or_where('provinsi', $a );
                }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }

        $this->db->where('kategori', 1);
        $this->db->order_by('produk', 'asc');
        return $this->db->get($table)->result();

        // return $this->db->distinct()->select('produk')->where('produk_id', $produk)->where('provinsi', $area)->where('kategori', 1)->order_by('produk','asc')->get($table)->result();
    }

    function check_data($table, $area, $produk, $from='', $to='')
    {
        if (is_array($produk)) {
            $this->db->group_start();
            foreach($produk as $prod) {
                $this->db->or_where('produk', $prod);
            }
            $this->db->group_end();
        } else {
            $this->db->where('produk', $produk);
        }

        if(is_array($area)) {
            $this->db->group_start();
            foreach($area as $ar) {
                $this->db->or_where('provinsi', $ar);
            }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }

        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->limit(1);
        return $this->db->get($table)->result();   
    }

    function get_bcg_product_kompetitor($table, $area, $produk,$kompetitor) {
        return $this->db->distinct()->select('produk')->where("(produk='$produk' OR produk='$kompetitor')")->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
    }

    function get_bcg_product_detils($table, $area, $produk, $kompetitor) {
        return $this->db->distinct()->select('produk')->like('produk',$produk)->or_like('produk',$kompetitor)->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
    }

    function get_bcg_matrix_kompetitor($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        // $list_produk = $this->get_bcg_product_kompetitor($table, $area, $produk, $kompetitor);

        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }
        
        // var_dump($list_produk);
        // die;

        // GET product market share
        if ($filter == 'value'){
            // GET total market share value
            $this->db->select('SUM(total_sales) as total_market_share');
            if (is_array($produk)) {
                $this->db->group_start();
                foreach($list_produk as $pr) {
                    // var_dump($pr);
                    // die;
                    $this->db->or_where('produk', $pr);
                }
                $this->db->group_end();
            } else {
                $this->db->where('produk', $produk);
            }

            if (is_array($area)) {
                $this->db->group_start();
                foreach($area as $ar) {
                    $this->db->or_where('provinsi', $ar);
                }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }
            $total_market_share = $this->db->get($table)->result();

            // die($this->db->last_query());
            // var_dump($total_market_share);
            // die;

            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p . '", total_sales, 0)) / "' . $total_market_share[0]->total_market_share . '" AS DECIMAL(10,4)) * 100 as "' . $p . '"');
            }
        } else {
            // GET total market share quantity
            $this->db->select('SUM(quantity) as total_market_share');
            // if (is_array($produk)) {
                $this->db->group_start();
                foreach($list_produk as $pr) {
                    $this->db->or_where('produk', $pr);
                }
                $this->db->group_end();
            // } else {
            //     $this->db->where('produk_id', $produk);
            // }

            if (is_array($area)) {
                $this->db->group_start();
                foreach($area as $ar) {
                    $this->db->or_where('provinsi', $ar);
                }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }
            $total_market_share = $this->db->get($table)->result();

            foreach ($list_produk as $p) {
                $this->kf->select('CAST(SUM(IF( produk = "' . $p .'", quantity, 0)) / "' .  $total_market_share[0]->total_market_share .'" AS DECIMAL(10,4)) * 100 as "' . $p . '" ');
            }
        }
        $this->kf->from($table);

        if (is_array($area)) {
            $this->kf->group_start();
            foreach($area as $ar) {
                $this->kf->or_where('provinsi', $ar);
            }
            $this->kf->group_end();
        } else {
            $this->kf->where('provinsi', $area);
        }

        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);

        $output_marketshare = $this->kf->generate();

        // die($this->db->last_query());
        // var_dump($output_marketshare);
        // die;

        // GET market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');

            $this->db->group_start();
            if(is_array($produk) AND is_array($kompetitor)) {
                foreach($produk as $p) {
                    $this->db->or_where('produk', $p);
                }

                foreach($kompetitor as $k) {
                    $this->db->or_where('produk', $k);
                }
            } else if(is_array($produk)) {
                foreach($produk as $p) {
                    $this->db->or_where('produk', $p);
                }
            } else if(is_array($kompetitor)) {
                foreach($kompetitor as $k) {
                    $this->db->or_where('produk', $k);
                }
            } else {
                $this->db->where('produk', $produk);
                $this->db->or_where('produk', $kompetitor);
            }
            $this->db->group_end();

            $this->db->group_start();
            if (is_array($area)) {
                foreach($area as $a) {
                    $this->db->or_where('provinsi', $a);
                }
            } else {
                $this->db->where('provinsi', $area);
            }
            $this->db->group_end();

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');

            $this->db->group_start();
            if(is_array($produk) AND is_array($kompetitor)) {
                foreach($produk as $p) {
                    $this->db->or_where('produk', $p);
                }

                foreach($kompetitor as $k) {
                    $this->db->or_where('produk', $k);
                }
            } else if(is_array($produk)) {
                foreach($produk as $p) {
                    $this->db->or_where('produk', $p);
                }
            } else if(is_array($kompetitor)) {
                foreach($kompetitor as $k) {
                    $this->db->or_where('produk', $k);
                }
            } else {
                $this->db->where('produk', $produk);
                $this->db->or_where('produk', $kompetitor);
            }
            $this->db->group_end();

            $this->db->group_start();
            if (is_array($area)) {
                foreach($area as $a) {
                    $this->db->or_where('provinsi', $a);
                }
            } else {
                $this->db->where('provinsi', $area);
            }
            $this->db->group_end();

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk", "bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // die($this->db->last_query());
        // var_dump($output_growth);
        // die;

        $growth = array();
        $bulan = array();
        $num = array();
        $json = array();
        $marketshare = json_decode($output_marketshare);

        foreach($list_produk as $p=>$val) {
            $json[$p]["y"] = 0;                            

            if (empty($output_growth)) {
                $json[$p]["y"] = 0;                            
            } else {
                $id = 0;
                foreach($output_growth as $key=>$value) {
                    // $growth[$value->produk][] = 0;
                    // $bulan[$value->produk][] = 0;
                    if ($val == $value->produk) {
                        $id++;
                        $growth[$value->produk][] = $value->total;
                        $bulan[$value->produk][] = $value->bulan;
                        $num[$value->produk] = $id;
                    }
                }

                foreach($output_growth as $key=>$value) {
                    if ($val == $value->produk) {
                        // $m = linear_regression($bulan[$val], $growth[$val]);
                        // $market_growth[$val] = $m["m"];
                        if ($num[$val] > 1) {
                            $market_growth[$val] = round( ($growth[$val][$num[$val]-1] - $growth[$val][0]) / $growth[$val][0] * 100, 2);
                            $mg[$val] = round( (pow($growth[$val][$num[$val]-1] / $growth[$val][0], 1/$num[$val])  - 1 ) * 100, 2) ;
                            $json[$p]["y"] = $mg[$val]; 
                        } else if ($num[$val] == 1 ){
                            $market_growth[$val] = 100;
                            $mg[$val] = 100;
                            $json[$p]["y"] = 100;
                        } else if ($num[$val] == 0) {
                            $market_growth[$val] = 0;
                            $mg[$val] = 0;
                            $json[$p]["y"] = 0;
                        }
                                                   
                    }
                }
            }
        }

        // print("<pre>". print_r($growth, true) . "</pre>");
        // var_dump(count($bulan['FITUNO']));
        // var_dump($num['IMUNOS']);
        // var_dump(2000 ^ 1/$num['FITUNO']);
        // print("<pre>". print_r($market_growth, true) . "</pre>");
        // print("<pre>". print_r($mg, true) . "</pre>");
        // die;

        // var_dump($json);
        // die;

        $varian_produk = $this->get_varian_product($table, $area, $list_produk);

        // die($this->db->last_query());
        
        foreach ($list_produk as $key=>$value){
            foreach($marketshare->data as $market)
            {
                $in = $value;
                $json[$key]["x"] = $marketshare->data[0]->$in;
            }

            $json[$key]["name"] = $value;
            
            // coloring node
            // if ($value == $produk) {
            //     $json[$key]["color"] = '#FFA351FF';
            // }
            foreach ($varian_produk as $vp){
                if ($value == $vp->produk) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }
        // var_dump($json);
        // die;
        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }

    function rekomendasi($table, $market_share, $market_growth)
    {
        if ($table == 'usc_bst_product_mart') {
            $perspektif = 'BRAND KF';
        } else if ($table == 'usc_bst_product_details_mart') {
            $perspektif = 'Varian Produk KF';
        } else {
            $perspektif = 'Kompetitor';
        }

        $rekomendasi = $this->db->select('rekomendasi')
            ->where('perspektif', $perspektif)
            ->where('market_share', $market_share)
            ->where('product_growth', $market_growth)
            ->get('usc_bst_product_recommendation')->row()->rekomendasi;
        return $rekomendasi;
    }

    function rekomendasi_kompetitor($table, $area, $produk, $filter, $from, $to)
    {
        $rekomendasi = '';
        $bcg = $this->get_bcg_matrix_kompetitor($table, $area, $produk, $filter, $from, $to);
        $datas = json_decode($bcg);
        // var_dump($datas);
        // die();
        foreach ($datas as $data) {
            if ($data->name != $produk){
                $marketvalue = $this->get_marketvalue($table, $area, $produk, $data->x, $data->y, $filter);
                $rekomendasi .= '<li> Perspektif : Kompetitor - ' . $data->name . ' ' . $this->rekomendasi('Kompetitor', $marketvalue['share'], $marketvalue['growth']) . '</li>';    
            }
        }
        // var_dump($rekomendasi);
        // die();
        return $rekomendasi;
    }

    function get_bcg_rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';

        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }

        $bcg = $this->get_bcg_matrix_kompetitor($table, $area, $produk, $kompetitor, $filter,  $from, $to);
        $data = json_decode($bcg);
        // var_dump($list_produk);
        // die;
        // var_dump($data);
        // die();
        $arr_data = array();
        foreach ($data as $d) {
            foreach ($list_produk as $p) {
                if ($d->name == $p) {
                    $arr_data[$d->name]["x"] = $d->x;
                    $arr_data[$d->name]["y"] = $d->y;
                }
            }
        }


        // var_dump($arr_data);
        // die();
        $check = $this->check_data($table, $area, $produk, $from, $to);
        // var_dump($check);
        // die;

        if (empty($data) or empty($check)) {
            if (is_array($produk)) {
                foreach($produk as $pr) {
                    $rekomendasi .= 'Data market share dan market growth untuk produk ' . $pr . ' tidak ada';
                }
            } else {
                $rekomendasi = 'Data market share dan market growth untuk produk ' . $produk. ' tidak ada';
            }
        } else {
            $marketvalue = array();
            // var_dump($data);
            // die;

            foreach ($data as $key=>$data_market) {
                // var_dump($data_market->name);
                $marketvalue[$data_market->name] = $this->get_marketvalue($table, $area, $produk, $arr_data[$data_market->name]["x"], $arr_data[$data_market->name]["y"], $filter);

                if (($marketvalue[$data_market->name]['share'] == 'High' and $marketvalue[$data_market->name]['growth'] == 'High') or ($marketvalue[$data_market->name]['share'] == 'Low' and $marketvalue[$data_market->name]['growth'] == 'High')){
                    $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue[$data_market->name]['share'], $marketvalue[$data_market->name]['growth']) . '</li>';
                } 
                else {
                    $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue[$data_market->name]['share'], $marketvalue[$data_market->name]['growth']) . '</li>';
                    // $rekomendasi .= $this->rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to);
                }
                $rekomendasi = str_replace("[FITUNO/BATUGIN]", $data_market->name, $rekomendasi);
                $rekomendasi = str_replace("[KOMPETITOR]", $data_market->name, $rekomendasi);
                $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '.$data_market->name, $rekomendasi);
                if (is_array($area)) {
                    foreach ($area as $a) {
                        $rekomendasi = str_replace("[AREA]", $a, $rekomendasi);
                    }
                } else {
                    $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);
                }
            }
        }
        return $rekomendasi;
    }

    // MATRIX KFA VARIAN PRODUCT
    function get_bcg_matrix_detils_kompetitor($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        // $list_produk = $this->get_bcg_product_detils($table, $area, $produk, $kompetitor);
      
        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }

        $this->db->distinct()->select('produk');
        $this->db->group_start();
        foreach($list_produk as $lp) {
            $this->db->or_like('produk', $lp);
        }
        $this->db->group_end();
        $list_varian_produk = $this->db->get($table)->result();

        // die($this->db->last_query());
        // var_dump($list_varian_produk);
        // var_dump(count($list_varian_produk));
        // die;

        //get market share
        if ($filter == 'value'){
            // GET total market share value
            $this->db->select('SUM(total_sales) as total_market_share');
            // if (is_array($produk)) {
            // if (count($list_varian_produk) > 1) {
                $this->db->group_start();
                foreach($list_varian_produk as $lvp) {
                    // var_dump($lvp->produk);
                    // die;
                    $this->db->or_like('produk', $lvp->produk);
                }
                $this->db->group_end();
            // } else {
            //     $this->db->like('produk', $produk);
            // }

            if (is_array($area)) {
                $this->db->group_start();
                foreach($area as $ar) {
                    $this->db->or_where('provinsi', $ar);
                }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }
            $total_market_share = $this->db->get($table)->result();

            // die($this->db->last_query());

            if (empty($total_market_share[0]->total_market_share)) {
                $total_market_share[0]->total_market_share = 0;
            }
            // var_dump($total_market_share[0]->total_market_share);
            // die;

            foreach ($list_varian_produk as $p) {
                if (empty($total_market_share[0]->total_market_share)) {
                    $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk . '", 0, 0)) AS DECIMAL(10,4)) * 100 as "' . $p->produk . '"');
                } else {
                    $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk . '", total_sales, 0)) / "' . $total_market_share[0]->total_market_share . '" AS DECIMAL(10,4)) * 100 as "' . $p->produk . '"');
                }
                
            }
        } else {
            // GET total market share value
            $this->db->select('SUM(quantity) as total_market_share');
            // if (is_array($produk)) {
                $this->db->group_start();
                foreach($list_varian_produk as $lvp) {
                    $this->db->or_like('produk', $lvp->produk);
                }
                $this->db->group_end();
            // } else {
            //     $this->db->like('produk', $produk);
            // }

            if (is_array($area)) {
                $this->db->group_start();
                foreach($area as $ar) {
                    $this->db->or_where('provinsi', $ar);
                }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }
            $total_market_share = $this->db->get($table)->result();
            if (empty($total_market_share[0]->total_market_share)) {
                $total_market_share[0]->total_market_share = 0;
            }
            foreach ($list_varian_produk as $p) {
                if (empty($total_market_share[0]->total_market_share)) {
                    $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk . '", 0, 0)) AS DECIMAL(10,4)) * 100 as "' . $p->produk . '"');
                } else {
                    $this->kf->select('CAST(SUM( IF( produk = "' . $p->produk . '", quantity, 0)) / "' . $total_market_share[0]->total_market_share . '" AS DECIMAL(10,4)) * 100 as "' . $p->produk . '"');
                }
            }
        }
        $this->kf->from($table);
        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $output_marketshare_varian = $this->kf->generate();

        // die($this->db->last_query());
        // var_dump($output_marketshare_varian);
        // die;

        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');

            $this->db->group_start();
            foreach($list_varian_produk as $l) {
                $this->db->or_like('produk', $l->produk);
            }
            $this->db->group_end();

            $this->db->group_start();
            if (is_array($area)) {
                foreach($area as $a) {
                    $this->db->or_where('provinsi', $a);
                }
            } else {
                $this->db->where('provinsi', $area);
            }
            $this->db->group_end();

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            // $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();

            // die($this->db->last_query());
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');

            $this->db->group_start();
            foreach($list_varian_produk as $l) {
                $this->db->or_like('produk', $l->produk);
            }
            $this->db->group_end();
            
            $this->db->group_start();
            if (is_array($area)) {
                foreach($area as $a) {
                    $this->db->or_where('provinsi', $a);
                }
            } else {
                $this->db->where('provinsi', $area);
            }
            $this->db->group_end();

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk", "bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();

        $growth = array();
        $bulan = array();
        $num = array();
        $json = array();
        $marketshare_varian = json_decode($output_marketshare_varian);

        foreach($list_varian_produk as $p=>$val) {
            $json[$p]["y"] = 0;                            
            
            if (empty($output_growth)) {
                $json[$p]["y"] = 0;                            
            } else {
                $id = 0;
                foreach($output_growth as $key=>$value) {
                    // $growth[$value->produk][] = 0;
                    // $bulan[$value->produk][] = 0;

                    if ($val->produk == $value->produk) {
                        $id++;
                        $growth[$value->produk][] = $value->total;
                        $bulan[$value->produk][] = $value->bulan;
                        $num[$value->produk] = $id;

                    }
                }

                foreach($output_growth as $key=>$value) {
                    if ($val->produk == $value->produk) {
                        // $m = linear_regression($bulan[$val->produk], $growth[$val->produk]);
                        // $market_growth[$val->produk] = $m["m"];
                        if ($num[$val->produk] > 1) {
                            $market_growth[$val->produk] = round( ($growth[$val->produk][$num[$val->produk]-1] - $growth[$val->produk][0]) / $growth[$val->produk][0] * 100, 2);
                            $mg[$val->produk] = round( (pow($growth[$val->produk][$num[$val->produk]-1] / $growth[$val->produk][0], 1/$num[$val->produk])  - 1 ) * 100, 2) ;
                            $json[$p]["y"] =  $mg[$val->produk]; 
                        } else if ($num[$val->produk] == 1 ){
                            $market_growth[$val->produk] = 100;
                            $mg[$val->produk] = 100;
                            $json[$p]["y"] = 100;
                        } else if ($num[$val->produk] == 0) {
                            $market_growth[$val->produk] = 0;
                            $mg[$val->produk] = 0;
                            $json[$p]["y"] = 0;
                        }
                    }
                }
            }
        }

        // print("<pre>". print_r($output_growth, true) . "</pre>");
        // print("<pre>". print_r($growth, true) . "</pre>");
        // print("<pre>". print_r($num, true) . "</pre>");
        // print("<pre>". print_r($market_growth, true) . "</pre>");
        // print("<pre>". print_r($mg, true) . "</pre>");
        // die;

        $lvarian = array();
        foreach($list_varian_produk as $lvprod) {
            array_push($lvarian, $lvprod->produk);
        }

        $varian_produk = $this->get_varian_product($table, $area, $lvarian);

        foreach ($list_varian_produk as $key=>$value){
            foreach($marketshare_varian->data as $market)
            {
                $in = $value->produk;
                $json[$key]["x"] = $marketshare_varian->data[0]->$in;
            }

            $json[$key]["name"] = $value->produk;
            if ($value->produk == $produk) {
                $json[$key]["color"] = '#FFA351FF';
            }
            foreach ($varian_produk as $vp){
                if ($value->produk == $vp->produk) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }

        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }

    function get_bcg_detils_rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';
        $bcg = $this->get_bcg_matrix_detils_kompetitor($table, $area, $produk,$kompetitor, $filter,  $from, $to);
        $datas = json_decode($bcg);

        $check = $this->check_data($table, $area, $produk, $from, $to);
        // var_dump($datas);
        // die;

        if (empty($datas)) {
            if (is_array($produk) or is_array($kompetitor)) {
                foreach($produk as $p) {
                    $rekomendasi .= 'Data market share dan market growth untuk produk ' . $p .' tidak ada';
                }
            } else {
                $rekomendasi = 'Data market share dan market growth untuk produk ' . $produk .' dan ' . $kompetitor . ' tidak ada';
            }
        }else {
            foreach ($datas as $data) {
                $marketvalue[$data->name] = $this->get_marketvalue($table, $area, $produk, $data->x, $data->y, $filter);
                
                $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue[$data->name]['share'], $marketvalue[$data->name]['growth']) . '</li>';
                $rekomendasi = str_replace("[VARIAN PRODUK]", $data->name, $rekomendasi);

                $rekomendasi = str_replace("[FITUNO/BATUGIN]", $data->name, $rekomendasi);
                $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '. $data->name, $rekomendasi);

                if (is_array($area)) {
                    foreach ($area as $a) {
                        $rekomendasi = str_replace("[AREA]", $a, $rekomendasi);
                    }
                } else {
                    $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);
                }
            }
        }
        
        return $rekomendasi;
        
    }
    
    function get_bcg_x($table, $area, $produk)
    {
        $this->db->select_avg('batasan_market_share');

        if(is_array($area)) {
            $this->db->group_start();
            foreach($area as $a){
                $this->db->or_where('area', $a);
            }
            $this->db->group_end();
        } else {
            $this->db->where('area', $area);
        }

        // if($table == 'usc_bst_product_details_value' or $table = 'usc_bst_product_details_quantity_new') {
            if (is_array($produk)) {
                $this->db->group_start();
                foreach($produk as $p) {
                    $this->db->or_like('nama_produk', $p);
                }
                $this->db->group_end();
            } else {
                $this->db->like('nama_produk', $produk);
            }
        // } else {
        //     if (is_array($produk)) {
        //         $this->db->group_start();
        //         foreach($produk as $p) {
        //             $this->db->or_where('nama_produk', $p);
        //         }
        //         $this->db->group_end();
        //     } else {
        //         $this->db->where('nama_produk', $produk);
        //     }
        // }

        $data = $this->db->get($table);

        // var_dump($produk);
        // var_dump($area);
        // var_dump($data->row()->batasan_market_share);
        // die;
        // kasih kondisi biar jika data nya kosong tidak terjadi error            
        if ($data->num_rows() <= 0  OR $data->row()->batasan_market_share == NULL) {
            return 0;
        } else {
            return $data->row()->batasan_market_share;
        }
    }

    function get_bcg_y($table, $area, $produk)
    {
        $this->db->select_avg('batasan_market_growth');

        if(is_array($area)) {
            $this->db->group_start();
            foreach($area as $a){
                $this->db->or_where('area', $a);
            }
            $this->db->group_end();
        } else {
            $this->db->where('area', $area);
        }

        // if($table == 'usc_bst_product_details_value' or $table = 'usc_bst_product_details_quantity_new') {
            if (is_array($produk)) {
                $this->db->group_start();
                foreach($produk as $p) {
                    $this->db->or_like('nama_produk', $p);
                }
                $this->db->group_end();
            } else {
                $this->db->like('nama_produk', $produk);
            }
        // } else {
        //     if (is_array($produk)) {
        //         $this->db->group_start();
        //         foreach($produk as $p) {
        //             $this->db->or_where('nama_produk', $p);
        //         }
        //         $this->db->group_end();
        //     } else {
        //         $this->db->where('nama_produk', $produk);
        //     }
        // }

        $data = $this->db->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error            
        if ($data->num_rows() <= 0 OR $data->row()->batasan_market_growth == NULL) {
            return 0;
        } else {
            return $data->row()->batasan_market_growth;
        }
    }

    function get_marketvalue($table, $area, $produk, $xproduk, $yproduk, $filter)
    {
        if ($table == 'usc_bst_product_mart') {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_ details_quantity_new', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_quantity_new_new', $area, $produk);
            }
        } else {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_details_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
            }
        }

        if ($xproduk >= $x) {
            $data['share'] = 'High';
        } else {
            $data['share'] = 'Low';
        }
        if ($yproduk >= $y) {
            $data['growth'] = 'High';
        } else {
            $data['growth'] = 'Low';
        }
        return $data;
    }

    // SELL OUT KFA
    function sell_out_kfa_kompetitor($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }

        if ($filter == 'value') {
            $this->db->select("produk, nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("produk, nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }

        $this->db->group_start();
        foreach($list_produk as $prod) {
            $this->db->or_where('produk', $prod);
        }
        $this->db->group_end();

        if (is_array($area)) {
            $this->db->group_start();
                foreach($area as $a){
                    $this->db->or_where('provinsi', $a);
                }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result();

        // die($this->db->last_query());

        $this->db->distinct()->select('nama_kfa');

        $this->db->group_start();
        foreach($list_produk as $prod) {
            $this->db->or_where('produk', $prod);
        }
        $this->db->group_end();

        if (is_array($area)) {
            $this->db->group_start();
                foreach($area as $a){
                    $this->db->or_where('provinsi', $a);
                }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }

        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->order_by('nama_kfa', 'asc');
        $kfa = $this->db->get($table)->result();

        // die($this->db->last_query());
        
        $this->db->distinct()->select("date_format(tanggal, '%M-%y') as bulan");

        $this->db->group_start();
        foreach($list_produk as $prod) {
            $this->db->or_where('produk', $prod);
        }
        $this->db->group_end();

        if (is_array($area)) {
            $this->db->group_start();
                foreach($area as $a){
                    $this->db->or_where('provinsi', $a);
                }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by('bulan');
        $bulan = $this->db->get($table)->result();

        $json = array();
        $cat = array();

        foreach($bulan as $b){
            $cat[] = $b->bulan;
        }

        foreach($kfa as $key=>$kf) {
            $json[$key]["name"] = $kf->nama_kfa;
            $total_sales = array();
            foreach($datas as $k=>$data) {
                if ($kf->nama_kfa == $data->nama_kfa){
                    $total_sales[] = $data->total_sales;
                }
            }    
            $json[$key]["data"] = $total_sales;
        }

        $output = array(
            'categories' => json_encode($cat),
            'data' => json_encode($json, JSON_NUMERIC_CHECK)
        );

        return json_encode($output);
    }

    public function data_sell_out($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='') {
        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }

        if ($filter == 'value') {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }

        $this->db->group_start();
        foreach($list_produk as $prod) {
            $this->db->or_where('produk', $prod);
        }
        $this->db->group_end();

        if (is_array($area)) {
            $this->db->group_start();
                foreach($area as $a){
                    $this->db->or_where('provinsi', $a);
                }
            $this->db->group_end();
        } else {
            $this->db->where('provinsi', $area);
        }

        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result_array();
        return $datas;
    }

    function get_data_export($table, $area, $produk, $kompetitor, $from, $to) {
        $list_produk = array();
        if (is_array($produk) AND is_array($kompetitor)) {
            $result = array_merge($produk, $kompetitor);
            $list_produk = array_unique($result);
        } else if (is_array($produk)) {
            foreach($produk as $pr) {
                array_push($list_produk, $pr);
            }
            array_push($list_produk, $kompetitor );
        } else if (is_array($kompetitor)) {
            foreach($kompetitor as $komp) {
                array_push($list_produk, $komp);
            }
            array_push($list_produk, $produk);
        } else {
            array_push($list_produk, $produk);
            array_push($list_produk, $kompetitor);
        }

        $this->db->distinct()->select('produk');
        $this->db->group_start();
        foreach($list_produk as $lp) {
            $this->db->or_like('produk', $lp);
        }
        $this->db->group_end();
        $list_varian_produk = $this->db->get($table)->result();

        if ($table == 'usc_bst_product_mart') {
            $this->db->select('produk, provinsi, bulan, SUM(total_sales) as sales, SUM(quantity) as quantity');
            $this->db->group_start();
            foreach($list_produk as $prod) {
                $this->db->or_where('produk', $prod);
            }
            $this->db->group_end();

            if (is_array($area)) {
                $this->db->group_start();
                    foreach($area as $a){
                        $this->db->or_where('provinsi', $a);
                    }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
        } else {
            $this->db->select('produk, provinsi, bulan, SUM(total_sales) as sales, SUM(quantity) as quantity');
            $this->db->group_start();
            foreach($list_varian_produk as $prod) {
                $this->db->or_where('produk', $prod->produk);
            }
            $this->db->group_end();

            if (is_array($area)) {
                $this->db->group_start();
                    foreach($area as $a){
                        $this->db->or_where('provinsi', $a);
                    }
                $this->db->group_end();
            } else {
                $this->db->where('provinsi', $area);
            }

            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
        }
        
        return $this->db->get($table)->result_array();
    }

}