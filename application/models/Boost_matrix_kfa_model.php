<?php
class Boost_matrix_kfa_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function check_data($table, $area, $produk, $from='', $to='')
    {
        if(($table == 'usc_bst_product_mart') or ($table == 'usc_bst_product_details_mart') ){
            $this->db->or_where_in('produk_id', $produk);
        } else {
            $this->db->or_where_in('produk', $produk);
        }
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        return $this->db->get($table)->result();   
    }

    function get_bcg_product_kompetitor($table, $area, $produk,$kompetitor) {
        // var_dump($produk);
        // var_dump($kompetitor);
        // die;
        // return $this->db->distinct()->select('produk')->where("(produk='$produk' OR produk='$kompetitor')")->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
        return $this->db->distinct()->select('produk')->where_in('produk',$produk)->or_where_in('produk',$kompetitor)->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
        // var_dump($a);
        // die;
    }


    function get_varian_product($table, $area, $produk) {
        // var_dump($produk);
        // die;
        return $this->db->distinct()->select('produk')->or_where_in('produk_id',$produk)->where('provinsi', $area)->where('kategori',1)->order_by('produk','asc')->get($table)->result();
    }

    function get_bcg_product_detils($table, $area, $produk,$kompetitor) {
        $gabung = array_merge($produk,$kompetitor);
        $pd = [];
            foreach($gabung as $k){
                $hasil = $this->db->distinct()->select('produk')->or_like('produk',$k)->where('provinsi', $area)->order_by('produk','asc')->get($table)->result();
                array_push($pd,$hasil);
        }
        // var_dump($pd);
        // die;
         return $pd;
    }

    function get_bcg_matrix_kompetitor($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
    //    var_dump($produk);
    //    var_dump($kompetitor);
    //    die;
    // echo"haha";
        $list_produk = [];
        $lp = $this->get_bcg_product_kompetitor($table, $area, $produk, $kompetitor);
        // var_dump($lp);
        // die;
        foreach($lp as $a){
            array_push($list_produk,$a->produk);

        }
        // var_dump($list_produk);
        // die;
        //get market share
        if ($filter == 'value'){
            foreach ($list_produk as $p) {
                    $this->kf->select('CAST(SUM( IF( produk = "'.$p.'", total_sales, 0)) / SUM( IF( produk_id= "'.$p.'", total_sales, 0)) AS DECIMAL(10,2)) * 100 as "' .$p . '"');
            }

        } else {
            foreach ($list_produk as $p) {

                $this->kf->select('CAST(SUM( IF( produk = "' . $p .'", quantity, 0)) / SUM( IF( produk_id= "' . $p .'", quantity, 0)) AS DECIMAL(10,2)) * 100 as "' . $p . '"');
            }
        }
        $this->kf->from($table);
        $this->kf->where('provinsi', $area);
        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $output = $this->kf->generate();
        
        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');
            $this->db->or_where_in('produk_id', $list_produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
            // var_dump($output_growth);
            // die;
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');
            $this->db->or_where_in('produk_id', $list_produk);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
            // var_dump($output_growth);
            // die;
        }

        $growth = array();
        $bulan = array();
        $json = array();
        $marketshare = json_decode($output);
        // // var_dump($marketshare);
        // //     die;
        // foreach ($marketshare as $ms){
        //     var_dump($ms);
        //     die;
        // }
        

        foreach($list_produk as $p=>$val) {
            $json[$p]["y"] = 0;                            

            foreach($output_growth as $key=>$value) {
                $growth[$value->produk][] = 0;
                $bulan[$value->produk][] = 0;
                if ($val == $value->produk) {
                    $growth[$value->produk][] = $value->total;
                    $bulan[$value->produk][] = $value->bulan;
                }
            }
            if (empty($output_growth)) {
                $json[$p]["y"] = 0;   
                // var_dump($json[$p]["y"]);
                // die;                         
            } else {
                foreach($output_growth as $key=>$value) {
                    if ($val == $value->produk) {
                        $m = linear_regression($bulan[$val], $growth[$val]);
                        $market_growth[$val] = $m["m"];
                        $json[$p]["y"] = $market_growth[$val];    
                        // var_dump($json[$p]["y"]);
                        // die;                        
                    }
                }
            }
        }
        $varian_produk=[];
        $varpro = $this->get_varian_product($table, $area, $list_produk);
        foreach($varpro as $provar){
            array_push($varian_produk,$provar->produk);
        }
        // var_dump($varian_produk);
        // die;
        foreach ($list_produk as $key=>$value){
            foreach($marketshare->data as $market)
            {
                // var_dump($market);
                // die;
                $in = $value;
                $json[$key]["x"] = $marketshare->data[0]->$in;
                // var_dump($json[$key]["x"]);
                // die;
            }

            $json[$key]["name"] = $value;
            if ($value == $list_produk) {
                $json[$key]["color"] = '#FFA351FF';
            }
            foreach ($varian_produk as $vp){
                if ($value == $vp) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }
                // var_dump($json);
                // die;
        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }


// detils
// 


function rekomendasi_kompetitor($table, $area, $produk,$kompetitor, $filter, $from, $to)
{
    // var_dump($produk);
    // var_dump($kompetitor);
    // $prod = array_merge($produk,$kompetitor);
    // var_dump($prod);

    // die;

    $bc = [];
    $rekomendasi = '';
    // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
    // $this->db->where('produk_id', $produk);
    // $this->db->where('nama_produk <>', $produk);
    // $this->db->where('area', $area);
    // $datas = $this->db->get($table)->result();
    
    // foreach($lp as $a){
    //     array_push($list_produk,$a->produk);

    // }
    $bcg = $this->get_bcg_matrix_kompetitor($table, $area, $produk,$kompetitor, $filter, $from, $to);
    $datas = json_decode($bcg);
    // var_dump(datas);
    // die();

    foreach ($datas as $data) {
        if ($data->name != $produk){
            $marketvalue = $this->get_marketvalue($table, $area, $produk, $data->x, $data->y, $filter);
            $rekomendasi .= '<li> Perspektif : Kompetitor - ' . $data->name . ' ' . $this->rekomendasi('Kompetitor', $marketvalue['share'], $marketvalue['growth']) . '</li>';    
        }
    }

    // var_dump($rekomendasi);
    // die();
    return $rekomendasi;
}

    function get_bcg_rekomendasi_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $rekomendasi = '';
        $list_produk = [];
        // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        // $this->db->where('nama_produk', $produk);
        // $this->db->where('area', $area);
        // $data = $this->db->get($table)->row();
        
        $bcg = $this->get_bcg_matrix_kompetitor($table, $area, $produk, $kompetitor, $filter,  $from, $to);
        
        $data = json_decode($bcg);
    //    var_dump($bcg);
        // die;
        foreach($data as $a){
            array_push($list_produk,$a->name);

        }
        
        $arr_data = array();
        foreach ($data as $d) {
            // if ($d->name == $produk) {
                $arr_data["x"] = $d->x;
                $arr_data["y"] = $d->y;
            // }
        // var_dump($arr_data);

        }
        // die();
       
        $check = $this->check_data($table, $area, $produk, $from, $to);
        
        if (empty($data) or empty($check)) {
            
            foreach($list_produk as $pr){
                    $rekomendasi = 'Data market share dan market growth untuk produk ' .$pr. ' tidak ada';
                    // var_dump($pr);
                }
            // die;

            // $rekomendasi = 'Data market share dan market growth untuk produk ' .$produk. ' tidak ada';
            
        } else {
            foreach($list_produk as $pr){
            //     var_dump($pr);
            //     die;
                $marketvalue = $this->get_marketvalue($table, $area, $pr, $arr_data["x"], $arr_data["y"], $filter);
                if (($marketvalue['share'] == 'High' and $marketvalue['growth'] == 'High') or ($marketvalue['share'] == 'Low' and $marketvalue['growth'] == 'High')){
                    $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
                } 
                else {
                    $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
                    $rekomendasi .= $this->rekomendasi_kompetitor($table, $area,$produk, $kompetitor, $filter, $from, $to);
                }
                $rekomendasi = str_replace("[FITUNO/BATUGIN]", $pr, $rekomendasi);
                $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '.$pr, $rekomendasi);
                $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);

            }
            // die;
           
        }
        return $rekomendasi;
    }

    function get_bcg_matrix_detils_kompetitor($table, $area, $produk, $kompetitor, $filter = 'value', $from='', $to='')
    {
        $list_produk=[];
        $lp = $this->get_bcg_product_detils($table, $area, $produk, $kompetitor);
       
        foreach($lp as $l){
            foreach($l as $o){
            array_push($list_produk,$o->produk);
            }
        }
        $prod = array_merge($produk,$kompetitor);

        //get market share
        if ($filter == 'value'){
            foreach ($list_produk as $p) {
                foreach ($prod as $prd) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p .'", total_sales, 0)) / SUM( IF( produk_id= "' . $prd .'", total_sales, 0)) AS DECIMAL(10,2)) * 100 as "' . $p . '"');
                 }
            }
        } else {
            foreach ($list_produk as $p) {
                foreach ($prod as $prd) {
                $this->kf->select('CAST(SUM( IF( produk = "' . $p .'", quantity, 0)) / SUM( IF( produk_id= "' . $prd .'", quantity, 0)) AS DECIMAL(10,2)) * 100 as "' . $p . '"');
                }
            }
        }
        $this->kf->from($table);
        $this->kf->where('provinsi', $area);
        if ($from == '' or $to == '') 
        {
            $from = '2019-07-01';
            $to = '2019-12-31';
        }
        $this->kf->where('tanggal >=', $from);
        $this->kf->where('tanggal <=', $to);
        $output = $this->kf->generate();

        // get market growth raw data
        if ($filter == 'value'){
            $this->db->select('produk, bulan, SUM(total_sales) as total');
            $this->db->where_in('produk_id', $prod);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        } else {
            $this->db->select('produk, bulan, SUM(quantity) as total');
            $this->db->where_in('produk_id', $prod);
            $this->db->where('provinsi', $area);
            $this->db->where('tanggal >=', $from);
            $this->db->where('tanggal <=', $to);
            $this->db->group_by(array("produk","bulan"));
            $output_growth = $this->db->get($table)->result();
        }
        // var_dump($output_growth);
        // die();
        $growth = array();
        $bulan = array();
        $json = array();
        $marketshare = json_decode($output);
        // var_dump($marketshare);
        // die();
        foreach($list_produk as $p=>$val) {
            // var_dump($val);
            $json[$p]["y"] = 0;                            

            foreach($output_growth as $key=>$value) {
            // var_dump($value);

                $growth[$value->produk][] = 0;
                $bulan[$value->produk][] = 0;
                if ($val == $value->produk) {
                    $growth[$value->produk][] = $value->total;
                    $bulan[$value->produk][] = $value->bulan;
                }
            }
            if (empty($output_growth)) {
                $json[$p]["y"] = 0;                            
            } else {
                foreach($output_growth as $key=>$value) {
                    // var_dump($value);
                    
                    if ($val == $value->produk) {
                        $m = linear_regression($bulan[$val], $growth[$val]);
                        $market_growth[$val] = $m["m"];
                        $json[$p]["y"] = $market_growth[$val];                            
                    }
                }
            }
        }
        // die();
        $varian_produk = $this->get_varian_product($table, $area, $prod);       
            // var_dump($prod);

        foreach ($list_produk as $key=>$value){
            // var_dump($value);

            foreach($marketshare->data as $market)
            {
                $in = $value;
                $json[$key]["x"] = $marketshare->data[0]->$in;
            }

            $json[$key]["name"] = $value;
            if ($value == $produk) {
                $json[$key]["color"] = '#FFA351FF';
            }
            foreach ($varian_produk as $vp){
                if ($value == $vp->produk) {
                    $json[$key]["color"] = '#FFA351FF';
                }
            }
        }
            // var_dump($json);
// die;
        return json_encode(array_values($json), JSON_NUMERIC_CHECK);
    }

    function sell_out_kfa_kompetitor($table, $area, $produk,$kompetitor, $filter = 'value', $from='', $to='')
    {
        $prod = array_merge($produk,$kompetitor);
        if ($filter == 'value') {
            $this->db->select("produk,nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("produk,nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }
        $this->db->where_in('produk',$prod);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result();

        $this->db->distinct()->select('nama_kfa');
        $this->db->where_in('produk',$prod);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->order_by('nama_kfa', 'asc');
        $kfa = $this->db->get($table)->result();
       
        $this->db->distinct()->select("date_format(tanggal, '%M-%y') as bulan");
        $this->db->where_in('produk',$prod);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by('bulan');
        $bulan = $this->db->get($table)->result();
        $json = array();
        $cat = array();

        foreach($bulan as $b){
            $cat[] = $b->bulan;
        }

        foreach($kfa as $key=>$kf) {
         
            $json[$key]["name"] = $kf->nama_kfa;
            $total_sales = array();
            foreach($datas as $k=>$data) {
                
                if ($kf->nama_kfa == $data->nama_kfa){
                    $total_sales[] = $data->total_sales;
                }
            }    
            $json[$key]["data"] = $total_sales;
        }

        $output = array(
            'categories' => json_encode($cat),
            'data' => json_encode($json, JSON_NUMERIC_CHECK)
        );
        return json_encode($output);
    }

    public function data_sell_out($table, $area, $produk,$kompetitor, $filter = 'value', $from='', $to='') {
        $prod = array_merge($produk,$kompetitor);
        
        if ($filter == 'value') {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`total_sales`) as total_sales");
        } else {
            $this->db->select("nama_kfa, date_format(tanggal, '%M-%y') as bulan, SUM(`quantity`) as total_sales");
        }
        $this->db->where_in('produk_id', $prod);
        $this->db->where('provinsi', $area);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("nama_kfa","bulan"));
        
        $datas = $this->db->get($table)->result_array();
        return $datas;
    }

    function get_data_export($table, $area, $produk,$kompetitor, $from, $to) {
        $prod = array_merge($produk,$kompetitor);
        
        $this->db->select('produk, provinsi, bulan, SUM(total_sales) as sales, SUM(quantity) as quantity');
        $this->db->where('provinsi', $area);
        $this->db->where_in('produk_id', $prod);
        $this->db->where('tanggal >=', $from);
        $this->db->where('tanggal <=', $to);
        $this->db->group_by(array("produk","bulan"));

        return $this->db->get($table)->result_array();
    }

    function get_area($table)
    {
        return $this->db->distinct()->select('provinsi')->get($table)->result();
    }

    function get_produk($table)
    {
        return $this->db->distinct()->select('produk_id')->get($table)->result();
    }

    function get_kompetitor($table)
    {
        return $this->db->distinct()->select('produk')->get($table)->result();
    }

    function rekomendasi_bcg_detils_kompetitor($table, $area, $produk, $kompetitor, $filter, $from, $to)
    {
        $prod = array_merge($produk,$kompetitor);
    //   var_dump($produk);
    //   die;
        $rekomendasi = '';
        // $this->db->select('CAST(market_share AS DECIMAL(10,2)) as x, CAST(market_growth AS DECIMAL(10,2)) as y, nama_produk as name');
        // $this->db->like('LOWER(nama_produk)', strtolower($produk));
        // $this->db->where('area', $area);
        // $datas = $this->db->get($table)->result();
        $bcg = $this->get_bcg_matrix_detils_kompetitor($table, $area, $produk,$kompetitor, $filter,  $from, $to);
        // var_dump("$bcg");
        // die();
        $datas = json_decode($bcg);
        // var_dump($datas);
        // die();
        $check = $this->check_data($table, $area, $produk, $from, $to);
        // var_dump($datas);
        // die();

        if (empty($datas)  or empty($check)) {
            foreach($prod as $p){
                // var_dump($p);
                // die();
                $rekomendasi = 'Data market share dan market growth untuk produk ' .$p. ' tidak ada';
            }
        }else {
            foreach ($datas as $data) {
                foreach ($prod as $pr) {
                   
                  if(strpos($data->name, $pr) !== false) {    
                      $marketvalue = $this->get_marketvalue($table, $area, $pr, $data->x, $data->y, $filter);
                      $rekomendasi .= '<li>' . $this->rekomendasi($table, $marketvalue['share'], $marketvalue['growth']) . '</li>';
                      $rekomendasi = str_replace("[VARIAN PRODUK]", $data->name, $rekomendasi);
                  }
                }
                
            }
        }
        
        foreach($prod as $prd){
            $rekomendasi = str_replace("[FITUNO/BATUGIN]", $prd, $rekomendasi);
            $rekomendasi = str_replace("[KATEGORI FITUNO/BATUGIN]", 'KATEGORI '.$prd, $rekomendasi);
            $rekomendasi = str_replace("[AREA]", $area, $rekomendasi);
        }
       
        return $rekomendasi;
    }
    
    function get_bcg_x($table, $area, $produk)
    {
        // var_dump($produk);
        // die;
        $data = $this->db->select('batasan_market_share')
            ->where('area', $area)
            ->where_in('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error            
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_share;
        }
    }

    function get_bcg_y($table, $area, $produk)
    {
        $data = $this->db->select('batasan_market_growth')
            ->where('area', $area)
            ->where_in('produk_id', $produk)
            ->get($table);
        // kasih kondisi biar jika data nya kosong tidak terjadi error
        if ($data->num_rows() <= 0) {
            return 0;
        } else {
            return $data->row()->batasan_market_growth;
        }
    }

    function get_marketvalue($table, $area, $produk, $xproduk, $yproduk, $filter)
    {
        if ($table == 'usc_bst_product_mart') {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_quantity', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_quantity', $area, $produk);
            }
        } else {
            if ($filter == 'value') {
                $x = $this->get_bcg_x('usc_bst_product_details_value', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_value', $area, $produk);
            } else {
                $x = $this->get_bcg_x('usc_bst_product_details_quantity_new', $area, $produk);
                $y = $this->get_bcg_y('usc_bst_product_details_quantity_new', $area, $produk);
            }
        }

        if ($xproduk >= $x) {
            $data['share'] = 'High';
        } else {
            $data['share'] = 'Low';
        }
        if ($yproduk >= $y) {
            $data['growth'] = 'High';
        } else {
            $data['growth'] = 'Low';
        }
        return $data;
    }

    function rekomendasi($table, $market_share, $market_growth)
    {
        if ($table == 'usc_bst_product_mart') {
            $perspektif = 'BRAND KF';
        } else if ($table == 'usc_bst_product_details_mart') {
            $perspektif = 'Varian Produk KF';
        } else {
            $perspektif = 'Kompetitor';
        }

        $rekomendasi = $this->db->select('rekomendasi')
            ->where('perspektif', $perspektif)
            ->where('market_share', $market_share)
            ->where('product_growth', $market_growth)
            ->get('usc_bst_product_recommendation')->row()->rekomendasi;
        return $rekomendasi;
    }

}