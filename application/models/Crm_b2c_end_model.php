<?php
class Crm_b2c_end_model extends CI_Model {
	
	private $table = 'crm_b2c_end_trans';
	private $id    = 'customer_name';

	public function __construct()
	{
	    $this->load->database();
	}
	
	// Query for new datatables purpose ;
	//---------------------------------------------------------------------------------------------------------------------
	function dtquery($param)
	{  
        // ========================== initial query ==========================
		// return $this->db->query("select customer, address, phone, month, year, level,
        //     sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq 
        //     FROM ".$this->table." $param[where] 
        //     group by customer, month, year
        //     $param[order] $param[limit]");
        // ====================================================================

        return $this->db->query("SELECT /* SQL_NO_CACHE */ customer, address, phone, month, year, level, sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq
         FROM b2c_end_cust_segment $param[where] group by customer, month, year
         $param[order] 
         $param[limit]");
	}
	
	function dtfiltered($param)
	{
		// $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
        $result = $this->db->query('SELECT count(*) as jumlah
        from (
            SELECT /* SQL_NO_CACHE */ customer, address, phone, month, year, level, 
            sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq
            FROM b2c_end_cust_segment '.$param["where"].' group by customer, month, year
            '.$param["order"].' '.$param["limit"].') a'
        )->row();
		
		return $result->jumlah;
	} 
	
	function dtcount($param)
	{
		// return $this->db->count_all($this->table);
        // return  $this->db->query("select count(*) total from ( select * from ".$this->table." $param[where] 
        //group by customer, month, year) x"); 

        $result = $this->db->query('SELECT count(*) as jumlah
        from (
            SELECT /* SQL_NO_CACHE */ customer, address, phone, month, year, level, 
            sum(rev) as rev, sum(freq) as freq, (sum(rev)*sum(freq)) as revfreq
            FROM b2c_end_cust_segment '.$param["where"].' group by customer, month, year
            '.$param["order"].' '.$param["limit"].') a'
        )->row();
		
		return $result->jumlah;
	}

    function dtquery_detail($param)
    {
        return $this->db->query("select customer,tanggal,name_produk,provinsi,payment_to_sentence,rev,freq 
            from crm_b2c_end_history
            $param[where] $param[order] $param[limit]");
    }

    function dtfiltered_detail($param)
    {
        // $result = $this->db->query('SELECT FOUND_ROWS() as jumlah')->row();
        $result = $this->db->query('SELECT count(*) as jumlah
        from (
            select customer,tanggal,name_produk,provinsi,payment_to_sentence,rev,freq 
            from crm_b2c_end_history
            '.$param["where"].' '.$param["order"].' '.$param["limit"].') a
        ')->row();

        return $result->jumlah;
    }

    function dtcount_detail()
    {
        return $this->db->count_all('crm_b2c_end_history');
    }
	//---------------------------------------------------------------------------------------------------------------------


    public function b2c_level($id)
    {
        return $this->db->query(" SELECT distinct level FROM ".$this->table." WHERE customer = '$id' order by year desc, month desc");
    }

    function getProfile($id)
    {
        return $this->db->query("select * from ".$this->table." where customer = '$id' limit 0, 1");
    }

    public function detail_trans($id, $year)
    {
        return $this->db->query("SELECT customer, month, sum(rev) rev, sum(freq) freq
            FROM crm_b2c_end_trans 
            WHERE customer = '$id' and year = '$year'
            GROUP BY customer, month            
            order by month desc");
    }

    function history($where) // trx history
    {
        // ================ initial query ==================
        // return $this->db->query("SELECT year, month, sum(rev) as rev FROM ".$this->table." $where GROUP BY year, month");
        // =================================================

        return $this->db->query("SELECT /* SQL_NO_CACHE */ year, month, sum(rev) as rev 
        FROM crm_b2c_end_user_purchase_history $where GROUP BY year, month");
    }

    function getLevelPie($where) // customer_level
    {
        // ================ initial query ==============
        // return $this->db->query("SELECT distinct level, count(*) as jml from ".$this->table." $where group by level");
        // =============================================
        
        return $this->db->query("SELECT /* SQL_NO_CACHE */ distinct level, jml
         from b2c_end_user_customer_level $where
         order by level");
    }

    function getGender($where)
    {
        // ==================== initial query ========================
        // return $this->db->query("SELECT lower(gender) as gender, count(*) as jml from ".$this->table." $where group by gender");
        // ===========================================================
        return $this->db->query("SELECT /* SQL_NO_CACHE */ gender , sum(jml) jml 
        from (
            SELECT lower(case when gender ='perempuan' then 'p' ELSE gender end) as gender, jml
            from datamart_kf.b2c_end_user_gender
            $where
        ) a group by gender");
    }

    function getAge($where)
    {
        return $this->db->query("SELECT age, count(*) as jml from ".$this->table." $where group by age");
    }

    function getHeatmaps($where)
    {
        // ===================== initial code =========================
        // return $this->db->query("SELECT provinsi, count(*) as jml from ".$this->table." $where group by provinsi");
        // ============================================================

        return $this->db->query("SELECT /* SQL_NO_CACHE */ provinsi, sum(hitung) as jml from b2c_end_user_heatmap $where group by provinsi");
    }

    function getAllProvinsi()
    {
        return $this->db->query("SELECT provinsi from ".$this->table." group by provinsi");
    }

    public function detail_level($id, $month='', $year='')
    {
        return $this->db->query(" SELECT * FROM ".$this->table." WHERE doctor = '$id'");
    }

    public function purchase($id)
    {
        return $this->db->query("SELECT month, year, rev FROM ".$this->table." WHERE doctor = '$id' and year = '".date('Y')."' GROUP BY month");
    }

	public function b2b_detail($id)
    {
		$query = $this->db->query(" SELECT * FROM b2c_crm_doctor WHERE bcd_doctor_name = '$id'");
		return $query->row();
	}

	
	function getYear()
	{
			return $this->db->query("SELECT ccl_year from b2c_cust_lini  
					group by ccl_year order by ccl_year");
	}
	
	function getLini()
	{
			return $this->db->query("SELECT lini from b2c_crm_doctor_trans  
					group by lini order by lini");
	}
	
	function getLayanan()
	{
			return $this->db->query("SELECT bcd_spesialisasi from b2c_crm_doctor where bcd_spesialisasi!='' 
					group by bcd_spesialisasi order by bcd_spesialisasi");
	}
	
	function getChannel()
	{
			return $this->db->query("SELECT ct_channel from b2c_customers  
					group by ct_channel order by ct_channel");
	}

	function getLevels($level,$start,$end,$area)
	{   
		$where = "where created_at BETWEEN '".$start[2]."-".$start[1]."-".$start[0]."' and '".$end[2]."-".$end[1]."-".$end[0]."'";
		if($level!="") $where .=" and lower(level)='$level'"; 
		// if($lini!="0") $where .=" and lini	='$lini'"; 
		if($area!="0") $where .=" and bcd_address	='$area'";     

		// echo "select count(*) total from (SELECT distinct level,doctor from b2c_crm_doctor left join b2c_crm_doctor_trans on doctor=bcd_doctor_name $where limit 0,100)a";
		return $this->db->query("select count(*) total from (SELECT distinct level,doctor from b2c_crm_doctor left join b2c_crm_doctor_trans on doctor=bcd_doctor_name $where limit 0,100)a");
	} 


	//---------------------------------------------------------------------------------------------------------------------

}
