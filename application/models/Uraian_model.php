<?php
class Uraian_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function group_by($key, $data)
	{
		$result = array();
		// $result_ = array();
		foreach ($data as $val) {
			if (array_key_exists($key, $val)) {
				$result[$val[$key]][] = $val['desc'];
			} else {
				$result[""][] = $val;
			}
		}
		$result_ =	array_keys($result);
		foreach ($result_ as $title) {
			$pages_array[] = array('title' => $title, 'data' => $result[$title]);
		}
		return $pages_array;
	}

	public function getUraianBS()
	{
		$query = $this->db->query("SELECT `desc`,groupBS FROM uraian where for_to = 'bs' AND groupBS IS NOT NULL GROUP BY groupBS,`desc`");
		$arr = $query->result_array();
		$result = $this->group_by("groupBS", $arr);
		return $result;
	}
	public function getUraianIS()
	{
		$query = $this->db->query("SELECT `desc`
			FROM uraian where for_to = 'is'");

		return $query->result_array();
	}

	public function getUraian($ket)
	{
		$ket = $ket ? " where for_to =" . $this->db->escape($ket) : '';
		$query = $this->db->query("SELECT * FROM uraian " . $ket);

		return $query->result_array();
	}

	public function InsertDataUraian($forTo, $desc, $groupBS)
	{
		$query = "INSERT INTO uraian (for_to, `desc`,groupBS) VALUES(" . $forTo . ", " . $desc . "," . $groupBS . ");";
		$return_result = $this->db->query($query);
		return $return_result;
	}

	public function UpdateDataUraian($id, $forTo, $desc, $groupBS)
	{
		$query = "UPDATE uraian SET for_to=" . $forTo . ", `desc`=" . $desc . ", groupBS=" . $groupBS . " WHERE id=" . $id;
		$return_result = $this->db->query($query);
		return $return_result;
	}

	public function DeleteDataUraian($id)
	{
		$query = "DELETE FROM uraian WHERE Id =" . $this->db->escape($id);
		$return_result = $this->db->query($query);
		return $return_result;
	}
}
