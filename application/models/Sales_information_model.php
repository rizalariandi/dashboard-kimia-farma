<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_information_model extends MY_Model
{
    public function querys($head, $value, $where = NULL)
    {
        $sql = "
        SELECT 
          " . $this->filterheader($head) . " ,
           " . $this->filterheader_month($head) . ",
           DATE_FORMAT(tanggal_faktur,'%Y') tahun,
           SUM(target_quantity) target_qty_ty,
           IFNULL(SUM(" . $this->gettarget($value) . "),0) target_amount_ty,
          SUM(qty_ty) realisasi_qty_ty,
          SUM(" . $value . ") realisasi_ty,
          SUM(qty_ty) realisasi_qty_ly,
          IFNULL(SUM(" . $value . "),0) realisasi_ly,
          SUM(PTD_TY) ptd,
            0 achievement,
          0 growth,
        SUM(" . $value . ") / (
			SELECT SUM(c.grand_total) FROM (
				SELECT SUM(" . $value . ") grand_total 
				FROM datamart_kf.dm_market_operation1 
                WHERE 1=1 
                " . $this->condition($where, false) . "
				) c
			)  share_realisasi_ty,
       
            SUM(" . $value . ") / (
                SELECT SUM(c.grand_total) FROM (
                SELECT SUM(" . $this->getly($value) . ") grand_total
                FROM datamart_kf.dm_market_operation1
                WHERE 1=1
                " . $this->condition($where, false) . "
                ) c
                ) share_realisasi_ly,   
        SUM((" . $value . ") - (HPP_TY)) margin_marketing_amount,
        (SUM((" . $value . ") - (HPP_TY)) / SUM(" . $value . ")) margin_marketing_persen,
        SUM((hjd_ty) - (hjp_ty)) margin_distributor_amount,
        SUM((hjd_ty) - (hjp_ty))/SUM((hjd_ty)) margin_distributor_persen,
        0 noo,
        (COUNT(DISTINCT (CUSTOMER_CODE)) ) otr
        FROM datamart_kf.dm_market_operation1
        WHERE 1=1
        ";

        //echo nl2br($sql);die();
        return $sql;
    }

    public function querys_noo($head, $value, $where = NULL)
    {
        if ($head == 'Month') {
            $sql = "SELECT branch_name nama,bulan_tahun, COUNT(DISTINCT customer_code) res 
FROM datamart_kf.dm_market_operation1 WHERE 1=1 AND noo_desc = 'new_customer' ";
        } else {
            $sql = "SELECT branch_name nama,bulan_tahun, " . $head . ",COUNT(DISTINCT customer_code) res 
FROM datamart_kf.dm_market_operation1 WHERE 1=1 AND noo_desc = 'new_customer' ";
        }


        //echo nl2br($sql);die();
        return $sql;
    }

    public function querys_last($head, $value, $where = NULL, $distributor)
    {
        $sql = "
        SELECT 
          " . $this->filterheader($head) . " , 
           " . $this->filterheader_month($head) . ",
        
           DATE_FORMAT(tanggal_faktur,'%Y') tahun,
           SUM(target_quantity) target_qty_ty,
           SUM(" . $this->gettarget($value) . ") target_amount_ty,
          SUM(qty_ty) realisasi_qty_ty,
          SUM(" . $value . ") realisasi_ty,
          SUM(qty_ty) realisasi_qty_ly,
          SUM(" . $value . ") realisasi_ly,
          SUM(PTD_TY) ptd,
            0 achievement,
          0 growth,
        SUM(" . $value . ") / (
			SELECT SUM(c.grand_total) FROM (
				SELECT SUM(" . $value . ") grand_total 
				FROM datamart_kf.dm_market_operation1 
                WHERE 1=1 
                " . $this->condition_last($where) . "
				) c
			)  share_realisasi_ty,
       
            SUM(" . $value . ") / (
                SELECT SUM(c.grand_total) FROM (
                SELECT SUM(" . $this->getly($value) . ") grand_total
                FROM datamart_kf.dm_market_operation1
                WHERE 1=1
                " . $this->condition_last($where) . "
                ) c
                ) share_realisasi_ly,   
        SUM((" . $value . ") - (HPP_TY)) margin_marketing_amount,
        (SUM((" . $value . ") - (HPP_TY)) / SUM(" . $value . ")) margin_marketing_persen,
        SUM((hjd_ty) - (hjp_ty)) margin_distributor_amount,
        SUM((hjd_ty) - (hjp_ty))/SUM((hjd_ty)) margin_distributor_persen,
        0 noo,
        (COUNT(DISTINCT (CUSTOMER_CODE)) ) otr
        FROM datamart_kf.dm_market_operation1
        WHERE 1=1 
        " . $this->filterheader_branch($head, $distributor);;


        //echo nl2br($sql);die();
        return $sql;
    }
    public function queryrow($head, $value)
    {
        $sql = "
        SELECT 
        SUM(" . $value . ") realisasi_ty
        FROM datamart_kf.dm_market_operation1
        WHERE 1=1 
        ";
        //echo nl2br($sql);die();
        return $sql;
    }
    public function querylimit()
    {
        $query = "SELECT
        '' nama,
        '' target_qty_ty,
        ''  target_amount_ty,
        ''  realisasi_qty_ty,
        ''  realisasi_ty,
        ''  realisasi_qty_ly,
        ''  realisasi_ly,
        ''  ptd,
        ''  achievement,
        ''  growth,
        ''  share_realisasi_ty,
        ''  share_realisasi_ly,
        ''  margin_marketing_amount,
        ''  margin_marketing_persen,
        ''  margin_distributor_amount,
        ''  margin_distributor_persen,
        ''  noo,
        ''  otr
        ";

        return $query;
    }

    public function querydata_last($where = NULL, $head = NULL, $value = NULL, $distributor = NULL)
    {
        // var_dump($head);die();
        $query = $this->querys_last($head, $value, $where, $distributor);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition_last($where);
        }
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
            } else {

                $query .= " GROUP BY  " . $head;
            }
        }


        //echo nl2br($query);die();
        return $query;
    }

    public function querydata_noo($where = NULL, $head = NULL, $value = NULL)
    {
        // var_dump($head);die();
        $query = $this->querys_noo($head, $value, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition($where, false);
        }
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
            } else {

                $query .= " GROUP BY  " . $head;
            }
        }

        //echo nl2br($query);die();
        return $query;
    }
    public function querydata($where = NULL, $head = NULL, $value = NULL)
    {
        // var_dump($head);die();
        $query = $this->querys($head, $value, $where);
        //echo nl2br($query);die();
        if ($where != NULL) {
            $query .= $this->condition($where, false);
        }
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY DATE_FORMAT(tanggal_faktur,'%m%Y')";
            } else {

                $query .= " GROUP BY  " . $head;
            }
        }

        //echo nl2br($query);die();
        return $query;
    }
    public function get_limit_data($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $header = NULL, $value = NULL)
    {
        // echo $header;die();
        $query  = $this->querydata($where, $header, $value);
        //echo $value;die();
        //echo nl2br($query);die();
        if ($type == "data") {

            $query .= " LIMIT " . $start . "," . $limit . "";
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result_array();
            } else {
                return false;
            }
        } else if ($type == "numrow") {

            $query .= " LIMIT " . $start . "," . $limit . "";
            return $this->db->query($query)->num_rows();
        } else if ($type == "allrow") {

            return $this->db->query($query)->num_rows();
        } else if ($type == "excel") {
            //echo nl2br($query);die();
            //$query .= " LIMIT ".$start.",".$limit."";
            if ($this->db->query("select * from datamart_admedika.temp_dm_marketing_opration")->num_rows() > 0) {
                return $this->db->query("select * from datamart_admedika.temp_dm_marketing_opration")->result_array();
            } else {
                return false;
            }
        }
        // echo nl2br($query);die();

    }

    public function ordering_last($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL, $distributor = NULL)
    {

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydata_last($where, $header, $value, $distributor);
        //echo nl2br($querys);die();


        if ($type == "data") {
            //            if($start == 0){
            //                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration");//die();
            //                $create_tmp_data = "
            //                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration (
            //                SELECT * FROM (
            //                    ".$querys.")X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir']." );";
            //           // echo nl2br($create_tmp_data);die();
            //
            //            $this->db->query($create_tmp_data);//die();
            //
            //
            //            }


            $query = 'SELECT * FROM ( ' . $querys;
            if ($header == 'Month') {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY bulan_tahun " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY bulan_tahun " . $ordered['dir'];
                }
            } else {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY " . $header . " " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $header . " " . $ordered['dir'];
                }
            }

            $querylimit_total = 'SELECT * FROM (
                SELECT * FROM datamart_admedika.temp_dm_marketing_opration';

            $querylimit_total .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            $data['data'] = $this->db->query($query)->result_array();
            $data['grandtotal'] = [];
            return $data;
            //            if($this->db->query($querylimit_total)->num_rows()) {
            //
            //                $querytotal = "
            //
            //                    SELECT
            //                    'Grand Total' nama,
            //                    SUM(target_qty_ty) target_qty_ty ,
            //                    SUM(target_amount_ty) target_amount_ty,
            //                    SUM(realisasi_qty_ty) realisasi_qty_ty,
            //                    SUM(realisasi_ty) realisasi_ty,
            //                    SUM(realisasi_qty_ly) realisasi_qty_ly,
            //                    SUM(realisasi_ly) realisasi_ly,
            //                    SUM(ptd) ptd,
            //                    SUM(achievement) achievement,
            //                    SUM(growth) growth,
            //                    SUM(share_realisasi_ty) share_realisasi_ty ,
            //                    SUM(share_realisasi_ly) share_realisasi_ly,
            //                    SUM(margin_marketing_amount) margin_marketing_amount,
            //                    SUM(margin_marketing_persen) margin_marketing_persen,
            //                    SUM(margin_distributor_amount) margin_distributor_amount,
            //                    SUM(margin_distributor_persen) margin_distributor_persen,
            //                    SUM(noo) noo,
            //                    SUM(otr) otr
            //                    FROM (".$querylimit_total.") X;";
            //               // echo nl2br($querytotal);die();
            //                $data['data'] = $this->db->query($query)->result_array();
            //                $data['grandtotal'] = [];
            //                return $data;
            //                //$data['total'] = $this->db->query
            //
            //            }else{
            //                return false;
            //            }

        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }

            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration LIMIT " . $start . ",10";
            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = "";
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }

    public function all_row()
    {
        $queryrow = "SELECT * FROM datamart_kf.";
        return $this->db->query($queryrow)->num_rows();
    }
    public function ordering_noo($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydata_noo($where, $header, $value);
        //echo nl2br($querys);die();


        if ($type == "data") {
            //            if($start == 0){
            //                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration");//die();
            //                $create_tmp_data = "
            //                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration (
            //                SELECT * FROM (
            //                    ".$querys.")X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir']." );";
            //           // echo nl2br($create_tmp_data);die();
            //
            //            $this->db->query($create_tmp_data);//die();
            //
            //
            //            }

            $query = 'SELECT * FROM ( ' . $querys;
            if ($header == 'Month') {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY bulan_tahun " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY bulan_tahun " . $ordered['dir'];
                }
            } else {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY " . $header . " " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $header . " " . $ordered['dir'];
                }
            }

            $querylimit_total = 'SELECT * FROM (
                SELECT * FROM datamart_admedika.temp_dm_marketing_opration';

            $querylimit_total .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            $data['data'] = $this->db->query($query)->result_array();
            $data['grandtotal'] = [];
            return $data;
            //            if($this->db->query($querylimit_total)->num_rows()) {
            //
            //                $querytotal = "
            //
            //                    SELECT
            //                    'Grand Total' nama,
            //                    SUM(target_qty_ty) target_qty_ty ,
            //                    SUM(target_amount_ty) target_amount_ty,
            //                    SUM(realisasi_qty_ty) realisasi_qty_ty,
            //                    SUM(realisasi_ty) realisasi_ty,
            //                    SUM(realisasi_qty_ly) realisasi_qty_ly,
            //                    SUM(realisasi_ly) realisasi_ly,
            //                    SUM(ptd) ptd,
            //                    SUM(achievement) achievement,
            //                    SUM(growth) growth,
            //                    SUM(share_realisasi_ty) share_realisasi_ty ,
            //                    SUM(share_realisasi_ly) share_realisasi_ly,
            //                    SUM(margin_marketing_amount) margin_marketing_amount,
            //                    SUM(margin_marketing_persen) margin_marketing_persen,
            //                    SUM(margin_distributor_amount) margin_distributor_amount,
            //                    SUM(margin_distributor_persen) margin_distributor_persen,
            //                    SUM(noo) noo,
            //                    SUM(otr) otr
            //                    FROM (".$querylimit_total.") X;";
            //               // echo nl2br($querytotal);die();
            //                $data['data'] = $this->db->query($query)->result_array();
            //                $data['grandtotal'] = [];
            //                return $data;
            //                //$data['total'] = $this->db->query
            //
            //            }else{
            //                return false;
            //            }

        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }

            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration LIMIT " . $start . ",10";
            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = "";
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }

    public function grandtotal(){
        return ("        
        SUM(X.qty_hna) as qty_hna,
        SUM(X.target_hna) as target_hna,
        SUM(X.qty_hna_nett) as qty_hna_nett,
        SUM(X.target_hna_nett) as target_hna_nett,
        SUM(X.realisasi_qty_ty) as realisasi_qty_ty,
        SUM(X.realisasi_ty) as realisasi_ty,
        SUM(X.realisasi_ly) as realisasi_ly,
        SUM(X.growth) as growth,
        SUM(X.achievement_hna) as achievement_hna,
        SUM(X.achievement_hna_nett) as achievement_hna_nett,
        SUM(X.share_realisasi_ty) as share_realisasi_ty,
        SUM(X.share_realisasi_ly) as share_realisasi_ly,
        SUM(X.realisasi_qty_ly) as realisasi_qty_ly,
        SUM(X.ptd) as ptd,
        SUM(X.margin_marketing_amount) as margin_marketing_amount,
        SUM(X.margin_marketing_persen) as margin_marketing_persen,
        SUM(X.margin_distributor_amount) as margin_distributor_amount ,
        SUM(X.margin_distributor_persen) margin_distributor_persen,
        SUM(X.noo) as noo,
        SUM(X.npp) as npp,
        SUM(X.otr) as otr ");

    }
    
    public function npp($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL){
        $query = "
        SELECT COUNT(DISTINCT material_code) npp 
        FROM datamart_kf.dm_market_operation1 WHERE 1=1 AND noo_desc ='new_customer'";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        //$query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->npp;
    }

    public function ordering($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {
        // print_r($value);
        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $group = "";
        // $querys = $this->querydata($where, $header, $value);
        //echo nl2br($querys);die();
        if ($header == 'Month') {
            $group = "DATE_FORMAT(tanggal_faktur,'%m%Y')";
        } else {

            $group = $header;
        }

        if ($type == "data") {
            $query = "select A.nama,
            A.code,
            A.header_filter_month,
            A.branch_code,
            A.tahun,
            A.target_qty_ty,
            A.target_amount_ty,
            A.realisasi_qty_ty,
            A.realisasi_ty,
            B.realisasi_ty as realisasi_ly,
            (A.realisasi_ty- B.realisasi_ty)*100 / B.realisasi_ty as growth,
            A.realisasi_ty*100/(SELECT SUM(" . $value . ") FROM datamart_kf.dm_market_operation1  WHERE 1=1 " . $this->condition($where, false) . ") share_realisasi_ty,
            B.realisasi_ty*100/(SELECT SUM(" . $value . ") FROM datamart_kf.dm_market_operation1   WHERE 1=1 " . $this->condition($where, true) . ") share_realisasi_ly,
            B.realisasi_qty_ty as realisasi_qty_ly ,
            A.ptd,
            A.margin_marketing_amount,
            (A.margin_marketing_persen *100) as margin_marketing_persen,
            A.margin_distributor_amount,
            (A.margin_distributor_persen *100) as margin_distributor_persen,
            A.noo,
            A.npp,
            A.otr
            " . $this->selectHNA($group) . "            
            FROM ((SELECT " . $this->filterheader($header) . "," . $this->filterheader_month($header) . " as header_filter_month 
            ,branch_code as branch_code,
            DATE_FORMAT(tanggal_faktur,'%Y') tahun,
            SUM(target_quantity) target_qty_ty,
            IFNULL(SUM(TARGET_AMOUNT_HNA),0) target_amount_ty, 
            SUM(qty_ty) realisasi_qty_ty,
            SUM(" . $value . ") realisasi_ty,
            SUM(PTD_TY) ptd,
            SUM((" . $value . ") - (HPP_TY)) margin_marketing_amount,
            (SUM((" . $value . ") - (HPP_TY)) / SUM(" . $value . ")) margin_marketing_persen,
            SUM((hjd_ty) - (hjp_ty)) margin_distributor_amount, 
            SUM((hjd_ty) - (hjp_ty))/SUM((hjd_ty)) margin_distributor_persen,
            sum(noo_num) noo, sum(npp_num) npp, (COUNT(DISTINCT (CUSTOMER_CODE)) ) otr,
            bulan_faktur,
            material_code,
            layanan_group,
            lini_name
            " . $this->ASelect($group) . "
            FROM datamart_kf.dm_market_operation1   WHERE 1=1 
            " . $this->condition($where, false) . " GROUP BY " . $group . ") AS A 
            LEFT JOIN  (SELECT  " . $this->filterheader($header) . "
            ," . $this->filterheader_month($header) . " as header_filter_month,branch_code as branch_code,
            " . $this->BSelect($group) . "
            DATE_FORMAT(tanggal_faktur,'%Y') tahun,
            SUM(target_quantity) target_qty_ty,
            IFNULL(SUM(TARGET_AMOUNT_HNA),0) target_amount_ty, 
            SUM(qty_ty) realisasi_qty_ty,
            SUM(" . $value . ") realisasi_ty FROM datamart_kf.dm_market_operation1  
            WHERE 1=1 " . $this->condition($where, true) . " GROUP BY " . $group . ") 
            AS B on " . $this->AJoin($group) . " = " . $this->BJoin($group) . "
            " . $this->HNALEFT($group, $where) . "          
            )";
            // die($query);
            // where A.nama is not null and A.nama != '' and A.code is not null";
            // die($query);
            $querytotal = "                    
            SELECT
            ".$this->grandtotal()."
            FROM (".$query.") X;";
            // die($querytotal);
            $data['grandtotal'] = $this->db->query($querytotal)->row();
            $data['data'] = $this->db->query($query)->result_array();
            return $data;
        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }

            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration LIMIT " . $start . ",10";
            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = "";
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }

    public function ordering_backup($start = NULL, $limit = NULL, $where = NULL, $type = NULL, $ordered = NULL, $header = NULL, $value = NULL)
    {

        $query = $this->querylimit();
        $query .= " LIMIT 1";
        $setdataorder = $this->db->query($query)->result_array();

        foreach ($setdataorder[0] as $rowdataorder => $val) {
            $rows[] =  $rowdataorder;
        }

        //echo $header;die();
        $query  = "";
        $querys  = $this->querydata($where, $header, $value);
        //echo nl2br($querys);die();


        if ($type == "data") {
            //            if($start == 0){
            //                $this->db->query("DROP  TABLE IF EXISTS datamart_admedika.temp_dm_marketing_opration");//die();
            //                $create_tmp_data = "
            //                CREATE  TABLE IF NOT EXISTS datamart_admedika.temp_dm_marketing_opration (
            //                SELECT * FROM (
            //                    ".$querys.")X ORDER BY ".$rows[$ordered['column']]." ".$ordered['dir']." );";
            //           // echo nl2br($create_tmp_data);die();
            //
            //            $this->db->query($create_tmp_data);//die();
            //
            //
            //            }

            $query = 'SELECT * FROM ( ' . $querys;
            if ($header == 'Month') {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY bulan_tahun " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY bulan_tahun " . $ordered['dir'];
                }
            } else {
                if ($limit == '-1') {
                    $query .= ") X ORDER BY " . $header . " " . $ordered['dir'];
                } else {
                    $query .= " LIMIT " . $start . "," . $limit . ") X ORDER BY " . $header . " " . $ordered['dir'];
                }
            }


            $querylimit_total = 'SELECT * FROM (
                SELECT * FROM datamart_admedika.temp_dm_marketing_opration';

            $querylimit_total .= ") X ORDER BY " . $rows[$ordered['column']] . " " . $ordered['dir'];
            $data['data'] = $this->db->query($query)->result_array();
            $data['grandtotal'] = [];
            // die($query);
            return $data;
            //            if($this->db->query($querylimit_total)->num_rows()) {
            //
            //                $querytotal = "
            //
            //                    SELECT
            //                    'Grand Total' nama,
            //                    SUM(target_qty_ty) target_qty_ty ,
            //                    SUM(target_amount_ty) target_amount_ty,
            //                    SUM(realisasi_qty_ty) realisasi_qty_ty,
            //                    SUM(realisasi_ty) realisasi_ty,
            //                    SUM(realisasi_qty_ly) realisasi_qty_ly,
            //                    SUM(realisasi_ly) realisasi_ly,
            //                    SUM(ptd) ptd,
            //                    SUM(achievement) achievement,
            //                    SUM(growth) growth,
            //                    SUM(share_realisasi_ty) share_realisasi_ty ,
            //                    SUM(share_realisasi_ly) share_realisasi_ly,
            //                    SUM(margin_marketing_amount) margin_marketing_amount,
            //                    SUM(margin_marketing_persen) margin_marketing_persen,
            //                    SUM(margin_distributor_amount) margin_distributor_amount,
            //                    SUM(margin_distributor_persen) margin_distributor_persen,
            //                    SUM(noo) noo,
            //                    SUM(otr) otr
            //                    FROM (".$querylimit_total.") X;";
            //               // echo nl2br($querytotal);die();
            //                $data['data'] = $this->db->query($query)->result_array();
            //                $data['grandtotal'] = [];
            //                return $data;
            //                //$data['total'] = $this->db->query
            //
            //            }else{
            //                return false;
            //            }

        } else if ($type == "numrow") {
            //$queryrow = "SELECT * FROM temp_dm_marketing_opration" ;
            $where_c = "";
            if ($where != NULL) {
                $where_c .= $this->condition($where, false);
            }

            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration LIMIT " . $start . ",10";
            // echo nl2br($queryrow);die();
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "allrow") {
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->num_rows();
        } else if ($type == "excel") {
            $queryrow = "";
            $queryrow = "SELECT * FROM datamart_admedika.temp_dm_marketing_opration";
            return $this->db->query($queryrow)->result_array();
        }
        //echo $rows[$ordered['column']];die();
        //$ordered  = array();
        //print_r($ordered);die();
        //parse_str($order[0],$ordered);
        //print_r($ordered);die();
    }
    public function targetqty($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();
        //print_r($where);die();
        $query = "
        SELECT SUM(c.target_quantity) res
            FROM
            (
            SELECT distributor_code, branch_code, layanan_code, bulan_tahun, material_code, target_quantity
            FROM datamart_kf.dm_market_operation1
            where 1 = 1 " . $this->condition($where, false) . "
            GROUP BY distributor_code, branch_code, layanan_code, bulan_tahun, material_code, target_quantity
            ) c ";

        // print_r($header) ;
        // echo nl2br($query);die();
        return $this->db->query($query)->row()->res;
    }
    public function targetvalue($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        
            SELECT SUM((" . $this->gettarget($value) . ")) res
                  FROM 
                    (
                    SELECT   distributor_code, branch_code, layanan_code, bulan_tahun, material_code, (" . $this->gettarget($value) . ")
                    FROM datamart_kf.dm_market_operation1 
                    where 1 = 1 " . $this->condition($where, false) . "
                    GROUP BY distributor_code, branch_code, layanan_code, bulan_tahun, material_code, (" . $this->gettarget($value) . ")
                    ) c
        ";
        return $this->db->query($query)->row()->res;
    }
    public function realisasiqty($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
        SELECT SUM((QTY_TY)) res FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->res;
    }
    public function realisasivalue($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
        SELECT SUM((" . $value . ")) res FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->res;
    }
    public function lastyearqty($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
        SELECT SUM((QTY_TY)) res FROM datamart_kf.dm_market_operation1 WHERE 1=1
        ";
        if ($where != NULL) {
            $query  .= $this->condition_last($where);
        }
        $query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->res;
    }

    public function lastyearvalue($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
        SELECT SUM((" . $this->getly($value) . ")) res FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition_last($where);
        }
        $query .= $this->groupbyheadfilter($header);


        return $this->db->query($query)->row()->res;
    }
    public function ptd($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
        SELECT SUM((PTD_TY)) res FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);
        return $this->db->query($query)->row()->res;
    }
    public function achievement($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        select sum(res) * 100 res from(
            SELECT sum((" . $value . ")) /
            (SELECT SUM(c.target_amount)
            FROM
            (
            SELECT distributor_code, branch_code, layanan_code, bulan_tahun, material_code, (" . $this->gettarget($value) . ") target_amount
            FROM datamart_kf.dm_market_operation1
            where 1 = 1 " . $this->condition($where, false) . "
            GROUP BY distributor_code, branch_code, layanan_code, bulan_tahun, material_code, (TARGET_AMOUNT_HNA)
            ) c) res
            FROM datamart_kf.dm_market_operation1 WHERE 1=1";

        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);
        //echo nl2br($query);die();

        return $this->db->query($query)->row()->res;
    }
    public function growth($where = NULL, $header = NULL, $value = NULL)
    {
        //print_r($value);die();

        $query = "
        SELECT SUM(res) res FROM (
            SELECT SUM(( " . $this->getly($value) . ")) res 
            FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition_last($where);
        }
        $query .= $this->groupbyheadfilter($header);
        $sum_ly = $this->db->query($query)->row()->res;

        $query = "
        SELECT SUM(res) res FROM (
            SELECT SUM(" . $value . ") res 
            FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);

        $sum_ty = $this->db->query($query)->row()->res;

        $growth = (($sum_ty - $sum_ly) / $sum_ly) * 100;



        //echo nl2br($query);die();
        return $growth;
    }

    public function noo($where = NULL, $header = NULL, $value = NULL)
    {
        $query = "
        SELECT COUNT(noo_desc) noo 
        FROM datamart_kf.dm_market_operation1 WHERE 1=1 AND noo_desc = 'new_customer'";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        //$query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->noo;
    }

    public function otr($where = NULL, $header = NULL, $value = NULL)
    {
        $query = "
        SELECT SUM(res) res FROM (
            SELECT COUNT(DISTINCT CUSTOMER_CODE) res
        FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->res;
    }
    public function margin($where = NULL, $header = NULL, $value = NULL)
    {
        $query = "
        SELECT SUM(res) res FROM (
            SELECT SUM((" . $value . ")) res 
        FROM datamart_kf.dm_market_operation1 WHERE 1=1";
        if ($where != NULL) {
            $query  .= $this->condition($where, false);
        }
        $query .= $this->groupbyheadfilter($header);

        return $this->db->query($query)->row()->res;
    }

    public function gettarget($id = NULL)
    {
        if ($id == 'HPP_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HJP_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HJP_PTD_TY') {
            $id = 'TARGET_AMOUNT_HJP';
        } else if ($id == 'HNA_TY') {
            $id = 'TARGET_AMOUNT_HNA';
        } else if ($id == 'HJD_TY') {
            $id = 'TARGET_AMOUNT_HNA';
        }
        return $id;
    }

    public function getly($id = NULL)
    {
        if ($id == 'HPP_TY') {
            $id = 'HPP_TY';
        } else if ($id == 'HJP_TY') {
            $id = 'HJP_TY';
        } else if ($id == 'HJP_PTD_TY') {
            $id = 'HJP_PTD_TY';
        } else if ($id == 'HNA_TY') {
            $id = 'HNA_TY';
        } else if ($id == 'HJD_TY') {
            $id = 'HJD_TY';
        }
        return $id;
    }

    public function getly2($id = NULL)
    {
        if ($id == 'HPP_TY') {
            $id = 'HPP_LY';
        } else if ($id == 'HJP_TY') {
            $id = 'HJP_LY';
        } else if ($id == 'HJP_PTD_TY') {
            $id = 'HJP_PTD_LY';
        } else if ($id == 'HNA_TY') {
            $id = 'HNA_LY';
        } else if ($id == 'HJD_TY') {
            $id = 'HJD_LY';
        }
        return $id;
    }

    public function conditionDate($date)
    {
    $date_ = str_replace('/', '-', $date);
    $dateed =  date('Y-m-d', strtotime($date_));
    return $dateed;
    }
    
    public function getfiltervalue($id,$param,$name)
    {            
        $where ="where ";   
        // print_r($param->distributor_code[0]);    
        // print_r($id);         
        $select = "DISTINCT ".$id;     
        if ($name){
        $select .=" ,".$name; 
        }        
        $where = $where .$id." is not null ";  
        if($param->start && $param->end){
            $where .= "and tanggal_faktur BETWEEN '".$this->conditionDate($param->start)."' and '".$this->conditionDate($param->end)."' ";
        }
        if (count($param->distributor_code) > 0 && $id != "distributor_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->distributor_code) -1 ; $x++) {          
                 if ($x === count($param->distributor_code) - 1 ) {                   
                    $cat .= "'" . $param->distributor_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->distributor_code[$x] . "',";
                }
            }
                
            $where = $where . "and distributor_code in(".$cat.") ";                            
        }
        if (count($param->branch_code) > 0 && $id != "branch_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->branch_code) -1 ; $x++) {          
                 if ($x === count($param->branch_code) - 1 ) {                   
                    $cat .= "'" . $param->branch_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->branch_code[$x] . "',";
                }
            }                   
            $where = $where . "and branch_code in(".$cat.") ";     
                       
        }
        if (count($param->gpm_pm_code) > 0 && $id != "gpm_pm_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->gpm_pm_code) -1 ; $x++) {          
                 if ($x === count($param->gpm_pm_code) - 1 ) {                   
                    $cat .= "'" . $param->gpm_pm_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->gpm_pm_code[$x] . "',";
                }
            }                   
            $where = $where . "and gpm_pm_code in(".$cat.") ";     
                      
        }
        if (count($param->rsm_code) > 0 && $id != "rsm_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->rsm_code) -1 ; $x++) {          
                 if ($x === count($param->rsm_code) - 1 ) {                   
                    $cat .= "'" . $param->rsm_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->rsm_code[$x] . "',";
                }
            }                   
            $where = $where . "and rsm_code in(".$cat.") ";     
                      
        }
        if (count($param->shopper_code) > 0 && $id != "shopper_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->shopper_code) -1 ; $x++) {          
                 if ($x === count($param->shopper_code) - 1 ) {                   
                    $cat .= "'" . $param->shopper_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->shopper_code[$x] . "',";
                }
            }                   
            $where = $where . "and shopper_code in(".$cat.") ";     
                      
        }
        if (count($param->am_apm_asm_code) > 0 && $id != "am_apm_asm_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->am_apm_asm_code) -1 ; $x++) {          
                 if ($x === count($param->am_apm_asm_code) - 1 ) {                   
                    $cat .= "'" . $param->am_apm_asm_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->am_apm_asm_code[$x] . "',";
                }
            }                   
            $where = $where . "and am_apm_asm_code in(".$cat.") ";     
                      
        }
        if (count($param->msr_md_se_code) > 0 && $id != "msr_md_se_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->msr_md_se_code) -1 ; $x++) {          
                 if ($x === count($param->msr_md_se_code) - 1 ) {                   
                    $cat .= "'" . $param->msr_md_se_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->msr_md_se_code[$x] . "',";
                }
            }                   
            $where = $where . "and msr_md_se_code in(".$cat.") ";     
                      
        }
        if (count($param->segment) > 0 && $id != "segment")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->segment) -1 ; $x++) {          
                 if ($x === count($param->segment) - 1 ) {                   
                    $cat .= "'" . $param->segment[$x] . "'";
                } else {
                    $cat .= "'" . $param->segment[$x] . "',";
                }
            }                   
            $where = $where . "and segment in(".$cat.") ";     
                      
        }
        if (count($param->customer_code) > 0 && $id != "customer_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->customer_code) -1 ; $x++) {          
                 if ($x === count($param->customer_code) - 1 ) {                   
                    $cat .= "'" . $param->customer_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->customer_code[$x] . "',";
                }
            }                   
            $where = $where . "and customer_code in(".$cat.") ";     
                      
        }
        if (count($param->layanan_group) > 0 && $id != "layanan_group")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->layanan_group) -1 ; $x++) {          
                 if ($x === count($param->layanan_group) - 1 ) {                   
                    $cat .= "'" . $param->layanan_group[$x] . "'";
                } else {
                    $cat .= "'" . $param->layanan_group[$x] . "',";
                }
            }                   
            $where = $where . "and layanan_group in(".$cat.") ";     
                      
        }
        if (count($param->layanan_code) > 0 && $id != "layanan_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->layanan_code) -1 ; $x++) {          
                 if ($x === count($param->layanan_code) - 1 ) {                   
                    $cat .= "'" . $param->layanan_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->layanan_code[$x] . "',";
                }
            }                   
            $where = $where . "and layanan_code in(".$cat.") ";     
                      
        }
        if (count($param->lini_code) > 0 && $id != "lini_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->lini_code) -1 ; $x++) {          
                 if ($x === count($param->lini_code) - 1 ) {                   
                    $cat .= "'" . $param->lini_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->lini_code[$x] . "',";
                }
            }                   
            $where = $where . "and lini_code in(".$cat.") ";     
                      
        }
        if (count($param->material_group1_code) > 0 && $id != "material_group1_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->material_group1_code) -1 ; $x++) {          
                 if ($x === count($param->material_group1_code) - 1 ) {                   
                    $cat .= "'" . $param->material_group1_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->material_group1_code[$x] . "',";
                }
            }                   
            $where = $where . "and material_group1_code in(".$cat.") ";     
                      
        }
        if (count($param->material_group2_code) > 0 && $id != "material_group2_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->material_group2_code) -1 ; $x++) {          
                 if ($x === count($param->material_group2_code) - 1 ) {                   
                    $cat .= "'" . $param->material_group2_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->material_group2_code[$x] . "',";
                }
            }                   
            $where = $where . "and material_group2_code in(".$cat.") ";     
                      
        }
        if (count($param->material_group3_code) > 0 && $id != "material_group3_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->material_group3_code) -1 ; $x++) {          
                 if ($x === count($param->material_group3_code) - 1 ) {                   
                    $cat .= "'" . $param->material_group3_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->material_group3_code[$x] . "',";
                }
            }                   
            $where = $where . "and material_group3_code in(".$cat.") ";     
                      
        }
        if (count($param->brand) > 0 && $id != "brand")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->brand) -1 ; $x++) {          
                 if ($x === count($param->brand) - 1 ) {                   
                    $cat .= "'" . $param->brand[$x] . "'";
                } else {
                    $cat .= "'" . $param->brand[$x] . "',";
                }
            }                   
            $where = $where . "and brand in(".$cat.") ";     
                      
        }  
        if (count($param->material_code) > 0 && $id != "material_code")
        {
            $cat ="";
            for ($x = 0; $x <= count($param->material_code) -1 ; $x++) {          
                 if ($x === count($param->material_code) - 1 ) {                   
                    $cat .= "'" . $param->material_code[$x] . "'";
                } else {
                    $cat .= "'" . $param->material_code[$x] . "',";
                }
            }                   
            $where = $where . "and material_code in(".$cat.") ";     
                      
        }                
        $query = "SELECT " . $select ." from datamart_kf.dm_market_operation1 ". $where;       
        // die($query);
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return array();
        }
    }

    public function getfiltervalue_branch($id, $distributor)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($distributor)) !== 0) {
            foreach ($distributor as $key => $value) {
                $i++;

                if ($i === sizeof($distributor)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT branch_code,branch_name FROM datamart_kf.dm_market_operation1 where distributor_code in (' . $cat . ') AND branch_code IS NOT NULL AND branch_name Like ("KF%") group by branch_code,branch_name order by branch_name ASC;';
        } else {
            $query = 'SELECT branch_code,branch_name FROM datamart_kf.dm_market_operation1 where branch_code IS NOT NULL AND branch_name Like ("KF%") group by branch_code,branch_name order by branch_name ASC;';
        }
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_gm($id, $branch)
    {
        $i = 0;
        $cat = "";

        //        if (sizeof(($branch)) !== 0) {
        //            foreach ($branch as $key => $value) {
        //                $i++;
        //
        //                if ($i === sizeof($branch)) {
        //                    $cat .= "'" . $value . "'";
        //                } else {
        //                    $cat .= "'" . $value . "',";
        //                }
        //            }
        //            $query = 'SELECT gpm_pm_code,jabatan_pm FROM datamart_kf.dm_market_operation1 where branch_code in ('.$cat.') AND gpm_pm_code IS NOT NULL group by gpm_pm_code,jabatan_pm order by jabatan_pm ASC;';
        //        } else {
        //            $query = 'SELECT gpm_pm_code,jabatan_pm FROM datamart_kf.dm_market_operation1 where gpm_pm_code IS NOT NULL group by gpm_pm_code,jabatan_pm order by jabatan_pm ASC;';
        //        }
        $query = 'SELECT DISTINCT pm_no as gpm_pm_code,pm_name as jabatan_pm from datamart_kf.dm_market_operation1 order by jabatan_pm ASC;';
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_am_apm_asm()
    {
        $query = 'SELECT DISTINCT am_no as am_apm_asm_code,am_name as jabatan_am from datamart_kf.dm_market_operation1 order by jabatan_am ASC;';
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_group3($id, $lini)
    {
        $i = 0;
        $cat = "";
        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT material_group3_code,material_group3_name FROM datamart_kf.dm_market_operation1 where material_group3_code IS NOT NULL AND material_group3_code != "" and lini_code in (' . $cat . ') group by material_group3_code,material_group3_name order by material_group3_name ASC;';
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result();
            } else {
                return false;
            }
        } else {
            return array();
        }
    }

    public function getfiltervalue_group2($id, $lini)
    {
        $i = 0;
        $cat = "";
        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT material_group2_code,material_group2_name FROM datamart_kf.dm_market_operation1 where material_group2_code IS NOT NULL AND material_group2_code != "" and lini_code in (' . $cat . ') group by material_group2_code,material_group2_name order by material_group2_name ASC;';
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result();
            } else {
                return false;
            }
        } else {
            return array();
        }
    }
    public function getfiltervalue_group1($id, $lini)
    {
        $i = 0;
        $cat = "";
        if (sizeof(($lini)) !== 0) {
            foreach ($lini as $key => $value) {
                $i++;

                if ($i === sizeof($lini)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT material_group1_code,material_group1_name FROM datamart_kf.dm_market_operation1 where material_group1_code IS NOT NULL AND material_group1_code != "" and lini_code in (' . $cat . ') group by material_group1_code,material_group1_name order by material_group1_name ASC;';
            if ($this->db->query($query)->num_rows() > 0) {
                return $this->db->query($query)->result();
            } else {
                return false;
            }
        } else {
            return array();
        }
    }

    public function getfiltervalue_customer($id, $segment)
    {
        $i = 0;
        $cat = "";
        // if (sizeof(($segment)) !== 0) {
        //     foreach ($segment as $key => $value) {
        //         $i++;

        //         if ($i === sizeof($segment)) {
        //             $cat .= "'" . $value . "'";
        //         } else {
        //             $cat .= "'" . $value . "',";
        //         }
        //     }
        //     $query = 'SELECT customer_code,customer_name FROM datamart_kf.dm_market_operation1 where segment in ('.$cat.') AND customer_code IS NOT NULL group by customer_code,customer_name order by customer_name ASC;';            
        // } else {
        //     $query = 'SELECT customer_code,customer_name FROM datamart_kf.dm_market_operation1 where customer_code IS NOT NULL AND customer_name is not null group by customer_code,customer_name order by customer_name ASC;';

        // }
        // if($this->db->query($query)->num_rows() > 0 ){     
        if (sizeof(($segment)) !== 0) {
            foreach ($segment as $key => $value) {
                $i++;

                if ($i === sizeof($segment)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT customer_code,customer_name FROM datamart_kf.dm_market_operation1 where customer_name LIKE "%' . $id . '%" and segment in(' . $cat . ') group by customer_code,customer_name order by customer_name ASC limit 20;';
            return $this->db->query($query)->result();
        } else {
            $query = 'SELECT customer_code,customer_name FROM datamart_kf.dm_market_operation1 where customer_name LIKE "%' . $id . '%" group by customer_code,customer_name order by customer_name ASC limit 20;';
            return $this->db->query($query)->result();
        }

        // $query = "SELECT customer_code,customer_name FROM datamart_kf.dm_market_operation1 where customer_name LIKE '%HIKMA%' group by customer_code,customer_name order by customer_name ASC limit 5;";                

        // }else{            
        //     return false;
        // }
    }

    public function getfiltervalue_layanan($id, $customer)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($customer)) !== 0) {
            foreach ($customer as $key => $value) {
                $i++;

                if ($i === sizeof($customer)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT layanan_code,layanan_name FROM datamart_kf.dm_market_operation1 where layanan_group in (' . $cat . ') AND layanan_code IS NOT NULL group by layanan_code,layanan_name order by layanan_name ASC;';
        } else {
            return array();
        }
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_lini($id, $layanan)
    {
        $i = 0;
        $cat = "";
        //
        //        if (sizeof(($layanan)) !== 0) {
        //            foreach ($layanan as $key => $value) {
        //                $i++;
        //
        //                if ($i === sizeof($layanan)) {
        //                    $cat .= "'" . $value . "'";
        //                } else {
        //                    $cat .= "'" . $value . "',";
        //                }
        //            }
        //            $query = 'SELECT lini_code,lini_name FROM datamart_kf.dm_market_operation1 where layanan_code in ('.$cat.') AND lini_code IS NOT NULL group by lini_code,lini_name order by lini_name ASC;';
        //        } else {
        //            $query = 'SELECT lini_code,lini_name FROM datamart_kf.dm_market_operation1 where lini_code IS NOT NULL group by lini_code,lini_name order by layanan_name ASC;';
        //        }
        $query = 'SELECT DISTINCT lini_code,lini_name FROM datamart_kf.dm_market_operation1';
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function getfiltervalue_product($id, $brand)
    {
        $i = 0;
        $cat = "";

        if (sizeof(($brand)) !== 0) {
            foreach ($brand as $key => $value) {
                $i++;

                if ($i === sizeof($brand)) {
                    $cat .= "'" . $value . "'";
                } else {
                    $cat .= "'" . $value . "',";
                }
            }
            $query = 'SELECT material_code,material_name FROM datamart_kf.dm_market_operation1 where brand in (' . $cat . ') AND material_code IS NOT NULL group by material_code,material_name order by material_name ASC;';
        } else {
            $query = 'SELECT material_code,material_name FROM datamart_kf.dm_market_operation1 where material_code IS NOT NULL group by material_code,material_name order by material_name ASC;';
        }
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }



    public function f_tahun()
    {
        $query = "        
            SELECT RIGHT(BULAN_TAHUN,4) f_tahun 
            FROM datamart_kf.dm_market_operation1
            GROUP BY RIGHT(BULAN_TAHUN,4)
        ";
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }
    public function f_bulan()
    {
        $query = "        
            SELECT LEFT(BULAN_TAHUN,1) f_bulan
            FROM datamart_kf.dm_market_operation1
            GROUP BY LEFT(BULAN_TAHUN,1)
        ";
        if ($this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } else {
            return false;
        }
    }

    public function condition_case($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $dateed =  date('Y-m-d', strtotime($date));

                    $where .= " tanggal_faktur between '" . $dateed . "'";
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_end =  date('Y-m-d', strtotime($date));
                    $where .= " AND '" . $date_end . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {

                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";

                    for ($i = 0; $i < count($val); $i++) {
                        if ($val[$i] == "111") {
                            $val_where = "108'" . "," . "'105";
                        } else {
                            $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        }



                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }

        return $where;
    }

    public function condition_case_dated($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $dateed =  date('Y-m-d', strtotime($date));

                    $where .= " tanggal_faktur between '" . $dateed . "'";
                }
            }
        }

        return $dateed;
    }

    public function HNALEFT($group = NULL, $where = NULL)
    {
        if ($group  === 'branch_code' || $group  === "DATE_FORMAT(tanggal_faktur,'%m%Y')" || $group  === 'layanan_group' || $group  === 'lini_code' 
        || $group  === 'material_code' || $group  === 'brand' || $group  === 'material_group1_code' || $group  === 'material_group2_code' 
        || $group  === 'am_apm_asm_code' || $group  === 'gpm_pm_code' || $group  === 'rsm_code') {
            return "LEFT JOIN (select  sum(qty) qty,
            sum(value) value,
            branch,tahun_bulan,
            lini,lini_code,material,brand,material_group1_code,
            material_group2_code,am_apm_asm_code,gpm_pm_code,rsm_code,
            groupinglayanan from datamart_kf.kf_target_hna_manipulated where 1=1" . $this->whereHNA($where) . " GROUP BY " . $this->groupHNA($group) . ")
            as C on " . $this->ONHNA($group, 'C') . "
            LEFT JOIN ( select sum(qty) qty,
            sum(value) value,
            branch,tahun_bulan,
            lini,lini_code,material,brand,material_group1_code,
            material_group2_code,am_apm_asm_code,gpm_pm_code,rsm_code,
            groupinglayanan from datamart_kf.kf_target_nett_manipulated where 1=1" . $this->whereHNA($where) . " GROUP BY " . $this->groupHNA($group) . ")
            as D  on " . $this->ONHNA($group, 'D');
        } else {
            return '';
        }
    }

    public function selectHNA($cond = NULL)
    {
        if ($cond  === 'branch_code' || $cond  === "DATE_FORMAT(tanggal_faktur,'%m%Y')" || $cond  === 'layanan_group' || $cond  === 'lini_code' 
        || $cond  === 'material_code' || $cond  === 'brand' || $cond  === 'material_group1_code' || $cond  === 'material_group2_code' 
        || $cond  === 'am_apm_asm_code' || $cond  === 'gpm_pm_code' || $cond  === 'rsm_code') {
            return ",round(C.qty,0) as qty_hna,
           round(C.value,0) as target_hna,
           round(D.qty) as qty_hna_nett,
           round(D.value,0) as target_hna_nett,
           A.realisasi_ty*100/ round(C.value,0) as achievement_hna,
           A.realisasi_ty*100/round(D.value,0) as achievement_hna_nett
           ";
        } else {
            return ',"-" qty_hna, "-" target_hna, "-" qty_hna_nett,"-" target_hna_nett, "-" achievement_hna, "-" achievement_hna_nett';
        }
    }

    public function ONHNA($cond = NULL, $i)
    {
        if ($cond === 'branch_code') {
            if ($i === 'C') {
                return "A.branch_code = C.branch";
            } else {
                return "A.branch_code = D.branch";
            }
        } else if ($cond == "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            if ($i === 'C') {
                return "A.bulan_faktur = C.tahun_bulan";
            } else {
                return "A.bulan_faktur = D.tahun_bulan";
            }
        } else if ($cond == "layanan_group") {
            if ($i === 'C') {
                return "UPPER(A.layanan_group) = UPPER(C.groupinglayanan)";
            } else {
                return "UPPER(A.layanan_group) = UPPER(D.groupinglayanan)";
            }
        } else if ($cond == "lini_code") {
            if ($i === 'C') {
                return "UPPER(TRIM(A.code)) = UPPER(TRIM(C.lini_code))";
            } else {
                return "UPPER(TRIM(A.code)) = UPPER(TRIM(D.lini_code))";
            }
        }if ($cond === 'material_code') {
            if ($i === 'C') {
                return "A.material_code = C.material";
            } else {
                return "A.material_code = D.material";
            }
        }
        if ($cond === 'brand' || $cond === 'material_group1_code' || $cond === 'material_group1_code' 
        || $cond === 'material_group2_code' || $cond === 'am_apm_asm_code' || $cond === 'gpm_pm_code' || $cond === 'rsm_code') {
            if ($i === 'C') {
                return "A.".$cond." = C.".$cond;
            } else {
                return "A.".$cond." = D.".$cond;
            }
        }

        // $group  === 'brand' || $group  === 'material_group1_code' || $group  === 'material_group2_code' 
        // || $group  === 'am_apm_asm_code' || $group  === 'gpm_pm_code' || $group  === 'rsm_code')
    }

    public function groupHNA($cond = NULL)
    {
        if ($cond == 'branch_code') {
            return "branch";
        } else if ($cond == "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            return "tahun_bulan";
        } else if ($cond == 'layanan_group') {
            return "groupinglayanan";
        } else if ($cond == 'lini_code') {
            return "lini";
        } else if ($cond == 'material_code') {
            return "material";
        }
        else if ($cond === 'brand' || $cond === 'material_group1_code' || $cond === 'material_group1_code' 
        || $cond === 'material_group2_code' || $cond === 'am_apm_asm_code' || $cond === 'gpm_pm_code' || $cond === 'rsm_code') {
            return $cond;
        }
    }

    public function whereHNA($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $where =  " AND tahun_bulan >=" . date('Ym', strtotime($date));
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $where .=  " AND tahun_bulan <=" . date('Ym', strtotime($date));
                }
            }
        }

        return $where;
    }

    public function ASelect($group)
    {
        if ($group === "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            return ",tanggal_faktur";
        } else if ($group === "branch_code" || $group ===  "material_code" || $group === 'layanan_group') {
            return '';
        } else {
            return "," . $group;
        }
    }

    public function BSelect($group)
    {
        if ($group === "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            return "bulan_tahun,";
        } else if ($group === "branch_code") {
            return '';
        } else {
            return $group . ",";
        }
    }



    public function AJoin($group)
    {
        if ($group === "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            return "DATE_FORMAT(DATE_SUB(A.tanggal_faktur,INTERVAL 1 YEAR),'%m%Y')";
        } else {
            return "A." . $group;
        }
    }

    public function BJoin($group)
    {
        if ($group === "DATE_FORMAT(tanggal_faktur,'%m%Y')") {
            return "B.bulan_tahun";
        } else {
            return "B." . $group;
        }
    }

    public function condition($cond = NULL, $last)
    {        
        if ($cond != NULL) {
            $where = '';
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);                   
                    $dateed =  date('Y-m-d', strtotime($date));                    
                    if ($last) {
                        $dateed = "DATE_SUB('" . $dateed . "',INTERVAL 1 YEAR)";
                        $where .= " AND tanggal_faktur between " . $dateed;
                    } else {
                        $where .= " AND tanggal_faktur between '" . $dateed . "'";
                    }
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_end =  date('Y-m-d', strtotime($date));
                    if ($last) {
                        $date_end = "DATE_SUB('" . $date_end . "',INTERVAL 1 YEAR)";
                        $where .= " AND " . $date_end;
                    } else {
                        $where .= " AND '" . $date_end . "'";
                    }
                } else if ($key == 'value') {
                    $where .= "";
                } else {

                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";

                    for ($i = 0; $i < count($val); $i++) {
                        if ($val[$i] == "111") {
                            $val_where = "108'" . "," . "'105";
                        } else {
                            $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        }



                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }

        return $where;
    }

    public function condition_last($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $date_ly = array();
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);

                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $dateed =  date('Y-m-d', strtotime($date));

                    $where .= " AND tanggal_faktur between '" . $dateed . "'";
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);
                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $date_end =  date('Y-m-d', strtotime($date));
                    $where .= " AND '" . $date_end . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {

                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";

                    for ($i = 0; $i < count($val); $i++) {
                        if ($val[$i] == "111") {
                            $val_where = "108'" . "," . "'105";
                        } else {
                            $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        }



                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }

        return $where;
    }

    public function condition_last_case($cond = NULL)
    {
        if ($cond != NULL) {
            $where = '';
            $date_ly = array();
            $count_filter = count($cond);
            foreach ($cond as $key => $val) {

                if ($key == 'tanggal_faktur_start') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);

                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $dateed =  date('Y-m-d', strtotime($date));

                    $where .= " tanggal_faktur between '" . $dateed . "'";
                } else if ($key == 'tanggal_faktur_end') {
                    $var = $val;
                    $date = str_replace('/', '-', $var);
                    $date_ly = explode("-", $date);
                    $date = ($date_ly[2] - 1) . "-" . $date_ly[1] . "-" . $date_ly[0];
                    $date_end =  date('Y-m-d', strtotime($date));
                    $where .= " AND '" . $date_end . "'";
                } else if ($key == 'value') {
                    $where .= "";
                } else {

                    //$where .= "AND ".$key." in (";
                    $where .= "AND IFNULL(" . $key . ",'') in (";

                    for ($i = 0; $i < count($val); $i++) {
                        if ($val[$i] == "111") {
                            $val_where = "108'" . "," . "'105";
                        } else {
                            $val_where = $val[$i] == 'null' ? '' : $val[$i];
                        }



                        if (count($val) - $i > 1) {
                            $where .= " '" . $val_where . "' , ";
                        } else {
                            $where .= " '" . $val_where . "'  ";
                        }
                    }
                    $where .= ")";
                }
            }
        }

        return $where;
    }
    public function getdatabar($where = NULL, $head = NULL, $value)
    {

        $headname = $this->filterheader($head);
        //echo $headname;die();
        if (strlen($head) > 0) {
            if ($head != 'Month') {
                $query = "
                SELECT 
                " . $headname . " ,
                (SELECT SUM(c.target_value)
                FROM   (SELECT bulan_tahun,
                            distributor_code,
                            branch_code,
                            layanan_code,
                            material_code,
                            ( " . $this->gettarget($value) . " ) AS TARGET_VALUE
                        FROM   datamart_kf.dm_market_operation1
                        WHERE  1 = 1
                        " . $this->condition($where, false) . "
                        GROUP  BY bulan_tahun,
                                distributor_code,
                                branch_code,
                                layanan_code,
                                material_code,
                                ( " . $this->gettarget($value) . " )) c)  sales_target,
                SUM(" . $value . ") realisasi_next,
                SUM(" . $this->getly($value) . ") realisasi_prev
                FROM datamart_kf.dm_market_operation1
                WHERE 1=1";
            } else {
                $query = "
                SELECT 
                CASE 
                WHEN LEFT(bulan_tahun,2) = '01' THEN 'Januari'
                WHEN LEFT(bulan_tahun,2) = '02' THEN 'Februari'
                WHEN LEFT(bulan_tahun,2) = '03' THEN 'Maret'
                WHEN LEFT(bulan_tahun,2) = '04' THEN 'April'
                WHEN LEFT(bulan_tahun,2) = '05' THEN 'Mei'
                WHEN LEFT(bulan_tahun,2) = '06' THEN 'Juni'
                WHEN LEFT(bulan_tahun,2) = '07' THEN 'Juli'
                WHEN LEFT(bulan_tahun,2) = '08' THEN 'Agustus'
                WHEN LEFT(bulan_tahun,2) = '09' THEN 'September'
                WHEN LEFT(bulan_tahun,2) = '10' THEN 'Oktober'
                WHEN LEFT(bulan_tahun,2) = '11' THEN 'November'
                WHEN LEFT(bulan_tahun,2) = '12' THEN 'Desember'
                ELSE 'Bulan Tidak Ditemukan'
                END nama,
                (SELECT SUM(c.target_value)
                FROM   (SELECT bulan_tahun,
                            distributor_code,
                            branch_code,
                            layanan_code,
                            material_code,
                            ( " . $this->gettarget($value) . " ) AS TARGET_VALUE
                        FROM   datamart_kf.dm_market_operation1
                        WHERE  1 = 1
                        " . $this->condition($where, false) . "
                        GROUP  BY bulan_tahun,
                                distributor_code,
                                branch_code,
                                layanan_code,
                                material_code,
                                ( " . $this->gettarget($value) . " )) c)  sales_target,
                SUM(" . $value . ") realisasi_next,
                SUM(" . $this->getly($value) . ") realisasi_prev
                FROM datamart_kf.dm_market_operation1
                WHERE 1=1";
            }
        } else {
            $query = "
            SELECT 
            'All/No Grouping' name ,
            (SELECT SUM(c.target_value)
            FROM   (SELECT bulan_tahun,
                        distributor_code,
                        branch_code,
                        layanan_code,
                        material_code,
                        ( " . $this->gettarget($value) . " ) AS TARGET_VALUE
                    FROM   datamart_kf.dm_market_operation1
                    WHERE  1 = 1
                    " . $this->condition($where, false) . "
                    GROUP  BY bulan_tahun,
                            distributor_code,
                            branch_code,
                            layanan_code,
                            material_code,
                            ( " . $this->gettarget($value) . " )) c)  sales_target,
            SUM(" . $value . ") realisasi_next,
            SUM(" . $this->getly($value) . ") realisasi_prev
            FROM datamart_kf.dm_market_operation1
            WHERE 1=1";
        }


        if ($where != NULL) {
            $query .= $this->condition($where, false);
        }
        if (strlen($head) > 0) {
            if ($head == 'Month') {
                $query .= " GROUP BY date_format(tanggal_faktur,'%m') ";
            } else {
                $query .= " GROUP BY " . $head;
            }
        }

        //echo nl2br($query);die();
        //GROUP BY LEFT(bulan_tahun,1),RIGHT(bulan_tahun,4)";
        return $this->db->query($query)->result_array();
    }
    public function filterheader($head)
    {
        $headname = "";
        if ($head == 'Month') {
            $headname =  "date_format(tanggal_faktur,'%m') as nama,'-' as code";
        } else if ($head == 'distributor_code') {
            // $headname = "CONCAT(CONCAT(distributor_name,'-'),distributor_code) AS nama,distributor_code as code";
            $headname = "distributor_name AS nama,distributor_code as code";
        } else if ($head == 'branch_code') {
            // $headname = "CONCAT(CONCAT(branch_name,'-'),branch_code) AS nama";
            $headname = "branch_name AS nama,branch_code as code";
        } else if ($head == 'gpm_pm_code') {
            // $headname = "CONCAT(CONCAT(jabatan_pm,'-'),gpm_pm_code) AS nama,branch_code as gpm_pm_code";
            $headname = "jabatan_pm AS nama,'-' as code";
        } else if ($head == 'rsm_code') {
            // $headname = "CONCAT(CONCAT(jabatan_rsm,'-'),rsm_code) AS nama, rsm_code as code";
            $headname = "jabatan_rsm AS nama, '-' as code";
        } else if ($head == 'shopper_code') {
            // $headname = "CONCAT(CONCAT(jabatan_shopper,'-'),shopper_code) AS nama,shopper_code as code";
            $headname = "jabatan_shopper AS nama,'-' as code";
        } else if ($head == 'am_apm_asm_code') {
            // $headname = "CONCAT(CONCAT(jabatan_am,'-'),am_apm_asm_code) AS nama,am_apm_asm_code as code";
            $headname = "jabatan_am AS nama,'-' as code";
        } else if ($head == 'msr_md_se_code') {
            // $headname = "CONCAT(CONCAT(msr_md_se_name,'-'),msr_md_se_code) AS nama,as code";
            $headname = "msr_md_se_name AS nama,'-' as code";
        } else if ($head == 'segment') {
            // $headname = "SEGMENT AS nama,'-' as code";
            $headname = "SEGMENT AS nama,'-' as code";
        } else if ($head == 'customer_code') {
            // $headname = "CONCAT(CONCAT(customer_name,'-'),customer_code) AS nama,customer_code as code";
            $headname = "customer_name AS nama,customer_code as code";
        } else if ($head == 'layanan_code') {
            // $headname = "CONCAT(CONCAT(layanan_name,'-'),layanan_code) AS nama,layanan_code as code";
            $headname = "layanan_name AS nama,layanan_code as code";
        } else if ($head == 'layanan_group') {
            // $headname = "layanan_group AS nama,'-' as code";
            $headname = "layanan_group AS nama,'-' as code";
        } else if ($head == 'lini_code') {
            // $headname = "CONCAT(CONCAT(lini_name,'-'),lini_code) AS nama,lini_code as code";
            $headname = "lini_name AS nama,lini_code as code";
        } else if ($head == 'material_group1_code') {
            // $headname = "CONCAT(CONCAT(material_group1_name,'-'),material_group1_code) AS nama,material_group1_code as code";
            $headname = "material_group1_name AS nama,material_group1_code as code";
        } else if ($head == 'material_group2_code') {
            // $headname = "CONCAT(CONCAT(material_group2_name,'-'),material_group2_code) AS nama,material_group2_code as code";
            $headname = "material_group2_name AS nama,material_group2_code as code";
        } else if ($head == 'material_group3_code') {
            // $headname = "CONCAT(CONCAT(material_group3_name,'-'),material_group3_code) AS nama,material_group3_code as code";
            $headname = "material_group3_name AS nama,material_group3_code as code";
        } else if ($head == 'brand') {
            // $headname = "brand AS nama,as code";
            $headname = "brand AS nama,'-' as code";
        } else if ($head == 'material_code') {
            // $headname = " CONCAT(CONCAT(CONCAT(CONCAT(material_name,'-'),kemasan),'-'), material_code ) AS nama,material_code as code";
            $headname = "CONCAT(CONCAT(CONCAT(material_name,'-'),kemasan),'-') AS nama,material_code as code";
        }
        //  echo $headname;die();
        return $headname;
    }
    public function filterheader_branch($head, $distributor)
    {
        $headname = "";
        if ($distributor != 'APL') {
            if ($head == 'branch_code') {
                $headname = " AND branch_name LIKE 'KF%' AND branch_code != 2101 ";
            }
        }

        return $headname;
    }

    public function filterheader_month($head)
    {
        $headname = "";
        if ($head == 'Month') {
            $headname = "bulan_tahun ";
        } else {
            $headname = $head;
        }
        return $headname;
    }
    function groupbyheadfilter($header = NULL)
    {
        $query = "";
        if (strlen($header['head_filter']) > 0) {
            if ($header['head_filter'] == 'Month') {
                $query .= " GROUP BY  date_format(tanggal_faktur,'%m') )x";
            } else {

                $query .= " GROUP BY " . $header['head_filter'] . ")x";
            }
        } else {
            $query = ")x";
        }
        return $query;
    }
}
