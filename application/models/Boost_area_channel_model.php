<?php
class Boost_area_channel_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function area_channel_get_list_lini()
    {
        return $this->db->distinct()->select('lini')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_list_layanan()
    {
        return $this->db->distinct()->select('layanan')->where("layanan is not null", NULL, FALSE)->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_kftd_channel()
    {
        return $this->db->distinct()->select('IF(channel = "MARCKS\'", "MARCKS", REPLACE(channel, ".", "")) as channel ')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_kftd()
    {
        return $this->db->distinct()->select('nama_kftd')->get('usc_bst_sales_area_channel')->result();
    }

    function area_channel_get_revenue_total($from, $to, $lini, $layanan){
        if(!is_array($lini)){
            $data_lini = explode(",", $lini);
        }else{
            $data_lini = $lini;
        }

        if(!is_array($layanan)){
            $data_layanan = explode(",", $layanan);
        }else{
            $data_layanan = $layanan;
        }

        // var_dump("lini = ",gettype($lini), "layanan = ",gettype($layanan));
        // die;
        // $data_lini = (object) $lini;
        // $data_layanan = (object) $layanan;
        // var_dump($data_layanan);
        // $data_lini = explode(",", $lini);
        // $data_layanan = explode(",", $layanan);
        
        
        $this->db->select("sum(revenue) as total");
        // $this->db->like("provinsi", $provinsi);
        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);

        if(count($data_lini) >= 0 && $data_lini[0] != ''){
            // $this->db->group_start();
            // foreach ($data_lini as $lin) {
            //     $this->db->or_like('lini', $lin);
            // }
            // $this->db->group_end();

            $this->db->group_start();
            $this->db->where_in('lini', $data_lini);
            $this->db->group_end();
        }
        
        if(count($data_layanan) >= 0 && $data_layanan[0] != ''){
            // $this->db->group_start();
            // foreach ($data_layanan as $lay){
            //     $this->kf->or_like('layanan', $lay);
            // }
            // $this->db->group_end();

            $this->db->group_start();
            $this->db->where_in('layanan', $data_layanan);
            $this->db->group_end();
        }

        $data = $this->db->get('usc_bst_sales_area_channel');

        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }
    }

    function area_channel_get_revenue($provinsi, $from, $to, $lini, $layanan){
        // ================ initial code =================
        // $range = $this->input->post("range", true);
        // if($range == ""){
        //     $range = "2018-01-01 / 2019-12-30";
        // }
        // // $provinsi = $this->input->post("provinsi", true);
        // $tanggal = str_replace(" ","",$range);
		// $tanggal = str_replace("/",",",$tanggal);
		// $tgl =  explode(",", $tanggal);
        // $from = $tgl[0];
        // $to = $tgl[1];
        // $lini = $this->input->post("lini", true);
        // $layanan = $this->input->post("layanan", true);
        // ================================================
        
        if(!is_array($lini)){
            $data_lini = explode(",", $lini);
        }else{
            $data_lini = $lini;
        }
        if(!is_array($layanan)){
            $data_layanan = explode(",", $layanan);
        }else{
            $data_layanan = $layanan;
        }

        // $data_lini = (object) $lini;
        // $data_layanan = (object) $layanan;
        // var_dump($data_layanan);
        // $data_lini = explode(",", $lini);
        // $data_layanan = explode(",", $layanan);
        
        
        $this->db->select("provinsi, sum(revenue) as total");

        // ============== initial code ===============
        // $this->db->like("provinsi", $provinsi);
        // if(sizeof($lini) != 0){
            // $this->db->group_start();
            // foreach ($data_lini as $lin) {
            //     $this->db->or_like('lini', $lin);
            // }
            // $this->db->group_end();
        // }

        // if(sizeof($layanan) != 0){
            // $this->db->group_start();
            // foreach ($data_layanan as $lay){
            //     $this->kf->or_like('layanan', $lay);
            // }
            // $this->db->group_end();
        // }
        // ===========================================
        
        $this->db->where("provinsi", $provinsi);

        $this->db->where("tanggal >=", $from);
        $this->db->where("tanggal <=", $to);
        
        if(count($data_lini) >= 0 && $data_lini[0] != ''){
            // $this->db->group_start();
            // foreach ($data_lini as $lin) {
            //     $this->db->or_like('lini', $lin);
            // }
            // $this->db->group_end();

            $this->db->group_start();
            $this->db->where_in('lini', $data_lini);
            $this->db->group_end();
        }
        
        if(count($data_layanan) >= 0 && $data_layanan[0] != ''){
            // $this->db->group_start();
            // foreach ($data_layanan as $lay){
            //     $this->kf->or_like('layanan', $lay);
            // }
            // $this->db->group_end();

            $this->db->group_start();
            $this->db->where_in('layanan', $data_layanan);
            $this->db->group_end();
        }

        $this->db->group_by("provinsi");
        $data = $this->db->get('usc_bst_sales_area_channel');
        
        if($data->num_rows() > 0 ){
            return $data->row()->total;
        }else{
            return 0;
        }    
    }

    function json_channel_area()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
       
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd();
        
        // die($this->db->last_query());
        // var_dump("data lini = ",$data_lini, "data layanan = ",$data_layanan);
        // die;

        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( nama_kftd = "' . $kf->nama_kftd . '", revenue, 0) ) AS "' . $kf->nama_kftd . '"');
        }
        // die($this->db->last_query());

        $this->kf->select('channel');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);

        // ====================== initial code ======================
        // $this->kf->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->kf->group_end();
        
        // $this->kf->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->kf->or_like('layanan', $lay);
        // }
        // $this->kf->group_end();

        // if (!empty($provinsi) || $provinsi != ""){
        //     $this->kf->group_start();
        //     $this->kf->like('provinsi', strtoupper($provinsi));
        //     $this->kf->group_end();
        // }
        // ==========================================================
        
        if ($lini != ''){
            $this->kf->group_start();
            $this->kf->where_in('lini', $data_lini);
            $this->kf->group_end();
        }
        
        if ($layanan != ''){
            $this->kf->group_start();
            $this->kf->where_in('layanan', $data_layanan);
            $this->kf->group_end();
        }

        if (!empty($provinsi) || $provinsi != ""){
            $this->kf->group_start();
            $this->kf->where('provinsi', strtoupper($provinsi));
            $this->kf->group_end();
        }

        $this->kf->group_by('channel');
        $temp =$this->kf->generate();
        // die($this->db->last_query());

        $temp2 =json_decode($temp); 
        // ?var_dump($temp2->data);
        $data =$temp2->data; 
        // ?exit();
        
        $this->db->distinct();
        $this->db->select('SUM(`revenue`) AS `revenue`,`channel`');
        $this->db->where("tanggal >=", $from)->where("tanggal <=", $to);
        if ($lini != ''){
            $this->kf->group_start();
            $this->kf->where_in('lini', $data_lini);
            $this->kf->group_end();
        }
        
        if ($layanan != ''){
            $this->kf->group_start();
            $this->kf->where_in('layanan', $data_layanan);
            $this->kf->group_end();
        }

        if (!empty($provinsi) || $provinsi != ""){
            $this->kf->group_start();
            $this->kf->where('provinsi', strtoupper($provinsi));
            $this->kf->group_end();
        }
        $this->db->group_by('channel');

        // ============== initial code ===================
        // $this->db->like('provinsi', strtoupper($provinsi));

        // $this->db->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->db->or_like('layanan', $lay);
        // }
        // $this->db->group_end();
        // $this->db->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->db->group_end();
        // $this->db->group_by('channel');
        // =============================================
         
        $arr_total_per_channel = $this->db->get('usc_bst_sales_area_channel')->result();
        // die($this->db->last_query());

        // var_dump($arr_total_per_channel);
        $arr_channel= array();
        foreach($arr_total_per_channel as $total_per_channel){
            $arr_channel[$total_per_channel->channel]=$total_per_channel->revenue;
        }
        
        // ========== initial code ============
        // $arr_hasil= array();
        // $arr_kftd=$this->area_channel_get_kftd();
        // die($this->db->last_query());
        // ====================================
        
        $arr_hasil_akhir= array();
        foreach($data as $key => $temp ){
            // ========= initial code ===========
            // foreach($arr_kftd as $kftd_temp)
            // ==================================
            foreach($kftd as $kftd_temp){
                $pembagi=$arr_channel[$temp->channel];
            
                $yg_dibagi=$data[$key]->{$kftd_temp->nama_kftd};
                if($pembagi==0){
                    $temp5="0%";
                }else{
                    $temp5 =  round(((int)$yg_dibagi/(int)$pembagi)*100,2)." %";
                }
                $arr_hasil_akhir[$key][$kftd_temp->nama_kftd]=str_replace(' ','',$temp5);
            }
            $arr_hasil_akhir[$key]["channel"]=$temp->channel;               
        }
        $std_obj = new stdClass();
        $std_obj->data=  $arr_hasil_akhir;
        
        return  json_encode($std_obj);
    }

    function json_area_channel()
    {
        $range = $this->input->post("range", true);
        if($range == ""){
            $range = "2018-01-01 / 2019-12-30";
        }
        $provinsi = $this->input->post("provinsi", true);
        $lini = $this->input->post("lini", true);
        $tanggal = str_replace(" ","",$range);
		$tanggal = str_replace("/",",",$tanggal);
		$tgl =  explode(",", $tanggal);
        $from = $tgl[0];
        $to = $tgl[1];
        $layanan = $this->input->post("layanan", true);
        $data_lini = explode(",", $lini);
        $data_layanan = explode(",", $layanan);
        $kftd = $this->area_channel_get_kftd_channel();
        // die($this->db->last_query());

        foreach ($kftd as $kf) {
            $this->kf->select('SUM( IF( channel = "' . $kf->channel . '", revenue, 0) ) AS "' . $kf->channel . '"');
        }
        $this->kf->select('nama_kftd as area');
        $this->kf->from('usc_bst_sales_area_channel');
        $this->kf->where("tanggal >=", $from);
        $this->kf->where("tanggal <=", $to);

        if ($lini != ''){
            $this->kf->group_start();
            $this->kf->where_in('lini', $data_lini);
            $this->kf->group_end();
        }
        
        if ($layanan != ''){
            $this->kf->group_start();
            $this->kf->where_in('layanan', $data_layanan);
            $this->kf->group_end();
        }

        if (!empty($provinsi) || $provinsi != ""){
            $this->kf->group_start();
            $this->kf->where('provinsi', strtoupper($provinsi));
            $this->kf->group_end();
        }
        // ==================== initial code ==========================
        // $this->kf->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->kf->group_end();
        
        // $this->kf->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->kf->or_like('layanan', $lay);
        // }
        // $this->kf->group_end();

        // $this->kf->group_start();
        //     $this->kf->like('provinsi',strtoupper($provinsi));
        // $this->kf->group_end();
        // =========================================================

        $this->kf->group_by('nama_kftd');

        $temp =$this->kf->generate();
        // die($this->db->last_query());

        // ========= initial code ===========
        // $temp2 =json_decode($temp); 
        // ?var_dump($temp2->data);
        // $data =$temp2->data; 
        // ==================================

        $data =json_decode($temp)->data; 
         
        $this->db->distinct();
        $this->db->select('SUM(`revenue`) AS `revenue`,`nama_kftd`');
        $this->db->where("tanggal >=", $from)->where("tanggal <=", $to);

        // ======================= initial code ========================
        // $this->db->like('provinsi', strtoupper($provinsi));
        // $this->db->group_start();
        // foreach ($data_layanan as $lay){
        //     $this->db->or_like('layanan', $lay);
        // }
        // $this->db->group_end();
        // $this->db->group_start();
        // foreach ($data_lini as $lin) {
        //     $this->kf->or_like('lini', $lin);
        // }
        // $this->db->group_end();
        // =============================================================

        if ($lini != ''){
            $this->kf->group_start();
            $this->kf->where_in('lini', $data_lini);
            $this->kf->group_end();
        }
        
        if ($layanan != ''){
            $this->kf->group_start();
            $this->kf->where_in('layanan', $data_layanan);
            $this->kf->group_end();
        }

        if (!empty($provinsi) || $provinsi != ""){
            $this->kf->group_start();
            $this->kf->where('provinsi', strtoupper($provinsi));
            $this->kf->group_end();
        }

        $arr_total_per_kftd = $this->db->group_by('nama_kftd')->get('usc_bst_sales_area_channel')->result();
        // die($this->db->last_query());

        $arr_kftd= array();
        foreach($arr_total_per_kftd as $total_per_kftd){
            $arr_kftd[$total_per_kftd->nama_kftd]=$total_per_kftd->revenue;
        }

        // $arr_hasil= array();
        // $arr_channel=$this->area_channel_get_kftd_channel();
        // die($this->db->last_query());

        $arr_hasil_akhir= array();
        foreach($data as $key => $temp ){
            // ============ initial code =============
            // foreach($arr_channel as $channel_temp)
            // =======================================
            foreach($kftd as $channel_temp){
                
                $pembagi=$arr_kftd[$temp->area];
                // $pembagi=$arr_kftd[$temp->nama_kftd];
                $yg_dibagi=$data[$key]->{$channel_temp->channel};
                if($pembagi==0){
                    $temp5="0%";
                }else{
                    $temp5 = round(((int)$yg_dibagi/(int)$pembagi)*100,2)."%";
                }
                $arr_hasil_akhir[$key][$channel_temp->channel]=str_replace(' ','',$temp5);
            }
            $arr_hasil_akhir[$key]["area"]=$temp->area;       
        }
        $std_obj = new stdClass();
        $std_obj->data=  $arr_hasil_akhir;
        
        return  json_encode($std_obj);

    }
}
