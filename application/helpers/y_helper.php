<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function y_level_color()
{
    $return['gold'] = '#f9b94d';
    $return['silver'] = '#959595';
    $return['bronze'] = '#cd8032';
    $return['regular'] = '#dddddd';
    $return['kosmetik'] = '#dddddd';

    return $return;
}

function y_cl($id)
{
    $array = y_level_color();
    $id = strtolower($id);

    if(!empty($array[$id]))
        return $array[$id];
    else
        return '#fefefe';
}

function y_level($rev, $freq)
{
    if($rev > 4000000 and $freq >= 5)
        return 'Gold';
    else if($rev > 4000000 and $freq < 5)
        return 'Silver';
	else if($rev <= 4000000 and $freq >= 5)
	    return 'Bronze';
	else
	    return 'Regular';
}

function y_level_rule($rev, $freq, $rev_rule, $freq_rule)
{
    if($rev > $rev_rule and $freq >= $freq_rule)
        return 'Gold';
    else if($rev > $rev_rule and $freq < $freq_rule)
        return 'Silver';
    else if($rev <= $rev_rule and $freq >= $freq_rule)
        return 'Bronze';
    else
        return 'Regular';
}

function y_level_rule_range($rev, $freq, $ra)
{
    if( ($rev > $ra['gold']['rev_min'] and $rev <= $ra['gold']['rev_max']) and ($freq > $ra['gold']['freq_min'] and $freq <= $ra['gold']['freq_max']) ) {
        return 'gold';
    } else if( ($rev > $ra['silver']['rev_min'] and $rev <= $ra['silver']['rev_max']) and ($freq > $ra['silver']['freq_min'] and $freq <= $ra['silver']['freq_max']) ) {
        return 'silver';
    } else if( ($rev > $ra['bronze']['rev_min'] and $rev <= $ra['bronze']['rev_max']) and ($freq > $ra['bronze']['freq_min'] and $freq <= $ra['bronze']['freq_max']) ) {
        return 'bronze';
    } else {
        return 'regular';
    }
}

function y_pad($int, $pad)
{
    return str_pad($int, $pad, '0', STR_PAD_LEFT);
}

function y_num_pad($number, $symbol=',')
{
    return number_format(round($number, 0), 0, '.', $symbol);
}

function y_num_idr($number)
{
    return 'Rp. '.y_num_pad($number, '.');
}