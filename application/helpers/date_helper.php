<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('date_to_day'))
{
    function date_to_day($date)
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            $dayOfWeek = date("l", strtotime($date));
        } else {
            $newDate = date("Y-m-d", strtotime($date));
            $dayOfWeek = date("l", strtotime($newDate));
        }
        return $dayOfWeek;
    }
}