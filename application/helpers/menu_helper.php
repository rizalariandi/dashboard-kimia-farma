<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('active_link_controller'))
{
    function active_link_controller($controller)
    {

        $CI    =& get_instance();
        $class = $CI->router->fetch_class();
        $value = "";
        for($i = 0 ; $i < count($controller) ; $i++){
            $value [] = $controller[$i] == $class ? 1 : 0; 
        }
        //print_r($value);die();
        return in_array(1,$value) ? 'nav-item active submenu' : 'nav-item';
    }
}
if ( ! function_exists('active_link_sub'))
{
    function active_link_sub($controller)
    {
         $CI    =& get_instance();
         $class = $CI->router->fetch_class();
         return ($class == $controller) ? 'active' : '';
    }
}