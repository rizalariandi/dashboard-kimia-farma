<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('random_color_part'))
{
    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
}

if ( ! function_exists('random_color'))
{
    function random_color() {
        return random_color_part() . random_color_part() . random_color_part();
    }
}

if ( ! function_exists('color_channel'))
{
    function color_channel($channel) {
        switch($channel) {
            case "Apotek":
                $color = "#56A8CBFF";
            break;
            case "Apotek KF":
                $color = "#FFA351FF";
            break;
            case "Grosir":
                $color = "#D7A9E3FF";
            break;
            case "HOREKA":
                $color = "#8BBEE8FF";
            break;
            case "Instansi":
                $color = "#A8D5BAFF";
            break;
            case "Klinik":
                $color = "#7DB46CFF";
            break;
            case "Koperasi":
                $color = "#53A567FF";
            break;
            case "Modern Market":
                $color = "#ABD6DFFF";
            break;
            case "PBF":
                $color = "#AE0E36FF";
            break;
            case "Rumah sakit":
                $color = "#F1AC88FF";
            break;
            case "Specialities":
                $color = "#FFBA52FF";
            break;
            case "Tenaga Kesehatan":
                $color = "#28334AFF";
            break;
            case "Toko Obat":
                $color = "#078282FF";
            break;
            case "Traditional Market":
                $color = "#2A2B2DFF";
            break;
            default:
                $color = "#339E66FF";
        } 

        return $color;
    }
}
/**
 * linear regression function
 * @param $x array x-coords
 * @param $y array y-coords
 * @returns array() m=>slope, b=>intercept
 */

if ( ! function_exists('linear_regression'))
{
    function linear_regression($x, $y) {
        // calculate number points
        $n = count($x);
        
        // ensure both arrays of points are the same size
        if ($n != count($y)) {
            trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
        }

        // calculate sums
        $x_sum = array_sum($x);
        $y_sum = array_sum($y);

        $xx_sum = 0;
        $xy_sum = 0;
        
        for($i = 0; $i < $n; $i++) {
        
            $xy_sum+=($x[$i]*$y[$i]);
            $xx_sum+=($x[$i]*$x[$i]);
            
        }

        // calculate slope
        if(($n * $xx_sum) - ($x_sum * $x_sum) == 0) {
            $m = 0;
        } else {
            $m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
        }
        
        
        // calculate intercept
        if ($n == 0) {
            $b = 0;
        } else {
            $b = ($y_sum - ($m * $x_sum)) / $n;
        }
        
        // return result
        return array("m"=>$m, "b"=>$b);
    }
  
    
}
