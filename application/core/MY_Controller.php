<?php
defined("BASEPATH") or exit("Not direct access allowed");

class MY_Controller extends CI_Controller{
  //PROPERTY
  public $data = array();
  //FUNCTION
  public function __construct(){
    parent::__construct();
    $this->load->helper(array('url','cookie','menu_helper','cookie'));
    $this->load->library(array('session','form_validation','template','support',"excel"));
    $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
    date_default_timezone_set("Asia/Jakarta");
  }

}

 ?>
