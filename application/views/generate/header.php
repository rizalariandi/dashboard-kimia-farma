<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!--<title>kimia farma</title>-->
	<title>D.0.2</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?= base_url() ?>assets/img/logo2.png" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?= base_url() ?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<!--<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">-->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/cdn/bootstrap.min.css">

	<link rel="stylesheet" href="<?= base_url() ?>assets/css/atlantis.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/js/plugin/jquery-ui-1.12.1/jquery-ui.css">
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/cdn/buttons.dataTables.min.css">

	<!-- Select2 -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/js/plugin/select2/css/select2.min.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/demo.css">
	<link href="<?= base_url()?>/assets/css/cdn/material-components-web.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/cdn/Material_Icons.css">

	<style>
		@media only screen and (max-width: 20%) {
			#card-filter {
				display: none;
			}
		}
		
	</style>
</head>
<body>
	<div class="wrapper sidebar_minimize" id="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="index.html" class="logo">
					<img src="<?= base_url() ?>assets/img/logo2.png" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar" onClick="hideSidebar();" id="sidebar-open">
						<i class="icon-menu"></i>
					</button>
					
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
					<button class="btn btn-toggle" onclick="filteringform();" id="filteringform" >
						<input type="hidden" id="valFilter" value	= 1>
						<input type="hidden" id="valSidebar" value	= 1>
						<i class="far fa-arrow-alt-circle-left"></i>
					</button>
				<div class="container-fluid">
					
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="<?= base_url() ?>assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="<?= base_url() ?>assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4>Hizrian</h4>
												<p class="text-muted">hello@example.com</p><a href="profile.html" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">My Profile</a>
										<a class="dropdown-item" href="#">My Balance</a>
										<a class="dropdown-item" href="#">Inbox</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Account Setting</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
		<!-- Custom template | don't include it in your project! -->
		<!-- End Custom template -->
		<div class="main-panel">
		