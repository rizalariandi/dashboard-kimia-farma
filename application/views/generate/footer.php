<button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
<footer class="footer">
				<div class="container-fluid">
					<div class="copyright ml-auto">
						2018, made with by <a href="#">Admedika</a>
					</div>				
				</div>
			</footer>
		</div>
</div>
<input type="hidden" value="<?= $url ?>" id="pathData">
<input type="hidden" value="<?= base_url() ?>" id="basedurl">

	<!--   Core JS Files   -->
	<script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!--<script src="http://themekita.com/demo-atlantis-bootstrap/livepreview/assets/js/core/jquery.3.2.1.min.js"></script>-->

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	
	<script src="<?= base_url() ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="<?= base_url() ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


	<!-- Chart JS -->
	<script src="<?= base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="<?= base_url() ?>assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="<?= base_url() ?>assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="<?= base_url() ?>assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="<?= base_url() ?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- Select2 -->
	<script src="<?= base_url() ?>assets/js/plugin/select2/js/select2.full.js"></script>

	<!-- jQuery Vector Maps -->
	<!--<script src="<?= base_url() ?>assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?= base_url() ?>assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>-->

	<!-- Sweet Alert -->
	<script src="<?= base_url() ?>assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Atlantis JS -->
	<script src="<?= base_url() ?>assets/js/atlantis.min.js"></script>

	<script src="<?= base_url() ?>assets/js/plugin/jquery-loading-master/src/loading.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="<?= base_url() ?>assets/js/setting-demo.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>/assets/js/custom/<?= isset($mycontroller) ? $mycontroller.".js" : "" ?>" ></script>
	<!--<script src="<?= base_url() ?>assets/js/demo.js"></script>-->
	<script scr="https://cdnjs.com/libraries/Chart.js"></script>
	<script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
	<script>
		$('body').tooltip({selector: '[data-toggle="tooltip"]'});
jQuery("#datatablesN").DataTable({
    "scrollY": true,
    "scrollX": true
	});
	window.onscroll = function() {scrollFunction()};

function scrollFunction() {
if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
	document.getElementById("myBtn").style.display = "block";
} else {
	document.getElementById("myBtn").style.display = "none";
}
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
$('html, body').animate({ scrollTop: $('body').offset().top }, 'slow');
//document.body.scrollTop = 0;
//document.documentElement.scrollTop = 0;
}	
	</script>
	
</body>
</html>