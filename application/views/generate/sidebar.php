<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="
							<?=
								active_link_controller(
									array(
										'sales_growth_information',
										'sales_detail_information'
									)
								)
							?>
							">
							<a  data-toggle="collapse" href="#sales_growth_information">
								<i class="fas fa-play"></i>
								<p>Marketing Operasional</p>
								<span class="caret"></span>
							</a>
								<div class="collapse show" id="sales_growth_information">
								<ul class="nav nav-collapse">
									<li class="<?= active_link_sub("sales_growth_information") ?>">
										<a href="<?= base_url('sales_growth_information') ?>">
											<span class="sub-item">Sales Information</span>
										</a>
									</li>
									<li class="<?= active_link_sub("marketing_expenses_information") ?>">
										<a href="<?= base_url('marketing_expences_information') ?>">
											<span class="sub-item">Marketing Expenses Information</span>
										</a>
									</li>
									<li class="<?= active_link_sub("report_sales_summary") ?>">
										<a href="<?= base_url('report_sales_summary') ?>">
											<span class="sub-item">Report Sales Summary</span>
										</a>
									</li>
									<li class="<?= active_link_sub("report_sales_margin") ?>">
										<a href="<?= base_url('report_sales_margin') ?>">
											<span class="sub-item">Report Sales Margin Summary</span>
										</a>
									</li>
									
									<li class="<?= active_link_sub("report_sales_margin") ?>">
										<a href="http://kimia-farma-dsc2.vsan-apps.playcourt.id/index.php/page/view">
											<span class="sub-item">Back</span>
										</a>
									</li>
									
								</ul>
							</div>
						</li>
						
						
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->