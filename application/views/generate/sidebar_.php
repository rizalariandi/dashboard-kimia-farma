<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<ul class="nav nav-primary">
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu</h4>
						</li>
						<li class="<?= active_link_controller('executive_dashboard')?>">
							<a  href="<?= base_url('executive_dashboard')?>">
								<i class="fas fa-play"></i>
								<p>Executive Dashboard</p>
								<span class=""></span> 
							</a>
						</li>
						<li class="<?= active_link_controller('oprational_dashboard')?>">
							<a  href="<?= base_url('oprational_dashboard')?>">
								<i class="fas fa-play"></i>
								<p>Oprational Dashboard</p>
								<span class=""></span>
							</a>
						</li>
						<li class="
							<?=
								active_link_controller(
									array(
										'sales_growth_information',
										'sales_detail_information'
									)
								)
							?>
							">
							<a  data-toggle="collapse" href="#sales_growth_information">
								<i class="fas fa-play"></i>
								<p>Sales Growth Information</p>
								<span class="caret"></span>
							</a>
								<div class="collapse show" id="sales_growth_information">
								<ul class="nav nav-collapse">
									<li class="<?= active_link_sub("sales_growth_information") ?>">
										<a href="<?= base_url('sales_growth_information') ?>">
											<span class="sub-item">Sales Information</span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="sub-item">Sales Detail Information</span>
										</a>
									</li>
									<li <?= active_link_sub("product_detail_claim_discount") ?>>
										<a href="<?= base_url('product_detail_claim_discount') ?>">
											<span class="sub-item">Product Detail Claim Discount</span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="sub-item">Product Parretto</span>
										</a>
									</li>
									<li>
										<a href="#">
											<span class="sub-item">Marketing Expences</span>
										</a>
									</li>
									<li>
										<a data-toggle="collapse" href="#subnav1">
											<span class="sub-item">Download Data</span>
											<span class="caret"></span>
										</a>
																				
										<div class="collapse" id="subnav1">
											<ul class="nav nav-collapse subnav">
												<li>
													<a href="#">
														<span class="sub-item">Summary By Distibutor Branch</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Summary by Lini</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Summary by Lini,Layanan & Item Group </span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Summary by Lini & Item Group</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Summary by Group</span>
													</a>
												</li>
												<li>
													<a href="#">
														<span class="sub-item">Margin Summary</span>
													</a>
												</li>
											</ul>
										</div>
									
									</li>
								</ul>
							</div>
						</li>
						
						<li class="<?= active_link_controller('insight_dashboard')?>">
							<a  href="<?= base_url('insight_dashboard')?>">
								<i class="fas fa-play"></i>
								<p>Insight Dashboard</p>
								<span class=""></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->