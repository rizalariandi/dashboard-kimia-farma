 <?php error_reporting(0); ?>
<!DOCTYPE html>
<!-- saved from url=(0073)https://wrappixel.com/demos/admin-templates/pixeladmin/inverse/blank.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>asset/logo.png">
    <title>Kimia Farma | Performance Dashbaord</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Bootstrap Core CSS -->
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>

    <link href="<?=base_url()?>asset/css/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?=base_url()?>asset/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>asset/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?=base_url()?>asset/css/default.css" id="theme" rel="stylesheet">

    <!-- color CSS -->
    <link rel="stylesheet" href="<?=base_url()?>asset/plugins/bootstrap/css/bootstrap-multiselect.css" type="text/css"/>
    <link href="<?=base_url()?>asset/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.css">
   
    <script src="<?=base_url()?>asset/js/download/jquery.3.3.1.js"></script>
    <script src="<?= base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/bootstrap.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/jszip.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/pdfmake.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/vfs_fonts.js"></script>
    <script src="<?=base_url()?>asset/js/download/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>asset/js/download/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>asset/plugins/bootstrap/js/bootstrap-multiselect.js"></script>


    <script src="<?=base_url()?>asset/js/highstock.js"></script>
    <script src="<?=base_url()?>asset/js/exporting.js"></script>

    <script src="<?=base_url()?>asset/js/jquery.easypiechart.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/daterangepicker/moment.min.js"></script>
    <script src="<?=base_url()?>asset/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?= base_url() ?>assets/js/plugin/select2/js/select2.full.js"></script>
    <!-- Sweet Alert -->
    <script src="<?= base_url() ?>assets/js/plugin/sweetalert/sweetalert.min.js"></script>

<script src="<?=base_url()?>asset/js/viz_v1.js"></script>
    <script src="<?=base_url()?>asset/js/tableau_v8.js"></script>
    <script src="<?=base_url()?>asset/js/tableau-2.min.js"></script>
  <script src="<?= base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>

    <!-- Menu Plugin JavaScript -->

    <!--slimscroll JavaScript -->
    <script src="<?=base_url()?>asset/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>asset/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url()?>asset/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="<?=base_url()?>asset/js/jQuery.style.switcher.js"></script>
    <script src="<?= base_url() ?>assets/js/plugin/xlsx/xlsx.full.min.js"></script>
    <script src="<?= base_url() ?>assets/js/plugin/accounting/accounting.min.js"></script>
    <script src="<?= base_url() ?>assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
</head>
<style>
.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.42;
  border-radius: 15px;
}
.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;
}

/* Do an horizontal flip when you move the mouse over the flip box container */
.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}

/* Position the front and back side */
.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}

/* Style the front side (fallback if image is missing) */
.flip-card-front {
  background-color: #bbb;
  color: black;
}

/* Style the back side */
.flip-card-back {
  background-color: dodgerblue;
  color: white;
  transform: rotateY(180deg);
}

		@media only screen and (max-width: 20%) {
			#card-filter {
				display: none;
			}
		}
		
        body button {
 
  cursor:progress;
}
</style>
<body class="fix-sidebar" style="">
    <!-- Preloader -->
    <div class="preloader" style="display: none;">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="<?=base_url()?>">
                    <b class="light-logo"><i class='fas fa-chart-line' style='color: transparent;;font-size: 30px;vertical-align: middle;'></i>
                    </b>
                    <span class="hidden-xs"><!--This is dark logo text-->
                        <img src="<?=base_url()?>asset/logo_putih.png" alt="home" class="dark-logo">
                        <!--This is light logo text-->
                        <img style="width: 100px;" src="<?=base_url()?>asset/logo_putih.png" alt="home" class="light-logo">
                    </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs active">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li><a style='font-weight: 500;'>Hi, <?=$_SESSION['nama']?>!</a></li>
                    <li><div class="user-profile">
                        <div class="dropdown user-pro-body">
                            <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?=base_url()?>asset/images/varun.jpg" alt="user-img" class="img-circle"></a>
                            <ul class="dropdown-menu animated flipInY">
                                <li><a href="#"><i class="far fa-user"></i> <?=$_SESSION['nama']?></a></li>
                                <li><a href="#"><i class="far fa-envelope"></i> <?=$_SESSION['email']?></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?=base_url()?>index.php/page/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div></li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar menu-navigation" role="navigation">
            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar" style="overflow: visible hidden; width: auto; height: 100%;" >
            <div id="menu-navigation"></div>
            <?php
            if($opration == true){
                $this->load->view($filter['page'],$filter['data']);
            }
            ?> 
                <ul class="nav" id="side-menu">
                
                <?php

//$mnu_level = array_unique($menu['level']);
$n = sizeof($menu);
$n--;
for($i=0;$i<=$n;$i++){
    $lvl = strlen($menu[$i]['level'])-strlen(str_replace('.','',$menu[$i]['level']));
    $next = $i;
    if($n!=$i){
        $next++;
        $lvl2 = strlen($menu[$next]['level'])-strlen(str_replace('.','',$menu[$next]['level']));
    }
    $str="";
    $class='';
   
   
    $link = ($menu[$i]['link']=='#')?'#':(strpos($menu[$i]['link'],"://") > 0 ? $menu[$i]['link'] :base_url()."index.php/page/view/".$menu[$i]['link']);
    switch($lvl){
        case 0 : $str.="<li><a href='$link' class='waves-effect'><i class='".$menu[$i]['icon']."'></i> <span class='hide-menu'>".$menu[$i]['name']."</span></a>";
            $class='nav nav-second-level collapse';
            break;
        case 1 : $str.="<li><a  href='$link' class='waves-effect'><span class='hide-menu'>".$menu[$i]['name']."</span></a>";
            $class='nav nav-third-level collapse';
            break;
        case 2 : $str.="<li><a  href='$link' class='waves-effect'><span class='hide-menu'>".$menu[$i]['name']."</span></a>";
            $class='nav nav-empat-level collapse';
            break;
        case 3 : $str.="<li><a  href='$link' class='waves-effect'><span class='hide-menu'>".$menu[$i]['name']."</span></a>";
            break;
    } 

    if($lvl==$lvl2){
        $str.="</li>";
    }elseif($lvl<$lvl2){
        $str.="<span class='hide-menu'><span class='fa arrow'></span></span><ul class='$class'>";
    }elseif($lvl>$lvl2){
        if($lvl>$lvl2)
            $str.=str_repeat("</ul>",$lvl-$lvl2);
        else 
            $str.="</ul>";
    }
        
    echo $str;
    //echo "</ul>";
    
}



?>
                </ul>
            </div><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 0px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 59.3413px;"></div><div class="slimScrollRail" style="width: 0px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper" style="min-height: 557px;">
            <div id="container-dom" class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"></h4>
                    </div>
                  
                    <!-- /.col-lg-12 -->
               