<div class="col-md-4 col-xs-4 col-lg-4" id="filter-side" style="display:non">
	<div class="card full-height card-filter mt-2 ml-2" >
        <div class="card-header">
			<div class="card-title" style="font-size:15px;">Please Choose :
            </div>
		</div>
		<div class="card-body" >
        <label for="" id="preparing_desktop"></label>
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
        </div>
            <form action="#" id="form-filter">
            <label for="">Periode</label>   
                    <div class='form-inline'>
                        <label for="email"></label>
                        <input type="text" class="form-control col-md-5 datepicker"  name="tanggal_faktur_start"  id="tanggal_faktur_start">
                        <label for="pwd"><b>&nbsp;to&nbsp;</b></label>
                        <input type="text" class="form-control col-md-5 datepicker" id="tanggal_faktur_end" name="tanggal_faktur_end" >
                    </div>
            <table class="table">
                <tr>
                    <td>Value</td>
                    <td>
                        <select name="value" id="value" class="form-control filter-select2">
                            <option value="">(All)</option>
                            <option value="HPP_TY">HPP</option>
                            <option value="HJP_TY">HJP</option>
                            <option value="HJP_PTD_TY">HJP-PTD</option>
                            <option value="HNA_TY">HNA</option>
                            <option value="HJD_TY">HJD</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Distributor</td>
                    <td>
                        <select name="distributor_code" id="distributor_code" class="form-control filter-select2">
                            <option value="">(All)</option>
                           
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Branch</td>
                    <td>
                        <select name="branch_code" id="branch_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                            
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>GM/PM</td>
                    <td>
                        <select name="gpm_pm_code" id="gpm_pm_code" class="form-control filter-select2">
                            <option value="">(All)</option>
                           
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>RSM</td>
                    <td>
                        <select name="rsm_code" id="rsm_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                          
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Shopper</td>
                    <td>
                        <select name="shopper_code" id="shopper_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                           
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>AM/APM/ASM</td>
                    <td>
                        <select name="am_apm_asm_code" id="am_apm_asm_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                           
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>MSR/MD/SE</td>
                    <td>
                        <select name="msr_md_se_code" id="msr_md_se_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                           
			    	    </select>
                    </td>
                </tr>
               
                <tr>
                    <td>Segment</td>
                    <td>
                        <select name="segment" id="segment" class="form-control filter-select2">
                        <option value="">(All)</option>
                         
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>
                        <select name="customer_code" id="customer_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                       
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Layanan</td>
                    <td>
                        <select name="layanan_code" id="layanan_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                        
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Lini</td>
                    <td>
                        <select name="lini_code" id="lini_code" class="form-control filter-select2">
                        <option value="">(All)</option>
                        
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Group 1</td>
                    <td>
                        <select name="material_group1_code" id="material_group1_code" class="form-control filter-select2">
                        <option value="">(All)</option>
			    	    </select>
                    </td>
                </tr>
                <tr>
                    <td>Group 2</td>
                    <td>
                        <select name="material_group2_code" id="material_group2_code" class="form-control filter-select2">
                            <option value="">(All)</option>
                           
                            </select>
                    </td>
                </tr>
                <tr>
                    <td>Group 3</td>
                    <td>
                         <select name="material_group3_code" id="material_group3_code" class="form-control filter-select2">
                            <option value="">(All)</option>
                           
                            </select>
                    </td>
                </tr>
                <tr>
                    <td>Brands</td>
                    <td>
                        <select name="brand" id="brand" class="form-control filter-select2">
                            <option value="">(All)</option>
                           
                            </select>
                    </td>
                </tr>
                <tr>
                    <td>Produk</td>
                    <td>
                        <select name="material_code" id="material_code" class="form-control filter-select2">
                            <option value="">(All)</option>`
                           
                            </select>
                    </td>
                </tr>
            </table>
            </form>
            <div class="form-group">
                   <!--<button id="btn-filter" class="btn btn-primary btn-block rounded" onClick="jQuery(this).text('Loading');tableses.ajax.reload(null,true);getcardsum();jQuery(this).text('Apply');drawmycanvasbar();return false;">Apply</button>-->
                   <button id="btn-filter" class="btn btn-primary btn-block rounded" onClick="choosetable('m')">Apply</button>
            </div>
       
        </div>
	</div>
</div>