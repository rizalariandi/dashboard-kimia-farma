<input type="hidden" id="pathData" value="<?= base_url('index.php/'.$this->uri->segment('3')) ?>">
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" type="text/css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js "></script> 
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script> 

<?php
$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$config['base_url'] .= "://".$_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$config['base_url'] .= "";

?>
<style>
.dataTables_processing {
    z-index: 3000;
}
.btn-generate{
	background-color: #08388F;
	color : #fff;
}
table thead{
    background: #F79966;
  text-align:center;
  color : white;
}
.table{
	font-size:1
}
.card-category{
	font-size: 10px;
}
.value-card{
	font-size:12px;
	
}
.my-canvas {
overflow-x: auto;
overflow-y: hidden;
white-space: nowrap;
}
.my-canvas .canvas-body{
display:block;
}
.my-canvas .canvas-body .canvas-dom {
display: inline-block;
float: none;
padding: 15px;
border: 1px solid indigo;
}
th, td { white-space: nowrap; font-size:10px; }
div.dataTables_wrapper {
	margin: 0 auto;
}
	#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #1565c0;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
    div.container {
        margin: auto;
		padding: 10px;
    }
    /* .stage {
      
    } */
    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    .bounce-6 {
        animation-name: bounce-6;
        animation-timing-function: ease;
    }
	
    @keyframes bounce-6 {
        0%   { transform: scale(1,1)      translateY(0); }
        10%  { transform: scale(1.1,.9)   translateY(0); }
        30%  { transform: scale(.9,1.1)   translateY(-100px); }
        50%  { transform: scale(1.05,.95) translateY(0); }
        57%  { transform: scale(1,1)      translateY(-7px); }
        64%  { transform: scale(1,1)      translateY(0); }
        100% { transform: scale(1,1)      translateY(0); }
	}
	
</style>
<div class="back-to-top">
	<div class="row">
	    <div class="col-md-12">
	        <div class="white-box">
				<center><h3 class="box-title">Report Sales Margin Summary</h3> </center>
                <h4 id="ket" class="page-title last_update" style="color: #cb3935;"><sup>*</sup>Note:Mandatory field</h4>
				
			</div>
	    </div>
	</div>
	<div class="row" >
		<div class="col-md-12">
			<div class="white-box" style="padding-bottom:10%">
		
		<form id="form-header" class="col-md-8">
        	<div class="form-group">
				<div class="col-md-2">
					<!-- <label for="" style="margin-left:">Table Name*</label> -->
					<label >Table Name*</label>
				</div>	
	    	    <div class="col-md-2">
					<!-- <label for="" style="margin-left:">:</label> -->
					<label>:</label>
				</div>
				<div class="col-md-4">
					<!--<select onchange="choosetable(jQuery(this).val())" name="head_filter" id="head_filter" class="form-control select2">-->
					<select style="width:120%;" onchange="choosetable('m')" name="head_filter" id="head_filter" class="form-control select2">
						<option value="" selected></option>
						<!--<option value="Month" data-badge="">Month</option>
						<option value="distributor_code" >Distributor</option>
						<option value="branch_code" >Branch</option>
						<option value="gpm_pm_code" >PM</option>
						<option value="rsm_code" >RSM</option>
						<option value="shopper_code" >Shopper</option>
						<option value="am_apm_asm_code" >AM</option>
						<option value="msr_md_se_code" >SE</option>
						<option value="segment" >Segment</option>
						<option value="customer_code" >Customer</option>
						<option value="layanan_code" >Layanan</option>
						<option value="lini_code" >Lini</option>
						<option value="material_group1_code" >Group 1</option>
						<option value="material_group2_code" >Group 2</option>
						<option value="material_group3_code" >Group 3</option>
						<option value="brand" >Brands</option>
						<option value="material_code" >Produk</option>-->
						<option value="gpm_pm_code" >PM</option>
						<option value="rsm_code" >RSM</option>
						<option value="shopper_code" >Shopper</option>
						<option selected value="lini_code" >Lini</option>
						<option value="brand" >Brands</option>
					</select>
				</div>
        	</div>
			<div class="col-md-2"></div>
		</form>
		
			</div>
			</div>
	</div>
	<!--div class="stage" >
    					<div class="box bounce-6"> <h1>Loading...</h1></div>
					</div>	-->
	<div class="row" >
	    <div class="col-md-12" >
	        <div class="white-box" >
						<div class="my-table">
							<table id="datatablesE" class="table table-responsive" width="100%">
							<!--<table id="" class="table table-responsive" width="100%">-->
							<thead>
									<tr>
										<th id="th-grand"><b>Monthly</b></th>
										<th id="th-qtytarget">Description <b></b></th>
										<th id="th-qtytarget">Target <b></b></th>
										<th id="th-target">Realisasi This Year<b></b></th>
										<th id="th-qtyterjual">Realisasi Last Year <b></b></th>
										<th id="th-realisasi">Achievement %<b></b></th>
										<th id="th-disc">Growth %</th>
										<!--<th id="th-grand"><b>SALES - PTD</b></th>
										<th id="th-qtytarget">PTD <b></b></th>
										<th id="th-qtytarget">SALES <b></b></th>
										<th id="th-target">COGS<b></b></th>
										<th id="th-qtyterjual">COGS <b></b></th>
										<th id="th-realisasi">MARKETING EXPENSE<b></b></th>
										<th id="th-disc">MARKETING EXPENSE %</th>
										<th id="th-disc">GROSS MARGIN</th>
										<th id="th-disc">GROSS MARGIN %</th>-->
									</tr>
								</thead>
							</table>
							
						</div>
						
					</div>
					<div class="col-md-12" style="padding-bottom:100%;display:none;"> 
						<button style=" position: absolute;right: 0;"  style="background-color:#073990;color:white" class="btn btn-sm btn-generate" onclick=' var filename =&quot;<?= "report_sales_margin_summary" ?>&quot;; var url = &quot;<?= base_url("index.php/report_sales_margin_summary/downloadexcel");?>&quot;; generateexcel(url,filename);return false;'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>
						<button style=" position: absolute;right: 0;display:none;background-color:#073990;color:white" class="btn btn-sm btn-danger btn-download" onclick=' var filename =&quot;<?= "report_sales_margin_summary" ?>&quot;; var url = &quot;<?= base_url("index.php/report_sales_margin_summary/downloadexcel");?>&quot;; downloaddataexcel(url,filename);return false;' ><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>
					</div>	
				</div>
			</div>


<script>
function generateexcel(url,filename){
    if(jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#value").val() == "" || jQuery("#head_filter").val() == "" )  {
		swal("Pastikan tanggal faktur, Value terisi, Filter table name terisi");
		return false;
	}  else{
		
		jQuery(".btn-generate").hide();
		
		
		//console.log(filename);
			printdata(url,filename);
	}
    }
    function downloaddataexcel(url,filename){
		jQuery(".btn-generate").show();
		jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open("<?php echo $config['base_url'] ?>/file/"+filename+".xls","","_blank","height=1000,width=800");
    }
</script>

