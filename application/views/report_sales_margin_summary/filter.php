<style>
        @media only screen and (max-width: 1026px) {
                .sidebar-filtering {
                    display: none;
                }
        }
        .last_update {
            position: absolute;
            padding-left: 5px;

            font-weight: normal;
            font-size: 12px;
            line-height: 10px;
            /* identical to box height, or 167% */


            color: #cb3935;

            font-family: Roboto;

        }
        </style>
        <div id="filtering-side" style="display:none;">

        <label for="" id="preparing_desktop"></label>

        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="hight:5%">0%</div>
        </div>
            <form action="#" id="form-filter" style="margin-left:10%">
                <div class="form-group">
                <label for="">Periode*</label>   
                    <div class='form-inline'>
                        <label for="email"></label>
                        <input style="width:40%;font-size:9px" type="text" class="form-control dates" id="tanggal_faktur_start" name="tanggal_faktur_start">
                        <label for="pwd"><b>&nbsp;to&nbsp;</b></label>
                        <input style="width:40%;font-size:9px" type="text" class="form-control dates" id="tanggal_faktur_end" name="tanggal_faktur_end" >
                    </div>
                    <!--<div class="form-group">
                            <label for="branch_code">Value :</label>
                            <select name="value" id="value" class="form-control filter-select2" style="width: 90%" >
                            <option value=""></option>
                            <option value="HPP_TY">HPP</option>
                            <option value="HJP_TY">HJP</option>
                            <option value="HJP_PTD_TY">HJP-PTD</option>
                            <option value="HNA_TY">HNA</option>
                            <option value="HJD_TY">HJD</option>
                        </select>
                    </div>-->      
            </div>
            <!--<div class="form-group">
                    <div class="form-group">
                        <label for="branch_code">Distributor :</label>
                        <select  name="distributor_code[]" id="distributor_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>  
            <div class="form-group">
                    <div class="form-group">
                        <label for="branch_code">Branch :</label>
                        <select  name="branch_code[]" id="branch_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>-->
            <div class="form-group">
                    <div class="form-group">
                        
                        <label for="gpm_pm_code" style="cursor:pointer" onclick="selectedfilter('gpm_pm_code')"><i class="fa fa-refresh" aria-hidden="true"></i> GM/PM : </label>
                        <select  name="gpm_pm_code[]" id="gpm_pm_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>
            <div class="form-group">
                    <div class="form-group">
                        <label for="rsm_code" style="cursor:pointer" onclick="selectedfilter_rsm('rsm_code')"><i class="fa fa-refresh" aria-hidden="true"></i> RSM : </label>
                        <select  name="rsm_code[]" id="rsm_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>
            <div class="form-group">
                    <div class="form-group">
                        <label for="shopper_code" style="cursor:pointer" onclick="selectedfilter_shopper('shopper_code')"><i class="fa fa-refresh" aria-hidden="true"></i> Shopper : </label>
                        <select  name="shopper_code[]" id="shopper_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>       
            <!--<div class="form-group">
                    <div class="form-group">
                        <label for="am_apm_asm_code">AM/APM/ASM :</label>
                        <select  name="am_apm_asm_code[]" id="am_apm_asm_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>   
            <div class="form-group">
                    <div class="form-group">
                        <label for="msr_md_se_code">MSR/MD/SE :</label>
                        <select  name="msr_md_se_code[]" id="msr_md_se_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>   
            <div class="form-group">
                    <div class="form-group">
                        <label for="segment">Segment :</label>
                        <select  name="segment[]" id="segment" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>
            <div class="form-group">
                    <div class="form-group">
                        <label for="customer_code">Customer :</label>
                        <select  name="customer_code[]" id="customer_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>    
            <div class="form-group">
                    <div class="form-group">
                        <label for="layanan_code">Layanan :</label>
                        <select  name="layanan_code[]" id="layanan_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>-->
            <div class="form-group">
                    <div class="form-group">
                        <label for="lini_code" style="cursor:pointer" onclick="selectedfilter('lini_code')"><i class="fa fa-refresh" aria-hidden="true"></i> Lini : </label>
                        <select  name="lini_code[]" id="lini_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>
            <!--<div class="form-group">
                    <div class="form-group">
                        <label for="material_group1_code">Group 1 :</label>
                        <select  name="material_group1_code[]" id="material_group1_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>  
            <div class="form-group">
                    <div class="form-group">
                        <label for="material_group2_code">Group 2 :</label>
                        <select  name="material_group2_code[]" id="material_group2_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>  
            <div class="form-group">
                    <div class="form-group">
                        <label for="material_group3_code">Group 3 :</label>
                        <select  name="material_group3_code[]" id="material_group3_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div> -->
            <div class="form-group">
                    <div class="form-group">
                        <label for="brand" style="cursor:pointer" onclick="selectedfilter_brand('brand')"><i class="fa fa-refresh" aria-hidden="true"></i> Brands : </label>
                        <select  name="brand[]" id="brand" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div> 
            <!--<div class="form-group">
                    <div class="form-group">
                        <label for="material_code">Produk :</label>
                        <select  name="material_code[]" id="material_code" class="form-control filter-multiple-select2" style="width: 90%"  multiple="multiple">
                        
			    	    </select>
                    </div>
            </div>-->
            </form>
            <div class="form-group" align="center">
                   <!--<button id="btn-filter" class="btn btn-primary btn-block rounded" onClick="jQuery(this).text('Loading');tableses.ajax.reload(null,true);getcardsum();jQuery(this).text('Apply');drawmycanvasbar();return false;">Apply</button>-->
                   <button id="btn-filter" style="background-color:#073990;color:white" class="btn" onClick="$('html, body').animate({ scrollTop: $('body').offset().top }, 'slow');choosetable('m')"><i class="fa fa-search" aria-hidden="true"></i> Apply</button>
            </div>
        </div>
        
        
