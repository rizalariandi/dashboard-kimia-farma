<input type="hidden" id="pathData" value="<?= base_url('index.php/'.$this->uri->segment('3')) ?>">
<style>
    .table{
        font-size:1,
    }
    table thead{
        background:#093890;

        color : white;
    }
    #th-target{
        align:right;
    }

    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #1565c0;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    .bounce-6 {
        animation-name: bounce-6;
        animation-timing-function: ease;
    }
    @keyframes bounce-6 {
        0%   { transform: scale(1,1)      translateY(0); }
        10%  { transform: scale(1.1,.9)   translateY(0); }
        30%  { transform: scale(.9,1.1)   translateY(-100px); }
        50%  { transform: scale(1.05,.95) translateY(0); }
        57%  { transform: scale(1,1)      translateY(-7px); }
        64%  { transform: scale(1,1)      translateY(0); }
        100% { transform: scale(1,1)      translateY(0); }
    }
</style>
<?php
$bulan = array(
    0 => array('no' => 1 , 'nama' => 'Januari'),
    1 => array('no' => 2 , 'nama' => 'Februari'),
    2 => array('no' => 3 , 'nama' => 'Maret'),
    3 => array('no' => 4, 'nama' => 'April'),
    4 => array('no' => 5, 'nama' => 'Mei'),
    5 => array('no' => 6, 'nama' => 'Juni'),
    6 => array('no' => 7, 'nama' => 'Juli'),
    7 => array('no' => 8, 'nama' => 'Agustus'),
    8 => array('no' => 9, 'nama' => 'September'),
    9 => array('no' => 10, 'nama' => 'Oktober'),
    10 => array('no' => 11, 'nama' => 'November'),
    11 => array('no' => 12, 'nama' => 'Desember'),
);

//print_r($bulan);die();

?>
<div class="col-md-12">
    <div class="card">

        <div class="page-inner mt-10">
            <div class="row mt-20">
                <div class="card full-hight col-md-12 mt-100">
                    <div align="center">
                        <h1>Marketing Expenses Information</h1>

                    </div>
                </div>
                <div class="card full-hight col-md-12 mt-100" style="padding-bottom:5%">
                    <div class="form-inline">
                        <div class="col-md-8">
                            <form id="form-filter">
                                <div class="form-group">

                                    <div class='form-inline'>
                                        <label for="">Periode</label>
                                        <label for="email"></label>
                                        <select style="width:20%; font-size:10px;" name="bulan_awal" id="bulan" class="form-control ">
                                            <?php
                                            for($i = 0 ; $i <= count($bulan); $i++){
                                                echo '<option value="'.$bulan[$i]['no'].'">'.$bulan[$i]['nama'].'</option>';
                                            }
                                            ?>
                                        </select>
                                        <label for="pwd"><b>&nbsp;to&nbsp;</b></label>
                                        <select style="width:20%; font-size:10px;" name="bulan_akhir" id="bulan" class="form-control ">
                                            <?php
                                            for($i = 0 ; $i <= count($bulan); $i++){
                                                echo '<option value="'.$bulan[$i]['no'].'">'.$bulan[$i]['nama'].'</option>';
                                            }
                                            ?>
                                        </select>

                                        <select style="width:20%font-size:10px;" name="tahun" id="tahun" class="form-control ">
                                            <?php
                                            $year = date('Y');

                                            for($i = ($year -10) ; $i <= $year; $i++){
                                                if($i == $year){
                                                    echo '<option value="'.$i.'" selected>'.$i.'</option>';
                                                }else{
                                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                                }

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <form id="form-header">
                            <div class="col-md-4 float-right">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <label for="head-display" style="margin-left:">Filter by</label>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="" style="margin-left:">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <!--<select onchange="choosetable(jQuery(this).val())" name="head_filter" id="head_filter" class="form-control select2">-->
                                        <select name="head_filter" id="head_filter" class="form-control ">
                                            <option value=""></option>
                                            <option value="cost_center">LINI</option>
                                            <option value="biaya" >BIAYA</option>
                                            <option value="gl_account" >URAIAN</option>
                                            <option value="gpm_pm_code" >GPM/PM</option>
                                            <option value="rsm_shopper_code" >RSM/SHOPPER</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div align="center"  >

            </div>
        </div>
        <div align="center" style="margin-top:0%">
            <h3>Report Biaya SBU Marketing & Sales</h3>
        </div>
    </div>
</div>
<div id="myTable" class="col-md-12">
    <table id="datatablesE" class="table table-striped table-bordered" width="100%">
    </table>
</div>
<div align="center"  style="padding-bottom:100%">
    <button class="btn " style="background-color:#073990;color:white" onClick="choosetable(jQuery('#head_filter').val())">Apply Filter <i class="fa fa-search" aria-hidden="true"></i></button>
    <button class="btn btn-generate" style="background-color:#073990;color:white" onclick=' var filename =&quot;<?= "marketing_expences_information" ?>&quot;; var url = &quot;<?= base_url("marketing_expences_information/downloadexcel");?>&quot;; generateexcel(url,filename);'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>
    <button style="display:none;background-color:#073990;color:white" class="btn btn-download" onclick=' var filename =&quot;<?= "marketing_expences_information" ?>&quot;; var url = &quot;<?= base_url("marketing_expences_information/downloadexcel");?>&quot;; downloaddataexcel(url,filename);' ><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade bd-example-modal-lg" role="dialog">
    <div class="modal-dialog nodal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Marketing Expenses Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div>
                    <h3 id="totaltext" align="center"></h3>
                    <div id="myTableModal">

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php
$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$config['base_url'] .= "://".$_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$config['base_url'] .= "";

?>
<script>
    function generateexcel(url,filename){
        if(jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#head_filter").val() == "" )  {
            swal("Pastikan Periode, dan Filter terisi");
            return false;
        }  else{
            jQuery(".stage").show();
            jQuery(".box h1").text("Generate data excel....");
            jQuery(".btn-generate").hide();


            //console.log(filename);
            printdata(url,filename);
        }
    }
    function downloaddataexcel(url,filename){
        jQuery(".btn-generate").show();
        jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open("<?php echo $config['base_url'] ?>/file/"+filename+".xls","","_blank","height=1000,width=800");
    }
</script>

