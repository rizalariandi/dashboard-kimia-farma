        <style>
        @media only screen and (max-width: 1026px) {
                .sidebar-filtering {
                    display: none;
                }
        }
        </style>
        <span class="hide-menu">
        <div id="filtering-side" class="col-md-12" style="display:none;">     
           
        <label for="" id="preparing_desktop"></label>
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="hight:5%">0%</div>
        </div>
            <form action="#" id="period-filter" style="margin-left:10%">

            <div class="form-group">
                    <div class="form-group">
                        <label for="cost_center_description" style="cursor:pointer" onclick="selectedfilter('cost_center_description')"><i class="fa fa-refresh" aria-hidden="true"></i> Lini : </label>
                        <select  name="cost_center_description[]" id="cost_center_description" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>  
            <div class="form-group">
                    <div class="form-group">
                    <label for="nama_biaya" style="cursor:pointer" onclick="selectedfilter_biaya('nama_biaya')"><i class="fa fa-refresh" aria-hidden="true"></i> Biaya : </label>
                        
                        <select  name="nama_biaya[]" id="nama_biaya" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div> 
            <div class="form-group">
                    <div class="form-group">
                        <label for="gl_account_description" style="cursor:pointer" onclick="selectedfilter_uraian('gl_account_description')"><i class="fa fa-refresh" aria-hidden="true"></i> Uraian : </label>
                        <select  name="gl_account_description[]" id="gl_account_description" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">
                        
                            
			    	    </select>
                    </div>
            </div>

                                
            </form>
            <div class="form-group" align="center">
                   <!--<button id="btn-filter" class="btn btn-primary btn-block rounded" onClick="jQuery(this).text('Loading');tableses.ajax.reload(null,true);getcardsum();jQuery(this).text('Apply');drawmycanvasbar();return false;">Apply</button>-->
                   <button id="btn-filter" class="btn btn-primary" onClick="$('html, body').animate({ scrollTop: $('body').offset().top }, 'slow');choosetableperiod($('#head_filter').val())"><i class="fa fa-search" aria-hidden="true"></i> Apply</button>
            </div>
        </div>
</span>
        
        
