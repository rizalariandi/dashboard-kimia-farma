<input type="hidden" id="pathData" value="<?= base_url('index.php/' . $this->uri->segment('3')) ?>">

<style>
    .table{
        font-size:1,
    }


    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #1565c0;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    .bounce-6 {
        animation-name: bounce-6;
        animation-timing-function: ease;
    }
    @keyframes bounce-6 {
        0%   { transform: scale(1,1)      translateY(0); }
        10%  { transform: scale(1.1,.9)   translateY(0); }
        30%  { transform: scale(.9,1.1)   translateY(-100px); }
        50%  { transform: scale(1.05,.95) translateY(0); }
        57%  { transform: scale(1,1)      translateY(-7px); }
        64%  { transform: scale(1,1)      translateY(0); }
        100% { transform: scale(1,1)      translateY(0); }
    }
</style>
<div class="col-md-">
    <div class="card">

        <div class="page-inner mt-10">
            <div class="row mt-20">
                <div class="card full-hight col-md-12 mt-100">
                    <div align="center">
                        <h1>Marketing Expenses Information</h1>
                    </div>
                </div>
                <br/>
                <div class="card full-hight col-md-12 mt-100" style="margin-top:20px;">
                    <div class="col-md-6">
                        <form class="form-inline" id="form-filter">
                            <div class="form-group">
                                <label for="email">Periode* </label>
                                <input type="text" class="form-control datepicker"  name="tanggal_faktur_start"  id="tanggal_faktur_start">
                            </div>
                            <div class="form-group">
                                <label for="email">sd </label>
                                <input type="text" class="form-control datepicker"  name="tanggal_faktur_end"  id="tanggal_faktur_end">
                            </div>
                        </form>
                    </div>
                    <!-- <div class="col-md-2">
                    </div> -->
                    <div class="col-md-6">
                        <form id="form-header" class="form-inline">
                            <div class="form-group">
                                <label for="head-display" style="margin-left:">Filter by*</label>
                                <select name="head_filter" id="head_filter" class="form-control select2">
                                    <option value=""></option>
                                    <option value="cost_center">LINI</option>
                                    <option value="biaya" >BIAYA</option>
                                    <option value="gl_account" >URAIAN</option>
                                    <option value="gpm_pm_code" >GPM/PM</option>
                                    <option value="rsm_shopper_code" >RSM/SHOPPER</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <br/>

    </div>
    <div align="center" style="margin-top:0%">
        <h3>Report Biaya SBU Marketing & Sales</h3>
    </div>
    <div align="center">
        <button class="btn btn-primary" onClick="choosetable(jQuery('#head_filter').val())">Apply Filter <i class="fa fa-search" aria-hidden="true"></i></button>
        <button class="btn btn-primary btn-success btn-generate" onclick=' var filename =&quot;<?= "marketing_expences_information" ?>&quot;; var url = &quot;<?= base_url("marketing_expences_information/downloadexcel");?>&quot;; generateexcel(url,filename);'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>
        <button style="display:none" class="btn btn-danger btn-download" onclick=' var filename =&quot;<?= "marketing_expences_information" ?>&quot;; var url = &quot;<?= base_url("marketing_expences_information/downloadexcel");?>&quot;; downloaddataexcel(url,filename);' ><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>
    </div>
</div>
</div>
<div id="myTable" class="col-md-12">
    <table id="datatablesE" class="table table-striped table-bordered" width="100%">
    </table>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Marketing Expenses Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" align="center">
                    <h3 id="totaltext"></h3>
                    <div id="myTableModal">

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function generateexcel(url,filename){
        if(jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#head_filter").val() == "" )  {
            swal("Pastikan Periode, dan Filter terisi");
            return false;
        }  else{
            jQuery(".stage").show();
            jQuery(".box h1").text("Generate data excel....");
            jQuery(".btn-generate").hide();


            //console.log(filename);
            printdata(url,filename);
        }
    }
    function downloaddataexcel(url,filename){
        jQuery(".btn-generate").show();
        jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open(jQuery('#basedurl').val()+"/file/"+filename+".xls","","_blank","height=1000,width=800");
    }
</script>

