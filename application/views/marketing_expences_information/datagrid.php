<input type="hidden" id="pathData" value="<?= base_url('index.php/' . $this->uri->segment('3')) ?>">
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" type="text/css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js "></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<style>
    .pointer {
        cursor: pointer;
    }

    .modal-header .close {
        position: absolute;
        top: 8px;
        right: 8px;
        font-size: 2em;
    }

    .dataTables_processing {
        z-index: 3000;
    }

    #chart_expen
    
    .table {
        font-size: 1,
    }

    table thead {
        background: #F79966;

        color: white;
    }

    /* #th-target {
        align: right;
    } */

    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #1565c0;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }

    .bounce-6 {
        animation-name: bounce-6;
        animation-timing-function: ease;
    }

    @keyframes bounce-6 {
        0% {
            transform: scale(1, 1) translateY(0);
        }

        10% {
            transform: scale(1.1, .9) translateY(0);
        }

        30% {
            transform: scale(.9, 1.1) translateY(-100px);
        }

        50% {
            transform: scale(1.05, .95) translateY(0);
        }

        57% {
            transform: scale(1, 1) translateY(-7px);
        }

        64% {
            transform: scale(1, 1) translateY(0);
        }

        100% {
            transform: scale(1, 1) translateY(0);
        }
    }

    .scrollTable{
        overflow-y: scroll;
        height: 400px;
        clear:both; 
    }
    thead th {
        white-space: nowrap;
        font-size: 10px;
    }

    div.dataTables_wrapper {
        margin: 0 auto;
    }

    #datatablesE tbody .enabled {
        cursor: pointer !important;
        color: #008efa !important;
    }
</style>
<?php
$bulan = array(
    0 => array('no' => 1, 'nama' => 'Januari'),
    1 => array('no' => 2, 'nama' => 'Februari'),
    2 => array('no' => 3, 'nama' => 'Maret'),
    3 => array('no' => 4, 'nama' => 'April'),
    4 => array('no' => 5, 'nama' => 'Mei'),
    5 => array('no' => 6, 'nama' => 'Juni'),
    6 => array('no' => 7, 'nama' => 'Juli'),
    7 => array('no' => 8, 'nama' => 'Agustus'),
    8 => array('no' => 9, 'nama' => 'September'),
    9 => array('no' => 10, 'nama' => 'Oktober'),
    10 => array('no' => 11, 'nama' => 'November'),
    11 => array('no' => 12, 'nama' => 'Desember'),
);

//print_r($bulan);die();

?>
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
<div class="col-md-">
    <div class="card">

        <div class="page-inner mt-10">
            <div class="row mt-20">
                <div class="card full-hight col-md-12 mt-100">
                    <div align="center">
                        <h1>Marketing Expenses Information</h1>

                    </div>
                </div>
                <div class="card full-hight col-md-12 mt-100" style="padding-bottom:5%">
                    <div class="form-inline">
                        <div class="col-md-6">
                            <form id="form-filter">
                                <div class="form-group">

                                    <div class='form-inline'>
                                        <label for="">Periode</label>
                                        <label for="email"></label>
                                        <select style="width:25%; font-size:12px;" name="bulan_awal" id="bulan" class="form-control bulan_awal" onchange="generatefilename()">
                                            <?php
                                                for ($i = 0; $i < count($bulan); $i++) {
                                                    echo '<option value="' . $bulan[$i]['no'] . '">' . $bulan[$i]['nama'] . '</option>';
                                                }
                                            ?>
                                        </select>
                                        <label for="pwd"><b>&nbsp;to&nbsp;</b></label>
                                        <select style="width:25%; font-size:12px;" name="bulan_akhir" id="bulan" class="form-control bulan_akhir" onchange="generatefilename()">
                                            <?php
                                                $now = date('m') - 1;

                                                for ($i = 0; $i < count($bulan); $i++) {
                                                    if ($i == $now){
                                                        echo '<option selected value="' . $bulan[$i]['no'] . '">' . $bulan[$i]['nama'] . '</option>';
                                                    }else{
                                                        echo '<option value="' . $bulan[$i]['no'] . '">' . $bulan[$i]['nama'] . '</option>';
                                                    }
                                                }
                                            ?>
                                        </select>

                                        <select style="width:20%;font-size:12px;" name="tahun" id="tahun" class="form-control" onchange="generatefilename()">
                                            <?php
                                            $year = date('Y');

                                            for ($i = ($year - 2); $i <= $year; $i++) {
                                                if ($i == $year) {
                                                    echo '<option value="' . $i . '" selected>' . $i . '</option>';
                                                } else {
                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <form id="form-header">
                            <div class="col-md-6 float-right">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label for="head-display">Filter by</label>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">:</label>
                                    </div>
                                    <div class="col-md-5">
                                        <!--<select onchange="choosetable(jQuery(this).val())" name="head_filter" id="head_filter" class="form-control select2">-->
                                        <select name="head_filter" id="head_filter" class="form-control select2" onchange="generatefilename()">
                                            <option value=""></option>
                                            <option selected value="cost_center">LINI</option>
                                            <option value="nama_biaya">BIAYA</option>
                                            <option value="gl_account_description">URAIAN</option>
                                            <!-- <option value="gpm_pm_code" >GPM/PM</option>
                                            <option value="rsm_shopper_code" >RSM/SHOPPER</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div align="center">

            </div>
        </div>
        <div align="center" style="margin-top:0%">
            <h3>Report Biaya SBU Marketing & Sales</h3>
        </div>
    </div>
</div>
<div id=chart_expen></div>
<br>
<div id="myTable" class="scrollTable" style="overflow:auto;"></div>
<div align="center" style="padding-bottom:100%">
    <input type="hidden" id="filename" value="">
    <button class="btn " style="background-color:#073990;color:white" onClick="choosetableperiod($('#head_filter').val())">Apply Filter <i class="fa fa-search" aria-hidden="true"></i></button>
    <!-- <button class="btn btn-generate" style="background-color:#073990;color:white" onclick=' var filename =generatefilename(); var url = &quot;<?= base_url("index.php/Marketing_expences_information/downloadexcel"); ?>&quot;; generateexcel(url,filename);'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>
    <button style="display:none;background-color:#073990;color:white" class="btn btn-download" onclick=' var filename =generatefilename(); var url = &quot;<?= base_url("index.php/Marketing_expences_information/downloadexcel"); ?>&quot;; downloaddataexcel(url,filename);' ><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>-->
</div>

<!-- Modal -->
<div id="myModalex" class="modal" style="background: #0000004d;">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Marketing Expenses Information</h4>
                <button type="button" class="close" onclick="closeModal()">&times;</button>
            </div>
            <div class="modal-body">
                <table id='datatablesModals' style='overflow:auto;' class='table table-striped table-bordered' width='100%'>
                    <thead>
                        <th id="th-1">Enter Document</th>
                        <th id="th-2">Cost Center</th>
                        <th id="th-3">Document Number</th>
                        <th id="th-4">Line Item</th>
                        <th id="th-5">Expense</th>
                        <th id="th-6">Text</th>
                    </thead>
                </table>
            </div>

        </div>

    </div>
</div>

</div>

<?php
$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$config['base_url'] .= "://" . $_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
$config['base_url'] .= "";

?>
<script>
    function generateexcel(url, filename) {
        if (jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#head_filter").val() == "") {
            swal("Pastikan Periode, dan Filter terisi");
            return false;
        } else {
            jQuery(".stage").show();
            jQuery(".box h1").text("Generate data excel....");
            jQuery(".btn-generate").hide();


            //console.log(filename);
            printdata(url, filename);
        }
    }

    function downloaddataexcel(url, filename) {
        jQuery(".btn-generate").show();
        jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open("<?php echo $config['base_url'] ?>/file/" + filename + ".xls", "", "_blank", "height=1000,width=800");
    }
</script>