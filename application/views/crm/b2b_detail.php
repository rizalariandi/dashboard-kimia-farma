<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
.error {
    color: #f44336; 
}

th{
    color : #000000 !important;
}
.datatable-header, .datatable-footer {
    padding: 10px;
}
</style>

<style>
    .table-borderless>tbody>tr>td {
        border: 0px;
    }
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
       padding: 8px;
    }
    .table-level > thead > tr > th {
        background-color: #093890;
        color:#ffffff !important;
    }
    .table-level > tbody > tr > td {
        background-color: #f7faff;
        color: #1d3563 !important;
    }
    .font-lg {
        font-size:19px;
    }

    /*-------------------------------*/
    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 160px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }
    /*-------------------------------*/
</style>

<?php
$months = array( '1' => 'Jan',
    '2' => 'Feb',
    '3' => 'Mar',
    '4' => 'Apr',
    '5' => 'Mei',
    '6' => 'Juni',
    '7' => 'Juli',
    '8' => 'Ags',
    '9' => 'Sep',
    '10' => 'Okt',
    '11' => 'Nov',
    '12' => 'Des');
?>

<!-- .row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <div id="sla-data-range" class="mrp-container nav navbar-nav">
                            <div class="mrp-monthdisplay">
                                <span class="mrp-lowerMonth"><?= $start3 ?></span>
                                <span class="mrp-to"> to </span>
                                <span class="mrp-upperMonth"><?= $end3 ?></span>
                            </div>
                            <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                            <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <select name="lini" id="lini" class="form-control">
                            <option value="all">Semua Lini</option>
                            <?php foreach($liniOption as $k => $p) { ?>
                                <option value="<?=$k?>" <?=($lini==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="channel" id="channel" class="form-control">
                            <option value="all">Semua Channel</option>
                            <?php foreach($channelOption as $k=> $p) { ?>
                                <option value="<?=$k?>" <?=($channel==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="layanan" id="layanan" class="form-control">
                            <option value="all">Semua Layanan</option>
                            <?php foreach($layananOption as $k=> $p) { ?>
                                <option value="<?=$k?>" <?=($layanan==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Customer Profile
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td width="20%">Nama Perusahaan</td>
                        <td width="3%">:</td>
                        <td width="78%"><?= $detail->bcc_cust_name ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?= $detail->cct_lokasi ?></td>
                    </tr>
                    <tr>
                        <td>Tipe Channel</td>
                        <td>:</td>
                        <td><?= $detail->bcc_type_channel ?></td>
                    </tr>
                    <tr>
                        <td>No. Telp</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>PIC / CP</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <table class="table table-level" style="width:70%">
                    <thead>
                    <tr>
                        <th>Lini</th>
                        <th style="text-align: center">Level</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $color = y_level_color(); foreach($lini_level as $as_lini => $as_level) {
                        $c = y_cl(strtolower($as_level));
                    ?>
                    <tr>
                        <td><?= $as_lini ?></td>
                        <td style="text-align: center"><i class="fa fa-star font-lg" style="color:<?= $c ?>"></i></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5 class="box-title"><strong>Lini</strong></h5>
                <div id="lini-chart"></div>
            </div>
            <div class="col-md-6">
            <h5 class="box-title"><strong>Produk</strong></h5>
                <div id="prod-chart"></div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-8" style="text-align: right;">
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-program')"><i class="fa fa-shopping-bag mr-1"></i> Rekomendasi Program Marketing</button>
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-prediksi')"><i class="fa fa-star mr-1"></i> Prediksi Tipe Konsumen</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Transaction History
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="table_transaction">
                    <thead>
                    <tr>
                        <th width="12%" style="color:#797979 !important">Tanggal</th>
                        <th width="15%" style="color:#797979 !important">Brand</th>
                        <th width="35%" style="color:#797979 !important">SKU</th>
                        <th width="15%" style="color:#797979 !important">Lokasi</th>
                        <th width="15%" style="color:#797979 !important">Rp</th>
                        <th width="8%" style="color:#797979 !important">Qty</th>
                    </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                History Reward
            </div>
            <table class="table table-striped" id="table_reward">
                <thead>
                <tr>
                    <th width="20%" style="color:#797979 !important">Tanggal</th>
                    <th width="20%" style="color:#797979 !important">Nama Produk</th>
                    <th width="20%" style="color:#797979 !important">Material</th>
                    <th width="20%" style="color:#797979 !important">Level</th>
                    <th width="10%" style="color:#797979 !important">Area</th>
                    <th width="10%" style="color:#797979 !important">Reward</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                History Retur
            </div>
            <table class="table table-striped" id="table_return">
                <thead>
                <tr>
                    <th width="10%" style="color:#797979 !important">Tanggal</th>
                    <th width="20%" style="color:#797979 !important">Brand</th>
                    <th width="40%" style="color:#797979 !important">SKU</th>
                    <th width="10%" style="color:#797979 !important">Qty</th>
                    <th width="20%" style="color:#797979 !important">Area</th>
                </tr>
                </thead>
                <tbody> 
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Prediksi -->
<div class="modal fade bs-example-modal-lg" id="box-prediksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Prediksi Tipe Konsumen</h4>
            </div>
            <div class="modal-body">
                <div id="prediksi-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<div class="modal fade bs-example-modal-lg" id="box-program" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Rekomendasi Program Marketing</h4>
            </div>
            <div class="modal-body">
                <table class="table table-level" style="width:100%">
                    <thead>
                    <tr>
                        <th width="25%">Lini</th>
                        <th width="10%" style="text-align: center">Level</th>
                        <th width="65%">Rekomendasi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $color = y_level_color(); foreach($lini_level as $as_lini => $as_level) { $c = y_cl(strtolower($as_level));?>
                        <tr>
                            <td><?= $as_lini ?></td>
                            <td style="text-align: center"><i class="fa fa-star font-lg" style="color:<?= $c ?>; text-shadow: 0 0 5px #333;""></i></td>
                            <td><?= isset($lini_rec[strtoupper($as_lini)][strtoupper($as_level)]) ? $lini_rec[strtoupper($as_lini)][strtoupper($as_level)] : '' ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<div id="debug"></div>
<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>
<script src="<?= base_url() ?>assets/validation/validate.min.js"></script>
<script src="<?= base_url() ?>assets/plugin/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/grouped-categories.js"></script>

<script>
var transaction 	= '#table_transaction';   
var reward 		    = '#table_reward';   
var returns 		= '#table_return';  
var baseurl = '<?=base_url()?>index.php/crm_b2b';

var default_startDate = $('#mrp-lowerDate').val();
var default_endDate = $('#mrp-upperDate').val();

$(document).ready(function () {
    var data_lini = <?= json_encode($graph_lini) ?>;
  

    chart('lini-chart', data_lini);
    //chart('prod-chart', data_prod);
    product();
    spline();

    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    $(transaction).dataTable({
        'ajax': {
            'url':baseurl+'/json_transaction',
            'data' : function(data) {
                data.id	    = '<?= $detail->cct_cust_code ?>';
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini   = $('#lini').val();
                data.channel	= $('#channel').val();
                data.layanan	= $('#layanan').val();
            },
            'method' : 'post'
        },
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"Bt><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
    });
    $(returns).dataTable({
        'ajax': {
            'url':baseurl+'/json_return',
            'data' : function(data) {
                data.id	    = '<?= $detail->cct_cust_code ?>';
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
            },
            'method' : 'post'
        },
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        "scrollY": "500px",
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"Bt><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
    });


    $(reward).dataTable({
        'ajax': {
            'url':baseurl+'/json_reward',
            'data' : function(data) {
                data.id	    = '<?= $detail->cct_cust_code ?>';
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
            },
            'method' : 'post'
        },
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        "scrollY": "500px",
        autoWidth: false,
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"Bt><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
    });

  
});

function filter() {
    let start_date = $('#mrp-lowerDate').val();
    let end_date = $('#mrp-upperDate').val();
    let start_splited = start_date.split("-");
    let end_splited = end_date.split("-");

    if (( parseInt(start_splited[1]) !== parseInt(end_splited[1])) || (parseInt(start_splited[0]) > parseInt(end_splited[0]))){
        // alert
        swal({
            icon: 'error',
            title: 'Terjadi Kesalahan!!',
            text: 'Tahun di Start Date dan End Date Harus Sama!'
        });

        start_date = default_startDate;
        end_date = default_endDate;

        window.location.href='<?= base_url() ?>index.php/crm_b2b/detail/<?= $detail->cct_cust_code ?>/'+start_date+'/'+end_date+'/'+$("#lini").val()+'/'+$("#channel").val()+'/'+$("#layanan").val();
    }else{
        window.location.href='<?= base_url() ?>index.php/crm_b2b/detail/<?= $detail->cct_cust_code ?>/'+start_date+'/'+end_date+'/'+$("#lini").val()+'/'+$("#channel").val()+'/'+$("#layanan").val();
    }

    // window.location.href='<?= base_url() ?>index.php/crm_b2b/detail/<?= $detail->cct_cust_code ?>/'+$('#mrp-lowerDate').val()+'/'+$('#mrp-upperDate').val()+'/'+$("#lini").val()+'/'+$("#channel").val()+'/'+$("#layanan").val();
    
}

function box(id)
{
    $('#'+id).modal({keyboard: false, backdrop: 'static'});
}

function chart(id, data)
{
    Highcharts.chart(id, {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Value (Rp)'
            }
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: false
                }//,
                //enableMouseTracking: false
            }
        },
        series: data
    });
}

function spline()
{
    Highcharts.chart('prediksi-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [{
                name: '<?= $year ?>',
                categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
            }],
            plotBands: [{
                color: '#efefef',
                from: -1,
                to: <?= $level_point ?>,
                label: {
                    text: 'Data Real',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                color: '#CFD1D1',
                from: <?= $level_point ?>,
                to: 12,
                label: {
                    text: 'Rekomendasi Prediksi',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        yAxis: {
            title: {
                text: 'Sales Profit'
            },
            plotLines: [{
                value: 250000000,
                color: 'red',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Bottom Sales'
                }
            }, {
                value: 325000000,
                color: 'green',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Goal Sales <?= $year ?>'
                }
            }]
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        if(this.y == 1 || this.y == 2)
                            return 'Reguler';
                        else if(this.y == 3 || this.y == 4)
                            return 'Bronze';
                        else if(this.y == 5 || this.y == 6 || this.y == 7)
                            return 'Silver';
                        else if(this.y == 8)
                            return 'Gold';
                    }
                },
                enableMouseTracking: false
            }
        },
        series: <?= json_encode($prediksi) ?>
    });
}

function product()
{
    Highcharts.chart('prod-chart', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: [{
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Value (Rp)',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Total SKU',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'horizontal'
        },
        series: [{
            name: 'Value',
            type: 'column',
            yAxis: 0,
            data: <?= json_encode($bulan_rev) ?>,
            tooltip: {
                valuePrefix: 'Rp. '
            }

        }, {
            name: 'SKU',
            type: 'spline',
            yAxis: 1,
            data: <?= json_encode($bulan_sku) ?>,
            tooltip: {
                valueSuffix: ' SKU'
            }
        }]
    });

    rangemonth('<?= $start ?>', '<?= $end ?>');
}

//------------------------------------//
var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}
</script>