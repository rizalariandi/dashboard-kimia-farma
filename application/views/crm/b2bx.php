<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />

<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>


<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }

    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }

    .area-item {
        font-weight: bold;
        margin-right: 10px;
    }

    /*-------------------------------*/
    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 160px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }
    /*-------------------------------*/

</style>

<!-- .row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body"> 
                 <div class="row">
                     <div class="col-md-2">
                         <div id="sla-data-range" class="mrp-container nav navbar-nav">
                             <div class="mrp-monthdisplay">
                                 <span class="mrp-lowerMonth"><?= $start3 ?></span>
                                 <span class="mrp-to"> to </span>
                                 <span class="mrp-upperMonth"><?= $end3 ?></span>
                             </div>
                             <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                             <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
                         </div>
                     </div>
                    <div class="col-md-2">
                        <select name="lini" id="lini" class="form-control">
                            <option value="all">Semua Lini</option>
                            <?php foreach($liniOption as $k => $p) { ?>
                                <option value="<?=$k?>" <?=($lini==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="channel" id="channel" class="form-control">
                            <option value="all">Semua Channel</option>
                            <?php foreach($channelOption as $k=> $p) { ?>
                                <option value="<?=$k?>" <?=($channel==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                     <div class="col-md-2">
                         <select name="area" id="area" class="form-control">
                             <option value="all">Semua Cabang</option>
                             <?php foreach($areaOption as $k=> $p) { ?>
                                 <option value="<?=$k?>" <?=($area==$k?'selected':'')?>><?=$p?></option>
                             <?php } ?>
                         </select>
                     </div>
                </div>
                <div class="row" style="margin-top:10px">
                    <div class="col-md-2">
                        <select name="layanan" id="layanan" class="form-control">
                            <option value="all">Semua Group Layanan</option>
                            <?php foreach($layananOption as $k=> $p) { ?>
                                <option value="<?=$k?>" <?=($layanan==$k?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="layanan_detail" id="layanan_detail" class="form-control">
                            <option value="all">Semua Layanan</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <p align="center">
            <span style="font-weight: bold; margin-right:15px"><i class="fa fa-circle" style="color:<?= y_cl('gold') ?>"></i> &nbsp; Gold</span>
            <span style="font-weight: bold; margin-right:15px"><i class="fa fa-circle" style="color:<?= y_cl('bronze') ?>"></i> &nbsp; Bronze</span>
            <span style="font-weight: bold; margin-right:15px"><i class="fa fa-circle" style="color:<?= y_cl('silver') ?>"></i> &nbsp; Silver</span>
            <span style="font-weight: bold; margin-right:15px"><i class="fa fa-circle" style="color:<?= y_cl('reguler') ?>"></i> &nbsp; Reguler</span>
        </p>
        <div class="row">
            <div class="col-md-6">
                <div id="scatter-chart"></div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div id="level-chart"></div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<div class="panel panel-default">
    <div class="panel-heading">
        Customer Segmentation
    </div>
    <div class="panel-heading" style="text-transform: none; font-weight: normal">
        Periode: <span id="cs_periode" class="font-bold"></span>; &nbsp;
        Lini: <span id="cs_lini" class="font-bold"></span>; &nbsp;
        Layanan: <span id="cs_layanan" class="font-bold"></span>
    </div>
    <div class="panel-body"> 
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="tables">
                    <thead>
                    <tr>
                        <th width="20%" style="color:#797979 !important">Nama Perusahaan</th>
                        <th width="10%" style="color:#797979 !important">Kode Customer</th>
                        <th width="15%" style="color:#797979 !important">Cabang</th>
                        <th width="15%" style="color:#797979 !important">Channel</th>
                        <th width="10%" style="color:#797979 !important">Layanan</th>
                        <th width="10%" style="color:#797979 !important">Value</th>
                        <th width="10%" style="color:#797979 !important">Freq</th>
                        <th width="10%" style="color:#797979 !important">Level</th>
                    </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/heatmap.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/treemap.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>

var tb 		= '#tables';   
var baseurl = '<?=base_url()?>index.php/crm_b2b'; 
$(document).ready(function () { 
    $('#periode').datepicker({
        format: "mm/yyyy",
        viewMode: "months",
        minViewMode: "months"
    });

    var reguler = null;
    var silver = null;
    var gold = null;
    var bronze = null;

    var all_reguler = <?= json_encode($type['all']['regular']); ?>;
    var all_silver = <?= json_encode($type['all']['silver']); ?>;
    var all_gold = <?= json_encode($type['all']['gold']); ?>;
    var all_bronze = <?= json_encode($type['all']['bronze']); ?>;

    scatter(all_gold, all_silver, all_bronze, all_reguler);
    pie();

    $(tb).dataTable({
        'ajax': {
            'url':baseurl+'/json',
            'data' : function(data) {
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini		= $('#lini').val(); 
                data.area		= $('#area').val(); 
                data.channel	= $('#channel').val();
                data.layanan	= $('#layanan').val();
                data.layanan_detail	= '<?= $var_layanan_detail ?>';
            }, 
            'method' : 'post'
        }, 
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
            { targets: [-1], searchable: false}
        ],
        "drawCallback": function( settings ) { 
        },
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        'scrollX': true, 
        autoWidth: false,
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"Bt><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ], 
        "pageLength": 30, // default records per page 
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading, 
        initComplete: function() {  
            
        }
    });

    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

    rangemonth('<?= $start ?>', '<?= $end ?>');

    $('#cs_periode').html($('.mrp-lowerMonth').html()+' - '+$('.mrp-upperMonth').html());
    $('#cs_lini').html($('#lini').val());
    $('#cs_layanan').html($('#layanan').val());

    $('#layanan').on('change', function (e) {
        if($(this).val() != "") {
            $.ajax({
                url: baseurl+'/layanan_detail',
                type:'post',
                dataType:'json',
                data: {
                    id : $(this).val()
                },
                success : function(e) {
                    var temp = "<option value=''>All Layanan</option>";
                    $.each( e, function( key, val ) {
                        temp +=  "<option value='"+val.id+"'>"+val.text+"</option>";
                    });
                    $('#layanan_detail').html(temp);
                }
            });
        }
        else {
            $('#layanan_detail').html("<option value=''>All Layanan</option>");
        }
    });

    $('#layanan').trigger('change');
});

function filter() {
    //var temp = $("#periode").val().split(" - ");
    //var start = temp[0].split('/').join('-');
    //var end   = temp[1].split('/').join('-');
    // console.log(start);
    // console.log(end);

    window.location.href='<?= base_url() ?>index.php/crm_b2b/index/'+$('#mrp-lowerDate').val()+'/'+$('#mrp-upperDate').val()+'/'+$("#lini").val()+'/'+$("#channel").val()+'/'+$("#layanan").val()+'/'+$("#area").val()+'/'+$("#layanan_detail").val();
}

function scatter(gold, silver, bronze, reguler)
{
    var max_value = <?= $max_value ?>;
    var max_freq = <?= $max_freq ?>;
    Highcharts.chart('scatter-chart', {
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            title: {
                enabled: true,
                text: null
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            gridLineWidth: 1,
            min: 0,
            max: max_freq
        },
        yAxis: {
            title: {
                text: null
            },
            min: 0,
            max: max_value
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        tooltip: {
            shared: true,
            style: {
                fontWeight: 'normal',
                fontSize: '10px'
            },
            formatter: function() {
                var x = this.x;
                var txt = '';
                if(this.y > max_value)
                    var y = max_value;
                else
                    var y = this.y;

                $.each(this.series.data,function(i,p) {
                    if(p.x === x && p.y === y) {
                        txt += '<b>'+p.name+'</b> ('+p.series.name+')<br>Value: Rp. '+p.label+'; Frekuensi: '+p.x+'<br><br>';
                    }
                });

                return txt;
            }
            //headerFormat: '',
            //pointFormat: '<b>{point.name}</b> ({series.name})<br>Value: Rp. {point.label}<br>Frekuensi: {point.x}'
        },
        plotOptions: {
            scatter: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        var x = this.x;
                        var c = 0;
                        if(this.y > max_value)
                            var y = max_value;
                        else
                            var y = this.y;

                        $.each(this.series.data,function(i,p) {
                            if(p.x === x && p.y === y) {
                                c = parseInt(c)+parseInt(1);
                            }
                        });

                        if(c > 1)
                            return c;
                        else
                            return null;
                    }
                },
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    },
                    cursor: 'pointer'
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                cursor: 'pointer',
                events: {
                    click: function (event) {
                        box();
                    }
                }

            }
        },
        series: [
            {
                turboThreshold:0,
                showInLegend: false,
                marker: {
                    symbol: 'circle'
                },
                name: 'Gold',
                color: '<?= y_cl('gold') ?>',
                data: gold
            },
            {
                turboThreshold:0,
                showInLegend: false,
                marker: {
                    symbol: 'circle'
                },
                name: 'Silver',
                color: '<?= y_cl('silver') ?>',
                data: silver
            },
            {
                turboThreshold:0,
                showInLegend: false,
                marker: {
                    symbol: 'circle'
                },
                name: 'Bronze',
                color: '<?= y_cl('bronze') ?>',
                data: bronze
            },
            {
                turboThreshold:0,
                showInLegend: false,
                marker: {
                    symbol: 'circle',
                    lineColor: '#999999',
                    lineWidth: 1
                },
                name: 'Reguler',
                color: '<?= y_cl('regular') ?>',
                data: reguler
            }
        ]
    }, function(chart) { // on complete

        var width = chart.plotBox.width / 2.0;
        var height = chart.plotBox.height / 2.0 + 1;

        var w1 = width;
        var h1 = height;

        var w2 = width;
        var h2 = height;

        //silver
        chart.renderer.rect(chart.plotBox.x,chart.plotBox.y, w2, height, 1)
            .on('click', function() {
                alert('yo');
            })
            .attr({
                fill: '#ebebeb',
                zIndex: 0
            })
            .add();

        //platinum
        chart.renderer.rect(chart.plotBox.x + w2,
            chart.plotBox.y, w1, height, 1)
            .attr({
                fill: '#fdeaca',
                zIndex: 0
            })
            .add();

        //reguler
        chart.renderer.rect(chart.plotBox.x,
            chart.plotBox.y + height, w2, height, 1)
            .attr({
                fill: '#ffffff',
                zIndex: 0
            })
            .add();

        //gold
        chart.renderer.rect(chart.plotBox.x + w2,
            chart.plotBox.y + height, w1, height, 1)
            .attr({
                fill: '#efd9c2',
                zIndex: 0
            })
            .add();
    });
}

function pie()
{
    Highcharts.chart('level-chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            renderTo: 'container',
            events: {
                load: function(event) {
                    var total = 0; // get total of data
                    for (var i = 0, len = this.series[0].yData.length; i < len; i++) {
                        total += this.series[0].yData[i];
                    }
                    var text = this.renderer.text(
                        'Total<br><strong style="font-size: 16px; font-weight: bold">' + total + '</strong>',
                        this.plotLeft+165,
                        this.plotTop+180
                    ).attr({
                        zIndex: 115
                    }).add() // write it to the upper left hand corner
                }
            }
        },
        title: {
            text: null
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}<br>{point.percentage:.1f} %</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: 1,
                    format: '{point.y} <br>{point.percentage:.1f} %'
                },
                showInLegend: false,
            }
        },
        legend: {
            align: 'right',
            layout: 'vertical',
            verticalAlign: 'top ',
            y: 100
        },
        series: [{
            name: 'Channel',
            colorByPoint: true,
            innerSize: '50%',
            data: <?= json_encode($pie); ?>
        }]
    });
}

function product()
{
    Highcharts.chart('product-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Sales Won and Negotiation > 70%'
            }
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Product 1',
            data: [118545000, 152801000, 185286000, 226950000, 227276000, 235679000, 257823400, 311491000, 311491000, 311491000, 311491000, 311491000]
        }, {
            name: 'Product 2',
            data: [11854500, 15280100, 18528600, 22695000, 22727600, 23567900, 25782340, 31149100, 31149100, 31149100, 31149100, 31149100]
        }]
    });
}

function age()
{
    Highcharts.chart('age-chart', {
        chart: {
            type: 'bar'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: [
                '< 10',
                '10-20',
                '21-30',
                '31-50',
                '>50'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        tooltip: {
            pointFormat: '{point.y:.1f}'
        },
        plotOptions: {
            bar: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'a',
            data: [49.9, 71.5, 106.4, 129.2, 144.0],
            showInLegend: false,
        }]
    });
}

function gender()
{
    Highcharts.chart('gender-chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: [
                'Laki-laki',
                'Perempuan'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            /*title: {
                text: 'Rainfall (mm)'
            }*/
        },
        tooltip: {
            pointFormat: '{point.y:.1f}'
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'a',
            data: [49.9, 71.5],
            showInLegend: false,
        }]
    });
}

//------------------------------------//
var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}
</script>