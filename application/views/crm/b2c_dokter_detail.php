<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
.error {
    color: #f44336; 
}

th{
    color : #000000 !important;
}
</style>

<style>
    .table-borderless>tbody>tr>td {
        border: 0px;
    }
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
       padding: 8px;
    }
    .table-level > thead > tr > th {
        background-color: #093890;
        color:#ffffff !important;
    }
    .table-level > tbody > tr > td {
        background-color: #f7faff;
        color: #1d3563 !important;
    }
    .font-lg {
        font-size:19px;
    }
</style>

<?php
$months = array( '1' => 'Jan',
    '2' => 'Feb',
    '3' => 'Mar',
    '4' => 'Apr',
    '5' => 'Mei',
    '6' => 'Juni',
    '7' => 'Juli',
    '8' => 'Ags',
    '9' => 'Sep',
    '10' => 'Okt',
    '11' => 'Nov',
    '12' => 'Des');
?>

<div class="panel panel-default">
    <div class="panel-heading">
        Customer Profile
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td width="27%">Nama Dokter</td>
                        <td width="3%">:</td>
                        <td width="70%"><?= strtoupper($detail->doctor) ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?= $detail->bcd_address ?></td>
                    </tr>
                    <tr>
                        <td>No. Telpon</td>
                        <td>:</td>
                        <td><?= $detail->bcd_no_tlpn ?></td>
                    </tr>
                    <tr>
                        <td>Tempat Praktek</td>
                        <td>:</td>
                        <td><?= $detail->bcd_hospital_name ?></td>
                    </tr> 
                    <tr>
                        <td>Poli</td>
                        <td>:</td>
                        <td><?= $detail->bcd_spesialisasi ?></td>
                    </tr>
                    <tr>
                        <td>Level</td>
                        <td>:</td>
                        <td><i class="fa fa-star font-lg" style="color:<?= !empty(array_values($level)[0]) ? y_cl(array_values($level)[0]) : y_cl('') ?>"></i></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6"> 
                <h5 class="box-title"><strong>Purchase History</strong></h5>
                <div id="lini-chart"></div>
            </div>
        </div> 
    </div>
    <div class="panel-footer">
        <div class="row"> 
            <div class="col-md-12">
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-program')"><i class="fa fa-shopping-bag mr-1"></i> Rekomendasi Program Marketing</button>
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-prediksi')"><i class="fa fa-star mr-1"></i> Prediksi Tipe Konsumen</button>
            </div>
        </div> 
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        Transaction History
    </div>
    <div class="panel-body">
        <div class="row">
        </div>
        <div class="row"><br><br>
            <div class="col-md-12">
                <table class="table table-striped" id="tables">
                    <thead>
                    <tr>
                        <th width="15%" style="color:#797979 !important">Tanggal</th>
                        <th width="40%" style="color:#797979 !important">Brand</th>
                        <th width="30%" style="color:#797979 !important">Rp</th>
                        <th width="15%" style="color:#797979 !important">Qty</th>
                    
                
                    </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                History Reward
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" id="xx">
                            <thead>
                            <tr>
                                <th width="20%" style="color:#797979 !important">Tanggal</th>
                                <th width="20%" style="color:#797979 !important">Level</th>
                                <th width="10%" style="color:#797979 !important">Area</th>
                                <th width="10%" style="color:#797979 !important">Reward</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">Data Kosong</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<!-- Prediksi -->
<div class="modal fade bs-example-modal-lg" id="box-prediksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Prediksi Tipe Konsumen</h4>
            </div>
            <div class="modal-body">
                <div id="prediksi-chart" style="height: 400px;"></div>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<div class="modal fade bs-example-modal-lg" id="box-program" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Rekomendasi Program Marketing</h4>
            </div>
            <div class="modal-body">
                <table class="table table-level" style="width:100%">
                    <thead>
                    <tr>
                        <th width="30%" colspan="2" style="text-align: center">Level</th>
                        <th width="70%">Rekomendasi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $color = y_level_color(); $c = y_cl(strtolower($lini_level)); ?>
                    <tr>
                        <td><?= strtoupper($lini_level) ?></td>
                        <td style="text-align: center"><i class="fa fa-star font-lg" style="color:<?= $c ?>; text-shadow: 0 0 5px #333;""></i></td>
                        <td><?= isset($lini_rec[strtoupper($lini_level)][strtoupper($lini_level)]) ? $lini_rec[strtoupper($lini_level)][strtoupper($lini_level)] : '' ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<div id="debug"></div>
<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>
<script src="<?= base_url() ?>assets/validation/validate.min.js"></script>
<script src="<?= base_url() ?>assets/plugin/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/grouped-categories.js"></script>

<script>
var tb 		= '#tables';      
var reward 		= '#table_reward';   
var baseurl = '<?=base_url()?>index.php/crm_b2c_dokter';
$(document).ready(function () {
    var data_lini = <?= json_encode($graph_lini) ?>;
    //var data_prod = < ?= json_encode($prod) ?>;

    chart('lini-chart', data_lini); 
    spline();
    // console.log(location.href,'location.href');
    var arry = location.href.split("/");
    console.log(arry[arry.length -2]);    
    $(tb).dataTable({
        'ajax': {
            'url':baseurl+'/json_detail',
            'data' : function(data) {
                data.id	    = '<?= $detail->doctor ?>'
                data.spesialisasi	  = '<?= $detail->bcd_spesialisasi ?>'
                data.startx = arry[arry.length -3]
                data.endx	  = arry[arry.length -2]
            },
            'method' : 'post'
        },
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"Bt><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,

    });
    
    /*$(reward).dataTable({
        'ajax': {
            'url':baseurl+'/json_reward',
            'data' : function(data) {
                data.id	    = '<?=$detail->bcd_doctor_name ?>'
                data.id	    = '<?=$detail->bcd_doctor_name ?>',
                data.month	    = '<?=$month ?>',
                data.year	    = '<?=$year ?>'
            },
            'method' : 'post'
        },
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,

    });*/
});

function box(id)
{
    $('#'+id).modal({keyboard: false, backdrop: 'static'});
}

function chart(id, data)
{
    Highcharts.chart(id, {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: 'Value: Rp. <b>{point.y}</b>'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Value (Rp)'
            }
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                },
                //enableMouseTracking: false,
                showInLegend: false
            }
        },
        series: data
    });
}

function spline()
{
    Highcharts.chart('prediksi-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            title: {
                text: ''
            },
            categories: [{
                name: '<?=date('Y')?>',
                categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
            }],
            plotBands: [{
                color: '#efefef',
                from: -1,
                to: <?= $level_point ?>,
                label: {
                    text: 'Data Real',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                color: '#CFD1D1',
                from: <?= $level_point ?>,
                to: 12,
                label: {
                    text: 'Rekomendasi Prediksi',
                    style: {
                        color: '#606060'
                    }
                }
            }]
        },
        yAxis: {
            title: {
                text: null
            },
            labels: {
                enabled: false
            }
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        if(this.y == 1 || this.y == 2)
                            return 'Reguler';
                        else if(this.y == 3 || this.y == 4)
                            return 'Bronze';
                        else if(this.y == 5 || this.y == 6 || this.y == 7)
                            return 'Silver';
                        else if(this.y == 8)
                            return 'Gold';
                    }
                },
                enableMouseTracking: false,
                showInLegend: false
            }
        },
        series: <?= json_encode($prediksi) ?>
    });
}
</script>