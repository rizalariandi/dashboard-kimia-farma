<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>


<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }

    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }

    .area-item {
        font-weight: bold;
        margin-right: 10px;
    }
</style>


<!-- .row -->

 

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body"> 
                <div class="row">
                    <!-- <div class="col-md-3">
                        <select name="month" id="month" class="form-control">
                            <option value="0">Semua Bulan</option>
                            <?php foreach($monthOption as $key=> $p) { ?>
                                <option value="<?=$key?>" <?=($month==$key?'selected':'')?>><?=$p?></option>
                            <?php } ?> 
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="year" id="year" class="form-control">
                            <?php foreach($yearOption as $p) { ?>
                                <option value="<?=$p?>" <?=($year==$p?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div>  -->
                    
                    
                    <div class="col-md-2"> 
                         <input name="periode" id="periode" class="form-control" value="<?=$periode?>">
                    </div>  
                    <div class="col-md-3">
                        <select name="produk" id="produk" class="form-control">
                            <option value="0">Semua Produk</option> 
                            <?php foreach($produkOption as $p) { ?>
                                <option value="<?=$p?>" <?=($produk==$p?'selected':'')?>><?=$p?></option>
                            <?php } ?>
                        </select>
                    </div> 
                    <div class="col-md-2">
                        <button type="button" name="filter" id="filter" onclick="filter()">Filter</button>
                    </div> 
                </div>   
            </div>
        </div>
    </div>
</div> 

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div id="heatmap-chart">
            </div> 
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <h5 class="box-title"><strong>Product</strong></h5>
                <div id="product-chart"></div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <h5 class="box-title"><strong>Customer Level</strong></h5>
                <div id="level-chart"></div>
            </div>
        </div>
        <div class="row" style="margin-top: 80px">
            <div class="col-md-6">
                <h5 class="box-title"><strong>Gender</strong></h5>
                <div id="gender-chart" style="height: 250px; width: 250px"></div>
            </div>
            <div class="col-md-6">
                <h5 class="box-title"><strong>Klasifikasi Usia</strong></h5>
                <div id="age-chart" style="height: 250px"></div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Customer Segementation
    </div>
    <div class="panel-body"> 
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="tables">
                    <thead>
                    <tr>
                        <th width="20%" style="color:#797979 !important">Nama Perusahaan</th> 
                        <th width="30%" style="color:#797979 !important">Alamat</th> 
                        <th width="10%" style="color:#797979 !important">No. Telp</th>  
                        <th width="5%" style="color:#797979 !important">Lini</th>   
                        <th width="15%" style="color:#797979 !important">Periode</th>   
                        <th width="10%" style="color:#797979 !important">Level</th>
                    </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div> 
        </div>
    </div>
</div>
<!-- /.row -->

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/heatmap.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/treemap.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>


var tb 		= '#tables';   
var baseurl = '<?=base_url()?>index.php/crm_b2c'; 
$(document).ready(function () {

    $('#periode').daterangepicker({
        applyClass: 'bg-primary-600',
        cancelClass: 'btn-light',
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true
    }, function(start, end, label) {
        if(start.format('YYYY') != end.format('YYYY')) {
            alert('Periode harus ditahun yang sama');
            end.setDate(start);
        }
    });

    var data_prod = <?= json_encode($prod) ?>;
    heatmap();
    level(); 
    chart('product-chart', data_prod);
    age();
    gender();

    $(tb).dataTable({
        'ajax': {
            'url':baseurl+'/json',
            'data' : function(data) { 
                data.periode	= $('#periode').val(); 
                data.lini		= $('#lini').val(); 
                data.produk		= $('#produk').val(); 
            }, 
            'method' : 'post'
        }, 
        'order':[
            [0, 'desc']
        ],
        'columnDefs': [   
        ],
        "drawCallback": function( settings ) { 
        },
        'scrollX': true, 
        autoWidth: false,
        dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ], 
        "pageLength": 30, // default records per page 
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading, 
        initComplete: function() {  
            
        }
    });   
 

    $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian'); 
 
}); 

function heatmap()
{
    Highcharts.chart('heatmap-chart', {

        /*chart: {
            type: 'heatmap',
            marginTop: 40,
            marginBottom: 80,
            plotBorderWidth: 1
        },
        title: {
            text: null
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6']
        },
        yAxis: {
            categories: ['1', '2', '3', '4'],
            title: null
        },
        colorAxis: {
            min: 0,
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        },
        legend: {
            align: 'right',
            layout: 'vertical',
            margin: 0,
            verticalAlign: 'top',
            y: 25,
            symbolHeight: 280
        },*/
        tooltip: {
            formatter: function () {
                return 'Total Customer<br><b>' + this.point.colorValue + '</b>';
                    //this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] + '</b>';
            }
        },
        colorAxis: {
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        },
        title: {
            text: null
        },
        series: [{
            type: 'treemap',
            layoutAlgorithm: 'squarified',
            data: <?=$heatmaps ?>
        }]
    });
}

function level()
{
    Highcharts.chart('level-chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: null
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    distance: 1,
                    format: '{point.percentage:.1f} %'
                },
                showInLegend: true,
            }
        },
        legend: {
            align: 'right',
            layout: 'vertical',
            verticalAlign: 'top ',
            y: 100
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            innerSize: '50%',
            data: [{
                name: 'Gold',
                y: <?=$level['gold']?>,
                color : '#f9b94d'
            }, {
                name: 'Silver',
                y: <?=$level['silver']?>,
                color : '#959595'
            }, {
                name: 'Bronze',
                y: <?=$level['bronze']?>,
                color : '#cd8032'
            }, {
                name: 'Reguler',
                y: <?=$level['reguler']?>,
                color : '#f5f5f5'
            }]
        }]
    });
}

function chart(id, data)
{
    Highcharts.chart(id, {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Value (Rp)'
            }
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: false
            }
        },
        series: data
    });
}



function filter() { 
    var temp = $("#periode").val().split(" - "); 
    var start = temp[0].split('/').join('-');
    var end   = temp[1].split('/').join('-');
   window.location.href='<?= base_url() ?>index.php/crm_b2c/index/'+start+'/'+end+'/'+$("#produk").val();
}

function age()
{
    Highcharts.chart('age-chart', {
        chart: {
            type: 'bar'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: [
                '< 10',
                '10-20',
                '21-30',
                '31-50',
                '>50'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        tooltip: {
            pointFormat: '{point.y:.0f}'
        },
        plotOptions: {
            bar: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'a',
            data: [<?=$age['0']?>,<?=$age['1']?>,<?=$age['2']?>, <?=$age['3']?>, <?=$age['4']?>],
            showInLegend: false,
        }]
    }); 
}

function gender()
{
    Highcharts.chart('gender-chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: [
                'Laki-laki',
                'Perempuan'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            /*title: {
                text: 'Rainfall (mm)'
            }*/
        },
        tooltip: {
            pointFormat: '{point.y:.0f}'
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'a',
            data: [<?=$gender['L']?>, <?=$gender['P']?>],
            showInLegend: false,
        }]
    });
}
</script>