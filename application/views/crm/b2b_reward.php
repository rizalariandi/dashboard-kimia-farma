<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    th {
        color: #333;
    }

    .dt-buttons {
        float: right;
        margin-right: 20px;
    }

    .dataTables_filter {
        margin-bottom: 10px;
    }

    .mrp-icon {
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right: 0px;
    }

    .mrp-monthdisplay {
        display: inline-block !important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor: pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth,
    .mrp-upperMonth {
        color: #40667A;
        font-weight: bold;
        font-size: 11px;
        text-transform: uppercase;
    }

    .mrp-to {
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar {
        display: inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child {
        border-right: none;
    }

    .mpr-month {
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5 {
        width: 100%;
        text-align: center;
        font-weight: bold;
        font-size: 18px
    }

    .mpr-selected {
        background: rgba(64, 102, 122, 0.75);
        ;
        color: #fff;
    }

    .mpr-month:hover {
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor: pointer;
    }

    .mpr-selected.mpr-month:hover {
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info {
        background-color: #40667A;
        border-color: #406670;
        width: 100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset {
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown,
    .mpr-yearup {
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown {
        float: left;
    }

    .mpr-yearup {
        float: right;
    }

    .mpr-yeardown:hover,
    .mpr-yearup:hover {
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first {
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last {
        background-color: #40667A;
    }

    .popover {
        max-width: 1920px !important;
    }

    .error {
        color: #ac2925;
    }

    table.dataTable,
    table.dataTable th,
    table.dataTable td {
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        box-sizing: content-box;
    }
</style>

<div class="panel panel-default">
    <div class="panel-body">
        <form id="form_upload" method="post" enctype="multipart/form-data">
            <div class="col-sm-3" style="margin-bottom:30px;">
                <input class="form-control input-sm" type="file" name="csv_file">
            </div>
            <div class="col-sm-190"><button class="btn btn-sm btn-success" id="submit_form" type="submit"><i class="fa  
fa-file-excel-o"></i>&nbsp;Upload Data </button>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?= base_url() ?>file/template_reward.xlsx">Download template</a>
                <a href="<?= site_url('crm_b2b_reward/create') ?>" class="btn btn-primary btn-sm pull-right">Add One</a>
            </div>
        </form>
        <table class="table table-bordered table-striped table-hover" id="table">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Customer Code</th>
                    <th>Nama Customer</th>
                    <th>Material</th>
                    <th>Nama Produk</th>
                    <th>Level</th>
                    <th>Area</th>
                    <th>Reward</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="frmbox_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form B2b Reward</h4>
            </div>
            <form id="frm_edit" class="form-horizontal">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="box-body" style="padding-bottom:0px">
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Tanggal</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" id="cbr_date" name="inp[cbr_date]" readonly required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Customer Code</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" id="cbr_customer_code" name="inp[cbr_customer_code]" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Nama Customer</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" id="cbr_customer_name" name="inp[cbr_customer_name]" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Material</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" id="cbr_material" name="inp[cbr_material]" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Nama Produk</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" name="inp[cbr_product_name]" id="cbr_product_name" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Level</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" name="inp[cbr_level]" id="cbr_level" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Area</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" name="inp[cbr_area]" id="cbr_area" required>
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="pus_name" class="col-sm-3 control-label">Reward</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control input-sm" name="inp[cbr_reward]" id="cbr_reward" required>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Batal
                    </button>
                    <button type="button" class="btn btn-success" id="act-update" onclick="edits('update_inp')">
                        Simpan Perubahan
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="frmbox" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>
                    <h4 class="modal-title">Log:</h4>
                </strong>
            </div>
            <div class="modal-body" style="height: 300px;overflow-y: auto;" id="log-info"></div>
        </div>
    </div>
</div>

<!-- Theme JS files -->
<script type="text/javascript" src="<?= base_url() ?>asset/js/download/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>asset/js/validate.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    var tb = '#table';
    var baseurl = 'index.php/crm_b2b_reward';
    var MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    
    $(document).ready(function() {        
        $(tb).dataTable({
            'ajax': {
                'url': '<?= base_url() ?>index.php/crm_b2b_reward/json',
                'data': function(data) {
                    data.periode = $('#periode').val();
                    data.lini = $('#lini').val();
                    data.area = $('#area').val();
                    data.channel = $('#channel').val();;
                    data.layanan = $('#layanan').val();
                },
                'method': 'post'
            },
            'order': [
                [0, 'desc']
            ],
            'columnDefs': [],
            "drawCallback": function(settings) {},
            'scrollX': true,
            autoWidth: false,
            dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': '&rarr;',
                    'previous': '&larr;'
                }
            },
            "lengthMenu": [
                [10, 20, 30, 50, 100, 150, -1],
                [10, 20, 30, 50, 100, 150, "All"]
            ],
            "pageLength": 10, // default records per page
            "autoWidth": false, // disable fixed width and enable fluid table
            "processing": true, // enable/disable display message box on record load
            "serverSide": true, // enable/disable server side ajax loading,
            initComplete: function() {

            }
        });


        $('.dataTables_filter input[type=search]').attr('placeholder', 'Pencarian');

        $('.dt-buttons').html('');

        $('#cbr_date').datepicker({
            format: "yyyy-mm-dd"
        });

        $('#frm').validate();
        $('#frm_edit').validate();

        /*import*/
        $("form#form_upload").submit(function(e) {
            e.preventDefault();

            var filename = $('#csv_file').val();
            var any_file = filename != '';

            var formData = new FormData($(this)[0]);
            $.ajax({
                dataType: "json",
                type: "POST",
                async: true,
                cache: false,
                processData: false,
                contentType: false,
                data: formData,
                url: '<?= base_url() ?>index.php/crm_b2b_reward/imports',
                success: function(response) {
                    if (response.status == 'ok;') {
                        $('#log-info').html(response.msg);
                        $('#form_upload')[0].reset();
                        $('#frmbox').modal({
                            keyboard: false,
                            backdrop: 'static'
                        });
                        _reload();
                    } else {
                        // alert(response.msg)
                        swal({
                            icon: 'error',
                            title: 'Terjadi Kesalahan!!',
                            text: response.msg
                        });
                    };
                },
                error: function(xhr, status, error) {
                    swal({
                        icon: 'error',
                        title: 'Terjadi Kesalahan!!',
                        text: "System Error: Periksa kembali dokumen anda, pastikan dapat dibuka dengan aplikasi ms excel dengan baik.\n\n" + xhr.responseText
                    });
                    // alert("System Error: Periksa kembali dokumen anda, pastikan dapat dibuka dengan aplikasi ms excel dengan baik.\n\n" + xhr.responseText);
                }
            });

        });
    });



    function add() {
        _reset();
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({
            keyboard: false,
            backdrop: 'static'
        });
    }

    function edit(id) {
        $.ajax({
            url: '<?= base_url() ?>index.php/crm_b2b_reward/edit_inp',
            global: false,
            async: true,
            dataType: 'json',
            type: 'post',
            data: ({
                id: id
            }),
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#id').val(e.cbr_id);
                $.each(e, function(key, value) {
                    if (key != 'cbr_date')
                        $('#' + key).val(value);
                });
                $('#cbr_date').datepicker('setDate', new Date(e.cbr_date));
                $('#frmbox_edit').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
            },
            error: function(xhr, status, error) {
                // alert('<?= $this->config->item('alert_error') ?>');
                swal({
                    icon: 'error',
                    title: 'Terjadi Kesalahan!!',
                    text: "System Error: Edit Data Gagal! \nError code: " + xhr.status + "\nMessage: " + xhr.statusText
                });
            },
            beforeSend: function() {
                $('#loading-img').show();
            },
            complete: function() {
                $('#loading-img').hide();
            }
        });
    }

    function save(url) {
        if ($("#frm").valid()) {
            $.ajax({
                url: '<?= base_url() ?>index.php/crm_b2b_reward/' + url,
                global: false,
                async: true,
                type: 'post',
                dataType: 'json',
                data: $('#frm').serialize(),
                success: function(e) {
                    if (e.status == 'ok;') {
                        _reload();
                        $("#frmbox").modal('hide');
                    } else alert(e.text);
                },
                error: function(xhr, status, error) {
                    // alert('<?= $this->config->item('alert_error') ?>');
                    swal({
                        icon: 'error',
                        title: 'Terjadi Kesalahan!!',
                        text: "System Error: Simpan Data Gagal! \nError code: " + xhr.status + "\nMessage: " + xhr.statusText
                    });
                },
                beforeSend: function() {
                    $('#loading-img').show();
                },
                complete: function() {
                    $('#loading-img').hide();
                }
            });
        }
    }

    function edits(url) {
        if ($("#frm_edit").valid()) {
            $.ajax({
                url: '<?= base_url() ?>index.php/crm_b2b_reward/' + url,
                global: false,
                async: true,
                type: 'post',
                dataType: 'json',
                data: $('#frm_edit').serialize(),
                success: function(e) {
                    if (e.status == 'ok;') {
                        _reload();
                        $("#frmbox_edit").modal('hide');
                    } else{
                        swal({
                            icon: 'error',
                            title: 'Terjadi Kesalahan!!',
                            text: e.text
                        });
                    } 
                    // alert(e.text);
                },
                error: function(xhr, status, error) {
                    // alert('<?= $this->config->item('alert_error') ?>');
                    swal({
                        icon: 'error',
                        title: 'Terjadi Kesalahan!!',
                        text: "System Error: Edit Data Gagal! \nError code: " + xhr.status + "\nMessage: " + xhr.statusText
                    });
                },
                beforeSend: function() {
                    $('#loading-img').show();
                },
                complete: function() {
                    $('#loading-img').hide();
                }
            });
        }
    }

    function del(id, date, name) {
        swal({
            icon: "warning",
            title: "Apakah anda yakin akan menghapus data tersebut ?",
            text: "Data Tanggal: " + date + "\nNama: " + name,
            buttons: ['Batal','Hapus'],
            dangerMode: true
        }).then(function(val){
            if(val){
                $.ajax({
                    url: '<?= base_url() ?>index.php/crm_b2b_reward/delete_inp',
                    global: false,
                    async: true,
                    type: 'post',
                    dataType: 'json',
                    data: ({
                        id: id
                    }),
                    success: function(e) {
                        if (e.status == 'ok;') {
                            _reload();
                            swal({
                                icon: 'success',
                                title: 'Hapus data berhasil!!',
                                text: "Data terhapus: \nData Tanggal: " + date + "\nNama: " + name
                            });
                        } else{
                            swal({
                                icon: 'error',
                                title: 'Terjadi Kesalahan!!',
                                text: e.text
                            });
                        } 
                        // alert(e.text);
                    },
                    error: function(xhr, status, error) {
                        // alert('<?= $this->config->item('alert_error') ?>');
                        swal({
                            icon: 'error',
                            title: 'Terjadi Kesalahan!!',
                            text: "System Error: Hapus Data Gagal! \nError code: " + xhr.status + "\nMessage: " + xhr.statusText
                        });
                    },
                    beforeSend: function() {
                        $('#loading-img').show();
                    },
                    complete: function() {
                        $('#loading-img').hide();
                    }
                });
            } else{
                swal({
                    icon: 'info',
                    title: 'Hapus data tidak dilakukan!!'
                });
            }
        });

        // =============================== initial code ==================================================
        // if (confirm('Data: ' + date + ' ' + name + '\nApakah anda yakin akan menghapus data tersebut ?')) {
            // $.ajax({
            //     url: '<?= base_url() ?>index.php/crm_b2b_reward/delete_inp',
            //     global: false,
            //     async: true,
            //     type: 'post',
            //     dataType: 'json',
            //     data: ({
            //         id: id
            //     }),
            //     success: function(e) {
            //         if (e.status == 'ok;') {
            //             _reload();
            //         } else{
            //             swal({
            //                 icon: 'error',
            //                 title: 'Terjadi Kesalahan!!',
            //                 text: e.text
            //             });
            //         } 
            //         // alert(e.text);
            //     },
            //     error: function(xhr, status, error) {
            //         // alert('<?= $this->config->item('alert_error') ?>');
            //         swal({
            //             icon: 'error',
            //             title: 'Terjadi Kesalahan!!',
            //             text: "System Error: Hapus Data Gagal! \nError code: " + xhr.status + "\nMessage: " + xhr.statusText
            //         });
            //     },
            //     beforeSend: function() {
            //         $('#loading-img').show();
            //     },
            //     complete: function() {
            //         $('#loading-img').hide();
            //     }
            // });
        // }
        // ==================================================================================
    }

    function _reset() {
        $("label.error").hide();
        $(".error").removeClass("error");
        $('#frm_edit')[0].reset();
    }

    function _reload() {
        $(tb).dataTable().fnDraw();
    }
</script>