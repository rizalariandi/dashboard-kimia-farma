<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>

<div class="panel panel-deafult">
    <form class="form-horizontal" id="form_reward" method="post">
        <div class="panel-body">
            <div class="form-group">
                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="tanggal" placeholder="Tanggal" name="tanggal">
                </div>
            </div>
            <div class="form-group">
                <label for="customer_code" class="col-sm-2 control-label">Customer Code</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="customer_code" name="customer_code" placeholder="Customer Code">
                </div>
            </div>

            <div class="form-group">
                <label for="nama_customer" class="col-sm-2 control-label">Nama Customer</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_customer" name="nama_customer" placeholder="Nama Customer">
                </div>
            </div>

            <div class="form-group">
                <label for="material" class="col-sm-2 control-label">Material</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="material" name="material" placeholder="Material">
                </div>
            </div>

            <div class="form-group">
                <label for="nama_produk" class="col-sm-2 control-label">Nama Produk</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Nama Produk">
                </div>
            </div>

            <div class="form-group">
                <label for="level" class="col-sm-2 control-label">Level</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="level" name="level" placeholder="Level">
                </div>
            </div>

            <div class="form-group">
                <label for="area" class="col-sm-2 control-label">Area</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="area" name="area" placeholder="Area">
                </div>
            </div>

            <div class="form-group">
                <label for="reward" class="col-sm-2 control-label">Reward</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="reward" name="reward" placeholder="Reward">
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary" onclick="add_reward()">Save</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function add_reward(){
        $.ajax({
            url:'<?= base_url() ?>index.php/crm_b2b_reward/insert_reward',
            type:'post',
            dataType:'json',
            data: $('#form_reward').serialize(),
            success : function(response) {
                if(response.status == 'success')
                {
                    swal({
                        icon: 'success',
                        title: 'Berhasil!!',
                        text: response.msg
                    });
                    window.location.href = '<?= base_url() ?>index.php/crm_b2b_reward';
                    
                }else{
                    swal({
                        icon: 'error',
                        title: 'Terjadi Kesalahan!!',
                        text: response.msg
                    });
                }
                // else alert(e.text);
            },
            error : function() {
                swal({
                    icon: 'error',
                    title: 'Terjadi Kesalahan!!'
                });
            },
            beforeSend : function() {
            },
            complete : function() {
            }
        });
    }
</script>
