<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <div id="sla-data-range" class="mrp-container nav navbar-nav">
                    <div class="mrp-monthdisplay">
                        <span class="mrp-lowerMonth"><?= $start3 ?></span>
                        <span class="mrp-to"> to </span>
                        <span class="mrp-upperMonth"><?= $end3 ?></span>
                    </div>
                    <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                    <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
                </div>
            </div>
            <div class="col-md-2">
                <select name="area" id="area" class="form-control">
                    <option value="all">Semua Area</option>
                    <?php foreach ($areaOption as $k => $p) { ?>
                        <option value="<?= $k ?>" <?= ($area == $k ? 'selected' : '') ?>><?= $p ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Customer Segmentation 
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="tables">
                    <thead>
                        <tr>
                            <th width="20%" style="color:#797979 !important">Dokter</th>
                            <th width="20%" style="color:#797979 !important">Nama Rumah Sakit</th>
                            <th width="20%" style="color:#797979 !important">Spesialisasi</th>
                            <th width="20%" style="color:#797979 !important">Alamat</th>
                            <th width="10%" style="color:#797979 !important">Periode</th>
                            <th width="10%" style="color:#797979 !important">Value</th>
                            <th width="10%" style="color:#797979 !important">Freq</th>
                            <th width="5%" style="color:#797979 !important">Level</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    var table = $('#tables').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "<?= site_url('crm_b2c_dokter/json2') ?>",
            type: "POST"
        },
        "lengthMenu": [
            [100, 300, 500, -1],
            [100, 300, 500, "All"]
        ],
        "columns": [{
                name: 'doctor',
                searchable: true,
                orderable: true
            },
            {
                name: 'bcd_hospital_name',
                searchable: true,
                orderable: true
            },
            {
                name: 'bcd_spesialisasi',
                searchable: true,
                orderable: true
            },
            {
                name: 'bcd_address',
                searchable: true,
                orderable: true
            },
            {
                name: 'month',
                searchable: true,
                orderable: true
            },
            {
                name: 'rev',
                searchable: true,
                orderable: true
            },
            {
                name: 'freq',
                searchable: true,
                orderable: true
            }, {
                name: 'level',
                searchable: true,
                orderable: true
            },
        ],
    });
    $(".dataTables_filter input")
        .off()
        .on('keyup change', function(e) {
            if (e.keyCode == 13 || this.value == "") {
                table.search(this.value)
                    .draw();
            }
        });
</script>