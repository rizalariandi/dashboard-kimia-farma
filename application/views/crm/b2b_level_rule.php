<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    th {
        color:#333;
    }
    .dt-buttons {
        float:right;
        margin-right: 20px;
    }
    .dataTables_filter {
        margin-bottom: 10px;
    }
    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }

    .error{
        color : #ac2925;
    }
</style>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered table-striped table-hover" id="table">
            <thead>
            <tr>
                <th>Bulan</th>
                <th>Tahun</th>
                <th>Bisnis Model</th>
                <th>Lini</th>
                <th>Layanan</th> 
                <th>Value</th>
                <th>Freq</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<?php
$start3 = date('M Y');
$end3 = date('M Y', strtotime("+1 month"));
$start = date('m-Y');
$end = date('m-Y', strtotime("+1 month"));
?>

<div class="modal fade" id="frmbox" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form B2b Level Rule</h4>
            </div>
            <form id="frm" class="form-horizontal">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="box-body" style="padding-bottom:0px">
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Tahun</label>
                        <div class="col-sm-3">
                            <select name="year" id="year" required>
                                <option value="">Pilih Tahun</option>
                                <?php foreach($year as $row){ ?>
                                    <option value="<?=$row?>"><?=$row?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <label for="pus_name" class="col-sm-3 control-label">Bisnis Model</label>
                        <div class="col-sm-3">
                            <select name="bisnis_model" id="bisnis_model" required>
                                <option value="">Pilih Bisnis Model</option> 
                                <option value="b2b">B2B</option>
                                <option value="b2c_dokter">B2C Dokter</option>
                                <option value="b2c_end_user">B2C End User</option>
                            </select>
                        </div>
                    </div> 
                    <table class="table table-bordered table-striped table-hover" id="tables">
                        <thead>
                        <tr>
                            <th>Lini</th>
                            <th>Layanan</th> 
                            <th>Value</th>
                            <th>Freq</th> 
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 0; 
                                foreach($lini as $keyln => $ln){
                                    foreach($layanan as $keyly => $ly){ 
                                        echo '<tr>
                                            <input type="hidden" name="lini['.$i.']" value="'.$keyln.'"> 
                                            <input type="hidden" name="layanan['.$i.']" value="'.$keyly.'">
                                            <td>'.$ln.'</td> 
                                            <td>'.$ly.'</td>
                                            <td><input type="text" class="form-control input-sm" name="value['.$i.']" id="value'.$i.'" required></td>
                                            <td><input type="text" class="form-control input-sm" name="freq['.$i.']" id="freq'.$i.'" required></td>     
                                            </tr>';
                                        $i++;
                                    } 
                                }
                            ?>
                        </tbody>
                    </table>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Batal
                    </button>
                    <button type="button" class="btn btn-success" id="act-save" onclick="save('insert_inp')">
                        Simpan
                    </button>
                    <button type="button" class="btn btn-success" id="act-update" onclick="save('update_inp')">
                        Simpan Perubahan
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="frmbox_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-navicon"></i> &nbsp;Form B2b Level Rule</h4>
            </div>
            <form id="frm_edit" class="form-horizontal">
                <input type="hidden" name="month" id="rule_month">
                <input type="hidden" name="year" id="rule_year">
                <input type="hidden" name="lini" id="rule_lini">
                <input type="hidden" name="layanan" id="rule_layanan">
                <input type="hidden" name="model" id="rule_model">
                <div class="modal-body">
                    <div class="box-body" style="padding-bottom:0px"> 
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Bulan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="month" readonly>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Tahun</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="years" readonly>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Bisnis Model</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="models" readonly>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Lini</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="lini" readonly>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Layanan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="layanan" readonly>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Value</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="inp[rule_value]" id="rule_value" required>
                        </div>
                    </div>
                    <div class="form-group form-group-sm">
                        <label for="pus_name" class="col-sm-3 control-label">Freq</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" name="inp[rule_freq]" id="rule_freq" required>
                        </div>
                    </div>
                    </div><!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        Batal
                    </button> 
                    <button type="button" class="btn btn-success" id="act-update" onclick="edits('update_inp')">
                        Simpan Perubahan
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Theme JS files -->
<script type="text/javascript" src="<?=base_url()?>asset/js/download/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>asset/js/validate.min.js"></script>

<script type="text/javascript">
    var tb 		= '#table';
    var baseurl = 'index.php/crm_b2b_level_rule';
    var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

    $(document).ready(function() {
        $(tb).dataTable({
            'ajax': {
                'url':'<?= base_url() ?>index.php/crm_b2b_level_rule/json',
                'data' : function(data) {
                    data.periode	= $('#periode').val();
                    data.lini		= $('#lini').val();
                    data.area		= $('#area').val();
                    data.channel	= $('#channel').val();;
                    data.layanan	= $('#layanan').val();
                },
                'method' : 'post'
            },
            'order':[
                [0, 'desc']
            ],
            'columnDefs': [
            ],
            "drawCallback": function( settings ) {
            },
            'scrollX': true,
            autoWidth: false,
            dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            "lengthMenu": [
                [10, 20, 30, 50, 100, 150, -1],
                [10, 20, 30, 50, 100, 150, "All"]
            ],
            "pageLength": 30, // default records per page
            "autoWidth": false, // disable fixed width and enable fluid table
            "processing": true, // enable/disable display message box on record load
            "serverSide": true, // enable/disable server side ajax loading,
            initComplete: function() {

            }
        });


        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');

        $('.dt-buttons').html('<button class="btn btn-sm btn-primary" onclick="add()"><i class="icon-add"></i> &nbsp;Tambah Data</button>');
 


        $('#frm').validate();
        $('#frm_edit').validate();

    });

    function add()
    {
        _reset();
        $('#act-save').show();
        $('#act-update').hide();
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function edit(month,year,lini,layanan,models)
    {
        $.ajax({
            url:'<?= base_url() ?>index.php/crm_b2b_level_rule/edit_inp',
            global:false,
            async:true,
            dataType:'json',
            type:'post',
            data: ({ month : month,year : year,lini : lini,layanan : layanan,models : models }),
            success: function(e) {
                _reset();
                $('#act-save').hide();
                $('#act-update').show();
                $('#month').val(e.rule_month);
                $('#years').val(e.rule_year);
                $('#lini').val(e.rule_lini);
                $('#layanan').val(e.rule_layanan);
                $('#models').val(e.rule_model);
                $.each(e, function(key, value) {
                    $('#'+key).val(value);
                }); 
                $('#frmbox_edit').modal({keyboard: false, backdrop: 'static'});
            },
            error : function() {
                alert('<?= $this->config->item('alert_error') ?>');
            },
            beforeSend : function() {
                $('#loading-img').show();
            },
            complete : function() {
                $('#loading-img').hide();
            }
        });
    }

    function save(url)
    {
        if($("#frm").valid())
	    {   
            $.ajax({
                url:'<?= base_url() ?>index.php/crm_b2b_level_rule/'+url,
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $('#frm').serialize(),
                success : function(e) {
                    if(e.status == 'ok;')
                    {
                        _reload();
                        $("#frmbox").modal('hide');
                    }
                    else alert(e.text);
                },
                error : function() {
                    alert('<?= $this->config->item('alert_error') ?>');
                },
                beforeSend : function() {
                    $('#loading-img').show();
                },
                complete : function() {
                    $('#loading-img').hide();
                }
            });
        }
    }

    function edits(url)
    {
        if($("#frm_edit").valid())
	    {   
            $.ajax({
                url:'<?= base_url() ?>index.php/crm_b2b_level_rule/'+url,
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $('#frm_edit').serialize(),
                success : function(e) {
                    if(e.status == 'ok;')
                    {
                        _reload();
                        $("#frmbox_edit").modal('hide');
                    }
                    else alert(e.text);
                },
                error : function() {
                    alert('<?= $this->config->item('alert_error') ?>');
                },
                beforeSend : function() {
                    $('#loading-img').show();
                },
                complete : function() {
                    $('#loading-img').hide();
                }
            });
        }
    }

    function del(id, txt)
    {
        if(confirm('Data: '+txt+'\nApakah anda yakin akan menghapus data tersebut ?')) {
            $.ajax({
                url:'<?= base_url() ?>index.php/crm//delete_inp',
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: ({id : id }),
                success: function(e) {
                    if(e.status == 'ok;')
                    {
                        _reload();
                    }
                    else alert(e.text);
                },
                error : function() {
                    alert('<?= $this->config->item('alert_error') ?>');
                },
                beforeSend : function() {
                    $('#loading-img').show();
                },
                complete : function() {
                    $('#loading-img').hide();
                }
            });
        }
    }

    function _reset()
    {
        $("label.error").hide();
        $(".error").removeClass("error");
        $('#frm')[0].reset();
    }

    function _reload()
    {
        $(tb).dataTable().fnDraw();
    }

    function rangemonth(start, end)
    {
        var s = start.split('-');
        var e = end.split('-');

        startMonth = s[0];
        startYear = s[1];
        endMonth = e[0];
        endYear = e[1];

        fiscalMonth = 7;
        if(startMonth < 10)
            startDate = parseInt("" + startYear + '0' + startMonth + "");
        else
            startDate = parseInt("" + startYear  + startMonth + "");
        if(endMonth < 10)
            endDate = parseInt("" + endYear + '0' + endMonth + "");
        else
            endDate = parseInt("" + endYear + endMonth + "");

        content = '<div class="row mpr-calendarholder">';
        calendarCount = endYear - startYear;
        if(calendarCount == 0)
            calendarCount++;
        var d = new Date();
        for(y = 0; y < 2; y++){
            content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
                + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
            for(m=0; m < 12; m++){
                var monthval;
                if((m+1) < 10)
                    monthval = "0" + (m+1);
                else
                    monthval = "" + (m+1);
                content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
            }
            content += '</div></div></div></div>';
        }
        content += '</div>';

        $(document).on('click','.mpr-month',function(e){
            e.stopPropagation();
            $month = $(this);
            var monthnum = $month.data('month');
            var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
            if($month.parents('#mpr-calendar-1').length > 0){
                //Start Date
                startDate = parseInt("" + year + monthnum);
                if(startDate > endDate){

                    if(year != parseInt(endDate/100))
                        $('.mpr-calendar:last h5 span').html(year);
                    endDate = startDate;
                }
            }else{
                //End Date
                endDate = parseInt("" + year + monthnum);
                if(startDate > endDate){
                    if(year != parseInt(startDate/100))
                        $('.mpr-calendar:first h5 span').html(year);
                    startDate = endDate;
                }
            }

            paintMonths();
        });


        $(document).on('click','.mpr-yearup',function(e){
            e.stopPropagation();
            var year = parseInt($(this).prev().html());
            year++;
            $(this).prev().html(""+year);
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        $(document).on('click','.mpr-yeardown',function(e){
            e.stopPropagation();
            var year = parseInt($(this).next().html());
            year--;
            $(this).next().html(""+year);
            //paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        $(document).on('click','.mpr-ytd', function(e){
            e.stopPropagation();
            var d = new Date();
            startDate = parseInt(d.getFullYear() + "01");
            var month = d.getMonth() + 1;
            if(month < 9)
                month = "0" + month;
            endDate = parseInt("" + d.getFullYear() + month);
            $('.mpr-calendar').each(function(){
                var $cal = $(this);
                var year = $('h5 span',$cal).html(d.getFullYear());
            });
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        $(document).on('click','.mpr-prev-year', function(e){
            e.stopPropagation();
            var d = new Date();
            var year = d.getFullYear()-1;
            startDate = parseInt(year + "01");
            endDate = parseInt(year + "12");
            $('.mpr-calendar').each(function(){
                var $cal = $(this);
                $('h5 span',$cal).html(year);
            });
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        $(document).on('click','.mpr-fiscal-ytd', function(e){
            e.stopPropagation();
            var d = new Date();
            var year;
            if((d.getMonth()+1) < fiscalMonth)
                year = d.getFullYear() - 1;
            else
                year = d.getFullYear();
            if(fiscalMonth < 10)
                fm = "0" + fiscalMonth;
            else
                fm = fiscalMonth;
            if(d.getMonth()+1 < 10)
                cm = "0" + (d.getMonth()+1);
            else
                cm = (d.getMonth()+1);
            startDate = parseInt("" + year + fm);
            endDate = parseInt("" + d.getFullYear() + cm);
            $('.mpr-calendar').each(function(i){
                var $cal = $(this);
                if(i == 0)
                    $('h5 span',$cal).html(year);
                else
                    $('h5 span',$cal).html(d.getFullYear());
            });
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        $(document).on('click','.mpr-prev-fiscal', function(){
            var d = new Date();
            var year;
            if((d.getMonth()+1) < fiscalMonth)
                year = d.getFullYear() - 2;
            else
                year = d.getFullYear() - 1;
            if(fiscalMonth < 10)
                fm = "0" + fiscalMonth;
            else
                fm = fiscalMonth;
            if(fiscalMonth -1 < 10)
                efm = "0" + (fiscalMonth-1);
            else
                efm = (fiscalMonth-1);
            startDate = parseInt("" + year + fm);
            endDate = parseInt("" + (d.getFullYear() - 1) + efm);
            $('.mpr-calendar').each(function(i){
                var $cal = $(this);
                if(i == 0)
                    $('h5 span',$cal).html(year);
                else
                    $('h5 span',$cal).html(d.getFullYear()-1);
            });
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
                paintMonths();
                $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
            });
        });

        var mprVisible = false;
        var mprpopover = $('.mrp-container').popover({
            container: "body",
            placement: "bottom",
            html: true,
            content: content
        }).on('show.bs.popover', function () {
            $('.popover').remove();
            var waiter = setInterval(function(){
                if($('.popover').length > 0){
                    clearInterval(waiter);
                    setViewToCurrentYears();
                    paintMonths();
                }
            },50);
        }).on('shown.bs.popover', function(){
            mprVisible = true;
        }).on('hidden.bs.popover', function(){
            mprVisible = false;
        });

        $(document).on('click','.mpr-calendarholder',function(e){
            e.preventDefault();
            e.stopPropagation();
        });
        $(document).on("click",".mrp-container",function(e){
            if(mprVisible){
                e.preventDefault();
                e.stopPropagation();
                mprVisible = false;
            }
        });
        $(document).on("click",function(e){
            if(mprVisible){
                $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                    $('.mpr-calendarholder').parents('.popover').remove();
                    $('.mrp-container').trigger('click');
                });
                mprVisible = false;
            }
        });
    }
    function setViewToCurrentYears(){
        var startyear = parseInt(startDate / 100);
        var endyear = parseInt(endDate / 100);
        $('.mpr-calendar h5 span').eq(0).html(startyear);
        $('.mpr-calendar h5 span').eq(1).html(endyear);
    }

    function paintMonths(){
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html();
            $('.mpr-month',$cal).each(function(i){
                if((i+1) > 9)
                    cDate = parseInt("" + year + (i+1));
                else
                    cDate = parseInt("" + year+ '0' + (i+1));
                if(cDate >= startDate && cDate <= endDate){
                    $(this).addClass('mpr-selected');
                }else{
                    $(this).removeClass('mpr-selected');
                }
            });
        });
        $('.mpr-calendar .mpr-month').css("background","");
        //Write Text
        var startyear = parseInt(startDate / 100);
        var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
        var endyear = parseInt(endDate / 100);
        var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
        $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
        $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
        $('#mrp-lowerDate').val(startmonth+'-'+startyear);
        $('#mrp-upperDate').val(endmonth+'-'+endyear);
        if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
            $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
        if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
            $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
    }

    function safeRound(val){
        return Math.round(((val)+ 0.00001) * 100) / 100;
    }
</script>