<link rel="stylesheet" href="<?= base_url()?>assets/jvectormap/jquery-jvectormap-2.0.2.css" type="text/css" media="screen"/>
    <!-- <script src="<?= base_url()?>assets/jvectormap/jquery.js"></script> -->
    <script src="<?= base_url()?>assets/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?= base_url()?>assets/jvectormap/indonesia-adm1.js"></script>
<style>
.row{
    text-align: center;
}
#box2{
}
#box2 ul{
    list-style: none;
}
#box-right{
    height: 450px;
    background-color: white;
}
#box-right ol{
    text-align: left;
    color: black;
}
#box-right h3{
    color: black;
}
</style>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
<form action="" method="post">  
        Kategori Customer : <select id="entitas" name="entitas"><?php $produk = ['Apotek', 'Apotek2'];
                                        $customer=['Apotek', 'customer1'];
                                        foreach ($produk as $p) {
                                            if ($p == $_SESSION['produk']) {
                                                echo "<option value='$p' selected>$p</option>";
                                            } else {
                                                echo "<option value='$p'>$p</option>";
                                            }
                                        } ?></select>
        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>


    <div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
        <div class="white-box">
            <div id="indonesia-map" style="width: 100%; height: 400px"></div>
                <script>
                    $(function(){
                    $('#indonesia-map').vectorMap({
                        map: 'indonesia-adm1_merc',
                        scaleColors: ['#C8EEFF', '#0071A4'],
                        normalizeFunction: 'polynomial',
                        hoverOpacity: 0.7,
                        hoverColor: false,
                        onMarkerClick: true,
                        markerStyle: {
                        initial: {
                            fill: 'red',
                            stroke: '#383f47'  
                        }
                        },
                        backgroundColor: '#383f47',
                        markers: [
                        {latLng: [-6.270849, 106.978180], name: 'Bekasi', style: {r: 7}},
                        {latLng: [0.802929, 127.343655], name: 'Ternate', style: {r: 7}},
                        {latLng: [3.585161, 98.672139], name: 'Medan', style: {r: 7}},
                        {latLng: [-7.969012, 112.629512], name: 'Malang', style: {r: 7}},
                        {latLng: [-6.220830, 106.845999], name: 'Jakarta', style: {r: 7}},
                        {latLng: [-6.178043, 106.652781], name: 'Tanggerang', style: {r: 7}},
                        {latLng: [-6.597219, 106.805412], name: 'Bogor', style: {r: 7}},
                        {latLng: [-6.916588, 107.626491], name: 'Bandung', style: {r: 7}},
                        {latLng: [-6.738408, 108.550494], name: 'Cirebon', style: {r: 7}},
                        {latLng: [-0.460423, 117.194758], name: 'Samarinda', style: {r: 7}},
                        {latLng: [1.480882, 124.850599], name: 'Manado', style: {r: 7}},
                        {latLng: [-8.659435, 115.227361], name: 'Denpasar', style: {r: 7}},
                        {latLng: [-1.252984, 116.849481], name: 'Balikpapan', style: {r: 7}},
                        {latLng: [-3.315736, 114.594374], name: 'Banjarmasin', style: {r: 7}},
                        {latLng: [-0.889700, 131.289309], name: 'Sorong', style: {r: 7}},
                        {latLng: [-6.115547, 106.158493], name: 'Serang', style: {r: 7}},
                        {latLng: [-7.803291, 110.373342], name: 'Yogyakarta', style: {r: 7}},
                        {latLng: [1.088128, 104.021483], name: 'Batam', style: {r: 7}},
                        {latLng: [-5.132955, 119.446093], name: 'Makasar', style: {r: 7}},
                        {latLng: [-2.961774, 104.760078], name: 'Palembang', style: {r: 7}},
                        {latLng: [-1.617014, 103.602660], name: 'Jambi', style: {r: 7}},
                        {latLng: [-2.919095, 139.955502], name: 'Jayapura', style: {r: 7}},
                        {latLng: [-8.586525, 116.102901], name: 'Mataram', style: {r: 7}},
                        {latLng: [-7.257438, 112.752319], name: 'Surabaya', style: {r: 7}},
                        {latLng: [-7.353258, 108.221246], name: 'Tasikmalaya', style: {r: 7}},
                        {latLng: [-7.439345, 112.694739], name: 'Sidoarjo', style: {r: 7}},
                        {latLng: [-2.213091, 113.910500], name: 'Palangkaraya', style: {r: 7}},
                        {latLng: [4.542658, 96.716320], name: 'Aceh', style: {r: 7}},
                        {latLng: [-0.900452, 100.385031], name: 'Padang', style: {r: 7}},
                        {latLng: [0.694093, 122.177432], name: 'Gorontalo', style: {r: 7}},
                        {latLng: [0.512117, 101.446055], name: 'Pekanbaru', style: {r: 7}},
                        {latLng: [-0.833437, 119.917816], name: 'Palu', style: {r: 7}},
                        {latLng: [-7.556301, 110.823824], name: 'Surakarta', style: {r: 7}},
                        {latLng: [-2.120540, 106.110956], name: 'Pangkal Pinang', style: {r: 7}},
                        {latLng: [-4.939874, 105.134725], name: 'Lampung', style: {r: 7}},
                        {latLng: [-3.648674, 128.158357], name: 'Ambon', style: {r: 7}},
                        {latLng: [-8.260031, 113.662417], name: 'Jember', style: {r: 7}},
                        {latLng: [2.966134, 99.059544], name: 'Pematang Siantar', style: {r: 7}},
                        {latLng: [-6.874550, 109.126246], name: 'Tegal', style: {r: 7}},
                        {latLng: [-0.030434, 109.331205], name: 'Pontianak', style: {r: 7}},
                        {latLng: [-10.168266, 123.609011], name: 'Kupang', style: {r: 7}},
                        {latLng: [-7.425648, 109.238430], name: 'Purwokerto', style: {r: 7}},
                        {latLng: [-3.391414, 102.124936], name: 'Bengkulu', style: {r: 7}},
                        {latLng: [-3.997056, 122.517720], name: 'Kendari', style: {r: 7}}
                        ],
                        onMarkerTipShow: function(e, el, code){
                        el.html('KFTD '+el.html());
                        },
                        onMarkerClick: function (e, code, name) {
                            name=[
                                ["<?=base_url()?>index.php/page/index7"], //code 27
                                ['Medan'], //code 2
                                ['Pematang Siantar'],
                                ['Padang'],
                                ['Pekanbaru'],
                                ['Batam'],
                                ['Jambi'],
                                ['Bengkulu'],
                                ['Palembang'],
                                ['Pangkal Pinang'],
                                ['Lampung'],
                                ['Serang'],
                                ['Tanggerang'],
                                ['Jakarta'],
                                ['Bekasi'],
                                ['Bogor'],
                                ['Bandung'],
                                ['Tasik Malaya'],
                                ['Cirebon'],
                                ['Tegal'],
                                ['Purwokerto'],
                                ['Yogyakarta'],
                                ['Surakarta'],
                                ['Surabaya'],
                                ['Sidoarjo'],
                                ['Malang'],
                                ['Jember'],
                                ['Denpasar'],
                                ['Mataram'],
                                ['Kupang'],
                                ['Pontianak'],
                                ['Palangkaraya'],
                                ['Banjarmasin'],
                                ['Balikpapan'],
                                ['Samarinda'],
                                ['Makasar'],
                                ['Palu'],
                                ['Gorontalo'],
                                ['Manado'],
                                ['Kendari'],
                                ['Ternate'],
                                ['Ambon'],
                                ['Sorong'],
                                ['Jayapura'],
                            ];
                            var name = name;
                        // alert(code);
                        var code = code;
                        if (code== 27){
                            res1=["<?=base_url()?>index.php/boosting/customer_aceh"]
                            $('#container-right').load(res1);
                        }else if(code == 2){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 37){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 28){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 30){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 17){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 20){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 42){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 19){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 34){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 15){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 5){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 4){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 0){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 6){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 7){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 24){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 8){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 38){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 41){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 16){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 32){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 23){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 25){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 3){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 36){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 11){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 22){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 40){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 39){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 26){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 13){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 12){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 9){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 18){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 31){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 29){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 10){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 43){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 1){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 35){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 14){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }else if(code == 21){
                            res1=["<?=base_url()?>index.php/boosting/customer_medan"]
                            $('#container-right').load(res1);
                        }
                        }

                    });
                    });
                </script>
        </div>
        </div>
       <div class="col-md-3">
       <div class="white-box" id="box-right">
            <div class="samping" id="container-right">
                <h3><b>Daftar Customer di Aceh</b></h3> 
                <ol>
                    <li>Apotek Siji = Diskon 50%</li>
                    <li>Apotek Fuji = Diskon 50%</li>
                    <li>Apotek Leti = Diskon 50%</li>
                    <li>Apotek Sugi = Diskon 50%</li>
                    <li>Apotek Polo = Diskon 50%</li>
                </ol>
            </div>
        </div>
        </div>
    </div>
    <div class="white-box">
            <div class="row">
                <div class="col-md-3 col-sm-3"></div>
                <div class="col-md-6 col-sm-6 col-xs-12" id="box2">
                <h4><b>REKOMENDASI</b></h4>
                    <ul style="list-style-type:none">
                    <li>
                    Program yang dianjurkan di Aceh adalah <b>Diskon 50%</b>
                    </li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3"></div>
            </div>
        </div>
</div