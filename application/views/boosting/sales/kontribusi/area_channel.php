<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<style>
    * {
        box-sizing: border-box;
    }

    .dataTables_wrapper .dataTables_length {
        float: left;
    }

    div.dataTables_wrapper div.dataTables_filter {
        float: left;
        left: 0;
        margin-left: 33%;
        margin-right: 15%;
        margin-bottom: 2px;
    }

    div.dataTables_wrapper div.dataTables_buttons {
        float: left;
    }

    table.dataTable thead tr {
        background-color: #F79868;
    }

    #box1 {

        background: #F79868;
        border-radius: 10px 10px 0px 0px;
    }

    #map-title {
        color: #fff;
        font-family: Roboto;
        font-style: normal;
        font-weight: bold;
        font-size: 25px;
        line-height: 20px;
        padding-top: 30px;
        padding-bottom: 30px;
        padding-left: 30px;
    }

    #flex {
        display: flex;
        margin-top: -5px;
        margin-left: 5px;
    }

    #selection {
        display: flex;
        padding-left: 0px;
    }

    .selectpicker {
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */

        display: flex;
        align-items: center;

        color: #4F4F4F;
    }

    .btn-default {
        background: #FFFFFF;
        border: 1px solid #08388F;
        box-sizing: border-box;
        border-radius: 5px;
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-size: 14px;
        line-height: 20px;
        /* or 143% */
        color: #4F4F4F;
    }

    .calendar-table thead,
    .calendar-table th,
    .calendar-table td {
        color: black !important;
    }
</style>


<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<!-- MAPS -->
<div class="white-box">
    <div id="selection" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px;">
        <form method="post" action="" id="filter-form">
            <input type="hidden" name="provinsi" id="provinsi" value="<?= $provinsi;?>" />
            <input  name="Date_" hidden value="<?= $data_range; ?>"/>
            From:<input type="text" name="range" value="<?= $data_range; ?>" class="btn btn-default" />
                <select id="lini" name="lini[]" class="lini selectpicker" multiple data-live-search="true" title="Lini">
                    <?php foreach ($list_lini as $ln) : ?>
                        <option value='<?= $ln->lini; ?>'><?= $ln->lini; ?></option>
                    <?php endforeach; ?>
                </select>

                <select id="layanan" name="layanan[]" class="layanan selectpicker" multiple data-live-search="true" title="Layanan">
                    <?php foreach ($list_layanan as $ly) : ?>
                        <option value='<?= $ly->layanan; ?>'><?= $ly->layanan; ?></option>
                    <?php endforeach; ?>
                </select>        

            
                <select id="area" name="area" class="area selectpicker">
                <?php $area = ['channel-area', 'area-channel'];
                foreach ($area as $l) {
                    if ($l == $areaf) {
                        echo "<option value='$l' selected>$l</option>";
                    } else {
                        echo "<option value='$l'>$l</option>";
                    }
                } ?></select>
           
                
                <button type="submit" id="btnSubmit" class="btn btn-success">Filter</button>
            </form>
        </div>
    <div id="box1">
        <h2 id="map-title">Distribusi Sales</h2>
        <div id="mymap" style="border-style: solid; border-width: 2px; border-color:#F79868"></div>
    </div>
</div>
<br>
<!-- #MAPS -->
<!-- TABLE -->
<div class="white-box" style="color:black;">
    <?php if ($provinsi == "") {
        $provinsis = "Seluruh Provinsi";
    } else {
        $provinsis = $provinsi;
    } ?>
    Provinsi: <?= $provinsis; ?>
    <?php if($provinsi != ""):?>
     <a onclick="resetprov()" style="color:blue;cursor:pointer">Reset Filter Provinsi</a>
    <?php endif;?>
    <br>
    <p> Legend :</p>
    <table>
       
        <tr>
            <td style="background-color:#55efc4; width:20px">&nbsp;</td>
            <td>&nbsp;n >= 5%</td>
        </tr>
        <tr>
            <td style="background-color:#F0E68C; width:20px">&nbsp;</td>
            <td>&nbsp;1% =< n > 5%</td>
        </tr>
        <tr>
            <td style="background-color:#FFFFFF; width:20px">&nbsp;</td>
            <td>&nbsp;n = 0%</td>
        </tr>
        <tr>
            <td style="background-color:#f74050; width:20px">&nbsp;</td>
            <td>&nbsp;n < 0%</td>
        </tr>
    </table>
    <br>
    <div class="table-responsive">
        <table id="mytable" class="display" width="100%">
            <thead>
                <tr>
                    <?php if ($areaf == "channel-area") : ?>
                        <th>Channel</th>
                    <?php else : ?>
                        <th>Area</th>
                    <?php endif; ?>
                <?php if ($provinsi != "" && $areaf == "channel-area") : ?>
                        <?php foreach ($kftd_brand_area as $kfs) : ?>
                            <th><?= $kfs->nama_kftd; ?></th>
                        <?php endforeach; ?>
                <?php else : ?>
                    <?php foreach ($kftd as $kf) : ?>
                        <?php if ($areaf == "channel-area") : ?>
                            <th><?= $kf->nama_kftd; ?></th>
                        <?php else : ?>
                            <th><?= $kf->channel; ?></th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

</div>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"> </script>

<script>

</script>
<script>
    function resetprov(){
        $("#provinsi").val("");
        document.getElementById("filter-form").submit();
    }

    $("#btnSubmit").click(function() {
        var prod = $("select[name='area']").val();
        if (prod == 'area-channel') {
            areaChannel();
        }
        if (prod == 'channel-area') {
            channelArea();
        }
    })

    $(function() {
        $('input[name="range"]').daterangepicker({
            opens: 'center',
            startDate: '<?= $startDate ?>',
            endDate: '<?= $endDate ?>',
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }

        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('Y-m-d') + ' to ' + end.format('Y-m-d'));
        
        });

        $('input[name="range"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));
        });
    });

    $(document).ready(function() {
        var provinsi = "";
        var t = $("#mytable").DataTable({
            initComplete: function() {
                var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode != 13) {
                                        api.search(this.value).draw();
                            }
                                   $("#keyword").val(this.value);
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollCollapse: true,
            processing: true,
            // pageLength: 5,
            //serverSide: true,
            ajax: {
                <?php if ($areaf == 'channel-area') : ?> "url": "boost_area_channel/json_channel_area",
                <?php else : ?> "url": "boost_area_channel/json_area_channel",
                <?php endif; ?> "type": "POST",
                "data": {
                    "range": "<?= $data_range; ?>",
                    "lini": "<?= implode(",", $data_lini); ?>",
                    "layanan": "<?= implode(",", $data_layanan); ?>",
                    "provinsi" : "<?= $provinsi;?>",
                    "length" : "all",
                    "start" : "0"
                }
            },
            columns: [
                {
                    <?php if ($areaf == 'channel-area') : {?> "data": "channel"
                    <?php }else : {?> "data": "area" <?php } ?>
                    <?php endif; ?>
                },
                <?php if ($provinsi != "" && $areaf == "channel-area") : ?>
                    <?php foreach ($kftd_brand_area as $kfs) : ?> 
                        { "data": "<?= $kfs->nama_kftd; ?>"},
                    <?php endforeach; ?>
                <?php else : ?>
                <?php foreach ($kftd as $kfs) : ?> {
                        <?php if ($areaf == 'channel-area') : ?>
                                "data": "<?= $kfs->nama_kftd; ?>"
                        <?php else : ?>
                                "data": "<?= $kfs->channel; ?>"
                        <?php endif; ?>
                    },
                <?php endforeach; ?>
                <?php endif; ?>
            ],
            order: [
                [0, 'desc']
            ],
            createdRow: function(row, data, dataIndex) {
                var status = false;
                <?php foreach ($kftd as $k) : ?>
                    <?php if ($areaf == 'channel-area') : ?>
                        if (data['<?= $k->nama_kftd; ?>'] != 0) {
                            status = true;
                        }
                    <?php else : ?>
                        if (data["<?= $k->channel; ?>"] != 0) {
                            status = true;
                        }
                    <?php endif; ?>
                <?php endforeach; ?>
                if (!status) {
                    t.rows($(row)).remove();
                }
            },
            rowCallback: function(row, data, iDisplayIndex) {         
                var i = 1;
                <?php foreach ($kftd as $k) : ?>
                    <?php if ($areaf == 'channel-area') : ?>
                        perc = data['<?= $k->nama_kftd; ?>'] ;
                        console.log(parseFloat(perc));
                    <?php else : ?>
                        perc = data['<?= $k->channel; ?>'] ;
                        console.log(parseFloat(perc));
                    <?php endif; ?>

                    if(parseFloat(perc) >= 5){
                        $(row).find('td:eq(' + i + ')').css('background-color', '#55efc4');
                    }else if(parseFloat(perc) >= 1 && parseFloat(perc) < 5){
                        $(row).find('td:eq(' + i + ')').css('background-color', '#F0E68C');
                    }else if(parseFloat(perc) == 0){
                        $(row).find('td:eq(' + i + ')').css('background-color', '#FFFFFF');
                    }else if(parseFloat(perc) < 0){
                        $(row).find('td:eq(' + i + ')').css('background-color', '#f74050');
                    }
                    i++;
                <?php endforeach; ?>
            },
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
				text: 'Download excel'
            }
        ]
    });

    // Create data for map chart
    var data = [
        ['id-3700', 0],
        ['id-ac', 113213],
        ['id-ki', 2],
        ['id-jt', 3],
        ['id-be', 4],
        ['id-bt', 5],
        ['id-kb', 6],
        ['id-bb', 7],
        ['id-ba', 8],
        ['id-ji', 9],
        ['id-ks', 10],
        ['id-nt', 11],
        ['id-se', 12],
        ['id-kr', 13],
        ['id-ib', 14],
        ['id-su', 15],
        ['id-ri', 16],
        ['id-sw', 17],
        ['id-la', 18],
        ['id-sb', 19],
        ['id-ma', 20],
        ['id-nb', 21],
        ['id-sg', 22],
        ['id-st', 23],
        ['id-pa', 24],
        ['id-jr', 25],
        ['id-1024', 26],
        ['id-jk', 27],
        ['id-go', 28],
        ['id-yo', 29],
        ['id-kt', 30],
        ['id-sl', 31],
        ['id-sr', 32],
        ['id-ja', 33],
        ['id-ku', 34]
    ];

    var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems.slice();

    buttons.push({
        text: 'Show All Provinsi',
        onclick: function () {
            $("#provinsi").val("");
            document.getElementById("filter-form").submit();
        }
    })

    // Create the chart
    Highcharts.mapChart('mymap', {
        chart: {
            map: 'countries/id/id-all'
        },

        title: {
            text: '',
        },

        subtitle: {
            text: ''
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            dataClasses: [{
                from: 5,
                color: "#55efc4"
            }, {
                from: 1,
                to: 4.99,
                color: "#F0E68C"
            }, {
                from: 0,
                to: 0.99,
                color: "#FFFFFF"
            }, {
                from: -100,
                to: -0.01,
                color: "#f74050"
            }]

        },
        // legend: {
        //     borderWidth: 1,

        // },
        legend: {
            backgroundColor: '#D3D3D3'
        },
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: buttons
                }
            }
        },

        series: [{
            data: <?= $mapdata_area_channel;?>,
            nullColor: 'red',
            name: 'Value',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.value} %'
            },
            point:{
                events:{
                    click: function(){      
                        if(this.name=="Jakarta Raya"){
                            $("#provinsi").val("DKI JAKARTA");
                        } else {
                            $("#provinsi").val(this.name);
                        }

                        document.getElementById("filter-form").submit();
                    }

                },
                properties: name
            }
        }]
    });
});
    
</script>
<script src="<?= base_url()?>asset/js/map.js"></script>
<script src="<?=base_url()?>asset/js/id-all-kaltara.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script>
    $('#lini').selectpicker('val', [<?= '"' . implode('","', $data_lini) . '"'; ?>]);
    $('#layanan').selectpicker('val', [<?= '"' . implode('","', $data_layanan) . '"'; ?>]);
</script>