<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" href="<?= base_url() ?>asset/css/css_brand_channel.css" rel="stylesheet">

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<!-- MAPS -->
<div class="white-box">
    <div id="selection" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px; width:100%;">
    <form method="post" action="" id="filter-form"  >
            <input type="text" name="range" value="<?= $data_range; ?>" class="btn btn-default"/>

            <select id="provinsi" name="provinsi[]" class="provinsi selectpicker" multiple data-live-search="true" title="Cabang">
                <?php foreach ($list_area as $ln) : ?>
                    <option value="<?= $ln->nama_kftd;?>" <?php if($data_area == $ln->nama_kftd) echo " selected"; ?> ><?= $ln->nama_kftd; ?></option>
                <?php endforeach; ?>
            </select>
            
            <select id="lini" name="lini[]" class="lini selectpicker" multiple data-live-search="true" title="Lini">
                <?php foreach ($list_lini as $ln) : ?>
                    <option value='<?= $ln->lini; ?>'><?= $ln->lini; ?></option>
                <?php endforeach; ?>
            </select>

            <select id="produk" name="produk[]" class="produk selectpicker" multiple data-live-search="true" title="Produk">
                <?php foreach ($list_produk as $ln) : ?>
                    <option value='<?= $ln->nama_brand; ?>' <?php if($data_produk == $ln->nama_brand) echo " selected"; ?>><?= $ln->nama_brand; ?></option>
                <?php endforeach; ?>
            </select>

            <select id="channel" name="channel" class="channel selectpicker">
                <?php $channel = ['brand-channel', 'channel-brand'];
                foreach ($channel as $l) {
                    if ($l == $channelf) {
                        echo "<option value='$l' selected>$l</option>";
                    } else {
                        echo "<option value='$l'>$l</option>";
                    }
                } ?></select>
                
            <button type="submit" id="btnSubmit" class="btn btn-success">Filter</button>
        </form>

    </div>
    <div id="box2">
        <div class="row">
        <div class="col-md-10">
        <h5>*Click and drag on the area to zoom in</h5>
        <div id="container"></div>
        </div>
        <div class="col-md-2">
        <div id="customLegend"></div>
        </div>
        </div>
    </div>
</div>
<br>
<!-- #MAPS -->
<!-- TABLE -->
<div class="white-box" style="color:black;">
    <div class="table-responsive">
        <table id="mytable" class="display" width="100%">
            <thead>
                <tr>
                    <?php if ($channelf == "brand-channel") : ?>
                        <th>Brand</th>
                    <?php else : ?>
                        <th>Channel</th>
                    <?php endif; ?>
                    <?php foreach ($chan as $kf) : ?>
                        <?php if ($channelf == "brand-channel") : ?>
                            <th><?= $kf->channel; ?></th>
                        <?php else : ?>
                            <th><?= $kf->nama_brand; ?></th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <h5>*notes:</h5>
<h5>Angka yang ditampilkan dalam tabel telah mengalami pembulatan. Pembulatan dilakukan ke bilangan terdekat sampai 3 tempat desimal. (contoh: angka 12,3456 dibulatkan menjadi 12,346).</h5>
<div class="legend"><div class="symbol1"></div>Persentase < 0%</div>
<div class="legend"><div class="symbol2" ></div>Persentase = 0%</div>
<div class="legend"><div class="symbol3"></div>Persentase 0% - 4.9%</div>
<div class="legend"><div class="symbol4"></div>Persentase >= 5%</div>
</div>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"> </script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script>
$(document).ready(function() {

        $('#lini').change(function(e){
            var selectvalue = $(this).val();

            $('#produk').html('<option value="">Loading...</option>').selectpicker('refresh');

            if (selectvalue == "") {
            //Display initial prompt in target select if blank value selected
                $('#produk').html('Produk');
            } else {
                //Make AJAX request, using the selected value as the GET
                $.ajax({
                    url: "boost_brand_channel/json_get_brand",
                    type: 'POST',
                    data: {
                        'table': 'usc_bst_sales_brand_channel',
                        'lini' : selectvalue
                    },
                    success: function(output) {
                            //alert(output);
                            $('#produk').html(output).selectpicker('refresh');
                        },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + " "+ thrownError);
                    }
                });
            }
        });
        
        var kota = "";
        var t = $("#mytable").DataTable({
            initComplete: function() {
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollCollapse: true,
            processing: true,
            ajax: {
                <?php if ($channelf == 'brand-channel') : ?> "url": "boost_brand_channel/json_brand_channel",
                <?php else : ?> "url": "boost_brand_channel/json_channel_brand",
                <?php endif; ?> 
                "type": "POST",
                "data": {
                    "range": "<?= $data_range; ?>",
                    "area": "<?= implode(",", $data_area); ?>",
                    "lini": "<?= implode(",", $data_lini); ?>",
                    "produk": "<?= implode(",", $data_produk); ?>",
                }
            },
            columns: [

                {
                    <?php if ($channelf == 'brand-channel') : ?> "data": "nama_brand"
                    <?php else : ?> "data": "channel"
                    <?php endif; ?>
                },
                
                <?php foreach ($chan as $kfs) : ?> {
                        <?php if ($channelf == 'brand-channel') : ?>
                                "data": "<?= $kfs->channel; ?>"
                        <?php else : ?>
                                "data": "<?= $kfs->nama_brand; ?>"
                        <?php endif; ?>
                    },
                <?php endforeach; ?>
            ],
            order: [
                [0, 'desc']
            ],
            createdRow: function(row, data, dataIndex) {
                var status = false;
                <?php foreach ($chan as $k) : ?>
                    <?php if ($channelf == 'brand-channel') : ?>
                        if (data['<?= $k->channel; ?>'] != 0) {
                            status = true;
                        }
                    <?php else : ?>
                        if (data["<?= $k->nama_brand; ?>"] != 0) {
                            status = true;
                        }
                    <?php endif; ?>
                <?php endforeach; ?>
                if (!status) {
                    t.rows($(row)).remove();
                }
            },
            rowCallback: function(row, data, iDisplayIndex) {
                var i = 1;
                <?php foreach ($chan as $k) : ?>
                    <?php if ($channelf == 'brand-channel') : ?>
                        perc = data['<?= $k->channel; ?>'];
                    <?php else : ?>
                        perc = data['<?= $k->nama_brand; ?>'];

                    <?php endif; ?>
                    if(parseFloat(perc) < 0){
                    $('td:eq(' + i + ')', row).css('background-color','#f80d41');
                    
                    }
                    if(parseFloat(perc) == 0){
                        $('td:eq(' + i + ')', row).css('background-color','#fff');
                        
                    }
                    if(parseFloat(perc) > 0){
                        $('td:eq(' + i + ')', row).css('background-color','#F0E68C');
                    }
                    if(parseFloat(perc) > 4.9){
                        $('td:eq(' + i + ')', row).css('background-color','#55efc4');
                    }
                    i++;
                <?php endforeach; ?>     
            },
            dom: '<"top"l>fBtr<"bottom"ip>',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Download excel'
                }
            ]
        });

    $(function () {
        Highcharts.chart('container', {
            chart: {
                type: 'scatter',
                zoomType: 'xy'
            },
            accessibility: {
                description: null
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: ''
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true,
                plotLines: [{
                    color: '#000101', // Color value
                    value: 0, // Value of where the line will appear
                    width: 2 // Width of the line    
                }]
            },
            yAxis: {
                title: {
                    text: 'Growth (%)'
                }, 
                // min: -100,
                // max: 100,
                plotLines: [{
                    color: '#000101', // Color value
                    value: 0, // Value of where the line will appear
                    width: 2 // Width of the line    
                }]
            },
            credits: {
            enabled: false
            },

            plotOptions: {
                scatter: {
                    marker: {
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: 'Growth: {point.y} %, Kontribusi:{point.x}'
                    }
                }
            },

            series: [<?=$scatter_series?>]
        }, function (chart) {
            <?php
            $data_legend='';

                foreach($data_channel as $key=>$nilai){
                $data_legend .= '<div class="item"><div class="symbol" style="background-color:'.$arrWarna[$nilai->channel].'"></div><div class="serieName" id="">'.$nilai->channel.'</div></div>';
                }
            
            ?>

            $legend = $('#customLegend');
            $legend.append('<?=$data_legend?>');

            $('#customLegend .item').click(function(){
                var inx = $(this).index(),
                    point = chart.series[0].data[inx];
            
                if(point.visible)
                    point.setVisible(false);
                else
                    point.setVisible(true);
            });        
            }
        );
    });
});

$(function() {
    $('input[name="range"]').daterangepicker({
        opens: 'center',
        startDate:'<?=$startDate?>',
        endDate:'<?=$endDate?>',
        locale:{
            format:'YYYY-MM-DD',
            separator:'/'
        }
    }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('Y-m-d') + ' to ' + end.format('Y-m-d'));
    });

    $('input[name="range"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));
    });
    
});

$('#lini').selectpicker('val', [<?= '"' . implode('","', $lini_data) . '"'; ?>]);
$('#provinsi').selectpicker('val', [<?= '"' . implode('","', $data_area) . '"'; ?>]);
$('#produk').selectpicker('val', [<?= '"' . implode('","', $data_produk) . '"'; ?>]);
</script>