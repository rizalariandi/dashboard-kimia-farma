<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" href="<?= base_url() ?>asset/css/css_outlet.css" rel="stylesheet">

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<div class="white-box">
    <div id="filter" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px;">
        <select id="area" name="area" class="selectpicker" multiple data-live-search="true" title="KFTD" data-width="fit">
            <?php foreach ($areas as $a) :?>
                <option value='<?= $a->nama_kftd;?>' <?= ($a->nama_kftd == $area)?'selected':'';?>><?= $a->nama_kftd;?></option>
            <?php endforeach; ?>
        </select>
        <select id="channel" name="channel" class="selectpicker" multiple data-live-search="true" title="Channel" data-width="fit">
            <?php foreach ($channels as $c) :?>
                <option value='<?= $c->channel;?>' <?= ($c->channel == $channel)?'selected':'';?>><?= $c->channel;?></option>
            <?php endforeach; ?>
        </select>
        
        <select id="lini" name="lini" class="selectpicker" multiple data-live-search="true" title="Lini" data-width="fit">
            <?php foreach ($linis as $c) :?>
                <option value='<?= $c->lini;?>' <?= ($c->lini == $lini)?'selected':'';?>><?= $c->lini;?></option>
            <?php endforeach; ?>
        </select>

        <select id="brand" name="brand" class="selectpicker" multiple data-live-search="true" title="Brand" data-width="fit">
            <?php foreach ($list_brand as $c) :?>
                <option value='<?= $c->nama_brand;?>' <?= ($c->nama_brand == $brand)?'selected':'';?>><?= $c->nama_brand;?></option>
            <?php endforeach; ?>
        </select>

        <input type="text" class="btn btn-default" id = "periode" name="periode" placeholder="Periode"/>
        <button id="submit_otr" class="btn btn-success">Filter</button>
    </div>
</div>

<div id="box1">
    <div class="top" style="padding-top: 15px; padding-left: 20px; padding-bottom: 5px;">
        <h3 id="title_chart">Top Brand</h3>
    </div>

    <div id='loader_otr' style='display: none; margin: 0 auto;'>
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
    <div id="outlet_otr"></div>
</div>

<br>
<!-- CHART 2 ONCLICK -->
<div id='loader_otr_pop' style='display: none; margin: 0 auto;'>
    <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
</div>
<div style="display:none" id="main-container">
    <div class="white-box">
        <div class="top">
            <h3 id="title_pop"></h3>
        </div>
        <div id="pop_up_outlet_otr"></div>
    </div>
</div>

<!-- CHART 3 Trend Outlet-->
<div id="box1">
    <div class="top" style="padding-top: 15px; padding-left: 20px; padding-bottom: 5px;">
    <h3>Trend Outlet</h3>
    </div>
    <div id='loader_trend' style='display: none; margin: 0 auto;'>
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
    <div id="trend_outlet"></div>
</div>
<br>

<!-- TABLE LIST -->

<div id="box1">
    <div class="box-body table-responsive"> 
        <div class="header" style="display: flex; margin-bottom: 20px;">
            <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Quantity</h1>
            <div style="margin-top: 20px;">
            </div>
        </div>
        <!-- <div id="periode_transaction_quantity"><h4><b><?php echo 'Periode : ' . $periode; ?></b></h4></div> -->
        <table class="table table stripped" id="outlet_transaction">
            <thead>
            <tr>
                <th rowspan="2" style="background-color:#4E9ED5; text-align:center;">Brand</th>
                <?php for($m=1; $m<=12; $m++) : ?>
                    <th colspan='2' center style="background-color:#F79868"><?php echo date('M', mktime(0,0,0,$m,10 ));?></th>
                <?php endfor; ?>
            </tr>
            <tr>
                <?php for($m=1; $m<=12; $m++) : ?>
                    <th style="background-color:#F79868">Qty</th>
                    <th style="background-color:#F79868">Value</th> 
                <?php endfor; ?>
            </tr>
            </thead>
            <tfoot>
                <tr>
                    <td rowspan="2" style="text-align:center;"><b>Total</b></td>
                    <?php for($m=1; $m<=12; $m++) : ?>
                        <!-- <td colspan='2' center ></td> -->
                        <td></td>
                        <td></td>
                    <?php endfor; ?>
                </tr>
                <tr>
                    <?php for($m=1; $m<=12; $m++) : ?>
                        <td></td>
                        <td></td> 
                    <?php endfor; ?>
                </tr>
            </tfoot>
            <tbody id="show_data">
            <!-- show data dari mana? -->
            </tbody>

        </table>
    </div>
</div>

<br>

<div id="box1">
<div class="box-body table-responsive"> 
    <div class="header" style="display: flex; margin-bottom: 20px;">
        <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Brand</h1>
        <div style="margin-top: 20px;">
        </div>
        <!-- <h5 style="float: right; margin-top: 30px; margin-left: auto;">Updated date: <?php echo $last_update; ?></h5> -->

    </div>
    <table class="table table stripped" id="outlet_brand">
        <thead>
        <tr>
            <th rowspan="2" style="background-color:#4E9ED5">Brand</th>
            <?php foreach($channels as $ch) : ?>
                <th colspan="2" style="background-color:#F79868"><?= $ch->channel;?></th>
            <?php endforeach; ?>
        </tr>
        <tr>
            <?php foreach($channels as $ch) : ?>
                <?php foreach ($tahun_table as $t) : ?>
                <th id="tahun" style="background-color:#F79868"><?php echo $t->tahun; ?></th>
                <?php endforeach; ?>   
            <?php endforeach; ?>
        </tr>
        </thead>
        <tfoot>
            <tr>
                <td rowspan="2" style="text-align:center;"><b>Total</b></td>
                <?php foreach($channels as $ch) : ?>
                    <td></td>
                    <td></td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach($channels as $ch) : ?>
                    <?php foreach($tahun_table as $t) : ?>
                        <td></td>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
</div>

<br>

<div id="box1">
<div class="box-body table-responsive"> 
    <div class="header" style="display: flex; margin-bottom: 20px; margin-top: 20px;">
        <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Cabang</h1>
        <!-- <h5 style="float: right; margin-top: 30px; margin-left: auto;">Updated date: <?php echo $last_update; ?></h5> -->
    </div>

    <table class="table table stripped" id="outlet_area">
        <thead>
        <tr>
            <th rowspan="2" style="background-color:#4E9ED5">Cabang</th>
            <?php foreach($channels as $ch) : ?>
                <th colspan="2" style="background-color:#F79868"><?= $ch->channel;?></th>
            <?php endforeach; ?>
        </tr>
        <tr>
            <?php foreach($channels as $ch) : ?>
                <?php foreach ($tahun_table as $t) : ?>
                <th style="background-color:#F79868"><?php echo $t->tahun; ?></th>
                <?php endforeach; ?>  
            <?php endforeach; ?>
        </tr>
        </thead>
        <tfoot>
            <tr>
                <td rowspan="2" style="text-align:center;"><b>Total</b></td>
                <?php foreach($channels as $ch) : ?>
                    <td></td>
                    <td></td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach($channels as $ch) : ?>
                    <?php foreach($tahun_table as $t) : ?>
                        <td></td>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tr>
        </tfoot>
        <tbody>     
        </tbody>
    </table>
</div>
</div>

<script>
var nama_brand_do = <?php echo $nama_brand_otr; ?>;
var data_brand = <?php echo $data_otr; ?>;
var tahun = <?php echo "[" . implode(',', $tahun) . "]"; ?>;

Highcharts.setOptions({
  lang: {
    numericSymbols: ['K', 'M', 'B', 'T']
  }
});

// Chart for top 20 Brand
var outlet_otr = Highcharts.chart('outlet_otr', {
    chart: {
        type: 'column'
    },
    title: {
        text: null
    },
    subtitle: {
        text: null
    },
    xAxis: {
        categories: nama_brand_do
    },
    yAxis: {
        title: {
            text: 'Jumlah Outlet'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>'+
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        pointFormat: null,
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    credits: {
    enabled: false
    }, 

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        drill_down(this.category);
                    }
                }
            }
        }
    },
    series :  data_brand,
});

// chart for drill down on brand clicked
function drill_down(name){
    $('#main-container').show();
    // $('#title').html(name);
    var kftd = $("select[name='area']").val();
    var channel = $("select[name='channel']").val();
    var periode = $("input[name='periode'").val();
        

    $.ajax({
        url: 'boost_outlet_otr/json_get_list_product',
        type: 'POST',
        data: { 
            'kftd' : kftd,
            'channel' : channel,
            'brand' : name,
            'periode' : periode
        },
        beforeSend: function(){
            $("#loader_otr_pop").show();
        },
        error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
            var cat = [];
            var series_data = [];
            // console.log(data.data);
            $('#title_pop').html(name);

            for (let i=0; i<data.categories.length; i++){
                cat.push(data.categories[i].nama_produk);
            }
            // console.log(cat);
            Highcharts.chart('pop_up_outlet_otr', {
                chart: {
                    zoomType: 'xy'
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                xAxis: [{
                    categories: cat,//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    crosshair: true
                }],
                yAxis: [{ // Primary yAxis 2018
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Jumlah Outlet',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { // Secondary yAxis 2018
                    title: {
                        text: 'Growth',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'horizontal'
                },
                series: data.data
            });
            
        },
        complete:function(data){
            $("#loader_otr_pop").hide();
        }

    });
};
</script>

<!-- chart for trend outlet -->
<script>
Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});

var trend_outlet = Highcharts.chart('trend_outlet', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: null
    },

    subtitle: {
        text: null
    },
    credits: {
        enabled: false
        }, 
    yAxis: [{
        title: {
            text: 'Jumlah Outlet'
        },
        labels: {
            formatter: function() {
                return Highcharts.numberFormat(this.value, 2, '.', ' ')
            }
	    },
    }, {
        gridLineWidth: 0,
        title: {
            text: 'Value',
        },
        opposite: true
    }],
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    xAxis: {
        categories: <?php echo $cat_month; ?>
    },
    tooltip: {
        shared: true
    },
    series: <?php echo $trend; ?>,
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>

<!-- script for data table -->
<script>
$(document).ready( function () {
    var kftd = $("select[name='area']").val();
    var channel = $("select[name='channel']").val();
    var lini = $("select[name='lini']").val();
    var brand = $("select[name='brand']").val();
    var periode = $("input[name='periode'").val();
    var startDate = "";
    var endDate = "";
    
    $(function() {
        let tgl = moment();

        $('input[name="periode"]').daterangepicker({
            opens: 'right',
            // startDate: '2019-01-01',
            // endDate: '2019-12-31',
            startDate: tgl.subtract('2', 'month').format('YYYY-MM-DD'),
            endDate: moment().format('YYYY-MM-DD'),
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }
        }, function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            startDate = start.format('YYYY');
            endDate = start.format('YYYY');
        });
        // $('input[name="periode"]').val('2019-01-01 / 2019-12-31');
        // $('input[name="periode"]').val('2021-01-01 / 2021-12-31');

    });

    // get brand based on lini
    $('#lini').change(function(e){
        var selectvalue = $(this).val();

        $('#brand').html('<option value="">Loading...</option>').selectpicker('refresh');

        if (selectvalue == "") {
            $('#brand').html('Brand');
        } else {
            $.ajax({
                url: "boost_outlet_otr/json_get_brand",
                type: 'POST',
                data: {
                    'table': 'usc_bst_sales_outlet_channel',
                    'lini' : selectvalue
                },
                success: function(output) {
                        $('#brand').html(output).selectpicker('refresh');
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " "+ thrownError);
                }
            });
        }
    });

    // table outlet transaction by quantity
    var outlet_transaction = $('#outlet_transaction').DataTable({
        initComplete: function(){
        },
        oLanguage:{
            sProcessing: "Loading..."
        },
        scrollCollapse: true,
        processing: true,
        serverSide: true,
        ajax: {
            "url": "boost_outlet_otr/json_outlet_transaction_quantity",
            "type": "POST",
            "async": "true",
            "data": {
                "kftd" : function() { return $('#area').val() },
                "channel" : function() { return $('#channel').val() },
                "lini": function() { return $('#lini').val() },
                "brand" : function() { return $('#brand').val() }, 
                "periode" : function(){ return $('#periode').val() }
            }
        },
        columns: [        
            {
                "data": "nama_brand"
            },
            <?php $quan = array('quantity','revenue'); ?>

            <?php for($m=1; $m<=12; $m++) : ?> 
                <?php foreach($quan as $cuan) : ?> {
                    "data" : "<?= date('M', mktime(0,0,0,$m, 10 )).'-'.$cuan; ?>",
                    "render" : $.fn.dataTable.render.number( '.', ',', 0)
                },   
                <?php endforeach; ?>
            <?php endfor; ?>
        ],
        order: [
            [0, 'asc']
        ],
        // pageLength: 5,
        searching: false,
        "autoWidth": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var numFormat = $.fn.dataTable.render.number( '.', ',', 0).display;
            nb_cols = api.columns().nodes().length;
            console.log("nb_cols outlet transaction by quantity = ",nb_cols);
			var j = 1;
            
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            // total = api
            //     .column( 4 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
 
            // Total over this page
            while(j < nb_cols){
                pageTotal = api
                    .column( j, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( j ).footer() ).html(
                    "<b>" + numFormat(pageTotal) + "</b>"
                );
                console.log("page total outlet trx by quantity = ",pageTotal);
                j++;
            }
        },

        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });

    // table outlet transaction by brand
    var outlet_brand = $('#outlet_brand').DataTable({
        initComplete: function(){
        },
        oLanguage:{
            sProcessing: "Loading..."
        },
        scrollCollapse: true,
        processing: true,
        // serverSide: true,
        ajax: {
            "url": "boost_outlet_otr/json_outlet_transaction_brand",
            "type": "POST",
            "async": "true",
            "data": {
                "kftd" : function() { return $('#area').val() },
                "channel" : function() { return $('#channel').val() },
                "lini": function() { return $('#lini').val() },
                "brand" : function() { return $('#brand').val() }, 
                "periode" : function(){ return $('#periode').val() }
            }
        },
        pageLength: 10,
        columns: [
            {
                "data": "nama_brand"
            },
            <?php foreach ($channels as $ch) : ?> 
                <?php foreach ($tahun_table as $th) : ?> {
                    "data": "<?= $ch->channel . "-" . $th->tahun; ?>",
                    "render": $.fn.dataTable.render.number( '.', ',', 0)
                },
                <?php endforeach; ?>
            
            <?php endforeach; ?>
        ],
        order: [
            [0, 'asc']
        ],
        searching: false,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var numFormat = $.fn.dataTable.render.number( '.', ',', 0).display;
            nb_cols = api.columns().nodes().length;
            console.log(nb_cols);
			var j = 1;
            
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            // total = api
            //     .column( 4 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
 
            // Total over this page
            while(j < nb_cols){
                pageTotal = api
                    .column( j, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( j ).footer() ).html(
                    "<b>" + numFormat(pageTotal) + "</b>"
                );
                console.log(pageTotal);
                j++;
            }
        },
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });

    // table outlet transaction by cabang
    var outlet_area = $('#outlet_area').DataTable({
        initComplete: function(){
        },
        oLanguage:{
            sProcessing: "Loading..."
        },
        scrollCollapse: true,
        // processing: true,
        // serverSide: true,
        ajax: {
            "url": "boost_outlet_otr/json_outlet_transaction_area",
            "type": "POST",
            "async": "true",
            "data": {
                "kftd" : function() { return $('#area').val() },
                "channel" : function() { return $('#channel').val() },
                "lini": function() { return $('#lini').val() },
                "brand" : function() { return $('#brand').val() }, 
                "periode" : function(){ return $('#periode').val() }
            }
        },
        pageLength: 10,
        columns: [
            {
                "data": "nama_kftd"
            },
            <?php foreach ($channels as $ch) : ?> 
                <?php foreach ($tahun_table as $th) : ?> {
                    "data": "<?= $ch->channel . "-" . $th->tahun; ?>",
                    "render": $.fn.dataTable.render.number( '.', ',', 0)
                },
                <?php endforeach; ?>
            
            <?php endforeach; ?>
        ],
        order: [
            [0, 'asc']
        ],
        searching: false,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var numFormat = $.fn.dataTable.render.number( '.', ',', 0).display;
            nb_cols = api.columns().nodes().length;
            console.log(nb_cols);
			var j = 1;
            
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            // total = api
            //     .column( 4 )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
 
            // Total over this page
            while(j < nb_cols){
                pageTotal = api
                    .column( j, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( j ).footer() ).html(
                    "<b>" + numFormat(pageTotal) + "</b>"
                );
                console.log(pageTotal);
                j++;
            }
        },
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });
    
    $("#submit_otr").click(function(){

        if (brand == ""){
            brand = [];
        }

        $.ajax({
            url: 'boost_outlet_otr/json_outlet_otr',
            type: 'POST',
            data: {
                "kftd" : $("select[name='area']").val(),
                "channel" : $("select[name='channel']").val(),
                "lini" : $("select[name='lini']").val(),
                "brand" : $("select[name='brand']").val(),
                "periode" : $("input[name='periode']").val(),
            },
            beforeSend: function(){
                $("#loader_otr").show();
                $('#main-container').hide();
                document.getElementById("outlet_otr").style.display = "none";

            },
            error: function() {
                
            },
            success: function(data) {
                
                outlet_otr.showLoading();
                outlet_otr.update({
                    series: data.data
                }, true, true);
                outlet_otr.xAxis[0].update({
                    categories: data.nama_brand
                })
                outlet_otr.redraw();
                outlet_otr.hideLoading();
            },
            complete:function(data){
                $("#loader_otr").hide();
                document.getElementById("outlet_otr").style.display = "block";
            }
        });

        $.ajax({
            url: 'boost_outlet_otr/json_outlet_trend',
            type: 'POST',
            data: {
                "kftd" : $("select[name='area']").val(),
                "channel" : $("select[name='channel']").val(),
                "lini" : $("select[name='lini']").val(),
                "brand" : $("select[name='brand']").val(),
                "periode" : $("input[name='periode']").val(),
            },
            beforeSend: function(){
                $("#loader_trend").show();
                document.getElementById("trend_outlet").style.display = "none";

            },
            error: function(){
                alert('Something is wrong');
            },
            success: function(response) {
                // console.log(response);
                trend_outlet.showLoading();
                trend_outlet.xAxis[0].update({
                    categories: response.bulan
                });
                trend_outlet.update({
                    series: response.data
                }, true, true);
                trend_outlet.redraw();
                trend_outlet.hideLoading();
            },
            complete:function(data){
                $("#loader_trend").hide();
                document.getElementById("trend_outlet").style.display = "block";
            }
        });

        // console.log($('#outlet_brand th').text());
        // if ($('#outlet_brand thead tr:nth-child(2) th').text() == '2018') {
        //     console.log('yes 2018');
        // }

        // var x = document.getElementById("tahun");
        // console.log(x);
        // console.log($('#outlet_brand thead tr:nth-child(2) th:nth-child(even)').text());
        // console.log(startDate);
        // console.log(endDate);

        if (startDate == endDate) {
            // console.log('Tahun nya sama');
            // console.log(startDate - 1);
            // console.log(endDate);
            $('#outlet_brand thead tr:nth-child(2) th:nth-child(odd)').text(startDate - 1 );
            $('#outlet_brand thead tr:nth-child(2) th:nth-child(even)').text(endDate);
            $('#outlet_area thead tr:nth-child(2) th:nth-child(odd)').text(startDate - 1 );
            $('#outlet_area thead tr:nth-child(2) th:nth-child(even)').text(endDate);

        } else {
            $('#outlet_brand thead tr:nth-child(2) th:nth-child(odd)').text(startDate);
            $('#outlet_brand thead tr:nth-child(2) th:nth-child(even)').text(endDate);
            $('#outlet_area thead tr:nth-child(2) th:nth-child(odd)').text(startDate);
            $('#outlet_area thead tr:nth-child(2) th:nth-child(even)').text(endDate);
        }

        outlet_transaction.ajax.reload();
        outlet_brand.ajax.reload();
        outlet_area.ajax.reload();

    });

});
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
