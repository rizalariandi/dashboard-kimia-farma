<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

<style>
#box1 {
    background: #fff;
    border-radius: 10px 10px 0px 0px;
}
#title{
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 20px;
}
.box-body{
    padding: 15px;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
}
.dataTables_wrapper .dataTables_length {
    float: left;
    margin-right: 70%;
}
div.dataTables_wrapper div.dataTables_filter{
    float:left;
    left:0;
    margin-left:33%;
    margin-right:15%;
    margin-bottom: 2px;
}
div.dataTables_wrapper div.dataTables_buttons{
    float:right;

}
table.dataTable thead tr {
    background-color: #F79868;
}

.selectpicker{
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    /* or 143% */

    display: flex;
    align-items: center;

    color: #4F4F4F;
}

.btn-default{
    background: #FFFFFF;
    border: 1px solid #08388F;
    box-sizing: border-box;
    border-radius: 5px;
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    /* or 143% */
    color: #4F4F4F;
}

.calendar-table thead,
.calendar-table th,
.calendar-table td {
        color: black !important;
}
</style>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>
    <div id="box1">
        <div id="filter" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px;">
            <select id="area" name="area" class="selectpicker" multiple data-live-search="true" title="KFTD" data-width="fit">
                <?php foreach ($areas as $a) :?>
                    <option value='<?= $a->nama_kftd;?>' <?= ($a->nama_kftd == $area)?'selected':'';?>><?= $a->nama_kftd;?></option>
                <?php endforeach; ?>
            </select>
            <select id="channel" name="channel" class="selectpicker" multiple data-live-search="true" title="Channel" data-width="fit">
                <?php foreach ($channels as $c) :?>
                    <option value='<?= $c->channel;?>' <?= ($c->channel == $channel)?'selected':'';?>><?= $c->channel;?></option>
                <?php endforeach; ?>
            </select>
            
            <select id="lini" name="lini" class="selectpicker" data-live-search="true" title="Lini" data-width="fit">
                <?php foreach ($linis as $c) :?>
                    <option value='<?= $c->lini;?>' <?= ($c->lini == $lini)?'selected':'';?>><?= $c->lini;?></option>
                <?php endforeach; ?>
            </select>

            <select id="brand" name="brand" class="selectpicker" multiple data-live-search="true" title="Brand" data-width="fit">
                <?php foreach ($list_brand as $c) :?>
                    <option value='<?= $c->nama_brand;?>' <?= ($c->nama_brand == $brand)?'selected':'';?>><?= $c->nama_brand;?></option>
                <?php endforeach; ?>
            </select>

            <input type="text" class="btn btn-default" name="daterange_otr" placeholder="Periode"/>
            <button id="submit_otr" class="btn btn-success">Filter</button>
        </div>
         <!-- Image loader -->
        <div id='loader_otr' style='display: none; margin: 0 auto;'>
            <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
        </div>
        <div id="outlet_otr"></div>

    </div>
    
    <br>
<!-- CHART 2 ONCLICK -->
    <div id='loader_otr_pop' style='display: none; margin: 0 auto;'>
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
    <div style="display:none" id="main-container">
        <div class="white-box">
            <div class="top">
                <h3 id="title"></h3>
            </div>
            <div id="pop_up_outlet_otr"></div>
        </div>
    </div>
    
    <div id='loader_growth' style='display: none; margin: 0 auto;'>
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
    <div style="display:none" id="growth_product">
        <div class="white-box">
            <div class="top">
                <h3 id="title_growth"></h3>
            </div>
            <div id="pop_up_growth"></div>
        </div>
    </div>

    <!-- CHART 3 Trend Outlet-->
    <div id="box1">
        <div class="top" style="padding-top: 15px; padding-left: 20px; padding-bottom: 5px;">
        <h3>Trend Outlet</h3>
        <div id='loader_trend' style='display: none; margin: 0 auto;'>
            <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
        </div>
        <div id="trend_outlet"></div>
    </div>
        <br>
    <!-- CHART 4 Trend Outlet by Product-->
    <!-- <div class="white-box">
    <div id="box1">
        <h3>Trend Outlet by Product</h3>
        <div id="filter" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px;">
        <select id="area_trend_product" name="area_trend_product" class="selectpicker" multiple data-live-search="true" title="Cabang" data-width="fit">
                <?php foreach ($areas as $a) :?>
                    <option value='<?= $a->nama_kftd;?>' <?= ($a->nama_kftd == $area)?'selected':'';?>><?= $a->nama_kftd;?></option>
                <?php endforeach; ?>
        </select>
        <select id="channel_trend" name="channel_trend" class="selectpicker" multiple data-live-search="true" title="Channel" data-width="fit">
            <?php foreach ($channels as $c) :?>
                <option value='<?= $c->channel;?>' <?= ($c->channel == $channel)?'selected':'';?>><?= $c->channel;?></option>
            <?php endforeach; ?>
        </select>
        <select id="brand_trend" name="brand_trend" class="selectpicker" multiple data-live-search="true" title="Brand" data-width="fit">
            <?php foreach ($list_brand as $c) :?>
                <option value='<?= $c->nama_brand;?>' <?= ($c->nama_brand == $brand_trend)?'selected':'';?>><?= $c->nama_brand;?></option>
            <?php endforeach; ?>
        </select>

        <input type="text" class="btn btn-default" name="daterange_outlet_product" placeholder="Periode"/>
        <button id="submit_product" class="btn btn-success">Filter</button>
        </div>
        <div id='loader_product' style='display: none; margin: 0 auto;'>
            <img style="display: block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
        </div>
        <div id="trend_outlet_product"></div>
    </div> -->
    <br>
    <br>
    <!-- TABLE LIST -->

    <div class="box-body table-responsive"> 
        <div class="header" style="display: flex; margin-bottom: 20px;">
            <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Quantity</h1>
            <div style="margin-top: 20px;">
            <!-- <select id="lini_table" name="lini_table" class="selectpicker" multiple data-live-search="true" title="Lini" data-width="fit">
                <?php foreach ($lini as $a) :?>
                    <option value='<?= $a->lini;?>' <?= ($a->lini == $lini_table)?'selected':'';?>><?= $a->lini;?></option>
                <?php endforeach; ?>
            </select> -->
            <!-- <select id="channel_table_outlet" name="channel_table_outlet" class="selectpicker" multiple data-live-search="true" title="Channel" data-width="fit">
                <?php foreach ($channels as $c) :?>
                    <option value='<?= $c->channel;?>' <?= ($c->channel == $channel_table_outlet)?'selected':'';?>><?= $c->channel;?></option>
                <?php endforeach; ?>
            </select> -->
            <!-- <select id="periode_table_outlet" style="display:none;" name="periode_table_outlet" class="selectpicker" data-live-search="true" title="Periode" data-width="fit">
                <?php foreach ($tahun_otr as $c) :?>
                    <option value='<?= $c->tahun;?>' <?= ($c->tahun == date('Y'))?'selected':'';?>><?= $c->tahun;?></option>
                <?php endforeach; ?>
            </select> -->
            <!-- <button id="submit_table_outlet" class="btn btn-success">Filter</button> -->
            </div>
        </div>
        <div id="tahun"><h4><b><?php echo date('Y'); ?></b></h4></div>
        <table class="table table stripped" id="outlet_transaction">
            <thead>
            <tr>
                <th rowspan="2" style="background-color:#4E9ED5, text-align:center;">Brand</th>
                <?php for($m=1; $m<=12; $m++) : ?>
                    <th colspan='2' center style="background-color:#F79868"><?php echo date('M', mktime(0,0,0,$m,10 ));?></th>
                <?php endfor; ?>
            </tr>
            <tr>
            <?php for($m=1; $m<=12; $m++) : ?>

                    <th style="background-color:#F79868">Qty</th>
                    <th style="background-color:#F79868">Value</th> 
                <?php endfor; ?>

            </tr>
            </thead>
            <tbody id="show_data">
            <!-- show data dari mana? -->
            </tbody>
        </table>
    </div>

    <div class="box-body table-responsive"> 
        <div class="header" style="display: flex; margin-bottom: 20px;">
            <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Brand</h1>
            <div style="margin-top: 20px;">
            <!-- <select id="area_table" name="area_table" class="selectpicker" multiple data-live-search="true" title="Cabang" data-width="fit">
                <?php foreach ($areas as $a) :?>
                    <option value='<?= $a->nama_kftd;?>' <?= ($a->nama_kftd == $area)?'selected':'';?>><?= $a->nama_kftd;?></option>
                <?php endforeach; ?>
            </select>
            <select id="channel_table" name="channel_table" class="selectpicker" multiple data-live-search="true" title="Channel" data-width="fit">
                <?php foreach ($channels as $c) :?>
                    <option value='<?= $c->channel;?>' <?= ($c->channel == $channel)?'selected':'';?>><?= $c->channel;?></option>
                <?php endforeach; ?>
            </select>
            <button id="submit_table" class="btn btn-success">Filter</button> -->
            </div>
            <h5 style="float: right; margin-top: 30px; margin-left: auto;">Updated date: <?php echo $last_update; ?></h5>

        </div>
        <table class="table table stripped" id="outlet_brand">
            <thead>
            <tr>
                <th rowspan="2" style="background-color:#4E9ED5">Brand</th>
                <?php foreach($channels as $ch) : ?>
                    <th colspan="2" style="background-color:#F79868"><?= $ch->channel;?></th>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach($channels as $ch) : ?>
                    <th style="background-color:#F79868"><?php echo date('Y', strtotime('-1 year'));?></th>
                    <th style="background-color:#F79868"><?php echo date("Y"); ?> </th>    
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
<br>
    <div class="box-body table-responsive"> 
        <div class="header" style="display: flex; margin-bottom: 20px; margin-top: 20px;">
            <h1 style="font-size: 20px; padding-right: 10px;">Outlet Transaction by Cabang</h1>
            <h5 style="float: right; margin-top: 30px; margin-left: auto;">Updated date: <?php echo $last_update; ?></h5>
        </div>
    
        <table class="table table stripped" id="outlet_area">
            <thead>
            <tr>
                <th rowspan="2" style="background-color:#4E9ED5">Cabang</th>
                <?php foreach($channels as $ch) : ?>
                    <th colspan="2" style="background-color:#F79868"><?= $ch->channel;?></th>
                <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach($channels as $ch) : ?>
                    <th style="background-color:#F79868"><?php echo date('Y', strtotime('-1 year'));?></th>
                    <th style="background-color:#F79868"><?php echo date("Y"); ?> </th>    
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>     
            </tbody>
        </table>
    </div>

<script>
var nama_brand_do = <?php echo $nama_brand_otr; ?>;
var data_brand = <?php echo $data_otr; ?>;
var tahun = <?php echo "[" . implode(',', $tahun) . "]"; ?>;

Highcharts.setOptions({
  lang: {
    numericSymbols: ['K', 'M', 'B', 'T']
  }
});

var outlet_otr = Highcharts.chart('outlet_otr', {
    chart: {
        type: 'column'
    },
    title: {
        text: null
    },
    subtitle: {
        text: null
    },
    xAxis: {
        categories: nama_brand_do
    },
    yAxis: {
        title: {
            text: 'Jumlah Outlet'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>'+
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        pointFormat: null,
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    credits: {
    enabled: false
    }, 

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        drill_down(this.category);
                    }
                }
            }
        }
    },
    series :  data_brand,
});

function drill_down(name){
    $('#main-container').show();
    $('#title').html(name);
    // var provinsi = $("select[name='area']").val();
    var kftd = $("select[name='area']").val();

    var channel = $("select[name='channel']").val();
    var periode = $("input[name='daterange_otr'").val();
        

    $.ajax({
        url: 'json_get_list_product',
        type: 'POST',
        data: { 
            'nama_brand' : name,
            'kftd' : kftd,
            'channel' : channel,
            'periode' : periode
        },
        beforeSend: function(){
            $("#loader_otr_pop").show();
        },
        error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
            var cat = [];
            var series_data = [];
            console.log(data.data);

            for (let i=0; i<data.categories.length; i++){
                cat.push(data.categories[i].nama_produk);
            }
            console.log(cat);
            Highcharts.chart('pop_up_outlet_otr', {
                chart: {
                    zoomType: 'xy'
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                xAxis: [{
                    categories: cat,//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    crosshair: true
                }],
                yAxis: [{ // Primary yAxis 2018
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Jumlah Outlet',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { // Secondary yAxis 2018
                    title: {
                        text: 'Growth',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'horizontal'
                },
                series: data.data
            });
            /*Highcharts.chart('pop_up_outlet_otr', {
                chart: {
                    zoomType: 'xy'
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                xAxis: [{
                    categories: cat,
                    crosshair: true
                }],
                yAxis: [{ // Primary yAxis
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Value (Rp)',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { // Secondary yAxis
                    title: {
                        text: 'Total SKU',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'horizontal'
                },
                series: data.data
            });

            Highcharts.chart('pop_up_outlet_otr', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                xAxis: {
                    categories: cat
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>'+
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    pointFormat: null,
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                // plotOptions: {
                //     series: {
                //         cursor: 'pointer',
                //         point: {
                //             events: {
                //                 click: function () {
                //                     pop_growth(this.category);
                //                 }
                //             }
                //         }
                //     }
                // },
                series : data.data
            });*/
        },
        complete:function(data){
            $("#loader_otr_pop").hide();
        }

    });
};

function pop_growth(name){
    $('#growth_product').show();
    $('#title_growth').html(name);
    var provinsi = $("select[name='area']").val();
    var channel = $("select[name='channel']").val();
    var periode = $("input[name='daterange_otr'").val();

    $.ajax({
        url: 'json_get_growth_product',
        type: 'POST',
        data: { 
            'nama_produk' : name,
            'provinsi' : provinsi,
            'channel' : channel,
            'periode' : periode
        },
        beforeSend: function(){
            $("#loader_growth").show();
        },
        error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
            Highcharts.chart('pop_up_growth', {
                title: {
                    text: null
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                    }, 
                yAxis: {
                    title: {
                        text: null
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                xAxis: {
                    categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                    type:'month'
                },
                series : {
                    name : 'growth product',
                    data : [20,15,50,46,70,80,90,88,77,100,150,140]
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
        },
        complete:function(data){
            $("#loader_growth").hide();
        }
    });
}
</script>
<script>
Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});

var trend_outlet = Highcharts.chart('trend_outlet', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: null
    },

    subtitle: {
        text: null
    },
    credits: {
        enabled: false
        }, 
    yAxis: [{
        title: {
            text: 'Jumlah Outlet'
        },
        labels: {
            formatter: function() {
                return Highcharts.numberFormat(this.value, 2, '.', ' ')
            }
	    }
    }, {
        gridLineWidth: 0,
        title: {
            text: 'Value',
        },
        opposite: true
    }],
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    xAxis: {
        // categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        categories: <?php echo $cat_month; ?>
        // type:'month'
    },
    tooltip: {
        // formatter: function() {
        //   return '<br> Jumlah Outlet: <b>' + Highcharts.numberFormat(this.point.y,2,',','.') + '</b><br>' + 'Value: <b>' + Highcharts.numberFormat(this.point.revenue,2,',','.') +'</b>';
        // },
        shared: true
    },
    series: <?php echo $trend; ?>,
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
</script>
<script>
// var trend_outlet_product = Highcharts.chart('trend_outlet_product', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: null
//     },
//     subtitle: {
//         text: null
//     },
//     xAxis: {
//         categories: nama_brand_do,
//         crosshair: true
//     },
//     yAxis: {
//         min: 0,
//         title: {
//             text: 'Jumlah Outlet'
//         }
//     },
//     tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
//     credits: {
//         enabled: false
//     }, 
//     plotOptions: {
//         column: {
//             pointPadding: 0.2,
//             borderWidth: 0
//         }
//     },
//     series: data_brand
// });
</script>

<script>
var d = new Date()

function formatDate(monthIndex, year) {
    var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
    ];    

    return monthNames[monthIndex] + '-' + year.toString().substr(-2);
}

$(document).ready( function () {
    var revenue = [];
    var cabang = $("select[name='area_table']").val();
    var channel = $("select[name='channel_table']").val();
    var periode = $("select[name='periode_table_outlet']").val();
    

    $(function() {
        $('input[name="daterange_otr"]').daterangepicker({
            opens: 'right',
            startDate: '2018-01-01',
            endDate: '2019-12-31',
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
        $('input[name="daterange_otr"]').val('2018-01-01 / 2019-12-31');

    });
    
    $(function() {
        $('input[name="daterange_outlet_product"]').daterangepicker({
            opens: 'right',
            startDate: '2018-01-01',
            endDate: '2019-12-31',
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
        $('input[name="daterange_outlet_product"]').val('2018-01-01 / 2019-12-31');

    });

    $('#lini').change(function(e){
        var selectvalue = $(this).val();

        $('#brand').html('<option value="">Loading...</option>').selectpicker('refresh');

        if (selectvalue == "") {
        //Display initial prompt in target select if blank value selected
            $('#brand').html('Brand');
        } else {
            //Make AJAX request, using the selected value as the GET
            $.ajax({
                url: "json_get_brand",
                type: 'POST',
                data: {
                    'table': 'usc_bst_sales_outlet_channel',
                    'lini' : selectvalue
                },
                success: function(output) {
                        //alert(output);
                        $('#brand').html(output).selectpicker('refresh');
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " "+ thrownError);
                }
            });
        }
    });

    var outlet_brand = $('#outlet_brand').DataTable({
        initComplete: function(){

        },
        oLanguage:{
            sProcessing: "loading..."
        },
        scrollCollapse: true,
        processing: true,
        ajax: {
            "url": "json_outlet_brand",
            "type": "POST",
            "async": "true",
            "data": {
                // "area_table": function() { return $('#area_table').val() },
                // "channel_table" : function() { return $('#channel_table').val() }
                "lini_table": function() { 
                    return $("select[name='lini']").val() == '' ? $('#lini_table').val():$("select[name='lini']").val()
                },
                "channel_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='channel']").val() == '' ? $('#channel_table').val():$("select[name='channel']").val() 
                },
                "brand_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='brand']").val() == '' ? $('#brand_table').val():$("select[name='brand']").val() 
                },
                "area_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='area']").val() == '' ? $('#area_table').val():$("select[name='area']").val() 
                }
            }
        },
        columns: [
            {
                "data": "nama_brand"
            },
            <?php foreach ($channels as $ch) : ?> 
                <?php foreach ($tahun as $key=>$value) : ?> {
                    "data": "<?= $ch->channel . "-" . $value; ?>"
                },
                <?php endforeach; ?>
            
            <?php endforeach; ?>
        ],
        order: [
            [0, 'asc']
        ],
        searching: false,
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });

    var outlet_area = $('#outlet_area').DataTable({
        initComplete: function(){

        },
        oLanguage:{
            sProcessing: "loading..."
        },
        scrollCollapse: true,
        processing: true,
        ajax: {
            "url": "json_outlet_area",
            "type": "POST",
            "async": "true",
            "data": {
                // "area_table": function() { return $('#area_table').val() },
                // "channel_table" : function() { return $('#channel_table').val() }
                "lini_table": function() { 
                    return $("select[name='lini']").val() == '' ? $('#lini_table').val():$("select[name='lini']").val()
                },
                "channel_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='channel']").val() == '' ? $('#channel_table').val():$("select[name='channel']").val() 
                },
                "brand_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='brand']").val() == '' ? $('#brand_table').val():$("select[name='brand']").val() 
                },
                "area_table" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='area']").val() == '' ? $('#area_table').val():$("select[name='area']").val() 
                }
                
                // "periode_table_outlet" : function(){
                //     return $("select[name='daterange_otr']").val() == '' ? "2018" :"2018";
                //     // $("select[name='daterange_otr']").val()
                // }
            }
        },
        columns: [
            {
                "data": "nama_kftd"
            },
            <?php foreach ($channels as $ch) : ?> 
                <?php foreach ($tahun as $key=>$value) : ?> {
                    "data": "<?= $ch->channel . "-" . $value; ?>"
                },
                <?php endforeach; ?>
            
            <?php endforeach; ?>
        ],
        order: [
            [0, 'asc']
        ],
        searching: false,
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });

    var outlet_transaction = $('#outlet_transaction').DataTable({
        initComplete: function(){

        },
        oLanguage:{
            sProcessing: "loading..."
        },
        scrollCollapse: true,
        processing: true,
        ajax: {
            "url": "json_outlet_transaction",
            "type": "POST",
            "async": "true",
            "data": {
                "lini_table": function() { 
                    return $("select[name='lini']").val() == '' ? $('#lini_table').val():$("select[name='lini']").val()
                },
                "channel_table_outlet" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='channel']").val() == '' ? $('#channel_table_outlet').val():$("select[name='channel']").val() 
                },
                "brand_table_outlet" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='brand']").val() == '' ? $('#brand_table_outlet').val():$("select[name='brand']").val() 
                },
                "kftd_table_outlet" : function() { 
                    // return $('#channel_table_outlet').val()
                    return $("select[name='area']").val() == '' ? $('#area_table_outlet').val():$("select[name='area']").val() 
                },
                
                "periode_table_outlet" : function(){
                    return $("select[name='daterange_otr']").val() == '' ? "2018" :"2018";
                    // $("select[name='daterange_otr']").val()
                }
            }
        },
        columns: [        
            {
                "data": "nama_brand"
            },
            <?php $quan = array('quantity','revenue'); ?>

            <?php for($m=1; $m<=12; $m++) : ?> 
                <?php foreach($quan as $cuan) : ?> {
                    "data" : "<?= date('M', mktime(0,0,0,$m, 10 )).'-'.$cuan; ?>"
                },   
                <?php endforeach; ?>
            <?php endfor; ?>
        ],
        order: [
            [0, 'asc']
        ],
        searching: false,
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'excel',
                text: 'Download Excel'
            },
        ]
    });

    $("#submit_otr").click(function(){
        // var provinsi = $("select[name='area']").val();
        var kftd = $("select[name='area']").val();

        var channel = $("select[name='channel']").val();
        var brand = $("select[name='brand']").val();
        var lini = $("select[name='lini']").val();
        var periode = $("input[name='daterange_otr'").val();

        var getPeriodRange = $("input[name='daterange_otr'").data('daterangepicker')
        var startDate = getPeriodRange.startDate._d
        var endDate = getPeriodRange.endDate._d
        console.log('tanggal'+ startDate + ' ' + endDate)
        console.log('tanggal'+ periode)
        if (brand == ""){
            brand = [];
        }

        outlet_transaction.ajax.reload();
        outlet_brand.ajax.reload();
        outlet_area.ajax.reload();

        $.ajax({
            url: 'json_outlet_otr',
            type: 'POST',
            data: {
                "kftd" : kftd,
                "channel" : channel,
                "periode" : periode,
                "brand" : brand,
                "lini" : lini
            },
            beforeSend: function(){
                // Show image container
                $("#loader_otr").show();
                $('#main-container').hide();
                document.getElementById("outlet_otr").style.display = "none";

            },
            error: function() {
                alert('Something is wrong');
            },
            success: function(data) {
                console.log(data);
                outlet_otr.showLoading();
                outlet_otr.update({
                    series: data.data
                }, true, true);
                outlet_otr.xAxis[0].update({
                    categories: data.nama_brand
                })
                outlet_otr.redraw();
                outlet_otr.hideLoading();
            },
            complete:function(data){
                // Hide image container
                $("#loader_otr").hide();
                document.getElementById("outlet_otr").style.display = "block";
            }
        });

        $.ajax({
            url: 'json_outlet_trend',
            type: 'POST',
            data: {
                // "provinsi" : provinsi,
                "kftd" : kftd,
                "channel" : channel,
                "periode" : periode,
                "lini" : lini
            },
            beforeSend: function(){
                $("#loader_trend").show();
                document.getElementById("trend_outlet").style.display = "none";

            },
            error: function(){
                alert('Something is wrong');
            },
            success: function(response) {
                console.log(response);
                trend_outlet.showLoading();
                trend_outlet.xAxis[0].update({
                    categories: response.bulan
                });
                trend_outlet.update({
                    series: response.data
                }, true, true);
                trend_outlet.redraw();
                trend_outlet.hideLoading();
            },
            complete:function(data){
                $("#loader_trend").hide();
                document.getElementById("trend_outlet").style.display = "block";
            }
        });

    });

                // Hide image container
    $("#submit_product").click(function(){
        var cabang = $("select[name='area_trend_product']").val();
        var channel = $("select[name='channel_trend']").val();
        var periode = $("input[name='daterange_outlet_product'").val();
        var brand = $("select[name='brand_trend']").val();

        if (brand == ""){
            brand = [];
        }

        $.ajax({
            url: 'json_outlet_product',
            type: 'POST',
            data: {
                "cabang" : cabang,
                "channel" : channel,
                "periode" : periode,
                "brand" : brand
            },
            beforeSend: function(){
                // Show image container
                $("#loader_product").show();
                document.getElementById("trend_outlet_product").style.display = "none";

            },
            error: function() {
                alert('Something is wrong');
            },
            success: function(data) {
                trend_outlet_product.showLoading();
                trend_outlet_product.update({
                    series: data.data
                }, true, true);
                trend_outlet_product.xAxis[0].update({
                    categories: data.nama_brand
                })
                trend_outlet_product.redraw();
                trend_outlet_product.hideLoading();
                
            },
            complete:function(data){
                // Hide image container
                $("#loader_product").hide();
                document.getElementById("trend_outlet_product").style.display = "block";
            }
        });
    });

    $("#submit_table").click(function(){
        var cabang = $("select[name='area_table']").val();
        var channel = $("select[name='channel_table']").val();
        outlet_brand.ajax.reload();
        outlet_area.ajax.reload();

    });

    $("#submit_table_outlet").click(function(){
        // var lini = $("select[name='lini_table']").val();
        // var channel = $("select[name='channel_table_outlet']").val();
        // var periode = $("select[name='periode_table_outlet']").val();
        // $("#tahun").text(periode);
        var cabang = $("select[name='area_trend_product']").val();
        var channel = $("select[name='channel_trend']").val();
        var periode = $("input[name='daterange_outlet_product'").val();
        var brand = $("select[name='brand_trend']").val();

        outlet_transaction.ajax.reload();
    });

});
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
