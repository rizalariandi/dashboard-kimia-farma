

    <script src="https://code.highcharts.com/maps/highmaps.js"></script>
    <!-- <script src="https://code.highcharts.com/maps/modules/exporting.js"></script> -->
    <script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>

    

    <style>
.row{
    text-align: center;
}
/* #box2{
} */
#box2 ul{
    /* list-style: none; */
    text-align: left;
}
#showChart{
		background-color: #5bb85b;
		color: #ffffff;
		padding: 10px;
		border: 0px;
		border-radius: 8px;
		font-size: 18px;
		outline: none;
		cursor: pointer;
    } 
#showChart1{
		background-color: #5bb85b;
		color: #ffffff;
		padding: 10px;
		border: 0px;
		border-radius: 8px;
		font-size: 18px;
		outline: none;
		cursor: pointer;
    } 
#container3{
		position: fixed;
		top: 50%;
		width:100%;
		text-align: center;
		margin-top: -41px;
  	}
.modal{
    height: 100%;
    width: 100%;
    resize: true;  
}
i#hide{
    cursor: pointer;
}
#flex {
    display: flex;
}
#selection{
    display: flex;
    padding-left: 35%;
}

</style>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right" id="selection"> 
<form style="padding-right: 3%; padding-top: 3%">

        Start From : <select id="bulan" name="bulan" class="bulan">
        <?php $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        foreach ($bulan as $b) {
            if ($b == $_SESSION['bulan']) {
                echo "<option value='$b' selected>$b</option>";
            } else {
                echo "<option value='$b'>$b</option>";
            }
        }?></select>

<!-- Tahun : <select id="tahun" name="tahun" class="tahun">
        <?php $tahun = ['2019'];
        foreach ($tahun as $t) {
            if ($t == $_SESSION['tahun']) {
                echo "<option value='$t' selected>$t</option>";
            } else {
                echo "<option value='$t'>$t</option>";
            }
        }?></select> -->
        
        Kategori Produk : <select id="produk" name="produk" class="produk">
        <?php $produk = ['fituno', 'batugin'];
        foreach ($produk as $p) {
            if ($p == $_SESSION['produk']) {
                echo "<option value='$p' selected>$p</option>";
            } else {
                echo "<option value='$p'>$p</option>";
            }
        }?></select>
</form>
    <div id="flex">
        <button id="btnSubmit" class="btn btn-success">Filter</button>
    </div>
</div>
</div>

        <!-- MAPS -->
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div id="maps" style="width: 100%; height: 400px">
                    </div>
                </div>
            </div>
        </div>
        <!-- MAPS -->
        <!-- <?php print_r($mapData)?> -->
        <!-- National view -->
        <div id="national-view">

            <div class="row">
                <div class="col-md-6">
                    <div class="white-box">
                        <div id="national-chart">
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="white-box">
                        <div id="national-chart-competitor">
                        
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="row">
                <div class="white-box">
                    <div id="popup-chart"></div>
                </div>
            </div> -->
            
            <div class="row">
                <div id="top_nasional" style="height: 400px; min-width: 100%; max-width: 600px; margin: 0 auto"></div>
            </div>
        <!-- POPUP BOX NASIONAL -->
            <div class="modal fade" id="top1" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Potensi Boosting</h4>
                        </div>
                        <div class="modal-body" id="popup-chart">
                        </div>
                        <div class="modal-footer">
                            <p style="text-align:left;"><b>REKOMENDASI</b><br><span id="rekomendasi-boosting-nasional"></span></p>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END POPUP BOX NASIONAL -->
        </div>
        <!-- end national view -->
            
        <!-- regional view -->
        <div style="display:none" id="main-container">
            <!-- FIRST BOX -->
            <!-- REKOMENDASI -->
            <!-- <div class="white-box" style="color: black;"> -->
            <div class="white-box">
                <div class="row" style="background-color: #093890;">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h3 style="color: white;"><b>REKOMENDASI</b><br>Area <span id="area-title"></span></h3>
                </div>
                <div class="col-md-2" style="color: white">
                    <i class="fas fa-fast-backward" style="display: none; font-size: 25px; padding-top:15px;" id="hide"></i>
                    <h5 class="back" style="color: white">Back to National View</h5>
                </div>
                
                </div>
                <div class="row">
                    <div class="alert alert-secondary" style="font-size: 20px" id="recomendation-box">
                    <ul style="font-size: 20px; list-style: none;">

                    </div>

                </div>
            </div>
            <!-- END REKOMENDASI  -->
            <!-- END FIRST BOX -->

            <!-- SECOND BOX -->
            <!-- <div class="white-box"> -->
                <div class="row" id="container">

                    <!-- KFA REVENUE VS TOP5 REVENUE -->
                    <div class="col-md-6" id="chart">
                        <div class="white-box">
                            <div id="topProduk1">
                            </div>
                        </div>
                    </div>
                    <!-- END KFA REVENUE VS TOP5 REVENUE -->

                    <!-- KFA REVENUE VS COMPETITOR -->
                    <div class="col-md-6" id="chart">
                        <div class="white-box">
                            <div id="vs-comp">
                            </div>
                        </div>
                    </div>
                    <!-- END KFA REVENUE VS COMPETITOR -->
                    
                </div>
            <!-- </div> -->
            <!-- END SECOND BOX -->

            <!-- THIRD BOX -->
            <!-- TOP5 KFA -->
            <div class="white-box">
                <div class="row" id="container5">
                    <div id="clickable-chart">
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <!-- END TOP5 KFA -->
            <!-- END THIRD BOX -->


            <!-- POP UP BOX -->
            <!-- POP UP CHART BOX REGIONAL-->
            <div class="modal fade" id="top" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Potensi Boosting</h4>
                        </div>
                        <div class="modal-body" id="pop-graph">
                        </div>
                        <div class="modal-footer">
                            <p style="text-align:left;"><b>REKOMENDASI</b><br><span id="rekomendasi-boosting"></span></p>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END POP UP CHART BOX REGIONAL -->
            <!-- POP UP BOX -->

<script>
                                var difference = <?php echo($difference)?>;

    // MAPS 

                            // Create the chart
                            var map_chart = Highcharts.mapChart('maps', {
                                chart: {
                                    map: 'countries/id/id-all'
                                },

                                title: {
                                    text: null
                                },

                                subtitle: {
                                    text: null
                                },

                                mapNavigation: {
                                    enabled: true,
                                    buttonOptions: {
                                        verticalAlign: 'bottom'
                                    }
                                },

                                colorAxis: {
                                    min: 0
                                },

                                series: [{
                                    data: <?php echo($mapData)?>,
                                    name: 'Revenue',
                                    cursor:'pointer',
                                    states: {
                                        hover: {
                                            color: '#BADA55'
                                        }
                                    },
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}'
                                    },
                                    point:{
                                        events:{
                                            click: function () {
                                                region(this.name);
                                                topFive(this.name);
                                                top5perArea(this.name);
                                                test(this.hc-key);
                                                // rekomendasi(this.name);
                                            }
                                        }
                                    }
                                }]
                            });
                            var boosting = <?php echo($difference)?>;
                            var produk = $("select[name='produk']").val();
                            function region(title) {
                                $('#area-title').html(title);
                                $('#area-container').show();
                                $('#main-container').show();
                                $('#national-view').hide();
                                $('#hide').show();
                                var produk = $("select[name='produk']").val();
                                

                                    var kfas = title.split(' ').join('_');
                                    var produk = $("select[name='produk']").val();
                                    var bulan = $("select[name='bulan]").val();
                                    var url_get = '<?php echo site_url('boosting/call_rekomendasi_data/')?>'+kfas+'/'+ produk+'/'+bulan;

                                        $.ajax({
                                            url: url_get,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong with recomendation');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                // alert(data);
                                                rekomendasi(data);
                                                
                                            }
                                        });
                                        function rekomendasi(data){
                                            if (data > 40){
                                $('#recomendation-box').text('Penjualan '+produk+' untuk area '+title+' tidak didominasi oleh beberapa KFA saja. Dengan demikian program boosting dapat dilakukan di semua KFA area.');
                                }else{
                                    $('#recomendation-box').text(' Penjualan '+produk+' untuk area '+title+' didominasi oleh maksimum 5 KFA saja. Dengan demikian program boosting untuk produk '+produk+' di area '+title+' dapat difokuskan di 5 KFA tersebut.')
                                }
                                        }
                            }


                            // NATIONAL VIEW
                            // TOP5 NASIONAL KIRI
                            var kf_chart = Highcharts.chart('national-chart', {
                                            chart:{
                                                type: 'spline'
                                            },

                                        title: {
                                            text: 'Revenue Nasional'
                                        },

                                        subtitle: {
                                            text: ''
                                        },

                                        yAxis: {
                                            title: {
                                                text: 'Revenue (Rp)'
                                            }
                                        },
                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'middle'
                                        },
                                        xAxis: {
                                            categories: <?php echo($month_categories)?>,
                                            type:'datetime'
                                        // categories: [?php echo join($ftanggal, ',') ?>]
                                        },

                                        series:<?php echo($natData)?>,
                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                },
                                                chartOptions: {
                                            },
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'bottom'
                                                    }
                                                }]
                                        }
                                        // console.log($nasional);
                                        });
// KOMPETITOR NASIONAL KANAN
                            var competitor_chart = Highcharts.chart('national-chart-competitor', {
                                    chart:{
                                        type: 'spline'
                                    },

                                    title: {
                                        text: 'Revenue Nasional Competitor'
                                    },

                                    subtitle: {
                                        text: ''
                                    },

                                    yAxis: {
                                        title: {
                                            text: 'Revenue (Rp)'
                                        }
                                    },
                                    legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'middle'
                                    },
                                    xAxis: {
                                        categories: <?php echo($month_categories)?>,
                                        type:'datetime'
                                    // categories: [?php echo join($ftanggal, ',') ?>]
                                    },

                                    series:<?php echo($natData_comp)?>,
                                    responsive: {
                                        rules: [{
                                            condition: {
                                                maxWidth: 500
                                            },
                                            chartOptions: {
                                                legend: {
                                                    layout: 'horizontal',
                                                    align: 'center',
                                                    verticalAlign: 'bottom'
                                                }
                                            }
                                        }]
                                    }
                                    // console.log($nasional);
                                    });

                            // END NATIONAL VIEW


                                    $("#btnSubmit").click(function(){
                                    
                                    var prod = $("select[name='produk']").val();
                                    var bulan = $("select[name='bulan']").val();
                                    var produk = prod.toLowerCase();
                                    var url_get_map = '<?php echo site_url('boosting/call_map_data/')?>'+ produk;
                                    var top_kfa_nasional = '<?php echo site_url('boosting/call_top_kfa_nasional/')?>'+ produk+'/'+bulan;
                                    var revenue_nasional_kfa = '<?php echo site_url('boosting/call_nasional_revenue/')?>'+ produk+'/'+bulan;
                                    var comp_revenue_nasional = '<?php echo site_url('boosting/call_nasional_competitor/')?>'+ produk+'/'+bulan;
                                    var url_get_month = '<?php echo site_url('boosting/month_cat/')?>'+bulan;
                                        $.ajax({
                                            url: url_get_map,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                map_chart.series[0].update({
                                                    data: JSON.parse(data)
                                                });
                                                map_chart.redraw();
                                                
                                            }
                                        });


                                        $.ajax({
                                            url: revenue_nasional_kfa,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                kf_chart.update({
                                                    series: JSON.parse(data)
                                                });
                                                // competitor_chart.setTitle({text: 'Revenue '+produk +' Top KFA Nasional'})
                                                kf_chart.redraw();

                                              
                                            }
                                        });

                                        



                                        $.ajax({
                                            url: comp_revenue_nasional,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong with competitor data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                competitor_chart.update({
                                                    series: JSON.parse(data)
                                                });
                                                // competitor_chart.setTitle({text: 'Revenue '+produk +' Top KFA Nasional'})
                                                competitor_chart.redraw();

                                              
                                            }
                                        });

                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                competitor_chart.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });


                                        $.ajax({
                                            url: top_kfa_nasional,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                kfa_nasional.series[0].update({
                                                    data: JSON.parse(data)
                                                });
                                                kfa_nasional.setTitle({text: 'Revenue '+produk +' Top KFA Nasional'})
                                                // kfa_nasional.redraw();


                                            }
                                        });

                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                kf_chart.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });
                                        
                                    });

                                    


                            
                            // REGION VIEW
                            function topFive(kfa){
                                // KFA REVENUE VS TOP5 REVENUE CHART KIRI
                                // alert(kfa);

                                var top5_chart = Highcharts.chart('topProduk1', {
                                            chart:{
                                                type: 'spline'
                                            },

                                        title: {
                                            text: 'Revenue KFA '+kfa+' vs Top-5 KFA Revenue 3 Bulan Terakhir'
                                        },

                                        subtitle: {
                                            text: ''
                                        },

                                        yAxis: {
                                            title: {
                                                text: 'Revenue (Rp)'
                                            }
                                        },
                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'middle'
                                        },
                                        xAxis: {
                                            categories: <?php echo($month_categories)?>,
                                            type:'datetime'
                                        // categories: [?php echo join($ftanggal, ',') ?>]
                                        },

                                        series:<?php echo($myData)?>,
                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                },
                                                chartOptions: {
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                        });

                                        
                                // END KFA REVENUE VS TOP5 REVENUE CHART

                                // KFA REVENUE VS COMPETITOR CHART REGIONAL KANAN
                                    var vs_comp = Highcharts.chart('vs-comp', {
                                        chart:{
                                            type: 'spline'
                                        },

                                    title: {
                                        text: 'Perbandingan '+$("select[name='produk']").val() +' Dengan Produk Pesaingnya di Aceh'
                                    },

                                    subtitle: {
                                        text: ''
                                    },

                                    yAxis: {
                                        title: {
                                            text: 'Revenue (Rp)'
                                        }
                                    },
                                    legend: {
                                        layout: 'vertical',
                                        align: 'right',
                                        verticalAlign: 'middle'
                                    },
                                    xAxis: {
                                        categories: <?php echo($month_categories)?>,
                                        type:'datetime'
                                            // categories: [?php echo join($tanggal, ',') ?>]
                                            },

                                    series: <?php echo($data_competitor_regional)?>,

                                    responsive: {
                                        rules: [{
                                            condition: {
                                                maxWidth: 500
                                            },
                                            chartOptions: {
                                                legend: {
                                                    layout: 'horizontal',
                                                    align: 'center',
                                                    verticalAlign: 'bottom'
                                                }
                                            }
                                        }]
                                    }

                                    });




                                    // FUNCTION FOR POP UP CHART
                                    // function modal(name){
                                    //     alert(name);
                                    //     var produk = $("select[name='produk']").val();
                                    //     var kfa=name;
                                    //     var potensi_boosting = '<?php echo site_url('boosting/potensi_boosting/')?>'+kfa+'/'+ produk;
                                    //     $.ajax({
                                    //         url: potensi_boosting,
                                    //         type: 'GET',
                                    //         error: function() {
                                    //             alert('Something is wrong');
                                    //         },
                                    //         success: function(data) {
                                    //             // console.log(data);
                                    //             pop_boosting.update({
                                    //                 series: JSON.parse(data)
                                    //             });
                                    //             pop_boosting.setTitle({text: 'Revenue '+ $("select[name='produk']").val() + ' KFA '+kfa+' vs Top-5 KFA Revenue Dua Minggu Terakhir'})
                                    //             pop_boosting.redraw();

                                    //             $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
                                    //         }
                                    //     });
                                    // }

                                    
                                    $("#btnSubmit").click(function(){
                                        // function() {
                                        //         region(this.name);
                                        //         topFive(this.name);
                                        //         top5perArea(this.name);
                                        //     }
                                        // filter();
                                    var kfas = kfa.split(' ').join('_');
                                    var produk = $("select[name='produk']").val();
                                    var bulan =$("select[name='bulan']").val();
                                    var url_get = '<?php echo site_url('boosting/call_top5_data/')?>'+kfas+'/'+ produk+'/'+bulan;
                                    var url_get_competitor = '<?php echo site_url('boosting/call_data_competitor_area/')?>'+kfas+'/'+ produk+'/'+bulan;
                                    var url_get_map = '<?php echo site_url('boosting/map_data/')?>'+ produk;
                                    var url_get_top5= '<?php echo site_url('boosting/call_top5_area/')?>'+kfas+'/'+ produk;
                                    var url_get_month = '<?php echo site_url('boosting/month_cat/')?>'+bulan;
                                    // var url_get_compt= '?php echo site_url('boosting/call_comp_data/')?>'+kfa +'/'+ produk;

                                        $.ajax({
                                            url: url_get,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                top5_chart.update({
                                                    series: JSON.parse(data)
                                                });
                                                top5_chart.setTitle({text: 'Revenue '+ $("select[name='produk']").val() + ' KFA '+kfa+' vs Top-5 KFA Revenue 3 bulan Terakhir'})
                                                // vs_comp.setTitle({text: 'Perbandingan '+$("select[name='produk']").val() +' Dengan Produk Pesaingnya di '+kfa})
                                                top5_chart.redraw();

                                                // $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
                                            }
                                        });

                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                top5_chart.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });

                                        $.ajax({
                                            url: url_get_competitor,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong with competitor data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                vs_comp.update({
                                                    series: JSON.parse(data)
                                                });
                                                vs_comp.setTitle({text: 'Perbandingan '+ $("select[name='produk']").val() + ' dengan produk pesaingnya di '+kfa})
                                                vs_comp.redraw();

                                                // $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
                                            }
                                        });


                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                vs_comp.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });


                                        

                                    });

                                    // AJAX TRIGGER FOR AREA FILTER
                                    var kfas = kfa.split(' ').join('_'); 
                                    var produk = $("select[name='produk']").val();
                                    var bulan =$("select[name='bulan']").val();
                                    var url_get = '<?php echo site_url('boosting/call_top5_data/')?>'+kfas+'/'+ produk+'/'+bulan;
                                    var url_get_competitor = '<?php echo site_url('boosting/call_data_competitor_area/')?>'+kfas+'/'+ produk+'/'+bulan;
                                    var url_get_map = '<?php echo site_url('boosting/map_data/')?>'+ produk;
                                    var url_get_top5= '<?php echo site_url('boosting/call_top5_area/')?>'+kfas+'/'+ produk;
                                    var url_get_month = '<?php echo site_url('boosting/month_cat/')?>'+bulan;
                                    // var url_get_compt= '?php echo site_url('boosting/call_comp_data/')?>'+kfa +'/'+ produk;

                                        $.ajax({
                                            url: url_get,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                top5_chart.update({
                                                    series: JSON.parse(data)
                                                });
                                                top5_chart.setTitle({text: 'Revenue '+ $("select[name='produk']").val() + ' KFA '+kfa+' vs Top-5 KFA Revenue 3 bulan Terakhir'})
                                                // vs_comp.setTitle({text: 'Perbandingan '+$("select[name='produk']").val() +' Dengan Produk Pesaingnya di '+kfa})
                                                top5_chart.redraw();

                                                // $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
                                            }
                                        });

                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                top5_chart.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });

                                        $.ajax({
                                            url: url_get_competitor,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong with competitor data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                vs_comp.update({
                                                    series: JSON.parse(data)
                                                });
                                                vs_comp.setTitle({text: 'Perbandingan '+ $("select[name='produk']").val() + ' dengan produk pesaingnya di '+kfa})
                                                vs_comp.redraw();

                                                // $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
                                            }
                                        });


                                        $.ajax({
                                            url: url_get_month,
                                            type: 'GET',
                                            error: function() {
                                                alert('Something is wrong top kfa data');
                                            },
                                            success: function(data) {
                                                // console.log(data);
                                                vs_comp.xAxis[0].update({
                                                    categories: JSON.parse(data)
                                                });
                                                // kfa_nasional.redraw();


                                            }
                                        });

                                 
                                // KFA REVENUE VS COMPETITOR CHART
                            } //close bracket for topKFA function


                            function top5perArea(kfa){
                                var top5_kfa = Highcharts.chart('clickable-chart', {
                                        chart: {
                                            type: 'bar'
                                        },
                                        title: {
                                            text: 'KFA  yang potensi di boosting'
                                        },
                                        xAxis: {
                                            type: 'category',
                                            title: {
                                            text: null
                                            },
                                            min: 0,
                                            // max: 6,
                                            tickLength: 0
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'Revenue'
                                            }
                                        },
                                        legend: {
                                            reversed: true
                                        },
                                        plotOptions: {
                                            series: {
                                                stacking: 'normal',
                                                            cursor: 'pointer',
                                                point: {
                                                    events:{
                                                        click: function() {
                                                            pop_pie(this.name);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                                        series: [{
                                            data: <?php echo($topKFAR)?>,
                                            name: 'Revenue',
                                            showInLegend: false
                                        }]
                                    });




                                    var produk = $("select[name='produk']").val();
                                    var kfas = kfa.split(' ').join('_');
                                    // var kfas = kfa.replace(' ', '_');
                                    // var link = str_replace(' ', '', $sent);
                                    var url_get_top5= '<?php echo site_url('boosting/call_top5_area/')?>'+kfas+'/'+produk;

                                    $.ajax({
                                        url: url_get_top5,
                                        type: 'GET',
                                        error: function() {
                                            alert('Something is wrong');
                                        },
                                        success: function(data) {
                                            // console.log(data);
                                            top5_kfa.series[0].update({
                                                data: JSON.parse(data)
                                            });
                                            top5_kfa.setTitle({text: 'KFA di '+kfa+' yang Potensi Diboosting'})
                                            top5_kfa.redraw();
                                        }
                                    });
                            }

                            $("#btnSubmit").click(function(){
                                    // FIX THIS
                                    var kfas = kfa.split(' ').join('_');
                                    // var kfas = kfa.replace(' ', '_');
                                    var produk = $("select[name='produk']").val();
                                    // var link = kfa+'/'+produk;
                                    var url_get_top5= '<?php echo site_url('boosting/call_top5_area/')?>'+kfas+'/'+$produk;

                                    $.ajax({
                                        url: url_get_top5,
                                        type: 'GET',
                                        error: function() {
                                            alert('Something is wrong with this');
                                        },
                                        success: function(data) {
                                            // console.log(data);
                                            top5_kfa.series[0].update({
                                                data: JSON.parse(data)
                                            });
                                            top5_kfa.setTitle({text: 'TOP 5 KFA di '+kfa+' yang Potensi Diboosting'})
                                            top5_kfa.redraw();
                                        }
                                    });
                                    });

                           
                            // BACK TO NATIONAL VIEW
                            $(document).ready(function(){
                                $('#hide').click(function(){
                                    $('#national-view').show();
                                    $('#main-container').hide();
                                    $('#area-container').hide();
                                })
                            })
    
    // END MAPS

        var kfa= '<span id="kfa"></span>';


    // TOP NASIONAL
    // window.onload = function () {
       var kfa_nasional= Highcharts.chart('top_nasional', {
            chart: {
                type: 'bar',
                marginLeft: 150,
            },
            title: {
                text: 'Revenue '+$("select[name='produk']").val()+' top KFA Nasional'
            },
            xAxis: {
                type: 'category',
                title: {
                text: null
                },
                min: 0,
                max: 6,
                scrollbar: {
                enabled: true
                },
                tickLength: 0
            },
            yAxis: {
                min: 0,
                // max: 100000000,
                title: {
                text: '',
                }
            },
            plotOptions: {
                bar: {
                dataLabels: {
                    enabled: true
                }
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
            series: [{
                name:'Revenue',
                cursor: 'pointer',
                data: <?php echo($topKFA)?>,
                point:{
                events:{
                    click: function() {
                        popup(this.name );
                    }
                }
            }
            }],
            }, 
            function(chart) {
            Highcharts.addEvent(
                chart.container,
                document.onmousewheel === undefined ? 'DOMMouseScroll' : 'mousewheel',
                function(e) {
                var delta,
                    xAxis = chart.xAxis[0],
                    range = xAxis.max - xAxis.min,
                    newMin, newMax;

                e = chart.pointer.normalize(e);

                // Firefox uses e.detail, WebKit and IE uses wheelDelta
                delta = e.detail || -(e.wheelDelta / 120);

                if (chart.isInsidePlot(e.chartX - chart.plotLeft, e.chartY - chart.plotTop)) {
                    newMin = xAxis.min + range * delta;
                    newMax = xAxis.max + range * delta;

                    if (newMin < xAxis.dataMin) {
                    newMin = xAxis.dataMin;
                    newMax = newMin + range;
                    }

                    if (newMax > xAxis.dataMax) {
                    newMax = xAxis.dataMax;
                    newMin = newMax - range;
                    }

                    xAxis.setExtremes(
                    newMin,
                    newMax,
                    true,
                    false
                    );
                }
                e.preventDefault();
                }
            )
            });



                        // }
    // END TOP NASIONAL


    var popchart_nasional= Highcharts.chart('popup-chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },     
            series: [{
                data: <?php echo($top_nasional_data) ?>,
                name: 'Persentase',
                colorByPoint: true
                

           }]
        });
    function popup(cate){

        var categ = cate.split(' ').join('_');
        var produk = $("select[name='produk']").val();
        var url_get_pie_data = '<?php echo site_url('boosting/call_piechart_data/')?>'+categ+'/'+produk;
        var rekomendasi_boosting_regional = '<?php echo site_url('boosting/call_rekomendasi_boosting_regional/')?>'+categ+'/'+ produk;
            $.ajax({
                url: url_get_pie_data,
                type: 'GET',
                error: function() {
                    alert('Something is wrong with pie chart data');
                },
                success: function(data) {
                    
                    // console.log(data);
                    console.log(categ);
                    popchart_nasional.series[0].update({
                    data:JSON.parse(data)
                    });
                    popchart_nasional.setTitle({text: cate});
                    popchart_nasional.redraw();
                }
            });

         
            $.ajax({
                url: rekomendasi_boosting_regional,
                type: 'GET',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {
                    console.log(data);
                    boost(data);
                    // rekomendasi_boosting.update(data);
                    // alert(rekomendasi_boosting);   
                    

                }
            });
            function boost(data){
                // var dat = data.toFixed();
            $('#top1').modal(); 
            if(data>1){
            $('#rekomendasi-boosting-nasional').text('Produk dapat diboosting sebesar '+data+' %');
            }
            if(data<1){
                $('#rekomendasi-boosting-nasional').text('Produk tidak perlu diboosting ');
            }
            }
        
    };


    var popchart_regional= Highcharts.chart('pop-graph', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Persentase',
                colorByPoint: true,
                data: <?php echo($top_regional_data)?>

            }]
        });
        
        var rekomendasi_boosting=[];


    function pop_pie(cate){
        var categ = cate.split(' ').join('_');
        var produk = $("select[name='produk']").val();
        var url_get_top_regional = '<?php echo site_url('boosting/call_piechart_data_regional/')?>'+categ+'/'+ produk;
        var rekomendasi_boosting_regional = '<?php echo site_url('boosting/call_rekomendasi_boosting_regional/')?>'+categ+'/'+ produk;

            $.ajax({
                url: url_get_top_regional,
                type: 'GET',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {
                    // console.log(data);
                    // console.log(categ);
                    // alert(rekomendasi_boosting);
                    popchart_regional.series[0].update({
                        data: JSON.parse(data)
                    });
                    popchart_regional.setTitle({text: cate})
                    popchart_regional.redraw();
                }
            });

            $.ajax({
                url: rekomendasi_boosting_regional,
                type: 'GET',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {
                    console.log(data);
                    boost(data);
                    // rekomendasi_boosting.update(data);
                    // alert(rekomendasi_boosting);   
                    

                }
            });
            function boost(data){
                // var dat = data.toFixed();
            $('#top').modal(); 
            if(data>1){
            $('#rekomendasi-boosting').text('Produk dapat diboosting sebesar '+data+' %');
            }
            if(data<1){
                $('#rekomendasi-boosting').text('Produk tidak perlu diboosting ');
            }
            }

            // $('body').on('hidden.bs.modal', '.modal', function (e) {
            //     $(e.target).removeData('bs.modal');
         
            // });
    };

</script>
