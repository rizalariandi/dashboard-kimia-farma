<script>
// var f=iQuery.noConflict();

window.onload = function () {
Highcharts.chart('clickable-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Top 5 KFA  yang potensi di boosting test'
    },
    xAxis: {
        categories: ['KFA Kabanjahe', 'KFA TB Simatupang', 'KFA Berastagi', 'KFA Binjei', 'KFA Stabat']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Revenue'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
                        cursor: 'pointer',
            point: {
                events: {
                    click: function (category, name, event) {

                        // alert(this.category);
                        
                            if (this.category == 'KFA Kabanjahe'){
                                $('#top1').modal();
                            }else if(this.category == 'KFA TB Simatupang'){
                                $('#top2').modal();
                            }else if (this.category == 'KFA Berastagi'){
                                $('#top3').modal();
                            }else if (this.category == 'KFA Binjei'){
                                $('#top4').modal();
                            }else if (this.category == 'KFA Stabat'){
                                $('#top5').modal();
                            }
                    }
                }
            }
        }
    },
    series: [{
        name: 'Revenue',
        data: [6123383077, 2584516642, 2522726447, 2495588355, 2495588355],
        showInLegend: false
    }]
});

}

</script>