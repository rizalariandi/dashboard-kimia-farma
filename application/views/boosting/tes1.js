$.ajax({
    url: url_get,
    type: 'GET',
    error: function() {
        alert('Something is wrong');
    },
    success: function(data) {
        // console.log(data);
        top5_chart.update({
            series: JSON.parse(data)
        });
        top5_chart.setTitle({text: 'Revenue '+ $("select[name='produk']").val() + ' KFA '+kfa+' vs Top-5 KFA Revenue Dua Minggu Terakhir'})
        vs_comp.setTitle({text: 'Perbandingan '+$("select[name='produk']").val() +' Dengan Produk Pesaingnya di'+kfa})
        top5_chart.redraw();

        $('#recomendation-box').text('Usaha boosting sales ['+$("select[name='produk']").val()+'] dapat difokuskan di 5 KFA teratas ');
    }
});