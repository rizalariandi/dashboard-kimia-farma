<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
    <div class="row">
        <div class="col-12">
            <div class="col-md-12">
                <h1>
                    <div class="float-right"><a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#Modal_Add"><span class="fa fa-plus"></span> Tambah Produk</a></div>
                </h1>
            </div>
            
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr>
                        <th style="color:black;">Produk KF</th>
                        <th style="color:black;">Produk Kompetitor</th>
                        <th style="text-align: right; color:black;">Actions</th>
                    </tr>
                </thead>
                <tbody id="show_data">
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>

	<!-- MODAL ADD -->
<form>
    <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Produk KF</label>
                    <div class="col-md-10">
                        <input type="text" name="produk_id" id="produk_id" class="form-control" placeholder="Produk KF">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Produk Kompetitor</label>
                    <div class="col-md-10">
                        <input type="text" name="nama_produk" id="nama_produk" class="form-control" placeholder="Produk Kompetitor">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="button" type="submit" id="btn_save" class="btn btn-primary">Simpan</button>
            </div>
        </div>
        </div>
    </div>
    </form>
<!--END MODAL ADD-->

<!-- MODAL EDIT -->
<form>
    <div class="modal fade" id="Modal_Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Produk KF</label>
                    <div class="col-md-10">
                        <input type="text" name="produk_id_edit" id="produk_id_edit" class="form-control" placeholder="Produk KF" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">Produk Kompetitor</label>
                    <div class="col-md-10">
                        <input type="text" name="nama_produk_edit" id="nama_produk_edit" class="form-control" placeholder="Produk Kompetitor">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="id_edit" id="id_edit" class="form-control">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="button" type="submit" id="btn_update" class="btn btn-primary">Update</button>
            </div>
        </div>
        </div>
    </div>
    </form>
<!--END MODAL EDIT-->

<!--MODAL DELETE-->
    <form>
    <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Produk</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <strong>Apakah yakin akan menghapus data ini?</strong>
            </div>
            <div class="modal-footer">
            <input type="hidden" name="id" id="id_delete" class="form-control">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Hapus</button>
            </div>
        </div>
        </div>
    </div>
    </form>
<!--END MODAL DELETE-->

<script type="text/javascript">
	$(document).ready(function(){
		show_product();	//call function show all product
		
		$('#mydata').dataTable();
		 
		//function show all product
		function show_product(){
		    $.ajax({
		        type  : 'ajax',
		        url   : '<?php echo site_url('boost_master_produk/product_data')?>',
		        async : false,
		        dataType : 'json',
		        success : function(data){
		            var html = '';
		            var i;
		            for(i=0; i<data.length; i++){
		                html += '<tr>'+
		                  		'<td>'+data[i].produk_id+'</td>'+
		                        '<td>'+data[i].nama_produk+'</td>'+
		                        '<td style="text-align:right;">'+
                                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-id="'+data[i].id+'" data-produk_id="'+data[i].produk_id+'" data-nama_produk="'+data[i].nama_produk+'">Edit</a>'+' '+
                                    '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-id="'+data[i].id+'">Hapus</a>'+
                                '</td>'+
		                        '</tr>';
		            }
		            $('#show_data').html(html);
		        }

		    });
		}

        //Save product
        $('#btn_save').on('click',function(){
            var produk_id = $('#produk_id').val();
            var nama_produk = $('#nama_produk').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('boost_master_produk/save')?>",
                dataType : "JSON",
                data : {produk_id:produk_id , nama_produk:nama_produk},
                success: function(data){
                    $('[name="produk_id"]').val("");
                    $('[name="nama_produk"]').val("");
                    $('#Modal_Add').modal('hide');
                    show_product();
                }
            });
            return false;
        });

        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
            var id = $(this).data('id');
            var produk_id = $(this).data('produk_id');
            var nama_produk = $(this).data('nama_produk');
            
            $('#Modal_Edit').modal('show');
            $('[name="id_edit"]').val(id);
            $('[name="produk_id_edit"]').val(produk_id);
            $('[name="nama_produk_edit"]').val(nama_produk);
        });

        //update record to database
         $('#btn_update').on('click',function(){
            var id = $('#id_edit').val();
            var produk_id = $('#produk_id_edit').val();
            var nama_produk = $('#nama_produk_edit').val();

            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('boost_master_produk/update')?>",
                dataType : "JSON",
                data : {id:id, produk_id:produk_id, nama_produk:nama_produk},
                success: function(data){
                    $('[name="produk_id_edit"]').val("");
                    $('[name="nama_produk_edit"]').val("");
                    $('#Modal_Edit').modal('hide');
                    show_product();
                }
            });
            return false;
        });

        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
            var id = $(this).data('id');
            
            $('#Modal_Delete').modal('show');
            $('[name="id"]').val(id);
        });

        //delete record to database
         $('#btn_delete').on('click',function(){
            var id = $('#id_delete').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('boost_master_produk/delete')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="id_delete"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_product();
                }
            });
            return false;
        });

	});

</script>

