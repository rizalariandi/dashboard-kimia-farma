<script src="<?= base_url()?>asset/js/highcharts-more.js"></script>
<script src="<?= base_url()?>asset/js/data.js"></script>
<!-- <script src="<?= base_url()?>asset/js/export-data.js"></script> -->
<script src="<?= base_url()?>asset/js/accessibility.js"></script>   
<script src="<?= base_url()?>asset/plugins/jquery.table2excel.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">


<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #ABD6DFFF;
  text-align: left;
  padding: 8px;
}

th{
    background-color: #8BBEE8FF;
    text-align: center;
}

tr:nth-child(even) {
  background-color: #b5d8f4;
}

.selectpicker{
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    /* or 143% */

    display: flex;
    align-items: center;

    color: #4F4F4F;
}

</style>

<!-- header area -->
    <div class="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?= $title ?></h4>
    </div>
    </div>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
        <form method="post"> 
            <input type="text" class="btn btn-default" value="<?= $data_range; ?>" name="period" placeholder="Periode"/>
            
            Area : <select id="area" name="area" class="area">
            <?php foreach ($areas as $a) :?>
            <option value='<?= $a->provinsi;?>' <?= ($a->provinsi == $area)?'selected':'';?>><?= $a->provinsi;?></option>
            <?php endforeach; ?>
            </select>

            Kategori Produk : <select id="produk" name="produk" title="Produk" class="selectpicker" data-live-search="true" data-width="fit">
            <?php  foreach ($produks as $p) :?>
                    <option value='<?= $p->produk;?>' <?= ($p->produk == $produk)?'selected':'';?>><?= $p->produk;?></option>
                <?php endforeach;?>
            </select>

            <select id="filter" name="filter">
                <option value="quantity" <?= ($opsi == 'quantity')?'selected':'';?>>quantity</option>
                <option value="value" <?= ($opsi == 'value')?'selected':'';?>>value</option>
            </select>

            <button id="btnSubmit" type="submit" class="btn btn-success">Filter</button>
        </form>
    </div>
</div>
<!-- header area -->

<div class="container-fluid">  
    <!-- bcg matrix area -->

    <div class="row">
        <h3 style="text-align: center;"><b><?php echo $notif;?></b></h3>

        <div class="white-box">    
            <div id="bcg_matrix"></div>
        </div>        
    </div>
    <div class="row">
        <div class="white-box">
            <p>
                <a id="download_channel" class="btn btn-success">Download as XLS</a>
            </p>

            <table id="table-channel" style="display:none;" class="table table-striped table-bordered table-hover table-header-fixed sortable">
                <thead>
                    <tr>
                        <th>Produk</th>
                        <th>Channel</th>
                        <th>Area</th>
                        <th>Periode</th>
                        <th>Quantity</th>
                        <th>Value</th>
                        </tr>
                </thead>
                <tbody>
                    <?php foreach ($export_channel as $h) { 
                        if ($h['channel'] == '') {
                            continue;
                        } else {
                        ?>

                        <tr>
                            <td><?php echo $h['produk']; ?></td>
                            <td><?php echo $h['channel']; ?></td>
                            <td><?php echo $h['provinsi']; ?></td>
                            <td><?php echo date('F', mktime(0, 0, 0, $h['bulan'], 10)); ?></td>
                            <td><?php echo $h['quantity']?></td>
                            <td><?php echo $h['sales'];?></td>
                        </tr> 
                        <?php } ?>
                    <?php } ?>
                </tbody>

            </table>
        </div>
    
            <div class="white-box">
            <h3><b>Summary</b></h3>
            <ul style="list-style-type:none">
                <li><b>Star (Kanan Atas)</b>: Produk dengan high-share dan high-growth. Berpotensi untuk menjadi pemimpin pasar dan profit generator.</li>
                <li><b>Cash Cow (Kanan Bawah)</b>: Produk dengan high-share dan low-growth. Merupakan produk yang menjadi sumber profit bagi organisasi.</li>
<li><b>Question Mark (Kiri Atas)</b>: Produk dengan low-share dan high-growth. Produk ini masih menghasilkan profit lebih sedikit dari pada biaya marketing yang diperlukan. Produk di sini masih memerlukan biaya marketing yang intensif sebelum menjadi produk yang profitable.</li>
<li><b>Dog/Pet (Kiri Bawah)</b>: Produk dengan low-share dan low-growth. Produk ini hampir tidak menghasilkan profit bagi organisasi. Produk ini dapat dilikuidasi dari portofolio produk organisasi.</li>
            </ul>
            </div>
    </div>
    
</div>

<script>
   
var bcg_chart = Highcharts.chart('bcg_matrix', {
    chart: {
        backgroundColor: '#E7EBE0FF',
        type: 'bubble',
        plotBorderWidth: 1,
        zoomType: 'xy'
    },
    legend: {
        enabled: false
    },
    title: {
        text: 'Strategi Marketing Channel berdasarkan produk ' + $("select[name='produk']").val() + ' dan Pangsa-Pertumbuhan Pasar'
    },
    xAxis: {
        min : -20,
        gridLineWidth: 1,
        title: {
            text: 'Relative Market Share'
        },
        labels: {
            format: '{value}'
        },
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_x; ?>,
            zIndex: 3
        }]
    },
    yAxis: {
        min : -20,
        startOnTick: false,
        endOnTick: false,
        title: {
            text: 'Market Growth'
        },
        labels: {
            format: '{value}'
        },
        maxPadding: 0.2,
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_y; ?>,
            zIndex: 3
        }]
    },
      
    credits: {
        enabled: false
    },
    plotOptions: {
        visible: true,
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        },
        bubble: {
            tooltip: {
                headerFormat: '<b>{point.key}</b><br>',
                pointFormat: 'Market Share : {point.x} % <br/> Product Growth : {point.y} %'

            }
        }
            
    },
    series: [{
        data: <?php echo $bcg; ?>
    }]
});
</script>

<script>
$("#download_channel").click(function(){
    $("#table-channel").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet 1",
        filename: "data_channel" //do not include extension
    });
});

$(function() {
        $('input[name="period"]').daterangepicker({
            opens: 'right',
            startDate: '<?= $startDate ?>',
            endDate: '<?= $endDate ?>',
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
        // $('input[name="period"]').val('2019-07-01 / 2019-12-31');
        $('input[name="period"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));
        });

    });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>