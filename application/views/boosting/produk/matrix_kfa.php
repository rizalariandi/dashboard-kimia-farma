<script src="<?= base_url()?>asset/js/accessibility.js"></script>
<script src="<?= base_url()?>asset/plugins/jquery.table2excel.min.js"></script>
<script src="<?= base_url()?>asset/js/highcharts-more.js"></script>
<script src="<?= base_url()?>asset/js/data.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">


<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #ABD6DFFF;
  text-align: left;
  padding: 8px;
}

th{
    background-color: #8BBEE8FF;
    text-align: center;
}

tr:nth-child(even) {
    background-color: #b5d8f4;
}

.selectpicker{
    font-family: Roboto;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    /* or 143% */

    display: flex;
    align-items: center;

    color: #4F4F4F;
}

</style>

<!-- header area -->
<div class="row">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?= $title ?></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
        <form method="post"> 
            <input type="text" class="btn btn-default" value="<?= $data_range; ?>" name="period" placeholder="Periode"/>

            Area : <select id="area" name="area" class="area">
            <?php foreach ($areas as $a) :?>
            <option value='<?= $a->provinsi;?>' <?= ($a->provinsi == $area)?'selected':'';?>><?= $a->provinsi;?></option>
            <?php endforeach; ?>
            </select>

            Kategori Produk : <select id="produk" name="produk[]" title="Produk" class="selectpicker" multiple data-live-search="true" data-width="fit">
            <?php  foreach ($produks as $p) :?>
                    <option value='<?= $p->produk_id;?>' <?= ($p->produk_id == $produk)?'selected':'';?>><?= $p->produk_id;?></option>
                <?php endforeach;?>
            </select>
            Produk kompetitor : <select id="kompetitor" name="kompetitor[]" title="Kompetitor" class="selectpicker" multiple data-live-search="true" data-width="fit">
            <?php  foreach ($kompetitors as $k) :?>
                    <option value='<?= $k->produk;?>' <?= ($k->produk == $kompetitor)?'selected':'';?>><?= $k->produk;?></option>
                <?php endforeach;?>
            </select>
            <select id="filter" name="filter">
                <option value="quantity" <?= ($opsi == 'quantity')?'selected':'';?>>quantity</option>
                <option value="value" <?= ($opsi == 'value')?'selected':'';?>>value</option>
            </select>

            <button id="btnSubmit" type="submit" class="btn btn-success">Filter</button>
        </form>
    </div>
</div>
<!-- header area -->

<div class="container-fluid">  
    <!-- bcg matrix area -->
</div>
    <div class="row">
        <h3 style="text-align: center;"><b><?php echo $notif;?></b></h3>

        <div class="white-box">    
            <div id="bcg_matrix"></div>
        </div>        
    </div>
    <!-- bcg matrix area -->
    
    <!-- rekomendasi area BCG-->
    <div class="row">
        <div class="white-box">
        <p>
            <a id="download" class="btn btn-success">Download as XLS</a>
        </p>

            <table id="table-brand" style="display: none;" class="table table-striped table-bordered table-hover table-header-fixed sortable">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th>Area</th>
                        <th>Periode</th>
                        <th>Quantity</th>
                        <th>Value</th>
                        </tr>
                </thead>
                <tbody>
                    <?php foreach ($export_product as $h) { 
                        ?>
                        <tr>
                            <td><?php echo $h['produk']; ?></td>
                            <td><?php echo $h['provinsi']; ?></td>
                            <td><?php echo date('F', mktime(0, 0, 0, $h['bulan'], 10)); ?></td>
                            <td><?php echo $h['quantity']?></td>
                            <td><?php echo $h['sales'];?></td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
        </div>
        <div class="white-box" style="text-align:center;">
            <h4 id="title_rekomendasi"><b></b></h4>
            <ul style="list-style-type:none">
            <li>
                <!-- <?= $bcg_rekomendasi;?> -->
            </li>
            
            </ul>
        </div>
    </div>
    <!-- rekomendasi area BCG Produk -->

    <!-- bcg matrix detil produk -->
    <div class="row">
        <div class="white-box">    
            <div id="bcg_matrix_detil"></div>
        </div>        
    </div>
    <!-- bcg matrix detil produk -->
    
    <!-- rekomendasi strategi detil produk -->
    <div class="row">
    <div class="white-box">
        <p>
            <a id="download_variant" class="btn btn-success">Download as XLS</a>
        </p>

            <table id="table-variant" style="display:none;" class="table table-striped table-bordered table-hover table-header-fixed sortable">
                <thead>
                    <tr>
                        <th>Nama Varian Produk</th>
                        <th>Area</th>
                        <th>Periode</th>
                        <th>Quantity</th>
                        <th>Value</th>
                        </tr>
                </thead>
                <tbody>
                    <?php foreach ($export_variant as $h) { 
                        ?>
                        <tr>
                            <td><?php echo $h['produk']; ?></td>
                            <td><?php echo $h['provinsi']; ?></td>
                            <td><?php echo date('F', mktime(0, 0, 0, $h['bulan'], 10)); ?></td>
                            <td><?php echo $h['quantity']?></td>
                            <td><?php echo $h['sales'];?></td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
        </div>
        <div class="white-box" style="text-align:center;">
            <h4 id="title_rekomendasi_detil"><b>Rekomendasi Strategi</b></h4>
            <ul style="list-style-type:none">
                <?= $bcg_detils_rekomendasi;?>
            </ul>
        </div>
    </div>
    <!-- rekomendasi strategi detil produk -->

    <div class="row">
        <div class="white-box">    
            <div id="sell_out"></div>
        </div>

        <div class="white-box">
        <p>
            <a id="download_sellout" class="btn btn-success">Download as XLS</a>
        </p>

            <table id="table-sellout" style="display:none;" class="table table-striped table-bordered table-hover table-header-fixed sortable">
                <thead>
                    <tr>
                        <th>Nama KFA</th>
                        <th>Bulan</th>
                        <th><?php echo ($opsi == 'value') ? 'Value' : 'Quantity'; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data_raw as $h) { 
                        ?>
                        <tr>
                            <td><?php echo $h['nama_kfa']; ?></td>
                            <td><?php echo $h['bulan']; ?></td>
                            <td><?php echo $h['total_sales']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>

            </table>
        </div>        
    </div>

</div>

<script>
var bcg_chart = Highcharts.chart('bcg_matrix', {
    chart: {
        type: 'bubble',
        backgroundColor: '#E7EBE0FF',
        plotBorderWidth: 1,
        zoomType: 'xy'
    },
    legend: {
        enabled: false
    },
    title: {
        text: 'Strategi Marketing Produk ' + $("select[name='produk']").val() + ' berdasarkan Pangsa-Pertumbuhan Pasar'
    },
    credits: {
        enabled: false
    },
    xAxis: {
        // min : -10,
        gridLineWidth: 1,
        title: {
            text: 'Relative Market Share (%)'
        },
        labels: {
            format: '{value}'
        },
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_x; ?>,
            zIndex: 3
        }]
    },
    yAxis: {
        // min : -10,
        startOnTick: false,
        endOnTick: false,
        title: {
            text: 'Market Growth'
        },
        labels: {
            format: '{value}'
        },
        maxPadding: 0.2,
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_y; ?>,
            zIndex: 3
        }]
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                color: 'black',
                format: '{point.name}'
            }
        },
        bubble: {
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: 'Market Share : {point.x} % <br/> Product Growth : {point.y}'
                }
        }
    },
    series: [{
        data: <?php echo $bcg; ?>
    }]
});

var bcg_chart_detil = Highcharts.chart('bcg_matrix_detil', {
    chart: {
        type: 'bubble',
        backgroundColor: '#E7EBE0FF',
        plotBorderWidth: 1,
        zoomType: 'xy'
    },
    legend: {
        enabled: false
    },
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    xAxis: {
        // min : -10,
        gridLineWidth: 1,
        title: {
            text: 'Relative Market Share (%)'
        },
        labels: {
            format: '{value}'
        },
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_detils_x; ?>,
            zIndex: 3
        }]
    },
    yAxis: {
        // min : -10,
        startOnTick: false,
        endOnTick: false,
        title: {
            text: 'Market Growth'
        },
        labels: {
            format: '{value}'
        },
        maxPadding: 0.2,
        plotLines: [{
            color: 'black',
            dashStyle: 'solid',
            width: 2,
            value: <?= $bcg_detils_y; ?>,
            zIndex: 3
        }]
    },
    plotOptions: {
        series: {
            color: '#97BC62FF',
            dataLabels: {
                color: 'black',
                enabled: true,
                format: '{point.name}'
            }
        },
        bubble: {
                tooltip: {
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: 'Market Share : {point.x} % <br/> Product Growth : {point.y}'

                }
        }
    },
    series: [{
        data: <?php echo $bcg_detils ?>
    }]
});

var sell_out = Highcharts.chart('sell_out', {
    chart: {
        backgroundColor: '#E7EBE0FF',
    },
    title: {
        text: 'Grafik Sell Out KFA'
    },
    subtitle: {
        text: null
    },
    credits: {
        enabled: false
        }, 
    yAxis: {
        title: {
            text: '<?= $title_sell_out ?>'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    xAxis: {
        categories: <?= $cat ?>,
    },
    series: <?php echo $data_sell_out; ?> ,
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});

</script>
<script>
$("#download").click(function(){
    $("#table-brand").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet 1",
        filename: "data_produk" //do not include extension
    });
});

$("#download_variant").click(function(){
    $("#table-variant").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet 1",
        filename: "data_varian_produk" //do not include extension
    });
});

$("#download_sellout").click(function(){
    $("#table-sellout").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet 1",
        filename: "data_sellout_kfa"
    });
});

$(function() {
    $('input[name="period"]').daterangepicker({
            opens: 'right',
            startDate: '<?= $startDate ?>',
            endDate: '<?= $endDate ?>',
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' / '
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
        // $('input[name="period"]').val('2019-07-01 / 2019-12-31');
        $('input[name="period"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));
        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        // $('#produk').change(function(){
        //     var id=$(this).val();
        //     $.ajax({
        //         url : "<?php echo base_url();?>index.php/Boost/get_kompetitor",
        //         method : "POST",
        //         data : {id: id},
        //         async : false,
        //         dataType : 'json',
        //         success: function(data){
        //             var html = '';
        //             var i;
        //             for(i=0; i<data.length; i++){
        //                 html += '<option>'+data[i].kompetitor+'</option>';
        //             }
        //             $('.subkategori').html(html);
                     
        //         }
        //     });
        // });
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>