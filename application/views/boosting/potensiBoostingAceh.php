<div id="potensi-boosting">
    <div id="clickable-chart">
    <script>
// var f=iQuery.noConflict();

// Build the chart
window.onload = function () {
Highcharts.chart('clickable-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Top 5 KFA  yang potensi di boosting'
    },
    xAxis: {
        categories: ['KFA Langsa', 'KFA A. Yani', 'KFA Banda Aceh', 'KFA Malikul Saleh', 'KFA Lhoksumawe']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Revenue'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal',
                        cursor: 'pointer',
            point: {
                events: {
                    click: function (category, name, event) {

                        // alert(this.category);
                        
                            if (this.category == 'KFA Langsa'){
                                $('#top1').modal();
                            }else if(this.category == 'KFA A. Yani'){
                                $('#top2').modal();
                            }else if (this.category == 'KFA Banda Aceh'){
                                $('#top3').modal();
                            }else if (this.category == 'KFA Malikul Saleh'){
                                $('#top4').modal();
                            }else if (this.category == 'KFA Lhoksumawe'){
                                $('#top5').modal();
                            }
                    }
                }
            }
        }
    },
    series: [{
        name: 'Revenue',
        data: [6123383077, 2584516642, 2522726447, 2495588355, 2495588355],
        showInLegend: false
    }]
});

}
</script>
    </div>
</div>