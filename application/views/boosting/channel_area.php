
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
    

<style>
* {
  box-sizing: border-box;
}

.dataTables_wrapper .dataTables_length {
float: left;
}
/* .dataTables_wrapper .dataTables_filter {
float: center;
text-align: center;
} */
div.dataTables_wrapper div.dataTables_filter{
float:left;
left:0;
margin-left:33%;
margin-right:15%;
margin-bottom: 2px;
}
div.dataTables_wrapper div.dataTables_buttons{
    float:left;
}
table.dataTable thead tr {
  background-color: #F79868;
}
#box1 {

	background: #F79868;
	border-radius: 10px 10px 0px 0px;
}
#map-title{
    color: #fff;
    font-family: Roboto;
    font-style: normal;
    font-weight: bold;
    font-size: 25px;
    line-height: 20px;
    padding-top: 30px;
    padding-bottom: 30px;
    padding-left: 30px;
}
#flex {
    display: flex;

}
#flex {
    display: flex;
    margin-top: -5px;
    margin-left: 5px;
}
#selection{
    display: flex;
    padding-left: 0px;
}


</style>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

        <!-- MAPS -->
        <div class="white-box">
        <div id="selection" style="margin-bottom: 20px; margin-left: 30px; padding-top: 30px;">
        <form>
        From: <select id="periode" name="periode" class="periode">
        <?php $periode = ['Januari 2019', 'Februari 2019'];
        foreach ($periode as $a) {
            if ($a == $_SESSION['periode']) {
                echo "<option value='$a' selected>$a</option>";
            } else {
                echo "<option value='$a'>$a</option>";
            }
        }?></select>
         - To: <select id="periode" name="periode" class="periode">
        <?php $periode = ['Januari 2019', 'Februari 2019'];
        foreach ($periode as $a) {
            if ($a == $_SESSION['periode']) {
                echo "<option value='$a' selected>$a</option>";
            } else {
                echo "<option value='$a'>$a</option>";
            }
        }?></select>
        <select id="lini" name="lini" class="mdb-select lini colorful-select dropdown-primary md-form" searchable="Search here..">
        <?php $lini = ['Lini', 'CARDIOVASCULAR', 'FILARIASIS', 'MARCKS', 'NATURAL', 'NUTRITIONAL', 'WOUND', 'LAUNCHING', 'MENTAL', 'ANTIBIOTIC', 'BAHAN', 'ONCOLOGY', 'CENTRAL', 'CHEMICAL', 'DECORATIVE', 'DERMATOLOGY', 'MONITOR', 'VENUS', 'AIDS', 'EXIST', 'HOSPITAL', 'ONKOLOGI', 'ORAL', 'ANTIRETROVIRAL', 'HEALTH', 'OTC', 'FOOD' ,'JANSSEN', 'KOSMETIK', 'OBGYN', 'ANTISEPTIC', 'DERMATOLOGICALS', 'ETIKAL', 'MOTHER', 'NEW', 'RADIOPHARMACEUTICAL', 'SKIN', 'TRADITIONAL', 'BKKBN', 'BONE', 'COMMODITY', 'FOCUS', 'GASTROINESTINAL', 'NARCOTICS', 'OGB', 'ANALGESIC', 'RAW', 'RESPIRATORY', 'SAS', 'TUBERCULOSIS'];
        foreach ($lini as $l) {
            if ($l == $_SESSION['lini']) {
                echo "<option value='$l' selected>$l</option>";
            } else {
                echo "<option value='$l'>$l</option>";
            }
        }?></select>

        <select id="layanan" name="layanan" class="layanan">
        <?php $layanan = ['Layanan', 'E-Catalog RS', 'Kontrak-BUFFER', 'RS. SILOAM GROUP', 'BPJS', 'Kontrak- PRO GIZI', 'PERTAMINA SD', 'Rutin', 'E-Catalog Reguler', 'KOntrak- E-Katalog', 'BPJS KALTARA', 'KFA', 'Kontrak Tentatif', 'Tender Rutin', 'YAKES-TELKOM', 'Kontrak-E-K KALTARA', 'PERTAMINA','E-Catalog REG KALTAR', 'E-Catalog Dinkes(old)', 'YKKBI', 'E-CATALOG RS SULBAR', 'INHEALTH','Muhammadiah' ];
        foreach ($layanan as $l) {
            if ($l == $_SESSION['layanan']) {
                echo "<option value='$l' selected>$l</option>";
            } else {
                echo "<option value='$l'>$l</option>";
            }
        }?></select>
         <select id="area" name="area" class="area">
        <?php $area = ['area-channel', 'channel-area'];
        foreach ($area as $l) {
            if ($l == $_SESSION['area']) {
                echo "<option value='$l' selected>$l</option>";
            } else {
                echo "<option value='$l'>$l</option>";
            }
        }?></select>
        </form>
            <div id="flex">
                <button id="btnSubmit" class="btn btn-success">Filter</button>
            </div>
        </div>
        <div id="box1">
            <h2 id="map-title">Distribusi Sales</h2>
            <div id="container" style="border-style: solid; border-width: 2px; border-color:#F79868"></div>
        </div>
        </div>
        <br>
        <!-- #MAPS -->
        <!-- TABLE -->
        <div class="white-box" style="color:black;">
            <table id="mytable" class="display" width="100%"></table>
        </div>
         <!-- <div class="row">
            <div class="white-box">
                <div class="box-header">
                </div> -->

                <!-- <div class="box-body table-responsive"> 
                    <table class="table table stripped" id="mytable">
                        <thead>
                            <tr>
                                <th style="background-color: #4E9ED5;">Area</th>
                                <th style="background-color: #F79868;">Apotek</th>
                                <th style="background-color: #F79868;">Traditional Market</th>
                                <th style="background-color: #F79868;">Modern Market</th>
                                <th style="background-color: #F79868;">KFA</th>
                                <th style="background-color: #F79868;">Specialities</th>
                                <th style="background-color: #F79868;">Toko Obat</th>
                                <th style="background-color: #F79868;">PBF</th>
                            </tr>
                        </thead>
                        <tbody>
                        ?php
                        foreach($row->result() as $key =>$data){?>
                        
                            <tr>
                                <td style="background-color:rgba(213, 233, 255, 0.5);"><?php echo($data->nama_kfa) ?></td>
                                <td><?php echo($data->total_qty) ?></td>
                                <td><?php echo($data->total_penjualan) ?></td>
                                <td><?php echo($data->total_qty) ?></td>
                                <td><?php echo($data->total_penjualan) ?></td>
                                <td><?php echo($data->total_qty) ?></td>
                                <td><?php echo($data->total_penjualan) ?></td>
                                <td><?php echo($data->total_qty) ?></td>
                            </tr>
                            <?php
                            ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer">
                </div>
            </div>
         </div>
         <p style="text-align: right;">&copy 2019 Kimia Farma</p> -->

<script>

var data = <?php echo($mapData)?>;

// Create the chart
Highcharts.mapChart('container', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text:'',
    },

    subtitle: {
        text: ''
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        minColor: '#f49f55',
        maxColor: '#d66306',
        min: 0
    },
    credits: {
    enabled: false
    },

    series: [{
        data: data,
        nullColor: 'red',
        name: 'Random data',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
            enabled: false,
            format: '{point.name}'
        }
    }]
});
</script>

<script>
var dataSet = [
    [ "Apotek", 82.0, 75.6, 179.0, 75.6, 179.0, 75.6, 179.0, 75.6],
    [ "Rumah Sakit", 179.0, 75.6, 179.0, 75.6, 179.0, 75.6, 179.0, 75.6],
    [ "Specialities", 79.0, 75.6, 179.0, 75.6, 179.0, 75.6, 179.0, 75.6],
    [ "Toko Obat", 99.0, 110.6, 179.0, 75.6, 179.0, 75.6, 179.0, 75.6]
    ];
var channel_area=$(document).ready( function () {
    $('#mytable').DataTable({
        scrollX:true,
        data: dataSet,
        columns: [
            { title: "Channel" },
            { title: "KFTD Aceh" },
            { title: "KFTD Bekasi" },
            { title: "KFTD Jakarta 1" },
            { title: "KFTD Denpasar" },
            { title: "KFTD Madiun" },
            { title: "KFTD Malang" },
            { title: "KFTD Sidoarjo" },
            { title: "KFTD Bali" },

        ],
        rowCallback: function(row, data, index){
            $(row).find('td:eq(0)').css('background-color', 'rgba(213, 233, 255, 0.5)');
            $(row).find('th:eq(0)').css('background-color', '#F79868');
            if(data[1]> 100){
                $(row).find('td:eq(1)').css('background-color', '#66FFCC');
            }
            if(data[2]>100){
                $(row).find('td:eq(2)').css('background-color', '#66FFCC');
            }
            if(data[1]<80){
                $(row).find('td:eq(1)').css('background-color', '#FFFF00');
            }
        },
        searching: true,
        dom: '<"top"l>fBtr<"bottom"ip>',
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            // {
            //     extend: 'excel',
			// 	text: 'Download excel'
            // }
        ]
    });

} );

</script>

