<style>
th {
  background-color: #093890;
  color: white;
  text-align: center;
} 

td {
  color: #4A5675;
  text-align: center;
} 

</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>
    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Week : <select id="week" name="week"><?php for($i=1;$i<=4;$i++){
      if($i==$_SESSION['week']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>

    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
  </form>
</div>
</div>

<div class="row" style="background:white;padding: 10px;">

  <div class="col-md-12">
    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Cash Collection</h4>
    </div>
  </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
      <table class="table table table-striped">
        <thead class="thead-dark">
          <tr>
            <th rowspan="2" scope="col">Current Year Collection (CYC) & Collection Rate (CR)</th>
         
            <th colspan="3" scope="col">Month to Date</th>
            <th colspan="3" scope="col">Year to Date</th>
          </tr>
          <tr>
            <th scope="col">Target</th>
            <th scope="col">Real</th>
            <th scope="col">Ach.</th>
            <th scope="col">Target</th>
            <th scope="col">Real</th>
            <th scope="col">Ach.</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>a. Cash Collection</th>
              <td></th>
                <td id="cash_col_target_total"></th>
                  <td id="cash_col_real_total"></th>
                    <td id="cash_col_ach_total"></th>
                      <td id="cash_col_target_ccy_total"></th>
                        <td id="cash_col_real_ccy_total"></th>
                          <td id="cash_col_ach_ccy_total"></th>
                          </tr>
                          <tr>
                            <td>i. Internal</td>
                        
                              <td id="cash_col_target_internal">xxx</th>
                                <td id="cash_col_real_internal">xxx</th>
                                  <td id="cash_col_ach_internal">xxx</th>
                                    <td id="cash_col_target_ccy_internal">xxx</th>
                                      <td id="cash_col_real_ccy_internal">xxx</th>
                                        <td id="cash_col_ach_ccy_internal">xxx</th>
                                        </tr>
                                        <tr>
                                          <td>i. Eksternal</td>
                                   
                                            <td id="cash_col_target_eksternal">xxx</th>
                                              <td id="cash_col_real_eksternal">xxx</th>
                                                <td id="cash_col_ach_eksternal">xxx</th>
                                                  <td id="cash_col_target_ccy_eksternal">xxx</th>
                                                    <td id="cash_col_real_ccy_eksternal">xxx</th>
                                                      <td id="cash_col_ach_ccy_eksternal">xxx</th>
                                                      </tr>
                                                      <tr>
                                                        <td>b. Cash Collection Ratio</th>
                                                
                                                            <td id="cash_rat_target_total"></th>
                                                              <td id="cash_rat_real_total"></th>
                                                                <td id="cash_rat_ach_total"></th>
                                                                  <td id="cash_rat_target_ccy_total"></th>
                                                                    <td id="cash_rat_real_ccy_total"></th>
                                                                      <td id="cash_rat_ach_ccy_total"></th>
                                                                      </tr>
                                                                      <tr>
                                                                        <td>i. Internal</td>
                                                               
                                                                         <td id="cash_rat_target_internal">xxx</th>
                                                                          <td id="cash_rat_real_internal">xxx</th>
                                                                            <td id="cash_rat_ach_internal">xxx</th>
                                                                              <td id="cash_rat_target_ccy_internal">xxx</th>
                                                                                <td id="cash_rat_real_ccy_internal">xxx</th>
                                                                                  <td id="cash_rat_ach_ccy_internal">xxx</th>
                                                                                  </tr>
                                                                                  <tr>
                                                                                    <td>i. Eksternal</td>
                                                                       
                                                                                      <td id="cash_rat_target_eksternal">xxx</th>
                                                                                        <td id="cash_rat_real_eksternal">xxx</th>
                                                                                          <td id="cash_rat_ach_eksternal">xxx</th>
                                                                                            <td id="cash_rat_target_ccy_eksternal">xxx</th>
                                                                                              <td id="cash_rat_real_ccy_eksternal">xxx</th>
                                                                                                <td id="cash_rat_ach_ccy_eksternal">xxx</th>
                                                                                                </tr>
                                                                                              </tbody>
                                                                                            </table>
                                                                                          </div>
                                                                                        </div>
                                                                                      </div>

                                                                                      <script>

                                                                                        $(document).ready(function() {
                                                                                          var urlpost = "<?php echo base_url('index.php/page/refresh_cash_collection') ?>";

                                                                                          var year = $('#year').val();
                                                                                          var month = $('#month').val();
                                                                                          var week = $('#week').val();

                                                                                          var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
      urlpost,
      {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'week': JSON.stringify(week),
        'entitas': JSON.stringify(entitas)



      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {


      if (result.data.length > 0){
       $.each(result.data, function( key, value ) {
        if (value.datacollection === "CASH COLLECTION"){
          if (value.cakupan === "INTERNAL"){
            $('#cash_col_target_internal').text("Rp "+value.target);
            $('#cash_col_real_internal').text("Rp "+value.real);
            $('#cash_col_ach_internal').text(value.achievement);
            $('#cash_col_target_ccy_internal').text("Rp "+value.acctarget);
            $('#cash_col_real_ccy_internal').text("Rp "+value.accreal);
            $('#cash_col_ach_ccy_internal').text(value.accachievement);

          }else{
            $('#cash_col_target_eksternal').text("Rp "+value.target);
            $('#cash_col_real_eksternal').text("Rp "+value.real);
            $('#cash_col_ach_eksternal').text(value.achievement);
            $('#cash_col_target_ccy_eksternal').text("Rp "+value.acctarget);
            $('#cash_col_real_ccy_eksternal').text("Rp "+value.accreal);
            $('#cash_col_ach_ccy_eksternal').text(value.accachievement);
          }

        }else{
          if (value.cakupan === "INTERNAL"){
            $('#cash_rat_target_internal').text(value.target);
            $('#cash_rat_real_internal').text(value.real);
            $('#cash_rat_ach_internal').text(value.achievement);
            $('#cash_rat_target_ccy_internal').text(value.acctarget);
            $('#cash_rat_real_ccy_internal').text(value.accreal);
            $('#cash_rat_ach_ccy_internal').text(value.accachievement);
          }else{
            $('#cash_rat_target_eksternal').text(value.target);
            $('#cash_rat_real_eksternal').text(value.real);
            $('#cash_rat_ach_eksternal').text(value.achievement);
            $('#cash_rat_target_ccy_eksternal').text(value.acctarget);
            $('#cash_rat_real_ccy_eksternal').text(value.accreal);
            $('#cash_rat_ach_ccy_eksternal').text(value.accachievement);
          }
        }






      })
     }else{

      $('#cash_col_target_internal').text("0");
      $('#cash_col_real_internal').text("0");
      $('#cash_col_ach_internal').text("0");
      $('#cash_col_target_ccy_internal').text("0");
      $('#cash_col_real_ccy_internal').text("0");
      $('#cash_col_ach_ccy_internal').text("0");
      $('#cash_col_target_eksternal').text("0");
      $('#cash_col_real_eksternal').text("0");
      $('#cash_col_ach_eksternal').text("0");
      $('#cash_col_target_ccy_eksternal').text("0");
      $('#cash_col_real_ccy_eksternal').text("0");
      $('#cash_col_ach_ccy_eksternal').text("0");
      $('#cash_rat_target_internal').text("0");
      $('#cash_rat_real_internal').text("0");
      $('#cash_rat_ach_internal').text("0");
      $('#cash_rat_target_ccy_internal').text("0");
      $('#cash_rat_real_ccy_internal').text("0");
      $('#cash_rat_ach_ccy_internal').text("0");
      $('#cash_rat_target_eksternal').text("0");
      $('#cash_rat_real_eksternal').text("0");
      $('#cash_rat_ach_eksternal').text("0");
      $('#cash_rat_target_ccy_eksternal').text("0");
      $('#cash_rat_real_ccy_eksternal').text("0");
      $('#cash_rat_ach_ccy_eksternal').text("0");
    }

  }


});

$("#btnSubmit").click(function (e) {

  e.preventDefault();




  var urlpost = "<?php echo base_url('index.php/page/refresh_cash_collection') ?>";

  var year = $('#year').val();
  var month = $('#month').val();
  var week = $('#week').val();

  var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
      urlpost,
      {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'week': JSON.stringify(week),
        'entitas': JSON.stringify(entitas)



      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {

       if (result.data.length > 0){
       $.each(result.data, function( key, value ) {
       if (value.datacollection === "CASH COLLECTION"){
          if (value.cakupan === "INTERNAL"){
            $('#cash_col_target_internal').text("Rp "+value.target);
            $('#cash_col_real_internal').text("Rp "+value.real);
            $('#cash_col_ach_internal').text(value.achievement);
            $('#cash_col_target_ccy_internal').text("Rp "+value.acctarget);
            $('#cash_col_real_ccy_internal').text("Rp "+value.accreal);
            $('#cash_col_ach_ccy_internal').text(value.accachievement);

          }else{
            $('#cash_col_target_eksternal').text("Rp "+value.target);
            $('#cash_col_real_eksternal').text("Rp "+value.real);
            $('#cash_col_ach_eksternal').text(value.achievement);
            $('#cash_col_target_ccy_eksternal').text("Rp "+value.acctarget);
            $('#cash_col_real_ccy_eksternal').text("Rp "+value.accreal);
            $('#cash_col_ach_ccy_eksternal').text(value.accachievement);
          }

        }else{
          if (value.cakupan === "INTERNAL"){
            $('#cash_rat_target_internal').text(value.target);
            $('#cash_rat_real_internal').text(value.real);
            $('#cash_rat_ach_internal').text(value.achievement);
            $('#cash_rat_target_ccy_internal').text(value.acctarget);
            $('#cash_rat_real_ccy_internal').text(value.accreal);
            $('#cash_rat_ach_ccy_internal').text(value.accachievement);
          }else{
            $('#cash_rat_target_eksternal').text(value.target);
            $('#cash_rat_real_eksternal').text(value.real);
            $('#cash_rat_ach_eksternal').text(value.achievement);
            $('#cash_rat_target_ccy_eksternal').text(value.acctarget);
            $('#cash_rat_real_ccy_eksternal').text(value.accreal);
            $('#cash_rat_ach_ccy_eksternal').text(value.accachievement);
          }
        }






      })
     }else{
   
      $('#cash_col_target_internal').text("0");
      $('#cash_col_real_internal').text("0");
      $('#cash_col_ach_internal').text("0");
      $('#cash_col_target_ccy_internal').text("0");
      $('#cash_col_real_ccy_internal').text("0");
      $('#cash_col_ach_ccy_internal').text("0");
      $('#cash_col_target_eksternal').text("0");
      $('#cash_col_real_eksternal').text("0");
      $('#cash_col_ach_eksternal').text("0");
      $('#cash_col_target_ccy_eksternal').text("0");
      $('#cash_col_real_ccy_eksternal').text("0");
      $('#cash_col_ach_ccy_eksternal').text("0");
      $('#cash_rat_target_internal').text("0");
      $('#cash_rat_real_internal').text("0");
      $('#cash_rat_ach_internal').text("0");
      $('#cash_rat_target_ccy_internal').text("0");
      $('#cash_rat_real_ccy_internal').text("0");
      $('#cash_rat_ach_ccy_internal').text("0");
      $('#cash_rat_target_eksternal').text("0");
      $('#cash_rat_real_eksternal').text("0");
      $('#cash_rat_ach_eksternal').text("0");
      $('#cash_rat_target_ccy_eksternal').text("0");
      $('#cash_rat_real_ccy_eksternal').text("0");
      $('#cash_rat_ach_ccy_eksternal').text("0");
    }

     }

   });

 </script>