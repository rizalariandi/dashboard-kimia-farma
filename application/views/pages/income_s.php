<?php
$_SESSION['year'] = $_GET['year'] ? $_GET['year'] : date("Y");
$_SESSION['month'] =  $_GET['month'] ? $_GET['month'] : date("n");
if (isset($_GET['period'])) {
    $_SESSION['period'] = $_GET['period'];
}
if (isset($_GET['entitas'])) {
    $_SESSION['entitas'] = $_GET['entitas'];
}
?>

<style type="text/css">
    .last_update {
        position: absolute;
        padding-left: 5px;

        font-weight: normal;
        font-size: 12px;
        line-height: 10px;
        /* identical to box height, or 167% */


        color: #cb3935;

        font-family: Roboto;
        font-style: italic;
    }
</style>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <?php
    // var_dump(json_decode($is));
    ?>
    <h4 class="last_update"> </h4>
    </h4>
    <h4 id="ket" class="page-title last_update"><?php if ($is[0]['ket'] != 'adjusted') {
                                                    echo '<sup>*</sup>Note: Data ' . $is[0]['ket'];
                                                } ?></h4>

</div>

<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">

    <form action="" method="post">
        Period : <select id="period" name="period"><?php $ent = ['Monthly', 'YearToDate'];
                                                    foreach ($ent as $e) {
                                                        if ($e == $_SESSION['period']) {
                                                            echo "<option value='$e' selected>$e</option>";
                                                        } else {
                                                            echo "<option value='$e'>$e</option>";
                                                        }
                                                    } ?></select>
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($_SESSION['role_entitas'] as $e) {

                                                            echo "<option value='$e'>$e</option>";
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        <label id="label_week"> Week : </label><select id="select_week" name="week"><?php for ($i = 1; $i <= 4; $i++) {
                                                                                        if ($i == $_SESSION['week']) {
                                                                                            echo "<option value='$i' selected>$i</option>";
                                                                                        } else {
                                                                                            echo "<option value='$i'>$i</option>";
                                                                                        }
                                                                                    } ?></select>
        <label id="label_day">Day :</label> <select id="select_day" name="day"><?php for ($i = 1; $i <= 31; $i++) {
                                                                                    if ($i == $_SESSION['day']) {
                                                                                        echo "<option value='$i' selected>$i</option>";
                                                                                    } else {
                                                                                        echo "<option value='$i'>$i</option>";
                                                                                    }
                                                                                } ?>
        </select>
        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>


<?php
$key = ['Net Sales', 'COGS', 'Gross Profit', 'Operating Expenses', 'Net Operataing Income', 'Other Income', 'Net Income'];
function format_round($rp)
{
    if ($rp < 1 && $rp > 0) {
        return (round($rp * 100)) . '%';
    } elseif (strlen(round($rp)) > 10) {
        return number_format(($rp / pow(10, 9)), 0, ',', '.') . 'M';
    } else {
        return number_format($rp, 0, ',', '.');
    }
}
function format_chart($rp)
{
    if ($rp < 1 && $rp > 0) {
        return (round($rp * 100));
    } elseif (strlen(round($rp)) > 10) {
        return round(($rp / pow(10, 9)));
    } else {
        return round($rp);
    }
}
$j = 1;

if (!empty($is)) {
    foreach ($is as $v) {

        if ($j == 1 || $j == 4 || $j == 7) {
            echo "<div class='row'>";
        }

        ?>

<div class="col-md-4">


    <div class="white-box">

        <div class='row'>

            <div class='col-md-8'>


                <h3 id="income_statement<?= $j ?>" class="box-title m-b-0" style='font-size: 16px;font-weight: 400;color: #E74040;'><?= $v['income_statement'] ?></h3>
                <h2 id="realisasi<?= $j ?>" class='real animated tada'><?= format_round($v['realisasi']) ?></h2>
                <table class='table-condensed'>
                    <tr>
                        <td>Target</td>

                        <td>Growth</td>

                    </tr>
                    <tr>
                        <td id="target<?= $j ?>"><?= format_round($v['target']) ?></td>
                        <td id="growth<?= $j ?>" class='text-<?= ($v['growth'] > 0) ? 'success' : 'danger' ?>'><i class='fas fa-caret-<?= ($v['growth'] > 0) ? 'up' : 'down' ?> fa-lg'></i><?= $v['growth'] ?>%</td>

                    </tr>
                </table>
            </div>
            <div id="div-pie<?= $j ?>" class='col-md-4'>
                <h4 style="
                                                                                    text-align: center;
                                                                                    margin-bottom: 5px;
                                                                                ">Ach</h4>
                <div id="pie<?= $j ?>" class="chart" style='margin-top:0px;' data-percent="0" data-scale-color="#ffb400">
                    <span class="percent"></span>
                </div>
            </div>
            <div class='col-md-12'>
                <div id='chart<?= $j ?>' style='height: 200px;'></div>
            </div>
        </div>
    </div>
</div>
<?php
        if ($j % 3 == 0) {
            echo "</div>";
        }
        $j++;
    }
}
?>

<div class="row">
    <div class="col-md-4">
        <h4>Achievement</h4>
        <i style="color: orangered;font-size: 12px;"><b>COGS,OPERATING EXPENSES,INTEREST EXPENSE,TAX EXPENSE</b></i><br />
        <i class="fa fa-circle-o" style="color: #cb3935; font-size: 1.5em;"></i><b> &#62;&#61; 100%</b><br />
        <i class="fa fa-circle-o" style="color: #f0ad4e; font-size: 1.5em;"></i><b> 95% &#45; 99.9%</b><br />
        <i class="fa fa-circle-o" style="color: #5cb85c; font-size: 1.5em;"></i><b> &#60; 94.9%</b><br />

    </div>
    <div class="col-md-4">
        <h4>Achievement</h4>
        <i style="color: orangered;font-size: 12px;"><b>NET SALES,GROSS PROFIT,NET OPERATING INCOME,OTHER INCOME,INCOME BEFORE TAXES,NET INCOME</b></i><br />
        <i class="fa fa-circle-o" style="color: #cb3935; font-size: 1.5em;"></i><b> &#60; 94.9%</b><br />
        <i class="fa fa-circle-o" style="color: #f0ad4e; font-size: 1.5em;"></i><b> 95% &#45; 99.9%</b><br />
        <i class="fa fa-circle-o" style="color: #5cb85c; font-size: 1.5em;"></i><b> &#62;&#61; 100%</b><br />

    </div>
</div>

<script>
    var easyChart1 = $('#pie1').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart2 = $('#pie2').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart3 = $('#pie3').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart4 = $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart5 = $('#pie5').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart6 = $('#pie6').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart7 = $('#pie7').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });

    var easyChart8 = $('#pie8').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });


    var easyChart9 = $('#pie9').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });


    var easyChart10 = $('#pie10').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: '#E15656',
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor: function(percent) {
            return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });


    <?php
    //foreach($key as $i=>$k){
    for ($j = 1; $j <= 10; $j++) {
        // if(array_key_exists($k,$data_target)){

        ?>

    var myChart<?= $j ?> = Highcharts.chart('chart<?= $j ?>', {

        title: {
            text: ''
        },
        legend: {
            enable: false,
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            itemStyle: {
                "font-size": "10px",
                "font-weight": "100"
            },
            borderWidth: 1
            //backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            minorTickLength: 0,
            tickLength: 0,
            labels: {
                rotation: 90
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        yAxis: {
            title: {
                text: ''
            },
            visible: false
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Target',
            type: 'line',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            animation: {
                duration: 2000
                // Uses Math.easeOutBounce
                // easing: 'easeOutBounce'
            },
            color: '#DC6B89'
        }, {
            name: 'Real',
            type: 'line',
            data: [0, 0],
            animation: {
                duration: 2000
                // Uses Math.easeOutBounce
                //easing: 'easeOutBounce'
            },
            color: '#6CD0B7'
        }],
        exporting: {
            enabled: false
        }
    });



    <?php } ?>


    $(document).ready(function() {
        $('#select_week').attr("hidden", true);
        $('#label_week').attr("hidden", true);

        $('#select_day').attr("hidden", true);
        $('#label_day').attr("hidden", true);


        generateDashboard();

    });


    $('#period').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected === 'Monthly' || valueSelected === 'YearToDate') {
            $('#select_week').attr("hidden", true);
            $('#label_week').attr("hidden", true);

            $('#select_day').attr("hidden", true);
            $('#label_day').attr("hidden", true);

        } else if (valueSelected === 'Daily') {
            $('#select_week').attr("hidden", false);
            $('#label_week').attr("hidden", false);

            $('#select_day').attr("hidden", false);
            $('#label_day').attr("hidden", false);

        }
    });

    $("#btnSubmit").click(function(e) {

        e.preventDefault();


        generateDashboard();


    });

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function generateDashboard() {

        var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";
        loading()
        var year = $('#year').val();
        var month = $('#month').val();
        var day = $('#day').val();
        var period = $('#period').val();

        var entitas = $('#entitas').val();

        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'day': JSON.stringify(day),
                'period': JSON.stringify(period)



            },
            AjaxSucceeded, "json"

        ).fail(function(response) {
            swal(response.status.toString(), response.statusText, "error");
        });

        function AjaxSucceeded(result) {


            //target initiation
            if (!result.is_sum_income.length) {
                emptyIncomes()
            } else
                $.each(result.is_sum_income, function(key, value) {
                    if (value.income_statement == 'NET SALES') {
                        var txt1 = $("<sup>*</sup>");
                        $('#ket').empty();
                        $('#ket').attr("class", 'page-title last_update');
                        if (value.ket === 'adjusted') {
                            $('#ket').text("");
                        } else {
                            $('#ket').append(txt1);
                            $('#ket').append("Note: Data " + value.ket);
                        }

                        $('#income_statement1').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi1').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi1').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target1').text(format_round(value.sum_target));
                        } else {
                            $('#target1').text(format_round(value.sum_target_monthly));
                        }


                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth1').text("");


                        if (result.is_net_sales_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_net_sales_lastmonth[0].REALISASI_YTD) / result.is_net_sales_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_net_sales_lastmonth[0].realisasi) / result.is_net_sales_lastmonth[0].realisasi) * 100;
                            }


                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth1').prepend(txt2);
                            $('#growth1').append(growth.toFixed(2) + "%");
                            $("#growth1").attr("class", 'text-success');


                        } else {
                            $('#growth1').prepend(txt1);
                            $('#growth1').append(growth.toFixed(2) + "%");
                            $("#growth1").attr("class", 'text-danger');


                        }

                        $("#pie1").attr("data-percent", value.sum_achievement);

                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;

                        $('#div-pie1').empty();
                        $('#div-pie1').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie1' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart1 = $('#pie1').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart1.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart1 = $('#pie1').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart1.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart1 = $('#pie1').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart1.data("easyPieChart").update(achiev);
                        }







                    } else if (value.income_statement == 'COGS') {

                        $('#income_statement2').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi2').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi2').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target2').text(format_round(value.sum_target));
                        } else {
                            $('#target2').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa2').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth2').text("");

                        if (result.is_cogs_lastmonth.length > 0) {

                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_cogs_lastmonth[0].REALISASI_YTD) / result.is_cogs_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_cogs_lastmonth[0].realisasi) / result.is_cogs_lastmonth[0].realisasi) * 100;
                            }

                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth2').prepend(txt2);
                            $('#growth2').append(growth.toFixed(2) + "%");
                            $("#growth2").attr("class", 'text-success');

                        } else {
                            $('#growth2').prepend(txt1);
                            $('#growth2').append(growth.toFixed(2) + "%");
                            $("#growth2").attr("class", 'text-danger');


                        }
                        $('#pie2').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;

                        $('#div-pie2').empty();
                        $('#div-pie2').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie2' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev >= 100) {

                            var easyChart2 = $('#pie2').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart2.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart2 = $('#pie2').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart2.data("easyPieChart").update(achiev);
                        } else if (achiev < 95) {
                            var easyChart2 = $('#pie2').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart2.data("easyPieChart").update(achiev);
                        }

                    } else if (value.income_statement == 'GROSS PROFIT') {

                        $('#income_statement3').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi3').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi3').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target3').text(format_round(value.sum_target));
                        } else {
                            $('#target3').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa3').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth3').text("");

                        if (result.is_gross_profit_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_gross_profit_lastmonth[0].REALISASI_YTD) / result.is_gross_profit_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_gross_profit_lastmonth[0].realisasi) / result.is_gross_profit_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth3').prepend(txt2);
                            $('#growth3').append(growth.toFixed(2) + "%");
                            $("#growth3").attr("class", 'text-success');

                        } else {
                            $('#growth3').prepend(txt1);
                            $('#growth3').append(growth.toFixed(2) + "%");
                            $("#growth3").attr("class", 'text-danger');

                        }
                        $('#pie3').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie3').empty();
                        $('#div-pie3').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie3' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart3 = $('#pie3').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart3.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart3 = $('#pie3').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart3.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart3 = $('#pie3').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart3.data("easyPieChart").update(achiev);
                        }
                    } else if (value.income_statement == 'OPERATING EXPENSES') {

                        $('#income_statement4').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi4').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi4').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target4').text(format_round(value.sum_target));
                        } else {
                            $('#target4').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa4').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth4').text("");

                        if (result.is_operating_expenses_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_operating_expenses_lastmonth[0].REALISASI_YTD) / result.is_operating_expenses_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_operating_expenses_lastmonth[0].realisasi) / result.is_operating_expenses_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth4').prepend(txt2);
                            $('#growth4').append(growth.toFixed(2) + "%");
                            $("#growth4").attr("class", 'text-success');

                        } else {
                            $('#growth4').prepend(txt1);
                            $('#growth4').append(growth.toFixed(2) + "%");
                            $("#growth4").attr("class", 'text-danger');

                        }
                        $('#pie4').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie4').empty();
                        $('#div-pie4').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie4' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev >= 100) {

                            var easyChart4 = $('#pie4').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart4.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart4 = $('#pie4').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart4.data("easyPieChart").update(achiev);
                        } else if (achiev < 95) {
                            var easyChart4 = $('#pie4').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart4.data("easyPieChart").update(achiev);
                        }

                    } else if (value.income_statement == 'NET OPERATING INCOME') {

                        $('#income_statement5').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi5').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi5').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target5').text(format_round(value.sum_target));
                        } else {
                            $('#target5').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa5').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth5').text("");

                        if (result.is_net_operating_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_net_operating_lastmonth[0].REALISASI_YTD) / result.is_net_operating_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_net_operating_lastmonth[0].realisasi) / result.is_net_operating_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth5').prepend(txt2);
                            $('#growth5').append(growth.toFixed(2) + "%");
                            $("#growth5").attr("class", 'text-success');

                        } else {
                            $('#growth5').prepend(txt1);
                            $('#growth5').append(growth.toFixed(2) + "%");
                            $("#growth5").attr("class", 'text-danger');


                        }
                        $('#pie5').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie5').empty();
                        $('#div-pie5').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie5' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart5 = $('#pie5').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart5.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart5 = $('#pie5').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart5.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart5 = $('#pie5').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart5.data("easyPieChart").update(achiev);
                        }

                    } else if (value.income_statement == 'OTHER INCOME') {

                        $('#income_statement6').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi6').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi6').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target6').text(format_round(value.sum_target));
                        } else {
                            $('#target6').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa6').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth6').text("");

                        if (result.is_other_income_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_other_income_lastmonth[0].REALISASI_YTD) / result.is_other_income_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_other_income_lastmonth[0].realisasi) / result.is_other_income_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth6').prepend(txt2);
                            $('#growth6').append(growth.toFixed(2) + "%");
                            $("#growth6").attr("class", 'text-success');

                        } else {
                            $('#growth6').prepend(txt1);
                            $('#growth6').append(growth.toFixed(2) + "%");
                            $("#growth6").attr("class", 'text-danger');


                        }
                        $('#pie6').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie6').empty();
                        $('#div-pie6').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie6' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart6 = $('#pie6').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart6.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart6 = $('#pie6').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart6.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart6 = $('#pie6').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart6.data("easyPieChart").update(achiev);
                        }

                    } else if (value.income_statement == 'INTEREST EXPENSE') {

                        $('#income_statement7').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi7').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi7').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target7').text(format_round(value.sum_target));
                        } else {
                            $('#target7').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa6').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth7').text("");

                        if (result.is_interest_expense_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_interest_expense_lastmonth[0].REALISASI_YTD) / result.is_interest_expense_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_interest_expense_lastmonth[0].realisasi) / result.is_interest_expense_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth7').prepend(txt2);
                            $('#growth7').append(growth.toFixed(2) + "%");
                            $("#growth7").attr("class", 'text-success');

                        } else {
                            $('#growth7').prepend(txt1);
                            $('#growth7').append(growth.toFixed(2) + "%");
                            $("#growth7").attr("class", 'text-danger');


                        }
                        $('#pie7').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie7').empty();
                        $('#div-pie7').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie7' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev >= 100) {

                            var easyChart7 = $('#pie7').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart7.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart7 = $('#pie7').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart7.data("easyPieChart").update(achiev);
                        } else if (achiev < 95) {
                            var easyChart7 = $('#pie7').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart7.data("easyPieChart").update(achiev);
                        }

                    } else if (value.income_statement == 'INCOME BEFORE TAXES') {
                        $('#income_statement8').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi8').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi8').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target8').text(format_round(value.sum_target));
                        } else {
                            $('#target8').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa8').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth8').text("");

                        if (result.is_income_before_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_income_before_lastmonth[0].REALISASI_YTD) / result.is_income_before_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_income_before_lastmonth[0].realisasi) / result.is_income_before_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth8').prepend(txt2);
                            $('#growth8').append(growth.toFixed(2) + "%");
                            $("#growth8").attr("class", 'text-success');

                        } else {
                            $('#growth8').prepend(txt1);
                            $('#growth8').append(growth.toFixed(2) + "%");
                            $("#growth8").attr("class", 'text-danger');


                        }
                        $('#pie8').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie8').empty();
                        $('#div-pie8').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie8' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart8 = $('#pie8').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart8.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart8 = $('#pie8').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart8.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart8 = $('#pie8').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart8.data("easyPieChart").update(achiev);
                        }
                    } else if (value.income_statement == 'TAX EXPENSE') {
                        $('#income_statement9').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi9').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi9').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target9').text(format_round(value.sum_target));
                        } else {
                            $('#target9').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa9').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth9').text("");

                        if (result.is_tax_expense_lastmonth.length > 0) {
                            var percent_growth = 0;
                            if (period === 'YearToDate') {
                                percent_growth = ((value.sum_realisasi_ytd - result.is_tax_expense_lastmonth[0].REALISASI_YTD) / result.is_tax_expense_lastmonth[0].REALISASI_YTD) * 100;
                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_tax_expense_lastmonth[0].realisasi) / result.is_tax_expense_lastmonth[0].realisasi) * 100;
                            }
                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth9').prepend(txt2);
                            $('#growth9').append(growth.toFixed(2) + "%");
                            $("#growth9").attr("class", 'text-success');

                        } else {
                            $('#growth9').prepend(txt1);
                            $('#growth9').append(growth.toFixed(2) + "%");
                            $("#growth9").attr("class", 'text-danger');


                        }
                        $('#pie9').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie9').empty();
                        $('#div-pie9').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie9' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev >= 100) {

                            var easyChart9 = $('#pie9').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart9.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart9 = $('#pie9').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart9.data("easyPieChart").update(achiev);
                        } else if (achiev < 95) {
                            var easyChart9 = $('#pie9').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent >= 100 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart9.data("easyPieChart").update(achiev);
                        }
                    } else if (value.income_statement == 'NET INCOME') {

                        $('#income_statement10').text(value.income_statement);
                        if (period === 'YearToDate') {
                            $('#realisasi10').text(format_round(value.sum_realisasi_ytd));
                        } else {
                            $('#realisasi10').text(format_round(value.sum_realisasi));
                        }
                        if (period === 'YearToDate') {
                            $('#target10').text(format_round(value.sum_target));
                        } else {
                            $('#target10').text(format_round(value.sum_target_monthly));
                        }
                        $('#prognosa10').text(format_round(value.sum_prognosa));
                        var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

                        var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
                        var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
                        var growth = 0;
                        $('#growth10').text("");

                        if (result.is_net_income_lastmonth.length > 0) {



                            var percent_growth = 0;
                            if (period === 'YearToDate') {

                                percent_growth = ((value.sum_realisasi_ytd - result.is_net_income_lastmonth[0].REALISASI_YTD) / result.is_net_income_lastmonth[0].REALISASI_YTD) * 100;

                            } else {
                                percent_growth = ((value.sum_realisasi - result.is_net_income_lastmonth[0].realisasi) / result.is_net_income_lastmonth[0].realisasi) * 100;
                            }

                            if (isNaN(percent_growth)) {
                                growth = 0;
                            } else {
                                growth = percent_growth;
                            }

                        } else {
                            growth = 0;
                        }
                        growth = isFinite(growth) ? growth : 0;

                        if (parseInt(growth) > 0) {
                            $('#growth10').prepend(txt2);
                            $('#growth10').append(growth.toFixed(2) + "%");
                            $("#growth10").attr("class", 'text-success');

                        } else {
                            $('#growth10').prepend(txt1);
                            $('#growth10').append(growth.toFixed(2) + "%");
                            $("#growth10").attr("class", 'text-danger');

                        }
                        $('#pie10').attr("data-percent", value.sum_achievement);
                        var achievement;
                        var achiev;
                        if (period === 'YearToDate') {
                            achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                        } else {
                            achievement = (value.sum_realisasi / value.sum_target_monthly) * 100;
                        }
                        if (isNaN(achievement)) {
                            achiev = 0;
                        } else {
                            achiev = achievement;
                        }

                        achiev = isFinite(achiev) ? achiev : 0;
                        $('#div-pie10').empty();
                        $('#div-pie10').append("<h4 style='text-align: center;margin-bottom: 5px;'>Ach</h4><div id='pie10' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
                        if (achiev < 95) {

                            var easyChart10 = $('#pie10').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#cb3935',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart10.data("easyPieChart").update(achiev);
                        } else if (achiev >= 95 && achiev < 100) {
                            var easyChart10 = $('#pie10').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#f0ad4e',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart10.data("easyPieChart").update(achiev);
                        } else if (achiev >= 100) {
                            var easyChart10 = $('#pie10').easyPieChart({
                                easing: 'easeOutBounce',
                                scaleColor: false,
                                scaleLength: 0,
                                lineWidth: 8,
                                size: 109,
                                trackColor: '#5cb85c',
                                barColor: function(percent) {
                                    return (percent < 95 ? '#cb3935' : percent >= 95 && percent < 100 ? '#f0ad4e' : '#5cb85c');
                                },
                                onStep: function(from, to, percent) {
                                    $(this.el).find('.percent').text(Math.round(percent));
                                }
                            });
                            easyChart10.data("easyPieChart").update(achiev);
                        }

                    }

                })







            //chart initiation

            for (var j = 0; j <= myChart1.series.length; j++) {
                myChart1.series[0].remove();
            }

            myChart1.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart2.series.length; j++) {
                myChart2.series[0].remove();
            }

            myChart2.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart3.series.length; j++) {
                myChart3.series[0].remove();
            }

            myChart3.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart4.series.length; j++) {
                myChart4.series[0].remove();
            }

            myChart4.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart5.series.length; j++) {
                myChart5.series[0].remove();
            }

            myChart5.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart6.series.length; j++) {
                myChart6.series[0].remove();
            }

            myChart6.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart7.series.length; j++) {
                myChart7.series[0].remove();
            }

            myChart7.xAxis[0].setCategories([], false);

            for (var j = 0; j <= myChart8.series.length; j++) {
                myChart8.series[0].remove();
            }

            myChart8.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart9.series.length; j++) {
                myChart9.series[0].remove();
            }

            myChart9.xAxis[0].setCategories([], false);


            for (var j = 0; j <= myChart10.series.length; j++) {
                myChart10.series[0].remove();
            }

            myChart10.xAxis[0].setCategories([], false);


            var label1 = [];
            var data_target1 = [];
            var data_real1 = [];

            var label2 = [];
            var data_target2 = [];
            var data_real2 = [];

            var label3 = [];
            var data_target3 = [];
            var data_real3 = [];

            var label4 = [];
            var data_target4 = [];
            var data_real4 = [];

            var label5 = [];
            var data_target5 = [];
            var data_real5 = [];

            var label6 = [];
            var data_target6 = [];
            var data_real6 = [];

            var label7 = [];
            var data_target7 = [];
            var data_real7 = [];

            var label8 = [];
            var data_target8 = [];
            var data_real8 = [];

            var label9 = [];
            var data_target9 = [];
            var data_real9 = [];

            var label10 = [];
            var data_target10 = [];
            var data_real10 = [];

            $.each(result.is_net_sales, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label1.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target1.push(parseInt(value.TARGET));
                        data_real1.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target1.push(parseInt(value.TARGETMONTHLY));
                        data_real1.push(parseInt(value.realisasi));
                    }
                }

            })

            $.each(result.is_cogs, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label2.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target2.push(parseInt(value.TARGET));
                        data_real2.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target2.push(parseInt(value.TARGETMONTHLY));
                        data_real2.push(parseInt(value.realisasi));
                    }
                }

            })


            $.each(result.is_gross_profit, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label3.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target3.push(parseInt(value.TARGET));
                        data_real3.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target3.push(parseInt(value.TARGETMONTHLY));
                        data_real3.push(parseInt(value.realisasi));
                    }
                }

            })


            $.each(result.is_operating_expenses, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label4.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target4.push(parseInt(value.TARGET));
                        data_real4.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target4.push(parseInt(value.TARGETMONTHLY));
                        data_real4.push(parseInt(value.realisasi));
                    }
                }

            })


            $.each(result.is_net_operating, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label5.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target5.push(parseInt(value.TARGET));
                        data_real5.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target5.push(parseInt(value.TARGETMONTHLY));
                        data_real5.push(parseInt(value.realisasi));
                    }
                }

            })

            $.each(result.is_other_income, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label6.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target6.push(parseInt(value.TARGET));
                        data_real6.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target6.push(parseInt(value.TARGETMONTHLY));
                        data_real6.push(parseInt(value.realisasi));
                    }
                }

            })

            $.each(result.is_interest_expense, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label7.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target7.push(parseInt(value.TARGET));
                        data_real7.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target7.push(parseInt(value.TARGETMONTHLY));
                        data_real7.push(parseInt(value.realisasi));
                    }
                }

            })


            $.each(result.is_income_before, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label8.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target8.push(parseInt(value.TARGET));
                        data_real8.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target8.push(parseInt(value.TARGETMONTHLY));
                        data_real8.push(parseInt(value.realisasi));
                    }
                }

            })


            $.each(result.is_tax_expense, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label9.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target9.push(parseInt(value.TARGET));
                        data_real9.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target9.push(parseInt(value.TARGETMONTHLY));
                        data_real9.push(parseInt(value.realisasi));
                    }
                }

            })

            $.each(result.is_net_income, function(key, value) {
                if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16) {

                    label10.push(month_name(value.month));

                    if (period === 'YearToDate') {
                        data_target10.push(parseInt(value.TARGET));
                        data_real10.push(parseInt(value.REALISASI_YTD));
                    } else {
                        data_target10.push(parseInt(value.TARGETMONTHLY));
                        data_real10.push(parseInt(value.realisasi));
                    }
                }

            })



            myChart1.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target1,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart1.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real1,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart1.xAxis[0].setCategories(label1, true);

            myChart2.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target2,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart2.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real2,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart2.xAxis[0].setCategories(label2, true);

            myChart3.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target3,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart3.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real3,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart3.xAxis[0].setCategories(label3, true);

            myChart4.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target4,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart4.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real4,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart4.xAxis[0].setCategories(label4, true);

            myChart5.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target5,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart5.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real5,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart5.xAxis[0].setCategories(label5, true);

            myChart6.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target6,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart6.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real6,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart6.xAxis[0].setCategories(label6, true);

            myChart7.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target7,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart7.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real7,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart7.xAxis[0].setCategories(label7, true);

            myChart8.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target8,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart8.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real8,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart8.xAxis[0].setCategories(label8, true);

            myChart9.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target9,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart9.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real9,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart9.xAxis[0].setCategories(label9, true);

            myChart10.addSeries({
                name: 'Target',
                type: 'line',
                data: data_target10,
                animation: {
                    duration: 2000
                },
                color: '#DC6B89'
            }, true);
            myChart10.addSeries({
                name: 'Real',
                type: 'line',
                data: data_real10,
                animation: {
                    duration: 2000
                },
                color: '#6CD0B7'
            }, true);

            myChart10.xAxis[0].setCategories(label10, true);
            swal.close()
        }
    }


    function numberWithCommas(x) {
        //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }

    }

    function format_round(rp) {

        if (rp < 1 && rp > 0) {
            return (Math.round(rp * 100)) + '%';

        } else if (Math.round(rp).toString().length >= 10) {

            return "Rp " + numberWithCommas(Math.round(rp / Math.pow(10, 9))) + 'M';
        } else {
            return ("Rp " + numberWithCommas(rp));
        }
    }


    function format_round_chart(rp) {

        if (rp < 1 && rp > 0) {
            return (Math.round(rp * 100)) + '%';

        } else if (Math.round(rp).toString().length >= 10) {

            return numberWithCommas(Math.round(rp / Math.pow(10, 9))) + 'M';
        } else {
            return (numberWithCommas(rp));
        }
    }

    function emptyIncomes() {
        $('#income_statement1').text("NET SALES")
        $('#growth1').text('-');
        $('#realisasi1').text(format_round('-'));
        $('#target1').text(format_round('-'));
        $('#div-pie1').empty();
        $('#div-pie1').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth2').text('-');
        $('#income_statement2').text("COGS")
        $('#realisasi2').text(format_round('-'));
        $('#target2').text(format_round('-'));
        $('#div-pie2').empty();
        $('#div-pie2').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth3').text('-');
        $('#income_statement3').text("GROSS PROFIT")
        $('#realisasi3').text(format_round('-'));
        $('#target3').text(format_round('-'));
        $('#div-pie3').empty();
        $('#div-pie3').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth4').text('-');
        $('#income_statement4').text("OPERATING EXPENSES")
        $('#realisasi4').text(format_round('-'));
        $('#target4').text(format_round('-'));
        $('#div-pie4').empty();
        $('#div-pie4').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth5').text('-');
        $('#income_statement5').text("NET OPERATING INCOME")
        $('#realisasi5').text(format_round('-'));
        $('#target5').text(format_round('-'));
        $('#div-pie5').empty();
        $('#div-pie5').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth6').text('-');
        $('#income_statement6').text("OTHER INCOME")
        $('#realisasi6').text(format_round('-'));
        $('#target6').text(format_round('-'));
        $('#div-pie6').empty();
        $('#div-pie6').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth7').text('-');
        $('#income_statement7').text("INTEREST EXPENSE")
        $('#realisasi7').text(format_round('-'));
        $('#target7').text(format_round('-'));
        $('#div-pie7').empty();
        $('#div-pie7').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth8').text('-');
        $('#income_statement8').text("INCOME BEFORE TAXES")
        $('#realisasi8').text(format_round('-'));
        $('#target8').text(format_round('-'));
        $('#div-pie8').empty();
        $('#div-pie8').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth9').text('-');
        $('#income_statement9').text("TAX EXPENSE")
        $('#realisasi9').text(format_round('-'));
        $('#target9').text(format_round('-'));
        $('#div-pie9').empty();
        $('#div-pie9').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
        $('#growth10').text('-');
        $('#income_statement10').text("NET INCOME")
        $('#realisasi10').text(format_round('-'));
        $('#target10').text(format_round('-'));
        $('#div-pie10').empty();
        $('#div-pie10').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="120" height="100" viewBox="0 0 25 25"><path id="Block" d="M24.953,13.456 C24.828,15.175 24.379,16.804 23.553,18.317 C21.617,21.860 18.662,24.027 14.692,24.794 C14.210,24.887 13.717,24.932 13.229,25.000 C12.743,25.000 12.257,25.000 11.771,25.000 C11.704,24.985 11.638,24.960 11.571,24.955 C9.433,24.802 7.457,24.148 5.676,22.960 C2.699,20.975 0.864,18.219 0.207,14.693 C0.117,14.209 0.068,13.717 -0.000,13.229 C-0.000,12.743 -0.000,12.257 -0.000,11.771 C0.016,11.695 0.042,11.620 0.047,11.544 C0.170,9.834 0.616,8.211 1.435,6.706 C3.369,3.152 6.327,0.974 10.308,0.207 C10.791,0.114 11.283,0.068 11.771,-0.000 C12.257,-0.000 12.743,-0.000 13.229,-0.000 C13.296,0.015 13.362,0.040 13.429,0.045 C15.799,0.218 17.952,0.998 19.859,2.409 C22.535,4.390 24.192,7.024 24.793,10.307 C24.882,10.791 24.932,11.283 25.000,11.771 C25.000,12.257 25.000,12.743 25.000,13.229 C24.984,13.305 24.959,13.380 24.953,13.456 zM12.521,0.837 C6.137,0.795 0.898,5.943 0.832,12.381 C0.766,18.875 5.947,24.107 12.491,24.170 C18.828,24.231 24.104,19.018 24.169,12.632 C24.236,6.138 19.071,0.880 12.521,0.837 zM7.313,19.257 C11.291,15.279 15.271,11.298 19.260,7.309 C21.522,10.396 21.433,15.464 18.378,18.462 C15.184,21.597 10.092,21.360 7.313,19.257 zM19.113,8.663 C15.631,12.144 12.139,15.636 8.656,19.120 C11.211,20.508 15.340,20.427 17.925,17.745 C20.649,14.917 20.313,10.706 19.113,8.663 zM6.498,6.662 C9.432,3.626 14.509,3.425 17.695,5.741 C13.708,9.728 9.728,13.709 5.750,17.686 C3.703,15.043 3.351,9.919 6.498,6.662 zM16.377,5.897 C13.754,4.474 9.462,4.544 6.843,7.508 C4.394,10.280 4.708,14.350 5.908,16.366 C9.393,12.881 12.882,9.391 16.377,5.897 z" fill="#757575" /></svg>');
    }

    function month_name(x) {

        if (x == 1) {
            return "Jan";
        } else if (x == 2) {
            return "Feb";
        } else if (x == 3) {
            return "Mar";
        } else if (x == 4) {
            return "Apr";
        } else if (x == 5) {
            return "Mei";
        } else if (x == 6) {
            return "Jun";
        } else if (x == 7) {
            return "Jul";
        } else if (x == 8) {
            return "Agu";
        } else if (x == 9) {
            return "Sep";
        } else if (x == 10) {
            return "Okt";
        } else if (x == 11) {
            return "Nov";
        } else if (x == 12) {
            return "Des";
        }
    }
</script>