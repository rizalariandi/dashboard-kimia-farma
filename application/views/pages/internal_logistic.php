<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Stock on Hand</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang Laku</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Tidak Laku</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                    <span class="sr-only">40% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Kurang Laku</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                    <span class="sr-only">50% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Expired</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Day of Inventory</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div id="day_of_inventory" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Index Stock</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div id="index_stock" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Status PO</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div id="status_po" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</div>



<script>
        Highcharts.setOptions({
            colors: ['#D5EAFF', '#000000']
        });
        Highcharts.chart('day_of_inventory', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'linear'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                   
                }
            },

            series: [{
                type: 'area',
                name: 'Days',
                data: [[0.0, 225],
                    [0.1, 226],
                    [0.2, 228],
                    [0.2, 228],
                    [0.3, 228],
                    [0.4, 229],
                    [0.5, 229],
                    [0.5, 229],
                    [0.6, 230]]
            }]
        });
</script>
<script>
        Highcharts.setOptions({
            colors: ['#F2DCC5', '#000000']
        });
        Highcharts.chart('index_stock', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'linear'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                   
                }
            },

            series: [{
                type: 'area',
                name: 'Days',
                data: [[0.0, 225],
                    [0.1, 226],
                    [0.2, 228],
                    [0.2, 228],
                    [0.3, 228],
                    [0.4, 229],
                    [0.5, 229],
                    [0.5, 229],
                    [0.6, 230]]
            }]
        });
</script>
<script>
        Highcharts.setOptions({
            colors: ['#FFBEBC', '#000000']
        });
        Highcharts.chart('status_po', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'linear'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                   
                }
            },

            series: [{
                type: 'area',
                name: 'Days',
                data: [[0.0, 225],
                    [0.1, 226],
                    [0.2, 228],
                    [0.2, 228],
                    [0.3, 228],
                    [0.4, 229],
                    [0.5, 229],
                    [0.5, 229],
                    [0.6, 230]]
            }]
        });
</script>
