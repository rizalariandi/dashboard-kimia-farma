<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">List of items that will be expired in the next 90 days</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">List of Overstock items</h4>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Per Principal</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>Yearly</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>
<div class="row" style="background:white;padding: 10px;">
            <div class="col-lg-1">
                <h6>Barang 1</h6>
            </div>
            <div class="col-lg-10" style="vertical-align:text-bottom;">
                <div class="progress" style="height: 30px;">
                  <div class="progress-bar bg-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                  </div>
                </div>
            </div>
            <div class="col-lg-1">
                <h6>1000</h6>
            </div>            
</div>