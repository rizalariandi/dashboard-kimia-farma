<?php 
function format_round($rp){
  return number_format($rp,0,',','.');
}
$chart_ap1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ap2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ap_data as $a){
  $chart_ap1[$a['vendor_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ap2['IDR'] += abs($a['total']);
  }else{
    $chart_ap2['Non IDR'] += abs($a['total']);
  }
}
$chart_ar1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ar2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ar_data as $a){
  $chart_ar1[$a['customer_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ar2['IDR'] += abs($a['total']);
  }else{
    $chart_ar2['Non IDR'] += abs($a['total']);
  }
}
?>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

<style>
table tbody tr td.ttl{
	background: #F7FAFC;
  text-align: center;
  font-weight: bold;
}
table thead{
  background:#093890;
  text-align:center;
}

.dataTables_wrapper .dt-buttons {
  float:right;
}

.btn-table {
  background-color: #08388F !important;
  text-decoration: none;
}

.btn-table:hover {
  color: #fff !important;
  text-decoration: none;
}
.DivToScroll{   
  background-color: #F5F5F5;
  border: 1px solid #DDDDDD;
  border-radius: 4px 0 4px 0;
  color: #3B3C3E;
  font-size: 12px;
  font-weight: bold;
  left: -1px;
  padding: 10px 7px 5px;
}

.DivWithScroll{
  height:450px;
  overflow:scroll;
  overflow-x:hidden;
}
</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Value : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>
    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Day : <select name="day"><?php for($i=1;$i<=31;$i++){
      if($i==$_SESSION['day']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?>
  </select>
  <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="white-box">
      <h3 class="box-title" style="color: #08388F;">Account Payable</h3>
      <table class="table table-bordered">
        <thead><tr><th>Balance</th></tr></thead>
        <tbody><tr><td id="ap_balance" class='ttl'><?=format_round(array_sum($chart_ar1))?></td></tr></tbody>
      </table>
      DAOPS : <select id="daops1" name="multiselect[]" multiple="multiple">

        <?php 
        foreach ($daops as $key => $value)
        {
          echo '<option value="'.$value['daops'].'">'.$value['daops'].'</option>';
        }
        ?>
      </select>   
      <div class="row">
        <div class="col-md-6"> <div id="pieAr1"></div>


      </div>
      <div class="col-md-6"> 
        <div class="DivToScroll">
         <div class="DivWithScroll">
          <table id="table_tipe_segment" class="table  table-bordered">
            <thead class='thead-g' style="background: #5591F5;"><tr><th>No.</th><th>Tipe Segment</th><th>TOTAL</th></tr></thead> 
            <tbody></tbody> 
          </table>
        </div>
      </div>
    </div>
  </div>
  <h3 class="box-title" style="color: #08388F;">Table Aging AR by Customer</h3> 

  DAOPS : <select id="daops_ar" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($daops as $key => $value)
    {
      echo '<option value="'.$value['daops'].'">'.$value['daops'].'</option>';
    }
    ?>
  </select>   

  Plant : <select id="plant_ar" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($plant as $key => $value)
    {
      echo '<option value="'.$value['plant'].'">'.$value['plant'].'</option>';
    }
    ?>
  </select>

  Segment 1 : <select id="segment1" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($segment_1 as $key => $value)
    {
      echo '<option value="'.$value['segment'].'">'.$value['segment'].'</option>';
    }
    ?>
  </select>   


  <table class="table" id='ap_table'>
    <thead>
      <tr>
        <th>No</th>
        <th>DAOPS</th>
        <th>Plant</th>
        <th>Segment1</th>
        <th>0 &#45; 30 Days</th>
        <th>31 &#45; 60 Days</th>
        <th>60 &#45; 90 Days</th>
        <th>91 &#45; 120 Days</th>
        <th>120 &#45; 150 Days</th>
        <th>150 &#45; 360 Days</th>
        <th>&#62; 360 Days</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
</div>
</div>
</div>


<script>


  $(document).ready(function() {



   $('#pihak_ketiga').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#pihak_ketiga").multiselect('selectAll', false);

   $('#pihak_ketiga').multiselect('updateButtonText');


   $('#pihak_berelasi').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#pihak_berelasi").multiselect('selectAll', false);

   $('#pihak_berelasi').multiselect('updateButtonText');


   $('#daops1').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox3,
     onSelectAll: getValueCheckbox3,
     onSelectedAll: getValueCheckbox3,
     onDeselectAll: getValueCheckbox3
   });
   $("#daops1").multiselect('selectAll', false);

   $('#daops1').multiselect('updateButtonText');


   $('#daops_ar').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#daops_ar").multiselect('selectAll', false);

   $('#daops_ar').multiselect('updateButtonText');

   $('#daops_ap').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#daops_ap").multiselect('selectAll', false);

   $('#daops_ap').multiselect('updateButtonText');


   $('#plant_ar').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 250,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#plant_ar").multiselect('selectAll', false);

   $('#plant_ar').multiselect('updateButtonText');

   $('#plant_ap').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#plant_ap").multiselect('selectAll', false);

   $('#plant_ap').multiselect('updateButtonText');

   $('#segment1').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#segment1").multiselect('selectAll', false);

   $('#segment1').multiselect('updateButtonText');

   $('#segment3').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 250,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#segment3").multiselect('selectAll', false);

   $('#segment3').multiselect('updateButtonText');

   var params = {
    daops: [],
    plant: [],
    segment1: [],
    segment3: []
  };
  $('#daops_ar :selected').each(function() {
    params.daops.push($(this).val());
  });

  $('#plant_ar :selected').each(function() {
    params.plant.push($(this).val());
  });

  $('#segment1 :selected').each(function() {
    params.segment1.push($(this).val());
  });

  $('#segment3 :selected').each(function() {
    params.segment3.push($(this).val());
  });

    $("#btnSubmit").click();
  var radio_value_ap = "0-30";
  var year = $('#year').val();
  var month = $('#month').val();
  var i = 0;
  var entitas = $('#entitas').val();


   var urlpost = "<?php echo base_url('index.php/page/refresh_apar_kftd') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'year': JSON.stringify(year),
          'month': JSON.stringify(month),
          'entitas': JSON.stringify(entitas),
          'daops': JSON.stringify(params.daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {

      
        $('#ap_balance').text(format_round(result.ap_sum[0].total));

      }
  var i = 0;

  var data_table1 = $('#ap_table').DataTable({
    "scrollY": "400px",
    "scrollCollapse": true,
    "scrollX": false,
    "ordering": true,
    "bDestroy": true,
    "serverSide": true,
    "bFilter": false,
    "bLengthChange": false,
    responsive: true,
    scrollY: "50vh",
    scrollX: true,
    scrollCollapse: true,
    "processing": true,

    "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
    "dom"         : 'Blfrtip',
    buttons: [
    {
      extend: 'excelHtml5',
      text: "<i class='fas fa-download fa-lg'></i> Download",
      className: 'btn-table',
      exportOptions: {
        modifier: {
          search: 'applied',
          order: 'applied'
        }
      }
    }
    ],
    pageLength: 25,

    "ajax": {
      "url": "<?php echo base_url('index.php/page/balance_sheet_ap2_kftd') ?>",
      "method" : "POST",
      "data": {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'entitas': JSON.stringify(entitas),
        'daops': JSON.stringify(params.daops),
        'plant': JSON.stringify(params.plant)

      }
    },"columns": [

    { 
      data: 0,
      render: function(data, type, row){


        return (i = i +1);
      }
    },
    { 
      data: 1
    }
    ,
    { 
      data: 2
    }
    ,
    { 
      data: 3
    },
    { 
      data: 4,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    },
    { 
      data: 5,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    }
    ,
    { 
      data: 6,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }
    ,
    { 
      data: 7,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }
    ,
    { 
      data: 8,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }
    ,
    { 
      data: 9,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }
    ,
    { 
      data: 10,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }
    ,
    { 
      data: 11,
      width: 100,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }

    }

    ]
  });


})



Highcharts.setOptions({
  lang: {
    decimalPoint: ',',
    thousandsSep: '.'
  }
});


var chartar1 =   Highcharts.chart('pieAr1', {
  credits: {
    enabled: false
  },
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: ''
  },
  tooltip: {
    pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
      },
      showInLegend: false
    }
  },
  series: [{
    data :[]
  }]
});


function getValueCheckbox3(){
  var year = $('#year').val();
  var month = $('#month').val();

  var entitas = $('#entitas').val();

  var params = {
    daops: []
  };

  $('#daops1 :selected').each(function() {
    params.daops.push($(this).val());
  });


  var urlpost = "<?php echo base_url('index.php/page/kftd_tipe_segment_ap') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'year': JSON.stringify(year),
          'month': JSON.stringify(month),
          'entitas': JSON.stringify(entitas),
          'daops': JSON.stringify(params.daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
        var i = 0;
        $('#table_tipe_segment').empty();
        $('#table_tipe_segment').append("<thead class='thead-g' style='background: #5591F5;'><tr><th>No.</th><th>Tipe Segment</th><th>TOTAL</th></tr></thead><tbody></tbody>");
        $.each(result.data, function( key, value ) {
          i++;


          $('#table_tipe_segment').append("<tr><td>"+i+"</td><td>"+value.segment +"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
        })


      }


    }  
    function getValueCheckbox2(){

      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();

      var params = {
        daops: [],
        plant: [],
        segment1: [],
        segment3: []
      };

      $('#daops_ar :selected').each(function() {
        params.daops.push($(this).val());
      });

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });
      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });

      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });
      console.debug(params.plant);

      var i=0;
      var data_table1 = $('#ap_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,
        scrollY: "50vh",
        scrollX: true,
        scrollCollapse: true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ap2_kftd') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1)
           

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        }
        ,
        { 
          data: 6,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 7,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 8,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 9,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 10,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 11,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }

        ]
      });

    }

    function getValueCheckbox(){
     var year = $('#year').val();
     var month = $('#month').val();

     var entitas = $('#entitas').val();

     var params = {
      daops: [],
      plant: [],
      segment1: [],
      segment3: []
    };

    $('#daops_ar :selected').each(function() {
      params.daops.push($(this).val());
    });

    var urlpost = "<?php echo base_url('index.php/page/get_plant') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'daops': JSON.stringify(params.daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
        $('#plant_ar')
        .find('option')
        .remove()
        .end();


        $.each(result.data, function( key, value ) {
         $('#plant_ar')
         .append($("<option></option>")
           .attr("value",value.plant)
           .text(value.plant));

         $('#plant_ar').multiselect('rebuild');
         $("#plant_ar").multiselect('selectAll', false);

         $('#plant_ar').multiselect('updateButtonText');
          //$('#kota').append('<option value="'+value+'">'+value+'</option>');
        })


      }

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });
      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });

      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });
      console.debug(params.plant);

      var i=0;
      var data_table1 = $('#ap_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,
        scrollY: "50vh",
        scrollX: true,
        scrollCollapse: true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1)

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        }
        ,
        { 
          data: 6,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 7,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 8,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 9,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 10,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 11,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }

        ]
      });

    }

    function Employee(name, y) {
      this.name = name;
      this.y = y;
    }

    $("#btnSubmit").click(function (e) {

      e.preventDefault();

      var daops = []
      $('#daops1 :selected').each(function() {
        daops.push($(this).val());
      });


      var urlpost_tipe_segment = "<?php echo base_url('index.php/page/kftd_tipe_segment_ap') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      


      var params = {
        daops: [],
        plant: [],
        segment1: [],
        segment3: []
      };

      $('#daops_ar :selected').each(function() {
        params.daops.push($(this).val());
      });

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });

      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });


      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });



      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();
      var chart = [];
      $.post(
        urlpost_tipe_segment,
        {

          'year': JSON.stringify(year),
          'month': JSON.stringify(month),
          'entitas': JSON.stringify(entitas),
          'daops': JSON.stringify(daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
        var i=0;
         $('#ap_balance').text(format_round(result.ap_sum[0].total));
        $.each(result.data, function( key, value ) {

          i++;

          $('#table_tipe_segment').append("<tr><td>"+ i +"</td><td>"+value.segment +"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
        })


        var employeeObject;
        $.each(result.data_chart, function( key, value ) {
          employeeObject = new Employee(value.plant,value.total);
          chart.push({name:value.plant,y: parseInt(value.total)});
          
        })
       
        chartar1.addSeries({

         data: [
          {"name":"KFTD AMBON",y:Math.abs(38767989800)},
         {"name":"KFTD BALIKPAPAN",y:Math.abs(57205126247)},
         {"name":"KFTD BANDA ACEH",y:Math.abs(134182316106)},
         {"name":"KFTD BANDAR LAMPUNG",y:Math.abs(90966104578)},
         {"name":"KFTD BANDUNG",y:Math.abs(209185309233)},
         {"name":"KFTD BANJARMASIN",y:Math.abs(63350210518)},
         {"name":"KFTD BATAM",y:Math.abs(48912034555)},
         {"name":"KFTD BEKASI",y:Math.abs(100844156930)},
         {"name":"KFTD BENGKULU",y:Math.abs(45579417905)},
         {"name":"KFTD BOGOR",y:Math.abs(143537636919)}
         ]

       }, true);

      }




      var i=0;
      var data_table1 = $('#ap_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,
        scrollY: "50vh",
        scrollX: true,
        scrollCollapse: true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ap2_kftd') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1)
          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        }
        ,
        { 
          data: 6,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 7,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 8,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 9,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 10,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 11,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }

        ]
      });
    })

function format_round(rp){

  if(rp<1 && rp>0){
    return (Math.round(rp*100))+'%';

  }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
    }else{
      return ("Rp "+numberWithCommas(rp));
    }
  }

  function numberWithCommas(x) {
   if (x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }else{
    return 0;
  }
}

</script>
