<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Realisasi Investasi 2018</h4>
        </div>
    </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Uraian</th>
                          <th scope="col">Anggaran</th>
                          <th scope="col">Realisasi s.d. 31 Desember</th>
                          <th scope="col">%Realisasi Tahun 2018</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>Otto</td>
                          <td>Otto</td>
                        </tr>
                        <tr>
                          <td>2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>Thornton</td>
                          <td>Thornton</td>
                        </tr>
                        <tr>
                          <td>3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>the Bird</td>
                          <td>the Bird</td>
                        </tr>
                      </tbody>
                    </table>
		</div>
	</div>
</div>