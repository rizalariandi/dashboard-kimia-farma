<style>
  #map_achivement {
    height: 500px;
    max-width: 100%;
    margin: 0 auto;
  }

  .loading {
    margin-top: 10em;
    text-align: center;
    color: gray;
  }

  .btn-red {
    position: absolute !important;
    margin-left: 10px !important;
  }

  .none {
    display: none;
  }

  #sales_product .text-left {
    cursor: pointer;
  }
</style>
<div class="row">
  <div id="editor"></div>
  <h3 class="text-center">Overview Executive Marketing <button onclick="clearF()" id="data_selected" class="btn btn-danger btn-red"><i class="fa fa-close"></i></button></h3>
  <div class="col-lg-2">
    <div class="form-group">
      <label for="exampleFormControlSelect1">Value</label>
      <!-- onchange="action('value',this.value)" -->
      <select class="form-control" id="value"  id="exampleFormControlSelect1">
        <option value="hna_ty">HNA</option>
        <option value="hjp_ty">HJP</option>
        <option value="hjp_ptd_ty">HJP-PTD</option>
        <option value="hjd_ty">HJD</option>
        <option value="hpp_ty">HPP</option>
        <option value="hna_ptd_ty">HNA-PTD</option>
      </select>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group start_date">
      <label for="exampleFormControlSelect1">Start Month</label>
      <!-- onchange="action('start',this.value),starMonthF(this.value)" -->
      <select class="form-control" id="start" id="exampleFormControlSelect1">
        <option selected value="01">January</option>
        <option value="02">Febuary</option>
        <option value="03">March</option>
        <option value="04">April</option>
        <option value="05">May</option>
        <option value="06">June</option>
        <option value="07">July</option>
        <option value="08">August</option>
        <option value="09">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group end_date">
      <label for="exampleFormControlSelect1">End Month</label>
      <!-- onchange="action('end',this.value),endMonthF(this.value)" -->
      <select class="form-control" id="end" id="exampleFormControlSelect1">
        <option selected value="01">January</option>
        <option value="02">February</option>
        <option value="03">March</option>
        <option value="04">April</option>
        <option value="05">May</option>
        <option value="06">June</option>
        <option value="07">July</option>
        <option value="08">August</option>
        <option value="09">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group year_select">
      <label for="exampleFormControlSelect1">Year</label>
      <!-- onchange="action('year',this.value)" -->
      <select class="form-control" id="year" id="exampleFormControlSelect1">
        <!-- <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option selected value="2021">2021</option> -->
      </select>
    </div>
  </div>
  <div class="col-lg-1">
    <div class="form-group">
      <button style="margin-top: 2em;" id="goaction" type="button" class="btn btn-success" onclick="action('ket')">Go</button>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Distributor KFTD</h4>
    </div>
    <div id="kftd_hide" style="display: grid;">
      <h4 class="kftd text-center"></h4>
    </div>
    <div id='loader_otr_kftd' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <div class="col-lg-6">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Other Distributor</h4>
    </div>
    <div id="other_kftd_hide" style="display: grid;">
      <h4 class="other_kftd text-center"></h4>
    </div>
    <div id='loader_otr_nondist' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-4">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">MTD</h4>
    </div>
    <div id="tabel_mtd" style="display: grid;">
      <table style="width:100%">
        <tr>
          <th class="text-dark text-center">Target</th>
          <th class="text-dark text-center">Value Ty</th>
          <th class="text-dark text-center">Value Ly</th>
          <th class="text-dark text-center">Ach</th>
          <th class="text-dark text-center">Growth</th>
        </tr>
        <tr class="mtd">
        </tr>
      </table>
    </div>
    <div id='loader_otr_mtd' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <div class="col-lg-4">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">YTD</h4>
    </div>
    <div id="tabel_ytd" style="display: grid;">
      <table style="width:100%">
        <tr>
          <th class="text-dark text-center">Target</th>
          <th class="text-dark text-center">Value Ty</th>
          <th class="text-dark text-center">Value Ly</th>
          <th class="text-dark text-center">Ach</th>
          <th class="text-dark text-center">Growth</th>
        </tr>
        <tr class="ytd">
        </tr>
      </table>
    </div>
    <div id='loader_otr_ytd' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <!-- <div class="col-lg-2">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Marketing Expense</h4>
    </div>
    <div style="display: grid;">
      <table style="width:100%">
        <tr>
          <th class="text-dark text-center">MTD</th>
          <th class="text-dark text-center">YTD</th>
        </tr>
        <tr class="expense">

        </tr>
      </table>
    </div>
  </div> -->
  <div class="col-lg-4">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Sales Contribution</h4>
    </div>
    <div id="salescontrib" style="display: grid;">
      <table style="width:100%">
        <tr>
          <th class="text-dark text-center">KFA</th>
          <th class="text-dark text-center">Non KFA</th>
        </tr>
        <tr class="contribution">
        </tr>
      </table>
    </div>
    <div id='loader_otr_salescontrib' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <div class="col-lg-12">
    <h4> Achievement Distributor</h4>
    <div id="map_achivement"></div>
    <div id='loader_otr_map' style='display: none; margin: 0 auto;'>
      <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      <select class="form-control" onchange="actionSpesial('by',this.value)" id="by">
        <option value="default">Default</option>
        <option value="target">Target</option>
        <option value="sales_this_year">Sales this year</option>
        <option value="sales_last_year">Sales last year</option>
        <option value="growth">Growth</option>
        <option value="achievment">Achievement</option>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      <select class="form-control" onchange="actionSpesial('order',this.value)" id="order">
        <option value="ASC">Ascending</option>
        <option selected value="DESC">Descending</option>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      <select class="form-control" onchange="actionSpesial('cend',this.value)" id="cend"></select>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-3">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Sales Branch by MTD</h4>
        </div>
        <div id="sales_mtd"></div>
        <div id='loader_otr_salesmtd' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
      <div class="col-lg-3">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Sales Branch by YTD</h4>
        </div>
        <button type="button" onclick="detail_information()" class="btn detail_info btn-link none" style="position: absolute;top: 34px;z-index: 1;right: 35px;">Detail Information</button>
        <div id="sales_ytd"></div>
        <div id='loader_otr_salesytd' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
      <div class="col-lg-3">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Sales Contribution by Lini</h4>
        </div>
        <div id="sales_contribution_lini"></div>
        <div id='loader_otr_saleslini' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
      <div class="col-lg-3">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Trend Sales</h4>
        </div>
        <div id="trend_sales"></div>
        <div id='loader_otr_trendsales' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-4">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Sales By Customer Segment</h4>
        </div>
        <div id="sales_by_customer"></div>
        <div id='loader_otr_salescustomer' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
      <div class="col-lg-4">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Top 10 Brand</h4>
        </div>
        <div id="top_10_brand"></div>
        <div id='loader_otr_topbrand' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
      <div class="col-lg-4">
        <div style="background: #1f1fb9;display: grid;">
          <h4 class="text-center" style="color: white;">Sales by Brand</h4>
        </div>
        <div id="hide_salesprod" style="display: grid;">
          <table id="sales_product" style="width:100%">
            <thead>
              <th class="text-dark text-center">Material Name</th>
              <!-- <th class="text-dark text-center">Quantity</th> -->
              <th class="text-dark text-center">Value</th>
            </thead>
          </table>
        </div>
        <div id='loader_otr_salesprod' style='display: none; margin: 0 auto;'>
          <img style="display:block; margin: 0 auto;" src="<?= base_url() ?>asset/images/loader.gif" width="100px" height="100px">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<button class="button" onclick="myBtn()">Export data to CSV/PDF</button>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close opacity-1" onclick="closeModal()">&times;</span>
      <h2 class="">Export data to CSV/PDF</h2>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-6">
          <form>
            <fieldset>
              <div class="form-row">
                <div class="form-group OM col-md-12 mb-0 sheetlist_om">
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Sheet List</label>
                    <select onchange="disabledPDF(this)" class="form-control select" id="exampleFormControlSelect1">
                      <option value="">Please Select</option>
                      <option value="MTD">MTD</option>
                      <option value="YTD">YTD</option>
                      <option value="Sales Contribution">Sales Contribution</option>
                      <option value="Achivement Distributor">Achivement Distributor</option>
                      <option value="Sales Branch by MTD">Sales Branch by MTD</option>
                      <option value="Sales Branch by YTD">Sales Branch by YTD</option>
                      <option value="Sales Contribution by Lini">Sales Contribution by Lini</option>
                      <option value="Trend Sales">Trend Sales</option>
                      <option value="Sales Segment">Sales by Customer Segment</option>
                      <option value="Top 10 Brand">Top 10 Brand</option>
                      <option value="Sales by Branch">Sales by Branch</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-3">
                  <button onclick="downloadCSV()" type="button" class="btn btn_csv btn-primary btn-block mb-2">Download
                    CSV</button>
                </div>
                <div class="col-md-3">
                  <button onclick="exportToPDF()" type="button" class="btn btn_pdf btn-primary btn-block mb-2">Download
                    PDF</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>
<script>
  var value, start, end, end, data_table, Cend, order, by, starMonth, endMonth
  var map_achivement, paramdownload, sales_by_customer, top_10_brand, sales_mtd, sales_ytd, sales_contribution_lini, trend_sales
  var loading;
  var data_select = {}
  var data_download = {


  }
  var month = [{
      month: '01',
      month_name: 'January'
    }, {
      month: '02',
      month_name: 'Febuary'
    },
    {
      month: '03',
      month_name: 'March'
    },
    {
      month: '04',
      month_name: 'April'
    },
    {
      month: '05',
      month_name: 'May'
    },
    {
      month: '06',
      month_name: 'June'
    },
    {
      month: '07',
      month_name: 'July'
    },
    {
      month: '08',
      month_name: 'August'
    },
    {
      month: '09',
      month_name: 'September'
    },
    {
      month: '10',
      month_name: 'October'
    },
    {
      month: '11',
      month_name: 'November'
    },
    {
      month: '12',
      month_name: 'December'
    }
  ]

  $(document).ready(function() {
    setDate();

    data_table = $('#sales_product').DataTable({
      "scrollY": "300px",
      "scrollCollapse": true,
      "scrollX": true,
      "ordering": true,
      "bFilter": false,
      "bLengthChange": false,
      "bPaginate": false,
      pageLength: -1,

      "columns": [{
          data: 'material_brand',
          title: 'Material Name',
          className: 'text-left'
        },
        // {
        //   data: 'qty_ty',
        //   title: 'Quantity',
        //   className: 'text-left',
        //   render: $.fn.dataTable.render.number(".", ",", 0)
        // },
        {
          data: 'value',
          title: 'Value',
          className: 'text-left',
          render: $.fn.dataTable.render.number(".", ",", 0)
        },
      ]
    });

    $('#sales_product tbody').on('click', 'tr', function() {
      data_select.brand = data_table.row(this).data().material_brand || 'null';
      $("#data_selected").css("display", "initial");
      action()
    });

    endMonthF();
    action('ket')
    $("#data_selected").css("display", "none");
    // window.jsPDF = window.jspdf.jsPDF
    $('.btn_pdf').prop('disabled', true);
    $('.btn_csv').prop('disabled', true);
  });

  function setDate(){
    let curdate = new Date().getFullYear();
    
    const month_now = moment().format('MM'); // curdate.setMonth(curdate.getMonth())
    let month_start = moment().subtract(2,'month').format('MM'); // curdate.setMonth(curdate.getMonth() - 3)

    if (month_now === '01' || month_now === '02'){
      month_start = '01';
    }
    
    const limit_year = parseInt(curdate) - 2019;
    var dropdown = "";
    for(var i = limit_year; i >= 0; i--){
      tmp = moment().subtract(i, 'year').year();
      dropdown += "<option value="+tmp+">"+tmp+"</option>";
    }
    $('#year').html(dropdown);

    // console.log("tanggal end date = ",month_now,", -3 =",month_start);

    $('.start_date option[value='+month_start+']').attr('selected','selected');
    $('.end_date option[value='+month_now+']').attr('selected','selected');
    $('.year_select option[value='+$('#year option:last-child').val()+']').attr('selected','selected');
  }

  function detail_information() {
    // window.open("/dashboard-kimia-farma/index.php/Marketing_overview/detail?branch_name=" + data_select.ytd, "_self");
    window.open("/index.php/Marketing_overview/detail?branch_name=" + data_select.ytd, "_self");
  }

  function map(data) {
    map_achivement = Highcharts.mapChart('map_achivement', {
      chart: {
        map: 'countries/id/id-all'
      },

      title: {
        text: ''
      },

      subtitle: {
        text: ''
      },

      mapNavigation: {
        enabled: true,
        buttonOptions: {
          verticalAlign: 'bottom'
        }
      },
      credits: {
        enabled: false
      },

      colorAxis: {
        min: 0,
        minColor: '#fde3ce',
        maxColor: '#f4740b',
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.map = this.name;
                action();
                $("#data_selected").css("display", "initial");
              }
            }
          }
        }
      },
      series: [{
        data: data,
        name: 'Achievement %',
        states: {
          hover: {
            color: '#BADA55'
          }
        },
        dataLabels: {
          enabled: true,
          format: '{point.name}'
        }
      }],

      exporting: {
        enabled: false
      }
    });
  }

  function mtdF(data) {
    // console.log("data mtdf =", data)
    sales_mtd = Highcharts.chart('sales_mtd', {
      chart: {
        type: 'bar'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        min: 0,
        max: 4,
        tickLength: 0,
        categories: data.category,
        title: {
          text: null
        },
        scrollbar: {
          enabled: true
        },

      },
      yAxis: {
        min: 0,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          overflow: 'justify',
          formatter: function() {
            if (this.value >= 1000000000000) {
              return this.value / 1000000000000 + " Triliun"
            } else if (this.value >= 1000000000) {
              return this.value / 1000000000 + " Miliar"
            } else if (this.value >= 1000000) {
              return this.value / 1000000 + " Juta"

            }



          },
        },
      },
      tooltip: {
        formatter: function() {
          return 'sales This Year: <b>Rp ' + numberWithCommas(data.ty[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'sales Last Year: <b>Rp ' + numberWithCommas(data.ly[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Target: <b>Rp ' + numberWithCommas(data.target[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Achievement: <b>' + (data.achievement[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Growth: <b>' + (data.growth[this.series.data.indexOf(this.point)] || 0) + '</b></br>';
        }
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.mtd = this.category
                action()
                $("#data_selected").css("display", "initial");
              }
            }
          }
        },
        bar: {
          dataLabels: {
            enabled: true,
            allowOverlap: true,
            formatter: function() {
              if (this.y > 1000000000000) {
                return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Trilliun"
              } else if (this.y > 1000000000) {
                return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
              } else if (this.y > 1000000) {
                return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
              } else {
                return this.y
              }
            }
          }
        }
      },
      legend: {
        verticalAlign: 'bottom',
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: false
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Sales This Year',
        data: data.ty
      }, {
        name: 'Sales Last Year',
        data: data.ly
      }, {
        name: 'Target',
        data: data.target
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function ytdF(data) {
    sales_ytd = Highcharts.chart('sales_ytd', {
      chart: {
        type: 'bar'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        min: 0,
        max: 4,
        tickLength: 0,
        categories: data.category,
        title: {
          text: null
        },
        scrollbar: {
          enabled: true
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          overflow: 'justify',
          formatter: function() {
            if (this.value >= 1000000000000) {
              return this.value / 1000000000000 + " Triliun"
            } else if (this.value >= 1000000000) {
              return this.value / 1000000000 + " Miliar"
            } else if (this.value >= 1000000) {
              return this.value / 1000000 + " Juta"

            }



          },
        }
      },
      tooltip: {
        formatter: function() {
          console.log(data, 'data')
          return 'sales This Year: <b>Rp ' + numberWithCommas(data.ty[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'sales Last Year: <b>Rp ' + numberWithCommas(data.ly[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Target: <b>Rp ' + numberWithCommas(data.target[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Achievement: <b>' + (data.achievement[this.series.data.indexOf(this.point)] || 0) + '</b></br>' +
            'Growth: <b>' + (data.growth[this.series.data.indexOf(this.point)] || 0) + '</b></br>';;
        }
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.ytd = this.category
                action()
                $("#data_selected").css("display", "initial");
                $(".detail_info").css("display", "initial");
              }
            }
          }
        },
        bar: {
          dataLabels: {
            enabled: true,
            allowOverlap: true,
            formatter: function() {
              if (this.y > 1000000000000) {
                return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Trilliun"
              } else if (this.y > 1000000000) {
                return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
              } else if (this.y > 1000000) {
                return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
              } else {
                return this.y
              }
            }
          }
        }
      },
      legend: {
        verticalAlign: 'bottom',
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: false
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Sales This Year',
        data: data.ty
      }, {
        name: 'Sales Last Year',
        data: data.ly
      }, {
        name: 'Target',
        data: data.target
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function liniF(data) {
    sales_contribution_lini = Highcharts.chart('sales_contribution_lini', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: ''
      },
      tooltip: {
        formatter: function() {
          return 'Lini Name: <b>' + this.key + '</b></br>' +
            '% Kontribusi: <b>' + this.point.percentage.toFixed(2) + '%' + '</b></br>' +
            'Target Lini: <b>Rp ' + numberWithCommas(data[this.series.data.indexOf(this.point)].target || 0) + '</b></br>' +
            'Sales : <b>Rp ' + numberWithCommas(data[this.series.data.indexOf(this.point)].ty || 0) + '</b></br>' +
            'Sales Last Year: <b>Rp ' + numberWithCommas(data[this.series.data.indexOf(this.point)].ly || 0) + '</b></br>' +
            'Achievement: <b>' + parseFloat(data[this.series.data.indexOf(this.point)].Achievement || 0).toFixed(2) + '%' + '</b></br>' +
            'Growth: <b>' + parseFloat(data[this.series.data.indexOf(this.point)].Growth || 0).toFixed(2) + '%' + '</b></br>'
        }
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
        },
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: data,
        point: {
          events: {
            click: function(event) {
              data_select.lini = this.name
              action()
              $("#data_selected").css("display", "initial");
            }
          }
        },
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function trendF(data) {
    console.log('trendf = ', data)
    trend_sales = Highcharts.chart('trend_sales', {
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      tooltip: {
        formatter: function() {
          return 'Bulan: <b>' + this.x + '</b></br>' +
            'Value: <b>Rp ' + numberWithCommas(this.y || 0)+ '</b></br>' +
            'Target: <b>Rp ' + numberWithCommas(data.trend_sales[this.series.data.indexOf(this.point)].target || 0) + '</b></br>' +
            'Growth: <b>' + parseFloat(data.trend_sales[this.series.data.indexOf(this.point)].Growth || 0).toFixed(2) + '%' + '</b></br>' +
            'Achievement: <b>' + parseFloat(data.trend_sales[this.series.data.indexOf(this.point)].Achievement || 0).toFixed(2) + '%' + '</b>'
        }
      },
      xAxis: {
        categories: data.category
      },
      yAxis: {
        min: 0,
        title: {
          text: '',
          align: 'high'
        },
        labels: {
          overflow: 'justify',
          formatter: function() {
            if (this.value >= 1000000000000) {
              return this.value / 1000000000000 + " Triliun"
            } else if (this.value >= 1000000000) {
              return this.value / 1000000000 + " Miliar"
            } else if (this.value >= 1000000) {
              return this.value / 1000000 + " Juta"

            }



          },
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Value This Year',
        data: data.ty
      }, {
        name: 'Value Last Year',
        data: data.ly
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function top_brandF(data) {
    top_10_brand = Highcharts.chart('top_10_brand', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: ''
        },
        labels: {
          formatter: function() {
            if (this.value >= 1000000000000) {
              return this.value / 1000000000000 + " Triliun"
            } else if (this.value >= 1000000000) {
              return this.value / 1000000000 + " Miliar"
            } else if (this.value >= 1000000) {
              return this.value / 1000000 + " Juta"

            }



          },
        },
      },
      legend: {
        enabled: false
      },
      tooltip: {
        formatter: function() {
          return 'Value this year: <b>Rp ' + numberWithCommas(this.y || 0) + '</b></br>' +
            'Value last year: <b>Rp ' + numberWithCommas(data[this.x][2] || 0) + '</b></br>' +
            'Target : <b>Rp ' + numberWithCommas(data[this.x][5] || 0) + '</b></br>' +
            'Growth : <b>' + parseFloat(data[this.x][3] || 0).toFixed(2) + '%' + '</b></br>' +
            'Achievement : <b>' + parseFloat(data[this.x][4] || 0).toFixed(2) + '%' + '</b>'
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        color: '#f7a35c',
        name: '',
        data: data,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: '#000000',
          formatter: function() {
            if (this.y > 1000000000000) {
                return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Trilliun"
              } else if (this.y > 1000000000) {
                return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
              } else if (this.y > 1000000) {
                return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
              } else {
                return this.y
              }
          },
          // format: '{point.y:,.0f}', // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        },
      }],
      exporting: {
        enabled: false
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.brand = this.name
                action()
                $("#data_selected").css("display", "initial");
              }
            }
          }
        }
      }
    });
  }

  function segmentF(data) {
    sales_by_customer = Highcharts.chart('sales_by_customer', {
      chart: {
        type: 'column',
        inverted: true
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        min: 0,
        max: 4,
        tickLength: 0,
        scrollbar: {
          enabled: true
        },
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: ''
        },
        labels: {
          formatter: function() {
            if (this.value >= 1000000000000) {
              return this.value / 1000000000000 + " Triliun"
            } else if (this.value >= 1000000000) {
              return this.value / 1000000000 + " Miliar"
            } else if (this.value >= 1000000) {
              return this.value / 1000000 + " Juta"

            }



          },
        },
      },
      legend: {
        enabled: false
      },
      tooltip: {
        formatter: function() {
          return 'Value this year: <b>' + numberWithCommas(this.y || 0) + '</b></br>' +
            'Value last year: <b>' + numberWithCommas(data[this.x][2] || 0) + '</b></br>' +
            'Growth : <b>' + parseFloat(data[this.x][3] || 0).toFixed(2) + '%' + '</b>'
        }
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          color: '#f7a35c',
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.segment = this.name
                action()
                $("#data_selected").css("display", "initial");
              }
            }
          }
        }
      },
      series: [{
        name: '',
        data: data,
        dataLabels: {
          enabled: true,
          rotation: 0,
          color: '#000000',
          formatter: function() {
            if (this.y > 1000000000000) {
                return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Trilliun"
              } else if (this.y > 1000000000) {
                return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
              } else if (this.y > 1000000) {
                return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
              } else {
                return this.y
              }
          },
          y: 0, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }],
      exporting: {
        enabled: false
      }
    });
  }
  // Prepare demo data
  // Data is joined to map using value of 'hc-key' property by default.
  // See API docs for 'joinBy' for more info on linking data and map.  
  // Create the chart

  function result(x) {
    return x.y
  }

  function convertToInternationalCurrencySystem(labelValue) {

    // 12 Zeroes for triliun
    return Math.abs(Number(labelValue)) >= 1.0e+12

      ?
      (Math.abs(Number(labelValue)) / 1.0e+12).toFixed(2) + " Trilliun"
      // 9 Zeroes for Miliar 
      
      :
      Math.abs(Number(labelValue)) >= 1.0e+9

      ?
      (Math.abs(Number(labelValue)) / 1.0e+9).toFixed(2) + " Miliar"
      // 6 Zeroes for Juta

      
      :
      Math.abs(Number(labelValue)) >= 1.0e+6

      ?
      (Math.abs(Number(labelValue)) / 1.0e+6).toFixed(2) + " Juta"
      // Three Zeroes for Juta
      :
      Math.abs(Number(labelValue)) >= 1.0e+3

      ?
      (Math.abs(Number(labelValue)) / 1.0e+3).toFixed(2) + " K"

      :
      Math.abs(Number(labelValue));

  }

  function distributor_kftd(value, year, start, end, Cend, order, by, data_select) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/distributor_kftd') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        $("#loader_otr_kftd").show();
        // $('#main-container').hide();
        document.getElementById("kftd_hide").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.distributor_kftd = data.distributor_kftd
        
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);
      $("#loader_otr_kftd").hide();
      document.getElementById("kftd_hide").style.display = "block";
      $(".kftd").text(parseFloat(data.distributor_kftd || 0).toFixed(2) + '%');

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
      console.log("jqXHR_ajax customer = ", jqXHR)

    });
  }

  function other_kftd(value, year, start, end, Cend, order, by, data_select) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/other_kftd') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_nondist").show();
        // $('#main-container').hide();
        document.getElementById("other_kftd_hide").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.other_kftd = data.other_kftd
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);
      $(".other_kftd").text(parseFloat(data.other_kftd || 0).toFixed(2) + '%');

      $("#loader_otr_nondist").hide();
      document.getElementById("other_kftd_hide").style.display = "grid";
      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function get_MTD(value, year, start, end, Cend, order, by, data_select, mtd) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/MTD') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_mtd").show();
        document.getElementById("tabel_mtd").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.mtd = data["MTD"]
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // mtdF(mtd)

      $(".mtd").empty();
      $(".mtd").append('<td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["TMTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["MTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["LMTD"] || 0) + '</td><td class="text-center">' + parseFloat(data["MTD"]["AMTD"] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data["MTD"]["GMTD"] || 0).toFixed(2) + '%' + '</td>')
      $("#loader_otr_mtd").hide();
      document.getElementById("tabel_mtd").style.display = "grid";
      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function get_YTD(value, year, start, end, Cend, order, by, data_select, ytd) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/YTD') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        // 'start': JSON.stringify(year + '01'),
        // 'end': JSON.stringify(year + '' + end),
        'end': JSON.stringify(year + '12'),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        // 'Lstart': JSON.stringify(year - 1 + '01'),
        // 'Lend': JSON.stringify(year - 1 + '' + end),
        'Lend': JSON.stringify(year - 1 + '12'),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_ytd").show();
        // $('#tabel_ytd').hide();
        document.getElementById("tabel_ytd").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.ytd = data["YTD"]
        
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      $(".ytd").empty();

      $(".ytd").append('<td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["TYTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["YTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["LYTD"] || 0) + '</td><td class="text-center">' + parseFloat(data["YTD"]["AYTD"] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data["YTD"]["GYTD"] || 0).toFixed(2) + '%' + '</td>')
      // swal.close()

      $("#loader_otr_ytd").hide();
      document.getElementById("tabel_ytd").style.display = "grid";
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function marketing_expense(value, year, start, end, Cend, order, by, data_select) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/marketing_expense') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.marketing_expense = data['marketing_expense']
        
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      $(".expense").empty();

      $(".expense").append('<td class="text-center">' + parseFloat(data['marketing_expense']['MTD'] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data['marketing_expense']['YTD'] || 0).toFixed(2) + '%' + '</td>')

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_contribution(value, year, start, end, Cend, order, by, data_select) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_contribution') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_salescontrib").show();
        // $('#main-container').hide();
        document.getElementById("salescontrib").style.display = "none";
      },
      success: function(data) {

        data = JSON.parse(data)
        data_download.sales_contribution = data.sales_contribution
        
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);
      $(".contribution").empty()
      $(".contribution").append('<td class="text-center">' + parseFloat(data.sales_contribution.kf).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data.sales_contribution.non_kf).toFixed(2) + '%' + '</td>')
      // swal.close()

      $("#loader_otr_salescontrib").hide();
      document.getElementById("salescontrib").style.display = "grid";
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function achievement_distribution(value, year, start, end, Cend, order, by, data_select, data_map) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/achievement_distribution') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_map").show();
        // $('#main-container').hide();
        document.getElementById("map_achivement").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.achievement_distribution = data.achievement_distribution
        
        data.achievement_distribution.forEach(element => data_map.push([element.id_map, parseFloat(parseFloat(element.Achievement).toFixed(2))]));

        $("#loader_otr_map").hide();
        document.getElementById("map_achivement").style.display = "block";

        map(data_map)
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // $(".contribution").empty()

      // $(".contribution").append('<td class="text-center">' + parseFloat(data.sales_contribution.kf).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data.sales_contribution.non_kf).toFixed(2) + '%' + '</td>')

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_branch_mtd(value, year, start, end, Cend, order, by, data_select, mtd) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_branch_mtd') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_salesmtd").show();
        // $('#sales_mtd').hide();
        document.getElementById("sales_mtd").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.sales_branch_mtd = data['sales_branch_mtd']
        data['sales_branch_mtd'].forEach(function(element) {
          mtd.category.push(element.branch_A || 'null')
          mtd.ty.push(parseFloat(element.ty))
          mtd.ly.push(parseFloat(element.ly))
          mtd.target.push(parseFloat(element.target || 0))
          mtd.achievement.push(parseFloat(element.Achievement || 0).toFixed(2) + '%')
          mtd.growth.push(parseFloat(element.Growth || 0).toFixed(2) + '%')
        });

        $("#loader_otr_salesmtd").hide();
        document.getElementById("sales_mtd").style.display = "block";

        mtdF(mtd);

      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_branch_ytd(value, year, start, end, Cend, order, by, data_select, ytd) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_branch_ytd') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        // 'start': JSON.stringify(year + '01'),
        // 'end': JSON.stringify(year + '' + end),
        'end': JSON.stringify(year + '12'),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        // 'Lstart': JSON.stringify(year - 1 + '01'),
        // 'Lend': JSON.stringify(year - 1 + '' + Cend),
        'Lend': JSON.stringify(year - 1 + '12'),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_salesytd").show();
        // $('#main-container').hide();
        document.getElementById("sales_ytd").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.sales_branch_ytd = data['sales_branch_ytd']
        
        data['sales_branch_ytd'].forEach(function(element) {
          ytd.category.push(element.branch_A || 'null')
          ytd.ty.push(parseFloat(element.ty))
          ytd.ly.push(parseFloat(element.ly))
          ytd.target.push(parseFloat(element.target || 0))
          ytd.achievement.push(parseFloat(element.achievement || 0).toFixed(2) + '%')
          ytd.growth.push(parseFloat(element.growth || 0).toFixed(2) + '%')
        });

        $("#loader_otr_salesytd").hide();
        document.getElementById("sales_ytd").style.display = "block";
        ytdF(ytd)
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_lini(value, year, start, end, Cend, order, by, data_select) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_lini') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_saleslini").show();
        // $('#main-container').hide();
        document.getElementById("sales_contribution_lini").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.sales_lini = data.sales_lini
        
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);
      data.sales_lini.forEach(function(element) {
        element.y = parseFloat(element.ty);
        element.name = element.lini_A
      });

      $("#loader_otr_saleslini").hide();
      // $('#main-container').hide();
      document.getElementById("sales_contribution_lini").style.display = "block";
      liniF(data.sales_lini)


    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function get_trend_sales(trend, value, year, start, end, Cend, order, by) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/trend_sales') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_trendsales").show();
        // $('#main-container').hide();
        document.getElementById("trend_sales").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data_res = JSON.parse(data)
        data_download.trend_sales = data_res.trend_sales
        
        data_res.trend_sales.forEach(function(element) {
          trend.category.push(element.bulan_A)
          trend.ty.push(parseFloat(element.ty))
          trend.ly.push(parseFloat(element.ly))
        });

        trend.trend_sales = data_res.trend_sales;
        console.log(trend, 'trend')

        $("#loader_otr_trendsales").hide();
        document.getElementById("trend_sales").style.display = "block";
        trendF(trend)
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_segment(value, year, start, end, Cend, order, by, segment) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_segment') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_salescustomer").show();
        // $('#main-container').hide();
        document.getElementById("sales_by_customer").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.sales_segment = data.sales_segment
        
        data.sales_segment.forEach(element => segment.push([element.segment_A ? element.segment_A : 'null', parseFloat(element.ty), element.ly, element.Growth, element.Achievement, element.target]));
        console.log("sales segment = ", data.sales_segment)

        $("#loader_otr_salescustomer").hide();
        document.getElementById("sales_by_customer").style.display = "block";
        segmentF(segment);
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function get_top_brand(value, year, start, end, Cend, order, by, top_brand) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/top_brand') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_topbrand").show();
        // $('#main-container').hide();
        document.getElementById("top_10_brand").style.display = "none";

      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.top_brand=data.top_brand
        
        data.top_brand.forEach(element => top_brand.push([element.brand_A, parseFloat(element.ty), element.ly, element.Growth, element.Achievement, element.target]));

      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      $("#loader_otr_topbrand").hide();
      document.getElementById("top_10_brand").style.display = "block";
      top_brandF(top_brand);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function sales_product(value, year, start, end, Cend, order, by, length, newRow) {
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/sales_product') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        // 'material_brand': JSON.stringify(data_select.material_brand),
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        // loading();
        $("#loader_otr_salesprod").show();
        // $('#main-container').hide();
        document.getElementById("hide_salesprod").style.display = "none";
      },
      success: function(data) {
        // swal.close();

        data = JSON.parse(data)
        data_download.sales_product=data.sales_product
        length = data.sales_product.length;
        newRow = data.sales_product

        $("#loader_otr_salesprod").hide();
        // $('#main-container').hide();
        document.getElementById("hide_salesprod").style.display = "grid";

        if (length !== 0) {
          data_table.clear().draw();
          data_table.rows.add(newRow);
          data_table.columns.adjust().draw();
        } else {
          data_table.clear().draw();
        }
      }
    }).done(function(data) {
      enabled_action()
      data = JSON.parse(data);

      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {
      enabled_action()
    });
  }

  function enabled_action()
  {
    loading += 1
    if (loading == 14) {
      $("#goaction").prop("disabled",false);
      $("#by").prop("disabled",false);
      $("#cend").prop("disabled",false);
      $("#order").prop("disabled",false);            
    }    
  }

  function disabled_action()
  {    
    loading = 0
    $("#goaction").prop("disabled",true);
    $("#by").prop("disabled",true);
    $("#cend").prop("disabled",true);
      $("#order").prop("disabled",true)
  }

  function action(ket, param) {    
    if (ket) {
      $("#by").val("default")
    }        
    var length;
    var newRow = [];
    var data_map = [];
    var top_brand = [];
    var segment = [];
    var trend = {
      category: [],
      ty: [],
      ly: []
    };
    var mtd = {
      category: [],
      ty: [],
      ly: [],
      target: [],
      achievement: [],
      growth: []
    };
    var ytd = {
      category: [],
      ty: [],
      ly: [],
      target: [],
      achievement: [],
      growth: []

    };
    var lini = [];
    if (ket) {
      value = $("#value").val();
      start = $("#start").val();
      end = $("#end").val();
      Cend = $("#cend").val();
      year = $("#year").val();
      order = $("#order").val();
      by = $("#by").val();
    }    

    if (start <= end) {
      // loading()
      disabled_action()
      distributor_kftd(value, year, start, end, Cend, order, by, data_select)
      other_kftd(value, year, start, end, Cend, order, by, data_select)
      get_MTD(value, year, start, end, Cend, order, by, data_select, mtd)
      get_YTD(value, year, start, end, Cend, order, by, data_select, ytd)
      marketing_expense(value, year, start, end, Cend, order, by, data_select)
      sales_contribution(value, year, start, end, Cend, order, by, data_select)
      achievement_distribution(value, year, start, end, Cend, order, by, data_select, data_map)
      sales_branch_mtd(value, year, start, end, Cend, order, by, data_select, mtd)
      sales_branch_ytd(value, year, start, end, Cend, order, by, data_select, ytd)
      sales_lini(value, year, start, end, Cend, order, by, data_select)
      get_trend_sales(trend, value, year, start, end, Cend, order, by)
      sales_segment(value, year, start, end, Cend, order, by, segment)
      get_top_brand(value, year, start, end, Cend, order, by, top_brand)
      sales_product(value, year, start, end, Cend, order, by, length, newRow)
      // swal.close();
    } else if (start > end) {
      dangerNotif('Tanggal Start Kurang Dari Tanggal End');
      $('#start option').prop('selected', function() {
        return this.defaultSelected;
      });
      $('#end option').prop('selected', function() {
        return this.defaultSelected;
      });
      // action()
    } else {
      dangerNotif('Tanggal End Kurang Dari Tanggal Start');
      $('#end option').prop('selected', function() {
        return this.defaultSelected;
      });
      $('#start option').prop('selected', function() {
        return this.defaultSelected;
      });
    }

    // $.ajax({
    // url: "<?php // echo base_url('index.php/Marketing_overview/getSummary') 
              ?>",
    //   method: 'POST',
    //   data: {
    //     'value': JSON.stringify(value),
    //     'start': JSON.stringify(year + '' + start),
    //     'end': JSON.stringify(year + '' + end),
    //     'Lstart': JSON.stringify(year - 1 + '' + start),
    //     'Lend': JSON.stringify(year - 1 + '' + end),
    //     'Cend': JSON.stringify(year + '' + Cend),
    //     'order': JSON.stringify(order),
    //     'by': JSON.stringify(by),
    //     // 'material_brand': JSON.stringify(data_select.material_brand),
    //     'mtd': JSON.stringify(data_select.mtd),
    //     'ytd': JSON.stringify(data_select.ytd),
    //     'lini': JSON.stringify(data_select.lini),
    //     'segment': JSON.stringify(data_select.segment),
    //     'brand': JSON.stringify(data_select.brand),
    //     'map': JSON.stringify(data_select.map),
    //   },
    //   beforeSend: function() {
    // loading();
    //   },
    //   success: function(data) {
    // swal.close();

    //     data = JSON.parse(data)
    //     data_download = data
    //     length = data.sales_product.length;
    //     newRow = data.sales_product
    //     data.achievement_distribution.forEach(element => data_map.push([element.id_map, parseFloat(parseFloat(element.Achievement).toFixed(2))]));
    //     data.top_brand.forEach(element => top_brand.push([element.brand_A, parseFloat(element.ty), element.ly, element.Growth, element.Achievement, element.target]));
    //     data.sales_segment.forEach(element => segment.push([element.segment_A ? element.segment_A : 'null', parseFloat(element.ty), element.ly, element.Growth, element.Achievement, element.target]));
    //     data.trend_sales.forEach(function(element) {
    //       trend.category.push(element.bulan_A)
    //       trend.ty.push(parseFloat(element.ty))
    //       trend.ly.push(parseFloat(element.ly))
    //     });
    //     data['sales_branch_mtd'].forEach(function(element) {
    //       mtd.category.push(element.branch_A)
    //       mtd.ty.push(parseFloat(element.ty))
    //       mtd.ly.push(parseFloat(element.ly))
    //       mtd.target.push(parseFloat(element.target))
    //       mtd.achievement.push(parseFloat(element.Achievement || 0).toFixed(2) + '%')
    //       mtd.growth.push(parseFloat(element.Growth || 0).toFixed(2) + '%')
    //     });
    //     data['sales_branch_ytd'].forEach(function(element) {
    //       ytd.category.push(element.branch_A)
    //       ytd.ty.push(parseFloat(element.ty))
    //       ytd.ly.push(parseFloat(element.ly))
    //       ytd.target.push(parseFloat(element.target))
    //       ytd.achievement.push(parseFloat(element.achievement || 0).toFixed(2) + '%')
    //       ytd.growth.push(parseFloat(element.growth || 0).toFixed(2) + '%')
    //     });
    //     trend.trend_sales = data.trend_sales;
    //   }
    // }).done(function(data) {
    //   data = JSON.parse(data);
    //   data.sales_lini.forEach(function(element) {
    //     element.y = parseFloat(element.ty);
    //     element.name = element.lini_A
    //   });
    //   liniF(data.sales_lini)
    //   map(data_map)
    //   top_brandF(top_brand);
    //   segmentF(segment);
    //   trendF(trend)
    //   mtdF(mtd)
    //   ytdF(ytd)
    //   $(".kftd").text(parseFloat(data.distributor_kftd || 0).toFixed(2) + '%');
    //   $(".other_kftd").text(parseFloat(data.other_kftd || 0).toFixed(2) + '%');
    //   $(".mtd").empty();
    //   $(".ytd").empty();
    //   $(".expense").empty();
    //   $(".contribution").empty()
    //   $(".mtd").append('<td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["TMTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["MTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["MTD"]["LMTD"] || 0) + '</td><td class="text-center">' + parseFloat(data["MTD"]["AMTD"] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data["MTD"]["GMTD"] || 0).toFixed(2) + '%' + '</td>')

    //   $(".ytd").append('<td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["TYTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["YTD"] || 0) + '</td><td class="text-center">' + convertToInternationalCurrencySystem(data["YTD"]["LYTD"] || 0) + '</td><td class="text-center">' + parseFloat(data["YTD"]["AYTD"] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data["YTD"]["GYTD"] || 0).toFixed(2) + '%' + '</td>')

    //   $(".expense").append('<td class="text-center">' + parseFloat(data['marketing_expense']['MTD'] || 0).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data['marketing_expense']['YTD'] || 0).toFixed(2) + '%' + '</td>')
    //   $(".contribution").append('<td class="text-center">' + parseFloat(data.sales_contribution.kf).toFixed(2) + '%' + '</td><td class="text-center">' + parseFloat(data.sales_contribution.non_kf).toFixed(2) + '%' + '</td>')
    //   if (length !== 0) {
    //     data_table.clear().draw();
    //     data_table.rows.add(newRow);
    //     data_table.columns.adjust().draw();
    //   } else {
    //     data_table.clear().draw();
    //   }
    //   swal.close()
    // }).fail(function(jqXHR, textStatus, errorThrown) {

    // });
    endMonthF();
  }

  function starMonthF(value) {
    $("#cend").empty();
    starMonth = parseFloat(value) || parseFloat($("#start").val());
    endMonth = parseFloat($("#end").val())
    var x = ''
    month.slice(starMonth - 1, endMonth).forEach(function(element) {
      x += "<option value='" + element.month + "'>" + element.month_name + "</option>";
    });
    $("#cend").append(x);
  }

  function endMonthF(value) {
    $("#cend").empty();
    starMonth = parseFloat($("#start").val())
    endMonth = parseFloat(value) || parseFloat($("#end").val());
    var x = '';
    month.slice(starMonth - 1, endMonth).forEach(function(element) {
      x += "<option value='" + element.month + "'>" + element.month_name + "</option>";
    });
    $("#cend").append(x);
  }

  function actionSpesial(ket, param) {
    var length;
    var mtd = {
      category: [],
      ty: [],
      ly: [],
      target: [],
      achievement: [],
      growth: []
    };
    var ytd = {
      category: [],
      ty: [],
      ly: [],
      target: [],
      achievement: [],
      growth: []
    };
    value = ket == 'value' ? param : $("#value").val();
    start = ket == 'start' ? param : $("#start").val();
    end = ket == 'end' ? param : $("#end").val();
    Cend = ket == 'cend' ? param : $("#cend").val();
    year = ket == 'year' ? param : $("#year").val();
    order = ket == 'order' ? param : $("#order").val();
    by = ket == 'by' ? param : $("#by").val();
    by = by === 'default' ? 'sales_this_year' : by;
    forMTD = ket == 'cend' ? true : false;
    // loading()
    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/getSpesial') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'Lstart': JSON.stringify(year - 1 + '' + start),
        'Lend': JSON.stringify(year - 1 + '' + end),
        'Cend': JSON.stringify(year + '' + Cend),
        'order': JSON.stringify(order),
        'by': JSON.stringify(by),
        'forMTD': forMTD,
        'mtd': JSON.stringify(data_select.mtd),
        'ytd': JSON.stringify(data_select.ytd),
        'lini': JSON.stringify(data_select.lini),
        'segment': JSON.stringify(data_select.segment),
        'brand': JSON.stringify(data_select.brand),
        'map': JSON.stringify(data_select.map),
      },
      beforeSend: function() {
        $("#loader_otr_salesmtd").show();
        // $('#sales_mtd').hide();
        document.getElementById("sales_mtd").style.display = "none";
        // loading();
        if (!forMTD) {
          $("#loader_otr_salesytd").show();
          // $('#main-container').hide();
          document.getElementById("sales_ytd").style.display = "none";
        }
      },
      success: function(data) {
        data = JSON.parse(data)
        data['sales_branch_mtd'].forEach(function(element) {
          mtd.category.push(element.branch_A || 'null')
          mtd.ty.push(parseFloat(element.ty))
          mtd.ly.push(parseFloat(element.ly))
          mtd.target.push(parseFloat(element.target || 0))
          mtd.achievement.push(parseFloat(element.Achievement || 0).toFixed(2) + '%')
          mtd.growth.push(parseFloat(element.Growth || 0).toFixed(2) + '%')
        });
        if (!forMTD) {
          data['sales_branch_ytd'].forEach(function(element) {
            ytd.category.push(element.branch_A || 'null')
            ytd.ty.push(parseFloat(element.ty))
            ytd.ly.push(parseFloat(element.ly))
            ytd.target.push(parseFloat(element.target || 0))
            ytd.achievement.push(parseFloat(element.achievement || 0).toFixed(2) + '%')
            ytd.growth.push(parseFloat(element.growth || 0).toFixed(2) + '%')
          });
        }
      }
    }).done(function(data) {
      data = JSON.parse(data);
      $("#loader_otr_salesmtd").hide();
      document.getElementById("sales_mtd").style.display = "block";
      mtdF(mtd)
      if (!forMTD) {
        $("#loader_otr_salesytd").hide();
        document.getElementById("sales_ytd").style.display = "block";
        ytdF(ytd)
      }
      // swal.close()
    }).fail(function(jqXHR, textStatus, errorThrown) {

    });
  }

  function clearF() {
    $("#data_selected").css("display", "none");
    $(".detail_info").css("display", "none");
    data_select = {}
    action()
  }

  function loading() {
    swal({
      title: 'Tunggu Sebentar...',
      text: ' ',
      icon: 'info',
      buttons: false,
      closeOnClickOutside: false,
    });
  }

  function dangerNotif(message) {
    swal({
      title: 'Terjadi Kesalahan',
      text: message,
      icon: 'error',
      buttons: false,
      closeOnClickOutside: false,
      timer: 5000
    });
  }

  function numberWithCommas(x) {
    if (x) {
      x = parseFloat(x)
      x = x % 1 != 0 ? x : parseFloat(x).toFixed(0);
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    return x
  }

  function myBtn() {
    $("#myModal").show();
    // map_achivement.exportChart({
    // 		type: 'application/pdf'
    // 	})    
  }

  function closeModal() {
    $("#myModal").hide();
  }

  function exportToPDF() {
    if (paramdownload === "Achivement Distributor") {
      map_achivement.exportChart({
        filename: 'Achivement Distributor_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Sales Branch by MTD") {
      sales_mtd.exportChart({
        filename: 'Sales Branch by MTD_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Sales Branch by YTD") {
      sales_ytd.exportChart({
        filename: 'Sales Branch by YTD' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Sales Contribution by Lini") {
      sales_contribution_lini.exportChart({
        filename: 'Sales Contribution by Lini_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Trend Sales") {
      trend_sales.exportChart({
        filename: 'Trend Sales_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Sales Segment") {
      sales_by_customer.exportChart({
        filename: 'Sales By Customer Segment_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Top 10 Brand") {
      top_10_brand.exportChart({
        filename: 'Top 10 Brand_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    }

  }

  function disabledPDF(param) {
    $('.btn_pdf').prop('disabled', false);
    $('.btn_csv').prop('disabled', false);
    paramdownload = param.value
    if (param.value === "MTD" || param.value === "YTD" || param.value === "MTD" || param.value === "Sales Contribution" || param.value === "Sales by Branch") {
      $('.btn_pdf').prop('disabled', true);
      $('.btn_csv').prop('disabled', false);
    } else if (!param.value) {
      $('.btn_pdf').prop('disabled', true);
      $('.btn_csv').prop('disabled', true);
    } else {
      $('.btn_pdf').prop('disabled', false);
      $('.btn_csv').prop('disabled', false);
    }
  }


  function downloadCSV() {
    console.log("ni pas awal awal masuk fungsi : ",data_download);
    if (paramdownload === 'Sales by Branch') {
      var headers = {
        material_brand: 'Material Brand',
        value: "Value"
      };
      var itemsFormatted = [];
      // format the data
      data_download.sales_product.forEach((item) => {
        itemsFormatted.push({
          material_brand: item.material_brand,
          value: item.value
        });
      });
      var fileTitle = 'Sales by Branch' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Top 10 Brand') {
      var headers = {
        brand_A: 'Name Brand',
        target: "Target",
        ty: "Value This Year",
        ly: "Value Last Year",
        Achievement: "Achievement",
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.top_brand.forEach((item) => {
        itemsFormatted.push({
          brand_A: item.brand_A,
          target: item.target,
          ty: item.ty,
          ly: item.ly,
          Achievement: item.Achievement + ' %',
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Sales by Branch' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Sales Segment') {
      var headers = {
        segment_A: 'Name Segment',
        ty: "Value This Year",
        ly: "Value Last Year",
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.sales_segment.forEach((item) => {
        itemsFormatted.push({
          segment_A: item.segment_A,
          ty: item.ty,
          ly: item.ly,
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Sales By Customer Segment' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Trend Sales') {
      var headers = {
        bulan_A: 'Name Segment',
        target: 'Target',
        ty: "Value This Year",
        ly: "Value Last Year",
        Achievement: 'Achievement',
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.trend_sales.forEach((item) => {
        itemsFormatted.push({
          bulan_A: item.bulan_A,
          target: item.target,
          ty: item.ty,
          ly: item.ly,
          Achievement: item.Achievement + ' %',
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Trend Sales' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Sales Contribution by Lini') {
      var headers = {
        lini_A: 'Lini Name',
        target: 'Target',
        ty: "Value This Year",
        ly: "Value Last Year",
        Achievement: 'Achievement',
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.sales_lini.forEach((item) => {
        itemsFormatted.push({
          lini_A: item.lini_A,
          target: item.target,
          ty: item.ty,
          ly: item.ly,
          Achievement: item.Achievement + ' %',
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Sales Contribution by Lini' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Sales Branch by YTD') {
      var headers = {
        branch_A: 'Branch Name',
        target: 'Target',
        ty: "Value This Year",
        ly: "Value Last Year",
        Achievement: 'Achievement',
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.sales_branch_ytd.forEach((item) => {
        itemsFormatted.push({
          branch_A: item.branch_A,
          target: item.target,
          ty: item.ty,
          ly: item.ly,
          Achievement: item.Achievement + ' %',
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Sales Branch by YTD' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Sales Branch by MTD') {
      var headers = {
        branch_A: 'Branch Name',
        target: 'Target',
        ty: "Value This Year",
        ly: "Value Last Year",
        Achievement: 'Achievement',
        Growth: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      data_download.sales_branch_mtd.forEach((item) => {
        itemsFormatted.push({
          branch_A: item.branch_A,
          target: item.target,
          ty: item.ty,
          ly: item.ly,
          Achievement: item.Achievement + ' %',
          Growth: item.Growth + ' %'
        });
      });
      var fileTitle = 'Sales Branch by MTD' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Achivement Distributor') {
      var headers = {
        province_A: 'Province Name',
        Achievement: 'Achievement',
      };
      var itemsFormatted = [];
      // format the data
      data_download.achievement_distribution.forEach((item) => {
        itemsFormatted.push({
          province_A: item.province_A,
          Achievement: item.Achievement + ' %',
        });
      });
      var fileTitle = 'Achivement Distributor' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'MTD') {
      var headers = {
        TMTD: "Target",
        MTD: "Value This Year",
        LMTD: "Value Last Year",
        AMTD: "Achivement",
        GMTD: "Growth"
      };
      var itemsFormatted = [];
      // format the data
      itemsFormatted.push({
        TMTD: data_download['mtd'].TMTD,
        MTD: data_download['mtd'].MTD,
        LMTD: data_download['mtd'].LMTD,
        AMTD: data_download['mtd'].AMTD + ' %',
        GMTD: data_download['mtd'].GMTD + ' %',
      });
      var fileTitle = 'MTD' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'YTD') {
      var headers = {
        TYTD: "Target",
        YTD: "Value This Year",
        LYTD: "Value Last Year",
        AYTD: "Achivement",
        GYTD: "Growth"
      };
      var itemsFormatted = [];
      // format the data    
      itemsFormatted.push({
        TYTD: data_download['ytd'].TYTD,
        YTD: data_download['ytd'].YTD,
        LYTD: data_download['ytd'].LYTD,
        AYTD: data_download['ytd'].AYTD + ' %',
        GYTD: data_download['ytd'].GYTD + ' %',
      });
      var fileTitle = 'YTD' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    } else if (paramdownload === 'Sales Contribution') {
      var headers = {
        kf: 'KF',
        non_kf: 'Non KF',
      };
      var itemsFormatted = [];
      // format the data
      // baris lama
      // data_download.sales_contribution.forEach((item) => {
      //   itemsFormatted.push({
      //     kf: item.kf + ' %',
      //     Achievement: item.non_kf + ' %',
      //   });
      // });

      itemsFormatted.push({
        kf: data_download['sales_contribution'].kf+ ' %',
        non_kf: data_download['sales_contribution'].non_kf+ ' %',
      });
      var fileTitle = 'Sales Contribution' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    }
    exportCSVFile(headers, itemsFormatted, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
  }

  function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line != '') line += ','

        line += array[i][index];
      }

      str += line + '\r\n';
    }

    return str;
  }

  function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
      items.unshift(headers);
    }

    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], {
      type: 'text/csv;charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
</script>
