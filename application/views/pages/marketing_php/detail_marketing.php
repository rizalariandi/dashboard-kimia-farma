<style>
  #map_achivement {
    height: 500px;
    max-width: 100%;
    margin: 0 auto;
  }

  .loading {
    margin-top: 10em;
    text-align: center;
    color: gray;
  }

  .btn-red {
    position: absolute !important;
    margin-left: 10px !important;
  }

  .select-all {
    display: inline-table;
    width: 100%;
  }

  .slider.slider-horizontal {
    width: 100%;
  }

  .none {
    display: none;
  }

  #sales_brand .text-left {
    cursor: pointer;
  }

  #sales_product .text-left {
    cursor: pointer;
  }
</style>
<div class="row">
  <h2 class="text-center">Information Detail <button onclick="clearF()" id="data_selected" class="btn btn-danger btn-red"><i class="fa fa-close"></i></button></h2>
  <h4 class="text-center" id='all'>All</h4>
  <button onclick="backpage()" type="button" class="btn btn-secondary none back" style="position: absolute;top: 7em;margin-left: 2em;"><i class="fas fa-angle-double-left"></i> Back Page</button>
  <div class="col-lg-2">
    <div class="form-group">
      <label for="exampleFormControlSelect1">Value</label>
      <select class="form-control" id="value">
        <option value="hna_ty">HNA</option>
        <option value="hjp_ty">HJP</option>
        <option value="hjp_ptd_ty">HJP-PTD</option>
        <option value="hjd_ty">HJD</option>
        <option value="hpp_ty">HPP</option>
        <option value="hna_ptd_ty">HNA-PTD</option>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">
      <label for="exampleFormControlSelect1">Layanan Group</label>
      <select multiple="multiple" name="somename0" id="layanan_group">
        <!-- <option value="">Choose Layanan Group</option> -->
        <?php

        foreach ($group_layanan as $key => $value) {
          echo '<option value="' . $value['layanan_group'] . '">' . $value['layanan_group'] . '</option>';
        }
        ?>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group layanan_name">
      <label for="exampleFormControlSelect1">Layanan Name <img src="<?= base_url() ?>asset/images/loader_black.gif" style="width: 15px" class="loading_layanan" alt="loading" ></label>
      <select multiple="multiple" name="somename0" id="layanan_name">
        <!-- <option value="">Choose Layanan Name</option> -->
        <?php

        foreach ($layanan as $key => $value) {
          echo '<option value="' . $value['layanan_name'] . '">' . $value['layanan_name'] . '</option>';
        }
        ?>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group start_month">
      <label for="exampleFormControlSelect1">Start Month</label>
      <select class="form-control" id="start">
        <option value="01">January</option>
        <option value="02">Febuary</option>
        <option value="03">March</option>
        <option value="04">April</option>
        <option value="05">May</option>
        <option value="06">June</option>
        <option value="07">July</option>
        <option value="08">August</option>
        <option value="09">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group end_month">
      <label for="exampleFormControlSelect1">End Month</label>
      <select class="form-control" id="end">
        <option value="01">January</option>
        <option value="02">Febuary</option>
        <option value="03">March</option>
        <option value="04">April</option>
        <option value="05">May</option>
        <option value="06">June</option>
        <option value="07">July</option>
        <option value="08">August</option>
        <option value="09">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
    </div>
  </div>
  <div class="col-lg-1">
    <div class="form-group year_select">
      <label for="exampleFormControlSelect1">Year</label>
      <select class="form-control" id="year" style="display: inline-flex;">
        <!-- <option value="2019">2019</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option> -->
      </select>
    </div>
  </div>
  <div class="col-lg-1">
    <button type="button" onclick="running(true)" style="margin-top: 2em;" class="btn btn-success">Go</button>
  </div>
</div>
<div class="row">
  <div class="container col-lg-6">
    <div class="col-12" style="background: #1f1fb9; display: grid;">
      <h4 class="text-center" style="color: white;">Sales by Customer Segment</h4>
    </div>
    <div id="sales_segment_hide" style="display:grid;">
      <div id="sales_by_customer_segment"></div>
    </div>
    <div id='loader_sales_segment' style='position:block;text-align: center'>
      <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
  <div class="col-lg-6">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Sales by Customer Name</h4>
    </div>
    <div id="sales_cust_name_hide">
      <div class="row" style="background: white;padding-top: 5px;">
        <div class="col-lg-2"></div>
        <div class="col-lg-1">
          <button type="button" onclick="preview()" class="btn btn-light"><b> < </b></button>
        </div>
        <div class="col-lg-6" style="font-size: 15px;font-weight: 500;padding-top: 5px;">
          Showing <span id="start_"> 1 </span> to <span id="end_"> 50 </span> of <span id="total_data"> 0 </span> entries
        </div>
        <div class="col-lg-1">
          <button type="button" onclick="next()" class="btn btn-light"><b>></b></button>
        </div>
        <div class="col-lg-2"><button type="button" onclick="sales_by_customer_name_withbtn()" class="btn btn-success">Reload <i class="fa fa-refresh"></i></button></div>
      </div>
      <div id="sales_by_customer_name" style="display:block"></div>
    </div>
    <div id='loader_sales_customer' style='position:block; text-align: center'>
          <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
  </div>
</div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Sales by Product Brand</h4>
    </div>
    <div id="TableBrand" style="display: grid;" >     
      <table id="sales_brand" style="width:100%" >
        <thead>
          <th class="text-dark text-center">Material Brand</th>
          <th class="text-dark text-center">Value</th>
        </thead>
      </table>
    </div>
    <div id='loader_sales_brand' style='position:block; text-align: center;'>
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div> 
  </div>
  <div class="col-lg-6">
    <div style="background: #1f1fb9;display: grid;">
      <h4 class="text-center" style="color: white;">Sales by Product</h4>
    </div>

    <div id="TableProduct"  style="display: grid;">
          <table id="sales_product" style="width:100%">
            <thead>
              <th class="text-dark text-center">Material Name</th>
              <th class="text-dark text-center">Quantity</th>
              <th class="text-dark text-center">Value</th>
            </thead>
          </table>
    </div>
    <div id='loader_sales_product' style='position:block;
      text-align: center;'
    >
        <img style="display:block; margin: 0 auto;" src="<?=base_url()?>asset/images/loader.gif" width="100px" height="100px">
    </div>
    

  </div>
</div>
<br>
<button class="button" onclick="myBtn()">Export data to CSV/PDF</button>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close opacity-1" onclick="closeModal()">&times;</span>
      <h2 class="">Export data to CSV/PDF</h2>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-6">
          <form>
            <fieldset>
              <div class="form-row">
                <div class="form-group OM col-md-12 mb-0 sheetlist_om">
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Sheet List</label>
                    <select onchange="disabledPDF(this)" class="form-control select" id="exampleFormControlSelect1">
                      <option value="">Please Select</option>
                      <option value="Sales by Customer Segment">Sales by Customer Segment</option>
                      <option value="Sales by Customer Name">Sales by Customer Name</option>
                      <option value="Sales by Product Brand">Sales by Product Brand</option>
                      <option value="Sales by Product">Sales by Product</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-3">
                  <button onclick="downloadCSV()" type="button" class="btn btn_csv btn-primary btn-block mb-2">Download
                    CSV</button>
                </div>
                <div class="col-md-3">
                  <button onclick="exportToPDF()" type="button" class="btn btn_pdf btn-primary btn-block mb-2">Download
                    PDF</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var value, start, end, year, data_table_product, data_table_brand, branch, layanan_group, layanan_name;
  var data_customer_name, paramdownload, sales_by_customer_segment_data, sales_by_customer_name_data;
  var data_download = []
  var start_bar = 0;
  var end_bar = 50;
  var data_select = {};
  $("#data_selected").css("display", "none");
  var month = [{
      month: '01',
      month_name: 'January'
    }, {
      month: '02',
      month_name: 'Febuary'
    },
    {
      month: '03',
      month_name: 'March'
    },
    {
      month: '04',
      month_name: 'April'
    },
    {
      month: '05',
      month_name: 'May'
    },
    {
      month: '06',
      month_name: 'June'
    },
    {
      month: '07',
      month_name: 'July'
    },
    {
      month: '08',
      month_name: 'August'
    },
    {
      month: '09',
      month_name: 'September'
    },
    {
      month: '10',
      month_name: 'October'
    },
    {
      month: '11',
      month_name: 'November'
    },
    {
      month: '12',
      month_name: 'December'
    }
  ]

  $(document).ready(function() {
    setDate();
    data_table_product = $('#sales_product').DataTable({
      "scrollY": "300px",
      "scrollCollapse": true,
      "scrollX": true,
      "ordering": true,
      "bFilter": false,
      "bLengthChange": false,
      "bPaginate": false,
      pageLength: -1,

      "columns": [{
          data: 'material_name',
          title: 'Material Name',
          className: 'text-left'
        },
        {
          data: 'qty',
          title: 'Quantity',
          className: 'text-left',
          render: $.fn.dataTable.render.number(".", ",", 0)
        },
        {
          data: 'value',
          title: 'Value',
          className: 'text-left',
          render: $.fn.dataTable.render.number(".", ",", 0)
        },
      ]
    });
    $('#sales_product tbody').on('click', 'tr', function() {
      data_select.material_name = data_table_product.row(this).data().material_name || 'null';
      $("#data_selected").css("display", "initial");
      running(false)
    });
  

    data_table_brand = $('#sales_brand').DataTable({
      "scrollY": "300px",
      "scrollCollapse": true,
      "scrollX": true,
      "ordering": true,
      "bFilter": false,
      "bLengthChange": false,
      "bPaginate": false,
      pageLength: -1,

      "columns": [{
          data: 'material_brand',
          title: 'Material Brand',
          className: 'text-left'
        },
        {
          data: 'value',
          title: 'Value',
          className: 'text-left',
          render: $.fn.dataTable.render.number(".", ",", 0)
        },
      ]
    });

    $('#sales_brand tbody').on('click', 'tr', function() {
      data_select.material_brand = data_table_brand.row(this).data().material_brand || 'null';
      $("#data_selected").css("display", "initial");
      running(false)
    });

    endMonthF();
    // action()
    $('#layanan_group').SumoSelect({
      selectAll: true,
      okCancelInMulti: true
    });
    $('#layanan_name').SumoSelect({
      selectAll: true
    });
    $('#value').SumoSelect({});
    $('#start').SumoSelect({});
    $('#end').SumoSelect({});
    $('#year').SumoSelect({});
    $('#layanan_group')[0].sumo.selectAll();
    $("#layanan_group ~ .optWrapper .MultiControls .btnOk").click(function() {    
      $(".loading_layanan").show();  
      var num = $('.layanan_name option').length;
      for (var i = num; i >= 1; i--) {
        $('#layanan_name')[0].sumo.remove(i - 1);
      }
      $.ajax({
        url: "<?php echo base_url('index.php/Marketing_overview/layanan_name') ?>",
        method: 'POST',
        data: {
          'layanan_group': JSON.stringify($("#layanan_group").val())
        },
        beforeSend: function() {
          // loading();          
        },
        success: function(data) {
          $(".loading_layanan").hide();
          data = JSON.parse(data);
        if (data.length) {
          data.forEach( function (element){ 
            $('#layanan_name')[0].sumo.add(element.layanan_name)
          });
          
        }
        }
      }).done(function(data) {        
        $('#layanan_name')[0].sumo.selectAll();
      }).fail(function(jqXHR, textStatus, errorThrown) {
        // swal.close()
      });
    });
    $('#layanan_name')[0].sumo.selectAll();
    $("#data_selected").css("display", "none");
    branch = getParams(window.location.href).branch_name;
    $('#all').text(branch);
    if (branch) {
      $('.back').css("display", "block");
    }
    running(true)
    $('.btn_pdf').prop('disabled', true);
    $('.btn_csv').prop('disabled', true)
    $(".loading_layanan").hide();
  });


  var getParams = function(url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
  };

  function setDate(){
    let curdate = new Date().getFullYear();
    
    const month_now = moment().format('MM'); // curdate.setMonth(curdate.getMonth())
    let month_start = moment().subtract(2,'month').format('MM'); // curdate.setMonth(curdate.getMonth() - 3)

    if (month_now === '01' || month_now === '02'){
      month_start = '01';
    }
    
    const limit_year = parseInt(curdate) - 2019;
    var dropdown = "";
    for(var i = limit_year; i >= 0; i--){
      tmp = moment().subtract(i, 'year').year();
      dropdown += "<option value="+tmp+">"+tmp+"</option>";
    }
    $('#year').html(dropdown);

    // console.log("tanggal end date = ",month_now,", -3 =",month_start);

    $('.start_month option[value='+month_start+']').attr('selected','selected');
    $('.end_month option[value='+month_now+']').attr('selected','selected');
    $('.year_select option[value='+$('#year option:last-child').val()+']').attr('selected','selected');
  }

  function numberWithCommas(x) {
    if (x) {
      x = parseFloat(x)
      x = x % 1 != 0 ? x : parseFloat(x).toFixed(0);
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    return x
  }

  function sales_by_customer_segment(data) {
    sales_by_customer_segment_data = Highcharts.chart('sales_by_customer_segment', {
      chart: {
        type: 'column',
        inverted: true
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        min: 0,
        max: 4,
        tickLength: 0,
        scrollbar: {
          enabled: true
        },
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: ''
        },
        labels: {
                formatter: function(){
                    if (this.value>=1000000000000){
                     return this.value/1000000000000 + " Triliun"
                    } else if (this.value>=1000000000){
                     return this.value/1000000000 + " Miliar"
                    } else if (this.value>=1000000){
                     return this.value/1000000 + " Juta"
                     
                    }

                    
         
                },
            },
      },
      
      tooltip: {
        formatter: function() {
          return 'This Segment Sales: <b>Rp' + numberWithCommas(this.y) + '</b></br>' 
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        column: {
          turboThreshold: 3000
        },
        series: {
          color: '#f7a35c',
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.segment = this.name
                $("#data_selected").css("display", "initial");
                running(false);
              }
            }
          }
        }
      },
      series: [{
        name: '',
        data: data,
        dataLabels: {
          enabled: true,
          rotation: 0,
          color: '#000000',
          formatter: function() {
            if (this.y > 1000000000000) {
              return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Triliun"
            } else if (this.y > 1000000000) {
              return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
            } else if (this.y > 1000000) {
              return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
            } else {
              return this.y
            }
          },
          y: 0, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function sales_by_customer_name_final(data) {
    sales_by_customer_name_data = Highcharts.chart('sales_by_customer_name', {
      chart: {
        type: 'column',
        inverted: true
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        },
        max: 10,
        tickLength: 0,
        scrollbar: {
          enabled: true
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: ''
        },
        labels: {
                formatter: function(){
                    if (this.value>=1000000000000){
                     return this.value/1000000000000 + " Triliun"
                    } else if (this.value>=1000000000){
                     return this.value/1000000000 + " Miliar"
                    } else if (this.value>=1000000){
                     return this.value/1000000 + " Juta"
                     
                    }

                    
         
                },
            },
      },
      tooltip: {
        formatter: function() {
          return 'This Customer Name Sales: <b>Rp' + numberWithCommas(this.y) + '</b></br>' 
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        column: {
          turboThreshold: 3000
        },

        series: {
          color: '#f7a35c',
          cursor: 'pointer',
          point: {
            events: {
              click: function() {
                data_select.customer = this.name
                $("#data_selected").css("display", "initial");
                running(false);
              }
            }
          }
        }
      },
      series: [{
        name: '',
        data: data,
        dataLabels: {
          enabled: true,
          rotation: 0,
          color: '#000000',
          formatter: function() {
            if (this.y > 1000000000000) {
              return Highcharts.numberFormat(this.y / 1000000000000, 2) + " Triliun"
            } else if (this.y > 1000000000) {
              return Highcharts.numberFormat(this.y / 1000000000, 2) + " Miliar"
            } else if (this.y > 1000000) {
              return Highcharts.numberFormat(this.y / 1000000, 2) + " Juta";
            } else {
              return this.y
            }
          },
          y: 0, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }],
      exporting: {
        enabled: false
      }
    });
  }

  function sales_by_customer_name_part1(items) {
    $('#start_').text(1)
    $('#end_').text(50)
    data_customer_name = items
    $('#total_data').html(data_customer_name.length)
    sales_by_customer_name_final(data_customer_name.slice(0, 49))
  }

  function sales_by_customer_name_withbtn() {
    start = parseFloat($('#start_').text()) - 1;
    end = parseFloat($('#end_').text()) - 1;
    sales_by_customer_name_final(data_customer_name.slice(start, end));
  }

  function preview() {
    var star = parseFloat($('#start_').text());
    var end = parseFloat($('#end_').text());
    if (star > 50) {
      $('#start_').text(star - 50)
      $('#end_').text(end - 50)
    }
  }

  function next() {
    var star = parseFloat($('#start_').text());
    var end = parseFloat($('#end_').text());
    if (end < data_customer_name.length) {
      $('#start_').text(star + 50)
      $('#end_').text(end + 50)
    }
  }

  // Prepare demo data
  // Data is joined to map using value of 'hc-key' property by default.
  // See API docs for 'joinBy' for more info on linking data and map.  
  // Create the chart

  function result(x) {
    console.log(x)
    return x.y
  }

  function backpage() {
    window.history.back();
  }

  function convertToInternationalCurrencySystem(labelValue) {

    // Nine Zeroes for Billions
    return Math.abs(Number(labelValue)) >= 1.0e+9

      ?
      (Math.abs(Number(labelValue)) / 1.0e+9).toFixed(2) + "B"
      // Six Zeroes for Millions 
      :
      Math.abs(Number(labelValue)) >= 1.0e+6

      ?
      (Math.abs(Number(labelValue)) / 1.0e+6).toFixed(2) + "M"
      // Three Zeroes for Thousands
      :
      Math.abs(Number(labelValue)) >= 1.0e+3

      ?
      (Math.abs(Number(labelValue)) / 1.0e+3).toFixed(2) + "K"

      :
      Math.abs(Number(labelValue));

  }

  function starMonthF(value) {
    $("#cend").empty();
    starMonth = parseFloat(value) || parseFloat($("#start").val());
    endMonth = parseFloat($("#end").val())
    var x = ''
    month.slice(starMonth - 1, endMonth).forEach(function(element) {
      x += "<option value='" + element.month + "'>" + element.month_name + "</option>";
    });
    $("#cend").append(x);
    console.log(x);
  }

  function endMonthF(value) {
    $("#cend").empty();
    starMonth = parseFloat($("#start").val())
    endMonth = parseFloat(value) || parseFloat($("#end").val());
    var x = '';
    month.slice(starMonth - 1, endMonth).forEach(function(element) {
      x += "<option value='" + element.month + "'>" + element.month_name + "</option>";
    });
    $("#cend").append(x);
    console.log(x);
  }

  function clearF() {
    $("#data_selected").css("display", "none");
    data_select = {}
    running(false)
  }

  // function loading() {
  //   swal({
  //     title: 'Tunggu Sebentar...',
  //     text: ' ',
  //     icon: 'info',
  //     buttons: false,
  //     closeOnClickOutside: false,
  //   });
  // }

  function running(refresh) {
    if (refresh) {
      value = $("#value").val();
      start = $("#start").val();
      end = $("#end").val();
      year = $("#year").val();
      layanan_group = $("#layanan_group").val();
      layanan_name = $("#layanan_name").val();
      data_select.segment = undefined;
      data_select.material_brand = undefined;
      data_select.customer = undefined;
      data_select.material_name = undefined;
    }
   
    // loading()
    // console.log("AJAX mulailah")
    
    ajaxCustomerSegment(value,start,end,year,
    layanan_group,
    layanan_name,
    data_select.segment,
    data_select.material_brand,
    data_select.customer,
    data_select.material_name
    )
    ajaxCustomerName(value,start,end,year,
    layanan_group,
    layanan_name,
    data_select.segment,
    data_select.material_brand,
    data_select.customer,
    data_select.material_name
    )
    ajaxProductBrand(value,start,end,year,
    layanan_group,
    layanan_name,
    data_select.segment,
    data_select.material_brand,
    data_select.customer,
    data_select.material_name
    )
    ajaxProduct(value,start,end,year,
    layanan_group,
    layanan_name,
    data_select.segment,
    data_select.material_brand,
    data_select.customer,
    data_select.material_name
    )
    // swal.close()
  }
  
  function ajaxCustomerSegment(
    value,
    start,
    end,
    year,
    layanan_group,
    layanan_name,
    segment,
    material_brand,
    customer,
    material_name){
      
    console.log("AJAX segment gas")

    var data_name = [];
    var data_segment = [];

    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/getDetail_segment') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'segment': JSON.stringify(segment),
        'customer': JSON.stringify(customer),
        'material_brand': JSON.stringify(material_brand),
        'branch': JSON.stringify(branch),
        'layanan_group': JSON.stringify(layanan_group),
        'layanan_name': JSON.stringify(layanan_name),
        'material_name': JSON.stringify(material_name)
      },
      beforeSend: function() {
        // loading();
        $("#loader_sales_segment").show();
        document.getElementById("sales_segment_hide").style.display = "none";

      },
      success: function(data) {
        data = JSON.parse(data)
        console.log("data segment : ",data);
        data_download.detail_segment = data.detail_segment
        data.detail_segment.forEach(element => data_segment.push([element.segment_name || 'null', parseFloat(element.value)]));
      },
      
    }).done(function(data) {
      sales_by_customer_segment(data_segment)
      $("#loader_sales_segment").hide();
      document.getElementById("sales_segment_hide").style.display = "block";
      
      // swal.close()
      
      
    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log("GAGAL segment SADD")
      
      // swal.close()
    });
  }

  function ajaxCustomerName(
    value,
    start,
    end,
    year,
    layanan_group,
    layanan_name,
    segment,
    material_brand,
    customer,
    material_name){
      
    console.log("AJAX name gas")

    var data_name = [];
    var data_segment = [];

    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/getDetail_customer_name') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'segment': JSON.stringify(segment),
        'customer': JSON.stringify(customer),
        'material_brand': JSON.stringify(material_brand),
        'branch': JSON.stringify(branch),
        'layanan_group': JSON.stringify(layanan_group),
        'layanan_name': JSON.stringify(layanan_name),
        'material_name': JSON.stringify(material_name)
      },
      beforeSend: function() {
        // loading();
        $("#loader_sales_customer").show();
        document.getElementById("sales_cust_name_hide").style.display = "none";

      },
      success: function(data) {
        data = JSON.parse(data)
        console.log("data nama : ",data);
        data_download.detail_customer_name = data.detail_customer_name
        data.detail_customer_name.forEach(element => data_name.push([element.customer_name || 'null', parseFloat(element.value)]));
        
        
      },
    }).done(function(data) {    
        sales_by_customer_name_part1(data_name)
        $("#loader_sales_customer").hide();
      document.getElementById("sales_cust_name_hide").style.display = "block";
      
      // swal.close()

    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log("GAGAL name SADD")
      
      // swal.close()
    });
  }
  function ajaxProductBrand(
    value,
    start,
    end,
    year,
    layanan_group,
    layanan_name,
    segment,
    material_brand,
    customer,
    material_name){
      
    console.log("AJAX Brand gas")

    var data_name = [];
    var data_segment = [];

    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/getDetail_product_brand') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'segment': JSON.stringify(segment),
        'customer': JSON.stringify(customer),
        'material_brand': JSON.stringify(material_brand),
        'branch': JSON.stringify(branch),
        'layanan_group': JSON.stringify(layanan_group),
        'layanan_name': JSON.stringify(layanan_name),
        'material_name': JSON.stringify(material_name)
      },
      beforeSend: function() {
        // loading();
        $("#loader_sales_brand").show();
        document.getElementById("TableBrand").style.display = "none";

      },
      success: function(data) {
        data = JSON.parse(data)
        console.log("data brand : ",data);
        data_download.detail_product_brand = data.detail_product_brand
        newRow_ = data.detail_product_brand;
        length_ = data.detail_product_brand.length;
        
      },
    }).done(function(data) {
      
      $("#loader_sales_brand").hide();
      document.getElementById("TableBrand").style.display = "grid";
      if (length_ !== 0) {
        data_table_brand.clear().draw();
        data_table_brand.rows.add(newRow_);
        data_table_brand.columns.adjust().draw();
      } else {
        data_table_brand.clear().draw();
      }
      
      
    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log("GAGAL Brand SADD")
      
      // swal.close()
    });
  }
  function ajaxProduct(
    value,
    start,
    end,
    year,
    layanan_group,
    layanan_name,
    segment,
    material_brand,
    customer,
    material_name){
      
    console.log("AJAX Product gas")

    var data_name = [];
    var data_segment = [];

    $.ajax({
      url: "<?php echo base_url('index.php/Marketing_overview/getDetail_product') ?>",
      method: 'POST',
      data: {
        'value': JSON.stringify(value),
        'start': JSON.stringify(year + '' + start),
        'end': JSON.stringify(year + '' + end),
        'segment': JSON.stringify(segment),
        'customer': JSON.stringify(customer),
        'material_brand': JSON.stringify(material_brand),
        'branch': JSON.stringify(branch),
        'layanan_group': JSON.stringify(layanan_group),
        'layanan_name': JSON.stringify(layanan_name),
        'material_name': JSON.stringify(material_name)
      },
      beforeSend: function() {
        // loading();
        $("#loader_sales_product").show();
      document.getElementById("TableProduct").style.display = "none";



      },
      success: function(data) {
        data = JSON.parse(data)
        console.log("data produk : ",data);
        data_download.detail_product = data.detail_product
        newRow = data.detail_product
        length = data.detail_product.length;
      },
    }).done(function(data) {   
      $("#loader_sales_product").hide();
      document.getElementById("TableProduct").style.display = "grid";
      if (length !== 0) {
        data_table_product.clear().draw();
        data_table_product.rows.add(newRow);
        data_table_product.columns.adjust().draw();
      } else {
        data_table_product.clear().draw();
      }


    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log("GAGAL produk SADD")
      
      // swal.close()
    });
  }

  function loading() {
    swal({
      title: 'Tunggu Sebentar...',
      text: ' ',
      icon: 'info',
      buttons: false,
      closeOnClickOutside: false,
    });
  }

  function myBtn() {
    $("#myModal").show();
    // map_achivement.exportChart({
    // 		type: 'application/pdf'
    // 	})    
  }

  function closeModal() {
    $("#myModal").hide();
  }

  function disabledPDF(param) {
    $('.btn_pdf').prop('disabled', false);
    $('.btn_csv').prop('disabled', false);
    paramdownload = param.value
    if (param.value === "Sales by Product Brand" || param.value === "Sales by Product") {
      $('.btn_pdf').prop('disabled', true);
      $('.btn_csv').prop('disabled', false);
    } else if (!param.value) {
      $('.btn_pdf').prop('disabled', true);
      $('.btn_csv').prop('disabled', true);
    } else {
      $('.btn_pdf').prop('disabled', false);
      $('.btn_csv').prop('disabled', false);
    }
  }

  function exportToPDF() {
    if (paramdownload === "Sales by Customer Segment") {
      sales_by_customer_segment_data.exportChart({
        filename: 'Sales by Customer Segment_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    } else if (paramdownload === "Sales by Customer Name") {
      sales_by_customer_name_data.exportChart({
        filename: 'Sales by Customer Name_' + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val(),
        type: 'application/pdf'
      })
    }
  }

  function downloadCSV() {
    console.log("data download value : ",data_download)
    if (paramdownload === 'Sales by Customer Segment') {
      var headers = {
        segment_name: 'Customer Segment',
        value: "Value"
      };
      var itemsFormatted = [];
      // format the data
      data_download.detail_segment.forEach((item) => {
        itemsFormatted.push({
          segment_name: item.segment_name,
          value: item.value
        });
      });
      var fileTitle = 'Sales by Customer Segment' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    }
    if (paramdownload === 'Sales by Customer Name') {
      var headers = {
        segment_name: 'Customer Name',
        value: "Value"
      };
      var itemsFormatted = [];
      // format the data
      data_download.detail_customer_name.forEach((item) => {
        itemsFormatted.push({
          segment_name: item.customer_name,
          value: item.value
        });
      });
      var fileTitle = 'Sales by Customer Name' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    }
    if (paramdownload === 'Sales by Product Brand') {
      var headers = {
        segment_name: 'Material Brand',
        value: "Value"
      };
      var itemsFormatted = [];
      // format the data
      data_download.detail_product_brand.forEach((item) => {
        itemsFormatted.push({
          segment_name: item.material_brand,
          value: item.value
        });
      });
      var fileTitle = 'Sales by Product Brand' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    }
    if (paramdownload === 'Sales by Product') {
      var headers = {
        material_name: 'Material Name',
        qty: 'Quantity',
        value: "Value"
      };
      var itemsFormatted = [];
      // format the data
      data_download.detail_product.forEach((item) => {
        itemsFormatted.push({
          material_name: item.material_name,
          qty: item.qty,
          value: item.value
        });
      });
      var fileTitle = 'Sales by Product' + "_" + $('#value').val() + "_" + $('#start').val() + $('#end').val() + $('#year').val();
    }
    exportCSVFile(headers, itemsFormatted, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
  }

  function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line != '') line += ','

        line += array[i][index];
      }

      str += line + '\r\n';
    }

    return str;
  }

  function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
      items.unshift(headers);
    }

    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], {
      type: 'text/csv;charset=utf-8;'
    });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
</script>