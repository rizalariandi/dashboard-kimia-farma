<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>

<script src="<?= base_url() ?>assets/js/jqueryexport/xlsx.core.js"></script>
<script src="<?= base_url() ?>assets/js/jqueryexport/tableexport.js"></script>
<script src="<?= base_url() ?>assets/js/jqueryexport/FileSaver.js"></script>
<style>
    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    button.btn.btn-default {
        background-color: #08388F !important;
        color: #fff !important;
        text-decoration: none;
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        color: #fff !important;
        text-decoration: none;
        float: right;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }
</style>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Cash Collection</h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($_SESSION['role_entitas'] as $e) {

                                                            echo "<option value='$e'>$e</option>";
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Week : <select id="week" name="week"><?php for ($i = 1; $i <= 5; $i++) {
                                                    if ($i == $_SESSION['week']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>

        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>

<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-6">
        <h5 class="page-title text-center">Internal Month to Date</h5>
        <div class="chart-container">
            <canvas id="bar-chartcanvas-I-M" height="70vh"></canvas>
        </div>
    </div>
    <div class="col-md-6">
        <h5 class="page-title text-center">Internal Year to Date</h5>
        <div class="chart-container">
            <canvas id="bar-chartcanvas-I-Y" height="70vh"></canvas>
        </div>
    </div>
    <div class="col-md-6">
        <h5 class="page-title text-center">Eksternal Month to Date</h5>
        <div class="chart-container">
            <canvas id="bar-chartcanvas-E-M" height="70vh"></canvas>
        </div>
    </div>
    <div class="col-md-6">
        <h5 class="page-title text-center">Eksternal Year to Date</h5>
        <div class="chart-container">
            <canvas id="bar-chartcanvas-E-Y" height="70vh"></canvas>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box">
        <div class="table-responsive">
            <input type='submit' hidden="true" id="btnExport" value='Download' class='btn-table' />
            <table id="table_1" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">Month to Date</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col">Year to Date</th>
                        <th scope="col"></th>
                    </tr>
                    <tr>
                        <th scope="col">Current Year Collection</th>

                        <th scope="col">Target (RP)</th>
                        <th scope="col">Real (RP)</th>
                        <th scope="col">Ach.</th>
                        <th scope="col">Target (RP)</th>
                        <th scope="col">Real (RP)</th>
                        <th scope="col">Ach.</th>
                    </tr>
                </thead>
                <tbody>
                    <!--                <tr>-->
                    <!--                    <td>a. Cash Collection-->
                    <!--                    </th>-->
                    <!--                    <td>-->
                    <!--                    </th>-->
                    <!--                    <td id="cash_col_target_total">-->
                    <!--                    </th>-->
                    <!--                    <td id="cash_col_real_total">-->
                    <!--                    </th>-->
                    <!--                    <td id="cash_col_ach_total">-->
                    <!--                    </th>-->
                    <!--                    <td id="cash_col_target_ccy_total">-->
                    <!--                    </th>-->
                    <!--                    <td id="cash_col_real_ccy_total">-->
                    <!--                    </th>-->
                    <!---->
                    <!--                </tr>-->
                    <tr>
                        <td>i. Internal</td>

                        <td id="cash_col_target_internal">0
                            </th>
                        <td id="cash_col_real_internal">0
                            </th>
                        <td id="cash_col_ach_internal">0
                            </th>
                        <td id="cash_col_target_ccy_internal">0
                            </th>
                        <td id="cash_col_real_ccy_internal">0
                            </th>
                        <td id="cash_col_ach_ccy_internal">0
                            </th>
                    </tr>
                    <tr>
                        <td>i. Eksternal</td>

                        <td id="cash_col_target_eksternal">0
                            </th>
                        <td id="cash_col_real_eksternal">0
                            </th>
                        <td id="cash_col_ach_eksternal">0
                            </th>
                        <td id="cash_col_target_ccy_eksternal">0
                            </th>
                        <td id="cash_col_real_ccy_eksternal">0
                            </th>
                        <td id="cash_col_ach_ccy_eksternal">0
                            </th>
                    </tr>
                    <!--                                                      <tr>-->
                    <!--                                                        <td>b. Cash Collection Ratio</th>-->
                    <!--                                                -->
                    <!--                                                            <td id="cash_rat_target_total"></th>-->
                    <!--                                                              <td id="cash_rat_real_total"></th>-->
                    <!--                                                                <td id="cash_rat_ach_total"></th>-->
                    <!--                                                                  <td id="cash_rat_target_ccy_total"></th>-->
                    <!--                                                                    <td id="cash_rat_real_ccy_total"></th>-->
                    <!--                                                                      <td id="cash_rat_ach_ccy_total"></th>-->
                    <!--                                                                      </tr>-->
                    <!--                                                                      <tr>-->
                    <!--                                                                        <td>i. Internal</td>-->
                    <!--                                                               -->
                    <!--                                                                         <td id="cash_rat_target_internal">xxx</th>-->
                    <!--                                                                          <td id="cash_rat_real_internal">xxx</th>-->
                    <!--                                                                            <td id="cash_rat_ach_internal">xxx</th>-->
                    <!--                                                                              <td id="cash_rat_target_ccy_internal">xxx</th>-->
                    <!--                                                                                <td id="cash_rat_real_ccy_internal">xxx</th>-->
                    <!--                                                                                  <td id="cash_rat_ach_ccy_internal">xxx</th>-->
                    <!--                                                                                  </tr>-->
                    <!--                                                                                  <tr>-->
                    <!--                                                                                    <td>i. Eksternal</td>-->
                    <!--                                                                       -->
                    <!--                                                                                      <td id="cash_rat_target_eksternal">xxx</th>-->
                    <!--                                                                                        <td id="cash_rat_real_eksternal">xxx</th>-->
                    <!--                                                                                          <td id="cash_rat_ach_eksternal">xxx</th>-->
                    <!--                                                                                            <td id="cash_rat_target_ccy_eksternal">xxx</th>-->
                    <!--                                                                                              <td id="cash_rat_real_ccy_eksternal">xxx</th>-->
                    <!--                                                                                                <td id="cash_rat_ach_ccy_eksternal">xxx</th>-->
                    <!--                                                                                                </tr>-->
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    //get the bar chart canvas
    var ctxIM = $("#bar-chartcanvas-I-M");
    var ctxIY = $("#bar-chartcanvas-I-Y");
    var ctxEM = $("#bar-chartcanvas-E-M");
    var ctxEY = $("#bar-chartcanvas-E-Y");
    //bar chart data
    var dataIM;
    var dataIY;
    var dataEM;
    var dataEY;
    var chartIM;
    var chartIY;
    var chartEM;
    var chartEY;

    //options
    var options = {
        tooltips: {
            enabled: true,
            callbacks: {
                label: function(tooltipItem, data) {
                    console.log('tooltipItem', tooltipItem)
                    console.log('data', data)
                    var value = tooltipItem.yLabel
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);
                    value = value.join(',');
                    return data.datasets[tooltipItem.datasetIndex].label + ' : ' + value;
                }
            }
        },
        legend: {
            position: 'bottom'
        },
        scales: {
            yAxes: [{
                display: false,
                gridLines: {
                    display: false
                },
                ticks: {
                    display: true,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    };

    var tables = $('#table_1').tableExport({
        headings: true,
        footers: true,
        formats: ["xlsx"],
        filename: "CashCollection_" + entitas + "_" + year + '_' + month + "_" + week,
        bootstrap: true,
        position: "top",
        ignoreRows: null,
        ignoreCols: null,
        ignoreCSS: ".tableexport-ignore",
        emptyCSS: ".tableexport-empty",
        trimWhitespace: true
    });


    $(document).ready(function() {

        var urlpost = "<?php echo base_url('index.php/page/refresh_cash_collection') ?>";

        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();

        var entitas = $('#entitas').val();
        // console.debug(month+"-"+year+"-"+entitas);
        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'week': JSON.stringify(week),
                'entitas': JSON.stringify(entitas)


            },
            AjaxSucceeded, "json"
        );

        function AjaxSucceeded(result) {
            $('#cash_col_target_internal').text("0");
            $('#cash_col_real_internal').text("0");
            $('#cash_col_ach_internal').text("0");
            $('#cash_col_target_ccy_internal').text("0");
            $('#cash_col_real_ccy_internal').text("0");
            $('#cash_col_ach_ccy_internal').text("0");
            $('#cash_col_target_eksternal').text("0");
            $('#cash_col_real_eksternal').text("0");
            $('#cash_col_ach_eksternal').text("0");
            $('#cash_col_target_ccy_eksternal').text("0");
            $('#cash_col_real_ccy_eksternal').text("0");
            $('#cash_col_ach_ccy_eksternal').text("0");
            $('#cash_rat_target_internal').text("0");
            $('#cash_rat_real_internal').text("0");
            $('#cash_rat_ach_internal').text("0");
            $('#cash_rat_target_ccy_internal').text("0");
            $('#cash_rat_real_ccy_internal').text("0");
            $('#cash_rat_ach_ccy_internal').text("0");
            $('#cash_rat_target_eksternal').text("0");
            $('#cash_rat_real_eksternal').text("0");
            $('#cash_rat_ach_eksternal').text("0");
            $('#cash_rat_target_ccy_eksternal').text("0");
            $('#cash_rat_real_ccy_eksternal').text("0");
            $('#cash_rat_ach_ccy_eksternal').text("0");
            if (result.data.length > 0) {
                $.each(result.data, function(key, value) {
                    if (value.datacollection === "CASH COLLECTION") {
                        if (value.cakupan === "INTERNAL") {
                            $('#cash_col_target_internal').text(value.target ? value.target : 0);
                            $('#cash_col_real_internal').text(value.real ? value.real : 0);
                            $('#cash_col_ach_internal').text(value.achievement ? value.achievement : 0);
                            $('#cash_col_target_ccy_internal').text(value.acctarget ? value.acctarget : 0);
                            $('#cash_col_real_ccy_internal').text(value.accreal ? value.accreal : 0);
                            $('#cash_col_ach_ccy_internal').text(value.accachievement ? value.accachievement : 0);

                        } else {
                            $('#cash_col_target_eksternal').text(value.target ? value.target : 0);
                            $('#cash_col_real_eksternal').text(value.real ? value.real : 0);
                            $('#cash_col_ach_eksternal').text(value.achievement ? value.achievement : 0);
                            $('#cash_col_target_ccy_eksternal').text(value.acctarget ? value.acctarget : 0);
                            $('#cash_col_real_ccy_eksternal').text(value.accreal ? value.accreal : 0);
                            $('#cash_col_ach_ccy_eksternal').text(value.accachievement ? value.accachievement : 0);
                        }

                    } else {
                        if (value.cakupan === "INTERNAL") {
                            $('#cash_rat_target_internal').text(value.target ? value.target : 0);
                            $('#cash_rat_real_internal').text(value.real ? value.real : 0);
                            $('#cash_rat_ach_internal').text(value.achievement ? value.achievement : 0);
                            $('#cash_rat_target_ccy_internal').text(value.acctarget ? value.acctarget : 0);
                            $('#cash_rat_real_ccy_internal').text(value.accreal ? value.accreal : 0);
                            $('#cash_rat_ach_ccy_internal').text(value.accachievement ? value.accachievement : 0);
                        } else {
                            $('#cash_rat_target_eksternal').text(value.target ? value.target : 0);
                            $('#cash_rat_real_eksternal').text(value.real ? value.real : 0);
                            $('#cash_rat_ach_eksternal').text(value.achievement ? value.achievement : 0);
                            $('#cash_rat_target_ccy_eksternal').text(value.acctarget ? value.acctarget : 0);
                            $('#cash_rat_real_ccy_eksternal').text(value.accreal ? value.accreal : 0);
                            $('#cash_rat_ach_ccy_eksternal').text(value.accachievement ? value.accachievement : 0);
                        }
                    }


                })
            }

            tables.update({
                headings: true,
                footers: true,
                formats: ["xlsx"],
                filename: "CashCollection_" + entitas + "_" + year + '_' + month + "_" + week,
                bootstrap: true,
                position: "top",
                ignoreRows: null,
                ignoreCols: null,
                ignoreCSS: ".tableexport-ignore",
                emptyCSS: ".tableexport-empty",
                trimWhitespace: true
            })
            $(".btn-default").text("Download");
            chartBarInt(result.ChartInternal);
            chartBarEks(result.ChartEksternal);
        }


    });

    function chartBarInt(items) {
        // $("#bar-chartcanvas").empty();
        console.log('items', items)
        var target = []
        var real = []
        var acctarget = []
        var accreal = []
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
        var labelM = []
        var labelY = []
        items.forEach(function(element, key) {
            target.push(element.target);
            real.push(element.realisasi);
            acctarget.push(element.acctarget);
            accreal.push(element.accreal);
            labelM.push(month[parseFloat(element.month) - 1]);
            labelY.push(month[parseFloat(element.month) - 1]);
        });

        dataIM = {
            labels: labelM,
            datasets: [{
                    label: "Target",
                    data: target,
                    backgroundColor: "#F7830D",
                    borderColor: "rgba(10,20,30,1)",
                    borderWidth: 1
                },
                {
                    label: "Realisasi",
                    data: real,
                    backgroundColor: "#5774C9",
                    borderColor: "rgba(50,150,200,1)",
                    borderWidth: 1
                }
            ]
        };

        dataIY = {
            labels: labelY,
            datasets: [{
                    label: "Target",
                    data: acctarget,
                    backgroundColor: "#F7830D",
                    borderColor: "rgba(10,20,30,1)",
                    borderWidth: 1
                },
                {
                    label: "Realisasi",
                    data: accreal,
                    backgroundColor: "#5774C9",
                    borderColor: "rgba(50,150,200,1)",
                    borderWidth: 1
                }
            ]
        };

        //create Chart class object
        if (chartIM) {
            chartIM.destroy();
        }
        if (chartIY) {
            chartIY.destroy();
        }
        chartIM = new Chart(ctxIM, {
            type: "bar",
            data: dataIM,
            options: options
        });
        chartIY = new Chart(ctxIY, {
            type: "bar",
            data: dataIY,
            options: options
        });
    }

    function chartBarEks(items) {
        // $("#bar-chartcanvas").empty();
        console.log('items', items)
        var target = []
        var real = []
        var acctarget = []
        var accreal = []
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
        var labelM = []
        var labelY = []
        items.forEach(function(element, key) {
            target.push(element.target);
            real.push(element.realisasi);
            acctarget.push(element.acctarget);
            accreal.push(element.accreal);
            labelM.push(month[parseFloat(element.month) - 1]);
            labelY.push(month[parseFloat(element.month) - 1]);
        });

        dataEM = {
            labels: labelM,
            datasets: [{
                    label: "Target",
                    data: target,
                    backgroundColor: "#F7830D",
                    borderColor: "rgba(10,20,30,1)",
                    borderWidth: 1
                },
                {
                    label: "Realisasi",
                    data: real,
                    backgroundColor: "#5774C9",
                    borderColor: "rgba(50,150,200,1)",
                    borderWidth: 1
                }
            ]
        };

        dataEY = {
            labels: labelY,
            datasets: [{
                    label: "Target",
                    data: acctarget,
                    backgroundColor: "#F7830D",
                    borderColor: "rgba(10,20,30,1)",
                    borderWidth: 1
                },
                {
                    label: "Realisasi",
                    data: accreal,
                    backgroundColor: "#5774C9",
                    borderColor: "rgba(50,150,200,1)",
                    borderWidth: 1
                }
            ]
        };

        //create Chart class object
        if (chartEM) {
            chartEM.destroy();
        }
        if (chartEY) {
            chartEY.destroy();
        }
        chartEM = new Chart(ctxEM, {
            type: "bar",
            data: dataEM,
            options: options
        });
        chartEY = new Chart(ctxEY, {
            type: "bar",
            data: dataEY,
            options: options
        });
    }


    $("#btnExport").click(function(e) {
        e.preventDefault();
        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();

        var entitas = $('#entitas').val();
        if (month.length == 1) {
            month = "0" + month;
        }
        $(".btn-default").remove();
        $('#table_1').tableExport({
            headings: true,
            footers: true,
            formats: ["xlsx"],
            filename: "CashCollection_" + entitas + "_" + year + '_' + month + "_" + week,
            bootstrap: true,
            position: "top",
            ignoreRows: null,
            ignoreCols: null,
            ignoreCSS: ".tableexport-ignore",
            emptyCSS: ".tableexport-empty",
            trimWhitespace: true
        });
        $(".btn-default").text("Download");


    })
    $("#btnSubmit").click(function(e) {

        e.preventDefault();


        var urlpost = "<?php echo base_url('index.php/page/refresh_cash_collection') ?>";

        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();

        var entitas = $('#entitas').val();
        // console.debug(month+"-"+year+"-"+entitas);
        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'week': JSON.stringify(week),
                'entitas': JSON.stringify(entitas)


            },
            AjaxSucceeded, "json"
        );

        function AjaxSucceeded(result) {
            chartBarInt(result.ChartInternal);
            chartBarEks(result.ChartEksternal);
            if (result.data.length > 0) {
                $.each(result.data, function(key, value) {
                    if (value.datacollection === "CASH COLLECTION") {
                        if (value.cakupan === "INTERNAL") {
                            $('#cash_col_target_internal').text(value.target);
                            $('#cash_col_real_internal').text(value.real);
                            $('#cash_col_ach_internal').text(value.achievement);
                            $('#cash_col_target_ccy_internal').text(value.acctarget);
                            $('#cash_col_real_ccy_internal').text(value.accreal);
                            $('#cash_col_ach_ccy_internal').text(value.accachievement);

                        } else {
                            $('#cash_col_target_eksternal').text(value.target);
                            $('#cash_col_real_eksternal').text(value.real);
                            $('#cash_col_ach_eksternal').text(value.achievement);
                            $('#cash_col_target_ccy_eksternal').text(value.acctarget);
                            $('#cash_col_real_ccy_eksternal').text(value.accreal);
                            $('#cash_col_ach_ccy_eksternal').text(value.accachievement);
                        }

                    } else {
                        if (value.cakupan === "INTERNAL") {
                            $('#cash_rat_target_internal').text(value.target);
                            $('#cash_rat_real_internal').text(value.real);
                            $('#cash_rat_ach_internal').text(value.achievement);
                            $('#cash_rat_target_ccy_internal').text(value.acctarget);
                            $('#cash_rat_real_ccy_internal').text(value.accreal);
                            $('#cash_rat_ach_ccy_internal').text(value.accachievement);
                        } else {
                            $('#cash_rat_target_eksternal').text(value.target);
                            $('#cash_rat_real_eksternal').text(value.real);
                            $('#cash_rat_ach_eksternal').text(value.achievement);
                            $('#cash_rat_target_ccy_eksternal').text(value.acctarget);
                            $('#cash_rat_real_ccy_eksternal').text(value.accreal);
                            $('#cash_rat_ach_ccy_eksternal').text(value.accachievement);
                        }
                    }


                })
            } else {

                $('#cash_col_target_internal').text("0");
                $('#cash_col_real_internal').text("0");
                $('#cash_col_ach_internal').text("0");
                $('#cash_col_target_ccy_internal').text("0");
                $('#cash_col_real_ccy_internal').text("0");
                $('#cash_col_ach_ccy_internal').text("0");
                $('#cash_col_target_eksternal').text("0");
                $('#cash_col_real_eksternal').text("0");
                $('#cash_col_ach_eksternal').text("0");
                $('#cash_col_target_ccy_eksternal').text("0");
                $('#cash_col_real_ccy_eksternal').text("0");
                $('#cash_col_ach_ccy_eksternal').text("0");
                $('#cash_rat_target_internal').text("0");
                $('#cash_rat_real_internal').text("0");
                $('#cash_rat_ach_internal').text("0");
                $('#cash_rat_target_ccy_internal').text("0");
                $('#cash_rat_real_ccy_internal').text("0");
                $('#cash_rat_ach_ccy_internal').text("0");
                $('#cash_rat_target_eksternal').text("0");
                $('#cash_rat_real_eksternal').text("0");
                $('#cash_rat_ach_eksternal').text("0");
                $('#cash_rat_target_ccy_eksternal').text("0");
                $('#cash_rat_real_ccy_eksternal').text("0");
                $('#cash_rat_ach_ccy_eksternal').text("0");
            }
            tables.update({
                headings: true,
                footers: true,
                formats: ["xlsx"],
                filename: "CashCollection_" + entitas + "_" + year + '_' + month + "_" + week,
                bootstrap: true,
                position: "top",
                ignoreRows: null,
                ignoreCols: null,
                ignoreCSS: ".tableexport-ignore",
                emptyCSS: ".tableexport-empty",
                trimWhitespace: true
            });
            $(".btn-default").text("Download");
        }
    });
</script>