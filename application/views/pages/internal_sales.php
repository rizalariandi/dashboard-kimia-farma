<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>

<!--<div id="proporsi_penjualan_product_principal" style="height: 400px; min-width: 380px"></div>-->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>

<style>
th {
    background-color: #F79868;
    color: white;
    text-align: center;
	border: 1px solid ;
	font-weight:700
} 

td {
    text-align: center;
	font-weight:700
} 

.table-striped > tbody > tr:nth-child(2n+1) > td {
   background-color: #F2F2F2;background-color: #F3DCC6;
   
}, .table-striped > tbody > tr:nth-child(2n+1) > th {
   background-color: #F3DCC6;
   
}


</style>

<div class="row" style="background:white;padding: 10px;">
	<div class="col-lg-5">
		<h4 class="page-title" style="color: white; background-color: #F79868; font-weight:700; text-align: center">Sales per Lini, Province: All</h4>
		<div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	
		<script>
		Highcharts.chart('container3', {
			chart: {
				type: 'line'
			},
			title: {
				text: '',
				align: 'left',
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			},
			yAxis: {
				title: {
					text: ''
				}
			},
			credits: {
				enabled: false
			},
			plotOptions: {
				line: {
					dataLabels: {
						enabled: true
					},
					enableMouseTracking: false
				}
			},
			series: [{
				name: 'Lini 1',
				data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
			}, {
				name: 'Lini 2',
				data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
			}]
		});
		</script>
	
	</div>




	<div class="col-lg-3">
		<h4 class="page-title" style="color: white; background-color: #F79868; font-weight:700; text-align: center">Proposi Product Terjual</h4>
		<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	
	
<script>

// Build the chart
Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: '',
		style: {
         color: 'blue',
         font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
      },
		align: 'left',
		verticalAlign: 'bottom',
		layout: 'horizontal'
	},
	credits: {
    enabled: false
  },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
			size: 180,
            showInLegend: true
			
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'KF',
            y: 40,
			color: '#4980FF',
            sliced: true,
            selected: true
        }, {
            name: 'Non KF',
            y: 40,
			color: '#24CBB7'
        }, {
            name: 'Alkes',
            y: 20,
			color: '#F9CB20'
        }]
    }]
});
</script>



    <div class="col-md-2">
        <div class="table-responsive">
            <table class="table table table-striped" style="font-size:7px">
              <thead class="thead-dark">
                <tr>
                  <th colspan="4" scope='colgroup' style="font-size:16px; font-weight:700">Sales Per Lini</th>
                </tr>
				<tr>
                  <th scope="col">Nama Lini</th>
                  <th scope="col">Revenue</th>
				  <th scope="col">Growth</th>
				  <th scope="col">OTR</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>


    <div class="col-md-2">
        <div class="table-responsive">
            <table class="table table table-striped" style="font-size:7px">
              <thead class="thead-dark">
                <tr>
                  <th colspan="4" scope='colgroup' style="font-size:16px; font-weight:700">Margin Per Lini</th>
                </tr>
				<tr>
                  <th scope="col">Nama Lini</th>
                  <th scope="col">Revenue</th>
				  <th scope="col">Growth</th>
				  <th scope="col">OTR</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>



</div>



<div class="row" style="background:white;padding: 10px;">


    <div class="col-md-3">
        <div class="table-responsive">
            <table class="table table table-striped" style="font-size:7px">
              <thead class="thead-dark">
                <tr>
                  <th colspan="4" scope='colgroup' style="font-size:16px; font-weight:700">Sales Per Lini</th>
                </tr>
				<tr>
                  <th scope="col">Nama Lini</th>
                  <th scope="col">Revenue</th>
				  <th scope="col">Growth</th>
				  <th scope="col">OTR</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td>Lini 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>


    <div class="col-md-3">
        <div class="table-responsive">
            <table class="table table table-striped" style="font-size:7px">
              <thead class="thead-dark">
                <tr>
                  <th colspan="4" scope='colgroup' style="font-size:16px; font-weight:700">Margin Per Lini</th>
                </tr>
				<tr>
                  <th scope="col">Nama Lini</th>
                  <th scope="col">Revenue</th>
				  <th scope="col">Growth</th>
				  <th scope="col">OTR</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
                <tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
				<tr>
                  <td style="font-size:6px">Cabang 1</td>
                  <td>10 T</td>
                  <td>80%</td>
				  <td>80%</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-3">

		<table class="table borderless" >
		  <thead>
			<tr>
			  <th style="border-left:#F79868 1px solid; border-right:#F79868 1px solid" colspan="2" scope="col">Sales Cabang Terbaik</th>
			</tr>
		  </thead>
		  <tbody>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Nama Cabang</td>
			  <td style="border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="nama_cabang" value="Cabang 1" readonly size="10"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Region</td>
			  <td style="border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="region" value="Region 1" readonly size="10"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Growth</td>
			  <td style="color: green; border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="growth" value="80%" readonly size="10"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td colspan="2" scope="col" style="border-bottom:#F79868 1px solid; border-right:#F79868 1px solid; border-left:#F79868 1px solid"><img src="http://localhost/dashboard_kf/asset/images/happy.png" alt="Smiley face" height="160" width="120"></td>
			</tr>
		  </tbody>
		</table>


    </div>
	


    <div class="col-md-3">

		<table class="table borderless" >
		  <thead>
			<tr>
			  <th style="border-left:#F79868 1px solid; border-right:#F79868 1px solid" colspan="2" scope="col">Sales Cabang Terburuk</th>
			</tr>
		  </thead>
		  <tbody>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Nama Cabang</td>
			  <td style="border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="nama_cabang" value="Cabang 1" readonly size="10"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Region</td>
			  <td style="border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="region" value="Region 1" readonly size="10"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td style="text-align:left; border-left:#F79868 1px solid; border-bottom:white 1px solid">Growth</td>
			  <td style="color: red; border-right:#F79868 1px solid; border-bottom:white 1px solid"><input type="text" name="growth" value="30%" readonly size="10" color="red"></td>
			</tr>
			<tr style="border: 1px solid white">
			  <td colspan="2" scope="col" style="border-bottom:#F79868 1px solid; border-right:#F79868 1px solid; border-left:#F79868 1px solid"><img src="http://localhost/dashboard_kf/asset/images/sad.png" alt="Smiley face" height="160" width="120"></td>
			</tr>
		  </tbody>
		</table>


    </div>

</div>

<div class="row" style="background:white;padding: 10px;">
	<div class="col-lg-12">
		<marquee behavior="scroll" direction="left" style="font-weight:700">Running Text Sales  Update Per Cabang <font color="green">Running Text Sales  Update Per Cabang</font>  <font color="blue">Running Text Sales  Update Per Cabang</font> <font color="red">Running Text Sales  Update Per Cabang</font></marquee>
	
    </div>

</div>