<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>


<style type="text/css">
    .cursor-pointer {
        cursor: pointer
    }

    .last_update {
        position: absolute;


        font-family: Roboto;
        font-style: italic;
        font-weight: normal;
        font-size: 12px;

        color: #4F4F4F;
    }

    .progress-bar-danger {
        background-color: #cb3935;
    }

    .progress-bar-warning {
        background-color: #f0ad4e;
    }

    .progress-bar-success {
        background-color: #5cb85c;
    }
</style>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"></h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Period : <select id="period" name="period"><?php $ent = ['Monthly', 'YearToDate']; //'Daily',
                                                    foreach ($ent as $e) {
                                                        if ($e == $_SESSION['period']) {
                                                            echo "<option value='$e' selected>$e</option>";
                                                        } else {
                                                            echo "<option value='$e'>$e</option>";
                                                        }
                                                    } ?></select>
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($_SESSION['role_entitas'] as $e) {

                                                            echo "<option value='$e'>$e</option>";
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        <label id="label_week"> Week : </label><select id="select_week" name="week"><?php for ($i = 1; $i <= 4; $i++) {
                                                                                        if ($i == $_SESSION['week']) {
                                                                                            echo "<option value='$i' selected>$i</option>";
                                                                                        } else {
                                                                                            echo "<option value='$i'>$i</option>";
                                                                                        }
                                                                                    } ?></select>
        <label id="label_day">Day :</label> <select id="select_day" name="day"><?php for ($i = 1; $i <= 31; $i++) {
                                                                                    if ($i == $_SESSION['day']) {
                                                                                        echo "<option value='$i' selected>$i</option>";
                                                                                    } else {
                                                                                        echo "<option value='$i'>$i</option>";
                                                                                    }
                                                                                } ?>
        </select>
        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>

<?php


$i = 230;
$data = [
    'Financial Perspective' => ['Net Sales', 'COGS', 'Gross Profit', 'Operating Expenses', 'Net Operating Income', 'Other Income', 'Interest Expense', 'Income Before Taxes', 'Tax Expense', 'Net Income'],
    'Marketing Perspective' => ['Realisasi (Current Year)', 'Realisasi (Last Year)', 'Achievement', 'Growth', '', '', ''],
    'Internal Process Perspective' => ['Suplay Chain', 'Logistic', '', '', '', '', ''],
    'Learning and Growth Perspective' => ['Man Days Training(MDT)', 'Employee Productivity', 'Number of Employee', '', '', '', ''],
    'Leadership and Social Responsibility' => ['Health Level Score', 'Portal BUMN', 'GCG Score', '', '', '', ''],
    'Development Agent Perspective' => ['Synergy of BUMN', 'Downstreaming & Local', '', '', '', '', '']
];
$s = 1;
$k = 1;
$j = 1;
if ($_SESSION['level'] == 'kfc') {
    unset($data['Internal Process Perspective']);
}
if ($_SESSION['level'] == 'FIN_KFHO') {
    unset($data['Internal Process Perspective']);
    unset($data['Marketing Perspective']);
    unset($data['Learning and Growth Perspective']);
    unset($data['Leadership and Social Responsibility']);
    unset($data['Development Agent Perspective']);
} else if ($_SESSION['level'] == 'MARKETING_KFHO') {
    unset($data['Internal Process Perspective']);
    unset($data['Financial Perspective']);
    unset($data['Learning and Growth Perspective']);
    unset($data['Leadership and Social Responsibility']);
    unset($data['Development Agent Perspective']);
}
foreach ($data as $key => $d) {
    if ($s % 2 == 1) {
        echo "<div class='row' style='display: table;width: 100%;'>";
    }
    ?>
<div class="col-md-6" style='display: table-cell'>
    <div class="white-box">
        <?php
            if ($key === 'Marketing Perspective') {
                ?>
        <div class='text-right'><a href='<?= base_url() ?>index.php/marketing/view/overview_marketing'>Detail <i class='fas fa-angle-right'></i></a></div>
        <?php
            } else {


                ?>
        <div class='text-right cursor-pointer'><a id="toUrl">Detail <i class='fas fa-angle-right'></i></a></div>
        <?php
            }
            ?>
        <h3 class="box-title"><?= $key ?></h3>
        <?php
            if ($key === 'Financial Perspective') {
                ?>
        <div class="row">
            <div class='col-md-2 col-sm-offset-10'>
                <b>Target :</b>
            </div>
        </div>
        <?php } ?>
        <?php foreach ($d as $c) {
                $x = rand(70, 110);
                $prog = '';
                if ($x < 80) {
                    $prog = 'danger';
                } elseif ($x > 80 && $x < 95) {
                    $prog = 'warning';
                } else {
                    $prog = 'success';
                }

                if ($key === 'Financial Perspective') {
                    ?>
        <div class='row'>
            <div class='col-md-4'>
                <?= $c ?>
            </div>
            <div class='col-md-6'>
                <div id="progres<?= $k ?>" class="progress progress-lg">
                    <div class="progress-bar progress-bar-<?= $prog ?>" role="progressbar" style="width: <?= $x ?>%;" aria-valuenow="<?= $x ?>" aria-valuemin="0" aria-valuemax="100"><?= $x ?></div>
                </div>
            </div>
            <div id="prog-value<?= $k ?>" class='col-md-2'>
                <?php

                            $i = $i - 20;
                            echo "Rp " . $i . "M";
                            ?>

            </div>


        </div>
        <?php
                    if ($k === sizeof($d)) {
                        $k = 1;

                        ?>
        <div class="row last_update">
            Last Update <?php echo date("d/m/Y"); ?>
        </div>

        <?php
                    } else {
                        $k++;
                    }
                    ?>

        <?php

                } else {
                    ?>
        <div class='row'>
            <div class='col-md-4'>
                <?= $c ?>
            </div>
            <div class='col-md-6'>
                <?php
                            if (strlen($c) === 0) {
                                ?>

                <div class="progress progress-lg" style="background-color: #ffffff;">

                </div>
                <?php

                            } else {
                                ?>

                <div id="progres<?= $k ?>" class="progress progress-lg">
                    <div class="progress-bar progress-bar-<?= $prog ?>" role="progressbar" style="width: <?= $x ?>%; color: #000000;" aria-valuenow="<?= $x ?>" aria-valuemin="0" aria-valuemax="100"><?= $x ?></div>
                </div>
                <?php

                                $j++;
                            }

                            ?>


            </div>
            <?php
                        if (strlen($c) === 0) {
                            ?>
            <div class='col-md-2'>

            </div>


            <?php

                            if ($k === sizeof($d)) {
                                $k = 1;

                                ?>
            <br />
            <br />
            <div class="row last_update">
                Last Update <?php echo date("d/m/Y"); ?>
            </div>

            <?php

                            } else {
                                $k++;
                            }
                            ?>

            <?php


                        } else {

                            ?>
            <div id="prog-value<?= $k ?>" class='col-md-2'>
                100%
            </div>
            <?php

                            if ($k === sizeof($d)) {
                                $k = 1;

                                ?>
            <br />
            <br />
            <div class="row last_update">
                Last Update <?php echo date("d/m/Y"); ?>
            </div>

            <?php
                            } else {
                                $k++;
                            }
                            ?>

            <?php
                        }
                        ?>

        </div>

        <?php

                }
            } ?>
    </div>
</div>
<?php
    if ($s % 2 == 0) {
        echo "</div>";
    }
    $s++;
} ?>


<script>
    $(document).ready(function() {


        $('#select_week').attr("hidden", true);
        $('#label_week').attr("hidden", true);

        $('#select_day').attr("hidden", true);
        $('#label_day').attr("hidden", true);
        // ini di comment dulu
        // generateDashboard();

    })

    $('#period').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected === 'Monthly' || valueSelected === 'YearToDate') {
            $('#select_week').attr("hidden", true);
            $('#label_week').attr("hidden", true);

            $('#select_day').attr("hidden", true);
            $('#label_day').attr("hidden", true);

        } else if (valueSelected === 'Daily') {
            $('#select_week').attr("hidden", false);
            $('#label_week').attr("hidden", false);

            $('#select_day').attr("hidden", false);
            $('#label_day').attr("hidden", false);

        }
    });

    $("#btnSubmit").click(function(e) {

        e.preventDefault();

        generateDashboard();



    })


    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function generateDashboard() {
        var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";
        loading()
        var year = $('#year').val();
        var month = $('#month').val();
        var period = $('#period').val();
        var day = $('#day').val();
        var entitas = $('#entitas').val();
        //console.debug(month+"-"+year+"-"+entitas);
        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'period': JSON.stringify(period),
                'day': JSON.stringify(day)


            },
            AjaxSucceeded, "json"
        ).fail(function(response) {
            swal(response.status.toString(), response.statusText, "error");            
        });
        $("#toUrl").click(function(e) {

            window.open("<?= base_url() ?>index.php/page/view/income_s?period=" + $('#period').val() + "&entitas=" + $('#entitas').val() + "&year=" + $('#year').val() + "&month=" + $('#month').val(), "_top");
        })

        function AjaxSucceeded(result) {
            $(".last_update").text(result.max.length ? 'Last Update ' + formatDate(result.max[0].max_lastupdate) : '-')
            $.each(result.is_sum_income, function(key, value) {
                if (value.income_statement == 'NET SALES') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres1').empty();
                    $('#prog-value1').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value1').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres1').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value1').append(format_round(value.sum_target));
                        } else {
                            $('#progres1').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value1').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value1').append(0);
                    }


                } else if (value.income_statement == 'COGS') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres2').empty();
                    $('#prog-value2').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres2').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value2').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres2').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value2').append(format_round(value.sum_target));
                        } else {
                            $('#progres2').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value2').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres2').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value2').append(0);
                    }

                } else if (value.income_statement == 'GROSS PROFIT') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres3').empty();
                    $('#prog-value3').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value3').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres3').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value3').append(format_round(value.sum_target));
                        } else {
                            $('#progres3').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value3').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value3').append(0);
                    }
                } else if (value.income_statement == 'OPERATING EXPENSES') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres4').empty();
                    $('#prog-value4').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres4').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value4').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres4').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value4').append(format_round(value.sum_target));
                        } else {
                            $('#progres4').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value4').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres4').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value4').append(0);
                    }
                } else if (value.income_statement == 'NET OPERATING INCOME') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres5').empty();
                    $('#prog-value5').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value5').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres5').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value5').append(format_round(value.sum_target));
                        } else {
                            $('#progres5').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value5').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value5').append(0);
                    }
                } else if (value.income_statement == 'OTHER INCOME') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres6').empty();
                    $('#prog-value6').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value6').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres6').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value6').append(format_round(value.sum_target));
                        } else {
                            $('#progres6').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value6').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value6').append(0);
                    }
                } else if (value.income_statement == 'INTEREST EXPENSE') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres7').empty();
                    $('#prog-value7').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres7').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value7').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres7').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value7').append(format_round(value.sum_target));
                        } else {
                            $('#progres7').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value7').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres7').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value7').append(0);
                    }
                } else if (value.income_statement == 'INCOME BEFORE TAXES') {
                    var achievement;
                    var achiev;
                    console.debug("income before taxes");
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres8').empty();
                    $('#prog-value8').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    console.debug("income before taxes achiev " + achiev);
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres8').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value8').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres8').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value8').append(format_round(value.sum_target));
                        } else {
                            $('#progres8').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value8').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres8').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value8').append(0);
                    }
                } else if (value.income_statement == 'TAX EXPENSE') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres9').empty();
                    $('#prog-value9').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres9').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value9').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres9').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value9').append(format_round(value.sum_target));
                        } else {
                            $('#progres9').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value9').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres9').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value9').append(format_round(value.sum_target));
                    }
                } else if (value.income_statement == 'NET INCOME') {
                    var achievement;
                    var achiev;
                    if (period === 'YearToDate') {
                        achievement = (value.sum_realisasi_ytd / value.sum_target) * 100;
                    } else {
                        achievement = (value.sum_realisasi / value.sum_target) * 100;
                    }
                    if (isNaN(achievement)) {
                        achiev = 0;
                    } else {
                        achiev = achievement;
                    }

                    achiev = isFinite(achiev) ? achiev : 0;

                    $('#progres10').empty();
                    $('#prog-value10').empty();
                    var prog = '';
                    var title_achiev = achiev;

                    if (title_achiev >= 100) {
                        title_achiev = 100;
                    }
                    if (achiev !== 0) {
                        if (achiev < 95) {
                            $('#progres10').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value10').append(format_round(value.sum_target));
                        } else if (achiev >= 95 && achiev < 100) {
                            $('#progres10').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value10').append(format_round(value.sum_target));
                        } else {
                            $('#progres10').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + title_achiev + "%; color: #000000;' aria-valuenow='" + title_achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                            $('#prog-value10').append(format_round(value.sum_target));
                        }

                    } else {
                        achiev = 0;
                        $('#progres10').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achiev + "%; color: #000000;' aria-valuenow='" + achiev + "' aria-valuemin='0' aria-valuemax='100'>" + achiev.toFixed(0) + "%</div>");
                        $('#prog-value10').append(0);
                    }

                }


                if (result.marketing_statement) {

                    var current_achieve = (result.marketing_statement[0].realisasi_current_year / result.marketing_statement[0].target_current_year) * 100;
                    var last_achieve = (result.marketing_statement[0].realisasi_last_year / result.marketing_statement[0].target_current_year) * 100;
                    var achiev = parseFloat(result.marketing_statement[0].achievement);
                    var growth = parseFloat(result.marketing_statement[0].growth);

                    // if (isNull(result.marketing_statement[0].realisasi_current_year)){
                    //
                    // }

                    var achievement;
                    if (isNaN(achiev)) {
                        achievement = 0;
                    } else {
                        achievement = achiev;
                    }

                    $('#progres11').empty();
                    $('#prog-value11').empty();
                    if (current_achieve !== 0) {
                        if (current_achieve < 80) {
                            $('#progres11').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + current_achieve + "%; color: #000000;' aria-valuenow='" + current_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + current_achieve.toFixed(0) + "</div>");
                            $('#prog-value11').append(format_round(result.marketing_statement[0].target_current_year));
                        } else if (current_achieve >= 80 && current_achieve <= 95) {
                            $('#progres11').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + current_achieve + "%; color: #000000;' aria-valuenow='" + current_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + current_achieve.toFixed(0) + "</div>");
                            $('#prog-value11').append(format_round(result.marketing_statement[0].target_current_year));
                        } else {
                            $('#progres11').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + current_achieve + "%; color: #000000;' aria-valuenow='" + current_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + current_achieve.toFixed(0) + "</div>");
                            $('#prog-value11').append(format_round(result.marketing_statement[0].target_current_year));
                        }

                    } else {
                        current_achieve = 0;
                        $('#progres11').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + current_achieve + "%; color: #000000;' aria-valuenow='" + current_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + current_achieve.toFixed(0) + "</div>");
                        $('#prog-value11').append(0);
                    }
                    $('#progres12').empty();
                    $('#prog-value12').empty();
                    if (last_achieve !== 0) {
                        if (last_achieve < 80) {
                            $('#progres12').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + last_achieve + "%; color: #000000;' aria-valuenow='" + last_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + last_achieve.toFixed(0) + "</div>");
                            $('#prog-value12').append(format_round(result.marketing_statement[0].target_current_year));
                        } else if (last_achieve >= 80 && last_achieve <= 95) {
                            $('#progres12').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + last_achieve + "%; color: #000000;' aria-valuenow='" + last_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + last_achieve.toFixed(0) + "</div>");
                            $('#prog-value12').append(format_round(result.marketing_statement[0].target_current_year));
                        } else {
                            $('#progres12').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + last_achieve + "%; color: #000000;' aria-valuenow='" + last_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + last_achieve.toFixed(0) + "</div>");
                            $('#prog-value12').append(format_round(result.marketing_statement[0].target_current_year));
                        }

                    } else {
                        last_achieve = 0;
                        $('#progres12').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + last_achieve + "%; color: #000000;' aria-valuenow='" + last_achieve + "' aria-valuemin='0' aria-valuemax='100'>" + last_achieve.toFixed(0) + "</div>");
                        $('#prog-value12').append(0);
                    }

                    $('#progres13').empty();
                    $('#prog-value13').empty();
                    if (achievement !== 0) {
                        if (achievement < 80) {
                            $('#progres13').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achievement + "%; color: #000000;' aria-valuenow='" + achievement + "' aria-valuemin='0' aria-valuemax='100'>" + achievement.toFixed(0) + "</div>");
                            $('#prog-value13').append("100%");
                        } else if (achievement >= 80 && achievement <= 95) {
                            $('#progres13').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + achievement + "%; color: #000000;' aria-valuenow='" + achievement + "' aria-valuemin='0' aria-valuemax='100'>" + achievement.toFixed(0) + "</div>");
                            $('#prog-value13').append("100%");
                        } else {
                            $('#progres13').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + achievement + "%; color: #000000;' aria-valuenow='" + achievement + "' aria-valuemin='0' aria-valuemax='100'>" + achievement.toFixed(0) + "</div>");
                            $('#prog-value13').append("100%");
                        }

                    } else {
                        achievement = 0;
                        $('#progres13').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + achievement + "%; color: #000000;' aria-valuenow='" + achievement + "' aria-valuemin='0' aria-valuemax='100'>" + achievement.toFixed(0) + "</div>");
                        $('#prog-value13').append("100%");
                    }

                    $('#progres14').empty();
                    $('#prog-value14').empty();
                    if (growth !== 0) {
                        if (growth < 80) {
                            $('#progres14').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + growth + "%; color: #000000;' aria-valuenow='" + growth + "' aria-valuemin='0' aria-valuemax='100'>" + growth.toFixed(0) + "</div>");
                            $('#prog-value14').append("100%");
                        } else if (last_achieve >= 80 && last_achieve <= 95) {
                            $('#progres14').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: " + growth + "%; color: #000000;' aria-valuenow='" + growth + "' aria-valuemin='0' aria-valuemax='100'>" + growth.toFixed(0) + "</div>");
                            $('#prog-value14').append("100%");
                        } else {
                            $('#progres14').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: " + growth + "%; color: #000000;' aria-valuenow='" + growth + "' aria-valuemin='0' aria-valuemax='100'>" + growth.toFixed(0) + "</div>");
                            $('#prog-value14').append("100%");
                        }

                    } else {
                        growth = 0;
                        $('#progres14').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: " + growth + "%; color: #000000;' aria-valuenow='" + growth + "' aria-valuemin='0' aria-valuemax='100'>" + growth.toFixed(0) + "</div>");
                        $('#prog-value14').append("100%");
                    }
                }


            })
            swal.close()
        }
    }

    function numberWithCommas(x) {
        //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }

    }

    function formatDate(date) {
        if(date)
        {
        date = date.slice(8, 10) + '/' + date.slice(5, 7) + '/' + date.slice(0, 4);   
        }        
        return date
    }

    function format_round(rp) {

        if (rp < 1 && rp > 0) {
            return (Math.round(rp * 100)) + '%';

        } else if (Math.round(rp).toString().length >= 10) {
            //  console.debug((rp/Math.pow(10,9))+'M');
            return "Rp " + numberWithCommas(Math.round(rp / Math.pow(10, 9))) + 'M';
        } else {
            return (numberWithCommas(rp));
        }
    }
</script>