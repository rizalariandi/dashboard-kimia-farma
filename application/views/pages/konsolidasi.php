<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Konsolidasi s.d 31 Desember 2018</h4>
        </div>
    </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                            <th rowspan="3" scope="col">Financial</th>
                            <th rowspan="3" scope="col">Unit</th>
                            
                            <th colspan="6" scope="col">Desember 2018</th>
                            
                            <th colspan="7" scope="col">YtD (3/12/2018)</th>
                            
                        </tr>
                        <tr>
                            <th rowspan="2" scope="col">Target/bulan</th>
                            <th colspan="2" scope="col">MtD</th>
                            <th colspan="2" scope="col">Prognosa</th>
                            <th rowspan="2" scope="col">MoM</th>
                            <th rowspan="2" scope="col">Target/bulan</th>
                            <th colspan="2" scope="col">MtD</th>
                            <th colspan="2" scope="col">Prognosa</th>
                            <th rowspan="2" scope="col">s.d Desember 2017</th>
                            <th rowspan="2" scope="col">YoY</th>
                        </tr>
                        <tr>
                            <th scope="col">Real</th>
                            <th scope="col">Ach</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Ach</th>
                            <th scope="col">Real</th>
                            <th scope="col">Ach</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Ach</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Revenue</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                        <tr>
                          <td>BPP</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                        <tr>
                          <td>Laba Kotor</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                        <tr>
                          <td>Biaya Usaha</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                        <tr>
                          <td>Laba Usaha</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                        <tr>
                          <td>Net Income</th>
                          <td>Rp M</th>
                          <td>Rp 10 T</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>80 %</th>
                          <td>Rp 1 T</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                          <td>95 %</th>
                        </tr>
                      </tbody>
                    </table>
		</div>
	</div>
</div>