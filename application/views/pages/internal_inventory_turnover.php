<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Inventory Turn Over</h4>
        </div>
    </div>
    <div class="col-md-12">
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
    

</div>


<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: null
    },
    subtitle: {
        text: null
    },
    xAxis: {
        categories: [
            'Principal 1',
            'Principal 2',
            'Principal 3',
            'Principal 4',
            'Principal 5',
            'Principal 6',
            'Principal 7',
            'Principal 8',
            'Principal 9',
            'Principal 10',
            'Principal 11',
            'Principal 12'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Days'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} Days</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }]
});
</script>
