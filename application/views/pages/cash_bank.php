<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>


<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    table tbody tr.odd {
        background: #F7FAFC;
    }

    table thead {
        background: #093890;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    .d-none {
        display: none;
    }

    .dataTables_scrollHeadInner .dataTable th {
        text-align: center;
    }
</style>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"></h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
            foreach ($_SESSION['role_entitas'] as $e) {

                echo "<option value='$e'>$e</option>";

            } ?></select>

        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>

        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>

<div class="row">

    <div class="col-md-12">
        <div class="white-box displayshow d-none">
            
            <h3 class="box-title">Cashbank</h3>
            Profit Center : <select id="profit-center" name="multiselect[]" multiple="multiple">

                <?php               
                foreach ($profit_center as $key => $value) {
                    echo '<option value="' . $value['profit_center'] . '">' . $value['profit_center'] . '</option>';
                }
                ?>
            </select>

            <table class="table" id='tbl_cashbank' class='stripe'>
                <thead>
                    <tr>
                        <th>Profit Center</th>
                        <th>Gl Account</th>
                        <!-- <th>Beginning Balance </th>                 -->
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($cb as $c) {
                        echo "<tr>
                    <td>$c[profit_center]</td>
                    <td>$c[gl_account]</td>       
                <!--<td>Rp " . number_format($c['beginning_balance']) . "</td>-->         
                    <td>Rp " . number_format($c['ending_balance']) . "</td>
                    </tr>";
                    }
                    ?>
                </tbody>
                <tfoot align="right">
                    <tr>
                        <th style="color: #000000;"></th>
                        <th style="color: #000000;"></th>
                        <!-- <th style="color: #000000;"></th style="color: #000000;"> -->
                        <th style="color: #000000;"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


<script>
    var data_table;
    var total_selected = []
    var params = {
        profit: []
    };
    $(document).ready(function() {
        $('#profit-center').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true,
            onChange: getValueCheckbox,
            onSelectAll: getValueCheckbox,
            onSelectedAll: getValueCheckbox,
            onDeselectAll: getValueCheckbox
        });
        $("#profit-center").multiselect('selectAll', false);

        $('#profit-center').multiselect('updateButtonText');

        $('.displayshow').show();

        data_table = $('#tbl_cashbank').DataTable({
            "scrollY": true,
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excel',
                title: function() {
                    return 'Cash Bank_' + itemsSelected() + '_' + $('#entitas').val() + '_' + $('#year').val() + '_' + $('#month').val();
                },
                text: "<i class='fas fa-download fa-lg'></i> Download",
                className: 'btn-table',
                exportOptions: {
                    modifier: {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }],
            "columns": [{
                    data: 'profit_center',
                    title: 'Profit Center',
                    className: 'text-left',
                },
                {
                    data: 'gl_account',
                    title: 'GI Account',
                    className: 'text-left'
                },
                {
                    data: $('#entitas').val() == 'KFHO' ? 'ending_balance' : 'beginning_balance',
                    title: 'Balance (Rp)',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)

                }
            ],
            "footerCallback": function(row, data, start, end, display) {
                var sumCol4Filtered = 0
                var api = this.api(),
                    data;

                // converting to interger to find total
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over this page
                pageTotal = api
                    .column(2, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over all pages
                var tueTotal = api
                    // .column( 3 )
                    .column(2)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total filtered rows on the selected column (code part added)
                sumCol4Filtered = display.map(el => data[el].ending_balance).reduce((a, b) => intVal(a) + intVal(b), 0);

                // Update footer by showing the total with the reference of the column index 
                $(api.column(0).footer()).html('Total');
                $(api.column(1).footer()).html('');
                $(api.column(2).footer()).html('Rp ' + numberWithCommas(sumCol4Filtered));

            }

        });
        getValueCheckbox();

        $('#profit-center :selected').each(function() {
            total_selected.push($(this).val());
        });
    });


    function itemsSelected() {
        if (total_selected.length == params.profit.length) {
            return 'All';
        } else {
            return params.profit.toString().replace(/,/g, '_');
        }
    }

    function getValueCheckbox() {
        var year = $('#year').val();
        var month = $('#month').val();
        var i = 0;
        var entitas = $('#entitas').val();
        params.profit = []

        $('#profit-center :selected').each(function() {
            params.profit.push($(this).val());
        });

        $.ajax({
            url: "<?php echo base_url('index.php/page/refresh_cashbank') ?>",
            method: 'POST',
            data: {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'profit_center': JSON.stringify(params.profit)
            },
            beforeSend: function() {
                // loading();
            },
            success: function(data) {
                newRow = JSON.parse(data)

            }
        }).done(function() {
            if (newRow.length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    $("#btnSubmit").click(function(e) {
        e.preventDefault();
        var year = $('#year').val();
        var month = $('#month').val();
        var i = 0;
        var entitas = $('#entitas').val();
        params.profit = []
        var urlpost = "<?php echo base_url('index.php/page/get_profitcenter') ?>";

        $.post(
            urlpost, {

                'entitas': JSON.stringify(entitas)



            },
            AjaxSucceeded, "json"

        );

        function AjaxSucceeded(result) {
            $('#profit-center')
                .find('option')
                .remove()
                .end();


            $.each(result.data, function(key, value) {
                $('#profit-center')
                    .append($("<option></option>")
                        .attr("value", value.profit_center)
                        .text(value.profit_center));

                $('#profit-center').multiselect('rebuild');
                $("#profit-center").multiselect('selectAll', false);

                $('#profit-center').multiselect('updateButtonText');
                //$('#kota').append('<option value="'+value+'">'+value+'</option>');
            })
            $('#profit-center :selected').each(function() {
                params.profit.push($(this).val());
            });
            total_selected = params.profit;
            getValueCheckbox();
        }
    });

    function numberWithCommas(x) {

        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }
    }
</script>