<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>



<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <h6 class="page-title pull-right">Last update: 08-Mar-2018</h6>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <h4 class="page-title pull-left">Entitas: KFTD</h4>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="background:#FC8313;height: 18px;text-align: center;color: white;">
                Net Sales
            </div>
            <div class="col-md-12" style="background:#F2F2F2;height: 74px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2017</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2018</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#EF4D48;height: 18px;text-align: center;color: white;">
                -6.08%
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="background:#FC8313;height: 18px;text-align: center;color: white;">
                COGS
            </div>
            <div class="col-md-12" style="background:#F2F2F2;height: 74px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2017</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2018</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#EF4D48;height: 18px;text-align: center;color: white;">
                -6.08%
            </div>
        </div>
        
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <h4 class="page-title pull-left">Sales Performance</h4>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="background:#FC8313;height: 18px;text-align: center;color: white;">
                Net Sales vs COGS (%)
            </div>
            <div class="col-md-12" style="background:#F2F2F2;height: 74px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2017</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2018</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#27AE61;height: 18px;text-align: center;color: white;">
                -6.08%
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="background:#FC8313;height: 18px;text-align: center;color: white;">
                CR Memo
            </div>
            <div class="col-md-12" style="background:#F2F2F2;height: 74px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2017</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
                <div class="col-md-6" style="text-align: center;">
                    <div class="col-md-12">
                        <h6 style="text-align: center;vertical-align: middle;">Nov 2018</h6>
                    </div>
                    <div class="col-md-12">
                        <h4 style="text-align: center;vertical-align: middle;">308.8B</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#27AE61;height: 18px;text-align: center;color: white;">
                -6.08%
            </div>
        </div>
        
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-5">
        <div class="col-md-12">
            <div class="col-md-3">
                <select class='form-control'>
                    <option value='All'>year</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                </select>
            </div>
            <div class="col-md-3">
                <select class='form-control'>
                    <option value='All'>month</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                </select>
            </div>
            <div class="col-md-2">
                <form>
                    <div class="radio">
                      <label><input type="radio" name="optradio" checked>Option 1</label>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <form>
                    <div class="radio">
                      <label><input type="radio" name="optradio" checked>Option 2</label>
                    </div>
                </form>
            </div>
            <div class="col-md-2">
                <form>
                    <div class="radio">
                      <label><input type="radio" name="optradio" checked>Option 3</label>
                    </div>
                </form>
            </div>
            
        </div>
        <div class="col-md-7">
            <div class="table-responsive">
                <table class="table table table-striped">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">Plant</th>
                      <th scope="col">Value Barang</th>
                      <th scope="col">Value Barang</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                    <tr>
                      <td>Plant Bandung</td>
                      <td>-3,234,234,123 IDR</td>
                      <td>-3,234,234,123 IDR</td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-12">
                Pie proporsi produk terjual
                <div id="proporsi_produk_terjual" style="min-width: 100px; height: 400px; max-width: 200px; margin: 0 auto"></div>
            </div>
            <div class="col-md-12">
                Pie Bhinneka usaha raya
                <div id="bhinneka_usaha_raya" style="min-width: 100px; height: 400px; max-width: 200px; margin: 0 auto"></div>
            </div>
        </div>
        
    </div>
    <div class="col-md-7">
        <div class="col-md-12">
            <div id="net_period" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
        <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#home">Material</a></li>
              <li><a data-toggle="tab" href="#menu1">Principal</a></li>
              <li><a data-toggle="tab" href="#menu2">OTR</a></li>
              <li><a data-toggle="tab" href="#menu3">Segmen Pasar</a></li>
              <li><a data-toggle="tab" href="#menu4">Otlet Terdaftar</a></li>
            </ul>

            <div class="tab-content">
              <div id="home" class="tab-pane fade in active">
                <div class="col-md-12">
                    <select class='form-control'>
                        <option value='All'>All</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table table-striped">
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Material</th>
                              <th scope="col">Net Sales</th>
                              <th scope="col">Billing Qty: Base MoM</th>
                              <th scope="col">Billing Quantity SU</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div id="menu1" class="tab-pane fade">
               <div class="col-md-12">
                    <select class='form-control'>
                        <option value='All'>All</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table table-striped">
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Material</th>
                              <th scope="col">Net Sales</th>
                              <th scope="col">Billing Qty: Base MoM</th>
                              <th scope="col">Billing Quantity SU</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div id="menu2" class="tab-pane fade">
                <div class="col-md-12">
                    <select class='form-control'>
                        <option value='All'>All</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table table-striped">
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Material</th>
                              <th scope="col">Net Sales</th>
                              <th scope="col">Billing Qty: Base MoM</th>
                              <th scope="col">Billing Quantity SU</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div id="menu3" class="tab-pane fade">
                <div class="col-md-12">
                    <select class='form-control'>
                        <option value='All'>All</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table table-striped">
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Material</th>
                              <th scope="col">Net Sales</th>
                              <th scope="col">Billing Qty: Base MoM</th>
                              <th scope="col">Billing Quantity SU</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <div id="menu4" class="tab-pane fade">
                <div class="col-md-12">
                    <select class='form-control'>
                        <option value='All'>All</option><option value='KF Console'>KF Console</option><option value='KFHO'>KFHO</option><option value='KFTD'>KFTD</option><option value='KFA'>KFA</option><option value='SIL'>SIL</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table table-striped">
                          <thead class="thead-dark">
                            <tr>
                              <th scope="col">Material</th>
                              <th scope="col">Net Sales</th>
                              <th scope="col">Billing Qty: Base MoM</th>
                              <th scope="col">Billing Quantity SU</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                            <tr>
                              <td>Dun & Bradstreet Number</td>
                              <td>1</td>
                              <td>1</td>
                              <td>1</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

<script>

// Build the chart
Highcharts.chart('proporsi_produk_terjual', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Plants',
        colorByPoint: true,
        data: [{
            name: 'Plant Bandung',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Plant Jakarta',
            y: 11.84
        }, {
            name: 'Plant Semarang',
            y: 10.85
        }]
    }]
});
// Build the chart
Highcharts.chart('bhinneka_usaha_raya', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Plants',
        colorByPoint: true,
        data: [{
            name: 'Plant Bandung',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Plant Jakarta',
            y: 11.84
        }, {
            name: 'Plant Semarang',
            y: 10.85
        }]
    }]
});


Highcharts.chart('net_period', {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: [1, 2, 3, 4, 5,6,7,8,9,10,11,12]
    },
    labels: {
        items: [{
            html: null,
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Net Sales',
        data: [3, 2, 1, 3, 4,2, 3, 5, 7, 6, 7, 6]
    }, {
        type: 'column',
        name: 'Real Sales',
        data: [2, 3, 5, 7, 6,3, 2, 1, 3, 4,2, 3]
    }, {
        type: 'spline',
        name: 'Average',
        data: [3, 2.67, 3, 6.33, 3.33,3, 2, 1, 3, 4,2, 3],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'spline',
        name: 'Credit Memo (KFTD)',
        data: [3, 2.67, 3, 6.33, 3.33,3, 2, 1, 3, 4,2, 3],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
});


</script>