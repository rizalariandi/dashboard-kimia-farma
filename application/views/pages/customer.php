<div class="row">
    <div class="col-md-12">
    <div class="white-box">
        <div id="chart1"></div>
    </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <div class="white-box">
        <div id="chart2"></div>
    </div>
    </div>
</div>

<script src="<?=base_url()?>asset/js/highstock.js"></script>
<script>
Highcharts.chart('chart1', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Index Customer Satisfaction'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ]
    },
    yAxis: {
        title: {
            text: 'Fruit units'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Customer',
        data: [1, 3, 4, 3, 3, 5, 4]
    }]
});
Highcharts.chart('chart2', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Customer Growth'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ]
    },
    yAxis: {
        title: {
            text: 'Fruit units'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Customer',
        data: [1, 3, 4, 3, 3, 5, 4]
    }]
});
</script>