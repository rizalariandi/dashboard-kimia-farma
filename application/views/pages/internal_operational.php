<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>
<style>

th {
    background-color: #F79868;
    color: white;
    text-align: center;
	border: 1px solid ;
	vertical-align: middle;
} 

td {
    color: #4A5675;
    text-align: center;
	vertical-align: middle;
} 

.table-striped > tbody > tr:nth-child(2n+1) > td, .table-striped > tbody > tr:nth-child(2n+1) > th {
   background-color: #F8EEE4;
   
}



</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Operational Excellence Performance</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark" >
                <tr >
                  <th rowspan="3" scope="col" style="vertical-align : middle;text-align:center;">Operational</th>
                  <th rowspan="3" scope="col" style="vertical-align : middle;text-align:center;">Unit</th>
				  <th colspan="6" scope='colgroup'>YtD(W/M/Y)</th>				  
                </tr>
				<tr>
				  <th rowspan="2" scope='col' scope="col" style="vertical-align : middle;text-align:center;">Target</th>
				  <th rowspan="2" scope='col' scope="col" style="vertical-align : middle;text-align:center;">Real W54</th>
				  <th rowspan="2" scope='col' scope="col" style="vertical-align : middle;text-align:center;">Real W55</th>
				  <th rowspan="2" scope='col' scope="col" style="vertical-align : middle;text-align:center;">Ach.</th>
				  <th colspan="2" scope='colgroup'>Prognosa</th>
				</tr>
				<tr>
				  <th scope='col'>Amount</th>
				  <th scope='col'>Ach.</th>
				</tr>
              </thead>
              <tbody>
                <tr>
                  <td style="text-align:left;">Reformat Apotek Kimia Farma</th>
                  <td>Apotek</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
                <tr>
                  <td style="text-align:left;">Reformat Apotek Kimia Farma (OSHCS)</th>
                  <td>Apotek</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
                <tr>
                  <td style="text-align:left;">Reformat Klinik Kimia Farma</th>
                  <td>Klinik</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Peserta Kapitasi di Klinik Kimia Farma</th>
                  <td>Peserta</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Apotek Kimia Farma</th>
                  <td>Apotek</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Klinik Kimia Farma</th>
                  <td>Klinik</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Perubahan DPP ke Klinik Pratama Kimia Farma</th>
                  <td>Klinik</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Klinik Hemodialisa Kimia Farma</th>
                  <td>Klinik</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Laboratoriun Klinik Kimia Farma</th>
                  <td></td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:right;">Flagship (Hubungan Utama)</th>
                  <td>Lab</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:right;">Hub</th>
                  <td>Lab</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:right;">Spoke</th>
                  <td>Lab</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Optik Kimia Farma</th>
                  <td>Optik</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Pembukaan Klinik Kecantikan Kimia Farma</th>
                  <td>Beauty</td>
                  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
				  <td>x</td>
                  <td>x</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
