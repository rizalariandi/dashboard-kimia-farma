<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
table tbody tr.odd{
	background: #F7FAFC;
}
table thead{
  background:#093890;
}

.dataTables_wrapper .dt-buttons {
  float:right;
}

.btn-table {
  background-color: #08388F !important;
  text-decoration: none;
}

.btn-table:hover {
  color: #fff !important;
  text-decoration: none;
}
</style>

<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>

    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>

    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
  </form>
</div>
</div>

<div class="row">

  <div class="col-md-12">
    <div class="white-box">

      <h3 class="box-title">Cashbank</h3>
      Profit Center : <select id="profit-center" name="multiselect[]" multiple="multiple">

        <?php 
        foreach ($profit_center as $key => $value)
        {
          echo '<option value="'.$value['profit_center'].'">'.$value['profit_center'].'</option>';
        }
        ?>
      </select>  

      Status : <select id="status" name="multiselect[]" multiple="multiple">

        <?php 
        foreach ($status as $key => $value)
        {
          echo '<option value="'.$value['status'].'">'.$value['status'].'</option>';
        }
        ?>
      </select>   

      <table class="table" id='tbl_cashbank' class='stripe'>
        <thead>
          <tr>
            <th>Profit Center</th>
            <th>Gl Account</th>
            <th>Status</th>
            <th>Beginning Balance </th>
            <th>Ending Balance</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          foreach($cb as $c){
            echo "<tr>
            <td>$c[profit_center]</td>
            <td>$c[gl_account]</td>
            <td></td>
            <td>Rp ".number_format($c['beginning_balance'])."</td>
            <td>Rp ".number_format($c['ending_balance'])."</td>
            </tr>";
          }
          ?>
        </tbody>
        <tfoot align="right">
          <tr><th style="color: #000000;"></th><th style="color: #000000;"></th><th style="color: #000000;"></th style="color: #000000;"><th style="color: #000000;"></th><th style="color: #000000;"></th></tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>


<script>

  $(document).ready(function() {
    $('#profit-center').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
    $("#profit-center").multiselect('selectAll', false);

    $('#profit-center').multiselect('updateButtonText');
    $('#status').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
    $("#status").multiselect('selectAll', false);

    $('#status').multiselect('updateButtonText');

    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();
    var params = {
      profit: [],
      status: []
    };
    $('#profit-center :selected').each(function() {
      params.profit.push($(this).val());
    });

    $('#status :selected').each(function() {
      params.status.push($(this).val());
    });

    var data_table1 = $('#tbl_cashbank').DataTable({
      "scrollY": "1000px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
      "dom"         : 'Blfrtip',
      buttons: [
      {
        extend: 'excelHtml5',
        text: "<i class='fas fa-download fa-lg'></i> Download",
        className: 'btn-table',
        exportOptions: {
          modifier: {
            search: 'applied',
            order: 'applied'
          }
        }
      }
      ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/refresh_cashbank') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'profit_center': JSON.stringify(params.profit),
          'status': JSON.stringify(params.status)

        }
      },"columns": [

      { 
        data: 0
      },
      { 
        data: 1
      },
      { 
        data: 2
      },
      { 
        data: 3,
        render: function(data, type, row){
          var formmatedvalue=numberWithCommas(data);

          return "Rp "+formmatedvalue;
        }
      },
      { 
        data: 4,
        render: function(data, type, row){
          var formmatedvalue=numberWithCommas(data);

          return "Rp "+formmatedvalue;
        }
      }


      ],

      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
              return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
              i : 0;
            };

            // computing column Total of the complete result 
            var monTotal = api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            var tueTotal = api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            

            // Update footer by showing the total with the reference of the column index 
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html('');
            $( api.column( 2 ).footer() ).html('');
            $( api.column( 3 ).footer() ).html('Rp '+ numberWithCommas(monTotal));
            $( api.column( 4 ).footer() ).html('Rp '+ numberWithCommas(tueTotal));

          }

        });



  });  



function getValueCheckbox(){
 var year = $('#year').val();
 var month = $('#month').val();
 var i = 0;
 var entitas = $('#entitas').val();
 var params = {
  profit: [],
  status: []
};
$('#profit-center :selected').each(function() {
  params.profit.push($(this).val());
});

$('#status :selected').each(function() {
  params.status.push($(this).val());
});

var data_table1 = $('#tbl_cashbank').DataTable({
  "scrollY": "1000px",
  "scrollCollapse": true,
  "scrollX": false,
  "ordering": true,
  "bDestroy": true,
  "serverSide": true,
  "bFilter": false,
  "bLengthChange": false,
  "processing": true,

  "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
  "dom"         : 'Blfrtip',
  buttons: [
  {
    extend: 'excelHtml5',
    text: "<i class='fas fa-download fa-lg'></i> Download",
    className: 'btn-table',
    exportOptions: {
      modifier: {
        search: 'applied',
        order: 'applied'
      }
    }
  }
  ],
  pageLength: 25,

  "ajax": {
    "url": "<?php echo base_url('index.php/page/refresh_cashbank') ?>",
    "method" : "POST",
    "data": {
      'month': JSON.stringify(month),
      'year': JSON.stringify(year),
      'entitas': JSON.stringify(entitas),
      'profit_center': JSON.stringify(params.profit),
      'status': JSON.stringify(params.status)

    }
  },"columns": [

  { 
    data: 0
  },
  { 
    data: 1
  },
  { 
    data: 2
  },
  { 
    data: 3,
    render: function(data, type, row){
      var formmatedvalue=numberWithCommas(data);

      return "Rp "+formmatedvalue;
    }
  },
  { 
    data: 4,
    render: function(data, type, row){
      var formmatedvalue=numberWithCommas(data);

      return "Rp "+formmatedvalue;
    }
  }


  ],

  "footerCallback": function ( row, data, start, end, display ) {
    var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
              return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
              i : 0;
            };

            // computing column Total of the complete result 
            var monTotal = api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            var tueTotal = api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            

            // Update footer by showing the total with the reference of the column index 
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html('');
            $( api.column( 2 ).footer() ).html('');
            $( api.column( 3 ).footer() ).html('Rp '+ numberWithCommas(monTotal));
            $( api.column( 4 ).footer() ).html('Rp '+ numberWithCommas(tueTotal));

          }

        });
}

$("#btnSubmit").click(function (e) {
 e.preventDefault();
 var year = $('#year').val();
 var month = $('#month').val();
 var i = 0;
 var entitas = $('#entitas').val();
 var params = {
  profit: [],
  status: []
};
var urlpost = "<?php echo base_url('index.php/page/get_profitcenter') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'entitas': JSON.stringify(entitas)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
        $('#profit-center')
        .find('option')
        .remove()
        .end();


        $.each(result.data, function( key, value ) {
         $('#profit-center')
         .append($("<option></option>")
           .attr("value",value.profit_center)
           .text(value.profit_center));

         $('#profit-center').multiselect('rebuild');
         $("#profit-center").multiselect('selectAll', false);

         $('#profit-center').multiselect('updateButtonText');
          //$('#kota').append('<option value="'+value+'">'+value+'</option>');
        })
        $('#profit-center :selected').each(function() {
          params.profit.push($(this).val());
        });

        $('#status :selected').each(function() {
          params.status.push($(this).val());
        });

      }








      var data_table1 = $('#tbl_cashbank').DataTable({
        "scrollY": "1000px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/refresh_cashbank') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'profit_center': JSON.stringify(params.profit),
            'status': JSON.stringify(params.status)

          }
        },"columns": [

        { 
          data: 0
        },
        { 
          data: 1
        },
        { 
          data: 2
        },
        { 
          data: 3,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 4,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        }


        ],
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
              return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
              i : 0;
            };

            // computing column Total of the complete result 
            var monTotal = api
            .column( 3 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            var tueTotal = api
            .column( 4 )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b);
            }, 0 );

            

            // Update footer by showing the total with the reference of the column index 
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html('');
            $( api.column( 2 ).footer() ).html('');
            $( api.column( 3 ).footer() ).html('Rp '+ numberWithCommas(monTotal));
            $( api.column( 4 ).footer() ).html('Rp '+ numberWithCommas(tueTotal));

          }
        });





    });

function numberWithCommas(x) {

 if (x){
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}else{
  return 0;
}
}
</script>