<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>asset/logo.png">
<title>Kimia Farma | Performance Dashbaord</title>
<!-- Bootstrap Core CSS -->
<link href="<?=base_url()?>asset/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?=base_url()?>asset/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?=base_url()?>asset/css/style.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body style="color:#FAFFBD">
<!-- Preloader -->
<div class="preloader" style="display: none;">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar" style="background-color:transparent">
    <div class="white-box" style="background-color:transparent">
      <form class="form-horizontal form-material" method='POST' id="loginform" action="<?=site_url("page/login")?>">
        <div style='text-align: center;margin-bottom: 20px;'><img style='width: 300px' src='<?=base_url()?>asset/logo.png'/></div>
        <span style="display: block;font-style:italic;color:#757575;font-weight: 400;text-align: center;" >Dashboard Performance</span>
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" name='username' placeholder="Username" style="background: #FFFFFF;border-radius: 7px;text-align: center;">
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" name='password' placeholder="Password" style="background: #FFFFFF;border-radius: 7px;text-align: center;">
          </div>
        </div>
        <!-- <div class="form-group">
          <div class="col-md-12">
            <a href="javascript:void(0)" id="to-recover" class="pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div> -->
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"
            style='background: #08388F;'>Log In</button>
          </div>
        </div>
        <div class="row">
        </div>
      </form>
      <form class="form-horizontal" id="recoverform" action="<?=base_url()?>">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?=base_url()?>asset/js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?=base_url()?>asset/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=base_url()?>asset/js/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?=base_url()?>asset/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url()?>asset/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=base_url()?>asset/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?=base_url()?>asset/js/jQuery.style.switcher.js"></script>


<input id="ext-version" type="hidden" value="1.4.0"></body></html>