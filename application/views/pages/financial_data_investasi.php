<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Investasi 2018</h4>
        </div>
    </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                            <th rowspan="2" scope="col">Current Year Collection (CYC) & Collection Rate (CR)</th>
                            <th rowspan="2" scope="col">Unit</th>
                            <th colspan="3" scope="col">CYC(IV/Des/2018)</th>
                            <th colspan="3" scope="col">CYC(Jan s.d IV/Des/2018)</th>
                        </tr>
                        <tr>
                            <th scope="col">Target</th>
                            <th scope="col">Real</th>
                            <th scope="col">Ach.</th>
                            <th scope="col">Target</th>
                            <th scope="col">Real</th>
                            <th scope="col">Ach.</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>a. Cash Collection</th>
                          <td>Rp M</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>i. Internal</td>
                          <td>Rp M</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>i. Eksternal</td>
                          <td>Rp M</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>b. Cash Collection Ratio</th>
                          <td>%</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>i. Internal</td>
                          <td>%</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>i. Eksternal</td>
                          <td>%</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                      </tbody>
                    </table>
		</div>
	</div>
</div>