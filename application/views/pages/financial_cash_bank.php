<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Cash Bank</h4>
        </div>
        <div class="col-lg-2 text-right">
            <h6>Company: </h6>
        </div>
        <div class="col-lg-2 text-right">
            <form>
                <div class='group-control'>
                    <select class='form-control'>
                        <option value='All'>Kimia Farma Holding</option>
                        <option value='KF Console'>Kimia Farma Holding 2</option>
                        <option value='KFHO'>Kimia Farma Holding 3</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-lg-2 text-right">
            <h6>Profit Center:</h6> 
        </div>
        <div class="col-lg-2 text-right">
            <form>
                <div class='group-control'> 
                    <select class='form-control'>
                        <option value='All'>2201</option>
                        <option value='KF Console'>2202</option>
                        <option value='KFHO'>2203</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">GL Account</th>
                          <th scope="col">Beginning Ballance</th>
                          <th scope="col">Ending Balance</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                        </tr>
                        <tr>
                          <td>2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                        </tr>
                        <tr>
                          <td>3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                        </tr>
                      </tbody>
                    </table>
		</div>
	</div>
</div>