<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Value : <select id="entitas" name="entitas"><?php $ent = ['KFHO','KFA','KFTD','KFSP'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
        Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
            if($i==$_SESSION['year']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
            if($i==$_SESSION['month']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Week : <select name="week"><?php for($i=1;$i<=4;$i++){
            if($i==$_SESSION['week']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Day : <select name="day"><?php for($i=1;$i<=31;$i++){
            if($i==$_SESSION['day']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?>
    </select>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>


<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>asset/logo.png">
    <title>Kimia Farma | Performance Dashbaord</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <!-- Bootstrap Core CSS -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>

    <link href="<?=base_url()?>asset/css/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?=base_url()?>asset/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>asset/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?=base_url()?>asset/css/default.css" id="theme" rel="stylesheet">

    <!-- color CSS -->
    <link href="<?=base_url()?>asset/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">

   
    

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>asset/js/highstock.js"></script>
<script src="<?=base_url()?>asset/js/exporting.js"></script>

<script src="<?=base_url()?>asset/js/jquery.easypiechart.min.js"></script>
<script src="<?=base_url()?>asset/plugins/daterangepicker/moment.min.js"></script>
<script src="<?=base_url()?>asset/plugins/daterangepicker/daterangepicker.js"></script>
