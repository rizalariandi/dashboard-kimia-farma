<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>


<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
  <h4 class="page-title">Realisasi Investasi</h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                  foreach ($_SESSION['role_entitas'] as $e) {

                                                    echo "<option value='$e'>$e</option>";
                                                  } ?></select>
    Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                            if ($i == $_SESSION['year']) {
                                              echo "<option value='$i' selected>$i</option>";
                                            } else {
                                              echo "<option value='$i'>$i</option>";
                                            }
                                          } ?></select>
    Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                              if ($i == $_SESSION['month']) {
                                                echo "<option value='$i' selected>$i</option>";
                                              } else {
                                                echo "<option value='$i'>$i</option>";
                                              }
                                            } ?></select>

    <input type='button' id="btnSubmit" value='Filter' class='btn btn-success' />
  </form>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
  .dataTables_scrollBody tfoot {
    display: none
  }

  .font-weight-bold {
    font-weight: bold;
  }

  .dataTables_scroll th {
    background-color: #093890;
    color: white;
    text-align: center;
  }

  td {
    color: #4A5675;
    text-align: center;
  }

  .dataTables_wrapper .dt-buttons {
    float: right;
  }

  .btn-table {
    background-color: #08388F !important;
    text-decoration: none;
  }

  .btn-table:hover {
    color: #fff !important;
    text-decoration: none;
  }
</style>
<div class="row" style="background:white;padding: 10px;">
  <div class="col-md-12">
     <!-- <h4 class="page-title">Realisasi Investasi</h4> -->
  <div class="chart-container">
    <canvas id="bar-chartcanvas" height="70vh"></canvas>
  </div>
  </div>
</div>
<div class="row">
  <div class="white-box">
    <div class="table-responsive">
      <table class="table" id='realisasi-data' class='stripe'>
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Uraian</th>
            <th scope="col">Target (Rp)</th>
            <th scope="col">Realisasi (Rp)</th>
            <th scope="col">%Achievement</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot align="right">
          <tr>
            <th style="color: rgb(0, 0, 0);background-color: white; width: 354.4px;text-align: left;"></th>
            <th style="color: rgb(0, 0, 0);background-color: white; width: 460px;text-align: right;"></th>
            <th style="color: rgb(0, 0, 0);background-color: white; width: 300px;text-align: right;"></th>
            <th style="color: rgb(0, 0, 0);background-color: white; width: 364.8px;text-align: right;"></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>


<script>
  //get the bar chart canvas
  var ctx = $("#bar-chartcanvas");

  //bar chart data
  var data;
  var chart;
  //options
  var options = {
    tooltips: {
      enabled: true,
      callbacks: {
        label: function(tooltipItem, data) {
          console.log('tooltipItem',tooltipItem)
          console.log('data',data)
          var value = tooltipItem.yLabel
          value = value.toString();
          value = value.split(/(?=(?:...)*$)/);
          value = value.join(',');
          return data.datasets[tooltipItem.datasetIndex].label+' : '+value;
        }
      }
    },
    legend:{
      position:'bottom'
    },
    scales: {
      yAxes: [{
        display: false,
        gridLines: {
          display: false
        },
        ticks: {
          display: true,
          beginAtZero: true
        }
      }],
      xAxes: [{
        gridLines: {
          display: false
        },
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  //$('#realisasi-data').DataTable();
  var totalTarget = 0,
    totalRealisasi = 0,
    totalAchievement = 0,
    data_table;
  var year = $('#year').val();
  var month = $('#month').val();
  var entitas = $('#entitas').val();
  $(document).ready(function() {
    data_table = $('#realisasi-data').DataTable({
      "scrollY": true,
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bFilter": true,
      "bLengthChange": true,
      "lengthMenu": [
        [25, 100, -1],
        [25, 100, "All"]
      ],

      pageLength: 25,
      dom: 'Bfrtip',
      buttons: [{
        extend: 'excelHtml5',
        title: function() {
          return 'Realisasi Investasi_' + $('#entitas').val() + '_' + $('#year').val() + '_' + $('#month').val();
        },
        text: "<i class='fas fa-download fa-lg'></i> Download",
        className: 'btn-table'
      }],
      "columns": [{
          data: 'num',
        },
        {
          data: 'uraian',
          title: 'Uraian',
          className: 'text-left',
        },
        {
          data: 'target',
          title: 'Target (Rp)',
          className: 'text-right',
          render: $.fn.dataTable.render.number(",", ".", 0)
        },
        {
          data: 'realisasi',
          title: 'Realisasi (Rp)',
          className: 'text-right',
          render: $.fn.dataTable.render.number(",", ".", 0)
        },
        {
          data: 'achievement',
          title: 'Achievement (%)',
          className: 'text-right'
        }
      ],
      "footerCallback": function(row, data, start, end, display) {
        var sumTarget = 0;
        var sumRealisasi = 0;
        var sumAChievement = 0;
        var api = this.api(),
          data;

        // converting to interger to find total
        var intVal = function(i) {
          return typeof i === 'string' ?
            i.replace(/[\$,%]/g, '') * 1 :
            typeof i === 'number' ?
            i : 0;
        };

        // Total over this page
        pageTotal = api
          .column(2, {
            page: 'current'
          })
          .data()
          .reduce(function(a, b) {
            return intVal(a) + intVal(b);
          }, 0);

        // Total over all pages
        var tueTotal = api
          // .column( 3 )
          .column(2)
          .data()
          .reduce(function(a, b) {
            return intVal(a) + intVal(b);
          }, 0);

        // Total filtered rows on the selected column (code part added)
        sumTarget = display.map(el => data[el].target).reduce((a, b) => intVal(a) + intVal(b), 0);
        sumRealisasi = display.map(el => data[el].realisasi).reduce((a, b) => intVal(a) + intVal(b), 0);
        sumAChievement = (sumRealisasi / sumTarget) * 100

        // Update footer by showing the total with the reference of the column index 
        $(api.column(0).footer()).html('Total');
        $(api.column(1).footer()).html(numberWithCommas(sumTarget));
        $(api.column(2).footer()).html(numberWithCommas(sumRealisasi));
        $(api.column(3).footer()).html((isNaN(sumAChievement) ? 0 : sumAChievement).toFixed(2) + ' %');

      }

    });

    getValueCheckbox()
  })


  function getValueCheckbox() {
    var year = $('#year').val();
    var month = $('#month').val();
    var entitas = $('#entitas').val();
    $.ajax({
      url: "<?php echo base_url('index.php/page/realisasi_investasi_data') ?>",
      method: 'POST',
      data: {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'entitas': JSON.stringify(entitas),
      },
      beforeSend: function() {
        // loading();
      },
      success: function(data) {
        newRow = JSON.parse(data)

      }
    }).done(function() {
      chartBar(newRow.chart);
      if (newRow.data.length !== 0) {
        data_table.clear().draw();
        data_table.rows.add(newRow.data);
        data_table.columns.adjust().draw();
      } else {
        data_table.clear().draw();
      }
    }).fail(function(jqXHR, textStatus, errorThrown) {
      // if (jqXHR.status != 422)
      //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
    });
  }

  $("#btnSubmit").click(function(e) {
    getValueCheckbox()
  })

  function chartBar(items) {
    $("#bar-chartcanvas").empty();
    var data1 = []
    var data2 = []
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
    var labels = []
    items.forEach(function(element, key) {
      data1.push(element.target);
      data2.push(element.realisasi);
      labels.push(month[key]);
    });

    data = {
      labels: labels,
      datasets: [{
          label: "Target",
          data: data1,
          backgroundColor: "#F7830D",
          borderColor: "rgba(10,20,30,1)",
          borderWidth: 1
        },
        {
          label: "Realisasi",
          data: data2,
          backgroundColor: "#5774C9",
          borderColor: "rgba(50,150,200,1)",
          borderWidth: 1
        }
      ]
    };

    //create Chart class object
    if (chart) {
      chart.destroy();
    }
    chart = new Chart(ctx, {
      type: "bar",
      data: data,
      options: options
    });
  }

  function numberWithCommas(x) {
    if (x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
      return 0;
    }
  }
</script>