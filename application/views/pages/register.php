<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>asset/favicon.png">
<title>Register Digital Touchpoint</title>
<!-- Bootstrap Core CSS -->
<link href="<?=base_url()?>asset/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?=base_url()?>asset/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?=base_url()?>asset/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?=base_url()?>asset/css/colors/default.css" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body style="">

<!-- Preloader -->
<div class="preloader" style="display: none;">
  <div class="cssload-speeding-wheel"></div>
</div>

<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal form-material" method='POST' id="loginform" action="<?=base_url()?>index.php/page/register">
        <h3 class="box-title m-b-20">Register</h3>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Nama Lengkap" name='nama'>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email" name='email'>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Nomor Handphone" name='hp'>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <textarea name='alamat' placeholder="Alamat" class="form-control" required></textarea>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Institusi / Unit" name='unit'>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Password" name='password'>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Confirm Password" name='password2'>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Already have an account? <a href="<?=base_url()?>index.php/page/login" class="text-primary m-l-5"><b>Sign In</b></a></p>
          </div>
        </div>
      </form>

      <div class="social-icon">
        <h2>Contact Helpdesk </h2>
        <ul class="social-network">
          <a href="#" class="phone tool-tip" title="Handphone"><i class="fa fa-phone"></i></a> 0838-7458-5646&nbsp;</li>&nbsp;&nbsp;&nbsp;
          &nbsp;<a href="#" class="envelope tool-tip" title="Envelope"><i class="fa fa-envelope"></i> dtp.dsc@gmail.com </a></li>
        </ul>
      </div>
    </div>
  </div>
</section>


<!-- jQuery -->
    <script src="<?=base_url()?>asset/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>asset/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?=base_url()?>asset/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?=base_url()?>asset/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>asset/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url()?>asset/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="<?=base_url()?>asset/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>


<input id="ext-version" type="hidden" value="1.3.5"></body></html>
