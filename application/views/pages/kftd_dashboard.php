<style type="text/css">
.last_update {
    position: absolute;
   

    font-family: Roboto;
    font-style: italic;
    font-weight: normal;
    font-size: 12px;
    line-height: 20px;/* or 167% */

    color: #4F4F4F;
}

</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
        Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
            if($i==$_SESSION['year']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
            if($i==$_SESSION['month']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Week : <select name="week"><?php for($i=1;$i<=4;$i++){
            if($i==$_SESSION['week']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Day : <select name="day"><?php for($i=1;$i<=31;$i++){
            if($i==$_SESSION['day']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?>
    </select>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>
<div class="row">
  <div class="col-md-12">

    <i class="fa fa-circle" style="color: #cb3935; font-size: 1.5em;"></i><b> &#60; 80%</b>&nbsp;&nbsp;&nbsp;&nbsp;
    <i class="fa fa-circle" style="color: #f0ad4e; font-size: 1.5em;"></i><b> 80% &#45; 95%</b>&nbsp;&nbsp;&nbsp;&nbsp;
    <i class="fa fa-circle" style="color: #5cb85c; font-size: 1.5em;"></i><b> &#62; 90%</b>

  </div>
</div>
<?php
$i = 230;
$data = [
    'Financial Perspective'=>['Net Sales','COGS','Gross Profit','Operating Expenses','Net Operating Income','Other Income','Net Income'],
    'Marketing Perspective'=>['Total Sales','Growth','Achievement','','','',''],
    'Internal Process Perspective'=>['Suplay Chain','Logistic','','','','',''],
    'Learning and Growth Perspective'=>['Man Days Training(MDT)','Employee Productivity','Number of Employee','','','',''],
    'Leadership and Social Responsibility'=>['Health Level Score','Portal BUMN','GCG Score','','','',''],
    'Development Agent Perspective'=>['Synergy of BUMN','Downstreaming & Local','','','','','']];
    $s = 1;
    $k = 1;
    if($_SESSION['level']=='kfc'){
        unset($data['Internal Process Perspective']);
    }
    foreach($data as $key=>$d){
        if($s % 2==1){
            echo "<div class='row' style='display: table;width: 100%;'>";
        }
        ?>
        <div class="col-md-6" style='display: table-cell'>
            <div class="white-box">
                <div class='text-right'><a href='<?=base_url()?>index.php/page/view/income_s'>Detail <i class='fas fa-angle-right'></i></a></div>
                <h3 class="box-title"><?=$key?></h3> 
                <?php foreach($d as $c){
                    $x = rand(70,110);
                    $prog ='';
                    if($x<80){
                        $prog='danger';
                    }elseif($x>80 && $x<95){
                        $prog='warning';
                    }else{
                        $prog='success';
                    }
                    $j = 1;
                    if ($key === 'Financial Perspective' ){
                        ?>
                        <div class='row'>
                            <div class='col-md-4'>
                                <?=$c?> 
                            </div>
                            <div class='col-md-6'>
                                <div id="progres<?=$k?>" class="progress progress-lg">
                                    <div class="progress-bar progress-bar-<?=$prog?>" role="progressbar" style="width: <?=$x?>%;" aria-valuenow="<?=$x?>" aria-valuemin="0" aria-valuemax="100"><?=$x?></div>
                                </div>
                            </div>
                            <div id="prog-value<?=$k?>" class='col-md-2'>
                                <?php

                                $i = $i - 20;
                                echo "Rp ". $i ."M";
                                ?>
                                
                            </div>
                        </div>
                        <?php
                            if ($k === sizeof($d)){
                              $k=1;
                            
                        ?>
                        <div class="row last_update">
                            Last Update <?php echo date("d/m/Y"); ?>
                        </div>

                        <?php
                            }else{
                                 $k++;
                            }
                        ?>
                        <?php 
                       
                    }else{
                        ?>
                        <div class='row'>
                            <div class='col-md-4'>
                                <?=$c?> 
                            </div>
                            <div class='col-md-6'>
                                <?php
                                if (strlen($c)===0){
                                 ?>

                                 <div class="progress progress-lg" style="background-color: #ffffff;">

                                 </div>
                                 <?php 

                             }else{
                                ?>

                                <div class="progress progress-lg">
                                  <div class="progress-bar progress-bar-<?=$prog?>" role="progressbar" style="width: <?=$x?>%; color: #000000;" aria-valuenow="<?=$x?>" aria-valuemin="0" aria-valuemax="100"><?=$x?></div>
                              </div>
                              <?php


                          }

                          ?>


                      </div>
                      <?php
                      if (strlen($c)===0){
                        ?>
                        <div class='col-md-2'>

                        </div>

                        <?php

                    }else{

                        ?>
                        <div class='col-md-2'>
                           100%
                       </div>
                      
                       <?php
                   }   
                   ?>

               </div>
                 <?php
                         
                            if ($k === sizeof($d)){
                               $k=1;
                            
                        ?>
                        <div class="row last_update">
                            Last Update <?php echo date("d/m/Y"); ?>
                        </div>

                        <?php
                            }else{
                                $k++;
                            }
                        ?>

               <?php
               
           }
       } ?>
   </div>

</div>

<?php 
if($s % 2==0){
    echo "</div>";
}
$s++;
} ?>


<script>
    $(document).ready(function() {


      var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";

      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();
      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)


          
      },
      AjaxSucceeded,"json"

      );
      function AjaxSucceeded(result) {

         $.each(result.is_sum_income, function( key, value ) {

             if (value.income_statement == 'NET SALES'){
              var achievement = (value.sum_realisasi / value.sum_target)*100;
              var achiev;
              if (isNaN(achievement)){
                achiev = 0;
            }else{
                achiev = achievement;
            }

            achiev = isFinite(achiev) ? achiev : 0;

            $('#progres1').empty();
            $('#prog-value1').empty();
            var prog ='';
            if (achiev !== 0){
               if(achiev < 80){
                   $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value1').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres1').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value1').append(format_round(value.sum_target));
              }else{
                  $('#progres1').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value1').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value1').append(0);
        }


    }else if (value.income_statement == 'COGS'){
      var achievement = (value.sum_realisasi / value.sum_target)*100;
      var achiev;
      if (isNaN(achievement)){
        achiev = 0;
    }else{
        achiev = achievement;
    }

    achiev = isFinite(achiev) ? achiev : 0;

    $('#progres2').empty();
    $('#prog-value2').empty();
    var prog ='';
       if (achiev !== 0){
               if(achiev < 80){
                   $('#progres2').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value2').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres2').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value2').append(format_round(value.sum_target));
              }else{
                  $('#progres2').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value2').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres2').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value2').append(0);
        }

}else if (value.income_statement == 'GROSS PROFIT'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres3').empty();
$('#prog-value3').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value3').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres3').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value3').append(format_round(value.sum_target));
              }else{
                  $('#progres3').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value3').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value3').append(0);
        }
}else if (value.income_statement == 'OPERATING EXPENSES'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres4').empty();
$('#prog-value4').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres4').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value4').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres4').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value4').append(format_round(value.sum_target));
              }else{
                  $('#progres4').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value4').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres4').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value4').append(0);
        }
}else if (value.income_statement == 'NET OPERATING INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres5').empty();
$('#prog-value5').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value5').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres5').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value5').append(format_round(value.sum_target));
              }else{
                  $('#progres5').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value5').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value5').append(0);
        }
}else if (value.income_statement == 'OTHER INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres6').empty();
$('#prog-value6').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value6').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres6').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value6').append(format_round(value.sum_target));
              }else{
                  $('#progres6').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value6').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value6').append(0);
        }
}else if (value.income_statement == 'NET INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres7').empty();
$('#prog-value7').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres7').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value7').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres7').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value7').append(format_round(value.sum_target));
              }else{
                  $('#progres7').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value7').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres7').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value7').append(0);
        }

}
})
}


})


$("#btnSubmit").click(function (e) {

  e.preventDefault();


      var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";

      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();
      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)


          
      },
      AjaxSucceeded,"json"

      );
      function AjaxSucceeded(result) {

         $.each(result.is_sum_income, function( key, value ) {

             if (value.income_statement == 'NET SALES'){
              var achievement = (value.sum_realisasi / value.sum_target)*100;
              var achiev;
              if (isNaN(achievement)){
                achiev = 0;
            }else{
                achiev = achievement;
            }

            achiev = isFinite(achiev) ? achiev : 0;

            $('#progres1').empty();
            $('#prog-value1').empty();
            var prog ='';
            if (achiev !== 0){
               if(achiev < 80){
                   $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value1').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres1').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value1').append(format_round(value.sum_target));
              }else{
                  $('#progres1').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value1').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres1').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value1').append(0);
        }


    }else if (value.income_statement == 'COGS'){
      var achievement = (value.sum_realisasi / value.sum_target)*100;
      var achiev;
      if (isNaN(achievement)){
        achiev = 0;
    }else{
        achiev = achievement;
    }

    achiev = isFinite(achiev) ? achiev : 0;

    $('#progres2').empty();
    $('#prog-value2').empty();
    var prog ='';
       if (achiev !== 0){
               if(achiev < 80){
                   $('#progres2').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value2').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres2').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value2').append(format_round(value.sum_target));
              }else{
                  $('#progres2').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value2').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres2').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value2').append(0);
        }

}else if (value.income_statement == 'GROSS PROFIT'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres3').empty();
$('#prog-value3').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value3').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres3').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value3').append(format_round(value.sum_target));
              }else{
                  $('#progres3').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value3').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres3').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value3').append(0);
        }
}else if (value.income_statement == 'OPERATING EXPENSES'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres4').empty();
$('#prog-value4').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres4').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value4').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres4').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value4').append(format_round(value.sum_target));
              }else{
                  $('#progres4').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value4').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres4').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value4').append(0);
        }
}else if (value.income_statement == 'NET OPERATING INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres5').empty();
$('#prog-value5').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value5').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres5').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value5').append(format_round(value.sum_target));
              }else{
                  $('#progres5').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value5').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres5').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value5').append(0);
        }
}else if (value.income_statement == 'OTHER INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres6').empty();
$('#prog-value6').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value6').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres6').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value6').append(format_round(value.sum_target));
              }else{
                  $('#progres6').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value6').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres6').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value6').append(0);
        }
}else if (value.income_statement == 'NET INCOME'){
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
}else{
    achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;

$('#progres7').empty();
$('#prog-value7').empty();
var prog ='';
   if (achiev !== 0){
               if(achiev < 80){
                   $('#progres7').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                   $('#prog-value7').append(format_round(value.sum_target));
               }else if(achiev >= 80 && achiev <= 95){
                  $('#progres7').append("<div class='progress-bar progress-bar-warning' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value7').append(format_round(value.sum_target));
              }else{
                  $('#progres7').append("<div class='progress-bar progress-bar-success' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
                  $('#prog-value7').append(format_round(value.sum_target));
              }

          }else{
            achiev = 0;
            $('#progres7').append("<div class='progress-bar progress-bar-danger' role='progressbar' style='width: "+achiev+"%; color: #000000;' aria-valuenow='"+achiev+"' aria-valuemin='0' aria-valuemax='100'>"+achiev.toFixed(0)+"</div>");
            $('#prog-value7').append(0);
        }

}
})
}


})

function numberWithCommas(x) {
    //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if (x){
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }else{
      return 0;
  }

}

function format_round(rp){

    if(rp<1 && rp>0){
      return (Math.round(rp*100))+'%';

  }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
  }else{
      return ("Rp "+numberWithCommas(rp));
  }
}
</script>    