<div class="row">
	<div class="col-md-12">
	<div class="white-box">
<form class="form-horizontal">
	<div class="form-group">
		<label for="exampleInputuname" class="col-sm-2 control-label">ID Request</label>
		<div class="col-sm-9">
			<div class="input-group">
				DSC<?=$form->id?> </div>
		</div>
	</div>
	<div class="form-group">
		<label for="exampleInputuname" class="col-sm-2 control-label">Subject</label>
		<div class="col-sm-9">
			<div class="input-group">
				<?=$form->subject?> </div>
		</div>
	</div>
	<div class="form-group">
		<label for="exampleInputuname" class="col-sm-2 control-label">Isi</label>
		<div class="col-sm-9">
			<div class="input-group">
				<?=$form->isi?> </div>
		</div>
	</div>
</form>
	</div>
</div>
</div>

<div class="row">
	<div class="col-md-12">
	<div class="white-box">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>No</th>
						<th>Process</th>
						<th>Detail</th>
						<th>PIC</th>
						<th>Due Date</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
<?php $i=1; foreach($table as $t): ?>
<tr>
	<td><?=$i++?></td>
	<td><?=$t['nama']?></td>
	<td><?=$t['detail']?></td>
	<td><?=$t['pic']?></td>
	<td><?=$t['due_date']?></td>
<?php
$btn="";
	switch(strtolower($t['status'])){
		case 'rejected' : $btn='danger'; break;
		case 'process' : $btn='primary'; break;
		case 'closed' : $btn='success'; break;
		case 'accept' : $btn='default'; break;
	}
?>
	<td><a class='btn btn-<?=$btn?>'><?=$t['status']?></a></td>
</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>