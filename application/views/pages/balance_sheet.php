<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>

<style type="text/css">
    .last_update {

        padding-left: 2px;


        font-weight: normal;
        font-size: 12px;
        line-height: 10px;
        /* identical to box height, or 167% */



        color: #fffff;

        font-family: Roboto;
        font-style: italic;
    }
</style>
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

    <h4 class="last_update"> </h4>
    </h4>
    <h4 id="ket" class="page-title last_update"><?php if ($bs[0]['ket'] != 'adjusted') {
                                                    echo '<sup>*</sup>Note: Data ' . $bs[0]['ket'];
                                                } ?></h4>

</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Period : <select id="period" name="period"><?php $ent = ['Monthly', 'YearToDate'];
                                                    foreach ($ent as $e) {
                                                        if ($e == $_SESSION['period']) {
                                                            echo "<option value='$e' selected>$e</option>";
                                                        } else {
                                                            echo "<option value='$e'>$e</option>";
                                                        }
                                                    } ?></select>
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($_SESSION['role_entitas'] as $e) {

                                                            echo "<option value='$e'>$e</option>";
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        <label id="label_day">Day :</label> <select id="select_day" name="day"><?php for ($i = 1; $i <= 31; $i++) {
                                                                                    if ($i == $_SESSION['day']) {
                                                                                        echo "<option value='$i' selected>$i</option>";
                                                                                    } else {
                                                                                        echo "<option value='$i'>$i</option>";
                                                                                    }
                                                                                } ?>
        </select>
        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>

<style>
    table tbody tr.odd {
        background: #F7FAFC;
    }

    table tr th {
        background: #093890;
    }
</style>

<div class="row">

    <div class="col-md-3">        
            <div class="col-md-7  p-0">
                <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                    <h3 class="box-title">WORKING CAPITAL</h3>
                    <h2><b id="working_capital">0</b></h2>
                </div>
            </div>
            <div class="col-md-5 p-0">
                <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                    <h5 class="box-title title_up">Des 2018</h5>
                    <h5><b id="working_capital_ly">0</b></h5>
                </div>
                <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                    <h5 class="box-title title_down">Des 2019</h5>
                    <h5><b id="working_capital_dls">0</b></h5>
                </div>            
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                <h3 class="box-title">CURRENT RASIO</h3>
                <h2><b id="current_ratio">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="current_ratio_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="current_ratio_dls">0</b></h5>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                <h3 class="box-title">CASH RASIO</h3>
                <h2><b id="cash_ratio">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="cash_ratio_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="cash_ratio_dls">0</b></h5>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                <h3 class="box-title">ACID TEST RASIO</h3>
                <h2><b id="acid_rasio">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="acid_rasio_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="acid_rasio_dls">0</b></h5>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 16px 10px;">
                <h3 class="box-title">DER (INTEREST BEARING)</h3>
                <h2><b id="der">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="der_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="der_dls">0</b></h5>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                <h3 class="box-title">DEBT RASIO</h3>
                <h2><b id="debt_ratio">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="debt_ratio_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="debt_ratio_dls">0</b></h5>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="col-md-7  p-0">
            <div class="white-box" style="background: #FDEDDD;padding: 31px 10px">
                <h3 class="box-title">EQUITY RASIO</h3>
                <h2><b id="equity_ratio">0</b></h2>
            </div>
        </div>
        <div class="col-md-5 p-0">
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_up">Des 2019</h5>
                <h5><b id="equity_ratio_ly">0</b></h5>
            </div>
            <div class="white-box m-0" style="background: #FDEDDD;border: 1px solid;padding: 11px 0px 10px 5px;">
                <h5 class="box-title title_down">Des 2019</h5>
                <h5><b id="equity_ratio_dls">0</b></h5>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <ul class="nav customtab2 nav-tabs" role="tablist" style='float: right;border: unset;'>
                <li role="presentation" class="active"><a href="#home6" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Aktiva</span></a></li>
                <li role="presentation" class=""><a href="#profile6" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pasiva</span></a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="home6">
                    <div class="clearfix"></div>
                    <?php
                    foreach ($bs_arr['LANCAR'] as $k => $v) {
                        echo "<div class='row'><h2>$k</h2>
                            <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#FDEDDD;'><h2>" . format_round(array_sum($v)) . "</h2></td></tr></table></div>
                            <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th align='right'>Amount(IDR)<th>Growth</th></tr>";
                        foreach ($v as $key => $value) {
                            echo "<tr><td>$key</td><td align='right'>" . format_round($value) . "</td></tr>";
                        }
                        echo "</table></div>
                            </div>";
                    }
                    ?>
                    <div class="clearfix"></div>
                    <?php
                    foreach ($bs_arr['   '] as $k => $v) {
                        echo "<div id='div-eku' class='row'><h2>$k</h2>
                            <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#FDEDDD;'><h2>" . format_round(array_sum($v)) . "</h2></td></tr></table></div>
                            <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)<th>Growth</th></tr>";
                        foreach ($v as $key => $value) {
                            echo "<tr><td>$key</td><td align='right'>" . format_round($value) . "</td></tr>";
                        }
                        echo "</table></div>
                            </div>";
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="profile6">
                    <div class="clearfix"></div>
                    <?php
                    foreach ($bs_arr['TIDAK LANCAR'] as $k => $v) {
                        echo "<div class='row'><h2>$k</h2>
                        <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#FDEDDD;'><h2>" . format_round(array_sum($v)) . "</h2></td></tr></table></div>
                        <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th><th iped'><tr><th>Item</th><'>Growth</th></tr>";
                        foreach ($v as $key => $value) {
                            echo "<tr><td>$key</td><td align='right'>" . format_round($value) . "</td></tr>";
                        }
                        echo "</table></div>
                        </div>";
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    $('#period').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if (valueSelected === 'Monthly' || valueSelected === 'YearToDate') {


            $('#select_day').attr("hidden", true);
            $('#label_day').attr("hidden", true);

        }
        // else if (valueSelected === 'Daily') {
        //
        //
        //     $('#select_day').attr("hidden", false);
        //     $('#label_day').attr("hidden", false);
        //
        // }
    });

    $(document).ready(function() {


        $('#select_day').attr("hidden", true);
        $('#label_day').attr("hidden", true);


        generateDashboard();

    });


    $("#btnSubmit").click(function(e) {

        e.preventDefault();

        generateDashboard();

    });

    function textMonth(index) {
        item = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']
        return item[index - 1];
    }

    function generateDashboard() {
        var urlpost = "<?php echo base_url('index.php/page/refresh_balance_sheet') ?>";

        var year = $('#year').val();
        var month = $('#month').val();
        var period = $('#period').val();
        var entitas = $('#entitas').val();
        var day = $('#select_day').val();

        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'period': JSON.stringify(period),
                'day': JSON.stringify(day)


            },
            AjaxSucceeded, "json"
        );

        function AjaxSucceeded(result) {
            $(".title_up").text(textMonth($('#month').val()) + ' ' + ($('#year').val() - 1))
            $(".title_down").text(textMonth(12) + ' ' + ($('#year').val() - 1))
            // $.each(result, function( key, value ) {

            if (result.data.length !== 0) {
                var txt1 = $("<sup>*</sup>");
                var adj = "";
                if (result.bs.length !== 0) {
                    $('#ket').empty();
                    $('#ket').attr("class", 'page-title last_update');
                    if (result.bs[0].ket === 'adjusted') {
                        $('#ket').text("");
                        adj = 'adjusted';
                    } else {
                        adj = result.bs[0].ket;
                        $('#ket').append(txt1);
                        $('#ket').append("Note: Data " + result.bs[0].ket);
                    }
                }


                if (period === 'YearToDate') {
                    if (!result.data[0].WORKING_CAPITAL_YTD_A) {
                        $('#working_capital').text("0%");
                    } else {
                        $("#working_capital_ly").removeClass();
                        $("#working_capital_dls").removeClass()
                        $('#working_capital').text(format_round(result.data[0].WORKING_CAPITAL_YTD_A));
                        $('#working_capital_ly').text(format_round(result.data[0].WORKING_CAPITAL_YTD_B));
                        $('#working_capital_dls').text(format_round(result.data[0].WORKING_CAPITAL_YTD_C));                
                        $("#working_capital_ly").addClass(parseFloat(result.data[0].WORKING_CAPITAL_YTD_A) < parseFloat(result.data[0].WORKING_CAPITAL_YTD_B) ? "text-success" : "text-danger");
                        $("#working_capital_dls").addClass(parseFloat(result.data[0].WORKING_CAPITAL_YTD_A) < parseFloat(result.data[0].WORKING_CAPITAL_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].CURRENT_RATIO_YTD_A) {
                        $('#current_ratio').text("0.00%");
                    } else {
                        $("#current_ratio_ly").removeClass();
                        $("#current_ratio_dls").removeClass();
                        $('#current_ratio').text(parseFloat(result.data[0].CURRENT_RATIO_YTD_A).toFixed(2) + "%");
                        $('#current_ratio_ly').text(parseFloat(result.data[0].CURRENT_RATIO_YTD_B).toFixed(2) + "%");
                        $('#current_ratio_dls').text(parseFloat(result.data[0].CURRENT_RATIO_YTD_C).toFixed(2) + "%");
                        $("#current_ratio_ly").addClass(parseFloat(result.data[0].CURRENT_RATIO_YTD_A) < parseFloat(result.data[0].CURRENT_RATIO_YTD_B) ? "text-success" : "text-danger");
                        $("#current_ratio_dls").addClass(parseFloat(result.data[0].CURRENT_RATIO_YTD_A) < parseFloat(result.data[0].CURRENT_RATIO_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].CASH_RASIO_YTD_A) {
                        $('#cash_ratio').text("0.00%");
                    } else {
                        $("#cash_ratio_ly").removeClass();
                        $("#cash_ratio_dls").removeClass();
                        $('#cash_ratio').text(parseFloat(result.data[0].CASH_RASIO_YTD_A).toFixed(2) + "%");
                        $('#cash_ratio_ly').text(parseFloat(result.data[0].CASH_RASIO_YTD_B).toFixed(2) + "%");
                        $('#cash_ratio_dls').text(parseFloat(result.data[0].CASH_RASIO_YTD_C).toFixed(2) + "%");
                        $("#cash_ratio_ly").addClass(parseFloat(result.data[0].CASH_RASIO_YTD_A) < parseFloat(result.data[0].CASH_RASIO_YTD_B) ? "text-success" : "text-danger");
                        $("#cash_ratio_dls").addClass(parseFloat(result.data[0].CASH_RASIO_YTD_A) < parseFloat(result.data[0].CASH_RASIO_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].ACID_TEST_RASIO_YTD_A) {
                        $('#acid_rasio').text("0.00%");
                    } else {
                        $("#acid_rasio_ly").removeClass();
                        $("#acid_rasio_dls").removeClass();
                        $('#acid_rasio').text(parseFloat(result.data[0].ACID_TEST_RASIO_YTD_A).toFixed(2) + "%");
                        $('#acid_rasio_ly').text(parseFloat(result.data[0].ACID_TEST_RASIO_YTD_B).toFixed(2) + "%");
                        $('#acid_rasio_dls').text(parseFloat(result.data[0].ACID_TEST_RASIO_YTD_C).toFixed(2) + "%");
                        $("#acid_rasio_ly").addClass(parseFloat(result.data[0].ACID_TEST_RASIO_YTD_A) < parseFloat(result.data[0].ACID_TEST_RASIO_YTD_B) ? "text-success" : "text-danger");
                        $("#acid_rasio_dls").addClass(parseFloat(result.data[0].ACID_TEST_RASIO_YTD_A) < parseFloat(result.data[0].ACID_TEST_RASIO_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].DER_INTEREST_BEARING_YTD_A) {
                        $('#der').text("0%");
                    } else {
                        $("#der_ly").removeClass();;
                        $("#der_dls").removeClass();
                        $('#der').text(parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_A).toFixed(2) + "%");
                        $('#der_ly').text(parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_B).toFixed(2) + "%");
                        $('#der_dls').text(parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_C).toFixed(2) + "%");
                        $("#der_ly").addClass(parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_A) < parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_B) ? "text-success" : "text-danger");
                        $("#der_dls").addClass(parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_A) < parseFloat(result.data[0].DER_INTEREST_BEARING_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].DEBT_RASIO_YTD_A) {
                        $('#debt_ratio').text("0.00%");
                    } else {
                        $("#debt_ratio_ly").removeClass();
                        $("#debt_ratio_dls").removeClass();
                        $('#debt_ratio').text(parseFloat(result.data[0].DEBT_RASIO_YTD_A).toFixed(2) + "%");
                        $('#debt_ratio_ly').text(parseFloat(result.data[0].DEBT_RASIO_YTD_B).toFixed(2) + "%");
                        $('#debt_ratio_dls').text(parseFloat(result.data[0].DEBT_RASIO_YTD_C).toFixed(2) + "%");
                        $("#debt_ratio_ly").addClass(parseFloat(result.data[0].DEBT_RASIO_YTD_A) < parseFloat(result.data[0].DEBT_RASIO_YTD_B) ? "text-success" : "text-danger");
                        $("#debt_ratio_dls").addClass(parseFloat(result.data[0].DEBT_RASIO_YTD_A) < parseFloat(result.data[0].DEBT_RASIO_YTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].EQUITY_RASIO_YTD_A) {
                        $('#equity_ratio').text("0.00%");
                    } else {
                        $("#equity_ratio_ly").removeClass();
                        $("#equity_ratio_dls").removeClass();
                        $('#equity_ratio').text(parseFloat(result.data[0].EQUITY_RASIO_YTD_A).toFixed(2) + "%");
                        $('#equity_ratio_ly').text(parseFloat(result.data[0].EQUITY_RASIO_YTD_B).toFixed(2) + "%");
                        $('#equity_ratio_dls').text(parseFloat(result.data[0].EQUITY_RASIO_YTD_C).toFixed(2) + "%");
                        $("#equity_ratio_ly").addClass(parseFloat(result.data[0].EQUITY_RASIO_YTD_A) < parseFloat(result.data[0].EQUITY_RASIO_YTD_B) ? "text-success" : "text-danger");
                        $("#equity_ratio_dls").addClass(parseFloat(result.data[0].EQUITY_RASIO_YTD_A) < parseFloat(result.data[0].EQUITY_RASIO_YTD_C) ? "text-success" : "text-danger");
                    }

                } else {
                    if (!result.data[0].WORKING_CAPITAL_MTD_A) {
                        $('#working_capital').text("0%");
                    } else {
                        $("#working_capital_ly").removeClass();
                        $("#working_capital_dls").removeClass()
                        $('#working_capital').text(format_round(result.data[0].WORKING_CAPITAL_MTD_A));
                        $('#working_capital_ly').text(format_round(result.data[0].WORKING_CAPITAL_MTD_B));
                        $('#working_capital_dls').text(format_round(result.data[0].WORKING_CAPITAL_MTD_C));
                        $("#working_capital_ly").addClass(parseFloat(result.data[0].WORKING_CAPITAL_MTD_A) < parseFloat(result.data[0].WORKING_CAPITAL_MTD_B) ? "text-success" : "text-danger");
                        $("#working_capital_dls").addClass(parseFloat(result.data[0].WORKING_CAPITAL_MTD_A) < parseFloat(result.data[0].WORKING_CAPITAL_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].CURRENT_RATIO_MTD_A) {
                        $('#current_ratio').text("0.00%");
                    } else {
                        $("#current_ratio_ly").removeClass();
                        $("#current_ratio_dls").removeClass();
                        $('#current_ratio').text(parseFloat(result.data[0].CURRENT_RATIO_MTD_A).toFixed(2) + "%");
                        $('#current_ratio_ly').text(parseFloat(result.data[0].CURRENT_RATIO_MTD_B).toFixed(2) + "%");
                        $('#current_ratio_dls').text(parseFloat(result.data[0].CURRENT_RATIO_MTD_C).toFixed(2) + "%");
                        $("#current_ratio_ly").addClass(parseFloat(result.data[0].CURRENT_RATIO_MTD_A) < parseFloat(result.data[0].CURRENT_RATIO_MTD_B) ? "text-success" : "text-danger");
                        $("#current_ratio_dls").addClass(parseFloat(result.data[0].CURRENT_RATIO_MTD_A) < parseFloat(result.data[0].CURRENT_RATIO_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].CASH_RASIO_MTD_A) {
                        $('#cash_ratio').text("0.00%");
                    } else {
                        $("#cash_ratio_ly").removeClass();
                        $("#cash_ratio_dls").removeClass();
                        $('#cash_ratio').text(parseFloat(result.data[0].CASH_RASIO_MTD_A).toFixed(2) + "%");
                        $('#cash_ratio_ly').text(parseFloat(result.data[0].CASH_RASIO_MTD_B).toFixed(2) + "%");
                        $('#cash_ratio_dls').text(parseFloat(result.data[0].CASH_RASIO_MTD_C).toFixed(2) + "%");
                        $("#cash_ratio_ly").addClass(parseFloat(result.data[0].CASH_RASIO_MTD_A) < parseFloat(result.data[0].CASH_RASIO_MTD_B) ? "text-success" : "text-danger");
                        $("#cash_ratio_dls").addClass(parseFloat(result.data[0].CASH_RASIO_MTD_A) < parseFloat(result.data[0].CASH_RASIO_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].ACID_TEST_RASIO_MTD_A) {
                        $('#acid_rasio').text("0.00%");
                    } else {
                        $("#acid_rasio_ly").removeClass();
                        $("#acid_rasio_dls").removeClass();
                        $('#acid_rasio').text(parseFloat(result.data[0].ACID_TEST_RASIO_MTD_A).toFixed(2) + "%");
                        $('#acid_rasio_ly').text(parseFloat(result.data[0].ACID_TEST_RASIO_MTD_B).toFixed(2) + "%");
                        $('#acid_rasio_dls').text(parseFloat(result.data[0].ACID_TEST_RASIO_MTD_C).toFixed(2) + "%");
                        $("#acid_rasio_ly").addClass(parseFloat(result.data[0].ACID_TEST_RASIO_MTD_A) < parseFloat(result.data[0].ACID_TEST_RASIO_MTD_B) ? "text-success" : "text-danger");
                        $("#acid_rasio_dls").addClass(parseFloat(result.data[0].ACID_TEST_RASIO_MTD_A) < parseFloat(result.data[0].ACID_TEST_RASIO_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].DER_INTEREST_BEARING_MTD_A) {
                        $('#der').text("0%");
                    } else {
                        $("#der_ly").removeClass();;
                        $("#der_dls").removeClass();
                        $('#der').text(parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_A).toFixed(2) + "%");
                        $('#der_ly').text(parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_B).toFixed(2) + "%");
                        $('#der_dls').text(parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_C).toFixed(2) + "%");
                        $("#der_ly").addClass(parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_A) < parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_B) ? "text-success" : "text-danger");
                        $("#der_dls").addClass(parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_A) < parseFloat(result.data[0].DER_INTEREST_BEARING_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].DEBT_RASIO_MTD_A) {
                        $('#debt_ratio').text("0.00%");
                    } else {
                        $("#debt_ratio_ly").removeClass();
                        $("#debt_ratio_dls").removeClass();
                        $('#debt_ratio').text(parseFloat(result.data[0].DEBT_RASIO_MTD_A).toFixed(2) + "%");
                        $('#debt_ratio_ly').text(parseFloat(result.data[0].DEBT_RASIO_MTD_B).toFixed(2) + "%");
                        $('#debt_ratio_dls').text(parseFloat(result.data[0].DEBT_RASIO_MTD_C).toFixed(2) + "%");
                        $("#debt_ratio_ly").addClass(parseFloat(result.data[0].DEBT_RASIO_MTD_A) < parseFloat(result.data[0].DEBT_RASIO_MTD_B) ? "text-success" : "text-danger");
                        $("#debt_ratio_dls").addClass(parseFloat(result.data[0].DEBT_RASIO_MTD_A) < parseFloat(result.data[0].DEBT_RASIO_MTD_C) ? "text-success" : "text-danger");
                    }

                    if (!result.data[0].EQUITY_RASIO_MTD_A) {
                        $('#equity_ratio').text("0.00%");
                    } else {
                        $("#equity_ratio_ly").removeClass();
                        $("#equity_ratio_dls").removeClass();
                        $('#equity_ratio').text(parseFloat(result.data[0].EQUITY_RASIO_MTD_A).toFixed(2) + "%");
                        $('#equity_ratio_ly').text(parseFloat(result.data[0].EQUITY_RASIO_MTD_B).toFixed(2) + "%");
                        $('#equity_ratio_dls').text(parseFloat(result.data[0].EQUITY_RASIO_MTD_C).toFixed(2) + "%");
                        $("#equity_ratio_ly").addClass(parseFloat(result.data[0].EQUITY_RASIO_MTD_A) < parseFloat(result.data[0].EQUITY_RASIO_MTD_B) ? "text-success" : "text-danger");
                        $("#equity_ratio_dls").addClass(parseFloat(result.data[0].EQUITY_RASIO_MTD_A) < parseFloat(result.data[0].EQUITY_RASIO_MTD_C) ? "text-success" : "text-danger");
                    }

                }

            } else {
                $('#working_capital').text(format_round(0));
                $('#current_ratio').text("0%");
                $('#cash_ratio').text("0%");
                $('#acid_rasio').text("0%");
                $('#der').text("0%");
                $('#debt_ratio').text("0%");
                $('#equity_ratio').text("0%");

            }

            $('#home6').text("");
            $('#profile6').text("");
            $('#div-eku').text("");

            var totalGrowthAset;
            var totalGrowthLiabilitas;
            var totalGrowthAsetM;
            var totalGrowthAsetLancar2019;
            var totalGrowthAsetTidakLancar2019;
            var totalGrowthLiabilitasLancar2019;
            var totalGrowthLiabilitasTidakLancar2019;
            var totalGrowthLiabilitasM;
            var totalEkuitas;
            var amount_aset_lancar = 0;
            var growth_aset_lancar = 0;
            var amount_aset_tidak = 0;
            var growth_aset_tidak = 0;
            var amount_liabilitas_lancar = 0;
            var growth_liabilitas_lancar = 0;
            var amount_liabilitas_tidak = 0;
            var growth_liabilitas_tidak = 0;
            var amount_ekuitas = 0;
            var growth_ekuitas = 0;
            if (period === 'YearToDate') {
                $.each(result.bs, function(key, value) {
                    if ((value.balance_sheet === 'ASET') && (value.jenis === 'LANCAR')) {
                        amount_aset_lancar = parseFloat(amount_aset_lancar) + Math.ceil(value.amount_ytd1 ? value.amount_ytd1 : 0);
                        growth_aset_lancar = parseFloat(growth_aset_lancar) + Math.ceil(value.growth ? value.growth : 0);
                    } else if ((value.balance_sheet === 'ASET') && (value.jenis === 'TIDAK LANCAR')) {
                        amount_aset_tidak = parseFloat(amount_aset_tidak) + Math.ceil(value.amount_ytd1 ? value.amount_ytd1 : 0);
                        growth_aset_tidak = parseFloat(growth_aset_tidak) + Math.ceil(value.growth ? value.growth : 0);
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'LANCAR')) {
                        amount_liabilitas_lancar = parseFloat(amount_liabilitas_lancar) + Math.ceil(value.amount_ytd1 ? value.amount_ytd1 : 0);
                        growth_liabilitas_lancar = parseFloat(growth_liabilitas_lancar) + Math.ceil(value.growth ? value.growth : 0);
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'TIDAK LANCAR')) {
                        amount_liabilitas_tidak = parseFloat(amount_liabilitas_tidak) + Math.ceil(value.amount_ytd1 ? value.amount_ytd1 : 0);
                        growth_liabilitas_tidak = parseFloat(growth_liabilitas_tidak) + Math.ceil(value.growth ? value.growth : 0);
                    } else if (value.balance_sheet === 'EKUITAS') {
                        amount_ekuitas = parseFloat(amount_ekuitas) + Math.ceil(value.amount_ytd1 ? value.amount_ytd1 : 0);
                        growth_ekuitas = parseFloat(growth_ekuitas) + Math.ceil(value.growth ? value.growth : 0);
                    }

                });

                var i = 0;
                totalGrowthAset = parseFloat(((parseFloat(result.total[0].amount_ytd1) +
                    parseFloat(result.total[1].amount_ytd1)) - (parseFloat(result.total[0].amount_ytd_last) +
                    parseFloat(result.total[1].amount_ytd_last))) * 100 / (parseFloat(result.total[0].amount_ytd_last) +
                    parseFloat(result.total[1].amount_ytd_last))).toFixed(2);

                totalGrowthAset = isFinite(totalGrowthAset) ? totalGrowthAset : '0';

                totalGrowthLiabilitas = parseFloat(((parseFloat(result.total[3].amount_ytd1) +
                    parseFloat(result.total[4].amount_ytd1)) - (parseFloat(result.total[3].amount_ytd_last) +
                    parseFloat(result.total[4].amount_ytd_last))) * 100 / (parseFloat(result.total[3].amount_ytd_last) +
                    parseFloat(result.total[4].amount_ytd_last))).toFixed(2);

                totalGrowthLiabilitas = isFinite(totalGrowthLiabilitas) ? totalGrowthLiabilitas : '0';

                $('#home6').append("<div class='clearfix'></div>");
                $('#home6').append("<div id='div1' class='row'><h2>ASET</h2>");
                $('#div1').append("<div id='div2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset lancar</th><th style='text-align: center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" + parseFloat(result.total[0].growth).toFixed(2) + " % </h2></td></tr></table></div>");
                //if (adj === 'adjusted') {
                $('#div1').append("<div id='div3' class='col-md-6'><table id='table1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                // } else {
                //      $('#div1').append("<div id='div3' class='col-md-6'><table id='table1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                //  }


                $('#home6').append("<div class='clearfix'></div>");
                /*<h2>ASET</h2>*/
                $('#home6').append("<div id='div_lia1' class='row'><h2>&nbsp;</h2>");
                $('#div_lia1').append("<div id='div_lia2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset tidak lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" + parseFloat(result.total[1].growth).toFixed(2) + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div_lia1').append("<div id='div_lia3' class='col-md-6'><table id='table_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                $('#home6').append("<div id='div_lastrow' class='row'>");
                $('#div_lastrow').append("<div id='div_lia2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Total Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_tidak + amount_aset_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" +
                    totalGrowthAset +
                    " %</h2></td></tr></table></div>");
                //  } else {
                //      $('#div_lia1').append("<div id='div_lia3' class='col-md-6'><table id='table_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                //  }


                $('#profile6').append("<div id='div_tl1' class='row'><h2>LIABILITAS</h2>");
                $('#div_tl1').append("<div id='div_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" + parseFloat(result.total[3].growth).toFixed(2) + " % </h2></td></tr></table></div>");
                //  if (adj === 'adjusted') {
                $('#div_tl1').append("<div id='div_tl3' class='col-md-6'><table id='table_lia1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                //} else {
                //    $('#div_tl1').append("<div id='div_tl3' class='col-md-6'><table id='table_lia1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                //}

                /*LIABILITAS*/
                $('#profile6').append("<div id='div_lia_tl1' class='row'><h2>&nbsp;</h2>");
                $('#div_lia_tl1').append("<div id='div_lia_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas tidak lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" + parseFloat(result.total[4].growth).toFixed(2) + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div_lia_tl1').append("<div id='div_lia_tl3' class='col-md-6'><table id='table_lia_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                //} else {
                //    $('#div_lia_tl1').append("<div id='div_lia_tl3' class='col-md-6'><table id='table_lia_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                // }

                $('#profile6').append("<div id='div_lastrow2' class='row'>");
                $('#div_lastrow2').append("<div id='div_last' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Total Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_lancar + amount_liabilitas_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" +
                    totalGrowthLiabilitas + " %</h2></td></tr></table></div>");

                $('#profile6').append("<div id='div_eku_1' class='row'><h2>EKUITAS</h2>");
                $('#div_eku_1').append("<div id='div_eku_2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Ekuitas</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_ekuitas) + "</h2></td><td style='background:#FDEDDD;'><h2>" + parseFloat(result.total[2].growth).toFixed(2) + " % </h2></td></tr></table></div>");
                //if (adj === 'adjusted') {
                $('#div_eku_1').append("<div id='div_eku_3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                // } else {
                //   $('#div_eku_1').append("<div id='div_eku_3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                //}


                $.each(result.bs, function(key, value) {

                    if ((value.balance_sheet === 'ASET') && (value.jenis === 'LANCAR')) {


                        $('#table1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount_ytd1 ? numberWithCommas(value.amount_ytd1) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");


                    } else if ((value.balance_sheet === 'ASET') && (value.jenis === 'TIDAK LANCAR')) {

                        $('#table_tl1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount_ytd1 ? numberWithCommas(value.amount_ytd1) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    }
                    if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'LANCAR')) {


                        $('#table_lia1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount_ytd1 ? numberWithCommas(value.amount_ytd1) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'TIDAK LANCAR')) {


                        $('#table_lia_tl1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount_ytd1 ? numberWithCommas(value.amount_ytd1) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    } else if ((value.balance_sheet === 'EKUITAS') && (value.jenis === 'LANCAR')) {


                        $('#table_eku_1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount_ytd1 ? numberWithCommas(value.amount_ytd1) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    }

                });
            } else {
                var amount_aset_2019_lancar = 0
                var amount_aset_2018_lancar = 0
                var amount_aset_2019_tidak = 0
                var amount_aset_2018_tidak = 0
                var amount_liabilitas_2019_lancar = 0
                var amount_liabilitas_2018_lancar = 0
                var amount_liabilitas_2019_tidak = 0
                var amount_liabilitas_2018_tidak = 0
                var amount_ekuitas_2019 = 0
                var amount_ekuitas_2018 = 0
                $.each(result.bs, function(key, value) {
                    if ((value.balance_sheet === 'ASET') && (value.jenis === 'LANCAR')) {
                        amount_aset_lancar = parseFloat(amount_aset_lancar) + Math.ceil(value.amount ? value.amount : 0);
                        growth_aset_lancar = parseFloat(growth_aset_lancar) + Math.ceil(value.growth ? value.growth : 0);
                        amount_aset_2019_lancar = Math.ceil(value.amount) + parseFloat(amount_aset_2019_lancar);
                        amount_aset_2018_lancar = Math.ceil(value.amount_2018) + parseFloat(amount_aset_2018_lancar);
                    } else if ((value.balance_sheet === 'ASET') && (value.jenis === 'TIDAK LANCAR')) {
                        amount_aset_tidak = parseFloat(amount_aset_tidak) + Math.ceil(value.amount ? value.amount : 0);
                        growth_aset_tidak = parseFloat(growth_aset_tidak) + Math.ceil(value.growth ? value.growth : 0);
                        amount_aset_2019_tidak = Math.ceil(value.amount) + parseFloat(amount_aset_2019_tidak);
                        amount_aset_2018_tidak = Math.ceil(value.amount_2018) + parseFloat(amount_aset_2018_tidak);
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'LANCAR')) {
                        amount_liabilitas_lancar = parseFloat(amount_liabilitas_lancar) + Math.ceil(value.amount ? value.amount : 0);
                        growth_liabilitas_lancar = parseFloat(growth_liabilitas_lancar) + Math.ceil(value.growth ? value.growth : 0);
                        amount_liabilitas_2019_lancar = Math.ceil(value.amount) + parseFloat(amount_liabilitas_2019_lancar);
                        amount_liabilitas_2018_lancar = Math.ceil(value.amount_2018) + parseFloat(amount_liabilitas_2018_lancar);
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'TIDAK LANCAR')) {
                        amount_liabilitas_tidak = parseFloat(amount_liabilitas_tidak) + Math.ceil(value.amount ? value.amount : 0)
                        growth_liabilitas_tidak = parseFloat(growth_liabilitas_tidak) + Math.ceil(value.growth ? value.growth : 0);
                        amount_liabilitas_2019_tidak = Math.ceil(value.amount) + parseFloat(amount_liabilitas_2019_tidak);
                        amount_liabilitas_2018_tidak = Math.ceil(value.amount_2018) + parseFloat(amount_liabilitas_2018_tidak);
                    } else if (value.balance_sheet === 'EKUITAS') {
                        amount_ekuitas = parseFloat(amount_ekuitas) + Math.ceil(value.amount ? value.amount : 0)
                        growth_ekuitas = parseFloat(growth_ekuitas) + Math.ceil(value.growth ? value.growth : 0);
                        amount_ekuitas_2019 = Math.ceil(value.amount) + parseFloat(amount_ekuitas_2019);
                        amount_ekuitas_2018 = Math.ceil(value.amount_2018) + parseFloat(amount_ekuitas_2018);
                    }

                });

                var i = 0;
                totalGrowthAsetLancar2019 = ((amount_aset_2019_lancar - amount_aset_2018_lancar) / amount_aset_2018_lancar * 100).toFixed(2);
                totalGrowthAsetTidakLancar2019 = ((amount_aset_2019_tidak - amount_aset_2018_tidak) / amount_aset_2018_tidak * 100).toFixed(2);
                totalGrowthAsetM = (((amount_aset_2019_lancar + amount_aset_2019_tidak) - (amount_aset_2018_lancar + amount_aset_2018_tidak)) / (amount_aset_2018_lancar + amount_aset_2018_tidak) * 100).toFixed(2);

                // totalGrowthAsetM = parseFloat(((parseFloat(result.total[0].amount)+ 
                // parseFloat(result.total[1].amount))-(parseFloat(result.total[0].amount_last) +
                // parseFloat(result.total[1].amount_last)))*100/(parseFloat(result.total[0].amount_last) +
                // parseFloat(result.total[1].amount_last))).toFixed(2);

                totalGrowthLiabilitasLancar2019 = ((amount_liabilitas_2019_lancar - amount_liabilitas_2018_lancar) / amount_liabilitas_2018_lancar * 100).toFixed(2);
                totalGrowthLiabilitasTidakLancar2019 = ((amount_liabilitas_2019_tidak - amount_liabilitas_2018_tidak) / amount_liabilitas_2018_tidak * 100).toFixed(2);
                totalGrowthLiabilitasM = (((amount_liabilitas_2019_lancar + amount_liabilitas_2019_tidak) - (amount_liabilitas_2018_lancar + amount_liabilitas_2018_tidak)) / (amount_liabilitas_2018_lancar + amount_liabilitas_2018_tidak) * 100).toFixed(2);


                // totalGrowthLiabilitasM = ((amount_liabilitas_2019 - amount_liabilitas_2018) / amount_liabilitas_2018 * 100).toFixed(2);
                // totalGrowthLiabilitasM = parseFloat(((parseFloat(result.total[3].amount)+ 
                // parseFloat(result.total[4].amount))-(parseFloat(result.total[3].amount_last) +
                // parseFloat(result.total[4].amount_last)))*100/(parseFloat(result.total[3].amount_last) +
                // parseFloat(result.total[4].amount_last))).toFixed(2);
                totalEkuitas = ((amount_ekuitas_2019 - amount_ekuitas_2018) / amount_ekuitas_2018 * 100).toFixed(2);
                $('#home6').append("<div class='clearfix'></div>");
                $('#home6').append("<div id='div1' class='row'><h2>ASET</h2>");
                $('#div1').append("<div id='div2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset lancar</th><th style='text-align: center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" + totalGrowthAsetLancar2019 + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div1').append("<div id='div3' class='col-md-6'><table id='table1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                // } else {
                //     $('#div1').append("<div id='div3' class='col-md-6'><table id='table1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th></th><th>Growth</th></tr>");
                // }


                $('#home6').append("<div class='clearfix'></div>");
                /*<h2>ASET</h2>*/
                $('#home6').append("<div id='div_lia1' class='row'><h2>&nbsp;</h2>");

                $('#div_lia1').append("<div id=div_lia2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset tidak lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" + totalGrowthAsetTidakLancar2019 + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div_lia1').append("<div id=div_lia3' class='col-md-6'><table id='table_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                $('#home6').append("<div id='div_lastrow' class='row'>");
                $('#div_lastrow').append("<div id='div_lia2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total aset</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Total Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_aset_tidak + amount_aset_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" +
                    totalGrowthAsetM +
                    " %</h2></td></tr></table></div>");
                // } else {
                //    $('#div_lia1').append("<div id=div_lia3' class='col-md-6'><table id='table_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th><th>Growth</th></tr>");
                // }


                $('#profile6').append("<div id='div_tl1' class='row'><h2>LIABILITAS</h2>");
                $('#div_tl1').append("<div id='div_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_lancar) + "</h2></td><td style='background:#FDEDDD;'><h2>" + totalGrowthLiabilitasLancar2019 + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div_tl1').append("<div id='div_tl3' class='col-md-6'><table id='table_lia1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                //} else {
                //    $('#div_tl1').append("<div id='div_tl3' class='col-md-6'><table id='table_lia1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th><th>Growth</th></tr>");
                // }

                /*LIABILITAS*/
                $('#profile6').append("<div id='div_lia_tl1' class='row'><h2>&nbsp;</h2>");
                $('#div_lia_tl1').append("<div id='div_lia_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas tidak lancar</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" + totalGrowthLiabilitasTidakLancar2019 + " % </h2></td></tr></table></div>");
                // if (adj === 'adjusted') {
                $('#div_lia_tl1').append("<div id='div_lia_tl3' class='col-md-6'><table id='table_lia_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                //} else {
                //    $('#div_lia_tl1').append("<div id='div_lia_tl3' class='col-md-6'><table id='table_lia_tl1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th><th>Growth</th></tr>");
                // }

                $('#profile6').append("<div id='div_lastrow2' class='row'>");
                $('#div_lastrow2').append("<div id='div_last' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total liabilitas</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Total Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_liabilitas_lancar + amount_liabilitas_tidak) + "</h2></td><td style='background:#FDEDDD;'><h2>" +
                    totalGrowthLiabilitasM +
                    " %</h2></td></tr></table></div>");


                $('#profile6').append("<div id='div_eku_1' class='row'><h2>EKUITAS</h2>");
                $('#div_eku_1').append("<div id='div_eku_2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Ekuitas</th><th style='text-align:center;background:#ffffff; color: #E00000;'>Growth</th></tr><tr><td style='background:#FDEDDD;'><h2>" + numberWithCommas(amount_ekuitas) + "</h2></td><td style='background:#FDEDDD;'><h2>" + totalEkuitas + " % </h2></td></tr></table></div>");
                //if (adj === 'adjusted') {
                $('#div_eku_1').append("<div id='div_eku_3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)</th><th style='text-align: right'>Growth</th></tr>");
                //} else {
                //    $('#div_eku_1').append("<div id='div_eku_3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th style='text-align: right;'>Amount(IDR)<i class='last_update'>*not yet adjusted</i></th><th>Growth</th></tr>");
                // }


                $.each(result.bs, function(key, value) {

                    if ((value.balance_sheet === 'ASET') && (value.jenis === 'LANCAR')) {


                        $('#table1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount ? numberWithCommas(value.amount) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");


                    } else if ((value.balance_sheet === 'ASET') && (value.jenis === 'TIDAK LANCAR')) {

                        $('#table_tl1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount ? numberWithCommas(value.amount) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    }
                    if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'LANCAR')) {


                        $('#table_lia1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount ? numberWithCommas(value.amount) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    } else if ((value.balance_sheet === 'LIABILITAS') && (value.jenis === 'TIDAK LANCAR')) {


                        $('#table_lia_tl1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount ? numberWithCommas(value.amount) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    } else if ((value.balance_sheet === 'EKUITAS') && (value.jenis === 'LANCAR')) {


                        $('#table_eku_1').append("<tr><td>" + value.faktor + "</td><td align='right'>" + (value.amount ? numberWithCommas(value.amount) : 0) + "</td><td align='right'>" + (value.growth ? parseFloat(value.growth).toFixed(2) : 0) + " % </td></tr>");
                    }

                });

            }


        }

    }

    function numberWithCommas(x) {
        // console.debug("test"+x);

        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }


    function format_round(rp) {

        if (rp < 1 && rp > 0) {
            return (Math.round(rp * 100)) + '%';

        } else if (Math.round(rp).toString().length >= 10) {
            //  console.debug((rp/Math.pow(10,9))+'M');
            return "Rp " + numberWithCommas(Math.round(rp / Math.pow(10, 9))) + 'M';
        } else {
            return ("Rp " + numberWithCommas(rp));
        }
    }
</script>