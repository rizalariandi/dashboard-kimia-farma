<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>
<style>
th {
    background-color: #4E9DD5;
    color: white;
    text-align: center;
	border: 1px solid ;
	font-weight:700;
} 

td {
    color: #4A5675;
} 


</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-6">
		<div class="col-md-12">
			<div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Capex Apotek & Klinik</h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
				
				<a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
			</div>
		</div>
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table">
				  <thead class="thead-dark">
					<tr >
					  <th rowspan="3" scope="col" style="vertical-align : middle;text-align:center;">Uraian</th>
					  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">AP 2018</th>
					  <th colspan="2" scope='colgroup'>Real s.d 31 Des 2108</th>		
					  <th scope='col'>Selisih</th>					  
					</tr>
					<tr>
					  <th scope="col" style="vertical-align : middle;text-align:center;">Rupiah</th>
					  <th scope="col" style="vertical-align : middle;text-align:center;">%(b/a)</th>
					  <th scope='col' scope="col" style="vertical-align : middle;text-align:center;">(a-b)</th>
					</tr>
					<tr>
					  <th rowspan="1" scope='col'>a</th>
					  <th scope='col'>b</th>
					  <th scope='col'>c</th>
					  <th scope='col'>d</th>
					</tr>
				  </thead>
				  <tbody>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>Apotek Baru</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Apotek Sewa</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Apotek KSO</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Apotek Bandara</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Apotek Mall</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Apotek New Concept</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#C4C4C4; font-weight:700">
					  <td style="font-style:italic">Sub Total</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>Perpani, Kontrak, Reformat</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Perpanjangan</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Reformat</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Relokasi</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">OSHCS/Flagship</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">CO Perpanjangan</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#C4C4C4; font-weight:700">
					  <td style="font-style:italic">Sub Total</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>Klinik Kecantikan</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Klinik Naava Green</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>Counter</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Counter Alkes Center</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Counter Beauty Center</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#C4C4C4; font-weight:700">
					  <td style="font-style:italic">Sub Total</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>Pembelian Aset</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Pembelian Aset</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
					<tr style="background-color:#C4C4C4; font-weight:700">
					  <td>TOTAL</th>
					  <td>50 T</td>
					  <td>10 T</td>
					  <td>80%</td>
					  <td>40 T</td>
					</tr>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
			<div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title"></h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
				
				<a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
			</div>
		</div>
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table">
				  <thead class="thead-dark">
					<tr >
					  <th colspan="6" scope='colgroup'>Realisasi dan Estimasi Capex</th>			  
					</tr>
					<tr>
					  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Uraian</th>
					  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">AP 2018</th>
					  <th colspan="2" scope='colgroup'>IP 2018</th>
					  <th colspan="2" scope='colgroup'>Real SD Update</th>
					</tr>
					<tr>
					  <th scope='col'>(Rp.)</th>
					  <th scope='col'>(%)</th>
					  <th scope='col'>(Rp.)</th>
					  <th scope='col'>(%)</th>
					</tr>
				  </thead>
				  <tbody>					
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>A. Klinik Baru</th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Gabung Apotek</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Mandiri KSO</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Mandiri Sewa</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Hemodealisa</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
					<tr style="background-color:#EAF4FE; font-weight:700">
					  <td>B. Reformat </th>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr>
					  <td style="border-left:white 50px solid">Reformat Klinik</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
					<tr style="background-color:#C4C4C4; font-weight:700">
					  <td>TOTAL</th>
					  <td>50 T</td>
					  <td>50 T</td>
					  <td>50%</td>
					  <td>50 T</td>
					  <td>50%</td>
					</tr>
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
