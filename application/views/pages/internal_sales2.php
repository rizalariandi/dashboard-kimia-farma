<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>
<style>
th {
    background-color: #4E9DD5;
    color: white;
    text-align: center;
	border: 1px solid ;
	font-weight:700;
} 

td {
    color: #4A5675;
	text-align: center;
} 


</style>
<div class="row" style="background:white;padding: 10px;">
	<div class="col-lg-12">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
</div>

<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Penjualan PRB 2018 IDR xxx M',
		align: 'left'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '',	
		showInLegend: false, 
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
		color: '#4286F5'
		

    }],
	
	credits: {
    enabled: false
  }

});
</script>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Evaluasi Quick Count (Top 5)</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Unit</th>
                  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Strata BM</th>
                  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">AP 2018</th>
                  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Prognosa</th>
				  <th colspan="2" scope='colgroup'>Achievement</th>
				  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Ach. Prognosa</th>
				  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">2017</th>
				  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Growth</th>
                </tr>
				<tr>
                  <th scope="col">Value</th>
                  <th scope="col">%</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Manado</th>
                  <td>A</td>
                  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                </tr>
                <tr>
                  <td>Surabaya</th>
                  <td>A</td>
                  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                </tr>
                <tr>
                  <td>Denpasar</th>
                  <td>A</td>
                  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                </tr>
				<tr>
                  <td>Yogyakarta</th>
                  <td>A</td>
                  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                </tr>
				<tr>
                  <td>Bandung</th>
                  <td>A</td>
                  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                  <td>xxxx</td>
				  <td>xxxx</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>


<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-7">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Kinerja KFA per Layanan Entitas</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-7">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th rowspan="2" scope="col" style="vertical-align : middle;text-align:center;">Layanan</th>
                  <th colspan="3" scope='colgroup'>Tunai</th>
                  <th colspan="3" scope='colgroup'>Kredit</th>
				  <th colspan="3" scope='colgroup'>Total</th>
                </tr>
				<tr>
                  <th scope="col">R2018</th>
                  <th scope="col">R2017</th>
				  <th scope="col">Growth</th>
				  <th scope="col">R2018</th>
                  <th scope="col">R2017</th>
				  <th scope="col">Growth</th>
				  <th scope="col">R2018</th>
                  <th scope="col">R2017</th>
				  <th scope="col">Growth</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="text-align:left;">Apotek</th>
                  <td>xxx</td>
                  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
				  <td>xxx</td>
                </tr>
                <tr>
                  <td style="text-align:left;">Klinik</th>
                  <td >xxx</td>
                  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
				  <td>xxx</td>
                </tr>
                <tr>
                  <td style="text-align:left;">Optik</th>
                  <td>xxx</td>
                  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
				  <td>xxx</td>
                </tr>
				<tr>
                  <td style="text-align:left;">Lab Klinik</th>
                  <td>xxx</td>
                  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
				  <td>xxx</td>
                </tr>
				<tr style="font-weight:700">
                  <td style="text-align:left; color: blue;">KONSOLIDASI</th>
                  <td>xxx</td>
                  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
                  <td>xxx</td>
				  <td>xxx</td>
				  <td>xxx</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
	
	<div class="col-lg-5">
		<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	
	
<script>

// Build the chart
Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
	legend: {
			align: 'right',
			verticalAlign: 'top',
			layout: 'vertical',
			x: 0,
			y: 100
		},
    title: {
        text: 'Kontribusi Omzet Per Entitas Tahun 2018'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
		
            showInLegend: true
			
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Optik',
            y: 40,
            sliced: true,
            selected: true
        }, {
            name: 'Klinik',
            y: 20
        }, {
            name: 'Lab Klinik',
            y: 20
        }, {
            name: 'Apotek',
            y: 20
        }]
    }]
});
</script>
	
</div>


