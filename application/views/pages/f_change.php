<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Change Password</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .last_update {
        font-size: 12px;

        color: #cb3935;

        font-family: Roboto;
        font-style: italic;
    }

    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    .m-0 {
        margin: 0;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">

        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">

            <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                <div class="form-group">

                    <label class="control-label col-sm-2">Current Password <span class="text-danger">*</span>:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control current_password" placeholder="Enter Current Password">
                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">Current password is required</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">New Password <span class="text-danger">*</span>:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control new_password" placeholder="Enter New Password">
                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">New password is required</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Retype Password <span class="text-danger">*</span>:</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control retype_password" placeholder="Enter Retype Password">
                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">Retype password is required</small>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                </div>
            </form>

    </div>
</div>

<script>
    //$('#realisasi-data').DataTable();
    $(".help-block").hide();

    function saveData() {

        if ($('.current_password').val(), $('.new_password').val(), $('.retype_password').val()) {

                if ($('.new_password').val() === $('.retype_password').val()){
                    PostSave($('.current_password').val(), $('.new_password').val(), $('.retype_password').val())
                    $('.current_password').val("");
                    $('.new_password').val("");
                    $('.retype_password').val("");
                }else{
                    swal("Error", "Retype Password is not correct", "error");
                }


        } else {
            $(".help-block").show();
        }
    }

    function addAdjusment() {
        $('#ModalLabel').text('Add User');
        $(".help-block").hide();
        $('.post').text('Save');
        $('.nama').val(undefined);
        $('.username').val(undefined);
        $('.email ').val(undefined);
        $(".username").prop("disabled", false);
        $(".email").prop("disabled", false);
        // $('.password').val(undefined);
        $('.hp').val(undefined);
        $('.unit').val(undefined);
        $('.level').val(undefined);
        $('.jabatan').val(undefined);
        $('.nik').val(undefined);
        $('.status').bootstrapToggle('on')
    }

    function updateUser(nama, username, email, hp, unit, level, jabatan, nik, active) {
        $('#ModalLabel').text('Update User')
        $('.post').text('Update');
        $('.nama').val(nama);
        $('.username').val(username);
        $('.email ').val(email);
        $('.email').prop("disabled", true);
        $('.username').prop("disabled", true);
        // $('.password').val(undefined);
        $('.hp').val(hp);
        $('.unit').val(unit);
        $('.level').val(level);
        $('.jabatan').val(jabatan);
        $('.nik').val(nik);
        $('.status').bootstrapToggle(active == 1 ? 'on' : 'off')
    }

    function deleteUser(email) {
        swal({
            title: "Are you sure?",
            text: "Are you sure to delete!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/User_maintenance/DelUser') ?>",
                        method: 'post',
                        data: {
                            'email': email,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenter').modal('hide');
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {
                        loadDataTable();
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostSave(current_password, new_password, retype_password) {
        $.ajax({
            url: "<?php echo base_url('index.php/User_maintenance/change_password') ?>",
            method: 'post',
            data: {
                'current_password': current_password,
                'new_password': new_password,
                'retype_password': retype_password
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                console.log(response.status);
                if (response.status === 'success') {

                    swal("Succeeded", 'Saved successfully (You will be logout in 4 seconds)', "success");
                    var timer = setTimeout(function() {
                        window.location='<?= base_url() ?>index.php/page/logout'
                    }, 4000);

                } else {
                    swal('Error', "Wrong Password", "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {

        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function PostUpdate(nama, username, email, password, hp, unit, level, jabatan, nik, status) {
        $.ajax({
            url: "<?php echo base_url('index.php/User_maintenance/updateUser') ?>",
            method: 'post',
            data: {
                'nama': nama,
                'username': username,
                'email': email,
                'password': password,
                'hp': hp,
                'unit': unit,
                'level': level,
                'jabatan': jabatan,
                'nik': nik,
                'active': status ? 1 : 0
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                swal("Succeeded", 'Update successfully', "success");
                $('#ModalCenter').modal('hide');

            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {
            loadDataTable();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>