<div class="col-lg-12 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">

        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
            foreach ($_SESSION['role_entitas'] as $e) {

                echo "<option value='$e'>$e</option>";
            } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                if ($i == $_SESSION['year']) {
                    echo "<option value='$i' selected>$i</option>";
                } else {
                    echo "<option value='$i'>$i</option>";
                }
            } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                if ($i == $_SESSION['month']) {
                    echo "<option value='$i' selected>$i</option>";
                } else {
                    echo "<option value='$i'>$i</option>";
                }
            } ?></select>

        <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success' />
    </form>
</div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box"> <label>Pendidikan</label></label><div id="chart1"></div> </div>
    </div>
    <div class="col-md-6">
        <div id="div1" class="white-box">
            <table id='table1' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>
                <tr>
                    <th style='background: #4E9ED5;color: #FFFFFF;'>Pendidikan</th>
                    <th style='background: #4E9ED5;color: #FFFFFF;alignment: right'>Jumlah</th>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box"><label>Usia</label> <div id="chart2"></div> </div>
    </div>
    <div class="col-md-6">
        <div id="div2" class="white-box">
            <table id='table2' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>
                <tr>
                    <th style='background: #4E9ED5;color: #FFFFFF;'>Usia</th>
                    <th style='background: #4E9ED5;color: #FFFFFF;alignment: right'>Jumlah</th>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box"> <label>Jabatan</label><div id="chart3"></div> </div>
    </div>
    <div class="col-md-6">
        <div id="div3" class="white-box">
            <table id='table3' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>
                <tr>
                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jabatan</th>
                    <th style='background: #4E9ED5;color: #FFFFFF;alignment: right'>Jumlah</th>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box"><label>Masa Kerja</label> <div id="chart4"></div> </div>
    </div>
    <div class="col-md-6">
        <div id="div4" class="white-box">
            <table id='table4' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>
                <tr>
                    <th style='background: #4E9ED5;color: #FFFFFF;'>Masa Kerja (Year)</th>
                    <th style='background: #4E9ED5;color: #FFFFFF;alignment: right'>Jumlah</th>
                </tr>
            </table>
        </div>
    </div>
</div>


<script>

    var myChart1 = Highcharts.chart('chart1', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Plant Jakarta', 'Plant Bandung', 'Plant Watudakon', 'Plant Semarang', 'Plant Medan'],
            crosshair : true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Finish Good Value',
            data: [5, 3, 4, 7, 2]
        }]
    });

    var myChart2 = Highcharts.chart('chart2', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Plant Jakarta', 'Plant Bandung', 'Plant Watudakon', 'Plant Semarang', 'Plant Medan'],
            crosshair : true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Finish Good Value',
            data: [5, 3, 4, 7, 2]
        }]
    });

    var myChart3 = Highcharts.chart('chart3', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Plant Jakarta', 'Plant Bandung', 'Plant Watudakon', 'Plant Semarang', 'Plant Medan'],
            crosshair : true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Finish Good Value',
            data: [5, 3, 4, 7, 2]
        }]
    });

    var myChart4 = Highcharts.chart('chart4', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Plant Jakarta', 'Plant Bandung', 'Plant Watudakon', 'Plant Semarang', 'Plant Medan'],
            crosshair : true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Finish Good Value',
            data: [5, 3, 4, 7, 2]
        }]
    });
    // Build the chart

    $(document).ready(function() {

        generateDashboard();
    });

    $("#btnSubmit").click(function(e) {

        e.preventDefault();


        generateDashboard();


    });

    function generateDashboard() {
        var urlpost = "<?php echo base_url('index.php/Learning_growth/refresh_demografi') ?>";

        var year = $('#year').val();
        var month = $('#month').val();
        var entitas = $('#entitas').val();

        $.post(
            urlpost, {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas)
            },
            AjaxSucceeded, "json"

        ).fail(function(response) {
            swal(response.status.toString(), response.statusText, "error");
        });
    }

    function AjaxSucceeded(result) {
        //Pendidikan
        for (var j = 0; j <= myChart1.series.length; j++) {
            myChart1.series[0].remove();
        }

        myChart1.xAxis[0].setCategories([], false);

        var label1 = [];
        var data_pendidikan = [];

        $('#div1').empty();
        $('#div1').append("<table id='table1' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>\n" +
            "                <tr>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Pendidikan</th>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jumlah</th>\n" +
            "                </tr>\n" +
            "            </table>");
        $.each(result.sum_pendidikan, function(key, value) {
            label1.push(value.pendidikan);
            data_pendidikan.push(parseInt(value.vol));


            $('#table1').append("<tr><td>" + value.pendidikan + "</td><td align='right'>" + (value.vol ? numberWithCommas(value.vol) : 0) + "</td></tr>");


        })

        myChart1.addSeries({
            name: 'Pendidikan',
            type: 'column',
            data: data_pendidikan,
            animation: {
                duration: 2000
            },
            color: '#F79868'
        }, true);

        myChart1.xAxis[0].setCategories(label1, true);

        //Usia

        for (var j = 0; j <= myChart2.series.length; j++) {
            myChart2.series[0].remove();
        }

        myChart2.xAxis[0].setCategories([], false);

        var label1 = [];
        var data_usia = [];
        $('#div2').empty();
        $('#div2').append("<table id='table2' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>\n" +
            "                <tr>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Usia</th>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jumlah</th>\n" +
            "                </tr>\n" +
            "            </table>");

        $.each(result.sum_usia, function(key, value) {
            label1.push(value.usia);
            data_usia.push(parseInt(value.vol));
            $('#table2').append("<tr><td>" + value.usia + "</td><td align='right'>" + (value.vol ? numberWithCommas(value.vol) : 0) + "</td></tr>");
        })

        myChart2.addSeries({
            name: 'Usia',
            type: 'column',
            data: data_usia,
            animation: {
                duration: 2000
            },
            color: '#5178CE'
        }, true);

        myChart2.xAxis[0].setCategories(label1, true);

        //Jabatan

        for (var j = 0; j <= myChart3.series.length; j++) {
            myChart3.series[0].remove();
        }

        myChart3.xAxis[0].setCategories([], false);

        var label1 = [];
        var data_jabatan = [];
        $('#div3').empty();
        $('#div3').append("<table id='table3' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>\n" +
            "                <tr>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jabatan</th>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jumlah</th>\n" +
            "                </tr>\n" +
            "            </table>");

        $.each(result.sum_jabatan, function(key, value) {
            label1.push(value.level);
            data_jabatan.push(parseInt(value.vol));
            $('#table3').append("<tr><td>" + value.level + "</td><td align='right'>" + (value.vol ? numberWithCommas(value.vol) : 0) + "</td></tr>");
        })

        myChart3.addSeries({
            name: 'Jabatan',
            type: 'column',
            data: data_jabatan,
            animation: {
                duration: 2000
            },
            color: '#F79868'
        }, true);

        myChart3.xAxis[0].setCategories(label1, true);

        //Masa Kerja

        for (var j = 0; j <= myChart4.series.length; j++) {
            myChart4.series[0].remove();
        }

        myChart4.xAxis[0].setCategories([], false);

        var label1 = [];
        var data_masakerja = [];
        $('#div4').empty();
        $('#div4').append("<table id='table4' class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'>\n" +
            "                <tr>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Masa Kerja (Year)</th>\n" +
            "                    <th style='background: #4E9ED5;color: #FFFFFF;'>Jumlah</th>\n" +
            "                </tr>\n" +
            "            </table>");

        $.each(result.sum_masakerja, function(key, value) {
            label1.push(value.masa_kerja);
            data_masakerja.push(parseInt(value.vol));
            $('#table4').append("<tr><td>" + value.masa_kerja + "</td><td align='right'>" + (value.vol ? numberWithCommas(value.vol) : 0) + "</td></tr>");
        })

        myChart4.addSeries({
            name: 'Masa Kerja',
            type: 'column',
            data: data_masakerja,
            animation: {
                duration: 2000
            },
            color: '#5178CE'
        }, true);

        myChart4.xAxis[0].setCategories(label1, true);
    }

    function numberWithCommas(x) {
        // console.debug("test"+x);

        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    }


</script>