<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">List Level</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .last_update {
        font-size: 12px;

        color: #cb3935;

        font-family: Roboto;
        font-style: italic;
    }

    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    .m-0 {
        margin: 0;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addAdjusment()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Level</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">
        <div class="table-responsive">
            <table id="adjusment-data" class="table table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Level</th>
                    <th scope="col">Menu</th>
                    <th scope="col">action</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Level</h3>
                <h5 class="last_update">Note: * Mandatory Field</h5>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                    <div class="form-group">

                        <label class="control-label col-sm-2">Nama Level <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control nama" placeholder="Enter Nama" pattern="^[A-Za-z][A-Za-z0-9]*$" title="No Space character">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Nama is required</small>
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="control-label col-sm-2">Menu Level 1<span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select id="level1" name="multiselect[]" multiple="multiple">

                                <?php
                                foreach ($level1 as $key => $value)
                                {
                                    echo '<option value="'.$value['level'].'">'.$value['name'].'</option>';
                                }
                                ?>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Level is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Menu Level 2<span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select id="level2" name="multiselect[]" multiple="multiple">

                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Level is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Menu Level 3<span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select id="level3" name="multiselect[]" multiple="multiple">


                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Level is required</small>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    //$('#realisasi-data').DataTable();
    var Id
    $(".help-block").hide();
    var data_table;
    $(document).ready(function() {

        $('#level1').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true,
            onChange: getValueCheckbox1,
            onSelectAll: getValueCheckbox1,
            onSelectedAll: getValueCheckbox1,
            onDeselectAll: getValueCheckbox1
        });


        $('#level1').multiselect('updateButtonText');

        $('#level2').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true,
            onChange: getValueCheckbox2,
            onSelectAll: getValueCheckbox2,
            onSelectedAll: getValueCheckbox2,
            onDeselectAll: getValueCheckbox2
        });

        $('#level2').multiselect('updateButtonText');

        $('#level3').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true,
            onChange: getValueCheckbox,
            onSelectAll: getValueCheckbox,
            onSelectedAll: getValueCheckbox,
            onDeselectAll: getValueCheckbox
        });


        $('#level3').multiselect('updateButtonText');

        data_table = $('#adjusment-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,

            "columns": [{
                data: 'Id',
                render: function(data, type, row) {
                    return (i = i + 1);
                }
            },
                {
                    data: 'level',
                    title: 'Nama Level',
                    className: 'text-left'
                },
                {
                    data: 'menu',
                    title: 'Menu',
                    className: 'text-left',
                },
                {
                    "mRender": function(data, type, row) {
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateUser(' +
                            "'" + (row.level ? row.level : '') + "'" + ',' +
                            "'" + row.menu + "'" + ',' +
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteUser(' + "'" + row.level + "'" + ')" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });
        loadDataTable()
    });


    function getValueCheckbox1(){

        var urlpost = "<?php echo base_url('index.php/Menu_management/getMenuLevel2') ?>";
        var param = {
            menu: []
        };
        $('#level1 :selected').each(function() {
            param.menu.push($(this).val());
        });

        console.debug(param);
        console.debug(urlpost);

        // $("#sum").html(sum);
        $.post(
            urlpost,
            {
                'level1': JSON.stringify(param.menu)
            },
            AjaxSucceededOption1,"json"

        );
      //  getValueCheckbox2();
    }

    function getValueCheckbox2(){

        var urlpost = "<?php echo base_url('index.php/Menu_management/getMenuLevel3') ?>";
        var param = {
            menu: []
        };
        $('#level2 :selected').each(function() {
            param.menu.push($(this).val());
        });

        console.debug(param);
        console.debug(urlpost);
        console.debug("masuk value checkbox 2");

        // $("#sum").html(sum);
        $.post(
            urlpost,
            {
                'level2': JSON.stringify(param.menu)
            },
            AjaxSucceededOption2,"json"

        );

    }
    function getValueCheckbox(){


    }

    function AjaxSucceededOption1(result) {
        console.debug(result);
        $('#level2')
            .find('option')
            .remove()
            .end();


        var parent = result.data[0].parent_level;
        var i=0;
        var j=0;
        $('optgroup').remove();
        $('#level2')
            .append($("<optgroup></optgroup>")

                .attr("label",result.data[0].parent_level)
                .attr("id","group"+j));

        var first_group = result.data[0].parent_level;
        $.each(result.data, function( key, value ) {


                if (first_group !== value.parent_level){
                    first_group = value.parent_level;
                    j++;
                    $('#level2')
                        .append($("<optgroup></optgroup>")

                            .attr("label",value.parent_level)
                            .attr("id","group"+j));

                    $('#group'+j)
                        .append($("<option></option>")

                            .attr("value",value.lv)
                            .text(value.name));
                }else{
                    $('#group'+j)
                        .append($("<option></option>")

                            .attr("value",value.lv)
                            .text(value.name));

                }

            $('#level2').multiselect('rebuild');


            $('#level2').multiselect('updateButtonText');

            //$('#kota').append('<option value="'+value+'">'+value+'</option>');
        })
    }

    function AjaxSucceededOptionUpdate(result) {

        $('#level1')
            .find('option')
            .remove()
            .end();
        $('#level2')
            .find('option')
            .remove()
            .end();
        $('#level2')
            .find('optgroup')
            .remove()
            .end();

        $('#level3')
            .find('option')
            .remove()
            .end();
        $('#level3')
            .find('optgroup')
            .remove()
            .end();

        console.debug(result.level1);
        var selected = false;


        $.each(result.level1, function( key, value ) {
            $.each(result.level1_update, function( key1, value1 ) {

                if (value.level === value1.level){
                    selected = true;
                }

            })
            if (selected){
                $('#level1')
                    .append($("<option></option>")
                        .attr("value",value.level).attr("selected",true)
                        .text(value.name));
                selected = false;
            }else{
                $('#level1')
                    .append($("<option></option>")
                        .attr("value",value.level)
                        .text(value.name));
            }


            $('#level1').multiselect('rebuild');


            $('#level1').multiselect('updateButtonText');

        })


        var parent_level2 = result.level2[0].parent_level;
        var j=0;
        $('#level2')
            .append($("<optgroup></optgroup>")
                .attr("label",result.level2[0].parent_level).attr("id","groupupdate2"+j));
        $.each(result.level2, function( key, value ) {
            $.each(result.level2_update, function( key1, value1 ) {

                if (value.lv === value1.level){
                    selected = true;
                }

            })

            if (parent_level2 !== value.parent_level){
                parent_level2 = value.parent_level;
                j++;
                $('#level2')
                    .append($("<optgroup></optgroup>")
                        .attr("label",value.parent_level).attr("id","groupupdate2"+j));

                if (selected){

                    $('#groupupdate2'+j)
                        .append($("<option></option>")
                            .attr("value",value.lv).attr("selected",true)
                            .text(value.name));


                    selected = false;
                }else{


                    $('#groupupdate2'+j)
                        .append($("<option></option>")
                            .attr("value",value.lv)
                            .text(value.name));


                }
            }else{
                if (selected){

                    $('#groupupdate2'+j)
                        .append($("<option></option>")
                            .attr("value",value.lv).attr("selected",true)
                            .text(value.name));


                    selected = false;
                }else{


                    $('#groupupdate2'+j)
                        .append($("<option></option>")
                            .attr("value",value.lv)
                            .text(value.name));


                }
            }




            $('#level2').multiselect('rebuild');


            $('#level2').multiselect('updateButtonText');

        })


        var parent_level3=result.level3[0].parent_level;

        var i=0;



        $('#level3')
            .append($("<optgroup></optgroup>")
                .attr("label",result.level3[0].parent_level).attr("id","groupupdate3"+i));
        $.each(result.level3, function( key, value ) {
            $.each(result.level3_update, function( key1, value1 ) {

                if (value.lv === value1.level){
                    selected = true;
                }

            })

            if (parent_level3 !== value.parent_level){
                parent_level3 = value.parent_level;
                i++;
                $('#level3')
                    .append($("<optgroup></optgroup>")
                        .attr("label",value.parent_level).attr("id","groupupdate3"+i));


                if (selected){
                    $('#groupupdate3'+i)
                        .append($("<option></option>")
                            .attr("value",value.lv).attr("selected",true)
                            .text(value.name));
                    selected = false;
                }else{
                    $('#groupupdate3'+i)
                        .append($("<option></option>")
                            .attr("value",value.lv)
                            .text(value.name));
                }
            }else{
                if (selected){
                    $('#groupupdate3'+i)
                        .append($("<option></option>")
                            .attr("value",value.lv).attr("selected",true)
                            .text(value.name));
                    selected = false;
                }else{
                    $('#groupupdate3'+i)
                        .append($("<option></option>")
                            .attr("value",value.lv)
                            .text(value.name));
                }
            }




            $('#level3').multiselect('rebuild');


            $('#level3').multiselect('updateButtonText');

        })
    }


    function AjaxSucceededOption2(result) {
        console.debug(result);
        $('#level3')
            .find('option')
            .remove()
            .end();

        $('#level3')
            .find('optgroup')
            .remove()
            .end();
        //$('optgroup').remove();
        var parent_group = "";
        var j = 0;
        console.debug("success option lv 2");
        if (result.data.length !== 0){


            $('#level3')
                .append($("<optgroup></optgroup>")
                    .attr("label",result.data[0].parent_level)
                    .attr("id","group3"+j));
            parent_group = result.data[0].parent_level;
        }


        $.each(result.data, function( key, value ) {

            if (parent_group !== value.parent_level){
                parent_group = value.parent_level;
                j++;
                $('#level3')
                    .append($("<optgroup></optgroup>")
                        .attr("label",value.parent_level)
                        .attr("id","group3"+j));

                $('#group3'+j)
                    .append($("<option></option>")
                        .attr("value",value.lv)
                        .text(value.name));
            }else{
                $('#group3'+j)
                    .append($("<option></option>")
                        .attr("value",value.lv)
                        .text(value.name));
            }


            $('#level3').multiselect('rebuild');


            $('#level3').multiselect('updateButtonText');
            //$('#kota').append('<option value="'+value+'">'+value+'</option>');
        })


    }
    function loadDataTable(entitas, year, month) {
        var length;
        var newRow = [];
        var iterasi = 0;
        i = 0;
        $.ajax({
            url: "<?php echo base_url('index.php/Menu_management/getMenuLevel') ?>",
            method: 'get',
            data: {
                'year': year,
                'entitas': entitas,
                'month': month,
            },
            async: false,
            beforeSend: function() {
                $('button[type="submit"]').attr('disabled', true);
            },
            complete: function() {
                $('button[type="submit"]').attr('disabled', false);
            },
            success: function(data) {
                data = JSON.parse(data)
                length = data.length;
                newRow = data
            }
        }).done(function() {
            if (length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        });
    }

    function saveData() {

        var param = {
            level: []
        };
        $('#level1 :selected').each(function() {
            param.level.push($(this).val());
        });
        $('#level2 :selected').each(function() {
            param.level.push($(this).val());
        });
        $('#level3 :selected').each(function() {
            param.level.push($(this).val());
        });

        if ($('.nama').val()) {
            if ($('.post').text() == 'Save') {

                    PostSave($('.nama').val(), param)

            } else {
                PostUpdate($('.nama').val(), param)
            }
        } else {
            $(".help-block").show();
        }
    }

    function addAdjusment() {
        $('#ModalLabel').text('Add Level');
        $(".help-block").hide();
        $('.post').text('Save');
        $('.nama').val(undefined);


    }

    function updateUser(nama) {
        $('#ModalLabel').text('Update Level')
        $('.post').text('Update');
        $('.nama').val(nama);
        var urlpost = "<?php echo base_url('index.php/Menu_management/getUpdateMenuLevel') ?>";

        $.post(
            urlpost,
            {
                'nama': nama
            },
            AjaxSucceededOptionUpdate,"json"

        );


    }

    function deleteUser(level) {
        swal({
            title: "Are you sure?",
            text: "Are you sure to delete!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/Menu_management/DelLevel') ?>",
                        method: 'post',
                        data: {
                            'level': level,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenter').modal('hide');
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {
                        loadDataTable();
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostSave(nama, level) {
        $.ajax({
            url: "<?php echo base_url('index.php/Menu_management/add_level') ?>",
            method: 'post',
            data: {
                'nama': nama,
                'level': JSON.stringify(level)
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {
                    console.log('we call it');
                    swal("Succeeded", 'Saved successfully', "success");
                    $('#ModalCenter').modal('hide');
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {
            loadDataTable();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function PostUpdate(nama,level) {
        $.ajax({
            url: "<?php echo base_url('index.php/Menu_management/updateLevel') ?>",
            method: 'post',
            data: {
                'nama': nama,
                'level': JSON.stringify(level)
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                swal("Succeeded", 'Update successfully', "success");
                $('#ModalCenter').modal('hide');

            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {
            loadDataTable();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function filterAdjusment(entitas, year, month) {
        loadDataTable(entitas, year, month)
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>