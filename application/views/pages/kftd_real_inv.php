<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Value : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
        Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
            if($i==$_SESSION['year']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
            if($i==$_SESSION['month']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Week : <select name="week"><?php for($i=1;$i<=4;$i++){
            if($i==$_SESSION['week']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Day : <select name="day"><?php for($i=1;$i<=31;$i++){
            if($i==$_SESSION['day']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?>
    </select>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
th {
  background-color: #093890;
  color: white;
  text-align: center;
} 

td {
  color: #4A5675;
  text-align: center;
} 
.dataTables_wrapper .dt-buttons {
  float:right;
}

.btn-table {
background-color: #08388F !important;
text-decoration: none;
}

.btn-table:hover {
color: #fff !important;
text-decoration: none;
}

</style>
<div class="row" style="background:white;padding: 10px;">

  <div class="col-md-12">
    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Realisasi Investasi</h4>
    </div>
  </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
      <table id="realisasi-data" class="table table table-striped">
        <thead class="thead-dark">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Uraian</th>
            <th scope="col">Anggaran</th>
            <th scope="col">Realisasi</th>
            <th scope="col">%Realisasi</th>
          </tr>
        </thead>

      </table>
    </div>
  </div>
</div>


<script>
  //$('#realisasi-data').DataTable();

  var year = $('#year').val();
  var month = $('#month').val();
  var i = 0;
  var entitas = $('#entitas').val();
  var data_table = $('#realisasi-data').DataTable({
    "scrollY": "600px",
    "scrollCollapse": true,
    "scrollX": false,
    "ordering": true,
    "bDestroy": true,
    "serverSide": true,
    "bFilter": false,
    "bLengthChange": true,
    "processing": true,

    "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
         "dom"         : 'Blfrtip',
       
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
    pageLength: 25,

    "ajax": {
      "url": "<?php echo base_url('index.php/page/realisasi_investasi_data') ?>",
      "method" : "POST",
      "data": {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'entitas': JSON.stringify(entitas)

      }
    },"columns": [

    { 
      data: 0,
      render: function(data, type, row){


        return (i = i +1);
      }
    },
    { 
      data: 1
    },
    { 
      data: 2,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    },
    { 
      data: 3,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    },
    { 
      data: 4,
      render: function(data, type, row){


        return (0+"%");
      }
    }

    ]
  });


  $("#btnSubmit").click(function (e) {

    e.preventDefault();

    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();
    var data_table1 = $('#realisasi-data').DataTable({
      "scrollY": "600px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": true,
      "processing": true,
       "dom"         : 'Blfrtip',
       
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],

      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/realisasi_investasi_data') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)

        }
      },"columns": [

     { 
      data: 0,
      render: function(data, type, row){


        return (i = i +1);
      }
    },
    { 
      data: 1
    },
    { 
      data: 2,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    },
    { 
      data: 3,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
      }
    },
    { 
      data: 4,
      render: function(data, type, row){
        var percent = Math.round(row[2]/row[3]) * 100;

        return ("0%");
      }
    }

      ]
    });

  })
function numberWithCommas(x) {
     if (x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
        return 0;
    }
}

</script>