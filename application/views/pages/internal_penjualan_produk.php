<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>

<!--<div id="proporsi_penjualan_product_principal" style="height: 400px; min-width: 380px"></div>-->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>

<style>
th {
    background-color: #4E9DD5;
    color: white;
    text-align: center;
	font-weight:700
} 

td {
    color: blue;
    text-align: center;
	font-weight:700
} 
</style>

<div class="row" style="background:white;padding: 10px;">
	<div class="col-lg-8">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	
	<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
	legend: {
			align: 'left',
			verticalAlign: 'top',
			layout: 'horizontal',
			x: 100,
			y: 0
		},
    title: {
        text: 'Penjualan PRB 2018 IDR xxx M',
		align: 'left'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Net Sales 2018',	
		showInLegend: true, 
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
		color: '#083890'	

    },{
        name: 'Net Sales 2017',	
		showInLegend: true, 
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
		color: '#F9841A'	

    }],
	
	credits: {
    enabled: false
  }

});
</script>


	<div class="col-lg-4">
		<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	
	
<script>

// Build the chart
Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
	legend: {
			align: 'right',
			verticalAlign: 'top',
			layout: 'vertical',
			x: 0,
			y: 100
		},
    title: {
        text: 'Net Sales : Rp. xxxxx B',
		style: {
         color: 'blue',
         font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
      },
		align: 'left',
		verticalAlign: 'bottom',
		layout: 'horizontal'
	},
	credits: {
    enabled: false
  },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
		
            showInLegend: true
			
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'KF',
            y: 40,
			color: '#4980FF',
            sliced: true,
            selected: true
        }, {
            name: 'Non KF',
            y: 40,
			color: '#24CBB7'
        }, {
            name: 'Alkes',
            y: 20,
			color: '#F9CB20'
        }]
    }]
});
</script>


</div>





<div class="row" style="background:white;padding: 10px;">
	<div class="col-lg-12">
		<div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
</div>

<script>

Highcharts.chart('container3', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Tren Penjualan per Lini',
		align: 'left'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Lini 1',
        data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
    }, {
        name: 'Lini 2',
        data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
    }, {
        name: 'Lini 3',
        data: [5.9,	6.2,	7.7,	10.5,	13.9,	17.2,	19,	18.6,	16.2,	12.3,	8.6,	6.8]
    }, {
        name: 'Lini 4',
        data: [2.9,	3.2,	4.7,	7.5,	10.9,	14.2,	16,	15.6,	13.2,	9.3,	5.6,	3.8]
    }]
});
</script>







<div class="row" style="background:white;padding: 10px;">

    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Lini per Principal</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
			
        </div>
    </div>
	
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;">Principal A</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#92CAED;text-align: center;color: white; font-weight:700">
                <div class="col-md-4">
                    <div class="col-md-12" >
                        Net Sales
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        SKU
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        Kontribusi
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                <div class="col-md-4">
                    <div class="col-md-12">
                        Rp.xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
        
        
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;"> Principal B</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#F79868;text-align: center;color: white; font-weight:700">
                <div class="col-md-4">
                    <div class="col-md-12">
                        Net Sales
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        SKU
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        Kontribusi
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                 <div class="col-md-4">
                    <div class="col-md-12">
                        Rp.xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;"> Principal C</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#A7DCC8;text-align: center;color: white; font-weight:700">
                <div class="col-md-4">
                    <div class="col-md-12">
                        Net Sales
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        SKU
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        Kontribusi
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                 <div class="col-md-4">
                    <div class="col-md-12">
                        Rp.xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>

<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Lini per Material</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Nama Barang</th>
                  <th scope="col">Qty</th>
                  <th scope="col">Net Sales</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td >Material A</th>
                  <td>1</td>
                  <td>Rp. xxx</td>
                </tr>
                <tr>
                  <td>Material B</th>
                  <td>1</td>
                  <td>Rp. xxx</td>
                </tr>
                <tr>
                  <td>Material C</th>
                  <td>1</td>
                  <td>Rp. xxx</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>

