<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>


<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;">KF</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#92CAED;text-align: center;color: white;">
                <div class="col-md-3">
                    <div class="col-md-12">
                        2018
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        2017
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        Growth
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        Ach.
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
        
        
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;"> Non KF</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#EF6460;text-align: center;color: white;">
                <div class="col-md-3">
                    <div class="col-md-12">
                        2018
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        2017
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        Growth
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        Ach.
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title" style="text-align: center;"> SKU Transaksi</h4>
        </div>
        <div class="col-md-12">
            <div class="col-md-12" style="background:#FC8313;text-align: center;color: white;">
                <div class="col-md-4">
                    <div class="col-md-12">
                        2018
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        2017
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        Growth
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background:#F2F2F2;text-align: center;vertical-align: middle;height: 40px">
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        xxx%
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-6">
        <div class="row" style="background:white;padding: 10px;">
    
            <div class="col-md-12">
                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
                    <h6 class="page-title">Proporsi Penjualan Product Prinsipal A</h6>
                </div>
                <div class="col-lg-3 text-right">
                    <form>
                        <div class='group-control'>
                            <select class='form-control'>
                                <option value='All'>Per Principal</option>
                                <option value='KF Console'>Kimia Farma Holding 2</option>
                                <option value='KFHO'>Kimia Farma Holding 3</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 text-right">
                    <form>
                        <div class='group-control'> 
                            <select class='form-control'>
                                <option value='All'>Yearly</option>
                                <option value='KF Console'>2202</option>
                                <option value='KFHO'>2203</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row" style="background:white;padding: 10px;">
            <div id="proporsi_penjualan_product_principal" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
        
    </div>
    <div class="col-md-3">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h6 class="page-title">Proporsi Penjualan per Lini</h6>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <div id="proporsi_penjualan_perlini" style="min-width: 100px; height: 400px; max-width: 200px; margin: 0 auto"></div>
        </div>  
    </div>
    <div class="col-md-3">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h6 class="page-title">Proporsi Penjualan Prinsipal per Channel</h6>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <div id="proporsi_penjualan_principal_perchannel" style="min-width: 100px; height: 400px; max-width: 200px; margin: 0 auto"></div>
        </div> 
        
    </div>
    
    
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title pull-left">Real Sales 2018</h4>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title pull-right"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                            <th rowspan="2" scope="col">Principal</th>
                            <th rowspan="2" scope="col">Prognosa</th>
                            <th colspan="2" scope="col">Achievement</th>
                            <th rowspan="2" scope="col">Ach. Prognosa</th>
                            <th rowspan="2" scope="col">2017</th>
                            <th rowspan="2" scope="col">Growth</th>
                        </tr>
                        <tr>
                            <th scope="col">Net Sales</th>
                            <th scope="col">%</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Principal A</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Principal B</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Principal C</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Principal D</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Principal E</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td style="color:blue">Total Sales</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                      </tbody>
                    </table>
		</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title pull-left">E-Katalog 2018</h4>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title pull-right"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                            <th rowspan="2" scope="col">Channel</th>
                            <th colspan="2" scope="col">E-Katalog</th>
                            <th colspan="2" scope="col">Non E-Katalog</th>
                        </tr>
                        <tr>
                            <th scope="col">Value</th>
                            <th scope="col">Growth</th>
                            <th scope="col">Value</th>
                            <th scope="col">Growth</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Institusi Pusat</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Institusi Pusat</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Institusi Pusat</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                        <tr>
                          <td>Institusi Pusat</td>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                          <td>xxx</th>
                        </tr>
                      </tbody>
                    </table>
		</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h4 class="page-title pull-left">SKU Transaksi</h4>
        </div>
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title pull-right"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                    <table class="table table table-striped">
                      <thead class="thead-dark">
                        <tr>
                            <th scope="col">Principal</th>
                            <th scope="col">2018</th>
                            <th scope="col">2017</th>
                            <th scope="col">Growth</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Principal A</td>
                          <td>5</th>
                          <td>4</th>
                          <td>xxx%</th>
                        </tr>
                        <tr>
                          <td>Principal B</td>
                          <td>5</th>
                          <td>4</th>
                          <td>xxx%</th>
                        </tr>
                        <tr>
                          <td>Principal C</td>
                          <td>5</th>
                          <td>4</th>
                          <td>xxx%</th>
                        </tr>
                        <tr>
                          <td>Principal D</td>
                          <td>5</th>
                          <td>4</th>
                          <td>xxx%</th>
                        </tr>
                        <tr>
                          <td>Principal E</td>
                          <td>5</th>
                          <td>4</th>
                          <td>xxx%</th>
                        </tr>
                      </tbody>
                    </table>
		</div>
        </div>
    </div>
</div>

<script>

// Build the chart
Highcharts.chart('proporsi_penjualan_perlini', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Plants',
        colorByPoint: true,
        data: [{
            name: 'Plant Bandung',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Plant Jakarta',
            y: 11.84
        }, {
            name: 'Plant Semarang',
            y: 10.85
        }]
    }]
});
// Build the chart
Highcharts.chart('proporsi_penjualan_principal_perchannel', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Plants',
        colorByPoint: true,
        data: [{
            name: 'Plant Bandung',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Plant Jakarta',
            y: 11.84
        }, {
            name: 'Plant Semarang',
            y: 10.85
        }]
    }]
});


Highcharts.chart('net_period', {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: [1, 2, 3, 4, 5,6,7,8,9,10,11,12]
    },
    labels: {
        items: [{
            html: null,
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Net Sales',
        data: [3, 2, 1, 3, 4,2, 3, 5, 7, 6, 7, 6]
    }, {
        type: 'column',
        name: 'Real Sales',
        data: [2, 3, 5, 7, 6,3, 2, 1, 3, 4,2, 3]
    }, {
        type: 'spline',
        name: 'Average',
        data: [3, 2.67, 3, 6.33, 3.33,3, 2, 1, 3, 4,2, 3],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'spline',
        name: 'Credit Memo (KFTD)',
        data: [3, 2.67, 3, 6.33, 3.33,3, 2, 1, 3, 4,2, 3],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
});


</script>
<script>
        Highcharts.setOptions({
            colors: ['#F2DCC5', '#000000']
        });
        Highcharts.chart('index_stock', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'linear'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                   
                }
            },

            series: [{
                type: 'area',
                name: 'Days',
                data: [[0.0, 225],
                    [0.1, 226],
                    [0.2, 228],
                    [0.2, 228],
                    [0.3, 228],
                    [0.4, 229],
                    [0.5, 229],
                    [0.5, 229],
                    [0.6, 230]]
            }]
        });
</script>
<script>
        Highcharts.setOptions({
            colors: ['#FFBEBC', '#000000']
        });
        Highcharts.chart('proporsi_penjualan_product_principal', {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'linear'
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                   
                }
            },

            series: [{
                type: 'area',
                name: 'Days',
                data: [[0.0, 225],
                    [0.1, 226],
                    [0.2, 228],
                    [0.2, 228],
                    [0.3, 228],
                    [0.4, 229],
                    [0.5, 229],
                    [0.5, 229],
                    [0.6, 230]]
            }]
        });
</script>
