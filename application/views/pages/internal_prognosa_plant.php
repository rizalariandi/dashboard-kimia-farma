<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>




<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Plant</th>
                  <th scope="col">Value Total Stock</th>
                  <th scope="col">Prognosa Penyerahan Plant in Value</th>
                  <th scope="col">Prognosa Pesanan Plant in Value</th>
                  <th scope="col">%Value Penyerahan vs Value Pesanan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Plant Bandung</td>
                  <td>0 IDR</td>
                  <td>-3,234,234,123 IDR</td>
                  <td>-2,234,234,123 IDR</td>
                  <td>80%</td>
                </tr>
                <tr>
                  <td>Plant Jakarta</td>
                  <td>0 IDR</td>
                  <td>-3,234,234,123 IDR</td>
                  <td>-2,234,234,123 IDR</td>
                  <td>80%</td>
                </tr>
                <tr>
                  <td>Plant Semarang</td>
                  <td>0 IDR</td>
                  <td>-3,234,234,123 IDR</td>
                  <td>-2,234,234,123 IDR</td>
                  <td>80%</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-6">
        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Plant</th>
                  <th scope="col">Value Barang</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
                <tr>
                  <td>Plant Bandung</td>
                  <td>-3,234,234,123 IDR</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
<script>

// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Plants',
        colorByPoint: true,
        data: [{
            name: 'Plant Bandung',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Plant Jakarta',
            y: 11.84
        }, {
            name: 'Plant Semarang',
            y: 10.85
        }]
    }]
});
</script>