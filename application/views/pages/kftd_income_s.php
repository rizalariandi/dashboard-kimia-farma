<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>
    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Week : <select name="week"><?php for($i=1;$i<=4;$i++){
      if($i==$_SESSION['week']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Day : <select name="day"><?php for($i=1;$i<=31;$i++){
      if($i==$_SESSION['day']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?>
  </select>
  <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>

<?php 
$key =['Net Sales','COGS','Gross Profit','Operating Expenses','Net Operataing Income','Other Income','Net Income'];
function format_round($rp){
  if($rp<1 && $rp>0){
    return (round($rp*100)).'%';
  }elseif(strlen(round($rp))>10){
    return number_format(($rp/pow(10,9)),0,',','.').'M';
  }else{
    return number_format($rp,0,',','.');
  }
}
function format_chart($rp){
  if($rp<1 && $rp>0){
    return (round($rp*100));
  }elseif(strlen(round($rp))>10){
    return round(($rp/pow(10,9)));
  }else{
    return round($rp);
  }
}
$j=1;

foreach($is as $v){
  if($j==1 || $j==4 || $j==7){
    echo "<div class='row'>";
  }

  ?>

  <div class="col-md-4">
   <div class="white-box">
    <div class='row'>
      <div class='col-md-8'>
        <h3 id="income_statement<?=$j?>" class="box-title m-b-0" style='font-size: 16px;font-weight: 400;color: #E74040;'><?=$v['income_statement']?></h3>
        <h2 id="realisasi<?=$j?>" class='real animated tada'><?=format_round($v['realisasi'])?></h2>
        <table class='table-condensed'>
          <tr>
            <td>Target</td>

            <td>Growth</td>
           
          </tr>
          <tr>
            <td id="target<?=$j?>"><?=format_round($v['target'])?></td>
            <td id="growth<?=$j?>" class='text-<?=($v['growth']>0)?'success':'danger'?>'><i class='fas fa-caret-<?=($v['growth']>0)?'up':'down'?> fa-lg'></i><?=$v['growth']?>%</td>
            
          </tr>
        </table>
      </div>
      <div id="div-pie<?=$j?>" class='col-md-4'>
        <div id="pie<?=$j?>" class="chart" style='margin-top:0px' data-percent="0" data-scale-color="#ffb400">
          <span class="percent"></span>
        </div>
      </div>
      <div class='col-md-12'>
        <div id='chart<?=$j?>' style='height:100px'></div>
      </div>
    </div>
  </div>
</div>
<?php 
if($j % 3==0){
  echo "</div>";
}
$j++;
}
?>

<div class="row">
  <div class="col-md-4">

    <i class="fa fa-circle-o" style="color: #cb3935; font-size: 1.5em;"></i><b> &#60; 80%</b><br />
    <i class="fa fa-circle-o" style="color: #f0ad4e; font-size: 1.5em;"></i><b> 80% &#45; 95%</b><br />
    <i class="fa fa-circle-o" style="color: #5cb85c; font-size: 1.5em;"></i><b> &#62; 90%</b><br />

  </div>
</div>

<script>

 var easyChart1 =  $('#pie1').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: false,
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart2 =  $('#pie2').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart3 =  $('#pie3').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart4 =  $('#pie4').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart5 =  $('#pie5').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart6 =  $('#pie6').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});

 var easyChart7 =  $('#pie7').easyPieChart({
  easing: 'easeOutBounce',
  scaleColor: '#E15656',
  scaleLength: 0,
  lineWidth: 8,
  size: 109,
  trackColor: '#cb3935',
  barColor : function (percent) {
   return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
 },
 onStep: function(from, to, percent) {
  $(this.el).find('.percent').text(Math.round(percent));
}
});


 <?php
 //foreach($key as $i=>$k){
 for($j=1;$j<=7;$j++){
       // if(array_key_exists($k,$data_target)){

  ?>

  var myChart<?=$j?> = Highcharts.chart('chart<?=$j?>', {

    title: {
      text: ''
    },
    legend: {
      enable : false,
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: 150,
      y: 100,
      floating: true,
      itemStyle : {"font-size": "10px","font-weight": "100"},
      borderWidth: 1
        //backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      xAxis: {
        categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        minorTickLength: 0,
        tickLength: 0,
        labels: {
          rotation: 90
        }
      },
      tooltip: {
        shared: true,
        valueSuffix: ''
      },
      yAxis: {
        title: {
          text: ''
        },
        visible: false
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Target',
        type: 'spline',
        data: [0,0,0,0,0,0,0,0,0,0,0,0],
        animation: {
          duration: 2000
            // Uses Math.easeOutBounce
           // easing: 'easeOutBounce'
         },
         color: '#DC6B89'
       },{
        name: 'Real',
        type: 'areaspline',
        data: [0,0],
        animation: {
          duration: 2000
            // Uses Math.easeOutBounce
            //easing: 'easeOutBounce'
          },
          color: '#6CD0B7'
        }
        ],
        exporting: { enabled: false }
      });



<?php } ?>


$(document).ready(function() {


  var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";

  var year = $('#year').val();
  var month = $('#month').val();

  var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
      urlpost,
      {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'entitas': JSON.stringify(entitas)



      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {


        //target initiation
        $.each(result.is_sum_income, function( key, value ) {
          if (value.income_statement == 'NET SALES'){
           $('#income_statement1').text(value.income_statement);
           $('#realisasi1').text(format_round(value.sum_realisasi));
           $('#target1').text(format_round(value.sum_target));

           var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
           var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

           var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
           var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
           var growth=0;
           $('#growth1').text("");
         
          
           if (result.is_net_operating_lastmonth.length > 0){
           
            var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

            if (isNaN(percent_growth)){
              growth = 0;
            }else{
              growth = percent_growth;
            }

          }else{
            growth = 0;
          }
          growth = isFinite(growth) ? growth : 0;
          
          if (parseInt(growth) > 0){
            $('#growth1').prepend(txt2);
            $('#growth1').append(growth.toFixed(0)+"%");
            $("#growth1").attr("class", 'text-success');


        
          }else{
            $('#growth1').prepend(txt1);
            $('#growth1').append(growth.toFixed(0)+"%");
            $("#growth1").attr("class", 'text-danger');

          }

          $("#pie1").attr("data-percent", value.sum_achievement);
          var achievement = (value.sum_realisasi / value.sum_target)*100;
          var achiev;
          if (isNaN(achievement)){
            achiev = 0;
          }else{
            achiev = achievement;
          }

          achiev = isFinite(achiev) ? achiev : 0;
          console.debug("aciev "+achiev);
          $('#div-pie1').empty();
          $('#div-pie1').append("<div id='pie1' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
          if (achiev < 80){

            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#cb3935',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }else if (achiev >= 80 && achiev <= 95){
            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#f0ad4e',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }else if (achiev > 95){
            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#5cb85c',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }







        }else if (value.income_statement == 'COGS'){
         $('#income_statement2').text(value.income_statement);
         $('#realisasi2').text(format_round(value.sum_realisasi));
         $('#target2').text(format_round(value.sum_target));
         $('#prognosa2').text(format_round(value.sum_prognosa));
         var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
         var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

         var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
         var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
         var growth=0;
         $('#growth2').text("");
       
          if (result.is_net_operating_lastmonth.length > 0){
         
          var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

          if (isNaN(percent_growth)){
            growth = 0;
          }else{
            growth = percent_growth;
          }

        }else{
          growth = 0;
        }
        growth = isFinite(growth) ? growth : 0;
        console.debug("growth "+growth.toFixed(0));
        if (parseInt(growth) > 0){
          $('#growth2').prepend(txt2);
          $('#growth2').append(growth.toFixed(0)+"%");
          $("#growth2").attr("class", 'text-success');


        }else{
          $('#growth2').prepend(txt1);
          $('#growth2').append(growth.toFixed(0)+"%");
          $("#growth2").attr("class", 'text-danger');

      
        }
        $('#pie2').attr("data-percent", value.sum_achievement);
        var achievement = (value.sum_realisasi / value.sum_target)*100;
        var achiev;
        if (isNaN(achievement)){
          achiev = 0;
        }else{
          achiev = achievement;
        }

        achiev = isFinite(achiev) ? achiev : 0;

        $('#div-pie2').empty();
        $('#div-pie2').append("<div id='pie2' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
        if (achiev < 80){

          var easyChart2 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#cb3935',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }else if (achiev >= 80 && achiev <= 95){
          var easyChart2 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#f0ad4e',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }else if (achiev > 95){
          var easyChart1 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#5cb85c',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }

      }else if (value.income_statement == 'GROSS PROFIT'){
       $('#income_statement3').text(value.income_statement);
       $('#realisasi3').text(format_round(value.sum_realisasi));
       $('#target3').text(format_round(value.sum_target));
       $('#prognosa3').text(format_round(value.sum_prognosa));
       var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
       var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

       var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
       var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
       var growth=0;
       $('#growth3').text("");
      
       if (result.is_net_operating_lastmonth.length > 0){
        var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

        if (isNaN(percent_growth)){
          growth = 0;
        }else{
          growth = percent_growth;
        }

      }else{
        growth = 0;
      }
      growth = isFinite(growth) ? growth : 0;
      console.debug("growth "+growth.toFixed(0));
      if (parseInt(growth) > 0){
        $('#growth3').prepend(txt2);
        $('#growth3').append(growth.toFixed(0)+"%");
        $("#growth3").attr("class", 'text-success');


   
      }else{
        $('#growth3').prepend(txt1);
        $('#growth3').append(growth.toFixed(0)+"%");
        $("#growth3").attr("class", 'text-danger');

      
      }
      $('#pie3').attr("data-percent", value.sum_achievement);
      var achievement = (value.sum_realisasi / value.sum_target)*100;
      var achiev;
      if (isNaN(achievement)){
        achiev = 0;
      }else{
        achiev = achievement;
      }

      achiev = isFinite(achiev) ? achiev : 0;
      $('#div-pie3').empty();
      $('#div-pie3').append("<div id='pie3' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
      if (achiev < 80){

        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#cb3935',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }else if (achiev >= 80 && achiev <= 95){
        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#f0ad4e',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }else if (achiev > 95){
        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#5cb85c',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }

    }else if (value.income_statement == 'OPERATING EXPENSES'){
     $('#income_statement4').text(value.income_statement);
     $('#realisasi4').text(format_round(value.sum_realisasi));
     $('#target4').text(format_round(value.sum_target));
     $('#prognosa4').text(format_round(value.sum_prognosa));
     var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
     var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

     var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
     var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
     var growth=0;
     $('#growth4').text("");
   
     if (result.is_net_operating_lastmonth.length > 0){
      var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

      if (isNaN(percent_growth)){
        growth = 0;
      }else{
        growth = percent_growth;
      }

    }else{
      growth = 0;
    }
    growth = isFinite(growth) ? growth : 0;
    console.debug("growth "+growth.toFixed(0));
    if (parseInt(growth) > 0){
      $('#growth4').prepend(txt2);
      $('#growth4').append(growth.toFixed(0)+"%");
      $("#growth4").attr("class", 'text-success');


  
    }else{
      $('#growth4').prepend(txt1);
      $('#growth4').append(growth.toFixed(0)+"%");
      $("#growth4").attr("class", 'text-danger');

     
    }
    $('#pie4').attr("data-percent", value.sum_achievement);
    var achievement = (value.sum_realisasi / value.sum_target)*100;
    var achiev;
    if (isNaN(achievement)){
      achiev = 0;
    }else{
      achiev = achievement;
    }

    achiev = isFinite(achiev) ? achiev : 0;
    $('#div-pie4').empty();
    $('#div-pie4').append("<div id='pie4' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
    if (achiev < 80){

      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }else if (achiev >= 80 && achiev <= 95){
      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#f0ad4e',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }else if (achiev > 95){
      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#5cb85c',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }

  }else if (value.income_statement == 'NET OPERATING INCOME'){
   $('#income_statement5').text(value.income_statement);
   $('#realisasi5').text(format_round(value.sum_realisasi));
   $('#target5').text(format_round(value.sum_target));
   $('#prognosa5').text(format_round(value.sum_prognosa));
   var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
   var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

   var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
   var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
   var growth=0;
   $('#growth5').text("");
  
    if (result.is_net_operating_lastmonth.length > 0){
    var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

    if (isNaN(percent_growth)){
      growth = 0;
    }else{
      growth = percent_growth;
    }

  }else{
    growth = 0;
  }
  growth = isFinite(growth) ? growth : 0;
  console.debug("growth "+growth.toFixed(0));
  if (parseInt(growth) > 0){
    $('#growth5').prepend(txt2);
    $('#growth5').append(growth.toFixed(0)+"%");
    $("#growth5").attr("class", 'text-success');


  
  }else{
    $('#growth5').prepend(txt1);
    $('#growth5').append(growth.toFixed(0)+"%");
    $("#growth5").attr("class", 'text-danger');

   
  }
  $('#pie5').attr("data-percent", value.sum_achievement);
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
  }else{
    achiev = achievement;
  }

  achiev = isFinite(achiev) ? achiev : 0;
  $('#div-pie5').empty();
  $('#div-pie5').append("<div id='pie5' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
  if (achiev < 80){

    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#cb3935',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }else if (achiev >= 80 && achiev <= 95){
    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#f0ad4e',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }else if (achiev > 95){
    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#5cb85c',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }

}else if (value.income_statement == 'OTHER INCOME'){
 $('#income_statement6').text(value.income_statement);
 $('#realisasi6').text(format_round(value.sum_realisasi));
 $('#target6').text(format_round(value.sum_target));
 $('#prognosa6').text(format_round(value.sum_prognosa));
 var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

 var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
 var growth=0;
 $('#growth6').text("");
 
  if (result.is_net_operating_lastmonth.length > 0){
  var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

  if (isNaN(percent_growth)){
    growth = 0;
  }else{
    growth = percent_growth;
  }

}else{
  growth = 0;
}
growth = isFinite(growth) ? growth : 0;
console.debug("growth net sales"+growth.toFixed(0));
if (parseInt(growth) > 0){
  $('#growth6').prepend(txt2);
  $('#growth6').append(growth.toFixed(0)+"%");
  $("#growth6").attr("class", 'text-success');


}else{
  $('#growth6').prepend(txt1);
  $('#growth6').append(growth.toFixed(0)+"%");
  $("#growth6").attr("class", 'text-danger');

}
$('#pie6').attr("data-percent", value.sum_achievement);
var achievement = (value.sum_realisasi / value.sum_target)*100;
var achiev;
if (isNaN(achievement)){
  achiev = 0;
}else{
  achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;
$('#div-pie6').empty();
$('#div-pie6').append("<div id='pie6' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
if (achiev < 80){

  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#cb3935',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}else if (achiev >= 80 && achiev <= 95){
  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#f0ad4e',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}else if (achiev > 95){
  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#5cb85c',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}

}else if (value.income_statement == 'NET INCOME'){
 $('#income_statement7').text(value.income_statement);
 $('#realisasi7').text(format_round(value.sum_realisasi));
 $('#target7').text(format_round(value.sum_target));
 $('#prognosa7').text(format_round(value.sum_prognosa));
 var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

 var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
 var growth=0;
 $('#growth7').text("");
 
  if (result.is_net_operating_lastmonth.length > 0){
  var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

  if (isNaN(percent_growth)){
    growth = 0;
  }else{
    growth = percent_growth;
  }

}else{
  growth = 0;
}
growth = isFinite(growth) ? growth : 0;
console.debug("growth "+growth.toFixed(0));
if (parseInt(growth) > 0){
  $('#growth7').prepend(txt2);
  $('#growth7').append(growth.toFixed(0)+"%");
  $("#growth7").attr("class", 'text-success');


}else{
  $('#growth7').prepend(txt1);
  $('#growth7').append(growth.toFixed(0)+"%");
  $("#growth7").attr("class", 'text-danger');

  
}
$('#pie7').attr("data-percent", value.sum_achievement);
var achievement = (value.sum_realisasi / value.sum_target)*100;
var achiev;
if (isNaN(achievement)){
  achiev = 0;
}else{
  achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;
$('#div-pie7').empty();
$('#div-pie7').append("<div id='pie7' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
if (achiev < 80){

  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#cb3935',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}else if (achiev >= 80 && achiev <= 95){
  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#f0ad4e',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}else if (achiev > 95){
  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#5cb85c',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}

}

})







        //chart initiation

        for (var j = 0; j <= myChart1.series.length; j++){              
         myChart1.series[0].remove();
       } 

       myChart1.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart2.series.length; j++){              
         myChart2.series[0].remove();
       } 

       myChart2.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart3.series.length; j++){              
         myChart3.series[0].remove();
       } 

       myChart3.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart4.series.length; j++){              
         myChart4.series[0].remove();
       } 

       myChart4.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart5.series.length; j++){              
         myChart5.series[0].remove();
       } 

       myChart5.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart6.series.length; j++){              
         myChart6.series[0].remove();
       } 

       myChart6.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart7.series.length; j++){              
         myChart7.series[0].remove();
       } 

       myChart7.xAxis[0].setCategories([],false);


       var label1 = [];
       var data_target1 = [];
       var data_real1 = [];

       var label2 = [];
       var data_target2 = [];
       var data_real2 = [];

       var label3 = [];
       var data_target3 = [];
       var data_real3 = [];

       var label4 = [];
       var data_target4 = [];
       var data_real4 = [];

       var label5 = [];
       var data_target5 = [];
       var data_real5 = [];

       var label6 = [];
       var data_target6 = [];
       var data_real6 = [];

       var label7 = [];
       var data_target7 = [];
       var data_real7 = [];


       $.each(result.is_net_sales, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label1.push(month_name(value.month));
          data_target1.push(parseInt(value.target));
          data_real1.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_cogs, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label2.push(month_name(value.month));
          data_target2.push(parseInt(value.target));
          data_real2.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_gross_profit, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label3.push(month_name(value.month));
          data_target3.push(parseInt(value.target));
          data_real3.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_operating_expenses, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label4.push(month_name(value.month));
          data_target4.push(parseInt(value.target));
          data_real4.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_net_operating, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label5.push(month_name(value.month));
          data_target5.push(parseInt(value.target));
          data_real5.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_other_income, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label6.push(month_name(value.month));
          data_target6.push(parseInt(value.target));
          data_real6.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_net_income, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label7.push(month_name(value.month));
          data_target7.push(parseInt(value.target));
          data_real7.push(parseInt(value.realisasi));
        }

      })



       myChart1.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target1,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart1.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real1,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart1.xAxis[0].setCategories(label1,true);

       myChart2.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target2,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart2.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real2,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart2.xAxis[0].setCategories(label1,true);

       myChart3.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target3,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart3.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real3,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart3.xAxis[0].setCategories(label1,true);

       myChart4.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target4,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart4.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real4,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart4.xAxis[0].setCategories(label1,true);

       myChart5.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target5,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart5.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real5,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart5.xAxis[0].setCategories(label1,true);

       myChart6.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target6,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart6.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real6,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart6.xAxis[0].setCategories(label1,true);

       myChart7.addSeries({
         name: 'Target',
         type: 'line',
         data: data_target7,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart7.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real7,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart7.xAxis[0].setCategories(label1,true);




     }

   });  

$("#btnSubmit").click(function (e) {

  e.preventDefault();




  var urlpost = "<?php echo base_url('index.php/page/refresh_income_statement') ?>";

  var year = $('#year').val();
  var month = $('#month').val();

  var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
      urlpost,
      {
        'month': JSON.stringify(month),
        'year': JSON.stringify(year),
        'entitas': JSON.stringify(entitas)



      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {


        //target initiation
        $.each(result.is_sum_income, function( key, value ) {
          if (value.income_statement == 'NET SALES'){
           $('#income_statement1').text(value.income_statement);
           $('#realisasi1').text(format_round(value.sum_realisasi));
           $('#target1').text(format_round(value.sum_target));

           var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
           var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

           var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
           var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
           var growth=0;
           $('#growth1').text("");
       
          
           if (result.is_net_operating_lastmonth.length > 0){
           
            var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

            if (isNaN(percent_growth)){
              growth = 0;
            }else{
              growth = percent_growth;
            }

          }else{
            growth = 0;
          }
          growth = isFinite(growth) ? growth : 0;
          
          if (parseInt(growth) > 0){
            $('#growth1').prepend(txt2);
            $('#growth1').append(growth.toFixed(0)+"%");
            $("#growth1").attr("class", 'text-success');


        
          }else{
            $('#growth1').prepend(txt1);
            $('#growth1').append(growth.toFixed(0)+"%");
            $("#growth1").attr("class", 'text-danger');

           
          }

          $("#pie1").attr("data-percent", value.sum_achievement);
          var achievement = (value.sum_realisasi / value.sum_target)*100;
          var achiev;
          if (isNaN(achievement)){
            achiev = 0;
          }else{
            achiev = achievement;
          }

          achiev = isFinite(achiev) ? achiev : 0;

          $('#div-pie1').empty();
          $('#div-pie1').append("<div id='pie1' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
          if (achiev < 80){

            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#cb3935',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }else if (achiev >= 80 && achiev <= 95){
            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#f0ad4e',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }else if (achiev > 95){
            var easyChart1 =  $('#pie1').easyPieChart({
              easing: 'easeOutBounce',
              scaleColor: false,
              scaleLength: 0,
              lineWidth: 8,
              size: 109,
              trackColor: '#5cb85c',
              barColor : function (percent) {
               return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
             },
             onStep: function(from, to, percent) {
              $(this.el).find('.percent').text(Math.round(percent));
            }
          });
            easyChart1.data("easyPieChart").update(achiev);
          }







        }else if (value.income_statement == 'COGS'){
         $('#income_statement2').text(value.income_statement);
         $('#realisasi2').text(format_round(value.sum_realisasi));
         $('#target2').text(format_round(value.sum_target));
         $('#prognosa2').text(format_round(value.sum_prognosa));
         var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
         var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

         var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
         var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
         var growth=0;
         $('#growth2').text("");
       
          if (result.is_net_operating_lastmonth.length > 0){
         
          var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

          if (isNaN(percent_growth)){
            growth = 0;
          }else{
            growth = percent_growth;
          }

        }else{
          growth = 0;
        }
        growth = isFinite(growth) ? growth : 0;
        console.debug("growth "+growth.toFixed(0));
        if (parseInt(growth) > 0){
          $('#growth2').prepend(txt2);
          $('#growth2').append(growth.toFixed(0)+"%");
          $("#growth2").attr("class", 'text-success');


          
        }else{
          $('#growth2').prepend(txt1);
          $('#growth2').append(growth.toFixed(0)+"%");
          $("#growth2").attr("class", 'text-danger');

        }
        $('#pie2').attr("data-percent", value.sum_achievement);
        var achievement = (value.sum_realisasi / value.sum_target)*100;
        var achiev;
        if (isNaN(achievement)){
          achiev = 0;
        }else{
          achiev = achievement;
        }

        achiev = isFinite(achiev) ? achiev : 0;

        $('#div-pie2').empty();
        $('#div-pie2').append("<div id='pie2' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
        if (achiev < 80){

          var easyChart2 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#cb3935',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }else if (achiev >= 80 && achiev <= 95){
          var easyChart2 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#f0ad4e',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }else if (achiev > 95){
          var easyChart1 =  $('#pie2').easyPieChart({
            easing: 'easeOutBounce',
            scaleColor: false,
            scaleLength: 0,
            lineWidth: 8,
            size: 109,
            trackColor: '#5cb85c',
            barColor : function (percent) {
             return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
           },
           onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          }
        });
          easyChart2.data("easyPieChart").update(achiev);
        }

      }else if (value.income_statement == 'GROSS PROFIT'){
       $('#income_statement3').text(value.income_statement);
       $('#realisasi3').text(format_round(value.sum_realisasi));
       $('#target3').text(format_round(value.sum_target));
       $('#prognosa3').text(format_round(value.sum_prognosa));
       var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
       var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

       var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
       var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
       var growth=0;
       $('#growth3').text("");
      
       if (result.is_net_operating_lastmonth.length > 0){
        var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

        if (isNaN(percent_growth)){
          growth = 0;
        }else{
          growth = percent_growth;
        }

      }else{
        growth = 0;
      }
      growth = isFinite(growth) ? growth : 0;
      console.debug("growth "+growth.toFixed(0));
      if (parseInt(growth) > 0){
        $('#growth3').prepend(txt2);
        $('#growth3').append(growth.toFixed(0)+"%");
        $("#growth3").attr("class", 'text-success');


     
      }else{
        $('#growth3').prepend(txt1);
        $('#growth3').append(growth.toFixed(0)+"%");
        $("#growth3").attr("class", 'text-danger');

      
      }
      $('#pie3').attr("data-percent", value.sum_achievement);
      var achievement = (value.sum_realisasi / value.sum_target)*100;
      var achiev;
      if (isNaN(achievement)){
        achiev = 0;
      }else{
        achiev = achievement;
      }

      achiev = isFinite(achiev) ? achiev : 0;
      $('#div-pie3').empty();
      $('#div-pie3').append("<div id='pie3' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
      if (achiev < 80){

        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#cb3935',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }else if (achiev >= 80 && achiev <= 95){
        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#f0ad4e',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }else if (achiev > 95){
        var easyChart3 =  $('#pie3').easyPieChart({
          easing: 'easeOutBounce',
          scaleColor: false,
          scaleLength: 0,
          lineWidth: 8,
          size: 109,
          trackColor: '#5cb85c',
          barColor : function (percent) {
           return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
         },
         onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
        easyChart3.data("easyPieChart").update(achiev);
      }

    }else if (value.income_statement == 'OPERATING EXPENSES'){
     $('#income_statement4').text(value.income_statement);
     $('#realisasi4').text(format_round(value.sum_realisasi));
     $('#target4').text(format_round(value.sum_target));
     $('#prognosa4').text(format_round(value.sum_prognosa));
     var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
     var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

     var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
     var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
     var growth=0;
     $('#growth4').text("");
   
     if (result.is_net_operating_lastmonth.length > 0){
      var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

      if (isNaN(percent_growth)){
        growth = 0;
      }else{
        growth = percent_growth;
      }

    }else{
      growth = 0;
    }
    growth = isFinite(growth) ? growth : 0;
    console.debug("growth "+growth.toFixed(0));
    if (parseInt(growth) > 0){
      $('#growth4').prepend(txt2);
      $('#growth4').append(growth.toFixed(0)+"%");
      $("#growth4").attr("class", 'text-success');


    }else{
      $('#growth4').prepend(txt1);
      $('#growth4').append(growth.toFixed(0)+"%");
      $("#growth4").attr("class", 'text-danger');

    }
    $('#pie4').attr("data-percent", value.sum_achievement);
    var achievement = (value.sum_realisasi / value.sum_target)*100;
    var achiev;
    if (isNaN(achievement)){
      achiev = 0;
    }else{
      achiev = achievement;
    }

    achiev = isFinite(achiev) ? achiev : 0;
    $('#div-pie4').empty();
    $('#div-pie4').append("<div id='pie4' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
    if (achiev < 80){

      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#cb3935',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }else if (achiev >= 80 && achiev <= 95){
      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#f0ad4e',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }else if (achiev > 95){
      var easyChart4 =  $('#pie4').easyPieChart({
        easing: 'easeOutBounce',
        scaleColor: false,
        scaleLength: 0,
        lineWidth: 8,
        size: 109,
        trackColor: '#5cb85c',
        barColor : function (percent) {
         return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
       },
       onStep: function(from, to, percent) {
        $(this.el).find('.percent').text(Math.round(percent));
      }
    });
      easyChart4.data("easyPieChart").update(achiev);
    }

  }else if (value.income_statement == 'NET OPERATING INCOME'){
   $('#income_statement5').text(value.income_statement);
   $('#realisasi5').text(format_round(value.sum_realisasi));
   $('#target5').text(format_round(value.sum_target));
   $('#prognosa5').text(format_round(value.sum_prognosa));
   var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
   var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

   var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
   var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
   var growth=0;
   $('#growth5').text("");
   
    if (result.is_net_operating_lastmonth.length > 0){
    var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

    if (isNaN(percent_growth)){
      growth = 0;
    }else{
      growth = percent_growth;
    }

  }else{
    growth = 0;
  }
  growth = isFinite(growth) ? growth : 0;
  console.debug("growth "+growth.toFixed(0));
  if (parseInt(growth) > 0){
    $('#growth5').prepend(txt2);
    $('#growth5').append(growth.toFixed(0)+"%");
    $("#growth5").attr("class", 'text-success');


  }else{
    $('#growth5').prepend(txt1);
    $('#growth5').append(growth.toFixed(0)+"%");
    $("#growth5").attr("class", 'text-danger');

   
  }
  $('#pie5').attr("data-percent", value.sum_achievement);
  var achievement = (value.sum_realisasi / value.sum_target)*100;
  var achiev;
  if (isNaN(achievement)){
    achiev = 0;
  }else{
    achiev = achievement;
  }

  achiev = isFinite(achiev) ? achiev : 0;
  $('#div-pie5').empty();
  $('#div-pie5').append("<div id='pie5' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
  if (achiev < 80){

    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#cb3935',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }else if (achiev >= 80 && achiev <= 95){
    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#f0ad4e',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }else if (achiev > 95){
    var easyChart5 =  $('#pie5').easyPieChart({
      easing: 'easeOutBounce',
      scaleColor: false,
      scaleLength: 0,
      lineWidth: 8,
      size: 109,
      trackColor: '#5cb85c',
      barColor : function (percent) {
       return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
     },
     onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
    easyChart5.data("easyPieChart").update(achiev);
  }

}else if (value.income_statement == 'OTHER INCOME'){
 $('#income_statement6').text(value.income_statement);
 $('#realisasi6').text(format_round(value.sum_realisasi));
 $('#target6').text(format_round(value.sum_target));
 $('#prognosa6').text(format_round(value.sum_prognosa));
 var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

 var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
 var growth=0;
 $('#growth6').text("");

  if (result.is_net_operating_lastmonth.length > 0){
  var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

  if (isNaN(percent_growth)){
    growth = 0;
  }else{
    growth = percent_growth;
  }

}else{
  growth = 0;
}
growth = isFinite(growth) ? growth : 0;
console.debug("growth net sales"+growth.toFixed(0));
if (parseInt(growth) > 0){
  $('#growth6').prepend(txt2);
  $('#growth6').append(growth.toFixed(0)+"%");
  $("#growth6").attr("class", 'text-success');



}else{
  $('#growth6').prepend(txt1);
  $('#growth6').append(growth.toFixed(0)+"%");
  $("#growth6").attr("class", 'text-danger');

}
$('#pie6').attr("data-percent", value.sum_achievement);
var achievement = (value.sum_realisasi / value.sum_target)*100;
var achiev;
if (isNaN(achievement)){
  achiev = 0;
}else{
  achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;
$('#div-pie6').empty();
$('#div-pie6').append("<div id='pie6' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
if (achiev < 80){

  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#cb3935',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}else if (achiev >= 80 && achiev <= 95){
  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#f0ad4e',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}else if (achiev > 95){
  var easyChart6 =  $('#pie6').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#5cb85c',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart6.data("easyPieChart").update(achiev);
}

}else if (value.income_statement == 'NET INCOME'){
 $('#income_statement7').text(value.income_statement);
 $('#realisasi7').text(format_round(value.sum_realisasi));
 $('#target7').text(format_round(value.sum_target));
 $('#prognosa7').text(format_round(value.sum_prognosa));
 var txt1 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt2 = $("<i class='fas fa-caret-up fa-lg'></i>");

 var txt3 = $("<i class='fas fa-caret-down fa-lg'></i>");
 var txt4 = $("<i class='fas fa-caret-up fa-lg'></i>");
 var growth=0;
 $('#growth7').text("");

  if (result.is_net_operating_lastmonth.length > 0){
  var percent_growth = (value.sum_realisasi/result.is_net_operating_lastmonth[0].realisasi) * 100;

  if (isNaN(percent_growth)){
    growth = 0;
  }else{
    growth = percent_growth;
  }

}else{
  growth = 0;
}
growth = isFinite(growth) ? growth : 0;
console.debug("growth "+growth.toFixed(0));
if (parseInt(growth) > 0){
  $('#growth7').prepend(txt2);
  $('#growth7').append(growth.toFixed(0)+"%");
  $("#growth7").attr("class", 'text-success');


}else{
  $('#growth7').prepend(txt1);
  $('#growth7').append(growth.toFixed(0)+"%");
  $("#growth7").attr("class", 'text-danger');

}
$('#pie7').attr("data-percent", value.sum_achievement);
var achievement = (value.sum_realisasi / value.sum_target)*100;
var achiev;
if (isNaN(achievement)){
  achiev = 0;
}else{
  achiev = achievement;
}

achiev = isFinite(achiev) ? achiev : 0;
$('#div-pie7').empty();
$('#div-pie7').append("<div id='pie7' class='chart' style='margin-top:0px' data-percent='0' data-scale-color='#ffb400'> <span class='percent'></span></div>");
if (achiev < 80){

  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#cb3935',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}else if (achiev >= 80 && achiev <= 95){
  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#f0ad4e',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}else if (achiev > 95){
  var easyChart7 =  $('#pie7').easyPieChart({
    easing: 'easeOutBounce',
    scaleColor: false,
    scaleLength: 0,
    lineWidth: 8,
    size: 109,
    trackColor: '#5cb85c',
    barColor : function (percent) {
     return (percent < 80 ? '#cb3935' : percent >= 80 && percent <= 95 ? '#f0ad4e' : '#5cb85c');
   },
   onStep: function(from, to, percent) {
    $(this.el).find('.percent').text(Math.round(percent));
  }
});
  easyChart7.data("easyPieChart").update(achiev);
}

}

})







        //chart initiation

        for (var j = 0; j <= myChart1.series.length; j++){              
         myChart1.series[0].remove();
       } 

       myChart1.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart2.series.length; j++){              
         myChart2.series[0].remove();
       } 

       myChart2.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart3.series.length; j++){              
         myChart3.series[0].remove();
       } 

       myChart3.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart4.series.length; j++){              
         myChart4.series[0].remove();
       } 

       myChart4.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart5.series.length; j++){              
         myChart5.series[0].remove();
       } 

       myChart5.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart6.series.length; j++){              
         myChart6.series[0].remove();
       } 

       myChart6.xAxis[0].setCategories([],false);


       for (var j = 0; j <= myChart7.series.length; j++){              
         myChart7.series[0].remove();
       } 

       myChart7.xAxis[0].setCategories([],false);


       var label1 = [];
       var data_target1 = [];
       var data_real1 = [];

       var label2 = [];
       var data_target2 = [];
       var data_real2 = [];

       var label3 = [];
       var data_target3 = [];
       var data_real3 = [];

       var label4 = [];
       var data_target4 = [];
       var data_real4 = [];

       var label5 = [];
       var data_target5 = [];
       var data_real5 = [];

       var label6 = [];
       var data_target6 = [];
       var data_real6 = [];

       var label7 = [];
       var data_target7 = [];
       var data_real7 = [];


       $.each(result.is_net_sales, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label1.push(month_name(value.month));
          data_target1.push(parseInt(value.target));
          data_real1.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_cogs, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label2.push(month_name(value.month));
          data_target2.push(parseInt(value.target));
          data_real2.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_gross_profit, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label3.push(month_name(value.month));
          data_target3.push(parseInt(value.target));
          data_real3.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_operating_expenses, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label4.push(month_name(value.month));
          data_target4.push(parseInt(value.target));
          data_real4.push(parseInt(value.realisasi));
        }

      })


       $.each(result.is_net_operating, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label5.push(month_name(value.month));
          data_target5.push(parseInt(value.target));
          data_real5.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_other_income, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label6.push(month_name(value.month));
          data_target6.push(parseInt(value.target));
          data_real6.push(parseInt(value.realisasi));
        }

      })

       $.each(result.is_net_income, function( key, value ) {
        if (value.month != 0 && value.month != 13 && value.month != 14 && value.month != 15 && value.month != 16){

          label7.push(month_name(value.month));
          data_target7.push(parseInt(value.target));
          data_real7.push(parseInt(value.realisasi));
        }

      })



       myChart1.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target1,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart1.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real1,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart1.xAxis[0].setCategories(label1,true);

       myChart2.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target2,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart2.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real2,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart2.xAxis[0].setCategories(label1,true);

       myChart3.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target3,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart3.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real3,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart3.xAxis[0].setCategories(label1,true);

       myChart4.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target4,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart4.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real4,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart4.xAxis[0].setCategories(label1,true);

       myChart5.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target5,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart5.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real5,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart5.xAxis[0].setCategories(label1,true);

       myChart6.addSeries({
         name: 'Target',
         type: 'spline',
         data: data_target6,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart6.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real6,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart6.xAxis[0].setCategories(label1,true);

       myChart7.addSeries({
         name: 'Target',
         type: 'line',
         data: data_target7,
         animation: {
          duration: 2000   
        },
        color: '#DC6B89'
      }, true);
       myChart7.addSeries({
         name: 'Real',
         type: 'areaspline',
         data: data_real7,
         animation: {
          duration: 2000   
        }, color: '#6CD0B7'
      }, true);

       myChart7.xAxis[0].setCategories(label1,true);




     }




   });


function numberWithCommas(x) {
    //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if (x){
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
      return 0;
    }
    
  }

  function format_round(rp){

    if(rp<1 && rp>0){
      return (Math.round(rp*100))+'%';

    }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
    }else{
      return ("Rp "+numberWithCommas(rp));
    }
  }


  function format_round_chart(rp){

    if(rp<1 && rp>0){
      return (Math.round(rp*100))+'%';

    }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
    }else{
      return (numberWithCommas(rp));
    }
  }

  function month_name(x){

   if (x == 1){
    return "Jan";
  }else if (x == 2){
    return "Feb";
  }else if (x == 3){
    return "Mar";
  }else if (x == 4){
    return "Apr";
  }else if (x == 5){
    return "Mei";
  }else if (x == 6){
    return "Jun";
  }else if (x == 7){
    return "Jul";
  }else if (x == 8){
    return "Agu";
  }else if (x == 9){
    return "Sep";
  }else if (x == 10){
    return "Okt";
  }else if (x == 11){
    return "Nov";
  }else if (x == 12){
    return "Des";
  }
}
</script>
