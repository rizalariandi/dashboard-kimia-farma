<div class="row">
    <div class="col-md-6">
        <div class="white-box"><div id="chart1"></div></div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="box-title"><h4>Total Bad Stock</h4><h1>xx.xxx</h1></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="box-title"><h4>Plant KFTD</h4><h1>xx.xxx</h1></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box">
<table class='table'>
    <tr>
        <th>Plant</th>
        <th>Value Barang</th>
    </tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
</table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="box-title"><h4>Plant NDC</h4><h1>xx.xxx</h1></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="box-title"><h4>Plant KFHO</h4><h1>xx.xxx</h1></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url()?>asset/js/highstock.js"></script>
<script src="<?=base_url()?>asset/js/exporting.js"></script>
<script>
// Build the chart
Highcharts.chart('chart1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'All Plants'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Plant Jakarta',
            y: 10.85
        }, {
            name: 'Plant Bandung',
            y: 4.67
        }, {
            name: 'Plant Watudakon',
            y: 4.18
        }, {
            name: 'Plant Semarang',
            y: 7.05
        }]
    }]
});
</script>