<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
}

.orange th{
    background-color: white;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">GAP Per Entitas (Sesuai RKAP)</h4>
        </div>
    </div>
</div>
<div class="row">
	<div class="white-box">
		<div class="table-responsive">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Entitas</th>
                          <th style="background-color: #FC8215" scope="col">Juni</th>
                          <th style="background-color: #FC8215" scope="col">Juli</th>
                          <th style="background-color: #FC8215" scope="col">Agustus</th>
                          <th style="background-color: #FC8215" scope="col">September</th>
                          <th style="background-color: #FC8215" scope="col">Oktober</th>
                          <th style="background-color: #FC8215" scope="col">November</th>
                          <th style="background-color: #FC8215" scope="col">Desember</th>
                          <th style="background-color: #FC8215" scope="col">Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style="background-color: #EAF4FE">KFHO</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #EAF4FE">KFTD</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #EAF4FE">KFA</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #EAF4FE">SIL</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #EAF4FE">KFSP</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #EAF4FE">Dawaa</th>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                          <td>xxx</td>
                        </tr>
                        <tr>
                          <td style="background-color: #DADADA">TOTAL</th>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                          <td style="background-color: #DADADA">xxx</td>
                        </tr>
                      </tbody>
                    </table>
		</div>
	</div>
</div>