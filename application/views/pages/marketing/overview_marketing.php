<?php
$wgserver = 'http://172.16.100.120/';

$user = 'admin_tableau';

extract($_POST);
$url = $wgserver . '/trusted';
$fields_string = 'trusted_site=&username=' . $user;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

//tambahan
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
// return $result;
curl_close($ch);
echo '
    <script type="text/javascript">    
        var viz;
        var selectedSL
        function initViz(params, sheetexc) {
            var containerDiv = document.getElementById("vizContainer"),
                url = params,
                options = {
                    hideTabs: true,
                    hideToolbar: true,
                   onFirstInteractive: function() {

                    workbook = viz.getWorkbook();
                    dash = viz.getWorkbook().getActiveSheet();
                    workbook.activateSheetAsync(dash)
                      .then(function(dashboard) {
                        
                        var worksheets = dashboard.getWorksheets();
                        
                        var sheetNames = [];
                        
                        for (var i = 0, len = worksheets.length; i < len; i++) {

                          var sheet = worksheets[i];
                          sheetNames.push(sheet.getName());
                        }

                        var sel = document.getElementById("SheetList");
                        var fragment = document.createDocumentFragment();
                        if(sheetexc !=null){
                            sheetNames = sheetexc;
                        }
                        sheetNames.forEach(function(sheetName, index) {

                          var opt = document.createElement("option");
                          var shettemp = sheetName.length;
                          if(shettemp == 2){
                              opt.innerHTML = sheetName[1];
                              opt.value = sheetName[0];
                          } else {

                              opt.innerHTML = sheetName;
                              opt.value = sheetName;
                          }
                          fragment.appendChild(opt);
                        });

                        sel.appendChild(fragment);

                      });
                  }
                };

            viz = new tableau.Viz(containerDiv, url, options); 
            
        }

        function exportToPDF() {
          viz.showExportPDFDialog();
          $("#myModal").hide();
        }
        
        function getCSV() {
          location.href="http://172.16.100.120/trusted/' . $result . '/views/DahsboardExecutiveMarketingKF2mobile/OverviewExecutiveMarkerting.csv";
        }
        function getVizData() {
          options = {

             maxRows: 0, // Max rows to return. Use 0 to return all rows
             ignoreAliases: false,
             ignoreSelection: true,
             includeAllColumns: false
           };

           sheet = viz.getWorkbook().getActiveSheet();
           
           //if active tab is a worksheet, get data from that sheet
           if (sheet.getSheetType() === "worksheet") {
             sheet.getUnderlyingDataAsync(options).then(function(t) {
               buildMenu(t);
             });

             //if active sheet is a dashboard get data from a specified sheet
           } else {
             worksheetArray = viz.getWorkbook().getActiveSheet().getWorksheets();
             console.debug(worksheetArray);
             console.debug(selectedSL);
             if(selectedSL != null){                          
              console.log(viz.getWorkbook().getActiveSheet().getName()) 
              if (viz.getWorkbook().getActiveSheet().getName() === "Dashboard" && $(".page").val() === "overview marketing")
              {
                viz.showExportCrossTabDialog(selectedSL)
              }
              else if (viz.getWorkbook().getActiveSheet().getName() === "Dashboard_D" && $(".page").val() === "detail informasi")
              {
                viz.showExportCrossTabDialog(selectedSL)
              }                
              else {
                alert("The worksheet you are looking for was not found")
              }
             } else {                    
                 viz.showExportCrossTabDialog();
             }
           }
     }
     
        function exportExcel() {
            viz.showExportDataDialog();
        }

    window.onload = function() {
    
      var sheetexc = [];
   if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)){
           initViz("http://172.16.100.120/trusted/' . $result . '/views/DahsboardExecutiveMarketingKF2mobile/OverviewExecutiveMarkerting", sheetexc);
        }else{
            initViz("http://172.16.100.120/trusted/' . $result . '/views/overview_marketing_prod_tata/Dashboard", sheetexc);
        }

      sheetexc.push("Sales Contribution by Lini");
      sheetexc.push("Trend Sales");
      sheetexc.push("Sales Branch by MTD");
      sheetexc.push("Sales Branch by YTD");      
      sheetexc.push("Sales by Customer Segment");
      sheetexc.push("Top 10 Brand");
      sheetexc.push("Sales by Product");
      
    };   
    
    function CSV()
    {
      $("#myModal").hide();
      if($(".page").val() === "detail informasi")
      {
        selectedSL = $(".sheetlist_di .select").val()      
        getVizData()
      }      
      if($(".page").val() === "overview marketing")
      {
        selectedSL = $(".sheetlist_om .select").val()      
        getVizData()
      }      
    }

    function myBtn() {
      console.log("we call it")
      $("#myModal").show();
    }
     function closeModal() {
      $("#myModal").hide();
    }

    function page_select()
    {      
      if($(".page").val() === "overview marketing")
      {        
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
          $(".sheetlist_om").show()
      }
      else if ($(".page").val() === "detail informasi")
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
        $(".sheetlist_di").show()
      }
      else if ($(".page").val() === "marketing performanece")
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
        $(".value,.month,.year,.top_struktural,.lini_name").show()
      }
      else
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
      }
    }

  </script>
  <style>
  .sheetlist_om ,.sheetlist_di ,.value ,.month ,.top_struktural,.lini_name,.top_filter,.year
  {
    display: none;
  }
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  z-index: 10 !important;
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  -webkit-animation-name: fadeIn; /* Fade in the background */
  -webkit-animation-duration: 0.4s;
  animation-name: fadeIn;
  animation-duration: 0.4s
}

/* Modal Content */
.modal-content {
  position: fixed;
  bottom: 0;
  background-color: #fefefe;
  width: 100%;
  -webkit-animation-name: slideIn;
  -webkit-animation-duration: 0.4s;
  animation-name: slideIn;
  animation-duration: 0.4s
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color: #08388F;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #08388F;
  color: white;
}

/* Add Animation */
@-webkit-keyframes slideIn {
  from {bottom: -300px; opacity: 0} 
  to {bottom: 0; opacity: 1}
}

@keyframes slideIn {
  from {bottom: -300px; opacity: 0}
  to {bottom: 0; opacity: 1}
}

@-webkit-keyframes fadeIn {
  from {opacity: 0} 
  to {opacity: 1}
}

@keyframes fadeIn {
  from {opacity: 0} 
  to {opacity: 1}
}
.mb-0
{
  margin-bottom: 0;
}
.opacity-1
{
  opacity: 1;
}
</style>
<div id="vizContainer">
</div>

<button class="button" onclick="myBtn()">Export data to CSV/PDF</button>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close text-white opacity-1" onclick="closeModal()">&times;</span>
            <h2 class="text-white">Export data to CSV/PDF</h2>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <form>
                        <fieldset>
                            <legend>CSV</legend>
                            <div class="form-row">
                                <div class="form-group col-md-6 mb-0">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Page</label>
                                        <select class="form-control page" onchange="page_select()"
                                            id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>
                                            <option value="overview marketing">Overview Marketing</option>
                                            <option value="detail informasi">Detail Informasi</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group OM col-md-6 mb-0 sheetlist_om">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Sheet List</label>
                                        <select class="form-control select" id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>s
                                            <option value="Sales Branch by MTD">Sales Branch by MTD</option>
                                            <option value="Sales Branch by YTD">Sales Branch by YTD</option>
                                            <option value="Sales Contribution by Lini">Sales Contribution by Lini</option>
                                            <option value="Trend Sales">Trend Sales</option>
                                            <option value="Sales by Customer Segment">Sales by Customer Segment</option>
                                            <option value="Top 10 Brand">Top 10 Brand</option>
                                            <option value="Sales by Product">Sales by Product</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group DI col-md-6 mb-0 sheetlist_di">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Sheet List</label>
                                        <select class="form-control select" id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>
                                            <option value="Sales by Customer Segment_D">Sales by Customer Segment
                                            </option>
                                            <option value="Sales by Customer Name_D">Sales by Customer Name</option>
                                            <option value="Sales by Product Brand_D">Sales by Product Brand</option>
                                            <option value="Sales by Product_D">Sales by Product</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-0 value">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1 selec">Value</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>HNA</option>
                                        <option>HJP</option>
                                        <option>HJP-PTD</option>
                                        <option>HJD</option>
                                        <option>HPP</option>
                                        <option>HNA-PTD</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-0 month">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Month</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>January</option>
                                        <option>Febuary</option>
                                        <option>March</option>
                                        <option>April</option>
                                        <option>June</option>
                                        <option>August</option>
                                        <option>September</option>
                                        <option>October</option>
                                        <option>November</option>
                                        <option>December</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-0 year">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Year</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-0 top_struktural">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Top Struktural</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>AM</option>
                                        <option>MM</option>
                                        <option>PM</option>
                                        <option>RSM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 mb-0 lini_name">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Lini Name</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>Bahan Baku</option>
                                        <option>Bisnis Internasional</option>
                                        <option>Etikal</option>
                                        <option>Kosmetik</option>
                                        <option>OGB dan Produk Khusus</option>
                                        <option>OTC dan Herbal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6 mb-0 top_filter">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Top Filter</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>% Growth</option>
                                        <option>% Achievement</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-2">
                                        <button onclick="CSV()" type="button"
                                            class="btn btn-primary btn-block mb-2">Download
                                            CSV</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="col-md-2">
                    <form>
                        <fieldset>
                            <legend>PDF</legend>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                        <button onclick="exportToPDF();" type="button"
                                            class="btn btn-primary btn-block mb-2">Download
                                            PDF</button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>  
';
// <div class="select-style"><select id="SheetList"></select></div>
// <button class="button" onclick="getVizData()">Export data to CSV</button>
//     <button class="button" onclick="exportToPDF();">Export to PDF</button>
