<?php
$wgserver = 'http://172.16.100.120/';

$user = 'admin_tableau';

extract($_POST);
$url = $wgserver . '/trusted';
$fields_string = 'trusted_site=&username=' . $user;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

//tambahan
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
// return $result;
curl_close($ch);
echo '
    <script type="text/javascript"> 

        var viz;

        function initViz(params, sheetexc) {
            var containerDiv = document.getElementById("vizContainer"),
                url = params,
                options = {
                    hideTabs: true,
                    hideToolbar: true,
                   onFirstInteractive: function() {

                    workbook = viz.getWorkbook();
                    dash = viz.getWorkbook().getActiveSheet();
                    workbook.activateSheetAsync(dash)
                      .then(function(dashboard) {
                        
                        var worksheets = dashboard.getWorksheets();
                        
                        var sheetNames = [];
                        
                        for (var i = 0, len = worksheets.length; i < len; i++) {

                          var sheet = worksheets[i];
                          sheetNames.push(sheet.getName());
                        }

                        var sel = document.getElementById("SheetList");
                        var fragment = document.createDocumentFragment();
                        if(sheetexc !=null){
                            sheetNames = sheetexc;
                        }
                        sheetNames.forEach(function(sheetName, index) {

                          var opt = document.createElement("option");
                          var shettemp = sheetName.length;
                          if(shettemp == 2){
                              opt.innerHTML = sheetName[1];
                              opt.value = sheetName[0];
                          } else {

                              opt.innerHTML = sheetName;
                              opt.value = sheetName;
                          }
                          fragment.appendChild(opt);
                        });

                        sel.appendChild(fragment);

                      });
                  }
                };

            viz = new tableau.Viz(containerDiv, url, options); 
            
        }

        function exportToPDF() {
            viz.showExportPDFDialog();
            $("#myModal").hide();
        }
        
        function getCSV() {
          location.href="http://172.16.100.120/trusted/' . $result . '/views/DahsboardExecutiveMarketingKF2mobile/DetailInformasi.csv";
        }
        function getVizData() {
             options = {

                maxRows: 0, // Max rows to return. Use 0 to return all rows
                ignoreAliases: false,
                ignoreSelection: true,
                includeAllColumns: false
              };

              sheet = viz.getWorkbook().getActiveSheet();
              
              //if active tab is a worksheet, get data from that sheet
              if (sheet.getSheetType() === "worksheet") {
                sheet.getUnderlyingDataAsync(options).then(function(t) {
                  buildMenu(t);
                });

                //if active sheet is a dashboard get data from a specified sheet
              } else {
                worksheetArray = viz.getWorkbook().getActiveSheet().getWorksheets();
                console.debug(worksheetArray);
                console.debug(document.getElementById("SheetList"));
                if(document.getElementById("SheetList") != null){
                    for (var i = 0; i < worksheetArray.length; i++) {
                      worksheet = worksheetArray[i];
                      sheetName = worksheet.getName();
                    
                          var selectedVal = document.getElementById("SheetList").value;
                           console.debug("selected val "+selectedVal);

                          //get the data from the selected sheet
                          if (sheetName.trim() === selectedVal.trim()) {
                     
                            viz.showExportCrossTabDialog(sheetName);
                          }

                    }
                } else {
                       
                    viz.showExportCrossTabDialog();

                }
              }
        }
        function exportExcel() {
            viz.showExportDataDialog();
        }

    window.onload = function() {
    
      var sheetexc = [];
   if( navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)){
           initViz("http://172.16.100.120/trusted/' . $result . '/views/DahsboardExecutiveMarketingKF2mobile/DetailInformasi", sheetexc);
        }else{
            initViz("http://172.16.100.120/trusted/' . $result . '/views/Marketing_performance/Dashboard", sheetexc);
        }
        sheetexc.push("Marketing Performance");            
    };
    
    function myBtn() {
      console.log("we call it")
      $("#myModal").show();
    }
     function closeModal() {
      $("#myModal").hide();
    }

    function page_select()
    {      
      if($(".page").val() === "overview marketing")
      {        
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
          $(".sheetlist_om,.value,.month,.year").show()
      }
      else if ($(".page").val() === "detail informasi")
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
        $(".sheetlist_di").show()
      }
      else if ($(".page").val() === "marketing performanece")
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name").hide()
        $(".value,.month,.year,.top_struktural,.lini_name,.top_filter").show()
      }
      else
      {
        $(".sheetlist_di,.sheetlist_om,.value,.month,.year,.top_struktural,.lini_name,.top_filter").hide()
      }
    }

    function CSV() {   
      if( $(".page").val() && $(".value .select").val() && $(".month .select").val() && $(".top_filter .select").val() && $(".year .select").val() && $(".top_struktural .select").val() && $(".lini_name .select").val()) 
      {
      var year = $("#year").val();      
      var entitas = $("#entitas").val();    
        $.ajax({
          url: "' . base_url("index.php/Marketing/getdata_marketing_performance") . '",
          method: "POST",
          data: {
            "value": $(".value .select").val(),
            "month": $(".month .select").val(),
            "top_filter": $(".top_filter .select").val(),
            "year": $(".year .select").val(),
            "top_struktural": $(".top_struktural .select").val(),
            "lini_name": $(".lini_name .select").val()
          },
          beforeSend: function() {
            // loading();
          },
          success: function(data) {
          
  
          }
        }).done(function(data) {                  
          if (data.length){
            data = JSON.parse(data)            
            for (i = 0; i < data.length; i++) {
              console.log(data[i].value_number)
              delete data[i].value_number;
            }
          JSONToCSVConvertor(data, "Marketing Performance", true);
        }          
        }).fail(function(jqXHR, textStatus, errorThrown) {
          // if (jqXHR.status != 422)
          //     swal("Error " + jqXHR.status, textStatus + ", " + errorThrown, "error");
        });    
      }
      else {
        alert("Please complete selected and try again")
      }
    }

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
      //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
      var arrData = typeof JSONData != "object" ? JSON.parse(JSONData) : JSONData;
      
      var CSV = "";    
      //Set Report title in first row or line          
  
      //This condition will generate the Label/Header
      if (ShowLabel) {
          var row = "";
          
          //This loop will extract the label from 1st index of on array
          for (var index in arrData[0]) {
              
              //Now convert each value to string and comma-seprated
              row += index + ",";
          }
  
          row = row.slice(0, -1);
          
          //append Label row with line break
          CSV += row + "\r\n";
      }
      
      //1st loop is to extract each row
      for (var i = 0; i < arrData.length; i++) {
          var row = "";
          
          //2nd loop will extract each column and convert it in string comma-seprated
          for (var index in arrData[i]) {
              row += "" + arrData[i][index] + ",";
          }
  
          row.slice(0, row.length - 1);
          
          //add a line break after each row
          CSV += row + "\r\n";
      }
  
      if (CSV == "") {        
          alert("Invalid data");
          return;
      }   
      
      //Generate a file name
      var fileName = "MyReport_";
      //this will remove the blank-spaces from the title and replace it with an underscore
      fileName += ReportTitle.replace(/ /g,"_");   
      
      //Initialize file format you want csv or xls
      var uri = "data:text/csv;charset=utf-8," + escape(CSV);
      
      // Now the little tricky part.
      // you can use either>> window.open(uri);
      // but this will not work in some browsers
      // or you will not get the correct file extension    
      
      //this trick will generate a temp <a /> tag
      var link = document.createElement("a");    
      link.href = uri;
      
      //set the visibility hidden so it will not effect on your web-layout
      link.style = "visibility:hidden";
      link.download = fileName + ".csv";
      
      //this part will append the anchor tag and remove it after automatic click
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
  }

  </script>
  <style>
  .sheetlist_om ,.sheetlist_di ,.value ,.month ,.top_struktural,.lini_name,.top_filter,.year
  {
    display: none;
  }
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  z-index: 10 !important;
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  -webkit-animation-name: fadeIn; /* Fade in the background */
  -webkit-animation-duration: 0.4s;
  animation-name: fadeIn;
  animation-duration: 0.4s
}

/* Modal Content */
.modal-content {
  position: fixed;
  bottom: 0;
  background-color: #fefefe;
  width: 100%;
  -webkit-animation-name: slideIn;
  -webkit-animation-duration: 0.4s;
  animation-name: slideIn;
  animation-duration: 0.4s
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color: #08388F;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #08388F;
  color: white;
}

/* Add Animation */
@-webkit-keyframes slideIn {
  from {bottom: -300px; opacity: 0} 
  to {bottom: 0; opacity: 1}
}

@keyframes slideIn {
  from {bottom: -300px; opacity: 0}
  to {bottom: 0; opacity: 1}
}

@-webkit-keyframes fadeIn {
  from {opacity: 0} 
  to {opacity: 1}
}

@keyframes fadeIn {
  from {opacity: 0} 
  to {opacity: 1}
}
.mb-0
{
  margin-bottom: 0;
}
.opacity-1
{
  opacity: 1;
}
</style>
<div id="vizContainer">
</div>

<button class="button" onclick="myBtn()">Export data to CSV/PDF</button>
<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close text-white opacity-1" onclick="closeModal()">&times;</span>
            <h2 class="text-white">Export data to CSV/PDF</h2>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-10">
                    <form>
                        <fieldset>
                            <legend>CSV</legend>
                            <div class="form-row">
                                <div class="form-group col-md-6 mb-0">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Page</label>
                                        <select class="form-control page" onchange="page_select()"
                                            id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>
                                            <option value="marketing performanece">Marketing Performanece</option>      
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group OM col-md-6 mb-0 sheetlist_om">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Sheet List</label>
                                        <select class="form-control select" id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>s
                                            <option value="Sales Branch by MTD">Sales Branch by MTD</option>
                                            <option value="Sales Branch by YTD">Sales Branch by YTD</option>
                                            <option value="Sales Contribution by Lini">Sales Contribution by Lini</option>
                                            <option value="Trend Sales">Trend Sales</option>
                                            <option value="Sales by Customer Segment">Sales by Customer Segment</option>
                                            <option value="Top 10 Brand">Top 10 Brand</option>
                                            <option value="Sales by Product">Sales by Product</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group DI col-md-6 mb-0 sheetlist_di">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Sheet List</label>
                                        <select class="form-control select" id="exampleFormControlSelect1">
                                            <option value="">Please Select</option>
                                            <option value="Sales by Customer Segment_D">Sales by Customer Segment
                                            </option>
                                            <option value="Sales by Customer Name_D">Sales by Customer Name</option>
                                            <option value="Sales by Product Brand_D">Sales by Product Brand</option>
                                            <option value="Sales by Product_D">Sales by Product</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3 mb-0 value">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1 selec">Value*</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option value="hna_ty">HNA</option>
                                        <option value="hjp_ty">HJP</option>
                                        <option value="hjp_ptd_ty">HJP-PTD</option>
                                        <option value="hjd_ty">HJD</option>
                                        <option value="hpp_ty">HPP</option>
                                        <option value="hna_ptd_ty">HNA-PTD</option>
                                    </select>
                                </div>
                            </div>    
                            <div class="form-group col-md-3 mb-0 top_filter">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Top Filter*</label>
                                <select class="form-control select" id="exampleFormControlSelect1">
                                    <option value="">Please Select</option>
                                    <option>% Growth</option>
                                    <option>% Achievement</option>
                                </select>
                            </div>
                          </div>                        
                            <div class="form-group col-md-3 mb-0 month">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Month*</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option value="01">January</option>
                                        <option value="02">Febuary</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3 mb-0 year">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Year*</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3 mb-0 top_struktural">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Top Struktural*</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>AM</option>
                                        <option>MM</option>
                                        <option>PM</option>
                                        <option>RSM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3 mb-0 lini_name">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Lini Name*</label>
                                    <select class="form-control select" id="exampleFormControlSelect1">
                                        <option value="">Please Select</option>
                                        <option>Bahan Baku</option>
                                        <option>Bisnis Internasional</option>
                                        <option>Etikal</option>
                                        <option>Kosmetik</option>
                                        <option>OGB dan Produk Khusus</option>
                                        <option>OTC dan Herbal</option>
                                    </select>
                                </div>
                            </div>                       
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-2">
                                        <button onclick="CSV()" type="button"
                                            class="btn btn-primary btn-block mb-2">Download
                                            CSV</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div class="col-md-2">
                    <form>
                        <fieldset>
                            <legend>PDF</legend>

                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-12">
                                        <button onclick="exportToPDF();" type="button"
                                            class="btn btn-primary btn-block mb-2">Download
                                            PDF</button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>  
';
?>

<!-- <div class="select-style"><select id="SheetList"></select></div> -->
<!-- <button class="button" onclick="getVizData()">Export data to CSV</button>
    <button class="button" onclick="exportToPDF();">Export to PDF</button> -->