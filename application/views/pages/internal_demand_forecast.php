<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet"></link>
<style>
th {
    background-color: #093890;
    color: white;
    text-align: center;
} 

td {
    color: #4A5675;
    text-align: center;
} 
</style>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Demand Forecast Score - Marketing</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Plant</th>
                  <th scope="col">Harga Beli KFTD (HJP Marketing)</th>
                  <th scope="col">Target Marketing</th>
                  <th scope="col">DF Score Marketing by Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>Otto</td>
                </tr>
                <tr>
                  <td>2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>Thornton</td>
                </tr>
                <tr>
                  <td>3</th>
                  <td>Larry</td>
                  <td>the Bird</td>
                  <td>the Bird</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    
    <div class="col-md-12">
        <div class="col-lg-10 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Demand Forecast Score - KFTD</h4>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
            
            <a href="#"><h5 class="page-title"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> <u>Download</u></span></h5></a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table table-striped">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Plant</th>
                  <th scope="col">Net Value</th>
                  <th scope="col">Target Penjualan</th>
                  <th scope="col">DF Score KFTD</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>Otto</td>
                </tr>
                <tr>
                  <td>2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>Thornton</td>
                </tr>
                <tr>
                  <td>3</th>
                  <td>Larry</td>
                  <td>the Bird</td>
                  <td>the Bird</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>