<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Target Marketing</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addAdjusment()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Target</button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right pt-10">
            Entitas : <select class="entitasF" name="entitasF">
                <option value="">Choose Distributor</option>
                <option value="APL">APL</option>
                <option value="KFHO">KFHO</option>
                <option value="KFTD">KFTD</option>
            </select>
            Year : <select class="yearF" name="year">
                <option value="">Choose Year</option>
                <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                    echo "<option value='$i'>$i</option>";
                } ?>
            </select>
            Month : <select class="monthF" name="month">
                <option value="">Choose Month</option>
                <?php for ($i = 1; $i <= 12; $i++) {
                    echo "<option value='$i'>$i</option>";
                } ?>
            </select>
            <input type="button" onclick="filterAdjusment($('.entitasF').val(),$('.yearF').val(),$('.monthF').val())" value="Filter" class="btn btn-success">
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">
        <div class="table-responsive">
            <table id="adjusment-data" class="table table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th class="col-lg-1 col-sm-1">No</th>
                    <th class="col-lg-1 col-sm-1">month</th>
                    <th class="col-lg-1 col-sm-1">year</th>
                    <th class="col-lg-2 col-sm-2">distributor_code</th>
                    <th class="col-lg-1 col-sm-1">lini_code</th>
                    <th class="col-lg-1 col-sm-1">lini_name</th>
                    <!-- <th class="col">f01</th> -->
                    <th class="col-lg-1 col-sm-1">branch_code</th>
                    <th class="col-lg-1 col-sm-1">branch_name</th>
                    <th class="col-lg-2 col-sm-2">qty</th>
                    <th class="col-lg-1 col-sm-1">target_hna</th>
                    <th class="col-lg-1 col-sm-1">target_hna_nett</th>
                    <th class="col-lg-1 col-sm-1">target_hjd</th>
                    <th class="col-lg-1 col-sm-1">target_hjp</th>
                    <th class="col-lg-1 col-sm-1">target_hpp</th>
                    <th class="col-lg-2 col-sm-2">last update</th>
                    <th class="col-lg-1 col-sm-1">action</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Target</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Distributor :</label>
                        <div class="col-sm-10">
                            <select class="form-control entitas">
                                <option value="">Choose Entitas</option>
                                <option value='APL'>APL</option>
                                <option value='KFHO'>KFHO</option>
                                <option value='KFTD'>KFTD</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Month <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select class="form-control month" required>
                                <option value="">Choose Month</option>
                                <?php for ($i = 1; $i <= 12; $i++) {
                                    echo "<option value='$i'>$i</option>";
                                } ?>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Month is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Year :</label>
                        <div class="col-sm-10">
                            <select class="form-control year">
                                <option value="">Choose Year</option>
                                <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                    echo "<option value='$i'>$i</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Lini Name :</label>
                        <div class="col-sm-10">
                            <select class="form-control lini">
                                <option value="">Choose Lini</option>
                                <?php

                                foreach ($lini as $key => $value) {
                                    echo '<option value="' . $value['lini_code'] .'-'.$value['lini_name']. '">' . $value['lini_name'] . '</option>';

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Branch Name :</label>
                        <div class="col-sm-10">
                            <select class="form-control branch">
                                <option value="">Choose Branch</option>
                                <?php

                                foreach ($branch as $key => $value) {
                                    echo '<option value="' . $value['branch_code'] .'-'.$value['branch_name']. '">' . $value['branch_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Qty <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnQty" onclick="$('.btnQty').text($('.btnQty').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target HNA <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnHNA" onclick="$('.btnHNA').text($('.btnHNA').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target_hna" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target HNA Nett <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnNett" onclick="$('.btnNett').text($('.btnNett').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target_nett" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target HJD <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnHJD" onclick="$('.btnHJD').text($('.btnHJD').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target_hjd" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target HJP <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnHJP" onclick="$('.btnHJP').text($('.btnHJP').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target_hjp" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target HPP <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnHPP" onclick="$('.btnHPP').text($('.btnHPP').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target_hpp" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-sm-2">Real:</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control real" placeholder="Enter Real">
                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                        <label class="control-label col-sm-2">F01:</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control f01" placeholder="Enter F01">
                        </div>
                    </div> -->
                    <div class="form-group lastupdate">
                        <label class="control-label col-sm-2">Last Update :<span class="text-danger"></span></label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control last_update" required placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    //$('#realisasi-data').DataTable();
    var Id
    $(".help-block").hide();
    var data_table;
    $(document).ready(function() {
        data_table = $('#adjusment-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": true,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,

            "columns": [{
                data: 'id',
                render: function(data, type, row) {
                    return (i = i + 1);
                }
            },
                {
                    data: 'month',
                    title: 'Month'
                },
                {
                    data: 'year',
                    title: 'Year',
                    className: 'text-left'
                },
                {
                    data: 'distributor_code',
                    title: 'Distributor',
                    className: 'text-left'
                },
                {
                    data: 'lini_code',
                    title: 'Lini Code',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'lini_name',
                    title: 'Lini Name',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'branch_code',
                    title: 'Branch Code',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'branch_name',
                    title: 'Branch Name',
                    className: 'text-right'
                },
                {
                    data: 'qty',
                    title: 'Qty',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'target_hna',
                    title: 'Target HNA',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'target_hna_nett',
                    title: 'Target HNA Nett',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'target_hjd',
                    title: 'Target HJD',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'target_hjp',
                    title: 'Target HJP',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'target_hpp',
                    title: 'Target HPP',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    title: 'Last Update',
                    data: 'last_update'
                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateAdjusment(' +
                            (row.id ? row.id : "''") +
                            ',' + (row.branch_code ? row.branch_code : "''") +
                            ',' + (row.branch_name ? "'" + row.branch_name + "'" : "''") +
                            ',' + (row.distributor_code ? "'" + row.distributor_code + "'" : "''") +
                            ',' + (row.last_update ? "'" + row.last_update + "'" : "''") +
                            ',' + (row.lini_code ? row.lini_code : "''") +
                            ',' + (row.lini_name ? "'" + row.lini_name + "'" : "''") +
                            ',' + (row.month ? "'" + row.month + "'" : "''") +
                            ',' + (row.qty ? row.qty : "''") +
                            ',' + (row.target_hjd ? row.target_hjd : "''") +
                            ',' + (row.target_hjp ? row.target_hjp : "''") +
                            ',' + (row.target_hna ? row.target_hna : "''") +
                            ',' + (row.target_hna_nett ? row.target_hna_nett : "''") +
                            ',' + (row.target_hpp ? row.target_hpp : "''") +
                            ',' + (row.year ? "'" + row.year + "'" : "''") +
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteAdjusment(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });
        loadDataTable()
    });

    function loadDataTable(entitas, year, month) {
        var length;
        var newRow = [];
        var iterasi = 0;
        i = 0;
        $.ajax({
            url: "<?php echo base_url('index.php/Adj_target/getAdjTarget') ?>",
            method: 'get',
            data: {
                'year': year,
                'entitas': entitas,
                'month': month,
            },
            async: false,
            beforeSend: function() {
                $('button[type="submit"]').attr('disabled', true);
            },
            complete: function() {
                $('button[type="submit"]').attr('disabled', false);
            },
            success: function(data) {
                data = JSON.parse(data)
                length = data.length;
                newRow = data
            }
        }).done(function() {
            if (length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        });
    }

    function saveData() {
        // console.log(($('.btnPNA').text() == '-' ? '-' : '') + $('.adjusment').val().replace(/[.]/g, ''))
        // console.log(($('.btnPNT').text() == '-' ? '-' : '') + $('.target').val().replace(/[.]/g, ''))
        if ($('.month').val()) {
            if ($('.post').text() == 'Save') {
                var arr_branch = $('.branch').val().split("-",2);
                var arr_lini = $('.lini').val().split("-",2);

                PostAdjusment(null, arr_branch[0], arr_branch[1], $('.entitas').val(), arr_lini[0], arr_lini[1], $('.month').val(), $('.target').val(), $('.target_hjd').val(),$('.target_hjp').val(),$('.target_hna').val(),$('.target_nett').val(),$('.target_hpp').val(),$('.year').val())
            } else {
                PostAdjusment(Id, arr_branch[0], arr_branch[1], $('.entitas').val(), arr_lini[0], arr_lini[1], $('.month').val(), $('.target').val(), $('.target_hjd').val(),$('.target_hjp').val(),$('.target_hna').val(),$('.target_nett').val(),$('.target_hpp').val(),$('.year').val())
            }
        } else {
            $(".help-block").show();
        }
    }

    function addAdjusment() {
        $('.lastupdate').hide();
        $('#ModalLabel').text('Add Target');
        $(".help-block").hide();
        $('.post').text('Save');
        Id = null;
        $('.target').val(undefined);
        $('.target_hna').val(undefined);
        $('.target_nett').val(undefined);
        $('.target_hjd').val(undefined);
        $('.target_hjp').val(undefined);
        $('.target_hpp').val(undefined);
        $('.uraian').val(undefined);
        $('.year').val(undefined);
        $('.month').val(undefined);
        $('.branch').val(undefined);
        $('.lini').val(undefined);
        $('.entitas').val(undefined);
        $('.btnPNT').text('+');
        $('.btnPNA').text('+');
    }

    function updateAdjusment(id, branch_code, branch_name, distributor_code, last_update, lini_code, lini_name, month, qty, target_hjd,target_hjp,target_hna,target_hna_nett,target_hpp,year) {
        // var minusA = adjusment.toString().includes("-") ? '-' : '+';
        // var minusT = target.toString().includes("-") ? '-' : '+';
        // $('.btnPNT').text(minusT);
        // $('.btnPNA').text(minusA);
        $('#ModalLabel').text('Update Target')
        Id = id;
        $(".help-block").hide();
        // $('.adjusment').val(adjusment);
       // $('.adjusment').val(adjusment ? adjusment.toFixed(0).toString().replace("-", "") : null)
        $('.entitas').val(distributor_code);
        $('.branch').val(branch_name);
        $('.month').val(month);
        $('.lini').val(lini_code+'-'+lini_name);
        $('.branch').val(branch_code+'-'+branch_name);
       // $('.real').val(real);
        // $('.target').val(target);
        //$('.target').val(qty ? qty.toFixed(0).toString().replace("-", "") : null)
        $('.target').val(qty);
        $('.target_hjd').val(target_hjd);
        $('.target_hjp').val(target_hjp);
        $('.target_hna').val(target_hna);
        $('.target_nett').val(target_hna_nett);
        $('.target_hpp').val(target_hpp);
      //  $('.uraian').val(uraian);
        $('.year').val(year);
        $('.post').text('Update');
        if (last_update) {
            $('.last_update').val(last_update);
            $('.lastupdate').show()
        } else {
            $('.lastupdate').hide()
        }
    }

    function deleteAdjusment(id) {
        swal({
            title: "Are you sure?",
            text: "Are you sure to delete!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    PostAdjusment(id, null, null, null, null, null, null, null, null, true)
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostAdjusment(id, branch_code, branch_name, distributor_code, lini_code, lini_name, month, qty,target_hjd,target_hjp,target_hna,target_hna_nett,target_hpp, year, _delete) {
        $.ajax({
            url: "<?php echo base_url('index.php/Adj_target/postAdjTarget') ?>",
            method: 'post',
            data: {
                'id': id,
                'branch_code': branch_code,
                'branch_name': branch_name,
                'distributor_code': distributor_code,
                'lini_code': lini_code,
                'lini_name': lini_name,
                'month': month,
                'qty': qty,
                'target_hjd': target_hjd,
                'target_hjp': target_hjp,
                'target_hna': target_hna,
                'target_hna_nett': target_hna_nett,
                'target_hpp': target_hpp,
                'year': year,
                'delete': _delete
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.result.message) {
                    if (_delete) {
                        swal("Succeeded", 'Successfully deleted', "success");
                    } else {
                        swal("Succeeded", 'Saved successfully', "success");
                    }
                    $('#ModalCenter').modal('hide');
                    loadDataTable();
                } else {
                    swal('Error', response.result.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }


    function numberWithCommas(x) {
        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function filterAdjusment(entitas, year, month) {
        loadDataTable(entitas, year, month)
    }

    function reverseNumber(input) {
        return [].map.call(input, function(x) {
            return x;
        }).reverse().join('');
    }

    function plainNumber(number) {
        return number.split('.').join('');
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function splitInDots(input) {

        var value = input.value,
            plain = plainNumber(value),
            reversed = reverseNumber(plain),
            reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
            normal = reverseNumber(reversedWithDots);

        input.value = normal;
    }
</script>