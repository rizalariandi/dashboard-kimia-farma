<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Cash Collection</h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($ent as $e) {
                                                            if ($e == $_SESSION['entitas']) {
                                                                echo "<option value='$e' selected>$e</option>";
                                                            } else {
                                                                echo "<option value='$e'>$e</option>";
                                                            }
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Week : <select id="week" name="week"><?php for ($i = 1; $i <= 5; $i++) {
                                                    if ($i == $_SESSION['week']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        </select>
        <input type='button' value='Filter' onclick="loadDataTable()" class='btn btn-success' />
    </form>
</div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addCashCollection()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Cash Collection</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box no-pt">
        <div class="table-responsive">
            <table id="realisasi-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">datacollection</th>
                        <th scope="col">cakupan</th>
                        <th scope="col">target</th>
                        <th scope="col">real</th>
                        <th scope="col">achievement</th>
                        <th scope="col">Akumulatif Target</th>
                        <th scope="col">Akumulatif Real</th>
                        <th scope="col">Akumulatif Ach</th>
                        <th scope="col">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Cash Collection</h3>
            </div>
            <ul id="tabsCollection" class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id='tab1' href="#menu1">Add by Form</a></li>
                <li><a data-toggle="tab" id='tab2' href="#menu2">Add by Excel</a></li>
            </ul>
            <div class="modal-body">
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                        <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Datacollection<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control datacollection" required>
                                        <option value="">Choose Datacollection</option>
                                        <option value="CASH COLLECTION">CASH COLLECTION</option>
                                        <!-- <option value="CASH COLLECTION RATIO">CASH COLLECTION RATIO</option> -->
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Adjusment is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Cakupan<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control cakupan" required>
                                        <option value="">Choose Cakupan</option>
                                        <option value="INTERNAL">INTERNAL</option>
                                        <option value="EKSTERNAL">EKSTERNAL</option>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Cakupan is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Target<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNT" onclick="$('.btnPNT').text($('.btnPNT').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control target" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Real<span class="text-danger">* :</span></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNR" onclick="$('.btnPNR').text($('.btnPNR').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control real" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Real">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Real is required</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Achievement<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10 ">
                                    <div class="input-group">
                                        <input type="number" class="form-control achievement" required placeholder="Enter Achievement">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Achievement is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Acctarget<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNAT" onclick="$('.btnPNAT').text($('.btnPNAT').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control acctarget" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Acctarget">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Acctarget is required</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Accreal<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNAR" onclick="$('.btnPNAR').text($('.btnPNAR').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control accreal" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Accreal">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Accreal is required</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Accachievement<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10 ">
                                    <div class="input-group">
                                        <input type="number" class="form-control accachievement" required placeholder="Enter Accachievement">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Accachievement is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Entitas<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control entitas" required>
                                        <option value="">Choose Entitas</option>
                                        <option value="KFA">KFA</option>
                                        <option value="KFHO">KFHO</option>
                                        <option value="KFTD">KFTD</option>
                                        <option value="KFSP">KFSP</option>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Entitas is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Week<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control week" required>
                                        <option value="">Choose Week</option>
                                        <?php for ($i = 1; $i <= 5; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Month is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Month<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control month" required>
                                        <option value="">Choose Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Month is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Year<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control year">
                                        <option value="">Choose Year</option>
                                        <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Year is required</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <form>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Upload File</label>
                                <div class="col-sm-3 ">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <!-- <div class="col-sm-1">
                                   
                                </div>                                 -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="InsertDataCashCollection()" class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<style>
    .no-pt {
        padding-top: 0;
    }

    .table-responsive th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }
</style>

<script>
    //$('#realisasi-data').DataTable();
    var data_table, dataUpload = [],
        Id;
    $(document).ready(function() {
        $('input[type="file"]').change(function(e) {
            readURL(e.target);
        });

        data_table = $('#realisasi-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": false,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: -1,
            // dom: 'Bfrtip',
            // buttons: [{
            //     extend: 'excel',
            //     text: "<i class='fas fa-download fa-lg'></i> Download",
            //     className: 'btn-table',
            //     exportOptions: {
            //         modifier: {
            //             search: 'applied',
            //             order: 'applied'
            //         }
            //     }
            // }],
            "columns": [{
                    data: 'datacollection',
                    // title: 'Vendor'
                    className: 'text-left',
                },
                {
                    data: 'cakupan',
                    // title: 'Profit Center',
                    className: 'text-left',
                },
                {
                    data: 'target',
                    // title: 'Text GL',
                    className: 'text-left'
                },
                {
                    data: 'real',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    data: 'achievement',
                    // title: '0 - 30 Days',
                    className: 'text-right',

                },
                {
                    data: 'acctarget',
                    title: 'Akumulatif Target',
                    className: 'text-right',

                },
                {
                    data: 'accreal',
                    title: 'Akumulatif Real',
                    className: 'text-right',
                },
                {
                    data: 'accachievement',
                    title: 'Akumulatif Ach',
                    className: 'text-right',
                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.Id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateCollection(' +
                            (row.Id ? row.Id : "''") +
                            ',' + "'" + (row.datacollection ? row.datacollection : "''") + "'" +
                            ',' + "'" + (row.cakupan ? row.cakupan : "''") + "'" +
                            ',' + "'" + (row.target ? row.target : "''") + "'" +
                            ',' + "'" + (row.real ? row.real : "''") + "'" +
                            ',' + "'" + (row.achievement ? row.achievement : "''") + "'" +
                            ',' + "'" + (row.acctarget ? row.acctarget : "''") + "'" +
                            ',' + "'" + (row.accreal ? row.accreal : "''") + "'" +
                            ',' + "'" + (row.accachievement ? row.accachievement : "''") + "'" +
                            ',' + "'" + (row.entitas ? row.entitas : "''") + "'" +
                            ',' + "'" + (row.week ? row.week : "''") + "'" +
                            ',' + "'" + (row.month ? row.month : "''") + "'" +
                            ',' + "'" + (row.year ? row.year : "''") + "'" + ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteCashCollection(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });

        loadDataTable()
    });


    function loadDataTable() {
        var newRow = [];
        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();
        var entitas = $('#entitas').val();
        $.ajax({
            url: "<?php echo base_url('index.php/import/getCashCollection') ?>",
            method: 'POST',
            data: {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'week': JSON.stringify(week),
            },
            beforeSend: function() {
                // loading();
            },
            success: function(data) {
                newRow = JSON.parse(data)

            }
        }).done(function() {
            if (newRow.length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah').attr('src', e.target.result);
                var url = e.target.result;
                var oReq = new XMLHttpRequest();
                oReq.open("GET", url, true);
                oReq.responseType = "arraybuffer";

                oReq.onload = function(e) {
                    var arraybuffer = oReq.response;

                    /* convert data to binary string */
                    var data = new Uint8Array(arraybuffer);
                    var arr = new Array();
                    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                    var bstr = arr.join("");

                    /* Call XLSX */
                    var workbook = XLSX.read(bstr, {
                        type: "binary"
                    });

                    /* DO SOMETHING WITH workbook HERE */
                    var first_sheet_name = workbook.SheetNames[0];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[first_sheet_name];
                    // console.log(XLSX.utils.sheet_to_json(worksheet, {
                    //     raw: true
                    // }));
                    var datas = XLSX.utils.sheet_to_json(worksheet, {
                        raw: true
                    })
                    datas.forEach(function(element) {
                        element.target = accounting.formatNumber(element.target);
                        element.real = accounting.formatNumber(element.real);
                        element.achievement = (element.achievement * 100).toFixed(2) + '%';
                        element.acctarget = accounting.formatNumber(element.acctarget);
                        element.accreal = accounting.formatNumber(element.accreal);
                        element.accachievement = (element.accachievement * 100).toFixed(2) + '%';
                    });
                    dataUpload = datas


                    console.log('dataUpload', dataUpload)

                }

                oReq.send();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    function loading() {
        swal({
            title: 'Wait a minute...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }


    function deleteCashCollection(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/import/DeleteDataCashCollection') ?>",
                        method: 'POST',
                        data: {
                            'id': id,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            if (response) {
                                swal("Succeeded", 'Successfully deleted', "success");
                                $('#ModalCenter').modal('hide');
                                loadDataTable()
                            }
                        },
                        error: function(response) {
                            var errors = response.responseJSON.errors,
                                name_error = Object.keys(errors)[0].toString().replace('_', ' ');
                            swal(name_error.charAt(0).toUpperCase() + name_error.slice(1), errors[Object.keys(errors)[0]].toString(), "error");
                        }
                    }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function updateCashCollection(element) {
        var respon = false
        if (element.id && element.accachievement && element.accreal && element.acctarget && element.achievement && element.cakupan && element.datacollection && element.entitas && element.month && element.real && element.target && element.week && element.year) {
            respon = true
        }
        if (respon) {
            $.ajax({
                url: "<?php echo base_url('index.php/import/UpdateDataCashCollection') ?>",
                method: 'POST',
                data: {
                    'data': JSON.stringify(element),
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    if (response) {
                        swal("Succeeded", 'Saved successfully', "success");
                        $('#ModalCenter').modal('hide');
                        loadDataTable()
                    }
                },
                error: function(response) {
                    var errors = response.responseJSON.errors,
                        name_error = Object.keys(errors)[0].toString().replace('_', ' ');
                    swal(name_error.charAt(0).toUpperCase() + name_error.slice(1), errors[Object.keys(errors)[0]].toString(), "error");
                }
            }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                // if (jqXHR.status != 422)
                //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
            });
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function InsertDataCashCollection() {
        if (dataUpload.length) {
            var respon = false
            dataUpload.forEach(function(element) {
                if (element.accachievement && element.accreal && element.acctarget && element.achievement && element.cakupan && element.datacollection && element.entitas && element.month && element.real && element.target && element.week && element.year) {
                    respon = true
                }
            });
            if (respon) {
                $.ajax({
                    url: "<?php echo base_url('index.php/import/InsertDataCashCollection') ?>",
                    method: 'POST',
                    data: {
                        'data': JSON.stringify(dataUpload),
                    },
                    beforeSend: function() {
                        loading();
                    },
                    success: function(response) {
                        if (response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenter').modal('hide');
                        }
                    },
                    error: function(response) {
                        var errors = response.responseJSON.errors,
                            name_error = Object.keys(errors)[0].toString().replace('_', ' ');
                        swal(name_error.charAt(0).toUpperCase() + name_error.slice(1), errors[Object.keys(errors)[0]].toString(), "error");
                    }
                }).done(function() {
                    loadDataTable()
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    // if (jqXHR.status != 422)
                    //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                });
            } else {
                swal("Failed", "Please upload the correct file", "error");
            }
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function addCashCollection() {
        dataUpload = []
        Id = null
        $('#exampleFormControlFile1').val('');
        $('#ModalLabel').text('Add Cash Collection');
        $('#tabsCollection').show();
        $("#tab1").trigger("click");
        $(".help-block").hide();
        $('.datacollection').val('');
        $('.cakupan').val('');
        $('.target').val('');
        $('.real').val('');
        $('.achievement').val('');
        $('.acctarget').val('');
        $('.accreal').val('');
        $('.accachievement').val('');
        $('.entitas').val('');
        $('.week').val('');
        $('.month').val('');
        $('.year').val('');
        $('.btnPNT').text('+');
        $('.btnPNR').text('+');
        $('.btnPNAT').text('+');
        $('.btnPNAR').text('+');
        $('.post').text('Save');
    }

    function updateCollection(id, datacollection, cakupan, target, real, achievement, acctarget, accreal, accachievement, entitas, week, month, year) {
        $("#tab1").trigger("click");
        var minusT = target.toString().includes("-") ? '-' : '+';
        var minusR = real.toString().includes("-") ? '-' : '+';
        var minusAT = acctarget.toString().includes("-") ? '-' : '+';
        var minusAR = accreal.toString().includes("-") ? '-' : '+';
        $('.btnPNT').text(minusT);
        $('.btnPNR').text(minusR);
        $('.btnPNAT').text(minusAT);
        $('.btnPNAR').text(minusAR);
        $('#tabsCollection').hide();
        $('#ModalLabel').text('Update Cash Collection');
        Id = id;
        $(".help-block").hide();
        $('.datacollection').val(datacollection);
        $('.cakupan').val(cakupan);
        $('.target').val(target.replace(/[,*+?^${}()|[\]\\]/g, ""));
        $('.real').val(real.replace(/[,*+?^${}()|[\]\\]/g, ""));
        $('.achievement').val(parseFloat(achievement.replace(',', ".")));
        $('.acctarget').val(acctarget.replace(/[,*+?^${}()|[\]\\]/g, ""));
        $('.accreal').val(accreal.replace(/[,*+?^${}()|[\]\\]/g, ""));
        $('.accachievement').val(parseFloat(accachievement.replace(',', ".")));
        $('.entitas').val(entitas);
        $('.week').val(week);
        $('.month').val(month);
        $('.year').val(year);
        $('.post').text('Update');
    }

    function saveData() {
        if ($('.datacollection').val() && $('.cakupan').val() &&
            $('.target').val() && $('.real').val() &&
            $('.achievement').val() && $('.acctarget').val() &&
            $('.accreal').val() && $('.accachievement').val() &&
            $('.entitas').val() && $('.week').val() &&
            $('.month').val() && $('.year').val()) {
            if ($('.post').text() == 'Save') {
                dataUpload = [{
                    accachievement: parseFloat($('.accachievement').val()).toFixed(2) + '%',
                    accreal: $('.btnPNAR').text()=="-" && $('.accreal').val() ?  '-' : '' + accounting.formatNumber($('.accreal').val()),
                    acctarget: $('.btnPNAT').text()=="-" && $('.acctarget').val() ?  '-' : '' + accounting.formatNumber($('.acctarget').val()),
                    achievement: parseFloat($('.achievement').val()).toFixed(2) + '%',
                    cakupan: $('.cakupan').val(),
                    datacollection: $('.datacollection').val(),
                    entitas: $('.entitas').val(),
                    month: $('.month').val(),
                    real: $('.btnPNT').text()=="-" && $('.real').val() ? '-' : ''  + accounting.formatNumber($('.real').val()),
                    target: $('.btnPNR').text()=="-" && $('.target').val() ? '-' : '' + accounting.formatNumber($('.target').val()),
                    week: $('.week').val(),
                    year: $('.year').val()
                }]
                InsertDataCashCollection()
            } else {
                updateCashCollection({
                    id: Id,
                    accachievement: parseFloat($('.accachievement').val()).toFixed(2) + '%',
                    accreal: $('.btnPNAR').text()=="-" && $('.accreal').val() ?  '-' : '' + accounting.formatNumber($('.accreal').val()),
                    acctarget: $('.btnPNAT').text()=="-" && $('.acctarget').val() ?  '-' : '' + accounting.formatNumber($('.acctarget').val()),
                    achievement: parseFloat($('.achievement').val()).toFixed(2) + '%',
                    cakupan: $('.cakupan').val(),
                    datacollection: $('.datacollection').val(),
                    entitas: $('.entitas').val(),
                    month: $('.month').val(),
                    real: $('.btnPNT').text()=="-" && $('.real').val() ? '-' : '' + accounting.formatNumber($('.real').val()),
                    target: $('.btnPNR').text()=="-" && $('.target').val() ? '-' : '' + accounting.formatNumber($('.target').val()),
                    week: $('.week').val(),
                    year: $('.year').val()
                })
            }
        } else {
            $(".help-block").show();
        }
    }

    function reverseNumber(input) {
        return [].map.call(input, function(x) {
            return x;
        }).reverse().join('');
    }

    function plainNumber(number) {
        return number.split('.').join('');
    }

    function splitInDots(input) {

        var value = input.value,
            plain = plainNumber(value),
            reversed = reverseNumber(plain),
            reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
            normal = reverseNumber(reversedWithDots);

        // console.log(plain, reversed, reversedWithDots, normal);
        input.value = normal;
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>