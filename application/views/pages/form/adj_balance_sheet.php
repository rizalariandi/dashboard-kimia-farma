<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Adjustment Balance Sheet</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addAdjusment()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Adjustment</button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right pt-10">
            Entitas : <select class="entitasF" name="entitasF">
                <option value="">Choose Entitas</option>
                <option value="KFHO">KFHO</option>
                <option value="KFA">KFA</option>
                <option value="KFTD">KFTD</option>
                <option value="KFSP">KFSP</option>
            </select>
            Year : <select class="yearF" name="year">
                <option value="">Choose Year</option>
                <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                    echo "<option value='$i'>$i</option>";
                } ?>
            </select>
            Month : <select class="monthF" name="month">
                <option value="">Choose Month</option>
                <?php for ($i = 1; $i <= 12; $i++) {
                    echo "<option value='$i'>$i</option>";
                } ?>
            </select>
            <input type="button" onclick="filterAdjusment($('.entitasF').val(),$('.yearF').val(),$('.monthF').val())" value="Filter" class="btn btn-success">
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">
        <div class="table-responsive">
            <table id="adjusment-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th class="col-lg-1 col-sm-1">No</th>
                        <th class="col-lg-1 col-sm-1">month</th>
                        <th class="col-lg-1 col-sm-1">entitas</th>
                        <th class="col-lg-2 col-sm-2">uraian</th>
                        <th class="col-lg-1 col-sm-1">target</th>
                        <th class="col-lg-1 col-sm-1">real</th>
                        <!-- <th class="col">f01</th> -->
                        <th class="col-lg-1 col-sm-1">adjustment</th>
                        <th class="col-lg-1 col-sm-1">year</th>
                        <th class="col-lg-2 col-sm-2">last update</th>
                        <th class="col-lg-1 col-sm-1">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Adjustment Balance Sheet</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Entitas :</label>
                        <div class="col-sm-10">
                            <select class="form-control entitas">
                                <option value="">Choose Entitas</option>
                                <option value='KFHO'>KFHO</option>
                                <option value='KFA'>KFA</option>
                                <option value='KFTD'>KFTD</option>
                                <option value='KFSP'>KFSP</option>
                                <option value='DAWAA'>DAWAA</option>
                                <option value='SIL'>SIL</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Month <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select class="form-control month" required>
                                <option value="">Choose Month</option>
                                <?php for ($i = 1; $i <= 12; $i++) {
                                    echo "<option value='$i'>$i</option>";
                                } ?>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Month is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Year :</label>
                        <div class="col-sm-10">
                            <select class="form-control year">
                                <option value="">Choose Year</option>
                                <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                    echo "<option value='$i'>$i</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Uraian :</label>
                        <div class="col-sm-10">
                        <select class="form-control uraian">
                        <option value="">Choose Entitas</option>
                                <?php

                                foreach ($uraian_bs as $key => $value) {
                                    echo '<optgroup label="' . $value['title'] . '">';
                                    foreach ($value['data'] as $key => $value2) {
                                        echo '<option value="' . $value2 . '">' . $value2 . '</option>';                                        
                                    };
                                    '</optgroup>';
                                }
                                ?>   
                            </select>                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Target <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnPNT" onclick="$('.btnPNT').text($('.btnPNT').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control target" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                            </div>                            
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-sm-2">Real:</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control real" placeholder="Enter Real">
                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                        <label class="control-label col-sm-2">F01:</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control f01" placeholder="Enter F01">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-sm-2">Adjusment <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btnPNA" onclick="$('.btnPNA').text($('.btnPNA').text() === '-'? '+':'-')" type="button">+</button>
                                </span>
                                <input type="text" style="font-weight: bold;" class="form-control adjusment" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Adjusment is required</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group lastupdate">
                        <label class="control-label col-sm-2">Last Update :<span class="text-danger"></span></label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control last_update" required placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    //$('#realisasi-data').DataTable();
    var Id
    $(".help-block").hide();
    var data_table;
    $(document).ready(function() {
        data_table = $('#adjusment-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,

            "columns": [{
                    data: 'Id',
                    render: function(data, type, row) {
                        return (i = i + 1);
                    }
                },
                {
                    data: 'month',
                    title: 'Month'
                },
                {
                    data: 'entitas',
                    title: 'Entitas',
                    className: 'text-left',
                },
                {
                    data: 'uraian',
                    title: 'Uraian',
                    className: 'text-left'
                },
                {
                    data: 'target',
                    title: 'Target',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'real',
                    title: 'Real',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                // {
                //     data: 'f01',
                //     title: 'F01',
                //     className: 'text-right',
                //     render: $.fn.dataTable.render.number(",", ".", 0)
                // },
                {
                    data: 'adjusment',
                    title: 'Adjusment',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number(",", ".", 0)
                },
                {
                    data: 'year'
                },
                {
                    title: 'Last Update',
                    data: 'last_update'
                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.Id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateAdjusment(' +
                            (row.Id ? row.Id : "''") +
                            ',' + (row.adjusment ? row.adjusment : "''") +
                            ',' + (row.entitas ? "'" + row.entitas + "'" : "''") +
                            ',' + (row.f01 ? row.f01 : "''") +
                            ',' + (row.month ? row.month : "''") +
                            ',' + (row.real ? row.real : "''") +
                            ',' + (row.target ? row.target : "''") +
                            ',' + (row.uraian ? "'" + row.uraian + "'" : "''") +
                            ',' + (row.year ? row.year : "''") +
                            ',' + (row.last_update ? "'" + row.last_update + "'" : "''") +
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteAdjusment(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });
        loadDataTable()
    });

    function loadDataTable(entitas, year, month) {
        var length;
        var newRow = [];
        var iterasi = 0;
        i = 0;
        $.ajax({
            url: "<?php echo base_url('index.php/Adj_balance_sheet/getAdjBS') ?>",
            method: 'get',
            data: {
                'year': year,
                'entitas': entitas,
                'month': month,
            },
            async: false,
            beforeSend: function() {
                $('button[type="submit"]').attr('disabled', true);
            },
            complete: function() {
                $('button[type="submit"]').attr('disabled', false);
            },
            success: function(data) {
                data = JSON.parse(data)
                length = data.length;
                newRow = data
            }
        }).done(function() {
            if (length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        });
    }

    function saveData() {
        // console.log(($('.btnPNA').text() == '-' ? '-' : '') + $('.adjusment').val().replace(/[.]/g, ''))
        // console.log(($('.btnPNT').text() == '-' ? '-' : '') + $('.target').val().replace(/[.]/g, ''))
        if ($('.month').val() && $('.target').val() && $('.adjusment').val()) {
            if ($('.post').text() == 'Save') {
                PostAdjusment(null, $('.btnPNA').text()+$('.adjusment').val().replace(/[.]/g, ''), $('.entitas').val(), $('.f01').val(), $('.month').val(), $('.real').val(), $('.btnPNT').text()+$('.target').val().replace(/[.]/g, ''), $('.uraian').val(), $('.year').val())
            } else {
                PostAdjusment(Id, $('.btnPNA').text()+$('.adjusment').val().replace(/[.]/g, ''), $('.entitas').val(), $('.f01').val(), $('.month').val(), $('.real').val(), $('.btnPNT').text()+$('.target').val().replace(/[.]/g, ''), $('.uraian').val(), $('.year').val())
            }
        } else {
            $(".help-block").show();
        }
    }

    function addAdjusment() {
        $('.lastupdate').hide();
        $('#ModalLabel').text('Add Adjustment Balance Sheet');
        $(".help-block").hide();
        $('.post').text('Save');
        Id = null;
        $('.adjusment').val(undefined);
        $('.entitas').val(undefined);
        $('.f01').val(undefined);
        $('.month').val(undefined);
        $('.real').val(undefined);
        $('.target').val(undefined);
        $('.uraian').val(undefined);
        $('.year').val(undefined);
        $('.btnPNT').text('+');
        $('.btnPNA').text('+');
    }

    function updateAdjusment(id, adjusment, entitas, f01, month, real, target, uraian, year, last_update) {
        var minusA = adjusment.toString().includes("-") ? '-' : '+';
        var minusT = target.toString().includes("-") ? '-' : '+';
        $('.btnPNT').text(minusT);
        $('.btnPNA').text(minusA);
        $('#ModalLabel').text('Update Adjustment Balance Sheet')
        Id = id;
        $(".help-block").hide();
        // $('.adjusment').val(adjusment);
        $('.adjusment').val(adjusment ? adjusment.toFixed(0).toString().replace("-", "") : null)
        $('.entitas').val(entitas);
        $('.f01').val(f01);
        $('.month').val(month);
        $('.real').val(real);
        // $('.target').val(target);
        $('.target').val(target ? target.toFixed(0).toString().replace("-", "") : null)
        $('.uraian').val(uraian);
        $('.year').val(year);
        $('.post').text('Update');
        if (last_update) {
            $('.last_update').val(last_update);
            $('.lastupdate').show()
        } else {
            $('.lastupdate').hide()
        }
    }

    function deleteAdjusment(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    PostAdjusment(id, null, null, null, null, null, null, null, null, true)
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostAdjusment(id, adjusment, entitas, f01, month, real, target, uraian, year, _delete) {
        $.ajax({
            url: "<?php echo base_url('index.php/Adj_balance_sheet/postAdjBS') ?>",
            method: 'post',
            data: {
                'id': id,
                'adjusment': adjusment,
                'entitas': entitas,
                'f01': f01,
                'month': month,
                'real': real,
                'target': target,
                'uraian': uraian,
                'year': year,
                'delete': _delete
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.result.message) {
                    if (_delete) {
                        swal("Succeeded", 'Successfully deleted', "success");
                    } else {
                        swal("Succeeded", 'Saved successfully', "success");
                    }
                    $('#ModalCenter').modal('hide');
                    loadDataTable();
                } else {
                    swal('Error', response.result.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }


    function numberWithCommas(x) {
        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function filterAdjusment(entitas, year, month) {
        loadDataTable(entitas, year, month)
    }

    function reverseNumber(input) {
        return [].map.call(input, function(x) {
            return x;
        }).reverse().join('');
    }

    function plainNumber(number) {
        return number.split('.').join('');
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function splitInDots(input) {

        var value = input.value,
            plain = plainNumber(value),
            reversed = reverseNumber(plain),
            reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
            normal = reverseNumber(reversedWithDots);

        input.value = normal;
    }
</script>