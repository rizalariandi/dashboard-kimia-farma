<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Apl Form</h4>
</div>
<!-- <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                        foreach ($ent as $e) {
                                                            if ($e == $_SESSION['entitas']) {
                                                                echo "<option value='$e' selected>$e</option>";
                                                            } else {
                                                                echo "<option value='$e'>$e</option>";
                                                            }
                                                        } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>     
        </select>
        <input type='button' value='Filter' onclick="loadDataTable()" class='btn btn-success' />
    </form>
</div> -->
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addCashCollectionAPL()" data-toggle="modal" data-target="#ModalCenterAPL">
                <i class="fa fa-plus"></i>
                Add</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box no-pt">
        <div class="table-responsive">
            <table id="apl-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ZP code</th>
                        <th scope="col">SAP</th>
                        <th scope="col">Customer name</th>
                        <th scope="col">Customer Code</th>
                        <th scope="col">Costomer Type</th>
                        <th scope="col">Item Name</th>
                        <th scope="col">Chanel</th>
                        <th scope="col">Lini</th>
                        <th scope="col">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
<p>&nbsp;</p>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addCC()" data-toggle="modal" data-target="#ModalCenterCode">
                <i class="fa fa-plus"></i>
                Add</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box no-pt">
        <div class="table-responsive">
            <table id="kode-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Customer code </th>
                        <th scope="col">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ModalCenterAPL" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabelAPL">Add APL</h3>
            </div>
            <ul id="tabsCollection" class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id='tab1' href="#menu1">Add by Form</a></li>
                <li><a data-toggle="tab" id='tab2' href="#menu2">Add by Excel</a></li>
            </ul>
            <div class="modal-body">
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                        <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Pricipal Year<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control principal_year" required>
                                        <option value="">Choose Year</option>
                                        <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Pricipal Year is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">No Period<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control period_no" required placeholder="No Period">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The No Period is required</small>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-sm-2">Principal<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control principal" required placeholder="Enter Principal">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Principal is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">business division 1<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control business_division_1" required placeholder="Enter business division 1">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The business division 1 is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">business division 2<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control business_division_2" required placeholder="Enter business division 2">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The business division 2 is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">No Invoice<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control invoice_no" required placeholder="Enter No Invoice">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The No Invoice is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">ZP Code<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control zp_item_code" required placeholder="Enter ZP Code">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The ZP Code is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">SAP<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control sap" required placeholder="Enter SAP">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The SAP is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Item Name<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control item_name" required placeholder="Enter Item Name">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Item Name is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Lini<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control lini" required placeholder="Enter Lini">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Lini is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Branch<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control branch" required placeholder="Enter Branch">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The branch is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Cust group 1 code<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control cust_group_1_code" required placeholder="Enter Cust group 1 code">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Cust group 1 code is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Channel<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control channel" required placeholder="Enter Channel">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The channel is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Customer type<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control customer_type" required placeholder="Enter Customer type">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Customer type is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Customer code<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control customer_code" required placeholder="Enter Customer code">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The customer code is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Customer name<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control customer_name" required placeholder="Enter Customer name">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The customer name is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">List price a<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control list_price_a" required placeholder="Enter List price a">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The List Price A code is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Sales Unit<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control sales_unit" required placeholder="Enter Sales Unit">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Sales Unit is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Net Sales<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control net_sales" required placeholder="Enter Sales Unit">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Net Sales is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Discount Principal<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control discount_principal" required placeholder="Enter Discount Principal">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Discount Principal is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Sales Value<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control sales_value" required placeholder="Enter Sales Value">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Sales Value is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Bonus Value<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control bonus_value" required placeholder="Enter Bonus Value">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Bonus Value is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Bonus Unit<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control bonus_unit" required placeholder="Enter Bonus Unit">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Bonus Unit is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Transaction Date<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control transaction_date" required placeholder="Enter Transaction Date">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Transaction Date is required</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary post" onclick="saveDataAPL()">Save </button>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <form>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Upload File</label>
                                <div class="col-sm-3 ">
                                    <input type="file" onclick="loadDataTable()" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <!-- <div class="col-sm-1">
                                   
                                </div>                                 -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="InsertDataCashCollection()" class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenterCode" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabelCC">Add Customer Code</h3>
            </div>
            <ul id="tabsCollectionCC" class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id='tab1cc' href="#menucode1">Add by Form</a></li>
                <li><a data-toggle="tab" id='tab2cc' href="#menucode2">Add by Excel</a></li>
            </ul>
            <div class="modal-body">
                <div class="tab-content">
                    <div id="menucode1" class="tab-pane fade in active">
                        <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                            <div class="form-group">
                                <label class="control-label col-sm-2">customer code<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control code_customer" required placeholder="Enter Realisasi">
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary postCC" onclick="saveDataCC()">Save</button>
                            </div>
                        </form>
                    </div>
                    <div id="menucode2" class="tab-pane fade">
                        <form>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Upload File</label>
                                <div class="col-sm-3 ">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <!-- <div class="col-sm-1">
                                   
                                </div>                                 -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="InsertDataCodeCustomer()" class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<style>
    .no-pt {
        padding-top: 0;
    }

    .thead-dark th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }
</style>

<script>
    //$('#realisasi-data').DataTable();
    var data_table, data_table2, dataUpload = [],
        Id;
    $(document).ready(function() {
        $('input[type="file"]').change(function(e) {
            readURL(e.target);
        });

        data_table = $('#apl-data').DataTable({
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,
            "columns": [{
                    data: 'zp_item_code',
                    // title: 'Vendor'                    
                    className: 'text-left',
                },
                {
                    data: 'sap',
                    // title: 'Profit Center',
                    className: 'text-left',
                },
                {
                    data: 'customer_name',
                    // title: 'Text GL',
                    render: $.fn.dataTable.render.number(",", ".", 0),
                    className: 'text-left'
                },
                {
                    data: 'customer_code',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    data: 'customer_type',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    data: 'item_name',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    data: 'channel',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    data: 'lini',
                    // title: 'Total',
                    className: 'text-right',

                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenterAPL" onclick="updateDataAPL(' +
                            (row.id ? "'" + row.id + "'" : "''") +
                            ',' + (row.principal_year ? "'" + row.principal_year + "'" : "''") +
                            ',' + (row.period_no ? "'" + row.period_no + "'" : "''") +
                            ',' + (row.principal ? "'" + row.principal + "'" : "''") +
                            ',' + (row.business_division_1 ? "'" + row.business_division_1 + "'" : "''") +
                            ',' + (row.business_division_2 ? "'" + row.business_division_2 + "'" : "''") +
                            ',' + (row.invoice_no ? "'" + row.invoice_no + "'" : "''") +
                            ',' + (row.zp_item_code ? "'" + row.zp_item_code + "'" : "''") +
                            ',' + (row.sap ? "'" + row.sap + "'" : "''") +
                            ',' + (row.item_name ? "'" + row.item_name + "'" : "''") +
                            ',' + (row.lini ? "'" + row.lini + "'" : "''") +
                            ',' + (row.branch ? "'" + row.branch + "'" : "''") +
                            ',' + (row.cust_group_1_code ? "'" + row.cust_group_1_code + "'" : "''") +
                            ',' + (row.channel ? "'" + row.channel + "'" : "''") +
                            ',' + (row.customer_type ? "'" + row.customer_type + "'" : "''") +
                            ',' + (row.customer_code ? "'" + row.customer_code + "'" : "''") +
                            ',' + (row.customer_name ? "'" + row.customer_name + "'" : "''") +
                            ',' + (row.list_price_a ? "'" + row.list_price_a + "'" : "''") +
                            ',' + (row.sales_unit ? "'" + row.sales_unit + "'" : "''") +
                            ',' + (row.net_sales ? "'" + row.net_sales + "'" : "''") +
                            ',' + (row.discount_principal ? "'" + row.discount_principal + "'" : "''") +
                            ',' + (row.sales_value ? "'" + row.sales_value + "'" : "''") +
                            ',' + (row.bonus_value ? "'" + row.bonus_value + "'" : "''") +
                            ',' + (row.bonus_unit ? "'" + row.bonus_unit + "'" : "''") +
                            ',' + (row.transaction_date ? "'" + row.transaction_date + "'" : "''") +
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '<br> <button  class="btn btn-sm btn-danger" onclick="deleteAPL(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });


        data_table2 = $('#kode-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,
            "columns": [{
                    data: 'kode_customer',
                    // title: 'Vendor'                    
                    className: 'text-center',
                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenterCode" onclick="updateCC(' + "'" + id + "'" +
                            ',' + (row.kode_customer ? "'" + row.kode_customer + "'" : "''") + ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp;<button  class="btn btn-sm btn-danger" onclick="deleteCC(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });

        loadDataTable()
    });


    function loadDataTable() {
        var newRow = [];
        // var year = $('#year').val();
        // var month = $('#month').val();
        // var week = $('#week').val();
        // var entitas = $('#entitas').val();
        $.ajax({
            url: "<?php echo base_url('index.php/Import_apl/getDataAPL') ?>",
            method: 'POST',
            // data: {
            //     'month': JSON.stringify(month),
            //     'year': JSON.stringify(year),
            //     'entitas': JSON.stringify(entitas),
            //     'week': JSON.stringify(week),
            // },
            beforeSend: function() {
                // loading();
            },
            success: function(data) {
                newRow = JSON.parse(data);
            }
        }).done(function() {
            if (newRow.length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow.apl);
                data_table.columns.adjust().draw();
                data_table2.clear().draw();
                data_table2.rows.add(newRow.kode);
                data_table2.columns.adjust().draw();
            } else {
                data_table.clear().draw();
                data_table2.clear().draw();
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah').attr('src', e.target.result);
                var url = e.target.result;
                var oReq = new XMLHttpRequest();
                oReq.open("GET", url, true);
                oReq.responseType = "arraybuffer";

                oReq.onload = function(e) {
                    var arraybuffer = oReq.response;

                    /* convert data to binary string */
                    var data = new Uint8Array(arraybuffer);
                    var arr = new Array();
                    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                    var bstr = arr.join("");

                    /* Call XLSX */
                    var workbook = XLSX.read(bstr, {
                        type: "binary"
                    });

                    /* DO SOMETHING WITH workbook HERE */
                    var first_sheet_name = workbook.SheetNames[0];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[first_sheet_name];
                    // console.log(XLSX.utils.sheet_to_json(worksheet, {
                    //     raw: true
                    // }));
                    var datas = XLSX.utils.sheet_to_json(worksheet, {
                        raw: true
                    })
                    datas.forEach(function(element) {
                        element.achievement = (element.achievement * 100).toFixed(2) + '%';
                    });

                    dataUpload = datas
                    console.log('dataUpload', dataUpload)

                }

                oReq.send();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    function loading() {
        swal({
            title: 'Wait a minute...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function deleteCC(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/Import_apl/DeleteKodeCustomer') ?>",
                        method: 'POST',
                        data: {
                            'id': id,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            if (response) {
                                swal("Succeeded", 'Successfully deleted', "success");
                                $('#ModalCenterCode').modal('hide');
                                loadDataTable()
                            }
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }


    function updateCodeCustomer(element) {
        var respon = false
        if (element.kode_customer) {
            respon = true
        }
        if (respon) {
            $.ajax({
                url: "<?php echo base_url('index.php/Import_apl/UpdateKodeCustomer') ?>",
                method: 'POST',
                data: {
                    'data': JSON.stringify(element),
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    if (response) {
                        swal("Succeeded", 'Saved successfully', "success");
                        $('#ModalCenterCode').modal('hide');
                        loadDataTable()
                    }
                },
                error: function(response) {
                    swal(response.status, response.statusText, "error");
                }
            }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                // if (jqXHR.status != 422)
                //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
            });
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function InsertDataCodeCustomer() {
        if (dataUpload.length) {
            var respon = false
            dataUpload.forEach(function(element) {
                if (element.kode_customer) {
                    respon = true
                }
            });
            if (respon) {
                $.ajax({
                    url: "<?php echo base_url('index.php/Import_apl/InsertDataKodeCustomer') ?>",
                    method: 'POST',
                    data: {
                        'data': JSON.stringify(dataUpload),
                    },
                    beforeSend: function() {
                        loading();
                    },
                    success: function(response) {
                        if (response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenterCode').modal('hide');
                            loadDataTable()
                        }
                    },
                    error: function(response) {
                        swal(response.status, response.statusText, "error");
                    }
                }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                    // if (jqXHR.status != 422)
                    //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                });
            } else {
                swal("Failed", "Please upload the correct file", "error");
            }
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }


    function deleteAPL(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/Import_apl/DeleteAPL') ?>",
                        method: 'POST',
                        data: {
                            'id': id,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            if (response) {
                                swal("Succeeded", 'Successfully deleted', "success");
                                $('#ModalCenterAPL').modal('hide');
                                loadDataTable()
                            }
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function updateDataRowAPL(element) {
        var respon = false
        if (element.id && element.principal_year && element.period_no && element.principal && element.business_division_1 && element.business_division_2 && element.invoice_no && element.zp_item_code && element.sap && element.item_name && element.lini && element.branch && element.cust_group_1_code && element.channel && element.customer_type && element.customer_code && element.customer_name && element.list_price_a && element.sales_unit && element.net_sales && element.discount_principal && element.sales_value && element.bonus_value && element.bonus_unit && element.transaction_date) {
            respon = true
        }
        if (respon) {
            $.ajax({
                url: "<?php echo base_url('index.php/Import_apl/UpdateDataAPL') ?>",
                method: 'POST',
                data: {
                    'data': JSON.stringify(element),
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    if (response) {
                        swal("Succeeded", 'Saved successfully', "success");
                        $('#ModalCenterAPL').modal('hide');
                        loadDataTable()
                    }
                },
                error: function(response) {
                    swal(response.status, response.statusText, "error");
                }
            }).done(function() {}).fail(function(jqXHR, textStatus, errorThrown) {
                // if (jqXHR.status != 422)
                //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
            });
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function InsertDataAPL() {
        if (dataUpload.length) {
            var respon = false
            dataUpload.forEach(function(element) {
                if (element.principal_year && element.period_no && element.principal && element.business_division_1 && element.business_division_2 && element.invoice_no && element.zp_item_code && element.sap && element.item_name && element.lini && element.branch && element.cust_group_1_code && element.channel && element.customer_type && element.customer_code && element.customer_name && element.list_price_a && element.sales_unit && element.net_sales && element.discount_principal && element.sales_value && element.bonus_value && element.bonus_unit && element.transaction_date) {
                    respon = true
                }
            });
            if (respon) {
                $.ajax({
                    url: "<?php echo base_url('index.php/Import_apl/InsertDataAPL') ?>",
                    method: 'POST',
                    data: {
                        'data': JSON.stringify(dataUpload),
                    },
                    beforeSend: function() {
                        loading();
                    },
                    success: function(response) {
                        if (response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenterAPL').modal('hide');
                        }
                    },
                    error: function(response) {
                        swal(response.status, response.statusText, "error");
                    }
                }).done(function() {
                    loadDataTable()
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    // if (jqXHR.status != 422)
                    //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                });
            } else {
                swal("Failed", "Please upload the correct file", "error");
            }
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }


    function addCC() {
        dataUpload = []
        Id = null
        $('#exampleFormControlFile1').val('');
        $('#ModalLabelCC').text('Add Customer Code');
        $('#tabsCollectionCC').show();
        $("#tab1cc").trigger("click");
        $(".help-block").hide();
        $('.code_customer').val('');
        $('.postCC').text('Save');
    }

    function updateCC(id, code) {
        dataUpload = []
        $('#exampleFormControlFile1').val('');
        $("#tab1cc").trigger("click");
        $('#tabsCollectionCC').hide();
        $('#ModalLabelCC').text('Update Customer Code');
        Id = id;
        $(".help-block").hide();
        $('.code_customer').val(code);
        $('.postCC').text('Update');
    }

    function addCashCollectionAPL() {
        dataUpload = []
        Id = null
        $('#exampleFormControlFile1').val('');
        $('#ModalLabelAPL').text('Add APL');
        $('#tabsCollection').show();
        $("#tab1").trigger("click");
        $(".help-block").hide();
        $('.principal_year').val('');
        $('.period_no').val('');
        $('.principal').val('');
        $('.business_division_1').val('');
        $('.business_division_2').val('');
        $('.invoice_no').val('');
        $('.zp_item_code').val('');
        $('.sap').val('');
        $('.item_name').val('');
        $('.lini').val('');
        $('.branch').val('');
        $('.cust_group_1_code').val('');
        $('.channel').val('');
        $('.customer_type').val('');
        $('.customer_code').val('');
        $('.customer_name').val('');
        $('.list_price_a').val('');
        $('.sales_unit').val('');
        $('.net_sales').val('');
        $('.discount_principal').val('');
        $('.sales_value').val('');
        $('.bonus_value').val('');
        $('.bonus_unit').val('');
        $('.transaction_date').val('');
        $('.post').text('Save');
    }

    function updateDataAPL(id, principal_year, period_no, principal, business_division_1, business_division_2, invoice_no, zp_item_code, sap, item_name, lini, branch, cust_group_1_code, channel, customer_type, customer_code, customer_name, list_price_a, sales_unit, net_sales, discount_principal, sales_value, bonus_value, bonus_unit, transaction_date) {
        $("#tab1").trigger("click");
        $('#tabsCollection').hide();
        $('#ModalLabelAPL').text('Update APL');
        Id = id;
        $(".help-block").hide();
        $('.principal_year').val(principal_year);
        $('.period_no').val(period_no);
        $('.principal').val(principal);
        $('.business_division_1').val(business_division_1);
        $('.business_division_2').val(business_division_2);
        $('.invoice_no').val(invoice_no);
        $('.zp_item_code').val(zp_item_code);
        $('.sap').val(sap);
        $('.item_name').val(item_name);
        $('.lini').val(lini);
        $('.branch').val(branch);
        $('.cust_group_1_code').val(cust_group_1_code);
        $('.channel').val(channel);
        $('.customer_type').val(customer_type);
        $('.customer_code').val(customer_code);
        $('.customer_name').val(customer_name);
        $('.list_price_a').val(list_price_a);
        $('.sales_unit').val(sales_unit);
        $('.net_sales').val(net_sales);
        $('.discount_principal').val(discount_principal);
        $('.sales_value').val(sales_value);
        $('.bonus_value').val(bonus_value);
        $('.bonus_unit').val(bonus_unit);
        $('.transaction_date').val(transaction_date);
        $('.post').text('Update');
    }

    function saveDataAPL() {

        if ($('.principal_year').val() && $('.period_no').val() && $('.principal').val() && $('.business_division_1').val() && $('.business_division_2').val() && $('.invoice_no').val() &&
            $('.zp_item_code').val() && $('.sap').val() && $('.item_name').val() && $('.lini').val() && $('.branch').val() && $('.cust_group_1_code').val() &&
            $('.channel').val() && $('.customer_type').val() && $('.customer_code').val() && $('.customer_name').val() && $('.list_price_a').val() && $('.sales_unit').val() &&
            $('.net_sales').val() && $('.discount_principal').val() && $('.sales_value').val() && $('.bonus_value').val() && $('.bonus_unit').val() &&
            $('.transaction_date').val()) {
            if ($('.post').text() == 'Save') {
                dataUpload = [{
                    principal_year: $('.principal_year').val(),
                    period_no: $('.period_no').val(),
                    principal: $('.principal').val(),
                    business_division_1: $('.business_division_1').val(),
                    business_division_2: $('.business_division_2').val(),
                    invoice_no: $('.invoice_no').val(),
                    zp_item_code: $('.zp_item_code').val(),
                    sap: $('.sap').val(),
                    item_name: $('.item_name').val(),
                    lini: $('.lini').val(),
                    branch: $('.branch').val(),
                    cust_group_1_code: $('.cust_group_1_code').val(),
                    channel: $('.channel').val(),
                    customer_type: $('.customer_type').val(),
                    customer_code: $('.customer_code').val(),
                    customer_name: $('.customer_name').val(),
                    list_price_a: $('.list_price_a').val(),
                    sales_unit: $('.sales_unit').val(),
                    net_sales: $('.net_sales').val(),
                    discount_principal: $('.discount_principal').val(),
                    sales_value: $('.sales_value').val(),
                    bonus_value: $('.bonus_value').val(),
                    bonus_unit: $('.bonus_unit').val(),
                    transaction_date: $('.transaction_date').val(),
                }]
                InsertDataAPL()
            } else {
                updateDataRowAPL({
                    id: Id,
                    principal_year: $('.principal_year').val(),
                    period_no: $('.period_no').val(),
                    principal: $('.principal').val(),
                    business_division_1: $('.business_division_1').val(),
                    business_division_2: $('.business_division_2').val(),
                    invoice_no: $('.invoice_no').val(),
                    zp_item_code: $('.zp_item_code').val(),
                    sap: $('.sap').val(),
                    item_name: $('.item_name').val(),
                    lini: $('.lini').val(),
                    branch: $('.branch').val(),
                    cust_group_1_code: $('.cust_group_1_code').val(),
                    channel: $('.channel').val(),
                    customer_type: $('.customer_type').val(),
                    customer_code: $('.customer_code').val(),
                    customer_name: $('.customer_name').val(),
                    list_price_a: $('.list_price_a').val(),
                    sales_unit: $('.sales_unit').val(),
                    net_sales: $('.net_sales').val(),
                    discount_principal: $('.discount_principal').val(),
                    sales_value: $('.sales_value').val(),
                    bonus_value: $('.bonus_value').val(),
                    bonus_unit: $('.bonus_unit').val(),
                    transaction_date: $('.transaction_date').val(),
                })
            }
        } else {
            $(".help-block").show();
        }
    }

    function saveDataCC() {
        if ($('.code_customer').val()) {
            if ($('.postCC').text() == 'Save') {
                dataUpload = [{
                    kode_customer: $('.code_customer').val()
                }]
                InsertDataCodeCustomer()
            } else {
                updateCodeCustomer({
                    id: Id,
                    kode_customer: $('.code_customer').val()
                })
            }
        } else {
            $(".help-block").show();
        }
    }
</script>