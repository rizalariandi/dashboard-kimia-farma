<?php
$_SESSION['year'] = date("Y");
$_SESSION['month'] =  date("n");
?>

<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Realisasi Investasi</h4>
</div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Entitas : <select id="entitas" name="entitas"><?php $ent = ['KFHO', 'KFA', 'KFTD', 'KFSP'];
                                                    foreach ($ent as $e) {
                                                        if ($e == $_SESSION['entitas']) {
                                                            echo "<option value='$e' selected>$e</option>";
                                                        } else {
                                                            echo "<option value='$e'>$e</option>";
                                                        }
                                                    } ?></select>
        Year : <select id="year" name="year"><?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                                    if ($i == $_SESSION['year']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>
        Month : <select id="month" name="month"><?php for ($i = 1; $i <= 12; $i++) {
                                                    if ($i == $_SESSION['month']) {
                                                        echo "<option value='$i' selected>$i</option>";
                                                    } else {
                                                        echo "<option value='$i'>$i</option>";
                                                    }
                                                } ?></select>     
        </select>
        <input type='button' value='Filter' onclick="loadDataTable()" class='btn btn-success' />
    </form>
</div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
        </div>
    </div>
</div>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addCashCollection()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Realisasi Investasi</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box no-pt">
        <div class="table-responsive">
            <table id="realisasi-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">uraian</th>                                            
                        <th scope="col">target</th>
                        <th scope="col">realisasi</th>
                        <th scope="col">achievement</th>
                        <th scope="col">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Realisasi Investasi</h3>
            </div>
            <ul id="tabsCollection" class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id='tab1' href="#menu1">Add by Form</a></li>
                <li><a data-toggle="tab" id='tab2' href="#menu2">Add by Excel</a></li>
            </ul>
            <div class="modal-body">
                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                        <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Uraian<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control uraian" required>
                                        <option value="">Choose Datacollection</option>
                                        <option value="TANAH/BANGUNAN">TANAH/BANGUNAN</option>
                                        <option value="MESIN">MESIN</option>
                                        <option value="KENDARAAN">KENDARAAN</option>
                                        <option value="INVENTARIS">INVENTARIS</option>
                                        <option value="KSO">KSO</option>
                                        <option value="PROYEK">PROYEK</option>
                                        <option value="ORGANIK">ORGANIK</option>
                                        <option value="ANORGANIK">ANORGANIK</option>                                        
                                        <!-- <option value="CASH COLLECTION RATIO">CASH COLLECTION RATIO</option> -->
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Adjusment is required</small>
                                </div>
                            </div>                           
                            <div class="form-group">
                                <label class="control-label col-sm-2">Target<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNT" onclick="$('.btnPNT').text($('.btnPNT').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control target" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Target">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Target is required</small>
                                    </div>                                    
                                </div>
                            </div>   
                            <div class="form-group">
                                <label class="control-label col-sm-2">Realisasi<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btnPNR" onclick="$('.btnPNR').text($('.btnPNR').text() === '-'? '+':'-')" type="button">+</button>
                                        </span>
                                        <input type="text" style="font-weight: bold;" class="form-control realisasi" onkeyup="splitInDots(this)" onkeypress="return isNumberKey(event)" required placeholder="Enter Realisasi">
                                        <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Realisasi is required</small>
                                    </div>                                    
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="control-label col-sm-2">Achievement<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10 ">
                                    <div class="input-group">
                                        <input type="number" class="form-control achievement" required placeholder="Enter Achievement">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Achievement is required</small>
                                </div>
                            </div>                                                   
                            <div class="form-group">
                                <label class="control-label col-sm-2">Entitas<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control entitas" required>
                                        <option value="">Choose Entitas</option>
                                        <option value="KFA">KFA</option>
                                        <option value="KFHO">KFHO</option>
                                        <option value="KFTD">KFTD</option>
                                        <option value="KFSP">KFSP</option>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Entitas is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Month<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control month" required>
                                        <option value="">Choose Month</option>
                                        <?php for ($i = 1; $i <= 12; $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Month is required</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Year<span class="text-danger">*</span> :</label>
                                <div class="col-sm-10">
                                    <select class="form-control year">
                                        <option value="">Choose Year</option>
                                        <?php for ($i = date("Y") - 5; $i <= date("Y"); $i++) {
                                            echo "<option value='$i'>$i</option>";
                                        } ?>
                                    </select>
                                    <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Year is required</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <form>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Upload File</label>
                                <div class="col-sm-3 ">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                                <!-- <div class="col-sm-1">
                                   
                                </div>                                 -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="InsertDataCashCollection()" class="btn btn-primary">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<style>
    .no-pt {
        padding-top: 0;
    }

    .thead-dark th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }
</style>

<script>
    //$('#realisasi-data').DataTable();
    var data_table, dataUpload = [],
        Id;
    $(document).ready(function() {
        $('input[type="file"]').change(function(e) {
            readURL(e.target);
        });

        data_table = $('#realisasi-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": false,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: -1,
            // dom: 'Bfrtip',
            // buttons: [{
            //     extend: 'excel',
            //     text: "<i class='fas fa-download fa-lg'></i> Download",
            //     className: 'btn-table',
            //     exportOptions: {
            //         modifier: {
            //             search: 'applied',
            //             order: 'applied'
            //         }
            //     }
            // }],
            "columns": [{
                    data: 'uraian',
                    // title: 'Vendor'                    
                    className: 'text-left',
                },
                {
                    data: 'target',
                    // title: 'Profit Center',
                    render: $.fn.dataTable.render.number(",", ".", 0),
                    className: 'text-left',
                },
                {
                    data: 'realisasi',
                    // title: 'Text GL',
                    render: $.fn.dataTable.render.number(",", ".", 0),   
                    className: 'text-left'
                },
                {
                    data: 'achievement',
                    // title: 'Total',
                    className: 'text-right',

                },                
                {
                    "mRender": function(data, type, row) {
                        var id = row.Id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateCollection(' +
                            (row.Id ? row.Id : "''") +
                            ',' + "'" + (row.uraian ? row.uraian : "''") + "'" +
                            ',' + "'" + (row.target ? row.target : "''") + "'" +
                            ',' + "'" + (row.realisasi ? row.realisasi : "''") + "'" +
                            ',' + "'" + (row.achievement ? row.achievement : "''") + "'" +
                            ',' + "'" + (row.entitas ? row.entitas : "''") + "'" +                            
                            ',' + "'" + (row.month ? row.month : "''") + "'" +
                            ',' + "'" + (row.year ? row.year : "''") + "'" + ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteCashCollection(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });

        loadDataTable()
    });


    function loadDataTable() {
        var newRow = [];
        var year = $('#year').val();
        var month = $('#month').val();
        var week = $('#week').val();
        var entitas = $('#entitas').val();
        $.ajax({
            url: "<?php echo base_url('index.php/realisasi_investasi/getRealisasiInvestas') ?>",
            method: 'POST',
            data: {
                'month': JSON.stringify(month),
                'year': JSON.stringify(year),
                'entitas': JSON.stringify(entitas),
                'week': JSON.stringify(week),
            },
            beforeSend: function() {
                // loading();
            },
            success: function(data) {
                newRow = JSON.parse(data)

            }
        }).done(function() {
            if (newRow.length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // $('#blah').attr('src', e.target.result);
                var url = e.target.result;
                var oReq = new XMLHttpRequest();
                oReq.open("GET", url, true);
                oReq.responseType = "arraybuffer";

                oReq.onload = function(e) {
                    var arraybuffer = oReq.response;

                    /* convert data to binary string */
                    var data = new Uint8Array(arraybuffer);
                    var arr = new Array();
                    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                    var bstr = arr.join("");

                    /* Call XLSX */
                    var workbook = XLSX.read(bstr, {
                        type: "binary"
                    });

                    /* DO SOMETHING WITH workbook HERE */
                    var first_sheet_name = workbook.SheetNames[0];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[first_sheet_name];
                    // console.log(XLSX.utils.sheet_to_json(worksheet, {
                    //     raw: true
                    // }));
                    var datas = XLSX.utils.sheet_to_json(worksheet, {
                        raw: true
                    })
                    datas.forEach(function(element) {
                        element.achievement = (element.achievement*100).toFixed(2)+'%';                        
                    });
                    
                    dataUpload = datas
                    console.log('dataUpload', dataUpload)

                }

                oReq.send();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    function loading() {
        swal({
            title: 'Wait a minute...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }


    function deleteCashCollection(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/realisasi_investasi/DeleteRealisasiInvestas') ?>",
                        method: 'POST',
                        data: {
                            'id': id,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            if (response) {
                                swal("Succeeded", 'Successfully deleted', "success");
                                $('#ModalCenter').modal('hide');
                                loadDataTable()
                            }
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {                        
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function updateCashCollection(element) {
        var respon = false
        if (element.id && element.uraian && element.target && element.realisasi && element.month && element.year && element.achievement) {
            respon = true
        }
        if (respon) {
            $.ajax({
                url: "<?php echo base_url('index.php/realisasi_investasi/UpdateRealisasiInvestas') ?>",
                method: 'POST',
                data: {
                    'data': JSON.stringify(element),
                },
                beforeSend: function() {
                    loading();
                },
                success: function(response) {
                    if (response) {
                        swal("Succeeded", 'Saved successfully', "success");
                        $('#ModalCenter').modal('hide');
                        loadDataTable()
                    }
                },
                error: function(response) {
                    swal(response.status, response.statusText, "error");
                }
            }).done(function() {
            }).fail(function(jqXHR, textStatus, errorThrown) {
                // if (jqXHR.status != 422)
                //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
            });
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function InsertDataCashCollection() {
        if (dataUpload.length) {
            var respon = false
            dataUpload.forEach(function(element) {
                if (element.entitas && element.uraian && element.target && element.realisasi && element.month && element.year && element.achievement) {
                    respon = true
                }
            });
            if (respon) {
                $.ajax({
                    url: "<?php echo base_url('index.php/realisasi_investasi/InsertRealisasiInvestas') ?>",
                    method: 'POST',
                    data: {
                        'data': JSON.stringify(dataUpload),
                    },
                    beforeSend: function() {
                        loading();
                    },
                    success: function(response) {
                        if (response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenter').modal('hide');
                        }
                    },
                    error: function(response) {
                        swal(response.status, response.statusText, "error");
                    }
                }).done(function() {
                    loadDataTable()
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    // if (jqXHR.status != 422)
                    //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                });
            } else {
                swal("Failed", "Please upload the correct file", "error");
            }
        } else {
            swal("Failed", "Please upload the correct file", "error");
        }
    }

    function addCashCollection() {
        dataUpload = []
        Id = null
        $('#exampleFormControlFile1').val('');
        $('#ModalLabel').text('Add Realisasi Investasi');
        $('#tabsCollection').show();
        $("#tab1").trigger("click");
        $(".help-block").hide();    
        $('.uraian').val('');
        $('.target').val('');
        $('.realisasi').val('');
        $('.achievement').val('');        
        $('.entitas').val('');        
        $('.month').val('');
        $('.year').val('');
        $('.btnPNT').text('+');
        $('.btnPNA').text('+');
        $('.post').text('Save');
    }

    function updateCollection(id, uraian, target, realisasi, achievement, entitas, month, year) {
        $("#tab1").trigger("click");
        var minusR = realisasi.toString().includes("-") ? '-' : '+';
        var minusT = target.toString().includes("-") ? '-' : '+';
        $('.btnPNT').text(minusT);
        $('.btnPNR').text(minusR);
        $('#tabsCollection').hide();
        $('#ModalLabel').text('Update Realisasi Investasi');
        Id = id;
        $(".help-block").hide();
        $('.uraian').val(uraian);        
        $('.target').val(target.replace(/[.*+?^${}()|[\]\\]/g, ""));
        $('.realisasi').val(realisasi.replace(/[.*+?^${}()|[\]\\]/g, ""));
        $('.achievement').val(parseFloat(achievement.replace(',', ".")));        
        $('.entitas').val(entitas);
        $('.month').val(month);
        $('.year').val(year);
        $('.post').text('Update');
    }

    function saveData() {
        if ($('.uraian').val()&& $('.target').val() && 
            $('.realisasi').val() && $('.achievement').val() &&
            $('.entitas').val() && $('.month').val() && 
            $('.year').val()) {
            if ($('.post').text() == 'Save') {
                dataUpload = [{                    
                    uraian: $('.uraian').val(),                    
                    achievement: parseFloat($('.achievement').val()).toFixed(2) + '%',                                        
                    entitas: $('.entitas').val(),
                    month: $('.month').val(),
                    realisasi: $('.btnPNR').text()+$('.realisasi').val(),
                    target: $('.btnPNT').text()+$('.target').val(),
                    year: $('.year').val()
                }]
                InsertDataCashCollection()
            } else {
                updateCashCollection({
                    id: Id,
                    uraian: $('.uraian').val(),                    
                    achievement: parseFloat($('.achievement').val()).toFixed(2) + '%',                                        
                    entitas: $('.entitas').val(),
                    month: $('.month').val(),
                    realisasi: $('.btnPNR').text()+$('.realisasi').val(),
                    target: $('.btnPNT').text()+$('.target').val(),
                    year: $('.year').val()
                })
            }
        } else {
            $(".help-block").show();
        }
    }

    function reverseNumber(input) {
        return [].map.call(input, function(x) {
            return x;
        }).reverse().join('');
    }
    
    function plainNumber(number) {
        return number.split('.').join('');
    }

    function splitInDots(input) {

        var value = input.value,
            plain = plainNumber(value),
            reversed = reverseNumber(plain),
            reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
            normal = reverseNumber(reversedWithDots);

        // console.log(plain, reversed, reversedWithDots, normal);
        input.value = normal;
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

</script>