<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Kelola Uraian</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addAdjusment()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add Urain</button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right pt-10">
            Inform : <select class="Ket">
                <option value="">Choose Entitas</option>
                <option value="bs">Balance Sheet</option>
                <option value="is">Income Statement</option>
            </select>
            <input type="button" onclick="filterAdjusment($('.Ket').val())" value="Filter" class="btn btn-success">
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">
        <div class="table-responsive">
            <table id="adjusment-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th class="col-lg-1 col-sm-1">No</th>
                        <th class="col-lg-1 col-sm-1">Inform</th>
                        <th class="col-lg-1 col-sm-1">Group BS</th>
                        <th class="col-lg-2 col-sm-2">desc</th>
                        <th class="col-lg-1 col-sm-1">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add Uraian</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Inform <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select onchange="$('.inform').val() =='bs'?$('.gbs').show():$('.gbs').hide()" class="form-control inform">
                                <option value="">Choose Inform</option>
                                <option value="bs">Balance Sheet</option>
                                <option value="is">Income Statement</option>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Inform is required</small>
                        </div>
                    </div>
                    <div class="form-group gbs">
                        <label class="control-label col-sm-2">Group BS <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select class="form-control groupBS">
                                <option value="">Choose Group BS</option>
                                <option value="Aset Lancar">Aset Lancar</option>
                                <option value="Aset Tidak Lancar">Aset Tidak Lancar</option>
                                <option value="Liabilitas Lancar">Liabilitas Lancar</option>
                                <option value="Liabilitas Tidak Lancar">Liabilitas Tidak Lancar</option>
                                <option value="Ekuitas Lancar">Ekuitas Lancar</option>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Group BS is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Desc <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control desc" required placeholder="Enter desc">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The desc is required</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    //$('#realisasi-data').DataTable();
    var Id
    $(".help-block").hide();
    var data_table;
    $(document).ready(function() {
        $('.gbs').hide()
        data_table = $('#adjusment-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": false,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,

            "columns": [{
                    data: 'Id',
                    render: function(data, type, row) {
                        return (i = i + 1);
                    }
                },
                {
                    title: 'Inform',
                    "mRender": function(data, type, row) {
                        return row.for_to == 'bs' ? 'Balance Sheet' : (row.for_to == 'is' ? 'Income Statement' : '');
                    }
                },
                {
                    title: 'Group BS',
                    data: 'groupBS'
                },
                {
                    data: 'desc',
                    title: 'Desc',
                    className: 'text-left',
                },
                {
                    "mRender": function(data, type, row) {
                        var id = row.id
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateAdjusment(' +
                            (row.id ? row.id : "''") +
                            ',' + (row.for_to ? "'" + row.for_to + "'" : "''") +
                            ',' + (row.desc ? "'" + row.desc + "'" : "''") +
                            ',' + (row.groupBS ? "'" + row.groupBS + "'" : "''") +
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="alertDeleteUraian(\'' + id + '\');" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });
        loadDataTable()
    });

    function loadDataTable(ket) {
        var length;
        var newRow = [];
        var iterasi = 0;
        i = 0;
        $.ajax({
            url: "<?php echo base_url('index.php/UraianMaintenance/getDataUrain') ?>",
            method: 'post',
            data: {
                'ket': ket
            },
            async: false,
            beforeSend: function() {
                $('button[type="submit"]').attr('disabled', true);
            },
            complete: function() {
                $('button[type="submit"]').attr('disabled', false);
            },
            success: function(data) {
                data = JSON.parse(data)
                length = data.length;
                newRow = data
            }
        }).done(function() {
            if (length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        });
    }

    function saveData() {
        if ($('.inform').val() == 'bs') {
            if ($('.inform').val() && $('.desc').val() && $('.groupBS').val()) {
                if ($('.post').text() == 'Save') {
                    PostSave($('.inform').val(), $('.desc').val(), $('.groupBS').val())
                } else {
                    PostEdit(Id, $('.inform').val(), $('.desc').val(), $('.groupBS').val())
                }
            } else {
                $(".help-block").show();
            }
        } else {
            if ($('.inform').val() && $('.desc').val()) {
                if ($('.post').text() == 'Save') {
                    PostSave($('.inform').val(), $('.desc').val())
                } else {
                    PostEdit(Id, $('.inform').val(), $('.desc').val())
                }
            } else {
                $(".help-block").show();
            }
        }
    }

    function addAdjusment() {
        $('#ModalLabel').text('Add Uraian');
        $(".help-block").hide();
        $('.post').text('Save');
        $('.groupBS').val(undefined);
        $('.gbs').hide();
        $('.inform').val(undefined);
        $('.desc').val(undefined);
        Id = null;
    }

    function updateAdjusment(id, for_to, desc,groupBS) {
        $('#ModalLabel').text('Update Uraian')
        Id = id;
        $(".help-block").hide();
        $('.post').text('Update');
        $('.inform').val(for_to);
        $('.desc').val(desc);
        if (for_to == 'bs') {
            $('.gbs').show();
            $('.groupBS').val(groupBS);
        }
        else{
            $('.gbs').hide();            
        }
    }

    function alertDeleteUraian(id) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    PostDelete(id)
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostSave(inform, desc, groupBS) {
        $.ajax({
            url: "<?php echo base_url('index.php/UraianMaintenance/InsertDataUraian') ?>",
            method: 'post',
            data: {
                'inform': JSON.stringify(inform),
                'desc': JSON.stringify(desc),
                'groupBS': groupBS ? JSON.stringify(groupBS) : null
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {
                    swal("Succeeded", 'Saved successfully', "success");
                    $('#ModalCenter').modal('hide');
                    loadDataTable();
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function PostEdit(Id, inform, desc, groupBS) {
        $.ajax({
            url: "<?php echo base_url('index.php/UraianMaintenance/UpdateDataUraian') ?>",
            method: 'post',
            data: {
                'id': Id,
                'inform': JSON.stringify(inform),
                'desc': JSON.stringify(desc),
                'groupBS': groupBS ? JSON.stringify(groupBS) : null
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {
                    swal("Succeeded", 'Updated successfully', "success");
                    $('#ModalCenter').modal('hide');
                    loadDataTable();
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function PostDelete(Id) {
        $.ajax({
            url: "<?php echo base_url('index.php/UraianMaintenance/DeleteDataUraian') ?>",
            method: 'post',
            data: {
                'id': Id
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {
                    swal("Succeeded", 'Deleted successfully', "success");
                    $('#ModalCenter').modal('hide');
                    loadDataTable();
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }



    function numberWithCommas(x) {
        if (x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return 0;
        }
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function filterAdjusment(ket) {
        loadDataTable(ket)
    }

    function reverseNumber(input) {
        return [].map.call(input, function(x) {
            return x;
        }).reverse().join('');
    }

    function plainNumber(number) {
        return number.split('.').join('');
    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function splitInDots(input) {

        var value = input.value,
            plain = plainNumber(value),
            reversed = reverseNumber(plain),
            reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
            normal = reverseNumber(reversedWithDots);

        input.value = normal;
    }
</script>