<?php 
function format_round($rp){
    return number_format($rp,0,',','.');
}
$chart_ap1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ap2 = ['IDR'=>0,'Non IDR'=>0];
if (!empty($ap_data)) {
    foreach($ap_data as $a){
        $chart_ap1[$a['vendor_category']] += abs($a['total']);
        if($a['currency']=='IDR'){
            $chart_ap2['IDR'] += abs($a['total']);
        }else{
            $chart_ap2['Non IDR'] += abs($a['total']);
        }
    }
}
$chart_ar1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ar2 = ['IDR'=>0,'Non IDR'=>0];
if (!empty($ar_data)) {
    foreach($ar_data as $a){
        $chart_ar1[$a['customer_category']] += abs($a['total']);
        if($a['currency']=='IDR'){
            $chart_ar2['IDR'] += abs($a['total']);
        }else{
            $chart_ar2['Non IDR'] += abs($a['total']);
        }
    }
}
?>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
table tbody tr td.ttl{
	background: #F7FAFC;
    text-align: center;
    font-weight: bold;
}
table thead{
    background:#093890;
    text-align:center;
}

.dataTables_wrapper .dt-buttons {
  float:right;
  margin-right: 15px;
}

.btn-table {
background-color: #08388F !important;
text-decoration: none;
}

.btn-table:hover {
color: #fff !important;
text-decoration: none;
}
</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Value : <select id="entitas" name="entitas"><?php $ent = ['KFHO','KFA','KFTD','KFSP'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
        Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
            if($i==$_SESSION['year']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
            if($i==$_SESSION['month']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Day : <select name="day"><?php for($i=1;$i<=31;$i++){
            if($i==$_SESSION['day']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?>
    </select>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title" style="color: #08388F;">Account Payable</h3>
            <table class="table table-bordered">
                <thead><tr><th>Balance</th></tr></thead>
                <tbody><tr><td id="ap_balance" class='ttl'><?=format_round(array_sum($chart_ap1))?></td></tr></tbody>
            </table>
            <div class="row">
                <div class="col-md-6"> 
                    <div id="pieAp1"></div>
                    <table class="table  table-bordered">
                        <thead class='thead-g'><tr><th>PIHAK KETIGA</th></tr></thead> 
                        <tbody><tr><td id="ap_pihak_ketiga"><?= "Rp ". number_format($chart_ap1["PIHAK KETIGA"])?></td></tr></tbody> 
                    </table>

                </div>
                <div class="col-md-6"> 
                    <div id="pieAp2"></div> 
                    <table id="table_ap1" class="table  table-bordered">
                        <thead class='thead-g'><tr><th>CURRENCY</th><th>BALANCE</th></tr></thead> 
                        <tbody></tbody> 
                    </table>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <table class="table  table-bordered">
                    <thead class='thead-m'><tr><th>PIHAK BERELASI</th></tr></thead> 
                    <tbody><tr><td id="ap_pihak_berelasi"><?= "Rp ". number_format($chart_ap1["PIHAK BERELASI"])?></td></tr></tbody> 
                </table>
            </div>
            <div class="col-md-6">
                  <table id="table_ap2" class="table  table-bordered">
                <thead class='thead-m'><tr><th>CURRENCY</th><th>BALANCE</th></tr></thead> 
                <tbody></tbody> 
            </table> 
            </div>
        </div>
        <h3 class="box-title" style="color: #08388F;">Table Aging AP by Vendor</h3>

        <input type="radio" name="radio-ap" checked="checked" value="0-30"> 0 &#45; 30 Days
        <input type="radio" name="radio-ap" value="31-60"> 31 &#45; 60 Days
        <input type="radio" name="radio-ap" value="60-90"> 60 &#45; 90 Days
        <input type="radio" name="radio-ap" value="91-120"> 91 &#45; 120 Days
        <input type="radio" name="radio-ap" value="120-150"> 120 &#45; 150 Days
        <input type="radio" name="radio-ap" value="150-360"> 150 &#45; 360 Days
        <input type="radio" name="radio-ap" value=">360"> &#62; 360 Days

        <br />

        <table class="table" id='ap_table'>
            <thead>
                <tr>
                    <th>Vendor</th>
                    <th>Total</th>
                    <th>Day</th>
                    <th>Day Group</th>
                    <th>Status Overdue</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($ap_data as $a){
                    echo "<tr>
                    <td>$a[vendor]</td>
                    <td>Rp ".number_format($a['total'])."</td>
                    <td>$a[aging]</td>
                    <td>$a[age]</td>
                    <td>$a[status]</td>
                    </tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title" style="color: #08388F;">Account Receivable</h3>
            <table class="table table-bordered">
                <thead><tr><th>Balance</th></tr></thead>
                <tbody><tr><td id="ar_balance" class='ttl'><?=format_round(array_sum($chart_ar1))?></td></tr></tbody>
            </table>
            <div class="row">
                <div class="col-md-6"> <div id="pieAr1"></div>
                <table class="table  table-bordered">
                    <thead class='thead-g'><tr><th>PIHAK KETIGA</th></tr></thead> 
                    <tbody><tr><td id="ar_pihak_ketiga"><?= "Rp ".number_format($chart_ar1["PIHAK KETIGA"])?></td></tr></tbody> 
                </table>

            </div>
            <div class="col-md-6"> 
                <div id="pieAr2"></div> 
                <table id="table_ar1" class="table  table-bordered">
                    <thead class='thead-g'><tr><th>CURRENCY</th><th>BALANCE</th></tr></thead> 
                    <tbody></tbody> 
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6"> 
              <table class="table  table-bordered">
                <thead class='thead-m'><tr><th>PIHAK BERELASI</th></tr></thead> 
                <tbody><tr><td id="ar_pihak_berelasi"><?= "Rp ".number_format($chart_ar1["PIHAK BERELASI"])?></td></tr></tbody> 
            </table>
        </div>
        <div class="col-md-6"> 
            <table id="table_ar2" class="table  table-bordered">
                <thead class='thead-m'><tr><th>CURRENCY</th><th>BALANCE</th></tr></thead> 
                <tbody></tbody> 
            </table> 
        </div>
    </div>
    <h3 class="box-title" style="color: #08388F;">Table Aging AR by Customer</h3>

    <input type="radio" name="radio-ar" checked="checked" value="0-30"> 0 &#45; 30 Days
    <input type="radio" name="radio-ar" value="31-60"> 31 &#45; 60 Days
    <input type="radio" name="radio-ar" value="60-90"> 60 &#45; 90 Days
    <input type="radio" name="radio-ar" value="91-120"> 91 &#45; 120 Days
    <input type="radio" name="radio-ar" value="120-150"> 120 &#45; 150 Days
    <input type="radio" name="radio-ar" value="150-360"> 150 &#45; 360 Days
    <input type="radio" name="radio-ar" value=">360"> &#62; 360 Days

    <br />
    <table class="table" id='ar_table'>
        <thead>
            <tr>
                <th>Cutomer</th>
                <th>Total</th>
                <th>Day</th>
                <th>Day Group</th>
                <th>Status Overdue</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($ar_data as $a){
                echo "<tr>
                <td>$a[customer]</td>
                <td>Rp ".number_format($a['total'])."</td>
                <td>$a[aging]</td>
                <td>$a[age]</td>
                <td>$a[status]</td>
                </tr>";
            }
            ?>
        </tbody>
    </table>
</div>
</div>
</div>


<script>


    var radio_value_ar = "0-30";
    var radio_value_ap = "0-30";
    $('input:radio[name=radio-ar]').change(function() {

        if (this.value == '0-30') {
           radio_value_ar = this.value;
       }
       else if (this.value == '31-60') {
        radio_value_ar = this.value;   
    }
    else if (this.value == '60-90') {
        radio_value_ar = this.value;   
    }
    else if (this.value == '91-120') {
        radio_value_ar = this.value;   
    }
    else if (this.value == '120-150') {
        radio_value_ar = this.value;   
    }
    else if (this.value == '150-360') {
        radio_value_ar = this.value;   
    }
    else if (this.value == '>360') {
        radio_value_ar = this.value;   
    }

    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();



    var data_table1 = $('#ar_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
       "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/balance_sheet_ar') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'age': JSON.stringify(this.value)

      }
  },"columns": [

  { 
      data: 0
  },
  { 
      data: 1,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
    }
},
{ 
  data: 2
},
{ 
  data: 3
}
,
{ 
  data: 4

}

]
});
});


    $('input:radio[name=radio-ap]').change(function() {
        console.debug("radio ap = "+this.value);
        if (this.value == '0-30') {
           radio_value_ap = this.value;
       }
       else if (this.value == '31-60') {
        radio_value_ap = this.value;   
    }
    else if (this.value == '60-90') {
        radio_value_ap = this.value;   
    }
    else if (this.value == '91-120') {
        radio_value_ap = this.value;   
    }
    else if (this.value == '120-150') {
        radio_value_ap = this.value;   
    }
    else if (this.value == '150-360') {
        radio_value_ap = this.value;   
    }
    else if (this.value == '>360') {
        radio_value_ap = this.value;   
    }


    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();


    var data_table1 = $('#ap_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],

      pageLength: 25,
     "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
  "ajax": {
    "url": "<?php echo base_url('index.php/page/balance_sheet_ap') ?>",
    "method" : "POST",
    "data": {
      'month': JSON.stringify(month),
      'year': JSON.stringify(year),
      'entitas': JSON.stringify(entitas),
      'age': JSON.stringify(this.value)

  }
},"columns": [

{ 
  data: 0
},
{ 
  data: 1,
  render: function(data, type, row){
    var formmatedvalue=numberWithCommas(data);

    return "Rp "+formmatedvalue;
}
},
{ 
  data: 2
},
{ 
  data: 3
}
,
{ 
  data: 4

}

]
});
});

    $(document).ready(function() {



        var radio_value_ap = "0-30";
        var year = $('#year').val();
        var month = $('#month').val();
        var i = 0;
        var entitas = $('#entitas').val();


        var data_table1 = $('#ap_table').DataTable({
          "scrollY": "400px",
          "scrollCollapse": true,
          "scrollX": false,
          "ordering": true,
          "bDestroy": true,
          "serverSide": true,
          "bFilter": false,
          "bLengthChange": false,
          "processing": true,

          "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
           "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
          pageLength: 25,

          "ajax": {
            "url": "<?php echo base_url('index.php/page/balance_sheet_ap') ?>",
            "method" : "POST",
            "data": {
              'month': JSON.stringify(month),
              'year': JSON.stringify(year),
              'entitas': JSON.stringify(entitas),
              'age': JSON.stringify(radio_value_ap)

          }
      },"columns": [

      { 
          data: 0
      },
      { 
          data: 1,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
        }
    },
    { 
      data: 2
  },
  { 
      data: 3
  }
  ,
  { 
      data: 4

  }

  ]
});


        var data_table1 = $('#ar_table').DataTable({
          "scrollY": "400px",
          "scrollCollapse": true,
          "scrollX": false,
          "ordering": true,
          "bDestroy": true,
          "serverSide": true,
          "bFilter": false,
          "bLengthChange": false,
          "processing": true,

          "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
           "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
          pageLength: 25,

          "ajax": {
            "url": "<?php echo base_url('index.php/page/balance_sheet_ar') ?>",
            "method" : "POST",
            "data": {
              'month': JSON.stringify(month),
              'year': JSON.stringify(year),
              'entitas': JSON.stringify(entitas),
              'age': JSON.stringify(radio_value_ap)

          }
      },"columns": [

      { 
          data: 0
      },
      { 
          data: 1,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
        }
    },
    { 
      data: 2
  },
  { 
      data: 3
  }
  ,
  { 
      data: 4

  }

  ]
});

    })



    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });


    var chartap1 = Highcharts.chart('pieAp1', {
        credits: {
          enabled: false
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
            },
            colors :['#5FC87C','#FB6B74'],
            showInLegend: false
        }
    },
    series: [{
        data :[<?php foreach($chart_ap1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
});
    var chartap2 = Highcharts.chart('pieAp2', {
        credits: {
          enabled: false
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
            },
            colors :['#5FC87C','#FB6B74'],
            showInLegend: false
        }
    },
    series: [{
        data :[<?php foreach($chart_ap1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
});
    var chartar1 =   Highcharts.chart('pieAr1', {
        credits: {
          enabled: false
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
            },
            colors :['#5FC87C','#FB6B74'],
            showInLegend: false
        }
    },
    series: [{
        data :[<?php foreach($chart_ar1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
});
    var chartar2 =   Highcharts.chart('pieAr2', {
        credits: {
          enabled: false
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
            },
            colors :['#5FC87C','#FB6B74'],
            showInLegend: false
        }
    },
    series: [{
     data :[<?php foreach($chart_ar1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
 }]
});



    $("#btnSubmit").click(function (e) {

        e.preventDefault();


        for (var j = 0; j <= chartap1.series.length; j++){              
           chartap1.series[0].remove();
       }
       for (var j = 0; j <= chartap2.series.length; j++){              
           chartap2.series[0].remove();
       }
       for (var j = 0; j <= chartar1.series.length; j++){              
           chartar1.series[0].remove();
       }
       for (var j = 0; j <= chartar2.series.length; j++){              
           chartar2.series[0].remove();
       } 


       var urlpost = "<?php echo base_url('index.php/page/refresh_apar') ?>";

       var year = $('#year').val();
       var month = $('#month').val();

       var entitas = $('#entitas').val();
      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)


          
      },
      AjaxSucceeded,"json"

      );
      function AjaxSucceeded(result) {



        var total_ar_ketiga;
        var total_ar_berelasi;
        var total_ap_ketiga;
        var total_ap_berelasi;

        $.each(result.ar_sum, function( key, value ) {
            if (value.customer_category === 'PIHAK KETIGA'){
                $('#ar_pihak_ketiga').text('Rp '+numberWithCommas(value.total));
                total_ar_ketiga = value.total;

            }else if (value.customer_category === 'PIHAK BERELASI'){
              $('#ar_pihak_berelasi').text('Rp '+numberWithCommas(value.total));
              total_ar_berelasi = value.total;
          }   



      })

        $.each(result.ap_sum, function( key, value ) {

           if (value.vendor_category === 'PIHAK KETIGA'){
            $('#ap_pihak_ketiga').text('Rp '+numberWithCommas(value.total));
            total_ap_ketiga = value.total;
        }else if (value.vendor_category === 'PIHAK BERELASI'){
            $('#ap_pihak_berelasi').text('Rp '+numberWithCommas(value.total));
            total_ap_berelasi = value.total;
        }   

    })


        chartap1.addSeries({

         data: [{name:'PIHAK KETIGA',y:Math.abs(total_ap_ketiga)},{name:'PIHAK BERELASI',y:Math.abs(total_ap_berelasi)}]

     }, true);

        chartap2.addSeries({

         data: [{name:'IDR',y:Math.abs(result.ap_idr[0].total)},{name:'NON IDR',y:Math.abs(result.ap_nonidr[0].total)}]

     }, true);

        chartar1.addSeries({

         data: [{name:'PIHAK KETIGA',y:Math.abs(total_ar_ketiga)},{name:'PIHAK BERELASI',y:Math.abs(total_ar_berelasi)}]

     }, true);

        chartar2.addSeries({

         data: [{name:'IDR',y:Math.abs(result.ar_idr[0].total)},{name:'NON IDR',y:Math.abs(result.ar_nonidr[0].total)}]

     }, true);

        var total_ap = parseInt(total_ap_ketiga) + parseInt(total_ap_berelasi);
        var total_ar = parseInt(total_ar_ketiga) + parseInt(total_ar_berelasi);
        $('#ap_balance').text(format_round(total_ap));
        $('#ar_balance').text(format_round(total_ar));
        // $('#ar_idr').text('Rp '+numberWithCommas(result.ar_idr[0].total));
        // $('#ar_nonidr').text('Rp '+numberWithCommas(result.ar_nonidr[0].total));
        // $('#ap_idr').text('Rp '+numberWithCommas(result.ap_idr[0].total));
        // $('#ap_nonidr').text('Rp '+numberWithCommas(result.ap_nonidr[0].total));

        $.each(result.ar_currency_ketiga, function( key, value ) {



         $('#table_ar1').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
     })

        $.each(result.ar_currency_berelasi, function( key, value ) {



         $('#table_ar2').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
     })

        $.each(result.ap_currency_ketiga, function( key, value ) {



         $('#table_ap1').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
     })


        $.each(result.ap_currency_berelasi, function( key, value ) {



         $('#table_ap2').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
     })

    }



    radio_value_ar = "0-30";
    radio_value_ap = "0-30";

    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();
    var data_table1 = $('#ap_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
       "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/balance_sheet_ap') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'age': JSON.stringify(radio_value_ap)

      }
  },"columns": [

  { 
      data: 0
  },
  { 
      data: 1,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
    }
},
{ 
  data: 2
},
{ 
  data: 3
}
,
{ 
  data: 4

}

]
});


    var data_table1 = $('#ar_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
       "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/balance_sheet_ar') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'age': JSON.stringify(radio_value_ar)

      }
  },"columns": [

  { 
      data: 0
  },
  { 
      data: 1,
      render: function(data, type, row){
        var formmatedvalue=numberWithCommas(data);

        return "Rp "+formmatedvalue;
    }
},
{ 
  data: 2
},
{ 
  data: 3
}
,
{ 
  data: 4

}

]
});

})

function format_round(rp){

    if(rp<1 && rp>0){
      return (Math.round(rp*100))+'%';

  }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
  }else{
      return ("Rp "+numberWithCommas(rp));
  }
}

function numberWithCommas(x) {
 if (x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}else{
    return 0;
}
}

</script>