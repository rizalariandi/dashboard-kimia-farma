<?php 
function format_round($rp){
  return number_format($rp,0,',','.');
}
$chart_ap1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ap2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ap_data as $a){
  $chart_ap1[$a['vendor_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ap2['IDR'] += abs($a['total']);
  }else{
    $chart_ap2['Non IDR'] += abs($a['total']);
  }
}
$chart_ar1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ar2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ar_data as $a){
  $chart_ar1[$a['customer_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ar2['IDR'] += abs($a['total']);
  }else{
    $chart_ar2['Non IDR'] += abs($a['total']);
  }
}
?>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
table tbody tr td.ttl{
	background: #F7FAFC;
  text-align: center;
  font-weight: bold;
}
table thead{
  background:#093890;
  text-align:center;
}

.dataTables_wrapper .dt-buttons {
  float:right;
  margin-right: 15px;
}

.btn-table {
  background-color: #08388F !important;
  text-decoration: none;
}

.btn-table:hover {
  color: #fff !important;
  text-decoration: none;
}
</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Value : <select id="entitas" name="entitas"><?php $ent = ['KFHO','KFA','KFTD','KFSP'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>
    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Day : <select name="day"><?php for($i=1;$i<=31;$i++){
      if($i==$_SESSION['day']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?>
  </select>
  <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="white-box">
      <h3 class="box-title" style="color: #08388F;">Account Payable</h3>
      <table class="table table-bordered">
        <thead><tr><th>Balance</th></tr></thead>
        <tbody><tr><td id="ap_balance" class='ttl'><?=format_round(array_sum($chart_ap1))?></td></tr></tbody>
      </table>
      <div class="row">
        <div class="col-md-6"> 
            DAOPS : <select id="daops1" name="multiselect[]" multiple="multiple">

                <?php 
                foreach ($daops as $key => $value)
                {
                  echo '<option value="'.$value['profit_center'].'">'.$value['profit_center'].'</option>';
              }
              ?>
          </select>   
          <div id="pieAp1"></div>
          <table class="table  table-bordered">
            <thead class='thead-g' style="background: #5591F5;"><tr><th>No.</th><th>PIHAK KETIGA</th><th>TOTAL</th></tr></thead> 
            <tbody><tr><td></td><td></td><td></td></tr></tbody> 
          </table>

        </div>
        <div class="col-md-6"> 
          <div id="pieAp2"></div> 
          <table id="table_ap1" class="table  table-bordered">
            <thead class='thead-g' style="background: #5591F5;"><tr><th>No.</th><th>PIHAK BERELASI</th><th>TOTAL</th></tr></thead> 
             <tbody><tr><td></td><td></td><td></td></tr></tbody> 
          </table>

        </div>
      </div>
     
      <h3 class="box-title" style="color: #08388F;">Table Aging AP by Vendor</h3>


      <table class="table" id='ap_table'>
        <thead>
          <tr>
            <th>Vendor</th>
            <th>Total</th>
            <th>0 &#45; 30 Days</th>
            <th>31 &#45; 60 Days</th>
            <th>60 &#45; 90 Days</th>
            <th>91 &#45; 120 Days</th>
            <th>120 &#45; 150 Days</th>
            <th>150 &#45; 360 Days</th>
            <th>&#62; 360 Days</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="white-box">
      <h3 class="box-title" style="color: #08388F;">Account Receivable</h3>
      <table class="table table-bordered">
        <thead><tr><th>Balance</th></tr></thead>
        <tbody><tr><td id="ar_balance" class='ttl'><?=format_round(array_sum($chart_ar1))?></td></tr></tbody>
      </table>
      <div class="row">
        <div class="col-md-6"> <div id="pieAr1"></div>
      

      </div>
      <div class="col-md-6"> 
        <table id="table_ar1" class="table  table-bordered">
          <thead class='thead-g' style="background: #5591F5;"><tr><th>No.</th><th>Tipe Segment</th><th>TOTAL</th></tr></thead> 
           <tbody><tr><td></td><td></td><td></td></tr></tbody> 
        </table>

      </div>
    </div>
    <h3 class="box-title" style="color: #08388F;">Table Aging AR by Customer</h3>

          Profit Center : <select id="profit-center" name="multiselect[]" multiple="multiple">

                <?php 
                foreach ($profit_center as $key => $value)
                {
                  echo '<option value="'.$value['profit_center'].'">'.$value['profit_center'].'</option>';
              }
              ?>
          </select>   

            DAOPS : <select id="profit-center" name="multiselect[]" multiple="multiple">

                <?php 
                foreach ($daops as $key => $value)
                {
                  echo '<option value="'.$value['daops'].'">'.$value['daops'].'</option>';
              }
              ?>
          </select>   


          <h3 class="box-title">Plant</h3>
            Plant : <select id="profit-center" name="multiselect[]" multiple="multiple">

                <?php 
                foreach ($plant as $key => $value)
                {
                  echo '<option value="'.$value['plant'].'">'.$value['plant'].'</option>';
              }
              ?>
          </select>   

    <table class="table" id='ar_table'>
      <thead>
        <tr>
          <th>No</th>
          <th>Plant/Segment</th>
          <th>0 &#45; 30 Days</th>
          <th>31 &#45; 60 Days</th>
          <th>60 &#45; 90 Days</th>
          <th>91 &#45; 120 Days</th>
          <th>120 &#45; 150 Days</th>
          <th>150 &#45; 360 Days</th>
          <th>&#62; 360 Days</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
</div>
</div>


<script>

function getValueCheckbox(){



}
  $(document).ready(function() {

  $('#profit-center').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
 });
    $("#profit-center").multiselect('selectAll', false);

    $('#profit-center').multiselect('updateButtonText');

    var radio_value_ap = "0-30";
    var year = $('#year').val();
    var month = $('#month').val();
    var i = 0;
    var entitas = $('#entitas').val();


    var data_table1 = $('#ap_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
      "dom"         : 'Blfrtip',
      buttons: [
      {
        extend: 'excelHtml5',
        text: "<i class='fas fa-download fa-lg'></i> Download",
        className: 'btn-table',
        exportOptions: {
          modifier: {
            search: 'applied',
            order: 'applied'
          }
        }
      }
      ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/balance_sheet_ap2') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'age': JSON.stringify(radio_value_ap)

        }
      },"columns": [

      { 
        data: 0
      },
      { 
        data: 1,
        render: function(data, type, row){
          var formmatedvalue=numberWithCommas(data);

          return "Rp "+formmatedvalue;
        }
      },
      { 
        data: 2,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
      },
      { 
        data: 3,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
      }
      ,
      { 
        data: 4,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      },
      { 
        data: 5,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 6,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 7,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 8,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }

      ]
    });


    var data_table1 = $('#ar_table').DataTable({
      "scrollY": "400px",
      "scrollCollapse": true,
      "scrollX": false,
      "ordering": true,
      "bDestroy": true,
      "serverSide": true,
      "bFilter": false,
      "bLengthChange": false,
      "processing": true,

      "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
      "dom"         : 'Blfrtip',
      buttons: [
      {
        extend: 'excelHtml5',
        text: "<i class='fas fa-download fa-lg'></i> Download",
        className: 'btn-table',
        exportOptions: {
          modifier: {
            search: 'applied',
            order: 'applied'
          }
        }
      }
      ],
      pageLength: 25,

      "ajax": {
        "url": "<?php echo base_url('index.php/page/balance_sheet_ar2') ?>",
        "method" : "POST",
        "data": {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas),
          'age': JSON.stringify(radio_value_ap)

        }
      },"columns": [

      { 
        data: 0
      },
      { 
        data: 1,
        render: function(data, type, row){
          var formmatedvalue=numberWithCommas(data);

          return "Rp "+formmatedvalue;
        }
      },
      { 
        data: 2,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
      },
      { 
        data: 3,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
      }
      ,
      { 
        data: 4,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 5,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 6,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 7,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }
      ,
      { 
        data: 8,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

      }

      ]
    });

  })



  Highcharts.setOptions({
    lang: {
      decimalPoint: ',',
      thousandsSep: '.'
    }
  });


  var chartap1 = Highcharts.chart('pieAp1', {
    credits: {
      enabled: false
    },
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
        },
        colors :['#5591F5','#F79868'],
        showInLegend: false
      }
    },
    series: [{
      data :[<?php foreach($chart_ap1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
  });
  var chartap2 = Highcharts.chart('pieAp2', {
    credits: {
      enabled: false
    },
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
        },
        colors :['#5591F5','#F79868'],
        showInLegend: false
      }
    },
    series: [{
      data :[<?php foreach($chart_ap1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
  });
  var chartar1 =   Highcharts.chart('pieAr1', {
    credits: {
      enabled: false
    },
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
        },
        colors :['#5591F5','#F79868'],
        showInLegend: false
      }
    },
    series: [{
      data :[<?php foreach($chart_ar1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
  });
  var chartar2 =   Highcharts.chart('pieAr2', {
    credits: {
      enabled: false
    },
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: ''
    },
    tooltip: {
      pointFormat: '{point.name}: ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: ({point.percentage:.1f}%)'
        },
        colors :['#5591F5','#F79868'],
        showInLegend: false
      }
    },
    series: [{
     data :[<?php foreach($chart_ar1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
   }]
 });



  $("#btnSubmit").click(function (e) {

    e.preventDefault();


    for (var j = 0; j <= chartap1.series.length; j++){              
     chartap1.series[0].remove();
   }
   for (var j = 0; j <= chartap2.series.length; j++){              
     chartap2.series[0].remove();
   }
   for (var j = 0; j <= chartar1.series.length; j++){              
     chartar1.series[0].remove();
   }
   for (var j = 0; j <= chartar2.series.length; j++){              
     chartar2.series[0].remove();
   } 


   var urlpost = "<?php echo base_url('index.php/page/refresh_apar') ?>";

   var year = $('#year').val();
   var month = $('#month').val();

   var entitas = $('#entitas').val();

    var params = {
      profit: [],
      daops: [],
      plant: []
  };
  $('#profit-center :selected').each(function() {
      params.profit.push($(this).val());
  });

   $('#daops :selected').each(function() {
      params.daops.push($(this).val());
  });

    $('#plant :selected').each(function() {
      params.plant.push($(this).val());
  });
      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {



        var total_ar_ketiga;
        var total_ar_berelasi;
        var total_ap_ketiga;
        var total_ap_berelasi;

        $.each(result.ar_sum, function( key, value ) {
          if (value.customer_category === 'PIHAK KETIGA'){
            $('#ar_pihak_ketiga').text('Rp '+numberWithCommas(value.total));
            total_ar_ketiga = value.total;

          }else if (value.customer_category === 'PIHAK BERELASI'){
            $('#ar_pihak_berelasi').text('Rp '+numberWithCommas(value.total));
            total_ar_berelasi = value.total;
          }   



        })

        $.each(result.ap_sum, function( key, value ) {

         if (value.vendor_category === 'PIHAK KETIGA'){
          $('#ap_pihak_ketiga').text('Rp '+numberWithCommas(value.total));
          total_ap_ketiga = value.total;
        }else if (value.vendor_category === 'PIHAK BERELASI'){
          $('#ap_pihak_berelasi').text('Rp '+numberWithCommas(value.total));
          total_ap_berelasi = value.total;
        }   

      })


        chartap1.addSeries({

         data: [{name:'PIHAK KETIGA',y:Math.abs(total_ap_ketiga)},{name:'PIHAK BERELASI',y:Math.abs(total_ap_berelasi)}]

       }, true);

        chartap2.addSeries({

         data: [{name:'IDR',y:Math.abs(result.ap_idr[0].total)},{name:'NON IDR',y:Math.abs(result.ap_nonidr[0].total)}]

       }, true);

        chartar1.addSeries({

         data: [{name:'PIHAK KETIGA',y:Math.abs(total_ar_ketiga)},{name:'PIHAK BERELASI',y:Math.abs(total_ar_berelasi)}]

       }, true);

        chartar2.addSeries({

         data: [{name:'IDR',y:Math.abs(result.ar_idr[0].total)},{name:'NON IDR',y:Math.abs(result.ar_nonidr[0].total)}]

       }, true);

        var total_ap = parseInt(total_ap_ketiga) + parseInt(total_ap_berelasi);
        var total_ar = parseInt(total_ar_ketiga) + parseInt(total_ar_berelasi);
        $('#ap_balance').text(format_round(total_ap));
        $('#ar_balance').text(format_round(total_ar));
        // $('#ar_idr').text('Rp '+numberWithCommas(result.ar_idr[0].total));
        // $('#ar_nonidr').text('Rp '+numberWithCommas(result.ar_nonidr[0].total));
        // $('#ap_idr').text('Rp '+numberWithCommas(result.ap_idr[0].total));
        // $('#ap_nonidr').text('Rp '+numberWithCommas(result.ap_nonidr[0].total));

        $.each(result.ar_currency_ketiga, function( key, value ) {



         $('#table_ar1').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
       })

        $.each(result.ar_currency_berelasi, function( key, value ) {



         $('#table_ar2').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
       })

        $.each(result.ap_currency_ketiga, function( key, value ) {



         $('#table_ap1').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
       })


        $.each(result.ap_currency_berelasi, function( key, value ) {



         $('#table_ap2').append("<tr><td>"+value.currency+"</td><td>Rp "+numberWithCommas(value.total) +"</td></tr>");
       })

      }



    

      var data_table1 = $('#ar_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'profit_center': JSON.stringify(params.profit),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant)

          }
        },"columns": [

        { 
          data: 0
        },
        { 
          data: 1,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 2,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 3,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        }
        ,
        { 
          data: 4,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 5,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 6,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 7,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }
        ,
        { 
          data: 8,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }

        }

        ]
      });

    })

function format_round(rp){

  if(rp<1 && rp>0){
    return (Math.round(rp*100))+'%';

  }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
    }else{
      return ("Rp "+numberWithCommas(rp));
    }
  }

  function numberWithCommas(x) {
   if (x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }else{
    return 0;
  }
}

</script>