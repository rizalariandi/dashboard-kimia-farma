<div class="row">
    <div class="col-md-6">
        <div class="white-box"> <div id="chart1"></div> </div>
    </div>
    <div class="col-md-3">
        <div class="white-box"> <div id="chart2"></div></div>
    </div>
    <div class="col-md-3">
        <div class="white-box"> <div id="chart3"></div></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="white-box">
<table class="table">
    <tr>
        <th>Plant</th>
        <th>Finish Good Plant Value</th>
    </tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
    <tr><td>xxx</td><td>xxx</td></tr>
</table>

        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box"> <div id="chart4"></div></div>
    </div>
</div>

<script src="<?=base_url()?>asset/js/highstock.js"></script>
<script src="<?=base_url()?>asset/js/exporting.js"></script>
<script>
Highcharts.chart('chart1', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: ['Plant Jakarta', 'Plant Bandung', 'Plant Watudakon', 'Plant Semarang', 'Plant Medan']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total fruit consumption'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'Finish Good Value',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Semi Finish Good Value',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'Raw Material Value',
        data: [3, 4, 4, 2, 5]
    }, {
        name: 'Packaging',
        data: [3, 4, 4, 2, 5]
    }]
});
// Build the chart
Highcharts.chart('chart2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [ {
            name: 'Raw Material Aktif Value',
            y: 4.18
        }, {
            name: 'Lorem Ip',
            y: 7.05
        }]
    }]
});
// Build the chart
Highcharts.chart('chart3', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Packaging Material Sekunder',
            y: 4.18
        }, {
            name: '',
            y: 7.05
        }]
    }]
});

Highcharts.chart('chart4', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Plant KFTD'
    },
    xAxis: {
        categories: ['KFTD Makassar','KFTD Jakarta 2','KFTD Manado','KFTD Bandung','KFTD Semarang'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: '',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        data: [107,123,54,34,65,]
    }]
});
</script>