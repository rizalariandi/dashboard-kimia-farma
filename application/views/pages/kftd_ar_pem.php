<?php 
function format_round($rp){
  return number_format($rp,0,',','.');
}
$chart_ap1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ap2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ap_data as $a){
  $chart_ap1[$a['vendor_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ap2['IDR'] += abs($a['total']);
  }else{
    $chart_ap2['Non IDR'] += abs($a['total']);
  }
}
$chart_ar1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ar2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ar_data as $a){
  $chart_ar1[$a['customer_category']] += abs($a['total']);
  if($a['currency']=='IDR'){
    $chart_ar2['IDR'] += abs($a['total']);
  }else{
    $chart_ar2['Non IDR'] += abs($a['total']);
  }
}
?>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">

<style>
table tbody tr td.ttl{
	background: #F7FAFC;
  text-align: center;
  font-weight: bold;
}
table thead{
  background:#093890;
  text-align:center;
}

.dataTables_wrapper .dt-buttons {
  float:right;
  margin-right: 15px;
}

.btn-table {
  background-color: #08388F !important;
  text-decoration: none;
}

.btn-table:hover {
  color: #fff !important;
  text-decoration: none;
}
.DivToScroll{   
  background-color: #F5F5F5;
  border: 1px solid #DDDDDD;
  border-radius: 4px 0 4px 0;
  color: #3B3C3E;
  font-size: 12px;
  font-weight: bold;
  left: -1px;
  padding: 10px 7px 5px;
}

.DivWithScroll{
  height:450px;
  overflow:scroll;
  overflow-x:hidden;
}
</style>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
  <form action="" method="post">
    Value : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
    foreach($ent as $e){
      if($e==$_SESSION['entitas']) {
        echo "<option value='$e' selected>$e</option>";
      }else{
        echo "<option value='$e'>$e</option>";
      }
    } ?></select>
    Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
      if($i==$_SESSION['year']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
      if($i==$_SESSION['month']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?></select>
    Day : <select name="day"><?php for($i=1;$i<=31;$i++){
      if($i==$_SESSION['day']) {
        echo "<option value='$i' selected>$i</option>";
      }else{
        echo "<option value='$i'>$i</option>";
      }
    } ?>
  </select>
  <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="white-box">
  <h3 class="box-title" style="color: #08388F;">Table Pemasukan AR by Customer</h3>

  Customer Category : <select id="cust-cat" name="multiselect[]" multiple="multiple">

    <?php 

    foreach ($cust_cat as $key => $value)
    {
      echo '<option value="'.$value['customer_category'].'">'.$value['customer_category'].'</option>';
    }
    ?>
  </select>   

  Plant : <select id="plant_ar" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($plant as $key => $value)
    {
      echo '<option value="'.$value['text_prc'].'">'.$value['text_prc'].'</option>';
    }
    ?>
  </select>

  Segment 1 : <select id="segment1" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($segment_1 as $key => $value)
    {
      echo '<option value="'.$value['segment_1'].'">'.$value['segment_1'].'</option>';
    }
    ?>
  </select>   

  Segment 3 : <select id="segment3" name="multiselect[]" multiple="multiple">

    <?php 
    foreach ($segment_3 as $key => $value)
    {
      echo '<option value="'.$value['segment_3'].'">'.$value['segment_3'].'</option>';
    }
    ?>
  </select>      

  <table class="table" id='pem_table'>
    <thead>
      <tr>
        <th>No</th>
        <th>Customer Category</th>
        <th>Segment1</th>
        <th>Segment3</th>
        <th>Plant</th>
        <th>Pemasukan</th>
        <th>Year</th>
        <th>Month</th>
        <th>Day</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
</div>
</div>
</div>


<script>


  $(document).ready(function() {



   $('#pihak_ketiga').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#pihak_ketiga").multiselect('selectAll', false);

   $('#pihak_ketiga').multiselect('updateButtonText');


   $('#pihak_berelasi').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#pihak_berelasi").multiselect('selectAll', false);

   $('#pihak_berelasi').multiselect('updateButtonText');


   $('#daops1').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox3,
     onSelectAll: getValueCheckbox3,
     onSelectedAll: getValueCheckbox3,
     onDeselectAll: getValueCheckbox3
   });
   $("#daops1").multiselect('selectAll', false);

   $('#daops1').multiselect('updateButtonText');


   $('#cust-cat').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#cust-cat").multiselect('selectAll', false);

   $('#cust-cat').multiselect('updateButtonText');

   $('#daops_ap').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox,
     onSelectAll: getValueCheckbox,
     onSelectedAll: getValueCheckbox,
     onDeselectAll: getValueCheckbox
   });
   $("#daops_ap").multiselect('selectAll', false);

   $('#daops_ap').multiselect('updateButtonText');


   $('#plant_ar').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 250,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#plant_ar").multiselect('selectAll', false);

   $('#plant_ar').multiselect('updateButtonText');

   $('#plant_ap').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#plant_ap").multiselect('selectAll', false);

   $('#plant_ap').multiselect('updateButtonText');

   $('#segment1').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 300,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#segment1").multiselect('selectAll', false);

   $('#segment1').multiselect('updateButtonText');

   $('#segment3').multiselect({
     includeSelectAllOption: true,
     enableFiltering: true,
     maxHeight: 250,
     enableCaseInsensitiveFiltering: true,
     onChange: getValueCheckbox2,
     onSelectAll: getValueCheckbox2,
     onSelectedAll: getValueCheckbox2,
     onDeselectAll: getValueCheckbox2
   });
   $("#segment3").multiselect('selectAll', false);

   $('#segment3').multiselect('updateButtonText');

   var params = {
    cust_cat: [],
    plant: [],
    segment1: [],
    segment3: []
  };
  $('#cust-cat :selected').each(function() {
    params.cust_cat.push($(this).val());
  });

  $('#plant_ar :selected').each(function() {
    params.plant.push($(this).val());
  });

  $('#segment1 :selected').each(function() {
    params.segment1.push($(this).val());
  });

  $('#segment3 :selected').each(function() {
    params.segment3.push($(this).val());
  });
  var radio_value_ap = "0-30";
  var year = $('#year').val();
  var month = $('#month').val();
  var i = 0;
  var entitas = $('#entitas').val();


  var i = 0;

 var data_table1 = $('#pem_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd_pem') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1),
            'segment3': JSON.stringify(params.segment3)

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 6
        }
        ,
        { 
          data: 7
        }
        ,
        { 
          data: 8

        }
        ]
      });

})


function getValueCheckbox3(){
  var year = $('#year').val();
  var month = $('#month').val();

  var entitas = $('#entitas').val();

  var params = {
    daops: []
  };

  $('#cust-cat :selected').each(function() {
    params.daops.push($(this).val());
  });


      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'year': JSON.stringify(year),
          'month': JSON.stringify(month),
          'entitas': JSON.stringify(entitas),
          'daops': JSON.stringify(params.daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
     
      }


    }  
    function getValueCheckbox2(){

      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();

      var params = {
        daops: [],
        plant: [],
        segment1: [],
        segment3: []
      };

      $('#cust-cat :selected').each(function() {
        params.daops.push($(this).val());
      });

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });
      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });

      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });
      console.debug(params.plant);

      var i=0;
     var data_table1 = $('#pem_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,

        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd_pem') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1),
            'segment3': JSON.stringify(params.segment3)

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 6
        }
        ,
        { 
          data: 7
        }
        ,
        { 
          data: 8

        }
        ]
      });

    }

    function getValueCheckbox(){
     var year = $('#year').val();
     var month = $('#month').val();

     var entitas = $('#entitas').val();

     var params = {
      daops: [],
      plant: [],
      segment1: [],
      segment3: []
    };

    $('#cust-cat :selected').each(function() {
      params.daops.push($(this).val());
    });

    var urlpost = "<?php echo base_url('index.php/page/get_plant_pem') ?>";

      //console.debug(month+"-"+year+"-"+entitas);
      $.post(
        urlpost,
        {

          'daops': JSON.stringify(params.daops)


          
        },
        AjaxSucceeded,"json"

        );
      function AjaxSucceeded(result) {
        $('#plant_ar')
        .find('option')
        .remove()
        .end();


        $.each(result.data, function( key, value ) {
         $('#plant_ar')
         .append($("<option></option>")
           .attr("value",value.text_prc)
           .text(value.text_prc));

         $('#plant_ar').multiselect('rebuild');
         $("#plant_ar").multiselect('selectAll', false);

         $('#plant_ar').multiselect('updateButtonText');
          //$('#kota').append('<option value="'+value+'">'+value+'</option>');
        })


      }

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });
      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });

      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });
      console.debug(params.plant);

      var i=0;
      var data_table1 = $('#pem_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,


        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd_pem') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1),
            'segment3': JSON.stringify(params.segment3)

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 6
        }
        ,
        { 
          data: 7
        }
        ,
        { 
          data: 8

        }
        ]
      });

    }

    function Employee(name, y) {
      this.name = name;
      this.y = y;
    }

    $("#btnSubmit").click(function (e) {

      e.preventDefault();

   


      var params = {
        daops: [],
        plant: [],
        segment1: [],
        segment3: []
      };

      $('#cust-cat :selected').each(function() {
        params.daops.push($(this).val());
      });

      $('#plant_ar :selected').each(function() {
        params.plant.push($(this).val());
      });

      $('#segment1 :selected').each(function() {
        params.segment1.push($(this).val());
      });


      $('#segment3 :selected').each(function() {
        params.segment3.push($(this).val());
      });



      var year = $('#year').val();
      var month = $('#month').val();

      var entitas = $('#entitas').val();
      var chart = [];
  



      var i=0;
     var data_table1 = $('#pem_table').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": false,
        "ordering": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "processing": true,
   
        "lengthMenu": [[25, 100, -1], [25, 100, "All"]],
        "dom"         : 'Blfrtip',
        buttons: [
        {
          extend: 'excelHtml5',
          text: "<i class='fas fa-download fa-lg'></i> Download",
          className: 'btn-table',
          exportOptions: {
            modifier: {
              search: 'applied',
              order: 'applied'
            }
          }
        }
        ],
        pageLength: 25,

        "ajax": {
          "url": "<?php echo base_url('index.php/page/balance_sheet_ar2_kftd_pem') ?>",
          "method" : "POST",
          "data": {
            'month': JSON.stringify(month),
            'year': JSON.stringify(year),
            'entitas': JSON.stringify(entitas),
            'daops': JSON.stringify(params.daops),
            'plant': JSON.stringify(params.plant),
            'segment1': JSON.stringify(params.segment1),
            'segment3': JSON.stringify(params.segment3)

          }
        },"columns": [

        { 
          data: 0,
          render: function(data, type, row){


            return (i = i +1);
          }
        },
        { 
          data: 1
        }
        ,
        { 
          data: 2
        }
        ,
        { 
          data: 3
        },
        { 
          data: 4
        },
        { 
          data: 5,
          width: 100,
          render: function(data, type, row){
            var formmatedvalue=numberWithCommas(data);

            return "Rp "+formmatedvalue;
          }
        },
        { 
          data: 6
        }
        ,
        { 
          data: 7
        }
        ,
        { 
          data: 8

        }
        ]
      });
    })

function format_round(rp){

  if(rp<1 && rp>0){
    return (Math.round(rp*100))+'%';

  }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
    }else{
      return ("Rp "+numberWithCommas(rp));
    }
  }

  function numberWithCommas(x) {
   if (x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }else{
    return 0;
  }
}

</script>
