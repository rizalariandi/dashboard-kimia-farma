<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">List User</h4>
</div>
</div>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
    .last_update {
        font-size: 12px;

        color: #cb3935;

        font-family: Roboto;
        font-style: italic;
    }

    .pt-0 {
        padding-top: 0
    }

    .pt-10 {
        padding-top: 10px
    }

    .font-bold-500 {
        font-weight: 500
    }

    th {
        background-color: #093890;
        color: white;
        text-align: center;
    }

    td {
        color: #4A5675;
        text-align: center;
    }

    .dataTables_wrapper .dt-buttons {
        float: right;
    }

    .btn-table {
        background-color: #08388F !important;
        text-decoration: none;
    }

    .btn-table:hover {
        color: #fff !important;
        text-decoration: none;
    }

    .m-0 {
        margin: 0;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }
    }
</style>
<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-12">
        <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12 text-right">
            <button type="button" class="btn btn-primary" onclick="addAdjusment()" data-toggle="modal" data-target="#ModalCenter">
                <i class="fa fa-plus"></i>
                Add User</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="white-box pt-0">
        <div class="table-responsive">
            <table id="adjusment-data" class="table table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Hp</th>
                        <th scope="col">Level</th>
                        <th scope="col">Unit</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Status</th>
                        <th scope="col">Role Entitas</th>
                        <th scope="col">action</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title font-bold-500" id="ModalLabel">Add User</h3>
                <h5 class="last_update">Note: * Mandatory Field</h5>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" method="post" data-content="store" novalidate="" id="form-create-income">
                    <div class="form-group">

                        <label class="control-label col-sm-2">Nama <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control nama" placeholder="Enter Nama">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Nama is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Username <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control username" placeholder="Enter Username">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Username is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Email <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control email" placeholder="Enter Email">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Email is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Password <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control password" disabled value='kimiafarma' placeholder="Enter Password">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Password is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Role Entitas <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select id="opt_role_entitas" name="multiselect[]" multiple="multiple">
                                <option value="KFHO">KFHO</option>
                                <option value="KFA">KFA</option>
                                <option value="KFTD">KFTD</option>
                                <option value="KFSP">KFSP</option>
                            </select>

                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">Role Entitas is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">No HP:</label>
                        <div class="col-sm-10">
                            <input type="text" onkeypress="return isNumberKey(event)" class="form-control hp" placeholder="Enter No HP">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Unit <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control unit" placeholder="Enter Unit">
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Unit is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Level <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <select class="form-control level">
                                <option value="">Choose Level</option>
                                <?php
                                foreach ($level as $key => $value)
                                {
                                    echo '<option value="'.$value['level'].'">'.$value['level'].'</option>';
                                }
                                ?>
                            </select>
                            <small class="help-block text-danger" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="INVALID" style="">The Level is required</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Jabatan:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control jabatan" placeholder="Enter Jabatan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">NIK:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control nik" placeholder="Enter NIK">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Status <span class="text-danger">*</span>:</label>
                        <div class="col-sm-10">
                            <input type="checkbox" class="status" data-toggle="toggle">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary post" onclick="saveData()">Save </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    //$('#realisasi-data').DataTable();
    var Id
    $(".help-block").hide();
    var data_table;
    $(document).ready(function() {
        $('#opt_role_entitas').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            maxHeight: 300,
            enableCaseInsensitiveFiltering: true
        });

        $('#opt_role_entitas').multiselect('updateButtonText');

        data_table = $('#adjusment-data').DataTable({
            "scrollY": "600px",
            "scrollCollapse": true,
            "scrollX": true,
            "ordering": true,
            "bFilter": true,
            "bLengthChange": true,
            "lengthMenu": [
                [25, 100, -1],
                [25, 100, "All"]
            ],

            pageLength: 25,

            "columns": [{
                    data: 'Id',
                    render: function(data, type, row) {
                        return (i = i + 1);
                    }
                },
                {
                    data: 'nama',
                    title: 'Nama',
                    className: 'text-left'
                },
                {
                    data: 'username',
                    title: 'Username',
                    className: 'text-left',
                },
                {
                    data: 'email',
                    title: 'Email',
                    className: 'text-left',
                },
                {
                    data: 'hp',
                    title: 'HP',
                    className: 'text-right',
                },
                {
                    data: 'unit',
                    title: 'Unit',
                    className: 'text-right'
                },
                {
                    data: 'level',
                    title: 'Level',
                    className: 'text-right'
                },
                {
                    data: 'jabatan',
                    title: 'Jabatan',
                    className: 'text-right'
                },
                {
                    data: 'NIK',
                    title: 'NIK',
                    className: 'text-right'
                },
                {
                    title: 'Status',
                    "mRender": function(data, type, row) {
                        if (row.active == 1) {
                            return '<button class="btn btn-sm btn-success" style="border-radius: 15px;" title="Active"><i class="glyphicon glyphicon-ok"></i></button><p class="m-0">Active</p>'
                        } else {
                            return '<button class="btn btn-sm btn-danger" style="border-radius: 15px;" title="Non Activex"><i class="glyphicon glyphicon-remove"></i></button><p class="m-0">Inactive</p>'
                        }
                    }
                },
                {
                    data: 'entitas',
                    title: 'Role Entitas',
                    className: 'text-right'
                },
                {
                    "mRender": function(data, type, row) {
                        return '<button class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#ModalCenter" onclick="updateUser(' +
                            "'" + (row.nama ? row.nama : '') + "'" + ',' +
                            "'" + row.username + "'" + ',' +
                            "'" + row.email + "'" + ',' +
                            "'" + (row.hp ? row.hp : '') + "'" + ',' +
                            "'" + (row.unit ? row.unit : '') + "'" + ',' +
                            "'" + (row.level ? row.level : '') + "'" + ',' +
                            "'" + (row.jabatan ? row.jabatan : '') + "'" + ',' +
                            "'" + (row.NIK ? row.NIK : '') + "'" +
                            ',' + row.active +",'"+row.entitas+"'"+
                            ')" title="Edit"><i class="fa fa-edit"></i></button>' +
                            '&nbsp; <button  class="btn btn-sm btn-danger" onclick="deleteUser(' + "'" + row.email + "'" + ')" title="Delete"><i class="fa fa-trash"></i></button>';
                    }

                }
            ]
        });
        loadDataTable()
    });
    function getValueCheckbox1(){

        var urlpost = "<?php echo base_url('index.php/Menu_management/getMenuLevel2') ?>";
        var param = {
            menu: []
        };
        $('#level1 :selected').each(function() {
            param.menu.push($(this).val());
        });

        console.debug(param);
        console.debug(urlpost);

        // $("#sum").html(sum);
        $.post(
            urlpost,
            {
                'level1': JSON.stringify(param.menu)
            },
            AjaxSucceededOption1,"json"

        );
        //  getValueCheckbox2();
    }
    function loadDataTable(entitas, year, month) {
        var length;
        var newRow = [];
        var iterasi = 0;
        i = 0;
        $.ajax({
            url: "<?php echo base_url('index.php/User_maintenance/getUser') ?>",
            method: 'get',
            data: {
                'year': year,
                'entitas': entitas,
                'month': month,
            },
            async: false,
            beforeSend: function() {
                $('button[type="submit"]').attr('disabled', true);
            },
            complete: function() {
                $('button[type="submit"]').attr('disabled', false);
            },
            success: function(data) {
                data = JSON.parse(data)
                length = data.length;
                newRow = data
            }
        }).done(function() {
            if (length !== 0) {
                data_table.clear().draw();
                data_table.rows.add(newRow);
                data_table.columns.adjust().draw();
            } else {
                data_table.clear().draw();
            }
        });
    }

    function saveData() {

        if ($('.nama').val(), $('.username').val(), $('.email').val(), $('.unit').val(), $('.level').val()) {
            if ($('.post').text() == 'Save') {
                if ($('.password').val()) {
                    PostSave($('.nama').val(), $('.username').val(), $('.email').val(), $('.password').val(), $('.hp').val(), $('.unit').val(), $('.level').val(), $('.jabatan').val(), $('.nik').val(), $('.status').prop('checked'),$('#opt_role_entitas').val())
                } else {
                    $(".help-block").show();
                }
            } else {
                PostUpdate($('.nama').val(), $('.username').val(), $('.email').val(), $('.password').val(), $('.hp').val(), $('.unit').val(), $('.level').val(), $('.jabatan').val(), $('.nik').val(), $('.status').prop('checked'),$('#opt_role_entitas').val())
            }
        } else {
            $(".help-block").show();
        }
    }

    function addAdjusment() {
        $('#ModalLabel').text('Add User');
        $(".help-block").hide();
        $('.post').text('Save');
        $('.nama').val(undefined);
        $('.username').val(undefined);
        $('.email ').val(undefined);
        // $("username").prop("disabled", false);
        $(".email").prop("disabled", false);
        // $('.password').val(undefined);
        $('.hp').val(undefined);
        $('.unit').val(undefined);
        $('.level').val(undefined);
        $('.jabatan').val(undefined);
        $('.nik').val(undefined);
        $('.status').bootstrapToggle('on')
    }

    function updateUser(nama, username, email, hp, unit, level, jabatan, nik, active, role_entitas) {
        $('#ModalLabel').text('Update User')
        $('.post').text('Update');
        $('.nama').val(nama);
        $('.username').val(username);
        $('.email ').val(email);
        $('.email').prop("disabled", true);
        // $('.username').prop("disabled", true);
        // $('.password').val(undefined);
        $('.hp').val(hp);
        $('.unit').val(unit);
        $('.level').val(level);
        $('.jabatan').val(jabatan);
        $('.nik').val(nik);
        $('.status').bootstrapToggle(active == 1 ? 'on' : 'off')
        var role = role_entitas.split(",");
        $('#opt_role_entitas')
            .find('option')
            .remove()
            .end();

        var ent= [];
        ent.push("KFHO");
        ent.push("KFA");
        ent.push("KFTD");
        ent.push("KFSP");
        var selected = false;
        $.each(ent, function( key, value ) {
            $.each(role, function( key1, value1 ) {

                if (value === value1){
                    selected = true;
                }

            })

            if (selected){
                $('#opt_role_entitas')
                    .append($("<option></option>")
                        .attr("value",value).attr("selected",true)
                        .text(value));
                selected = false;
            }else{
                $('#opt_role_entitas')
                    .append($("<option></option>")
                        .attr("value",value)
                        .text(value));
            }


        })


        $('#opt_role_entitas').multiselect('rebuild');


        $('#opt_role_entitas').multiselect('updateButtonText');



    }

    function deleteUser(email) {
        swal({
                title: "Are you sure?",
                text: "Are you sure to delete!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo base_url('index.php/User_maintenance/DelUser') ?>",
                        method: 'post',
                        data: {
                            'email': email,
                        },
                        beforeSend: function() {
                            loading();
                        },
                        success: function(response) {
                            swal("Succeeded", 'Saved successfully', "success");
                            $('#ModalCenter').modal('hide');
                        },
                        error: function(response) {
                            swal(response.status.toString(), response.statusText, "error");
                        }
                    }).done(function() {
                        loadDataTable();
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        // if (jqXHR.status != 422)
                        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
                    });
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    }

    function PostSave(nama, username, email, password, hp, unit, level, jabatan, nik, status, role_entitas) {
        $.ajax({
            url: "<?php echo base_url('index.php/User_maintenance/register') ?>",
            method: 'post',
            data: {
                'nama': nama,
                'username': username,
                'email': email,
                'password': password,
                'hp': hp,
                'unit': unit,
                'level': level,
                'jabatan': jabatan,
                'nik': nik,
                'active': status ? 1 : 0,
                'entitas': JSON.stringify(role_entitas)
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {                    
                    swal("Succeeded", 'Saved successfully', "success");
                    $('#ModalCenter').modal('hide');
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {
            loadDataTable();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function PostUpdate(nama, username, email, password, hp, unit, level, jabatan, nik, status,role_entitas) {
        $.ajax({
            url: "<?php echo base_url('index.php/User_maintenance/updateUser') ?>",
            method: 'post',
            data: {
                'nama': nama,
                'username': username,
                'email': email,
                'password': password,
                'hp': hp,
                'unit': unit,
                'level': level,
                'jabatan': jabatan,
                'nik': nik,
                'active': status ? 1 : 0,
                'entitas': JSON.stringify(role_entitas)
            },
            beforeSend: function() {
                loading();
            },
            success: function(response) {
                response = JSON.parse(response);
                if (!response.message) {                    
                    swal("Succeeded", 'Update successfully', "success");
                    $('#ModalCenter').modal('hide');
                } else {
                    swal('Error', response.message, "error");
                }
            },
            error: function(response) {
                swal(response.status.toString(), response.statusText, "error");
            }
        }).done(function() {
            loadDataTable();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // if (jqXHR.status != 422)
            //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
        });
    }

    function loading() {
        swal({
            title: 'Tunggu Sebentar...',
            text: ' ',
            icon: 'info',
            buttons: false,
            closeOnClickOutside: false,
        });
    }

    function filterAdjusment(entitas, year, month) {
        loadDataTable(entitas, year, month)
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>