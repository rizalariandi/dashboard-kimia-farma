<?php 
function format_round($rp){
    return number_format($rp,0,',','.');
}
$chart_ap1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ap2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ap_data as $a){
    $chart_ap1[$a['vendor_category']] += abs($a['total']);
    if($a['currency']=='IDR'){
        $chart_ap2['IDR'] += abs($a['total']);
    }else{
        $chart_ap2['Non IDR'] += abs($a['total']);
    }
}
$chart_ar1 = ['PIHAK KETIGA'=>0,'PIHAK BERELASI'=>0];
$chart_ar2 = ['IDR'=>0,'Non IDR'=>0];
foreach($ar_data as $a){
    $chart_ar1[$a['customer_category']] += abs($a['total']);
    if($a['currency']=='IDR'){
        $chart_ar2['IDR'] += abs($a['total']);
    }else{
        $chart_ar2['Non IDR'] += abs($a['total']);
    }
}
$chart_aging = ['0-30'=> 0 , '31-60'=> 0 , '61-90' => 0  , '91-120' => 0 , '121-150' => 0 , '151-360' => 0];
foreach($ar_data_aging as $a){
    $chart_aging[$a['age']] += abs($a['aging']);
}
?>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<style>
table tbody tr td.ttl{
	background: #F7FAFC;
    text-align: center;
    font-weight: bold;
}
table thead{
    background:#093890;
    text-align:center;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
        <h3 class="box-title">Account Payable</h3>
        <table class="table table-bordered">
        <thead><tr><th>Balance In <?=$_SESSION['month']?> <?=$_SESSION['year']?></th></tr></thead>
        <tbody><tr><td class='ttl'><?=format_round(array_sum($chart_ap1))?></td></tr></tbody>
        </table>
        <div class="row">
            <div class="col-md-6"> <div id="pieAp1"></div> </div>
<!--             <div class="col-md-6"> <div id="pieAp2"></div> </div> -->
            <div class="col-md-6">
                <table class="table" id='ap_table'>
                <thead>
                    <tr>
                        <th>Vendor</th>
                        <th>Total</th>
                        <th>Day</th>
                        <th>Day Group</th>
                        <th>Status Overdue</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                foreach($ap_data as $a){
                    echo "<tr>
                    <td>$a[vendor]</td>
                    <td>$a[total]</td>
                    <td>$a[aging]</td>
                    <td>$a[age]</td>
                    <td>$a[status]</td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>        
            </div>
        </div>
        
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
        <h3 class="box-title">Account Receivable</h3>
        <table class="table table-bordered">
        <thead><tr><th>Balance In <?=$_SESSION['month']?> <?=$_SESSION['year']?></th></tr></thead>
        <tbody><tr><td  class='ttl'><?=format_round(array_sum($chart_ar1))?></td></tr></tbody>
        </table>
        <div class="row">
            <div class="col-md-3">
                    <table class="table table-bordered">
                    <thead><tr><th>Pihak Ketiga dan Pihak Berelasi</th></tr></thead>
                    <tbody><tr><td  class='ttl'><?php foreach($chart_ar1 as $k=>$v){echo " $k : $v <br>";} ?></td></tr></tbody>
                    </table>
            </div>
            <div class="col-md-3"> <div id="pieArAging"></div> </div>
<!--             <div class="col-md-6"> <div id="pieAr2"></div> </div> -->
            <div class="col-md-6">
            <table class="table" id='ar_table'>
            <thead>
                <tr>
                    <th>Cutomer</th>
                    <th>Total</th>
                    <th>Day</th>
                    <th>Day Group</th>
                    <th>Status Overdue</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            foreach($ar_data as $a){
                echo "<tr>
                <td>$a[customer]</td>
                <td>$a[total]</td>
                <td>$a[aging]</td>
                <td>$a[age]</td>
                <td>$a[status]</td>
                </tr>";
            }
            ?>
                        </tbody>
            </table>                
            </div>
        </div>
        </div>
    </div>
</div>

<script src="<?=base_url()?>asset/js/highcharts.js"></script>


<script>
$('#ap_table').DataTable({
    lengthChange : false
});
$('#ar_table').DataTable({
    lengthChange : false
});
Highcharts.setOptions({
    lang: {
        decimalPoint: ',',
        thousandsSep: '.'
    }
});
Highcharts.chart('pieAp1', {
    credits: {
      enabled: false
    },
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: <b>Rp. {point.y} ({point.percentage:.1f}%)</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: Rp. {point.y} ({point.percentage:.1f}%)'
            },
            colors :['#5FC87C','#FB6B74'],
            showInLegend: false
        }
    },
    series: [{
        data :[<?php foreach($chart_ap1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
});
Highcharts.chart('pieArAging', {
    credits: {
      enabled: false
    },
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{point.name}: {point.y} </b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y}'
            },
            colors :['#5FC87C','#FB6B74','#31A5E5','#E531DC','#C6D718','#18D7CA'],
            showInLegend: true
        }
    },
    series: [{
        data :[<?php foreach($chart_aging as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
    }]
});
// Highcharts.chart('pieAr1', {
//     credits: {
//       enabled: false
//     },
//     chart: {
//         plotBackgroundColor: null,
//         plotBorderWidth: null,
//         plotShadow: false,
//         type: 'pie'
//     },
//     title: {
//         text: ''
//     },
//     tooltip: {
//         pointFormat: '{point.name}: <b>Rp. {point.y} ({point.percentage:.1f}%)</b>'
//     },
//     plotOptions: {
//         pie: {
//             allowPointSelect: true,
//             cursor: 'pointer',
//             dataLabels: {
//                 enabled: true,
//                 format: '<b>{point.name}</b>: Rp. {point.y} ({point.percentage:.1f}%)'
//             },
//             colors :['#5FC87C','#FB6B74'],
//             showInLegend: false
//         }
//     },
//     series: [{
//         data :[<?php foreach($chart_ar1 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
//     }]
// });
// Highcharts.chart('pieAr2', {
//     credits: {
//       enabled: false
//     },
//     chart: {
//         plotBackgroundColor: null,
//         plotBorderWidth: null,
//         plotShadow: false,
//         type: 'pie'
//     },
//     title: {
//         text: ''
//     },
//     tooltip: {
//         pointFormat: '{point.name}: <b>Rp. {point.y} ({point.percentage:.1f}%)</b>'
//     },
//     plotOptions: {
//         pie: {
//             allowPointSelect: true,
//             cursor: 'pointer',  
//             dataLabels: {
//                 enabled: true,
//                 format: '<b>{point.name}</b>: Rp. {point.y} ({point.percentage:.1f}%)'
//             },
//             colors :['#5FC87C','#FB6B74'],
//             showInLegend: false
//         }
//     },
//     series: [{
//         data :[<?php foreach($chart_ar2 as $k=>$v){echo "{name:'$k',y:$v},";} ?>{}]
//     }]
// });
</script>