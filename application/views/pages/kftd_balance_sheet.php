<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <form action="" method="post">
        Value : <select id="entitas" name="entitas"><?php $ent = ['KFTD'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
        Year : <select id="year" name="year"><?php for($i=2016;$i<=2019;$i++){
            if($i==$_SESSION['year']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Month : <select id="month" name="month"><?php for($i=1;$i<=12;$i++){
            if($i==$_SESSION['month']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Week : <select name="week"><?php for($i=1;$i<=4;$i++){
            if($i==$_SESSION['week']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?></select>
        Day : <select name="day"><?php for($i=1;$i<=31;$i++){
            if($i==$_SESSION['day']) {
                echo "<option value='$i' selected>$i</option>";
            }else{
                echo "<option value='$i'>$i</option>";
            }
        } ?>
    </select>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>
</form>
</div>
</div>

<style>
table tbody tr.odd{
	background: #F7FAFC;
}
table tr th{
    background:#093890;
}
</style>

<div class="row">
    <?php
    function format_round($rp){
        return number_format($rp,0,',','.');
    }
    $data = ['Working Capital','Current Ratio','Cash Ratio'];
    $bs_arr = array();
    foreach($bs as $b){
        $bs_arr[$b['jenis']][$b['balance_sheet']][$b['faktor']]=$b['amount'];
    }

    ?>
    <div class="col-md-4">
        <div class="white-box" style="background: #FDEDDD;">
            <h3 class="box-title">WORKING CAPITAL</h3> 
            <h1><b id="working_capital">0</b></h1>
        </div>
    </div>
    <div class="col-md-4">
        <div class="white-box" style="background: #FDEDDD;">
            <h3 class="box-title">CURRENT RATIO</h3> 
            <h1><b id="current_ratio">0</b></h1>
        </div>
    </div>
    <div class="col-md-4">
        <div class="white-box" style="background: #FDEDDD;">
            <h3 class="box-title">CASH RATIO</h3> 
            <h1><b id="cash_ratio">0</b></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <ul class="nav customtab2 nav-tabs" role="tablist" style='float: right;border: unset;'>
                    <li role="presentation" class="active"><a href="#home6" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">Lancar</span></a></li>
                    <li role="presentation" class=""><a href="#profile6" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Tidak Lancar</span></a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home6">
                        <div class="clearfix"></div>
                        <?php
                        foreach($bs_arr['LANCAR'] as $k=>$v){
                            echo "<div class='row'><h2>$k</h2>
                            <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#A6DCC8;'><h2>".format_round(array_sum($v))."</h2></td></tr></table></div>
                            <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>";
                            foreach ($v as $key => $value) {
                                echo "<tr><td>$key</td><td>".format_round($value)."</td></tr>";

                            }
                            echo "</table></div>
                            </div>";
                        }
                        ?>
                        <div class="clearfix"></div>
                        <?php
                        foreach($bs_arr['   '] as $k=>$v){
                            echo "<div id='div-eku' class='row'><h2>$k</h2>
                            <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#A6DCC8;'><h2>".format_round(array_sum($v))."</h2></td></tr></table></div>
                            <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>";
                            foreach ($v as $key => $value) {
                                echo "<tr><td>$key</td><td>".format_round($value)."</td></tr>";

                            }
                            echo "</table></div>
                            </div>";
                        }
                        ?>
                         <div class="clearfix"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile6"><div class="clearfix"></div>
                    <?php
                    foreach($bs_arr['TIDAK LANCAR'] as $k=>$v){
                        echo "<div class='row'><h2>$k</h2>
                        <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#FFBEBC;'><h2>".format_round(array_sum($v))."</h2></td></tr></table></div>
                        <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>";
                        foreach ($v as $key => $value) {
                            echo "<tr><td>$key</td><td>".format_round($value)."</td></tr>";

                        }
                        echo "</table></div>
                        </div>";
                    }
                    ?>
                     <div class="clearfix"></div>
                    <?php
                    foreach($bs_arr['   '] as $k=>$v){
                        echo "<div id='div-eku' class='row'><h2>$k</h2>
                        <div class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total $k</th></tr><tr><td style='background:#FFBEBC;'><h2>".format_round(array_sum($v))."</h2></td></tr></table></div>
                        <div class='col-md-6'><table class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>";
                        foreach ($v as $key => $value) {
                            echo "<tr><td>$key</td><td>".format_round($value)."</td></tr>";

                        }
                        echo "</table></div>
                        </div>";
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<script>
 $(document).ready(function() {
     var urlpost = "<?php echo base_url('index.php/page/refresh_balance_sheet') ?>";

     var year = $('#year').val();
     var month = $('#month').val();

     var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
        urlpost,
        {
          'month': JSON.stringify(month),
          'year': JSON.stringify(year),
          'entitas': JSON.stringify(entitas)


          
      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {

     // $.each(result, function( key, value ) {

        $('#working_capital').text(format_round(result.data[0].working_capital));
        $('#current_ratio').text(result.data[0].current_ratio+"%");
        $('#cash_ratio').text(result.data[0].cash_ratio+"%");
        

   // });   


}

});


 $("#btnSubmit").click(function (e) {

  e.preventDefault();
  console.debug("test");
  var urlpost = "<?php echo base_url('index.php/page/refresh_balance_sheet') ?>";

  var year = $('#year').val();
  var month = $('#month').val();

  var entitas = $('#entitas').val();

  $.post(
    urlpost,
    {
      'month': JSON.stringify(month),
      'year': JSON.stringify(year),
      'entitas': JSON.stringify(entitas)



  },
  AjaxSucceeded,"json"

  );
  function AjaxSucceeded(result) {

     // $.each(result, function( key, value ) {

        $('#working_capital').text(format_round(result.data[0].working_capital));
        $('#current_ratio').text(result.data[0].current_ratio + "%");
        $('#cash_ratio').text(result.data[0].cash_ratio + "%");

        $('#home6').text("");
        $('#profile6').text("");
        $('#div-eku').text("");
        
        var amount_aset_lancar=0;
        var amount_aset_tidak=0;
        var amount_liabilitas_lancar=0;
        var amount_liabilitas_tidak=0;
        var amount_ekuitas = 0;
        
        $.each(result.bs, function( key, value ) {
            if ((value.balance_sheet === 'ASET')&&(value.jenis === 'LANCAR')){
                amount_aset_lancar = parseInt(amount_aset_lancar) + parseInt(value.amount);
            }else  if ((value.balance_sheet === 'ASET')&&(value.jenis === 'TIDAK LANCAR')){
                amount_aset_tidak = parseInt(amount_aset_tidak) + parseInt(value.amount);
            }else  if ((value.balance_sheet === 'LIABILITAS')&&(value.jenis === 'LANCAR')){
                amount_liabilitas_lancar = parseInt(amount_liabilitas_tidak) + parseInt(value.amount);
            }else  if ((value.balance_sheet === 'LIABILITAS')&&(value.jenis === 'TIDAK LANCAR')){
                amount_liabilitas_tidak = parseInt(amount_liabilitas_tidak) + parseInt(value.amount);
            }else if (value.balance_sheet === 'EKUITAS'){
                amount_ekuitas = parseInt(amount_ekuitas) + parseInt(value.amount);
            }

        });

        var i = 0;
        $('#home6').append("<div class='clearfix'></div>");
        $('#home6').append("<div id='div1' class='row'><h2>ASET</h2>");
        $('#div1').append("<div id='div2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total ASET</th></tr><tr><td style='background:#A6DCC8;'><h2>"+numberWithCommas(amount_aset_lancar)+"</h2></td></tr></table></div>");
        $('#div1').append("<div id='div3' class='col-md-6'><table id='table1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");

        $('#profile6').append("<div class='clearfix'></div>");
        $('#profile6').append("<div id='div_tl1' class='row'><h2>ASET</h2>");
        $('#div_tl1').append("<div id='div_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total ASET</th></tr><tr><td style='background:#FFBEBC;'><h2>"+numberWithCommas(amount_aset_tidak)+"</h2></td></tr></table></div>");
        $('#div_tl1').append("<div id='div_tl3' class='col-md-6'><table id='table_tl1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");

        $('#home6').append("<div id='div_lia1' class='row'><h2>LIABILITAS</h2>");
        $('#div_lia1').append("<div id='div_lia2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total LIABILITAS</th></tr><tr><td style='background:#A6DCC8;'><h2>"+numberWithCommas(amount_liabilitas_lancar)+"</h2></td></tr></table></div>");
        $('#div_lia1').append("<div id='div_lia3' class='col-md-6'><table id='table_lia1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");

        $('#profile6').append("<div id='div_lia_tl1' class='row'><h2>LIABILITAS</h2>");
        $('#div_lia_tl1').append("<div id='div_lia_tl2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total LIABILITAS</th></tr><tr><td style='background:#FFBEBC;'><h2>"+numberWithCommas(amount_liabilitas_tidak)+"</h2></td></tr></table></div>");
        $('#div_lia_tl1').append("<div id='div_lia_tl3' class='col-md-6'><table id='table_lia_tl1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");

        $('#home6').append("<div id='div_eku_1' class='row'><h2>EKUITAS</h2>");
        $('#div_eku_1').append("<div id='div_eku_2' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total EKUITAS</th></tr><tr><td style='background:#A6DCC8;'><h2>"+numberWithCommas(amount_ekuitas)+"</h2></td></tr></table></div>");
        $('#div_eku_1').append("<div id='div_eku_3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");


        $('#profile6').append("<div id='div_eku_tl1' class='row'><h2>EKUITAS</h2>");
        $('#div_eku_tl1').append("<div id='div_eku_tl1' class='col-md-6'> <table class='table table-bordered' style='width: 70%;text-align: center;    margin: 0px auto;'><tr><th style='background:#ffffff; color: #E00000;'>Total EKUITAS</th></tr><tr><td style='background:#FFBEBC;'><h2>"+numberWithCommas(amount_ekuitas)+"</h2></td></tr></table></div>");
        $('#div_eku_tl1').append("<div id='div_eku_tl3' class='col-md-6'><table id='table_eku_1' class='table table-striped'><tr><th>Item</th><th>Amount(IDR)</th></tr>");

        $.each(result.bs, function( key, value ) {

            if ((value.balance_sheet === 'ASET')&&(value.jenis === 'LANCAR')){





               $('#table1').append("<tr><td>"+value.faktor+"</td><td>"+numberWithCommas(value.amount) +"</td></tr>");


           }else if ((value.balance_sheet === 'ASET')&&(value.jenis === 'TIDAK LANCAR')){

            $('#table_tl1').append("<tr><td>"+value.faktor+"</td><td>"+numberWithCommas(value.amount) +"</td></tr>");
        } if ((value.balance_sheet === 'LIABILITAS')&&(value.jenis === 'LANCAR')){



            $('#table_lia1').append("<tr><td>"+value.faktor+"</td><td>"+numberWithCommas(value.amount) +"</td></tr>");
        }else if ((value.balance_sheet === 'LIABILITAS')&&(value.jenis === 'TIDAK LANCAR')){


            $('#table_lia_tl1').append("<tr><td>"+value.faktor+"</td><td>"+numberWithCommas(value.amount) +"</td></tr>");
        }else if (value.balance_sheet === 'EKUITAS'){


            $('#table_eku_1').append("<tr><td>"+value.faktor+"</td><td>"+numberWithCommas(value.amount) +"</td></tr>");
        }
        
    });   


    }




});
function numberWithCommas(x) {
   // console.debug("test"+x);
   
   return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
   
}


function format_round(rp){

    if(rp<1 && rp>0){
        return (Math.round(rp*100))+'%';

    }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
  }else{
    return ("Rp "+numberWithCommas(rp));
}
}


</script>
