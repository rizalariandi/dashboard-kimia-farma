

<style>
table tbody tr td.ttl{
    background: #F7FAFC;
    text-align: center;
    font-weight: bold;
}
table thead{
    background:#093890;
    text-align:center;
}
</style>

<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12 text-right">
    <div class="col-sm-7">
        Value : <select id="entitas" name="entitas"><?php $ent = ['HJP','HPP','HJP_PTD','HJD','HNA'];
        foreach($ent as $e){
            if($e==$_SESSION['entitas']) {
                echo "<option value='$e' selected>$e</option>";
            }else{
                echo "<option value='$e'>$e</option>";
            }
        } ?></select>
    </div>
    <div class="col-sm-7 text-left" id="reportrange" style="background: #fff; cursor: pointer;  border: 1px solid #ccc; width: 30%">
        <i class="fa fa-calendar"></i>&nbsp;
        <span></span> <i class="fa fa-caret-down"></i>
    </div>
    <input type='submit' id="btnSubmit" value='Filter' class='btn btn-success'/>

</div>
</div>
<div class="row" style="background:white;padding: 10px;">

  <div class="col-md-12">
    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Overview Marketing</h4>
  </div>
</div>
</div>

<div class="row" style="background:white;padding: 10px;">
 <div class="col-md-6">
    <div>
        <table class="table table-bordered">
            <thead><tr><th>KFTD</th></tr></thead>
            <tbody><tr><td id="percent-kftd" class='ttl'>0%</td></tr></tbody>
        </table>
    </div>
</div>
<div class="col-md-6">
    <div>
        <table class="table table-bordered">
            <thead><tr><th>OTHER</th></tr></thead>
            <tbody><tr><td id="percent-other" class='ttl'>0%</td></tr></tbody>
        </table>
    </div>
</div>
</div>


<div class="row" style="background:white;padding: 10px;">
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>Sales Today</th></tr></thead>
                <tbody><tr><td id="sales-today" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>WTD</th></tr></thead>
                <tbody><tr><td id="wtd" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>MTD</th></tr></thead>
                <tbody><tr><td id="mtd" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>YTD</th></tr></thead>
                <tbody><tr><td id="ytd" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>%Growth</th></tr></thead>
                <tbody><tr><td id="growth" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>%Achieve</th></tr></thead>
                <tbody><tr><td id="achieve" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead><tr><th>%Market Exp</th></tr></thead>
                <tbody><tr><td id="market_exp" class='ttl'>RP 0</td></tr></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-2">
        <div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2" style="font-size: 1em;">Kontribusi Sales</th>
                    </tr>
                    <tr>    
                        <th style="font-size: 0.75em; padding: 0;text-align: center;">KF %</th>
                        <th style="font-size: 0.75em; padding: 0;text-align: center;">Non KF %</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="percent-kf" class='ttl'  style="font-size: 0.75em; padding: 10px">0%</td>
                        <td id="percent-nonkf" class="ttl"  style="font-size: 0.75em; padding: 10px">0%</td>
                    </tr></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="background:white;padding: 10px;">
        <div class="col-md-12">
            <h4 class="page-title">Distribusi Achievement</h4>
            <img src="<?=base_url()?>asset/map_sample.png" width="1100px">
        </div>
    </div> 

    <div class="row" style="background:white;padding: 10px;">
        <div class="col-md-4">

           <center><h3 class="panel panel-title">Ranking Sales By All</h3></center>

           <div class="panel panel-body" id="rankingsales" ></div>
       </div>
       <div class="col-md-4">

           <center><h3 class="panel panel-title">Konstribusi Sales by Lini</h3></center>

           <div class="panel panel-body" id="salesbylini" ></div>
           <div id="addText" style="position:absolute; left:0px; top:0px;"></div>

       </div>
       <div class="col-md-4">

           <center><h3 class="panel panel-title">Trend Sales</h3></center>

           <div class="panel panel-body" id="trendsales" ></div>
       </div>
   </div> 
   <div class="row" style="background:white;padding: 10px;">
    <div class="col-md-4">

       <center><h3 class="panel panel-title">Sales Customer Segment</h3></center>

       <div class="panel panel-body" id="salescustomersegment" ></div>
   </div>
   <div class="col-md-4">

       <center><h3 class="panel panel-title">Top 5 Brand</h3></center>

       <div class="panel panel-body" id="top5brand"></div>
   </div>
   <div class="col-md-4">

    <center><h3 class="panel panel-title">Product By Sales</h3></center>
    
    <div class="panel panel-body" >
        <div>
            <table class="table" id='productsales_table'>
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Amount</th>
                        <th>Stock on Hand</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ACYCLOVIR 400MG TABLET 100</td>
                        <td>12738</td>
                        <td>527512538</td>
                        <td>527512538</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div> 

<script type="text/javascript">
    var startDate;
    var endDate;
    $(function() {

        // var start = moment().subtract(29, 'days');

        var start = moment('2018-12-25');
        var end = moment();

        
       //  function cb(start, end) {
       //      $('#reportrange span').html(start.format('DD-MMM-YYYY') + ' - ' + end.format('DD-MMM-YYYY'));
       //      startDate = start.format('YYYY-MM-DD');
       //      endDate = end.format('YYYY-MM-DD');
       //  }

       //  $('#reportrange').daterangepicker({
       //      startDate: start,
       //      endDate: end,
       //      opens: 'left',
       //      ranges: {
       //         'Today': [moment(), moment()],
       //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       //         'This Month': [moment().startOf('month'), moment().endOf('month')],
       //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
       //     }
       // }, cb);

       //  cb(start, end);
       function cb(start) {
            $('#reportrange span').html(start.format('DD-MMM-YYYY') );
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
        }
            $('#reportrange').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                opens: 'left',
                maxYear: parseInt(moment().format('YYYY'),10)
              },  cb)
        
        cb(start);

    });

    $('#btnSubmit').click(function(e){

        e.preventDefault();

        var urlpost = "<?php echo base_url('index.php/page/refresh_overview_marketing') ?>";

        var startdate = startDate;
        var enddate = endDate;

        var entitas = $('#entitas').val();
     // console.debug(month+"-"+year+"-"+entitas);
     $.post(
        urlpost,
        {
          'startDate': JSON.stringify(startdate),
          'endDate': JSON.stringify(enddate),
          'entitas': JSON.stringify(entitas)
          
      },
      AjaxSucceeded,"json"

      );
     function AjaxSucceeded(result) {
        $('#percent-kftd').text("0%");
        $('#percent-other').text("0%");
        $('#sales-today').text("0");
        $('#wtd').text("0");
        $('#mtd').text("0");
        $('#ytd').text("0");
        $('#growth').text("0%");
        $('#achieve').text("0%");
        $('#market_exp').text("0%");
        $('#percent-kf').text("0%");
        $('#percent-nonkf').text("0%");
        var percent_kftd = (result.sum_kftd[0].total/result.sum_all[0].total) * 100;
        var percent_other = (result.sum_other[0].total/result.sum_all[0].total) * 100;
        if (isNaN(percent_kftd)){
            $('#percent-kftd').text("0%");
        }else{
            $('#percent-kftd').text(percent_kftd.toFixed(0) + "%");
        }

        if (isNaN(percent_other)){
             $('#percent-other').text("0%");
        }else{
             $('#percent-other').text(percent_other.toFixed(0) + "%");
        }
        var sales_today = result.sales_today[0].total;
        if (isNaN(sales_today)){
             $('#sales-today').text("Rp 0");
        }else{
             $('#sales-today').text(format_round_2(sales_today));
        }
        var wtd = result.wtd[0].total;
        if (isNaN(wtd)){
             $('#wtd').text("Rp 0");
        }else{
             $('#wtd').text(format_round_2(wtd));
        }
        var mtd = result.mtd[0].total;
        if (isNaN(mtd)){
             $('#mtd').text("Rp 0");
        }else{
             $('#mtd').text(format_round_2(mtd));
        }
        var ytd = result.ytd[0].total;
        if (isNaN(ytd)){
             $('#ytd').text("Rp 0");
        }else{
             $('#ytd').text(format_round_2(ytd));
        }
        var sales_kf = (result.sales_kf[0].total/result.sum_all[0].total) * 100;
        var sales_nonkf = (result.sales_nonkf[0].total/result.sum_all[0].total) * 100;

        if (isNaN(sales_kf)){
            $('#percent-kf').text("0%");
        }else{
            $('#percent-kf').text(sales_kf.toFixed(0)+"%");
        }

        if (isNaN(sales_nonkf)){
             $('#percent-nonkf').text("0%");
        }else{
             $('#percent-nonkf').text(sales_nonkf.toFixed(0)+"%");
        }
         
        
     


}
});


    Highcharts.chart('trendsales', {
        chart: {
            type: 'line'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: ['Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Persent %'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Non KFA',
            data: [34, 74, 14]
        }, {
            name: 'KFA',
            data: [66, 26, 35]
        }]
    });

    Highcharts.chart('salescustomersegment', {
        chart: {
            type: 'bar'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: ['Rumah Sakit', 'Apotik', 'Izin PBF', 'Dinas', 'Toko/Warung'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Population (millions)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Rupiah',
            data: [45, 30, 6, 3, 1]
        }]
    });

    Highcharts.chart('top5brand', {
        chart: {
            type: 'bar'
        },
        title: {
            text: "Brand"
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: ['Fentanil', 'Retinol', 'Nitrokaf', 'Marck"s', 'Albumin'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rupiah (millions)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Rupiah',
            data: [40711123706, 11257988700, 7447074444, 6502126790, 5341112007]
        }]
    });

    Highcharts.chart('rankingsales', {
        chart: {
            type: 'bar'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: ['KFTD Makasar', 'KFTD Sidoarjo', 'KFTD Medan', 'KFTD Surakarta', 'KFTD Yogyakarta', 'KFTD Padang', 'KFTD Malang']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Rupiah in KFTD'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: '2018',
            data: [5, 3, 4, 7, 2,7,9]
        }, {
            name: 'Target',
            data: [2, 2, 3, 2, 1,12,15]
        }, {
            name: '2019',
            data: [3, 4, 4, 2, 5 ,20 ,25]
        }]
    });
      //chart Kontribusi by Lini
      function renderIcons() {
    // Move icon
    if (!this.series[0].icon) {
        this.series[0].icon = this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
        .attr({
            'stroke': '#303030',
            'stroke-linecap': 'round',
            'stroke-linejoin': 'round',
            'stroke-width': 2,
            'zIndex': 10
        })
        .add(this.series[2].group);
    }
    this.series[0].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[0].points[0].shapeArgs.innerR -
        (this.series[0].points[0].shapeArgs.r - this.series[0].points[0].shapeArgs.innerR) / 2
        );

    // Exercise icon
    if (!this.series[1].icon) {
        this.series[1].icon = this.renderer.path(
            ['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8,
            'M', 8, -8, 'L', 16, 0, 8, 8]
            )
        .attr({
            'stroke': '#ffffff',
            'stroke-linecap': 'round',
            'stroke-linejoin': 'round',
            'stroke-width': 2,
            'zIndex': 10
        })
        .add(this.series[2].group);
    }
    this.series[1].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[1].points[0].shapeArgs.innerR -
        (this.series[1].points[0].shapeArgs.r - this.series[1].points[0].shapeArgs.innerR) / 2
        );

    // Stand icon
    if (!this.series[2].icon) {
        this.series[2].icon = this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
        .attr({
            'stroke': '#303030',
            'stroke-linecap': 'round',
            'stroke-linejoin': 'round',
            'stroke-width': 2,
            'zIndex': 10
        })
        .add(this.series[2].group);
    }

    this.series[2].icon.translate(
        this.chartWidth / 2 - 10,
        this.plotHeight / 2 - this.series[2].points[0].shapeArgs.innerR -
        (this.series[2].points[0].shapeArgs.r - this.series[2].points[0].shapeArgs.innerR) / 2
        );
}
$(function() {
  // Create the chart

   Highcharts.setOptions({
     colors: ['#F0635E', '#50B451']
    });
   var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'salesbylini',
            type:'pie'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr']
        },
        
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },
        
        series: [{
            data: [10000, 0]        
        }]
    });
});

function numberWithCommas(x) {
    //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if (x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
        return 0;
    }
    
}

function format_round(rp){

    if(rp<1 && rp>0){
        return (Math.round(rp*100))+'%';

    }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
      return "Rp "+numberWithCommas(Math.round(rp/Math.pow(10,9)))+'M';
  }else{
    return ("Rp "+numberWithCommas(rp));
}
}

function format_round_2(rp){
    if(rp<1 && rp>0){
        return (Math.round(rp*100))+'%';
    }else if(Math.round(rp).toString().length >= 10){
      //  console.debug((rp/Math.pow(10,9))+'M');
        return "Rp "+numberWithCommas((rp/Math.pow(10,9)).toFixed(2))+'M';
    }else if(Math.round(rp).toString().length >= 7 & Math.round(rp).toString().length <= 9){
        return "Rp "+numberWithCommas((rp/Math.pow(10,6)).toFixed(2))+'Jt';
    }else{
        return ("Rp "+numberWithCommas(rp));
    }
}

</script>