<style>
	.table{
		font-size:1
	}
	.card-category{
		font-size: 10px;
	}
	.value-card{
		font-size:12px;
		color : #6610f2;
	}
	.my-canvas {
	overflow-x: auto;
	overflow-y: hidden;
	white-space: nowrap;
	}
	.my-canvas .canvas-body{
	display:block;
	}
	.my-canvas .canvas-body .canvas-dom {
	display: inline-block;
	float: none;
	padding: 15px;
	border: 1px solid indigo;
	}
	th, td { white-space: nowrap; font-size:10px; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
	#myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background-color: #1565c0;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 4px;
}
    div.container {
        margin: auto;
		padding: 10px;
    }
    .stage {
      
    }
    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    .bounce-6 {
        animation-name: bounce-6;
        animation-timing-function: ease;
    }
    @keyframes bounce-6 {
        0%   { transform: scale(1,1)      translateY(0); }
        10%  { transform: scale(1.1,.9)   translateY(0); }
        30%  { transform: scale(.9,1.1)   translateY(-100px); }
        50%  { transform: scale(1.05,.95) translateY(0); }
        57%  { transform: scale(1,1)      translateY(-7px); }
        64%  { transform: scale(1,1)      translateY(0); }
        100% { transform: scale(1,1)      translateY(0); }
    }
</style>

<div class="col-md-8" id="content-side">
	<div class="card">
		<div class="panel-header bg-primary-gradient">
			<div class="page-inner py-5">
				<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
					<div>
						<h2 class="text-white pb-2 fw-bold">Report Sales Summary by Lini, Layanan, & Item Group </h2>	
					</div>
				</div>
			</div>
		</div>
		<div class="page-inner mt--5">
		
			<div class="row mt--2">
				<!--<div class="stage" >
    					<div class="box bounce-6"> <h1 style="color:white;">Loading...</h1></div>
					</div>		-->
                <div class="card full-hight col-md-12" id="table-data-filter" style="z-index: 3;">
                    <div class="form-inline">    
                        <div class="col-md-6">
						<form id="form-header">
                            <div class="form-group">
								<div class="col-md-2">
									<label for="" style="margin-left:">Table Name</label>
								</div>	
	                            <div class="col-md-2">
									<label for="" style="margin-left:">:</label>
								</div>
								<div class="col-md-4">
									<!--<select onchange="choosetable(jQuery(this).val())" name="head_filter" id="head_filter" class="form-control select2">-->
									<select style="width:120%;" onchange="choosetable('m')" name="head_filter" id="head_filter" class="form-control select2">
										<option value="" selected></option>
										<option value="Month" data-badge="">Month</option>
										<option value="distributor_code" >Distributor</option>
										<option value="branch_code" >Branch</option>
										<option value="gpm_pm_code" >PM</option>
										<option value="rsm_code" >RSM</option>
										<option value="shopper_code" >Shopper</option>
										<option value="am_apm_asm_code" >AM</option>
										<option value="msr_md_se_code" >SE</option>
										<option value="segment" >Segment</option>
										<option value="customer_code" >Customer</option>
										<option value="layanan_code" >Layanan</option>
										<option value="lini_code" >Lini</option>
										<option value="material_group1_code" >Group 1</option>
										<option value="material_group2_code" >Group 2</option>
										<option value="material_group3_code" >Group 3</option>
										<option value="brand" >Brands</option>
										<option value="material_code" >Produk</option>
									</select>
								</div>
                            </div>
                        </div>
						<div class="col-md-2"></div>

						<!--<div class="col-md-4">
                            <div class="form-group">
								<div class="col-md-2">
									<label for="head-display" style="margin-left:">Choose Display :</label>
								</div>	
	                            <div class="col-md-2">
									<label for="" style="margin-left:"></label>
								</div>
                            </div>
                        </div>-->
						</form>
						
						
							<div class="col-md-12">
								<button style=" position: absolute;right: 0;"class="btn btn-sm btn-primary btn-generate" onclick=' var filename =&quot;<?= "report_sales_summary" ?>&quot;; var url = &quot;<?= base_url("report_sales_summary/downloadexcel");?>&quot;; generateexcel(url,filename);'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>
							</div>
							<div class="col-md-12">
								<button style=" position: absolute;right: 0;display:none" class="btn btn-sm btn-danger btn-download" onclick=' var filename =&quot;<?= "report_sales_summary" ?>&quot;; var url = &quot;<?= base_url("report_sales_summary/downloadexcel");?>&quot;; downloaddataexcel(url,filename);' ><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>
							
						</div>
					
					</div>
						
						<div class="my-table" style="margin-top:5%">
							<!--<table id="datatablesE" class="table table-striped table-bordered" width="100%" style="clear:both;layout:fixed;width:400px;">-->
							<table id="datatablesE" class="table table-striped table-bordered" style="width:100%;">
								<thead>
									<tr>
										<th id="th-grand"><b>Monthly</b></th>
										<th id="th-qtytarget">Target QTY <b></b></th>
										<th id="th-target">Target (Value)<b></b></th>
										<th id="th-qtyterjual">Realisasi Qty <b></b></th>
										<th id="th-realisasi">Realisasi (Value)<b></b></th>
										<th id="th-disc">Last Year QTY</th>
										<th id="th-hjp">Last Year (Value) <b></b></th>
										<th id="th-hjp">PTD <b></b></th>
										<th id="th-achievment">Achiev( % )</th>
										<th id="th-sales">Growth ( % )</th>
										<th id="th-disc-prev">Noo</th>
										<th id="th-disc-prev">OTR</th>
										<th id="th-realisasi-prev">Margin<b></b></th>
									</tr>
								</thead>
							</table>
							
						</div>
						
						<div class="my-canvas">
							<div class="canvas-body"></div>
						</div>
				</div>	
				
			</div>	
		</div>	
	</div>
</div>
<script>
function generateexcel(url,filename){
    if(jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#value").val() == "" || jQuery("#head_filter").val() == "" )  {
		swal("Pastikan tanggal faktur, Value terisi, Filter table name terisi");
		return false;
	}  else{
		
		jQuery(".btn-generate").hide();
		
		
		//console.log(filename);
			printdata(url,filename);
	}
    }
    function downloaddataexcel(url,filename){
		jQuery(".btn-generate").show();
		jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open(jQuery('#basedurl').val()+"/file/"+filename+".xls","","_blank","height=1000,width=800");
    }
</script>
