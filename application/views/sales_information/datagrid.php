<input type="hidden" id="pathData" value="<?= base_url('index.php/' . $this->uri->segment('3')) ?>">
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" rel="stylesheet" type="text/css">
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js "></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>

<?php
$config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$config['base_url'] .= "://" . $_SERVER['HTTP_HOST'];
$config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
$config['base_url'] .= "";

?>
<style>
    .last_update {
        position: absolute;
        padding-left: 5px;

        font-weight: normal;
        font-size: 12px;
        line-height: 10px;
        /* identical to box height, or 167% */


        color: #cb3935;

        font-family: Roboto;

    }

    .dataTables_processing {
        z-index: 3000;
    }

    
    .btn-generate {
        background-color: #08388F;
        color: #fff;
    }

    table thead {
        background: #F79966;
        text-align: center;
        color: white;
    }

    .table {
        font-size: 1
    }

    .card-category {
        font-size: 12px;
        background: #093890;
    }

    .value-card {
        font-size: 12px;

    }

    .my-canvas {
        overflow-x: auto;
        overflow-y: hidden;
        white-space: nowrap;
    }

    .my-canvas .canvas-body {
        display: block;
    }

    .my-canvas .canvas-body .canvas-dom {
        display: inline-block;
        float: none;
        padding: 15px;
        border: 1px solid indigo;
    }

    th,
    td {
        white-space: nowrap;
        font-size: 10px;
    }

    div.dataTables_wrapper {
        margin: 0 auto;
    }

    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #1565c0;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    div.container {
        margin: auto;
        padding: 10px;
    }

    .stage {}

    .box {
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }

    .bounce-6 {}

    @keyframes bounce-6 {
        0% {
            transform: scale(1, 1) translateY(0);
        }

        10% {
            transform: scale(1.1, .9) translateY(0);
        }

        30% {
            transform: scale(.9, 1.1) translateY(-100px);
        }

        50% {
            transform: scale(1.05, .95) translateY(0);
        }

        57% {
            transform: scale(1, 1) translateY(-7px);
        }

        64% {
            transform: scale(1, 1) translateY(0);
        }

        100% {
            transform: scale(1, 1) translateY(0);
        }
    }

    /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
    .modal {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading .modal {
        overflow: hidden;
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modal {
        display: block;
    }
</style>
<div class="back-to-top">
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <center>
                    <h3 class="box-title">Sales Information</h3>
                    <center>
                        <h4 id="ket" class="page-title last_update" style="color: #cb3935;"><sup>*</sup>Note:Mandatory field</h4>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-targetqty">
                            Target Qty<b style="font-color:red;"></b>
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="val-targetqty" id="val-targetqty" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-targetvalue">
                            Target (Value)<b style="font-color:red;"></b>
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="val-targetvalue" id="val-targetvalue" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-realisasiqty">
                            Realisasi Qty
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-realisasiqty" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-realisasivalue">
                            Realisasi (Value) <b style="font-color:red;"></b>
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-realisasivalue" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-lastyearqty">
                            Last Year Qty
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-lastyearqty" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-lastyearvalue">
                            Last Year (Value)
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-lastyearvalue" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-ptd">
                            PTD
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-ptd" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-achievement">
                            Achievment (%)
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-achievement" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-growth">
                            Growth (%)
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-growth" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-noo">
                            NOO
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-noo" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-npp">
                            NPP
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-npp" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-otr">
                            OTR
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-otr" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="border-radius:10%;">
            <div class="panel panel-default">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="card-category" align="center" id="c-margin">
                            Margin (Rp)
                        </div>
                        <div class="m-t-15 collapseblebox dn">
                            <div class="well">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="value-card" for="" id="val-margin" align="center">0</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!--<div class="stage">
                <div class="box bounce-6">
                    <h1>Loading...</h1>
                </div>
            </div>-->
            <div class="col-md-12">

                <div class="white-box" style="padding-bottom:200%">
                    <div class="card full-hight col-md-12" id="table-data-filter">
                        <div class="col-md-6" id="table-name">

                            <form id="form-header">
                                <div class="form-inline">
                                    <div class="col-md-6">
                                        <!-- <label for="" style="margin-left:">Table Name* :</label> -->
                                        <label for="" >Table Name* :</label>
                                            <select id="filter_combo" name="head_filter" id="head_filter" class="form-control select2">
                                                <option value=""></option>
                                                <option value="Month" selected data-badge="">Month</option>
                                                <option value="distributor_code">Distributor</option>
                                                <option value="branch_code">Branch</option>
                                                <option value="gpm_pm_code">PM</option>
                                                <option value="rsm_code">RSM</option>
                                                <option value="shopper_code">Shopper</option>
                                                <option value="am_apm_asm_code">AM</option>
                                                <option value="msr_md_se_code">SE</option>
                                                <option value="segment">Segment</option>
                                                <option value="customer_code">Customer</option>
                                                <option value="layanan_code">Layanan</option>
                                                <option value="layanan_group">Layanan Group</option>
                                                <option value="lini_code">Lini</option>
                                                <option value="material_group1_code">Group 1</option>
                                                <option value="material_group2_code">Group 2</option>
                                                <option value="material_group3_code">Group 3</option>
                                                <option value="brand">Brands</option>
                                                <option value="material_code">Produk</option>
                                            </select>
                                    </div>
                            </form>
                            <!-- <div style="">
                                <label for="" style="margin-left:">Choose Display : </label>
                                <select onchange="choosedisplay(jQuery(this).val())" name="display_filter" id="display_filter" class="form-control select2">
                                    <option value="all" selected>(All)</option>
                                    <option value="table">Table</option>
                                    <option value="diagram">Diagram</option>
                                </select>
                            </div> -->
                            </br>
                            </br>
                            </br>

                        </div>

                    </div>

                    <div style="padding-bottom:10%"></div>
                    <div class="row">
                        <div class="col-lg-2 col-2 pull-right">
                            <button onclick="excel()" class="btn btn-link pull-right"><i class="glyphicon glyphicon-download-alt"></i> Excel</button></label>
                            <button onclick="csv()" class="btn btn-link pull-right"><i class="glyphicon glyphicon-download-alt"></i> CSV</button></label>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="datatablesE" class="table table table-striped">
                            <thead>
                                <tr>
                                    <th id="th-grand">Name</th>
                                    <th id="th-kode">Kode</th>
                                    <th id="th-qtytarget">Target QTY <b></b></th>
                                    <th id="th-target">Target (Value)<b></b></th>
                                    <!-- <th id="th-disc-prev">HNA Qty</th>
                                <th id="th-disc-prev">HNA Target</th>
                                <th id="th-disc-prev">HNA Nett Qty</th>
                                <th id="th-disc-prev">HNA Nett Target</th> -->
                                    <th id="th-qtyterjual">Realisasi Qty <b></b></th>
                                    <th id="th-realisasi">Realisasi (Value)<b></b></th>
                                    <th id="th-disc">Last Year QTY</th>
                                    <th id="th-hjp">Last Year (Value) <b></b></th>
                                    <th id="th-hjp">PTD <b></b></th>
                                    <th id="th-achievment">Achiev( % )</th>
                                    <th id="th-sales">Growth ( % )</th>
                                    <th id="th-qrytarget-prev">Share Realisasi ( % ) <b></b></th>
                                    <th id="th-salestarget">Share Last Year( % ) <b></b></th>
                                    <th id="th-realisasi-prev">Margin Marketing Amount<b></b></th>
                                    <th id="th-realisasi-prev">Margin Marketing ( % )<b></b></th>
                                    <th id="th-realisasi-prev">Margin Distributor Amount<b></b></th>
                                    <th id="th-realisasi-prev">Margin Distributor ( % )<b></b></th>
                                    <th id="th-disc-prev">Noo</th>
                                    <th id="th-disc-prev">Npp</th>
                                    <th id="th-disc-prev">OTR</th>
                                </tr>
                            </thead>
                            <tfoot align="right">
                                <tr>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                    <th style="color: #000000;"></th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>

                    <div class="col-md-12" style="display:none;">

                        <button style=" position: absolute;left: 0;background-color:#073990;color:white" class="btn btn-sm btn-generate" onclick=' var filename =&quot;<?= "sales_information" ?>&quot;; var url = &quot;<?= base_url("index.php/sales_information/downloadexcel"); ?>&quot;; generateexcel(url,filename);return false;'><i class="fa fa-download" aria-hidden="true"></i> Generate Excel Data</button>

                        <button style=" position: absolute;left: 0;display:none;background-color:#073990;color:white" class="btn btn-sm btn-danger btn-download" onclick=' var filename =&quot;<?= "sales_information" ?>&quot;; var url = &quot;<?= base_url("index.php/sales_information/downloadexcel"); ?>&quot;; downloaddataexcel(url,filename);return false;'><i class="fa fa-download" aria-hidden="true"></i> Download Excel File</button>
                    </div>
                    </br>
                    </br>
                    <div class="my-canvas">
                        <div class="canvas-body"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>

</div>
</div>

<script>
    function generateexcel(url, filename) {
        if (jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#value").val() == "" || jQuery("#head_filter").val() == "") {
            swal("Pastikan tanggal faktur, Value terisi, Filter table name terisi");
            return false;
        } else {

            jQuery(".btn-generate").hide();


            //console.log(filename);
            printdata(url, filename);
        }
    }

    function downloaddataexcel(url, filename) {
        jQuery(".btn-generate").show();
        jQuery(".btn-download").hide();
        var path = jQuery("#pathData").val();
        window.open("<?php echo $config['base_url'] ?>/file/" + filename + ".xls", "", "_blank", "height=1000,width=800");
    }
</script>