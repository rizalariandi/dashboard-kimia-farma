        <style>
                .please_wait {
                        font-size: 10px;
                        font-weight: 300;
                        display: none;
                }

                .select_customer {
                        width: 12em;
                        border: 1px solid #aaa;
                }

                @media only screen and (max-width: 1026px) {
                        .sidebar-filtering {
                                display: none;
                        }
                }
        </style>
        <span class="hide-menu">

                <div id="filtering-side" class="col-md-12" style="display:none;">

                        <label for="" id="preparing_desktop"></label>

                        <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height:5%">0%</div>
                        </div>
                        <form action="#" id="form-filter" style="margin-left:10%">
                                <div class="form-group">
                                        <label for="">Periode*</label>
                                        <div class='form-inline'>
                                                <label for="email"></label>
                                                <input style="width:40%;font-size:9px" type="text" class="form-control dates" id="tanggal_faktur_start" name="tanggal_faktur_start">
                                                <label for="pwd"><b>&nbsp;to&nbsp;</b></label>
                                                <input style="width:40%;font-size:9px" type="text" class="form-control dates" id="tanggal_faktur_end" name="tanggal_faktur_end">
                                        </div>
                                        <div class="form-group">
                                                <label for="branch_code">Value* :</label>
                                                <select name="value" id="value" class="form-control filter-select2" style="width: 90%">
                                                        <option value=""></option>
                                                        <option selected value="HNA_TY">HNA</option>
                                                        <option value="HNA_PTD_TY">HNA-PTD</option>
                                                        <option value="HJP_TY">HJP</option>
                                                        <option value="HJP_PTD_TY">HJP-PTD</option>
                                                        <option value="HJD_TY">HJD</option>
                                                        <option value="HPP_TY">HPP</option>
                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="distributor_code" style="cursor:pointer" onclick="saveselected('distributor_code','distributor_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Distributor : </label>
                                                <select name="distributor_code[]" id="distributor_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="branch_code" style="cursor:pointer" onclick="saveselected('branch_code','branch_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Branch : </label>

                                                <select name="branch_code[]" id="branch_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="gpm_pm_code" style="cursor:pointer" onclick="saveselected('gpm_pm_code','jabatan_pm')"><i class="fa fa-refresh" aria-hidden="true"></i> GPM/PM : </label>
                                                <select name="gpm_pm_code[]" id="gpm_pm_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="rsm_code" style="cursor:pointer" onclick="saveselected('rsm_code','jabatan_rsm')"><i class="fa fa-refresh" aria-hidden="true"></i> RSM : </label>
                                                <select name="rsm_code[]" id="rsm_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple"></select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="shopper_code" style="cursor:pointer" onclick="saveselected('shopper_code','jabatan_shopper')"><i class="fa fa-refresh" aria-hidden="true"></i> Shopper : </label>
                                                <select name="shopper_code[]" id="shopper_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="am_apm_asm_code" style="cursor:pointer" onclick="saveselected('am_apm_asm_code','jabatan_am')"><i class="fa fa-refresh" aria-hidden="true"></i> AM/APM/ASM : </label>
                                                <select name="am_apm_asm_code[]" id="am_apm_asm_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="msr_md_se_code" style="cursor:pointer" onclick="saveselected('msr_md_se_code','msr_md_se_name')"><i class="fa fa-refresh" aria-hidden="true"></i> MSR/MD/SE : </label>
                                                <select name="msr_md_se_code[]" id="msr_md_se_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">


                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="segment" style="cursor:pointer" onclick="saveselected('segment')"><i class="fa fa-refresh" aria-hidden="true"></i> Segment : </label>
                                                <select name="segment[]" id="segment" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <!-- <div class="form-group">
                                        <div class="form-group customer_code">

                                                <label for="customer_code" style="cursor:pointer" onclick="selectedfilter_customer('customer_code')"><i class="fa fa-refresh" aria-hidden="true"></i> Customer :
                                                        <span class="please_wait">Please wait...</span>
                                                </label>
                                                <select name="customer_code[]" id="customer_code" class="form-control filter-multiple-select-typing" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div> -->
                                <div class="form-group">
                                        <div class="form-group customer_code">
                                                <label for="customer_code" style="cursor:pointer" onclick="selectedfilter_customer('customer_code','customer_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Customer :
                                                        <span class="please_wait">Please wait...</span>
                                                </label>
                                                <div style="margin-bottom: 15px;">
                                                        <input list="select_customer" oninput="selected_customer()" placeholder="Please Typing" class="form-control select_customer">
                                                        <datalist id="select_customer">
                                                        </datalist>
                                                </div>
                                                <select name="customer_code[]"  id="customer_code" class="form-control filter-multiple-select-typing" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="layanan_group" style="cursor:pointer" onclick="saveselected('layanan_group')"><i class="fa fa-refresh" aria-hidden="true"></i> Group Layanan : </label>
                                                <select name="layanan_group[]" id="layanan_group" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple"></select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="layanan_code" style="cursor:pointer" onclick="saveselected('layanan_code','layanan_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Layanan : </label>
                                                <select name="layanan_code[]" id="layanan_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple"></select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="lini_code" style="cursor:pointer" onclick="saveselected('lini_code','lini_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Lini : </label>
                                                <select name="lini_code[]" id="lini_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="material_group1_code" style="cursor:pointer" onclick="saveselected('material_group1_code','material_group1_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Group 1 : </label>
                                                <select name="material_group1_code[]" id="material_group1_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">
                                                <label for="material_group2_code" style="cursor:pointer" onclick="saveselected('material_group2_code','material_group2_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Group 2 : </label>
                                                <select name="material_group2_code[]" id="material_group2_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="material_group3_code" style="cursor:pointer" onclick="saveselected('material_group3_code','material_group3_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Group 3 : </label>
                                                <select name="material_group3_code[]" id="material_group3_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="brand" style="cursor:pointer" onclick="saveselected('brand')"><i class="fa fa-refresh" aria-hidden="true"></i> Brands : </label>
                                                <select name="brand[]" id="brand" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <div class="form-group">

                                                <label for="material_code" style="cursor:pointer" onclick="saveselected('material_code','material_name')"><i class="fa fa-refresh" aria-hidden="true"></i> Produk : </label>
                                                <select name="material_code[]" id="material_code" class="form-control filter-multiple-select2" style="width: 90%" multiple="multiple">

                                                </select>
                                        </div>
                                </div>

                        </form>
                        <div class="form-group" align="center">
                                <!--<button id="btn-filter" class="btn btn-primary btn-block rounded" onClick="jQuery(this).text('Loading');tableses.ajax.reload(null,true);getcardsum();jQuery(this).text('Apply');drawmycanvasbar();return false;">Apply</button>-->
                                <button id="btn-filter" class="btn btn-primary" onClick="$('html, body').animate({ scrollTop: $('body').offset().top }, 'slow');choosetable('m')"><i class="fa fa-search" aria-hidden="true"></i> Apply</button>
                        </div>
                </div>
        </span>