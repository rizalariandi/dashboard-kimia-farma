<style>
	.table{
		font-size:1
	}
</style>
<div class="col-md-8" id="content-side">
	<div class="card">
		<div class="panel-header bg-primary-gradient">
			<div class="page-inner py-5">
				<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
					<div>
						<h2 class="text-white pb-2 fw-bold">Sales Growth Information (Monthly Sales)</h2>	
					</div>
				</div>
			</div>
		</div>
		<div class="page-inner mt--5">
			<div class="row mt--2">
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
								<label for="val-qtysales" id="val-qtysales"><?= $qtysales2019 ?></label>
							</div>
							<div class="card-category" align="center" id="c-qtysales">
								QTY Sales <b style="font-color:red;"></b>
							</div>	
						</div>
                    </div>
                </div>   
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange; font-size:13px" >
							<label for="" id="val-realisasi"><?= $realisasi2019 ?></label>
							</div>
							<div class="card-category" align="center" id="c-realisasi"> 
								Realisasi <b style="font-color:red;"></b> ( Rp)
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
							<label for="" id="val-qtysales-prev"><?= $qtysales2018 ?></label>
							</div>
							<div class="card-category" align="center" id="c-qtysales-prev">
								QTY Sales <b style="font-color:red;"></b>
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
							<label for="" id="val-realisasi-prev"><?= $realisasi2018 ?></label>
							</div>
							<div class="card-category" align="center" id="c-realisasi-prev">
							Realisasi <b style="font-color:red;"></b> ( Rp)
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
							
							<label for="" id="val-ptd"><?= $ptd ?></label>
							</div>
							<div class="card-category" align="center" id="c-ptd">
								PTD (Rp)
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
							<label for="" id="val-hjp"><?= $hjpptd ?></label>
							</div>
							<div class="card-category" align="center" id="c-hjp" >
								HJP - PTD (Rp)
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >							
							<label for="" id="val-salesgrowth"><?= $salesgrowth ?></label>
							</div>
							<div class="card-category" align="center" id="c-sg">
								Sales Growth (%)
							</div>	
						</div>
                    </div>
                </div>
				<div class="col-md-3">
					<div class="card ">
						<div class="card-body">
							<div class="card-title" align="center" style="color:orange;font-size:13px" >
							<label for="" id="val-hna"><?= $hna ?></label>
							</div>
							<div class="card-category" align="center" id="c-hna" >
								HNA
							</div>	
						</div>
                    </div>
                </div> 
                <div class="card full-hight col-md-12">
                    <div class="form-inline">    
                        <div class="col-md-6">
						<form id="form-header">
                            <div class="form-group">
								<div class="col-md-2">
									<label for="" style="margin-left:">Table Name</label>
								</div>	
	                            <div class="col-md-2">
									<label for="" style="margin-left:">:</label>
								</div>
								<div class="col-md-2">
									<select onchange="choosetable(jQuery(this).val())" name="head_filter" id="head_filter" class="form-control select2">
										<option value=""></option>
										<option value="Month" selected>Month</option>
										<option value="distributor_code" >Distributor</option>
										<option value="branch_code" >Branch</option>
										<option value="PRODUCT_MANAGER" >PM</option>
										<option value="REGIONAL_SALES_MANAGER" >RSM</option>
										<option value="shopper" >Shopper</option>
										<option value="AREA_MANAGER" >AM</option>
										<option value="SE" >SE</option>
										<option value="SEGMENT" >Segment</option>
										<option value="CUSTOMER_CODE" >Customer</option>
										<option value="LAYANAN_CODE" >Layanan</option>
										<option value="Lini" >Lini</option>
										<option value="MATERIAL_GROUP_1" >Group 1</option>
										<option value="MATERIAL_GROUP_2" >Group 2</option>
										<option value="MATERIAL_GROUP_3" >Group 3</option>
										<option value="BRAND" >Brands</option>
										<option value="MATERIAL_CODE" >Produk</option>
									</select>
								</div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
								<div class="col-md-2">
									<label for="head-display" style="margin-left:">Choose Display</label>
								</div>	
	                            <div class="col-md-2">
									<label for="" style="margin-left:">:</label>
								</div>
								<div class="col-md-2">
									<select onchange="choosedisplay(jQuery(this).val())" name="display_filter" id="display_filter" class="form-control select2">
										<option value="all" selected>ALL</option>
										<option value="table">Table</option>
										<option value="diagram">Diagram</option>
									</select>
								</div>
                            </div>
                        </div>
						</form>
						
						<div class="row col-md-4">
							<div class="col-md-2">
							<button class="btn btn-sm btn-primary" onclick='jQuery(this).text(&quot;Loading&quot;); var filename =&quot;<?= date("Y-m-d-H-i-s").rand()."sales_growth_information" ?>&quot;; var url = &quot;<?= base_url("sales_growth_information/downloadexcel");?>&quot;; dataexcelsgi(url,filename);jQuery(this).text(&quot;Download excel&quot;);'><i class="fa fa-download" aria-hidden="true"></i> Download excel</button>
							</div>
							
						</div>
                    </div>
						<div class="my-table">
							<table id="datatablesE" class="table table-striped table-bordered" width="100%">
								<thead>
									<tr>
										<th id="th-grand"><b>Monthly</b></th>
										<th id="th-qtytarget">Qty Target <b></b></th>
										<th id="th-target">Target <b></b></th>
										<th id="th-qtyterjual">Qty Terjual <b></b></th>
										<th id="th-realisasi">Realisasi <b></b></th>
										<th id="th-disc">Disc PTD Ammount</th>
										<th id="th-hjp">HJP-PTD <b></b></th>
										<th id="th-achievment">Achievement</th>
										<th id="th-sales">Sales Growth</th>
										<th id="th-qrytarget-prev">Qty Target <b></b></th>
										<th id="th-salestarget">Sales Target <b></b></th>
										<th id="th-realisasi-prev">Realisasi <b></b></th>
										<th id="th-disc-prev">Disc PTD Ammount</th>
									</tr>
								</thead>
							</table>
						</div>
						
						<div id="my-canvas" class="row">

						</div>
				</div>	
			</div>	
		</div>	
	</div>
</div>
