<style>
    .panel-default .panel-body, .panel-white .panel-body {
        color : #ffffff;
        font-weight: bold;
        background-color : #08388F;
    }
    .panel-default {
        color : #ffffff;
    }
    .panel-default .panel-footer, .panel-white .panel-footer{
        color : #000000;
        background-color : #ffffff;
    }
    #form-header label{
        font-weight: bold;
    }
    #form-filter label{
        font-weight: bold;
    }
</style>
<?php $this->load->view('sales_information/datagrid');?>
