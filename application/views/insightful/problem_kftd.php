<div class="panel panel-default" style="overflow-x: scroll;">
    <div class="panel-heading">Sales Trend Sesuai Cabang</div>

    <table class="table table-striped table-hover table-pointer" id="table1">
        <thead>
        <tr>
            <th>Cabang</th>
            <th>Target</th>
            <th>Realisasi</th>
            <th>Realisasi Tahun lalu</th>
            <th align="center">Achievement</th>
            <th align="center">Growth</th>
            <th align="center">Stock</th>
        </tr>
        </thead>
        <tbody>

        <!--<input type="hidden" name="nama_cabang" id="nama_cabang" value="< ?=$ids  ?>">-->
        <?php foreach($kftd as $k) { ?>
            <tr onclick="brand('kftd', '<?= $k->nama_kftd ?>')">
                <td style="white-space: nowrap"><?= $k->nama_kftd ?></td>
                <td><?= y_num_pad($k->target) ?></td>
                <td><?= y_num_pad($k->realisasi) ?></td>
                <td><?= y_num_pad($k->realisasi_past) ?></td>
                <td align="center"><?= $k->target > 0 ? ($k->realisasi/$k->target)*100 : 100 ?></td>
                <td align="center"><?= $k->realisasi > 0 ? round((($k->realisasi - $k->realisasi_past) / $k->realisasi)*100, 2) : 0 ?></td>
                <td align="center"><?= y_num_pad($k->stock) ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div id="kftd-brand"></div>
<div id="kftd-product"></div>