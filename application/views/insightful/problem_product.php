<div class="panel panel-default" style="overflow-x: scroll;">
    <div class="panel-heading">Varian Produk Brand <?= $title ?></div>

    <table class="table table-striped table-hover table-pointer" id="table1">
        <thead>
        <tr>
            <th>Product</th>
            <th>Target</th>
            <th>Realisasi</th>
            <th>Realisasi Tahun lalu</th>
            <th align="center">Achievement</th>
            <th align="center">Growth</th>
            <th align="center">Stock</th>
        </tr>
        </thead>
        <tbody>
        <input type="hidden" name="nama_produkx" id="nama_produkx" value="<?= $ids ?>">
        <?php $failed = 0;
        if(!empty($product)) { foreach($product as $p) {
            if($p->target >= $p->realisasi) {
                $failed++;
                $bg = 'bg-danger';
            } else {
                $bg = '';
            }

        ?>
            <tr>
                <td class="<?= $bg ?>" style="white-space: nowrap"><?= $p->nama_produk ?></td>
                <td class="<?= $bg ?>"><?= y_num_pad($p->target) ?></td>
                <td class="<?= $bg ?>"><?= y_num_pad($p->realisasi) ?></td>
                <td class="<?= $bg ?>"><?= y_num_pad($p->realisasi_past) ?></td>
                <td class="<?= $bg ?>" align="center"><?= $p->target > 0 ? ($p->realisasi/$p->target)*100 : 100 ?></td>
                <td class="<?= $bg ?>" align="center"><?= $p->realisasi > 0 ? round((($p->realisasi - $p->realisasi_past) / $p->realisasi)*100, 2) : 0 ?></td>
                <td class="<?= $bg ?>" align="center"><?= y_num_pad($p->stock) ?></td>
            </tr>
        <?php } } else { ?>
        <tr>
            <td colspan="7">Data tidak tersedia</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Rekomendasi</div>
    <div class="panel-body">
        <?php
        if($col == 'kftd') {
            echo 'Terdapat '.$failed.' varian produk yang tidak mencapai target di seluruh Indonesia yang tersebar di sejumlah area.';
        } else {
            echo 'Terdapat '.$failed.' varian produk yang tidak mencapai target pada lini .';
        }

        ?>
    </div>
</div>