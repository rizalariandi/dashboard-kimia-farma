<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }
    .table-pointer > thead > tr > th {
        font-weight: normal;
        vertical-align: middle;
        text-align: center;
        background-color: #f79868;
        color: #fff;
        padding: 8px;
    }
    .table-pointer > thead > tr > th.th-lini {
        width: 200px;
        background-color: #4e9ed5;
        color: #fff;
        font-weight: bold;
    }
    .table-pointer > thead > tr > th.th-sm {
        font-size: 13px;
        text-align: center;
    }
    .table-pointer > tbody > tr > td {
        white-space:nowrap;
        font-size: 13px;

        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer > tbody > tr > td.td-lini {
        width: 200px;
        background-color: #eaf4ff;
        color: #323232;
    }
</style>

<div class="white-box">
    <div class="row">
        <div class="col-md-4">
            <input name="periode" id="periode" class="form-control">
        </div>
        <div class="col-md-3">
            <select name="var_lini" id="var_lini" class="form-control">
                <option value="all">Semua Lini</option>
            </select>
        </div>
        <div class="col-md-3">
            <select name="var_layanan" id="var_layanan" class="form-control">
                <option value="all">Semua Layanan</option>
            </select>
        </div>
        <!--<div class="col-md-5">
            <select name="month" id="month" class="form-control">
                <option value="1" <?=($month==1?'selected':'')?>>Januari</option>
                <option value="2" <?=($month==2?'selected':'')?>>Februari</option>
                <option value="3" <?=($month==3?'selected':'')?>>Maret</option>
                <option value="4" <?=($month==4?'selected':'')?>>April</option>
                <option value="5" <?=($month==5?'selected':'')?>>Mei</option>
                <option value="6" <?=($month==6?'selected':'')?>>Juni</option>
                <option value="7" <?=($month==7?'selected':'')?>>Juli</option>
                <option value="8" <?=($month==8?'selected':'')?>>Agustus</option>
                <option value="9" <?=($month==9?'selected':'')?>>September</option>
                <option value="10" <?=($month==10?'selected':'')?>>Oktober</option>
                <option value="11" <?=($month==11?'selected':'')?>>November</option>
                <option value="12" <?=($month==12?'selected':'')?>>Desember</option>
            </select>
        </div>
        <div class="col-md-5">
            <select name="year" id="year" class="form-control">
                <?php foreach($yearOption as $p) { ?>
                    <option value="<?=$p?>" <?=($year==$p?'selected':'')?>><?=$p?></option>
                <?php } ?>
            </select>
        </div>-->
        <div class="col-md-2">
            <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
        </div>
    </div>
</div>

<div class="white-box">
    <div id="chart" style="width: 100%; height: 300px;"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default" style="overflow-x: scroll;">
            <div class="panel-heading">Sales Trend Sesuai Lini</div>
            <table class="table table-striped table-hover table-bordered table-pointer" id="table1" style="text-wrap: none">
                <thead>
                <tr>
                    <th rowspan="2" class="th-lini">Lini</th>
                    <?php for($b=1; $b<=12; $b++) { ?>
                    <th colspan="2">Januari</th>
                    <?php } ?>
                </tr>
                <tr>
                    <?php for($b=1; $b<=12; $b++) { ?>
                    <th class="th-sm">Marketing Expense</th>
                    <th class="th-sm">Realisasi Sales</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="td-lini">OGB</td>
                        <?php for($b=1; $b<=12; $b++) { ?>
                        <td>Rp. 1.000.000</td>
                        <td>Rp. 1.000.000</td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td class="td-lini">Ethical</td>
                        <?php for($b=1; $b<=12; $b++) { ?>
                        <td>Rp. 1.000.000</td>
                        <td>Rp. 1.000.000</td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script>
$(document).ready(function () {
    $('#periode').daterangepicker({
        applyClass: 'bg-primary-600',
        cancelClass: 'btn-light',
        locale: {
            format: 'MM/YYYY'
        },
        showDropdowns: true
    }, function(start, end, label) {
        if(start.format('YYYY') != end.format('YYYY')) {
            alert('Periode harus ditahun yang sama');
            end.setDate(start);
        }
    });

    chart();
});
function chart() {
    Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>Rp. {point.y:.1f} Juta</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                dataLabels: {
                    enabled: true,
                }
            }
        },
        series: [{
            name: 'Marketing Expense',
            color: '#69d0ef',
            data: [49.9, 71.5, 106.4, 129.2]

        }, {
            name: 'Realisasi Sales',
            color: '#ab82c4',
            data: [83.6, 78.8, 98.5, 93.4]

        }]
    });
}
</script>