<script language="JavaScript">
    //var ch_data_area = '< ?= $area ?>';
    var ch_data_realisasi_1 = <?= json_encode(array_values($charts['realisasi_1'])) ?>;
    var ch_data_realisasi = <?= json_encode(array_values($charts['realisasi'])) ?>;

    var ch_data_realisasi_qty_1 = <?= json_encode(array_values($charts['realisasi_qty_1'])) ?>;
    var ch_data_realisasi_qty = <?= json_encode(array_values($charts['realisasi_qty'])) ?>;

    var ch_data_target = <?= json_encode(array_values($charts['target'])) ?>;
    var ch_data_stock = <?= json_encode(array_values($charts['stock'])) ?>;

    var ch_data_target_qty = <?= json_encode(array_values($charts['target_qty'])) ?>;
    var ch_data_stock_qty = <?= json_encode(array_values($charts['stock_qty'])) ?>;

    var ch_data_month = <?= json_encode(array_values($charts['month'])) ?>;
    var ch_data_year = '<?= $vyear ?>';
    var ch_data_year_1 = '<?= $vyear-1 ?>';
</script>