<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
</style>

<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Jenis Pembelian dengan Diskon</h3>
            <div id="resensi" style="width: 100%; height: 220px;"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Jumlah Besaran Diskon yang Diberikan</h3>
            <div id="monetary" style="width: 100%; height: 220px;"></div>
        </div>
    </div>
</div>
<!-- /.row -->
<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>
<script>
    $(document).ready(function () {
        //resensi
        var r_cat = [
            'Diskon 10%',
            'Diskon 20%',
            'Diskon 25%',
            'Diskon 30%',
            'Diskon 50%'
        ];
        var r_data = [{
            showInLegend: false,
            name: 'Jumlah Penjualan',
            data: [19, 22, 38, 70, 109],
            color: '#25457E'
        }];
        columnchart('resensi', r_cat, r_data);

        //resensi
        var r_cat = [
            'Diskon 10%',
            'Diskon 20%',
            'Diskon 25%',
            'Diskon 30%',
            'Diskon 50%'
        ];
        var r_data = [{
            showInLegend: false,
            name: 'Jumlah Penjualan',
            data: [19, 22, 38, 70, 109],
            color: '#25457E'
        }];
        columnchart('monetary', r_cat, r_data);

        maps();
    });

    Highcharts.chart('area-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        },
        yAxis: {
            title: {
                text: 'Sales Profit'
            }/*,
            plotLines: [{
                value: 250000000,
                color: 'red',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Bottom Sales'
                }
            }, {
                value: 325000000,
                color: 'green',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Goal Sales 2019'
                }
            }]*/
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Profit in 2019',
            data: [118545000, 152801000, 185286000, 226950000, 227276000, 235679000, 257823400, 311491000]
        }]
    });

    function columnchart(container, categories, data)
    {
        Highcharts.chart(container, {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah Penjualan (Milion Rp)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    pointWidth: 24,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                //alert('Category: ' + this.category + ', value: ' + this.y);
                                $('#frmbox-title').html(this.category);
                                $('#frmbox').modal({keyboard: false, backdrop: 'static'});
                            }
                        }
                    }
                }
            },
            series: data
        });
    }

    function barchart(container, categories, data)
    {
        Highcharts.chart(container, {
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Jumlah Penjualan (Milion Rp)'
                },
                plotLines: [{
                    color: 'green',
                    width: 2,
                    value: 80,
                    zIndex: 5,
                    dashStyle: 'shortdash',
                    label: {
                        text: 'Goal Sales 2019'
                    }
                }]
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                bar: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                },
                series: {
                    pointWidth: 15
                }
            },
            series: data
        });
    }

    function maps()
    {
        var data = [
            ['id-3700', 0],
            ['id-ac', 1],
            ['id-ki', 2],
            ['id-jt', 3],
            ['id-be', 4],
            ['id-bt', 5],
            ['id-kb', 6],
            ['id-bb', 7],
            ['id-ba', 8],
            ['id-ji', 9],
            ['id-ks', 10],
            ['id-nt', 11],
            ['id-se', 12],
            ['id-kr', 13],
            ['id-ib', 14],
            ['id-su', 15],
            ['id-ri', 16],
            ['id-sw', 17],
            ['id-la', 18],
            ['id-sb', 19],
            ['id-ma', 20],
            ['id-nb', 21],
            ['id-sg', 22],
            ['id-st', 23],
            ['id-pa', 24],
            ['id-jr', 25],
            ['id-1024', 26],
            ['id-jk', 27],
            ['id-go', 28],
            ['id-yo', 29],
            ['id-kt', 30],
            ['id-sl', 31],
            ['id-sr', 32],
            ['id-ja', 33]
        ];

// Create the chart
        Highcharts.mapChart('maps', {
            chart: {
                map: 'countries/id/id-all'
            },

            title: {
                text: 'Operation Analytic'
            },

            subtitle: {
                text: 'Matriks Kinerja Outlet per Daerah Tahun 2019'
            },

            mapNavigation: {
                enabled: true,
                buttonOptions: {
                    verticalAlign: 'bottom'
                }
            },

            colorAxis: {
                min: 0
            },

            series: [{
                data: data,
                name: 'Random data',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.x}'
                }
            }]
        });
    }
</script>