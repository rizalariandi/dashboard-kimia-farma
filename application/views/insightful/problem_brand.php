<div class="panel panel-default" style="overflow-x: scroll;">
    <div class="panel-heading">Brand Produk <?= $title ?></div>

    <table class="table table-striped table-hover table-pointer" id="table1">
        <thead>
        <tr>
            <th>Brand</th>
            <th>Target</th>
            <th>Realisasi</th>
            <th>Realisasi Tahun lalu</th>
            <th align="center">Achievement</th>
            <th align="center">Growth</th>
            <th align="center">Stock</th>
        </tr>
        </thead>
        <tbody>

        <input type="hidden" name="nama_cabang" id="nama_cabang" value="<?=$ids  ?>">
        <?php if(!empty($brand)) { foreach($brand as $b) { ?>
            <tr onclick="product('<?= $var_col ?>', '<?= $b->nama_brand ?>')">
                <td style="white-space: nowrap"><?= $b->nama_brand ?></td>
                <td><?= y_num_pad($b->target) ?></td>
                <td><?= y_num_pad($b->realisasi) ?></td>
                <td><?= y_num_pad($b->realisasi_past) ?></td>
                <td align="center"><?= $b->target > 0 ? ($b->realisasi/$b->target)*100 : 100 ?></td>
                <td align="center"><?= $b->realisasi > 0 ? round((($b->realisasi - $b->realisasi_past) / $b->realisasi)*100, 2) : 0 ?></td>
                <td align="center"><?= y_num_pad($b->stock) ?></td>
            </tr>
        <?php } } else { ?>
            <tr>
                <td colspan="7">Data tidak tersedia</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>