<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>


<?php
$d1 = json_decode('[{"id":10002567,"name":"AP RAUDAH FARMA(BRB)","rp":18545000},{"id":10002581,"name":"AP. AMANDIT","rp":22801000},{"id":10002695,"name":"APOTEK K-24 (BJM)","rp":11324000},{"id":10002854,"name":"R S K BEDAH BANJARMASIN SIAGA","rp":42234000},{"id":10002876,"name":"RSU. PAMBALAH BATUNG (RUTIN)","rp":30360000},{"id":10004363,"name":"AP.NURUL FIKRI","rp":27600000},{"id":10004514,"name":"PT. BUNDA MEDIK(AP. BUNDA MARGONDA)","rp":85857000},{"id":10007057,"name":"RS. KAMBANG","rp":85560000},{"id":10008919,"name":"GROSIR NUSA INDAH","rp":48760000},{"id":10014039,"name":"APOTEK BANDUNG","rp":97808000}]');
$d2 = json_decode('[{"id":10015478,"name":"APOTEK ASIH HUSADA","rp":248400000},{"id":10017808,"name":"RSUI. MUTIARA BUNDA (BREBES)","rp":25286000},{"id":10020499,"name":"ABDUL WACHID/AP. SIDO WARAS (JPR)","rp":26950000},{"id":10020910,"name":"APOTEK 39  (KDS)","rp":900000},{"id":10025982,"name":"AP. PRIMA - TSK","rp":160380000},{"id":10028323,"name":"KIMIA FARMA (PERSERO) Tbk, PT  - MM","rp":19690000},{"id":10028340,"name":"APOTIK KIMIA FARMA (RSCM)","rp":6716000},{"id":10051893,"name":"APOTEK JESAYA FARMA","rp":21450000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23980000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23276000}]');
$d3 = json_decode('[{"id":20000129,"name":"PT. KIMIA FARMA APOTEK","rp":10966000},{"id":20000880,"name":"KFTD AMBON","rp":14006000},{"id":20000882,"name":"KFTD BANDA ACEH","rp":658000},{"id":20000883,"name":"KFTD BANDAR LAMPUNG","rp":9460000},{"id":20000884,"name":"KFTD BANDUNG","rp":13320000},{"id":20000885,"name":"KFTD BANJARMASIN","rp":96725000},{"id":20000886,"name":"PT.KIMIA FARMA TRADING & DISTRIBUTI","rp":1528000},{"id":20000887,"name":"KFTD BEKASI","rp":1826000},{"id":20000888,"name":"KFTD BENGKULU","rp":2015000},{"id":20000889,"name":"KFTD BOGOR","rp":139000}]');
$d4 = json_decode('[{"id":20000890,"name":"KFTD CIREBON","rp":48473000},{"id":20000891,"name":"KFTD DENPASAR","rp":35000000},{"id":20000893,"name":"KFTD JAKARTA I","rp":16699000},{"id":20000894,"name":"KFTD JAKARTA II","rp":7155000},{"id":20000895,"name":"KFTD JAMBI","rp":2450000},{"id":20000896,"name":"KFTD JAYAPURA","rp":2300000},{"id":20000897,"name":"KFTD JEMBER","rp":18172000},{"id":20000898,"name":"KFTD KENDARI","rp":176283000},{"id":20000899,"name":"KFTD KUPANG","rp":128451000},{"id":20000900,"name":"KFTD MADIUN","rp":13061000}]');
$d5 = json_decode('[{"id":20000902,"name":"KFTD MALANG","rp":43989000},{"id":20000903,"name":"KFTD TERNATE","rp":95930000},{"id":20000904,"name":"KFTD MANADO","rp":26550000},{"id":20000906,"name":"KFTD MEDAN","rp":45540000},{"id":20000907,"name":"KFTD PADANG","rp":39146000},{"id":20000909,"name":"KFTD PALEMBANG","rp":24050000},{"id":20000910,"name":"KFTD PALU","rp":11855000},{"id":20000911,"name":"KFTD PANGKAL PINANG","rp":10070000},{"id":20000912,"name":"KFTD PEKAN  BARU","rp":225000},{"id":20000913,"name":"KFTD PEMATANGSIANTAR","rp":14136000}]');

function sum($data) {
    $a = 0;
    foreach($data as $row) {
        $a += $row->rp;
    }
    return 'Rp. '.number_format($a,0);
}
?>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }
    .nav-tabs>li>a {
        line-height: 1.2;
    }
    .nav-tabs.wizard {
        background-color: transparent;
        padding: 0;
        width: 100%;
        margin: 1em auto;
        border-radius: .25em;
        clear: both;
        border-bottom: none;
    }

    .nav-tabs.wizard li {
        width: 100%;
        float: none;
        margin-bottom: 3px;
    }

    .nav-tabs.wizard li>* {
        position: relative;
        color: #999999;
        background-color: #dedede;
        border-color: #dedede;
    }
    .nav-tabs.wizard li>* {
        padding: .5em .5em .5em 3em;
    }
    .nav-tabs.wizard li:first-child>* {
        padding: .5em .5em .5em .5em;
    }

    .nav-tabs.wizard li.completed>* {
        color: #fff !important;
        background-color: #96c03d !important;
        border-color: #96c03d !important;
        border-bottom: none !important;
    }

    .nav-tabs.wizard li.active>* {
        color: #fff !important;
        background-color: #2c3f4c !important;
        border-color: #2c3f4c !important;
        border-bottom: none !important;
    }

    .nav-tabs.wizard li::after:last-child {
        border: none;
    }

    .nav-tabs.wizard > li > a {
        opacity: 1;
        font-size: 12px;
    }

    .nav-tabs.wizard a:hover {
        color: #fff;
        background-color: #2c3f4c;
        border-color: #2c3f4c
    }

    span.nmbr {
        display: inline-block;
        padding: 5px 0 0 0;
        background: #ffffff;
        width: 20px;
        height: 20px;
        line-height: 100%;
        margin: auto;
        border-radius: 50%;
        font-weight: bold;
        font-size: 14px;
        color: #555;
        margin-bottom: 10px;
        text-align: center;
    }

    @media(min-width:992px) {
        .nav-tabs.wizard li {
            position: relative;
            padding: 0;
            margin: 4px 4px 4px 0;
            width: 19.2%;
            float: left;
            text-align: center;
        }
        .nav-tabs.wizard li::after,
        .nav-tabs.wizard li>*::after {
            content: '';
            position: absolute;
            top: 0px;
            left: 100%;
            height: 0;
            width: 0;
            border: 35px solid transparent;
            border-right-width: 0;
            /*border-left-width: 20px*/
        }
        .nav-tabs.wizard li::after {
            z-index: 1;
            -webkit-transform: translateX(4px);
            -moz-transform: translateX(4px);
            -ms-transform: translateX(4px);
            -o-transform: translateX(4px);
            transform: translateX(4px);
            border-left-color: #fff;
            margin: 0
        }
        .nav-tabs.wizard li>*::after {
            z-index: 2;
            border-left-color: inherit
        }
        .nav-tabs.wizard > li:nth-of-type(1) > a {
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }
        .nav-tabs.wizard li:last-child a {
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
        .nav-tabs.wizard li:last-child {
            margin-right: 0;
        }
        .nav-tabs.wizard li:last-child a:after,
        .nav-tabs.wizard li:last-child:after {
            content: "";
            border: none;
        }
        span.nmbr {
            display: block;
        }
    }
    .timeline>li>.timeline-badge {
        left: 3%;
    }
    .timeline:before {
        left: 3%;
    }
    .timeline>li>.timeline-panel {
        width: 90%;
    }
    th {
        color: #333333;
        font-weight: 500;
    }
</style>

<div class="white-box">
    <div id="pain"></div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">5 Provinsi dengan GAP Penjualan Tertinggi <strong><span class="bln">Agustus</span> 2019</strong></h3>

            <div class="row">
                <div class="col-md-4">KFTD Jayapura</div>
                <div class="col-md-6">
                    <a href="javascript:box('box-area', 'KFTD Jayapura')"><div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 80%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">7%</div>
                    </div></a>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 10 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">KFTD Mataram</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 85%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">5%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 8 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">KFTD Ternate</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 88%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">4.7%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 7 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">KFTD Sorong</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 91%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">3%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 5 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">KFTD Gorontalo</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 92%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">2.91%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 4 Juta</div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">5 LINI dengan GAP Penjualan Tertinggi <strong><span class="bln">Agustus</span> 2019</strong></h3>

            <div class="row">
                <div class="col-md-4">OGP</div>
                <div class="col-md-6">
                    <a href="javascript:boxlini('box-lini', 'OGP')"><div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 80%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">10%</div>
                        </div></a>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 17 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">OTC</div>
                <div class="col-md-6">
                    <a href="javascript:boxlini('box-lini', 'OTC')"><div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 83%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">8.15%</div>
                        </div></a>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 15 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">Ethical</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 87%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">7.25%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 12 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">Kosmetik</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 90%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">5%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 10 Juta</div>
            </div>
            <div class="row">
                <div class="col-md-4">Obat Bahan Alam</div>
                <div class="col-md-6">
                    <div id="progres1" class="progress progress-lg">
                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 92%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">4.5%</div>
                    </div>
                </div>
                <div id="prog-value1" class="col-md-2">Rp 8 Juta</div>
            </div>
        </div>
    </div>
</div>

<!-- Rekomendasi Program Marketing -->
<div class="modal fade bs-example-modal-lg" id="box-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="box-area-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Apotek Dengan Gap Penjualan Terendah</h3>
                        <div id="apotek"></div>
                    </div>
                    <div class="col-md-6">
                        <h3>Lini Terendah di <span id="lini-apotek"></span></h3>
                        <div id="lini" style="display: none">
                            <div class="row">
                                <div class="col-md-4">OGP</div>
                                <div class="col-md-6">
                                    <a href="javascript:produk('OGP')">
                                        <div id="progres1" class="progress progress-lg">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 80%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">10%</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">OTC</div>
                                <div class="col-md-6">
                                    <a href="javascript:produk('OTC')"><div id="progres1" class="progress progress-lg">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 83%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">8.15%</div>
                                    </div></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">Ethical</div>
                                <div class="col-md-6">
                                    <a href="javascript:produk('Ethical')"><div id="progres1" class="progress progress-lg">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 87%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">7.25%</div>
                                    </div></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">Kosmetik</div>
                                <div class="col-md-6">
                                    <a href="javascript:produk('Kosmetik')"><div id="progres1" class="progress progress-lg">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 90%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">5%</div>
                                    </div></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">Obat Bahan Alam</div>
                                <div class="col-md-6">
                                    <a href="javascript:produk('Obat Bahan Alam')"><div id="progres1" class="progress progress-lg">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 92%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">4.5%</div>
                                    </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Produk Lini <span id="lini-produk"></span></h3>
                        <table class="table table-striped" id="tb-produk" style="display: none">
                            <thead>
                            <tr>
                                <th width="30%">Produk</th>
                                <th width="30%">Stock Produk</th>
                                <th width="30%">Marketing Expanse Produk</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>DIARESQ</td>
                                <td><?= rand(450, 800) ?></td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            <tr>
                                <td>NITROKAF</td>
                                <td>1000</td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            <tr>
                                <td>FITUNO</td>
                                <td>1000</td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Total Lini</th>
                                <th>3000</th>
                                <th>Rp. 6.000.000</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Rekomendasi Tindakan Selanjutnya -->

<div class="modal fade bs-example-modal-lg" id="box-lini" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="box-lini-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Apotek Terendah di Lini <span id="apotek-lini"></span></h3>
                        <div id="apotek2"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Produk Terendah untuk Lini <span id="lini-produk2"></span> di <span id="apotek-produk2"></span></h3>
                        <table class="table table-striped" id="tb-produk2" style="display: none">
                            <thead>
                            <tr>
                                <th width="30%">Produk</th>
                                <th width="30%">Stock Produk</th>
                                <th width="30%">Marketing Expanse Produk</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>DIARESQ</td>
                                <td><?= rand(450, 800) ?></td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            <tr>
                                <td>NITROKAF</td>
                                <td>1000</td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            <tr>
                                <td>FITUNO</td>
                                <td>1000</td>
                                <td>Rp. 2.000.000</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Total Lini</th>
                                <th>3000</th>
                                <th>Rp. 6.000.000</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal-dialog -->
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script src="http://blacklabel.github.io/grouped_categories/grouped-categories.js"></script>

<script>
    $(document).ready(function () {
        pain();
    });

    function box(id, title)
    {
        //apotek();

        $('#lini').hide();
        $('#tb-produk').hide();

        $('#'+id+'-title').html(title);
        $('#'+id).modal({keyboard: false, backdrop: 'static'});
    }

    function boxlini(id, title)
    {
        apotek2(title);

        $('#apotek-lini').html(title);
        $('#tb-produk2').hide();

        $('#'+id+'-title').html(title);
        $('#'+id).modal({keyboard: false, backdrop: 'static'});
    }

    function apotek()
    {
        var html = '';
        var i;
        for(i=1; i<=5; i++) {
            console.log(i);
            var num = parseInt(15)-(parseInt(i)*parseInt(1.5));
            var persen = parseInt(100)-(parseInt(num)*parseInt(2));
            var name = rnd(299, 500);
            html += '<div class="row"><div class="col-md-4">Apotek KF '+name+'</div><div class="col-md-6"><a href="javascript:lini(\'Apotek KF '+name+'\')"><div id="progres1" class="progress progress-lg"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: '+persen+'%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">'+num+'%</div></div></a></div></div>';
        }
        $('#apotek').html(html);
    }

    function apotek2(lini)
    {
        var html = '';
        var i;
        for(i=1; i<=5; i++) {
            console.log(i);
            var num = parseInt(15)-(parseInt(i)*parseInt(1.5));
            var persen = parseInt(100)-(parseInt(num)*parseInt(2));
            var name = rnd(299, 500);
            html += '<div class="row"><div class="col-md-4">Apotek KF '+name+'</div><div class="col-md-6"><a href="javascript:produk2(\''+lini+'\', \'Apotek KF '+name+'\')"><div id="progres1" class="progress progress-lg"><div class="progress-bar progress-bar-warning" role="progressbar" style="width: '+persen+'%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100">'+num+'%</div></div></a></div></div>';
        }
        $('#apotek2').html(html);
    }

    function lini(title)
    {
        $('#lini-apotek').html(title);
        $('#lini').show();
    }

    function produk(title)
    {
        $('#lini-produk').html(title);
        $('#tb-produk').show();
    }

    function produk2(lini, apotek)
    {
        $('#lini-produk2').html(lini);
        $('#apotek-produk2').html(apotek);
        $('#tb-produk2').show();
    }

    function rnd(min, max) {
        return Math.floor(Math.random() * (+max - +min)) + +min;
    }

    function pain()
    {
        Highcharts.chart('pain', {
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                plotBands: [{
                    color: '#E9EFF5',
                    from: -1,
                    to: 6,
                    label: {
                        text: 'Data Real',
                        style: {
                            color: '#606060'
                        }
                    }
                }, {
                    color: '#B4C4D3',
                    from: 6,
                    to: 12,
                    label: {
                        text: 'Forcast Rekomendasi',
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            yAxis: {
                title: {
                    text: 'Sales Profit'
                }
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Target Sales 2019',
                data: [150000000, 175000000, 200000000, 225000000, 250000000, 275000000, 300000000, 325000000],
                color: '#3ACCE1'
            },{
                name: 'Realisasi Sales 2019',
                data: [{y: 118545000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, {y: 152801000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, {y: 185286000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, 229950000, {y: 232276000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, {y: 235679000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, {y: 257823400, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}, {y: 311491000, marker: {
                        symbol: 'url(<?= base_url() ?>/asset/images/failed.png)'
                    }}],
                color: '#0B4168'
            }]
        });
    }
</script>