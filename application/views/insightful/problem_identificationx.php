<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }

    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }

    .area-item {
        font-weight: bold;
        margin-right: 10px;
    }

    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody > table > tbody > tr > td {
        white-space: nowrap;
    }
    .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody > table > thead > tr > th {
        white-space: nowrap;
    }
    .dt-buttons {
        padding: 10px;
        text-align: center;
    }
    .buttons-html5 {
        background: #2c5ca9;
        border: 1px solid #2c5ca9;
        border-radius: 3px;
        color: #fff;
        display: inline-block;
        padding: 5px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
    }
    .table > tbody > tr.tr-failed > td {
        background-color: #ffb795 !important;
        color: #232323 !important;
    }
</style>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<div class="white-box">
    <div class="row">
        <div class="col-md-3">
            <div id="sla-data-range" class="mrp-container nav navbar-nav">
                <div class="mrp-monthdisplay">
                    <span class="mrp-lowerMonth"><?= $start3 ?></span>
                    <span class="mrp-to"> to </span>
                    <span class="mrp-upperMonth"><?= $end3 ?></span>
                </div>
                <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
            </div>
        </div>
        <div class="col-md-2">
            <?= form_dropdown('var_lini', $lini_filter, $var_lini, 'class="form-control selectpicker" id="var_lini"  multiple data-live-search="true" title="Lini" data-width="fit"'); ?>
        </div>
        <div class="col-md-2">
            <?= form_dropdown('var_col', $col_filter, '', 'class="form-control" id="var_col"  '); ?>
        </div>
        <!--<div class="col-md-2">
            <?= form_dropdown('var_col', $layanan_filter, '', 'class="form-control selectpicker" id="var_layanan" multiple data-live-search="true" title="Layanan" data-width="fit"'); ?>
        </div>
        -->
        <div class="col-md-2">
            <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
        </div>
    </div>
</div>

<div class="white-box">
    <h3 class="box-title text-center" style="margin-bottom: 0px">
        Achievment Terhadap Target

        <!--<div class="col-md-3 col-sm-4 col-xs-6 pull-right">
            <select class="pull-right row b-none">
                <option></option>
                <option>Achievment Terhadap Stock</option>
            </select>
        </div>-->
    </h3>
    <p class="text-center" style="margin-bottom: 10px">Klik salah satu daerah untuk melihat data detail dan rekomendasi. Gunakan <strong>ctrl</strong> untuk melihat lebih dari satu daerah.</p>
    <div id="maps" style="width: 100%; height: 400px;"></div>
</div>

<div class="white-box">
    Area Terpilih Saat Ini: <span class="font-weight-bold" id="area-selected"></span>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div id="col"></div>
        </div>
    </div>
</div>

<div id="area-container">

</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Sales Trend Sesuai Lini</div>

            <table class="table table-striped table-hover table-pointer" id="table_lini">
                <thead>
                <tr>
                    <th>Lini</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6" id="kftd">
        <div class="panel panel-default">
            <div class="panel-heading">Sales Trend Sesuai KFTD</div>

            <table class="table table-striped table-hover table-pointer" id="table_kftd">
                <thead>
                <tr>
                    <th>Nama KFTD</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default" id="brand1">
            <div class="panel-heading">
                Sales Trend Sesuai Brand: <span id="title_brand_lini">-</span>
                <input type="hidden" id="var_brand_lini">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_brand1">
                <thead>
                <tr>
                    <th>Nama Brand</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div> 
        
        <div class="panel panel-default" id="brand2" style="display:none">
            <div class="panel-heading">
                Sales Trend Sesuai Brand: <span id="title_brand_kftd">-</span>
                <input type="hidden" id="var_brand_kftd">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_brand2">
                <thead>
                <tr>
                    <th>Nama Brand</th>
                    <th>Target</th>
                    <th>Realisasi</th>
                    <th>Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default" id="prod1">
            <div class="panel-heading">
                Sales Trend Sesuai Produk: <span id="title_prod_lini">-</span>
                <input type="hidden" id="var_prod_lini">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_prod1">
                <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div class="panel panel-default" id="prod2" style="display:none">
            <div class="panel-heading">
                Sales Trend Sesuai Produk: <span id="title_prod_kftd">-</span>
                <input type="hidden" id="var_prod_kftd">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_prod2">
                <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <!--<div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sales Trend Sesuai Brand: <span id="title_brand_kftd">-</span>
                <input type="hidden" id="var_brand_kftd">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_brand2">
                <thead>
                <tr>
                    <th>Nama Brand</th>
                    <th>Target</th>
                    <th>Realisasi</th>
                    <th>Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    -->
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sales Trend Sesuai Grup Layanan: <span id="title_prod_lini">-</span>
                <input type="hidden" id="var_prod_lini">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_group_layanan">
                <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Sales Trend Sesuai Layanan: <span id="title_prod_kftd">-</span>
                <input type="hidden" id="var_prod_kftd">
            </div>

            <table class="table table-striped table-hover table-pointer" id="table_layanan">
                <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Target</th>
                    <th class="head_col">Realisasi</th>
                    <th class="head_col_1">Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default" id="panel_summary_prod_lini" style="display: none">
            <div class="panel-body">
                <strong>SUMMARY PRODUK</strong><br>&nbsp;
                <p id="summary_prod_lini"></p>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="panel panel-default" id="panel_summary_prod_kftd" style="display: none">
            <div class="panel-body">
                <strong>SUMMARY PRODUK</strong><br>&nbsp;
                <p id="summary_prod_kftd"></p>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>s"></script>

<script src="<?= base_url() ?>assets/highmaps/regression.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<script>
var baseurl = '<?= base_url() ?>index.php/insightful';
$(document).ready(function () {
    //maps();

    $('#periode').daterangepicker({
        applyClass: 'bg-primary-600',
        cancelClass: 'btn-light',
        locale: {
            format: 'MM/YYYY'
        },
        showDropdowns: true
    }, function(start, end, label) {
        if(start.format('YYYY') != end.format('YYYY')) {
            alert('Periode harus ditahun yang sama');
            end.setDate(start);
        }
    });

    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    area('');
    rangemonth('<?= $start ?>', '<?= $end ?>');

    table_kftd()
    table_lini();
    table_brand1();
    table_prod1();
    table_brand2();
    table_prod2();
});

function filter() {
    area_reload();
}

function table_lini()
{
    table_lini = $('#table_lini').dataTable({
        'ajax': {
            'url':baseurl+'/json_lini',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        }
    });
    table_lini.on('click','tr',function() {
        $('#brand1').show(); 
        $('#brand2').hide(); 

        var data = table_lini.fnGetData( this );
        $('#title_brand_lini').html(data[0]);
        $('#var_brand_lini').val(data[0]);
        $('#table_brand1').dataTable().fnDraw(); 

        $('#title_prod_lini').html('');
        $('#var_prod_lini').val('');
        $('#table_prod1').DataTable().ajax.reload();
    });
}

function table_kftd()
{
    table_kftd = $('#table_kftd').dataTable({
        'ajax': {
            'url':baseurl+'/json_kftd',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 5, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        }
    });
    table_kftd.on('click','tr',function() {
        
        $('#brand2').show(); 
        $('#brand1').hide(); 

        var data = table_kftd.fnGetData( this );
        $('#title_brand_kftd').html(data[0]);
        $('#var_brand_kftd').val(data[0]);
        $('#table_brand2').dataTable().fnDraw();

        $('#title_prod_kftd').html('');
        $('#var_prod_kftd').val('');
        $('#table_prod2').dataTable().fnDraw();
    });
}

function table_brand1()
{
    table_brand1 = $('#table_brand1').dataTable({
        'ajax': {
            'url':baseurl+'/json_brand',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;

                data.col = 'lini';
                data.var = $('#var_brand_lini').val();
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        }
    });
    table_brand1.on('click','tr',function() { 
        $('#prod1').show(); 
        $('#prod2').hide(); 

        var data = table_brand1.fnGetData( this );
        $('#title_prod_lini').html(data[0]);
        $('#var_prod_lini').val(data[0]);
        $('#table_prod1').dataTable().fnDraw();
    });
}

function table_prod1()
{
    var prod1_failed = 0;
    table_prod1 = $('#table_prod1').dataTable({
        'ajax': {
            'url':baseurl+'/json_prod',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;

                data.col = 'lini';
                data.var = $('#var_prod_lini').val();
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        },
        /*"fnRowCallback": function( row, data, iDisplayIndex, iDisplayIndexFull ) {
            var target = data[1].replace(/,/g, '');
            var realisasi = data[2].replace(/,/g, '');
            var prod1_failed = 0;

            if(parseInt(target) > parseInt(realisasi)) {
                $(row).addClass('tr-failed');
                prod1_failed = parseInt(prod1_failed) + parseInt(1);
            }

            $('#panel_summary_prod_lini').hide();
            if(prod1_failed >= 1) {
                if($('.area-item').length >= 1) {
                    var area = [];
                    $('.area-item').each(function() {
                        area.push($(this).html());
                    });
                    var area_text = ' Tersebar di area '+area.join(',');
                } else {
                    var area_text = ' Tersebar di seluruh area indonesia';
                }

                var message = 'Terdapat '+prod1_failed+' Produk yang tidak mencapai target pada Brand '+$('#title_brand_lini').html()+area_text;

                $('#summary_prod_lini').html(message);
                $('#panel_summary_prod_lini').show();
            }
        },*/
        createdRow: function(row, data, dataIndex) {
            var target = data[1].replace(/,/g, '');
            var realisasi = data[2].replace(/,/g, '');

            if(parseInt(target) > parseInt(realisasi)) {
                $(row).addClass('tr-failed');
            }
        },
        drawCallback: function(settings, json){
            var data = this.api().data();
            var prod1_failed = 0;

            data.each(function(i, a) {
                var target = i[1].replace(/,/g, '');
                var realisasi = i[2].replace(/,/g, '');

                if(parseInt(target) > parseInt(realisasi)) {
                    prod1_failed = parseInt(prod1_failed) + parseInt(1);
                }
            });

            $('#panel_summary_prod_lini').hide();
            if(prod1_failed >= 1) {
                if($('.area-item').length >= 1) {
                    var area = [];
                    $('.area-item').each(function() {
                        area.push($(this).html());
                    });
                    var area_text = ' Tersebar di area '+area.join(',');
                } else {
                    var area_text = ' Tersebar di seluruh area indonesia';
                }

                var message = 'Terdapat '+prod1_failed+' Produk yang tidak mencapai target pada Brand '+$('#title_prod_lini').html()+area_text;

                $('#summary_prod_lini').html(message);
                $('#panel_summary_prod_lini').show();
            }
        }
    });
    /*table_prod1.on('draw', function() {
        $('#panel_summary_prod_lini').hide();
        console.log('aaaaa'+prod1_failed);
        if(prod1_failed >= 1) {
            var message = 'Terdapat '+prod1_failed+' Produk yang tidak mencapai target pada Brand '+$('#title_brand_lini').html();
            $('#summary_prod_lini').html(message);
            $('#panel_summary_prod_lini').show();
        }
    })*/
}

function table_brand2()
{
    table_brand2 = $('#table_brand2').dataTable({
        'ajax': {
            'url':baseurl+'/json_brand',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;

                data.col = 'kftd';
                data.var = $('#var_brand_kftd').val();
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        }
    });
    table_brand2.on('click','tr',function() {
        $('#prod2').show(); 
        $('#prod1').hide(); 

        var data = table_brand2.fnGetData( this );
        $('#title_prod_kftd').html(data[0]);
        $('#var_prod_kftd').val(data[0]);
        $('#table_prod2').dataTable().fnDraw();
    });
}

function table_prod2()
{
    table_prod2 = $('#table_prod2').dataTable({
        'ajax': {
            'url':baseurl+'/json_prod',
            'data' : function(data) {
                var area = [];
                $('.area-item').each(function() {
                    area.push($(this).attr('id'));
                });
                var area_text = area.join(',');

                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.layanan= $('#var_layanan').val();
                data.area	= area_text;

                data.col = 'kftd';
                data.var = $('#var_prod_kftd').val();
            },
            'method' : 'post'
        },
        'order':[
            [4, 'asc']
        ],
        'columnDefs': [
        ],
        "drawCallback": function( settings ) {
        },
        'scrollX': true,
        autoWidth: false,
        dom: '<"datatable-header"B><"datatable-scroll"t>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 10, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ],
        initComplete: function() {

        },
        createdRow: function(row, data, dataIndex) {
            var target = data[1].replace(/,/g, '');
            var realisasi = data[2].replace(/,/g, '');

            if(parseInt(target) > parseInt(realisasi)) {
                $(row).addClass('tr-failed');
            }
        },
        drawCallback: function(settings, json){
            var data = this.api().data();
            var prod2_failed = 0;

            data.each(function(i, a) {
                var target = i[1].replace(/,/g, '');
                var realisasi = i[2].replace(/,/g, '');

                if(parseInt(target) > parseInt(realisasi)) {
                    prod2_failed = parseInt(prod2_failed) + parseInt(1);
                }
            });

            $('#panel_summary_prod_kftd').hide();
            if(prod2_failed >= 1) {
                if($('.area-item').length >= 1) {
                    var area = [];
                    $('.area-item').each(function() {
                        area.push($(this).html());
                    });
                    var area_text = ' Tersebar di area '+area.join(',');
                } else {
                    var area_text = ' Tersebar di seluruh area indonesia';
                }

                var message = 'Terdapat '+prod2_failed+' Produk yang tidak mencapai target pada Brand '+$('#title_prod_kftd').html()+area_text;

                $('#summary_prod_kftd').html(message);
                $('#panel_summary_prod_kftd').show();
            }
        }
    });
}

function area() {
    var area = [];
    $('.area-item').each(function() {
        area.push($(this).attr('id'));
    });
    var area_text = area.join(',');

    var cols = $('#var_col').val();

    if(cols=='qty'){
        $.ajax({
            url:'<?= base_url() ?>index.php/insightful_qty/problem_area',
            type:'post',
            data: ({area : area_text, start:$('#mrp-lowerDate').val(), end:$('#mrp-upperDate').val(), lini:$('#var_lini').val(), layanan:$('#var_layanan').val() }),
            success: function(e) {
                $('#area-container').html(e);
                col(ch_data_realisasi, ch_data_realisasi_1, ch_data_month);
            }
        });
    }
    else {

        $.ajax({
            url:'<?= base_url() ?>index.php/insightful/problem_area',
            type:'post',
            data: ({area : area_text, start:$('#mrp-lowerDate').val(), end:$('#mrp-upperDate').val(), lini:$('#var_lini').val(), layanan:$('#var_layanan').val() }),
            success: function(e) {
                $('#area-container').html(e);
                col(ch_data_realisasi, ch_data_realisasi_1, ch_data_month);
            }
        });
    }
}

function rec(title) {
    $('.r-brand').html($('.brand').html());
    $('#r-area').html($('#area-title').html());
    $('#r-prod').html(title);
    $('#rec-container').show();
}

function col(data_realisasi, data_realisasi_1, month) {
    Highcharts.chart('col', {
        chart: {
            type: 'column',
            renderTo: 'linear'
        },
        title: {
            text: 'Trend Sales and Stock Availability'
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: month,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>Rp. {point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var m = this.y / 1000000;
                        return Highcharts.numberFormat(m,2)+' Jt';
                    }
                }
            }
        },
        series: [{
            name: 'Sales '+ch_data_year,
            color: '#69d0ef',
            data: data_realisasi

        }, {
            name: 'Sales '+ch_data_year_1,
            color: '#ab82c4',
            data: data_realisasi_1

        },
        {
            type: 'line',
            name: 'Linear Sales '+ch_data_year,
            color: '#0d47a1',
            dashStyle: 'dash',
            marker: { enabled: false },
            data: (function() {
                return fitData(data_realisasi).data;
            })()
        },
        {
            type: 'line',
            name: 'Linear Sales '+ch_data_year_1,
            color: '#4a148c',
            dashStyle: 'dash',
            marker: { enabled: false },
            data: (function() {
                return fitData(data_realisasi_1).data;
            })()
        }]
    });
}

function rnd(min, max) {
    return Math.floor(Math.random() * (+max - +min)) + +min;
}

var data = <?= $data_maps ?>;
var maps = Highcharts.mapChart('maps', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text: null
    },

    subtitle: {
        text: null,
        style: {
            color: '#2c5ca9'
        }
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    plotOptions: {
        series: {
            point: {
                events: {
                    select: function () {
                        var t = '<span class="area-item" id="'+this.properties['hc-key']+'">'+this.name+'</span>';
                        $('#area-selected').append(t);

                        area_reload();
                        /*var text = 'Selected ' + this.name + ' (' + this.value + '/km²)',
                            chart = this.series.chart;
                        if (!chart.selectedLabel) {
                            chart.selectedLabel = chart.renderer.label(text, 0, 320)
                                .add();
                        } else {
                            chart.selectedLabel.attr({
                                text: text
                            });
                        }*/
                        //console.log(this.name);
                        //console.log(map_select());

                        /*var chart = $('#maps').highcharts();

                        var selectedPointsStr = "";

                        // when is the chart object updated? after this function finshes?
                        var selectedPoints = chart.getSelectedPoints();
                        $.each(selectedPoints, function(i, value) {
                            selectedPointsStr += "<br>"+value.name;
                        });

                        console.log(selectedPointsStr);*/
                    },
                    unselect: function () {
                        $('#'+this.properties['hc-key']).remove();

                        area_reload();
                        /*var text = 'Unselected ' + this.name + ' (' + this.value + '/km²)',
                            chart = this.series.chart;
                        if (!chart.unselectedLabel) {
                            chart.unselectedLabel = chart.renderer.label(text, 0, 300)
                                .add();
                        } else {
                            chart.unselectedLabel.attr({
                                text: text
                            });
                        }*/
                    }
                }
            }
        }
    },

    series: [{
        data: data,
        name: 'Provinsi',
        allowPointSelect: true,
        cursor: 'pointer',
        states: {
            hover: {
                color: '#BADA55'
            },
            select: {
                color: '#EFFFEF',
                borderColor: 'black',
                dashStyle: 'dot'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.value}%'
        },
        /*point:{
            events:{
                click: function() {
                    //console.log(this);
                    //area(this.properties['hc-key']);
                }
            }
        }*/
    }]
});

function area_reload()
{
    var col = $('#var_col').val();
    // alert($('#var_col').val());
    var temp = "";
    if(col=='qty'){
        temp = '_qty';
        $('.head_col').html('Quality');
        $('.head_col_1').html('Quality Tahun lalu');
    }
    else {
        $('.head_col').html('Realisasi');
        $('.head_col_1').html('Realisasi Tahun lalu');
    }

    table_lini.api().ajax.url(baseurl+temp+'/json_lini').load();
    table_kftd.api().ajax.url(baseurl+temp+'/json_kftd').load();
    table_brand1.api().ajax.url(baseurl+temp+'/json_brand').load();
    table_brand2.api().ajax.url(baseurl+temp+'/json_brand').load();
    table_prod1.api().ajax.url(baseurl+temp+'/json_prod').load();
    table_prod2.api().ajax.url(baseurl+temp+'/json_prod').load();



    $('#var_brand_kftd').val('');
    $('#var_brand_lini').val('');
    $('#title_brand_lini').html('-');
    $('#title_brand_kftd').html('-');

    // $('#table_brand1').dataTable().fnDraw();
    // $('#table_brand2').dataTable().fnDraw();

    $('#title_prod_lini').html('-');
    $('#title_prod_kftd').html('-');

    $('#var_prod_lini').val('');
    // $('#table_prod1').dataTable().fnDraw();
    $('#var_prod_kftd').val('');
    // $('#table_prod2').dataTable().fnDraw();

    area();
}

var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}
</script>