<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }

    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }

    .area-item {
        font-weight: bold;
        margin-right: 10px;
    }

    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }
</style>
<div class="white-box"> 
    <div class="row">
        <div class="col-md-3">
            <div id="sla-data-range" class="mrp-container nav navbar-nav">
                <div class="mrp-monthdisplay">
                    <span class="mrp-lowerMonth"><?= $start3 ?></span>
                    <span class="mrp-to"> to </span>
                    <span class="mrp-upperMonth"><?= $end3 ?></span>
                </div>
                <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
            </div>
        </div>
        <div class="col-md-3">
            <?= form_dropdown('var_lini', $lini_filter, $var_lini, 'class="form-control" id="var_lini"'); ?>
        </div>
        <div class="col-md-3">
            <?= form_dropdown('var_layanan', $layanan_filter, $var_layanan, 'class="form-control" id="var_layanan"'); ?>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
        </div> 
    </div> 
</div>

<div class="white-box">
    <h3 class="box-title text-center" style="margin-bottom: 0px">
        Achievment Terhadap Target

        <!--<div class="col-md-3 col-sm-4 col-xs-6 pull-right">
            <select class="pull-right row b-none">
                <option></option>
                <option>Achievment Terhadap Stock</option>
            </select>
        </div>-->
    </h3>
    <p class="text-center" style="margin-bottom: 10px">Klik salah satu daerah untuk melihat data detail dan rekomendasi. Gunakan <strong>ctrl</strong> untuk melihat lebih dari satu daerah.</p>
    <div id="maps" style="width: 100%; height: 400px;"></div>
</div>

<div class="white-box">
    Area Terpilih Saat Ini: <span class="font-weight-bold" id="area-selected">Nasional</span>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div id="col"></div>
        </div>
    </div>
</div>

<div id="area-container">

</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default" style="overflow-x: scroll;">
            <div class="panel-heading">Sales Trend Sesuai Lini</div>

            <table class="table table-striped table-hover table-pointer" id="table1">
                <thead>
                <tr>
                    <th>Lini</th>
                    <th>Target</th>
                    <th>Realisasi</th>
                    <th>Realisasi Tahun lalu</th>
                    <th align="center">Achievement</th>
                    <th align="center">Growth</th>
                    <th align="center">Stock</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($lini as $li) { ?>
                    <tr onclick="brand('lini','<?= $li->lini ?>')">
                        <td><?= $li->lini ?></td>
                        <td><?= y_num_pad($li->target) ?></td>
                        <td><?= y_num_pad($li->realisasi) ?></td>
                        <td><?= y_num_pad($li->realisasi_past) ?></td>
                        <td align="center"><?= $li->target > 0 ? ($li->realisasi/$li->target)*100 : 100 ?></td>
                        <td align="center"><?= $li->realisasi > 0 ? round((($li->realisasi - $li->realisasi_past) / $li->realisasi)*100, 2) : 0 ?></td>
                        <td align="center"><?= y_num_pad($li->stock) ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div id="lini-brand"></div>
        <div id="lini-product"></div>
    </div>
    <div class="col-md-6" id="kftd">

    </div>
</div>

<div class="row">
    <div class="col-md-12" id="rec-container"></div>
</div>

<div class="white-box" style="display: none" id="rec-container">
    <h3 class="box-title">Rekomendasi Insight</h3>

    <p>Area <span id="r-area"></span> di Lini OGB untuk Brand <span class="r-brand"></span> dibutuhkan aktifitas Efisiensi Stock untuk produk <span class="r-brand"></span> <span id="r-prod"></span></p>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
$(document).ready(function () {
    //maps();

    $('#periode').daterangepicker({
        applyClass: 'bg-primary-600',
        cancelClass: 'btn-light',
        locale: {
            format: 'MM/YYYY'
        },
        showDropdowns: true
    }, function(start, end, label) {
        if(start.format('YYYY') != end.format('YYYY')) {
            alert('Periode harus ditahun yang sama');
            end.setDate(start);
        }
    });

    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    area('');
    rangemonth('<?= $start ?>', '<?= $end ?>');
});

function filter() {
    window.location.href='<?= base_url() ?>index.php/insightful/problem/'+$("#mrp-lowerDate").val()+'/'+$("#mrp-upperDate").val()+'/'+$("#var_lini").val()+'/'+$("#var_layanan").val();
}

function area(id) {
    $.ajax({
        url:'<?= base_url() ?>index.php/insightful/problem_area',
        type:'post',
        data: ({id : id, start:'<?= $var_start ?>', end:'<?= $var_end ?>', lini:'<?= $var_lini ?>', layanan:'<?= $var_layanan ?>' }),
        success: function(e) {
            $('#area-container').html(e);
            col(ch_data_target, ch_data_realisasi, ch_data_stock, ch_data_month);
        }
    });
}

function kftd() {
    var area = [];
    $('.area-item').each(function() {
        area.push($(this).attr('id'));
    });
    var area_text = area.join(',');

    $.ajax({
        url:'<?= base_url() ?>index.php/insightful/problem_kftd',
        type:'post',
        data: ({id : area_text, start:'<?= $var_start ?>', end:'<?= $var_end ?>', lini:'<?= $var_lini ?>', layanan:'<?= $var_layanan ?>' }),
        success: function(e) {
            $('#kftd').html(e);
        }
    });
}

function brand(col, id) {
    $.ajax({
        url:'<?= base_url() ?>index.php/insightful/problem_brand',
        type:'post',
        data: ({col : col, id : id, start:'<?= $var_start ?>', end:'<?= $var_end ?>', lini:'<?= $var_lini ?>', layanan:'<?= $var_layanan ?>' }),
        success: function(e) {
            $('#'+col+'-brand').html(e);
        }
    });
}

function product(col, id) { 
    $.ajax({
        url:'<?= base_url() ?>index.php/insightful/problem_product',
        type:'post',
        data: ({col : col, id : id, start:'<?= $var_start ?>', end:'<?= $var_end ?>' }),
        success: function(e) {
            $('#'+col+'-product').html(e);
        }
    });
}
// function product_detail(col,nama_produk) {
//     $.ajax({
//         url:'<?= base_url() ?>index.php/insightful/problem_product_detail',
//         type:'post',
//         data: ({col:col,nama_produk : nama_produk,nama_produkx : $("#nama_produkx").val(),nama_cabang : $("#nama_cabang").val(),month:$('#month').val(),year:$('#year').val() }),
//         success: function(e) {
//             $('#'+col+'-product_detail').html(e);
//         }
//     });
// }

    function prod(title) {
        $('.brand').html(title);
        $('#prod-container').show();
    }

    function rec(title) {
        $('.r-brand').html($('.brand').html());
        $('#r-area').html($('#area-title').html());
        $('#r-prod').html(title);
        $('#rec-container').show();
    }



    function col(data_target, data_realisasi, data_stock, month) {
        Highcharts.chart('col', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Trend Sales and Stock Availability'
            },
            subtitle: {
                text: null
            },
            xAxis: {
                categories: month,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>Rp. {point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            var m = this.y / 1000000000;
                            return Highcharts.numberFormat(m,2)+' M';
                        }
                    }
                }
            },
            series: [{
                name: 'Target Sales',
                color: '#69d0ef',
                data: data_target

            }, {
                name: 'Actual Sales',
                color: '#ab82c4',
                data: data_realisasi

            }, {
                name: 'Stock Value',
                color: '#d66f9c',
                data: data_stock
            }]
        });
    }

    function rnd(min, max) {
        return Math.floor(Math.random() * (+max - +min)) + +min;
    }

var data = <?= $data_maps ?>;
var maps = Highcharts.mapChart('maps', {
    chart: {
        map: 'countries/id/id-all'
    },

    title: {
        text: null
    },

    subtitle: {
        text: null,
        style: {
            color: '#2c5ca9'
        }
    },

    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },

    colorAxis: {
        min: 0
    },

    plotOptions: {
        series: {
            point: {
                events: {
                    select: function () {
                        var t = '<span class="area-item" id="'+this.properties['hc-key']+'">'+this.name+'</span>';
                        $('#area-selected').append(t);

                        kftd();
                        /*var text = 'Selected ' + this.name + ' (' + this.value + '/km²)',
                            chart = this.series.chart;
                        if (!chart.selectedLabel) {
                            chart.selectedLabel = chart.renderer.label(text, 0, 320)
                                .add();
                        } else {
                            chart.selectedLabel.attr({
                                text: text
                            });
                        }*/
                        //console.log(this.name);
                        //console.log(map_select());

                        /*var chart = $('#maps').highcharts();

                        var selectedPointsStr = "";

                        // when is the chart object updated? after this function finshes?
                        var selectedPoints = chart.getSelectedPoints();
                        $.each(selectedPoints, function(i, value) {
                            selectedPointsStr += "<br>"+value.name;
                        });

                        console.log(selectedPointsStr);*/
                    },
                    unselect: function () {
                        $('#'+this.properties['hc-key']).remove();

                        kftd();
                        /*var text = 'Unselected ' + this.name + ' (' + this.value + '/km²)',
                            chart = this.series.chart;
                        if (!chart.unselectedLabel) {
                            chart.unselectedLabel = chart.renderer.label(text, 0, 300)
                                .add();
                        } else {
                            chart.unselectedLabel.attr({
                                text: text
                            });
                        }*/
                    }
                }
            }
        }
    },

    series: [{
        data: data,
        name: 'Provinsi',
        allowPointSelect: true,
        cursor: 'pointer',
        states: {
            hover: {
                color: '#BADA55'
            },
            select: {
                color: '#EFFFEF',
                borderColor: 'black',
                dashStyle: 'dot'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.value}%'
        },
        /*point:{
            events:{
                click: function() {
                    //console.log(this);
                    //area(this.properties['hc-key']);
                }
            }
        }*/
    }]
});

function map_select() {
    return maps.getSelectedPoints();
}

var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}
</script>