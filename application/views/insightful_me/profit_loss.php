<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<style>
    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }
    .table-pointer > thead > tr > th {
        font-weight: normal;
        vertical-align: middle;
        background-color: #f79868;
        color: #fff;
        padding: 8px;
    }
    .table-pointer > thead > tr > th.th-lini {
        width: 200px;
        background-color: #4e9ed5;
        color: #fff;
        font-weight: bold;
    }
    .table-pointer > thead > tr > th.th-sm {
        font-size: 13px; font-weight: bold;
    }
    .table-pointer > tbody > tr > td {
        white-space:nowrap;
        font-size: 13px;

        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer > tbody > tr > td.td-lini {
        width: 200px;
        background-color: #eaf4ff;
        color: #323232;
    }

    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }

    .dt-buttons {
        padding: 10px;
        text-align: center;
        float: left;
    }
    .dataTables_length {
        float: left;
        padding: 14px;
    }
    .dataTables_filter {
        padding: 14px;
    }
    .buttons-html5 {
        background: #2c5ca9;
        border: 1px solid #2c5ca9;
        border-radius: 3px;
        color: #fff;
        display: inline-block;
        padding: 5px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
    }
</style>


<div class="white-box">
    <div class="row">
        <div class="col-md-3">
            <div id="sla-data-range" class="mrp-container nav navbar-nav">
                <div class="mrp-monthdisplay">
                    <span class="mrp-lowerMonth"><?= $start3 ?></span>
                    <span class="mrp-to"> to </span>
                    <span class="mrp-upperMonth"><?= $end3 ?></span>
                </div>
                <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
            </div>
        </div>
        <!--<div class="col-md-3">
            < ?= form_dropdown('var_lini', $lini_filter, $var_lini, 'class="form-control" id="var_lini"'); ?>
        </div>-->
        <!--<div class="col-md-5">
            <select name="month" id="month" class="form-control">
                <option value="1" <?=($month==1?'selected':'')?>>Januari</option>
                <option value="2" <?=($month==2?'selected':'')?>>Februari</option>
                <option value="3" <?=($month==3?'selected':'')?>>Maret</option>
                <option value="4" <?=($month==4?'selected':'')?>>April</option>
                <option value="5" <?=($month==5?'selected':'')?>>Mei</option>
                <option value="6" <?=($month==6?'selected':'')?>>Juni</option>
                <option value="7" <?=($month==7?'selected':'')?>>Juli</option>
                <option value="8" <?=($month==8?'selected':'')?>>Agustus</option>
                <option value="9" <?=($month==9?'selected':'')?>>September</option>
                <option value="10" <?=($month==10?'selected':'')?>>Oktober</option>
                <option value="11" <?=($month==11?'selected':'')?>>November</option>
                <option value="12" <?=($month==12?'selected':'')?>>Desember</option>
            </select>
        </div>
        <div class="col-md-5">
            <select name="year" id="year" class="form-control">
                <?php foreach($yearOption as $p) { ?>
                    <option value="<?=$p?>" <?=($year==$p?'selected':'')?>><?=$p?></option>
                <?php } ?>
            </select>
        </div>-->
        <div class="col-md-2">
            <button type="button" class="btn btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Profit and Loss</div>
            <table class="table table-striped table-hover table-bordered table-pointer" id="table" style="text-wrap: none">
                <thead>
                <tr>
                    <th class="th-sm">&nbsp;</th>
                    <th class="th-sm">Target</th>
                    <th class="th-sm">Realisasi</th>
                    <th class="th-sm">Last Year</th>
                    <th class="th-sm">Achievement (%)</th>
                    <th class="th-sm">Growth (%)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Sales</td>
                    <td><?= y_num_pad($db->target_sales) ?></td>
                    <td><?= y_num_pad($db->sales) ?></td>
                    <td><?= y_num_pad($db->sales_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_sales) ? $db->sales / $db->target_sales : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->sales_ly) ? $db->sales / $db->sales_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>COGS</td>
                    <td><?= y_num_pad($db->target_cogs) ?></td>
                    <td><?= y_num_pad($db->cogs) ?></td>
                    <td><?= y_num_pad($db->cogs_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_cogs) ? $db->cogs / $db->target_cogs : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->cogs_ly) ? $db->cogs / $db->cogs_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>COGS (%)</td>
                    <td><?= y_num_pad($db->target_cogs_persen) ?></td>
                    <td><?= y_num_pad($db->cogs_persen) ?></td>
                    <td><?= y_num_pad($db->cogs_persen_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_cogs_persen) ? $db->cogs_persen / $db->target_cogs_persen : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->cogs_persen_ly) ? $db->cogs_persen / $db->cogs_persen_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Gross Margin</td>
                    <td><?= y_num_pad($db->target_gross_margin) ?></td>
                    <td><?= y_num_pad($db->gross_margin) ?></td>
                    <td><?= y_num_pad($db->gross_margin_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_gross_margin) ? $db->gross_margin / $db->target_gross_margin : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->gross_margin_ly) ? $db->gross_margin / $db->gross_margin_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Gross Margin (%)</td>
                    <td><?= y_num_pad($db->target_gross_margin_persen) ?></td>
                    <td><?= y_num_pad($db->gross_margin_persen) ?></td>
                    <td><?= y_num_pad($db->gross_margin_persen_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_gross_margin_persen) ? $db->gross_margin_persen / $db->target_gross_margin_persen : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->gross_margin_persen_ly) ? $db->gross_margin_persen / $db->gross_margin_persen_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Marketing Expense</td>
                    <td><?= y_num_pad($db->target_expense) ?></td>
                    <td><?= y_num_pad($db->expense) ?></td>
                    <td><?= y_num_pad($db->expense_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_expense) ? $db->expense / $db->target_expense : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->expense_ly) ? $db->expense / $db->expense_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Marketing Expense (%)</td>
                    <td><?= y_num_pad($db->target_expense_persen) ?></td>
                    <td><?= y_num_pad($db->expense_persen) ?></td>
                    <td><?= y_num_pad($db->expense_persen_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_expense_persen) ? $db->expense_persen / $db->target_expense_persen : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->expense_persen_ly) ? $db->expense_persen / $db->expense_persen_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Margin Marketing</td>
                    <td><?= y_num_pad($db->target_margin_mkt) ?></td>
                    <td><?= y_num_pad($db->margin_mkt) ?></td>
                    <td><?= y_num_pad($db->margin_mkt_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_margin_mkt) ? $db->margin_mkt / $db->target_margin_mkt : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->margin_mkt_ly) ? $db->margin_mkt / $db->margin_mkt_ly : 0) ?></td>
                </tr>
                <tr>
                    <td>Margin Marketing (%)</td>
                    <td><?= y_num_pad($db->target_margin_mkt_persen) ?></td>
                    <td><?= y_num_pad($db->margin_mkt_persen) ?></td>
                    <td><?= y_num_pad($db->margin_mkt_persen_ly) ?></td>
                    <td><?= y_num_pad(!empty($db->target_margin_mkt_persen) ? $db->margin_mkt_persen / $db->target_margin_mkt_persen : 0) ?></td>
                    <td><?= y_num_pad(!empty($db->margin_mkt_persen_ly) ? $db->margin_mkt_persen / $db->margin_mkt_persen_ly : 0) ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script>
var baseurl = '<?= base_url() ?>index.php/insightful_me';
$(document).ready(function () {
    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    rangemonth('<?= $start ?>', '<?= $end ?>');

    $('#table').dataTable({
        dom: '<"datatable-header"B><"datatable-scroll"t><"datatable-footer">',
        ordering: false,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ]
    });
});

function filter() {
    window.location.href='<?= base_url() ?>index.php/insightful_me/profit_loss/'+$("#mrp-lowerDate").val()+'/'+$("#mrp-upperDate").val();//+'/'+$("#var_lini").val();
}

Number.prototype.formatcurrency = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}

function number_format(number, decimals, dec_point, sep) {

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        s_part = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    console.log(s, n, prec);
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    s_part = s[0].substring(0, s[0].length - 4);
    if (s_part.length > 3) {
        s_part = s_part.replace(/\B(?=(?:\d{2})+(?!\d))/g, sep);
    }
    s[0] = s_part + ',' + s[0].substring(s[0].length - 4, s[0].length - 1);
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
</script>