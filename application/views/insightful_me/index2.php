<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>


<style>
    th {
        color: #333333;
        font-weight: 500;
    }
    .table-rec > tbody > tr > th, .table-rec > tbody > tr > td {
        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer td {
        cursor: pointer;
    }
    .table-pointer > thead > tr > th {
        font-weight: normal;
        vertical-align: middle;
        background-color: #f79868;
        color: #fff;
        padding: 8px;
    }
    .table-pointer > thead > tr > th.th-lini {
        width: 200px;
        background-color: #4e9ed5;
        color: #fff;
        font-weight: bold;
    }
    .table-pointer > thead > tr > th.th-sm {
        font-size: 13px; font-weight: bold;
    }
    .table-pointer > tbody > tr > td {
        white-space:nowrap;
        font-size: 13px;

        padding-top: 7px;
        padding-right: 8px;
        padding-bottom: 7px;
        padding-left: 8px;
    }
    .table-pointer > tbody > tr > td.td-lini {
        width: 200px;
        background-color: #eaf4ff;
        color: #323232;
    }

    .mrp-icon{
        border: solid 1px #ddd;
        border-radius: 5px 0px 0px 5px;
        color: #40667A;
        background: #eee;
        padding: 7px;
        margin-right:0px;
    }

    .mrp-monthdisplay{
        display:inline-block!important;
        border: solid 1px #ddd;
        padding: 7px 12px;
        border-radius: 5px;
        background-color: #fff;
        cursor:pointer;
        margin-left: -5px;
        height: 38px;
        width: 200px;
    }

    .mrp-lowerMonth, .mrp-upperMonth{
        color: #40667A;
        font-weight:bold;
        font-size: 11px;
        text-transform:uppercase;
    }

    .mrp-to{
        color: #aaa;
        margin-right: 0px;
        margin-left: 0px;
        font-size: 11px;
        text-transform: uppercase;
        /* background-color: #eee; */
        padding: 5px 3px 5px 3px;
    }

    .mpr-calendar{
        display:inline-block;
        padding: 3px 5px;
        border-right: solid #999 1px;
    }

    .mpr-calendar::last-child{
        border-right: none;
    }

    .mpr-month{
        padding: 20px;
        text-transform: uppercase;
        font-size: 12px;
    }

    .mpr-calendar h5{
        width:100%;
        text-align:center;
        font-weight:bold;
        font-size:18px
    }

    .mpr-selected{
        background: rgba(64, 102, 122, 0.75);;
        color: #fff;
    }

    .mpr-month:hover{
        border-radius: 5px;
        box-shadow: 0 0 0 1px #ddd inset;
        cursor:pointer;
    }

    .mpr-selected.mpr-month:hover{
        border-radius: 0px;
        box-shadow: none;
    }

    .mpr-calendarholder .col-xs-6 {
        max-width: 250px;
        min-width: 250px;
    }

    .mpr-calendarholder .col-xs-1 {
        max-width: 150px;
        min-width: 150px;
    }

    .mpr-calendarholder .btn-info{
        background-color: #40667A;
        border-color: #406670;
        width:100%;
        margin-bottom: 10px;
        text-transform: uppercase;
        font-size: 10px;
        padding: 10px 0px;
    }

    .mpr-quickset{
        color: #666;
        text-transform: uppercase;
        text-align: center;
    }

    .mpr-yeardown, .mpr-yearup{
        margin-left: 5px;
        cursor: pointer;
        color: #666;
    }

    .mpr-yeardown{
        float:left;
    }

    .mpr-yearup{
        float:right;
    }

    .mpr-yeardown:hover,.mpr-yearup:hover{
        color: #40667A;
    }

    .mpr-calendar:first .mpr-selected:first{
        background-color: #40667A;
    }

    .mpr-calendar:last .mpr-selected:last{
        background-color: #40667A;
    }

    .popover{
        max-width: 1920px!important;
    }

    .dt-buttons {
        padding: 10px 50px;
        text-align: center;
        float: left;
    }
    .dataTables_length {
        float: left;
        padding: 14px;
    }
    .dataTables_filter {
        padding: 14px;
    }
    .buttons-html5 {
        background: #2c5ca9;
        border: 1px solid #2c5ca9;
        border-radius: 3px;
        color: #fff;
        display: inline-block;
        padding: 5px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        touch-action: manipulation;
        cursor: pointer;
        user-select: none;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<div class="white-box">
    <div class="row">
        <div class="col-md-3">
            <div id="sla-data-range" class="mrp-container nav navbar-nav">
                <div class="mrp-monthdisplay">
                    <span class="mrp-lowerMonth"><?= $start3 ?></span>
                    <span class="mrp-to"> to </span>
                    <span class="mrp-upperMonth"><?= $end3 ?></span>
                </div>
                <input type="hidden" value="<?= $start ?>" id="mrp-lowerDate" />
                <input type="hidden" value="<?= $end ?>" id="mrp-upperDate" />
            </div>
        </div>
            <div class="col-md-3">
                <?= form_dropdown('var_lini', $lini_filter, $var_lini, 'class="form-control selectpicker" multiple data-live-search="true" title="Lini" id="var_lini"'); ?>
            </div>
            <div class="col-md-2">
            <?= form_dropdown('var_biaya', $biaya_filter, $var_biaya, 'class="form-control selectpicker" multiple data-live-search="true" title="Beban Biaya" id="var_biaya"'); ?>
            </div>
            <div class="col-md-3">
            <?= form_dropdown('var_pos', $pos_filter, $var_pos, 'class="form-control selectpicker" multiple data-live-search="true" title="Pos Rekening" id="var_pos"'); ?>    
           </div>
        <!--<div class="col-md-5">
            <select name="month" id="month" class="form-control">
                <option value="1" <?=($month==1?'selected':'')?>>Januari</option>
                <option value="2" <?=($month==2?'selected':'')?>>Februari</option>
                <option value="3" <?=($month==3?'selected':'')?>>Maret</option>
                <option value="4" <?=($month==4?'selected':'')?>>April</option>
                <option value="5" <?=($month==5?'selected':'')?>>Mei</option>
                <option value="6" <?=($month==6?'selected':'')?>>Juni</option>
                <option value="7" <?=($month==7?'selected':'')?>>Juli</option>
                <option value="8" <?=($month==8?'selected':'')?>>Agustus</option>
                <option value="9" <?=($month==9?'selected':'')?>>September</option>
                <option value="10" <?=($month==10?'selected':'')?>>Oktober</option>
                <option value="11" <?=($month==11?'selected':'')?>>November</option>
                <option value="12" <?=($month==12?'selected':'')?>>Desember</option>
            </select>
        </div>
        <div class="col-md-5">
            <select name="year" id="year" class="form-control">
                <?php foreach($yearOption as $p) { ?>
                    <option value="<?=$p?>" <?=($year==$p?'selected':'')?>><?=$p?></option>
                <?php } ?>
            </select>
        </div>-->
        <div class="col-md-1">
            <button type="button" class="btn mt-2 btn-primary" name="filter" id="filter" onclick="filter()">Filter</button>
        </div>
    </div>
</div>

<div class="white-box">
    <div id="chart" style="width: 100%; height: 400px;"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-5" style="padding-top:5px">Marketing Expense Summary By Beban Biaya</div>
                    <div class="col-md-7">
                    <!-- 1 Filter -->
          
                   
                      
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover table-bordered table-pointer" id="table_bb" style="text-wrap: none">
                    <thead>
                    <tr>
                        <th class="th-sm">Beban Biaya</th>
                        <th class="th-sm">Budget</th>
                        <th class="th-sm">Expense</th>
                        <th class="th-sm">Sisa Expense</th>
                        <th class="th-sm">Expense Tahun Lalu</th>
                        <th class="th-sm">Achieve(%)</th>
                        <th class="th-sm">Growth(%)</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-4" style="padding-top: 5px">Marketing Expense Information</div>
                    <div class="col-md-8">
                      
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover table-bordered table-pointer" id="table" style="text-wrap: none">
                    <thead>
                    <tr>
                        <th class="th-sm">Lini</th>    
                        <th class="th-sm">Pos Rekening</th>
                        <th class="th-sm">Beban Biaya</th>
                        <th class="th-sm">Budget</th>
                        <th class="th-sm">Expense</th>
                        <th class="th-sm">Sisa Expense</th>
                        <th class="th-sm">Expense Tahun Lalu</th>
                        <th class="th-sm">Achieve(%)</th>
                        <th class="th-sm">Growth(%)</th>
                
                    
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script>
var baseurl = '<?= base_url() ?>index.php/insightful_me';
$(document).ready(function () {
    $('#periode').daterangepicker({
        applyClass: 'bg-primary-600',
        cancelClass: 'btn-light',
        locale: {
            format: 'MM/YYYY'
        },
        showDropdowns: true
    }, function(start, end, label) {
        if(start.format('YYYY') != end.format('YYYY')) {
            alert('Periode harus ditahun yang sama');
            end.setDate(start);
        }
    });

    Highcharts.setOptions({
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });
    $('#table').dataTable({
        'ajax': {
            'url':baseurl+'/json_me',
            'data' : function(data) {
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.biaya  = $('#var_biaya').val();
                data.pos  = $('#var_pos').val();
                
            },
            'method' : 'post'
        },
        'order':[
            [0, 'dsc']
        ],
        'columnDefs': [
            { "searchable": false, "targets": [6,7,8] }
        ],
        
        "drawCallback": function( settings ) {
        },
        //'scrollX': true,
        //'scrollY': '300px',
        //autoWidth: false,
        dom: '<"datatable-header"flB><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        "scrollX": true,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ]//,
        //initComplete: function() {

        //}
    });
    $('#table_bb').dataTable({
        'ajax': {
            'url':baseurl+'/json_me_bb',
            'data' : function(data) {
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.biaya = $('#var_biaya').val();
                data.pos  = $('#var_pos').val();
                
            },
            'method' : 'post'
        },
        'order':[
            [0, 'asc']
        ],
        'columnDefs': [
            { "searchable": false, "targets": [4,5,6] }
        ],
        
        "drawCallback": function( settings ) {
        },
        //'scrollX': true,
        //'scrollY': '300px',
        //autoWidth: false,
        dom: '<"datatable-header"flB><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        "scrollX": true,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ]//,
        //initComplete: function() {

        //}
    });

    chart();
    //product('aa','aa');
    // table();
    // table_bb();
    rangemonth('<?= $start ?>', '<?= $end ?>');
    
    // $('#var_lini').change(function() {
    //     $('#table').dataTable().fnDraw();
    //     $('#table_bb').dataTable().fnDraw();
    // });
   
    // $('#var_biaya').change(function() {
    //     $('#table').dataTable().fnDraw();
    //     $('#table_bb').dataTable().fnDraw();
    // });
    // $('#var_pos').change(function() {
    //     $('#table').dataTable().fnDraw();
    //     $('#table_bb').dataTable().fnDraw();
    // });

    // $('#var_lini2_bb').change(function() {
    //     $('#table_bb').dataTable().fnDraw();
    // });
    // $('#var_pos_bb').change(function() {
    //     $('#table_bb').dataTable().fnDraw();
    // });
});

function filter() {
    //var lini = $("#var_lini").val().join('--');
    //console.log(lini);
    //window.location.href='<?= base_url() ?>index.php/insightful_me/index/'+$("#mrp-lowerDate").val()+'/'+$("#mrp-upperDate").val()+'/'+$("#var_lini").val()+'/'+$("#var_biaya").val()+'/'+$("#var_pos").val();
    chart();
    $('#table').dataTable().fnDraw();
    $('#table_bb').dataTable().fnDraw();
}

Number.prototype.formatcurrency = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function table()
{
    table_lini = $('#table').dataTable({
        'ajax': {
            'url':baseurl+'/json_me',
            'data' : function(data) {
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.biaya  = $('#var_biaya').val();
                data.pos  = $('#var_pos').val();
                
            },
            'method' : 'post'
        },
        'order':[
            [0, 'asc']
        ],
        'columnDefs': [
            { "searchable": false, "targets": [6,7,8] }
        ],
        
        "drawCallback": function( settings ) {
        },
        //'scrollX': true,
        //'scrollY': '300px',
        //autoWidth: false,
        dom: '<"datatable-header"flB><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        "scrollX": true,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ]//,
        //initComplete: function() {

        //}
    });
}

function table_bb()
{
    table_lini = $('#table_bb').dataTable({
        'ajax': {
            'url':baseurl+'/json_me_bb',
            'data' : function(data) {
                data.startx	= $('#mrp-lowerDate').val();
                data.end	= $('#mrp-upperDate').val();
                data.lini	= $('#var_lini').val();
                data.biaya = $('#var_biaya').val();
                data.pos  = $('#var_pos').val();
                
            },
            'method' : 'post'
        },
        'order':[
            [0, 'asc']
        ],
        'columnDefs': [
            { "searchable": false, "targets": [4,5,6] }
        ],
        
        "drawCallback": function( settings ) {
        },
        //'scrollX': true,
        //'scrollY': '300px',
        //autoWidth: false,
        dom: '<"datatable-header"flB><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        "lengthMenu": [
            [10, 20, 30, 50, 100, 150, -1],
            [10, 20, 30, 50, 100, 150, "All"]
        ],
        "pageLength": 30, // default records per page
        "autoWidth": false, // disable fixed width and enable fluid table
        "processing": true, // enable/disable display message box on record load
        "serverSide": true, // enable/disable server side ajax loading,
        "scrollX": true,
        buttons: [
            {
                extend: 'csv',
                text: 'Download CSV'
            },
            {
                extend: 'excel',
                text: 'Download excel'
            }
        ]//,
        //initComplete: function() {

        //}
    });
}

function chart()
{
    var area = [];
    $('.area-item').each(function() {
        area.push($(this).attr('id'));
    });
    var area_text = area.join(',');

    $.ajax({
        url:'<?= base_url() ?>index.php/insightful_me/chart',
        type:'post',
        dataType: 'json',
        data: ({area : area_text, start:$('#mrp-lowerDate').val(), end:$('#mrp-upperDate').val(), lini:$('#var_lini').val(), biaya:$('#var_biaya').val(), pos:$('#var_pos').val()}),
        success: function(e) {

            product(e.ex_bln, e.ex_data);

            //$('#area-container').html(e);
            //col('col', 'Trend Sales and Stock Availability (Value)', ch_data_realisasi, ch_data_realisasi_1, ch_data_target, ch_data_stock, ch_data_month);
            //col('col_qty', 'Trend Sales and Stock Availability (QTY)', ch_data_realisasi_qty, ch_data_realisasi_qty_1, ch_data_target_qty, ch_data_stock_qty, ch_data_month);
            //col2('col_sales', 'Trend Sales (Value)', ch_data_realisasi, ch_data_realisasi_1, ch_data_target_qty, ch_data_stock_qty, ch_data_month);
            //$("#loader7").hide();
        }
    });
}

function product(databln, data)
{
    var db = $.parseJSON(databln);
    var data = $.parseJSON(data);

    Highcharts.chart('chart', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: [{
            categories: db,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                },
                formatter: function() {
                    return '';
                }
            },
            title: {
                text: 'Value (Rp)',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Rasio',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true,
            /*formatter: function () {
                var txt = '<strong>'+this.x+'</strong><br>';
                $.each(this.points, function(i,p) {
                    if(p.series.name == 'Realisasi Sales')
                        var y = parseInt(p.y) * parseInt(100);
                    else
                        var y = p.y;

                    y = y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    if(p.series.name == 'Rasio Realisasi Expense')
                        y = y+'%';
                    else
                        y = 'Rp. '+y;

                    txt += '<span style="color:'+p.color+'">\u25CF</span> '+p.series.name+': <b>'+y+'</b><br/>';
                });
                return txt;
            }*/
        },
        legend: {
            layout: 'horizontal'
        },
        series: data
    });
}

var MONTHS = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

function rangemonth(start, end)
{
    var s = start.split('-');
    var e = end.split('-');

    startMonth = s[0];
    startYear = s[1];
    endMonth = e[0];
    endYear = e[1];

    fiscalMonth = 7;
    if(startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear  + startMonth + "");
    if(endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    content = '<div class="row mpr-calendarholder">';
    calendarCount = endYear - startYear;
    if(calendarCount == 0)
        calendarCount++;
    var d = new Date();
    for(y = 0; y < 2; y++){
        content += '<div class="col-xs-6" ><div class="mpr-calendar row" id="mpr-calendar-' + (y+1) + '">'
            + '<h5 class="col-xs-12"><i class="mpr-yeardown fa fa-chevron-circle-left"></i><span>' + (startYear + y).toString() + '</span><i class="mpr-yearup fa fa-chevron-circle-right"></i></h5><div class="mpr-monthsContainer"><div class="mpr-MonthsWrapper">';
        for(m=0; m < 12; m++){
            var monthval;
            if((m+1) < 10)
                monthval = "0" + (m+1);
            else
                monthval = "" + (m+1);
            content += '<span data-month="' + monthval  + '" class="col-xs-3 mpr-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '</div>';

    $(document).on('click','.mpr-month',function(e){
        e.stopPropagation();
        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.mpr-calendar').children('h5').children('span').html();
        if($month.parents('#mpr-calendar-1').length > 0){
            //Start Date
            startDate = parseInt("" + year + monthnum);
            if(startDate > endDate){

                if(year != parseInt(endDate/100))
                    $('.mpr-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        }else{
            //End Date
            endDate = parseInt("" + year + monthnum);
            if(startDate > endDate){
                if(year != parseInt(startDate/100))
                    $('.mpr-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        paintMonths();
    });


    $(document).on('click','.mpr-yearup',function(e){
        e.stopPropagation();
        var year = parseInt($(this).prev().html());
        year++;
        $(this).prev().html(""+year);
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-yeardown',function(e){
        e.stopPropagation();
        var year = parseInt($(this).next().html());
        year--;
        $(this).next().html(""+year);
        //paintMonths();
        $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $(this).parents('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        startDate = parseInt(d.getFullYear() + "01");
        var month = d.getMonth() + 1;
        if(month < 9)
            month = "0" + month;
        endDate = parseInt("" + d.getFullYear() + month);
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            var year = $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-year', function(e){
        e.stopPropagation();
        var d = new Date();
        var year = d.getFullYear()-1;
        startDate = parseInt(year + "01");
        endDate = parseInt(year + "12");
        $('.mpr-calendar').each(function(){
            var $cal = $(this);
            $('h5 span',$cal).html(year);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-fiscal-ytd', function(e){
        e.stopPropagation();
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 1;
        else
            year = d.getFullYear();
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(d.getMonth()+1 < 10)
            cm = "0" + (d.getMonth()+1);
        else
            cm = (d.getMonth()+1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + d.getFullYear() + cm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear());
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click','.mpr-prev-fiscal', function(){
        var d = new Date();
        var year;
        if((d.getMonth()+1) < fiscalMonth)
            year = d.getFullYear() - 2;
        else
            year = d.getFullYear() - 1;
        if(fiscalMonth < 10)
            fm = "0" + fiscalMonth;
        else
            fm = fiscalMonth;
        if(fiscalMonth -1 < 10)
            efm = "0" + (fiscalMonth-1);
        else
            efm = (fiscalMonth-1);
        startDate = parseInt("" + year + fm);
        endDate = parseInt("" + (d.getFullYear() - 1) + efm);
        $('.mpr-calendar').each(function(i){
            var $cal = $(this);
            if(i == 0)
                $('h5 span',$cal).html(year);
            else
                $('h5 span',$cal).html(d.getFullYear()-1);
        });
        $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeOut(175,function(){
            paintMonths();
            $('.mpr-calendar').find('.mpr-MonthsWrapper').fadeIn(175);
        });
    });

    var mprVisible = false;
    var mprpopover = $('.mrp-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function () {
        $('.popover').remove();
        var waiter = setInterval(function(){
            if($('.popover').length > 0){
                clearInterval(waiter);
                setViewToCurrentYears();
                paintMonths();
            }
        },50);
    }).on('shown.bs.popover', function(){
        mprVisible = true;
    }).on('hidden.bs.popover', function(){
        mprVisible = false;
    });

    $(document).on('click','.mpr-calendarholder',function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    $(document).on("click",".mrp-container",function(e){
        if(mprVisible){
            e.preventDefault();
            e.stopPropagation();
            mprVisible = false;
        }
    });
    $(document).on("click",function(e){
        if(mprVisible){
            $('.mpr-calendarholder').parents('.popover').fadeOut(200,function(){
                $('.mpr-calendarholder').parents('.popover').remove();
                $('.mrp-container').trigger('click');
            });
            mprVisible = false;
        }
    });
}
function setViewToCurrentYears(){
    var startyear = parseInt(startDate / 100);
    var endyear = parseInt(endDate / 100);
    $('.mpr-calendar h5 span').eq(0).html(startyear);
    $('.mpr-calendar h5 span').eq(1).html(endyear);
}

function paintMonths(){
    $('.mpr-calendar').each(function(){
        var $cal = $(this);
        var year = $('h5 span',$cal).html();
        $('.mpr-month',$cal).each(function(i){
            if((i+1) > 9)
                cDate = parseInt("" + year + (i+1));
            else
                cDate = parseInt("" + year+ '0' + (i+1));
            if(cDate >= startDate && cDate <= endDate){
                $(this).addClass('mpr-selected');
            }else{
                $(this).removeClass('mpr-selected');
            }
        });
    });
    $('.mpr-calendar .mpr-month').css("background","");
    //Write Text
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.mrp-monthdisplay .mrp-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.mrp-monthdisplay .mrp-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);
    $('#mrp-lowerDate').val(startmonth+'-'+startyear);
    $('#mrp-upperDate').val(endmonth+'-'+endyear);
    if(startyear == parseInt($('.mpr-calendar:first h5 span').html()))
        $('.mpr-calendar:first .mpr-selected:first').css("background","#40667A");
    if(endyear == parseInt($('.mpr-calendar:last h5 span').html()))
        $('.mpr-calendar:last .mpr-selected:last').css("background","#40667A");
}

function safeRound(val){
    return Math.round(((val)+ 0.00001) * 100) / 100;
}

function number_format(number, decimals, dec_point, sep) {

    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        s_part = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    console.log(s, n, prec);
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    s_part = s[0].substring(0, s[0].length - 4);
    if (s_part.length > 3) {
        s_part = s_part.replace(/\B(?=(?:\d{2})+(?!\d))/g, sep);
    }
    s[0] = s_part + ',' + s[0].substring(s[0].length - 4, s[0].length - 1);
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
</script>