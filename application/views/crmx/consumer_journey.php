

<?php
$d1 = $this->db->query("SELECT * FROM crm_cj LEFT JOIN crm_customers ON cc_id_customer = ct_id WHERE cc_id IN ( SELECT MAX(cc_id) FROM crm_cj GROUP BY cc_id_customer )")->result();

$data = array();
foreach($d1 as $d) {
    $data[$d->cc_step][] = $d;
}

$steps = array('kualifikasi', 'analisis kebutuhan', 'penawaran harga', 'negosiasi', 'closed');

function sum($data) {
    $a = 0;
    foreach($data as $row) {
        $a += $row->rp;
    }
    return 'Rp. '.number_format($a,0);
}
?>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
</style>

<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<!-- .row
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="area-chart"></div>
            </div>
        </div>
    </div>
</div>-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row" id="filter" style="margin-bottom:20px">
                    <select id="area" class="fiterCrm"><option value="">Semua Area</option><option>Area Jawa Barat</option></select> 
                    <select id="city" class="fiterCrm"><option value="">Semua KFTD</option>
                        <?php foreach($filter['city'] as $row){ ?>
                            <option value="<?=$row->ct_city ?>"><?=$row->ct_city ?></option>
                        <?php } ?>
                    </select> 
                    <select id="type" class="fiterCrm"><option value="">Semua Tipe Akun</option>
                        <?php foreach($filter['type'] as $row){ ?>
                            <option value="<?=$row->ct_typex ?>"><?=$row->ct_typex ?></option>
                        <?php } ?>
                    </select> 
                    <select id="level" class="fiterCrm"><option value="">Semua Level Konsumen</option>
                        <?php foreach($filter['level'] as $row){ ?>
                            <option value="<?=$row ?>"><?=$row ?></option>
                        <?php } ?>
                    </select> 
                    <!--
                    <select id="status" class="fiterCrm"><option value="">Semua Status Konsumen</option>
                        <?php foreach($filter['status'] as $row){ ?>
                            <option value="<?=$row->ct_status ?>"><?=$row->ct_status ?></option>
                        <?php } ?>
                    </select>  
                    -->
                </div> 
                <table class="table table-striped" id="table">
                    <thead>
                    <tr>
                        <th width="20%" class="nosort" style="background-color:#b3e5fc">KUALIFIKASI<br><span id="header1">0</span></th>
                        <th width="20%" style="background-color:#81d4fa">ANALISIS KEBUTUHAN<br><span id="header2">0</span></th>
                        <th width="20%" style="background-color:#4fc3f7">PENAWARAN HARGA<br><span id="header3">0</span></th>
                        <th width="20%" style="background-color:#42a5f5; color: #fff">NEGOSIASI<br><span id="header4">0</span></th>
                        <th width="20%" style="background-color:#2196f3; color: #fff;">CLOSED<br><span id="header5">0</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- <?php for($i=0; $i<4; $i++) { ?>
                        <tr class="tdselect">
                            <?php if(isset($data['kualifikasi'][$i])) { ?>
                            <td class="tdselect" style="background-color:#b3e5fc"><strong><?= $data['kualifikasi'][$i]->ct_name ?></strong><br>Rp. <?= number_format($data['kualifikasi'][$i]->cc_total,0) ?></td>
                            <?php } else { ?>
                            <td class="tdselect" style="background-color:#b3e5fc"><strong>-</strong></td>
                            <?php } ?>

                            <?php if(isset($data['analisis kebutuhan'][$i])) { ?>
                            <td class="tdselect" style="background-color:#81d4fa" onclick="link(<?= $data['analisis kebutuhan'][$i]->ct_id ?>)"><strong><?= $data['analisis kebutuhan'][$i]->ct_name ?></strong><br>Rp. <?= number_format($data['analisis kebutuhan'][$i]->cc_total,0) ?></td>
                            <?php } else { ?>
                                <td class="tdselect" style="background-color:#b3e5fc"><strong>-</strong></td>
                            <?php } ?>

                            <?php if(isset($data['penawaran harga'][$i])) { ?>
                            <td class="tdselect" style="background-color:#4fc3f7" onclick="link(<?= $data['penawaran harga'][$i]->ct_id ?>)"><strong><?= $data['penawaran harga'][$i]->ct_name ?></strong><br>Rp. <?= number_format($data['penawaran harga'][$i]->cc_total,0) ?></td>
                            <?php } else { ?>
                                <td class="tdselect" style="background-color:#4fc3f7"><strong>-</strong></td>
                            <?php } ?>

                            <?php if(isset($data['negosiasi'][$i])) { ?>
                            <td class="tdselect" style="background-color:#42a5f5; color: #fff" onclick="link(<?= $data['negosiasi'][$i]->ct_id ?>)"><strong><?= $data['negosiasi'][$i]->ct_name ?></strong><br>Rp. <?= number_format($data['negosiasi'][$i]->cc_total,0) ?><br><div class="badge badge-success badge">&nbsp;<?= $data['negosiasi'][$i]->cc_probability; ?>&nbsp;</div></td>
                            <?php } else { ?>
                                <td class="tdselect" style="background-color:#b3e5fc"><strong>-</strong></td>
                            <?php } ?>

                            <?php if(isset($data['closed'][$i])) { ?>
                            <td class="tdselect" style="background-color:#2196f3; color: #fff" onclick="link(<?= $data['closed'][$i]->ct_id ?>)"><strong><?= $data['closed'][$i]->ct_name ?></strong><br>Rp. <?= $data['closed'][$i]->cc_total ?><br><div class="badge badge-success badge">Berhasil</div></td>
                            <?php } else { ?>
                                <td class="tdselect" style="background-color:#b3e5fc"><strong>-</strong></td>
                            <?php } ?>

                        </tr>
                    <?php } ?> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<!-- <script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/export-data.js"></script> -->

 
<script>

var tb 		= '#table';      
var baseurl = '<?=base_url()?>index.php/crm';
    $(document).ready(function () {
        // $('#table').dataTable();
        
        total();

        $(tb).dataTable({
            'ajax': {
                'url':baseurl+'/json',
                'data' : function(data) {
                    data.city	    = $('#city').val();
                    data.type		= $('#type').val(); 
                    data.level		= $('#level').val(); 
                    data.status		= $('#status').val(); 
                }, 
                'method' : 'post'
            }, 
            'order':[
                [0, 'desc']
            ],
            'columnDefs': [   
            ],
            "drawCallback": function( settings ) { 
            },
            'scrollX': true, 
            autoWidth: false,
            dom: '<"datatable-header"f<"dt-buttons">l><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            "lengthMenu": [
                [10, 20, 30, 50, 100, 150, -1],
                [10, 20, 30, 50, 100, 150, "All"]
            ], 
            "pageLength": 30, // default records per page 
            "autoWidth": false, // disable fixed width and enable fluid table
            "processing": true, // enable/disable display message box on record load
            "serverSide": true, // enable/disable server side ajax loading,
            rowCallback: function(row, data, index){  
                if(data[0]!="")  $(row).find('td:eq(0)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});   
                else $(row).find('td:eq(0)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});     

                if(data[1]!="")  $(row).find('td:eq(1)').css({'background-color' : '#81d4fa','color' : '#FFFFFF'});   
                else $(row).find('td:eq(1)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});    
                
                if(data[2]!="")  $(row).find('td:eq(2)').css({'background-color' : '#4fc3f7','color' : '#FFFFFF'});   
                else $(row).find('td:eq(2)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});   
                
                if(data[3]!="")  $(row).find('td:eq(3)').css({'background-color' : '#42a5f5','color' : '#FFFFFF'});   
                else $(row).find('td:eq(3)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});   
                
                if(data[4]!="")  $(row).find('td:eq(4)').css({'background-color' : '#2196f3','color' : '#FFFFFF'});   
                else $(row).find('td:eq(4)').css({'background-color' : '#b3e5fc','color' : '#FFFFFF'});  

            }, 
            initComplete: function() { 

                $('#table_filter input').unbind();
                $('#table_filter input').bind('keyup', function(e) {
                    if(e.keyCode == 13) {
                        $(tb).dataTable().fnFilter(this.value);
                        total();
                    }
                });  
                
            }
        });  

	
        $('.dataTables_filter input[type=search]').attr('placeholder','Pencarian');
        $(".fiterCrm").change(function() { 
            $(tb).dataTable().fnDraw();
            total();
        });
 
    });

    function link(id) {
        window.location.href = '<?= base_url() ?>index.php/crm/consumer_journey_detail/'+id
    }

    function total(){
        $.ajax({
            url:baseurl+'/getTotal',
            global:false,
            async:true,
            type:'post',
            dataType:'json',
            data: ({
                city	    : $('#city').val(),
                type		: $('#type').val(),
                level		: $('#level').val(), 
                status		: $('#status').val(), 
                search		: $('#table_filter input').val()
            }),
            success: function(e) {  
                console.log(e);
                if(e.header1==null) $("#header1").html('Rp. 0');
                else $("#header1").html('Rp. '+number_format(e.header1, 2, ',', '.')); 
                if(e.header2==null) $("#header2").html('Rp. 0');
                else $("#header2").html('Rp. '+number_format(e.header2, 2, ',', '.')); 
                if(e.header3==null) $("#header3").html('Rp. 0');
                else $("#header3").html('Rp. '+number_format(e.header3, 2, ',', '.')); 
                if(e.header4==null) $("#header4").html('Rp. 0');
                else $("#header4").html('Rp. '+number_format(e.header4, 2, ',', '.')); 
                if(e.header5==null) $("#header5").html('Rp. 0');
                else $("#header5").html('Rp. '+number_format(e.header5, 2, ',', '.')); 
            },
            error : function() {
                alert('error');	 
            },
            beforeSend : function() { 
            },
            complete : function() { 
            }
        });	
    }

    
function number_format(number, decimals, dec_point, thousands_point) { 
	if (number == null || !isFinite(number)) {
			throw new TypeError("number is not valid");
	} 
	if (!decimals) {
			var len = number.toString().split('.').length;
			decimals = len > 1 ? len : 0;
	} 
	if (!dec_point) {
			dec_point = '.';
	} 
	if (!thousands_point) {
			thousands_point = ',';
	} 
	number = parseFloat(number).toFixed(decimals); 
	number = number.replace(".", dec_point); 
	var splitNum = number.split(dec_point);
	splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
	number = splitNum.join(dec_point); 
	return number;
}


    /*Highcharts.chart('area-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        },
        yAxis: {
            title: {
                text: 'Sales Won and Negotiation > 70%'
            },
            plotLines: [{
                value: 250000000,
                color: 'red',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Bottom Sales'
                }
            }, {
                value: 325000000,
                color: 'green',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Goal Sales 2019'
                }
            }]
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Sales in 2019',
            data: [118545000, 152801000, 185286000, 226950000, 227276000, 235679000, 257823400, 311491000]
        }]
    });*/
</script>