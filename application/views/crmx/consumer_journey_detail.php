<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>
<style>
.error {
    color: #f44336; 
}

th{
    color : #000000 !important;
}
</style>

<?php
$d1 = json_decode('[{"id":10002567,"name":"AP RAUDAH FARMA(BRB)","rp":18545000},{"id":10002581,"name":"AP. AMANDIT","rp":22801000},{"id":10002695,"name":"APOTEK K-24 (BJM)","rp":11324000},{"id":10002854,"name":"R S K BEDAH BANJARMASIN SIAGA","rp":42234000},{"id":10002876,"name":"RSU. PAMBALAH BATUNG (RUTIN)","rp":30360000},{"id":10004363,"name":"AP.NURUL FIKRI","rp":27600000},{"id":10004514,"name":"PT. BUNDA MEDIK(AP. BUNDA MARGONDA)","rp":85857000},{"id":10007057,"name":"RS. KAMBANG","rp":85560000},{"id":10008919,"name":"GROSIR NUSA INDAH","rp":48760000},{"id":10014039,"name":"APOTEK BANDUNG","rp":97808000}]');
$d2 = json_decode('[{"id":10015478,"name":"APOTEK ASIH HUSADA","rp":248400000},{"id":10017808,"name":"RSUI. MUTIARA BUNDA (BREBES)","rp":25286000},{"id":10020499,"name":"ABDUL WACHID/AP. SIDO WARAS (JPR)","rp":26950000},{"id":10020910,"name":"APOTEK 39  (KDS)","rp":900000},{"id":10025982,"name":"AP. PRIMA - TSK","rp":160380000},{"id":10028323,"name":"KIMIA FARMA (PERSERO) Tbk, PT  - MM","rp":19690000},{"id":10028340,"name":"APOTIK KIMIA FARMA (RSCM)","rp":6716000},{"id":10051893,"name":"APOTEK JESAYA FARMA","rp":21450000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23980000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23276000}]');
$d3 = json_decode('[{"id":20000129,"name":"PT. KIMIA FARMA APOTEK","rp":10966000},{"id":20000880,"name":"KFTD AMBON","rp":14006000},{"id":20000882,"name":"KFTD BANDA ACEH","rp":658000},{"id":20000883,"name":"KFTD BANDAR LAMPUNG","rp":9460000},{"id":20000884,"name":"KFTD BANDUNG","rp":13320000},{"id":20000885,"name":"KFTD BANJARMASIN","rp":96725000},{"id":20000886,"name":"PT.KIMIA FARMA TRADING & DISTRIBUTI","rp":1528000},{"id":20000887,"name":"KFTD BEKASI","rp":1826000},{"id":20000888,"name":"KFTD BENGKULU","rp":2015000},{"id":20000889,"name":"KFTD BOGOR","rp":139000}]');
$d4 = json_decode('[{"id":20000890,"name":"KFTD CIREBON","rp":48473000},{"id":20000891,"name":"KFTD DENPASAR","rp":35000000},{"id":20000893,"name":"KFTD JAKARTA I","rp":16699000},{"id":20000894,"name":"KFTD JAKARTA II","rp":7155000},{"id":20000895,"name":"KFTD JAMBI","rp":2450000},{"id":20000896,"name":"KFTD JAYAPURA","rp":2300000},{"id":20000897,"name":"KFTD JEMBER","rp":18172000},{"id":20000898,"name":"KFTD KENDARI","rp":176283000},{"id":20000899,"name":"KFTD KUPANG","rp":128451000},{"id":20000900,"name":"KFTD MADIUN","rp":13061000}]');
$d5 = json_decode('[{"id":20000902,"name":"KFTD MALANG","rp":43989000},{"id":20000903,"name":"KFTD TERNATE","rp":95930000},{"id":20000904,"name":"KFTD MANADO","rp":26550000},{"id":20000906,"name":"KFTD MEDAN","rp":45540000},{"id":20000907,"name":"KFTD PADANG","rp":39146000},{"id":20000909,"name":"KFTD PALEMBANG","rp":24050000},{"id":20000910,"name":"KFTD PALU","rp":11855000},{"id":20000911,"name":"KFTD PANGKAL PINANG","rp":10070000},{"id":20000912,"name":"KFTD PEKAN  BARU","rp":225000},{"id":20000913,"name":"KFTD PEMATANGSIANTAR","rp":14136000}]');

function sum($data) {
    $a = 0;
    foreach($data as $row) {
        $a += $row->rp;
    }
    return 'Rp. '.number_format($a,0);
}
?>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    .table-borderless>tbody>tr>td {
        border: 0px;
    }
    .nav-tabs>li>a {
        line-height: 1.2;
    }
    .nav-tabs.wizard {
        background-color: transparent;
        padding: 0;
        width: 100%;
        margin: 1em auto;
        border-radius: .25em;
        clear: both;
        border-bottom: none;
    }

    .nav-tabs.wizard li {
        width: 100%;
        float: none;
        margin-bottom: 3px;
    }

    .nav-tabs.wizard li>* {
        position: relative;
        color: #999999;
        background-color: #dedede;
        border-color: #dedede;
    }
    .nav-tabs.wizard li>* {
        padding: .5em .5em .5em 3em;
    }
    .nav-tabs.wizard li:first-child>* {
        padding: .5em .5em .5em .5em;
    }

    .nav-tabs.wizard li.completed>* {
        color: #fff !important;
        background-color: #96c03d !important;
        border-color: #96c03d !important;
        border-bottom: none !important;
    }

    .nav-tabs.wizard li.active>* {
        color: #fff !important;
        background-color: #2c3f4c !important;
        border-color: #2c3f4c !important;
        border-bottom: none !important;
    }

    .nav-tabs.wizard li::after:last-child {
        border: none;
    }

    .nav-tabs.wizard > li > a {
        opacity: 1;
        font-size: 12px;
    }

    .nav-tabs.wizard a:hover {
        color: #fff;
        background-color: #2c3f4c;
        border-color: #2c3f4c
    }

    span.nmbr {
        display: inline-block;
        padding: 5px 0 0 0;
        background: #ffffff;
        width: 20px;
        height: 20px;
        line-height: 100%;
        margin: auto;
        border-radius: 50%;
        font-weight: bold;
        font-size: 14px;
        color: #555;
        margin-bottom: 10px;
        text-align: center;
    }

    @media(min-width:992px) {
        .nav-tabs.wizard li {
            position: relative;
            padding: 0;
            margin: 4px 4px 4px 0;
            width: 19.2%;
            float: left;
            text-align: center;
        }
        .nav-tabs.wizard li::after,
        .nav-tabs.wizard li>*::after {
            content: '';
            position: absolute;
            top: 0px;
            left: 100%;
            height: 0;
            width: 0;
            border: 35px solid transparent;
            border-right-width: 0;
            /*border-left-width: 20px*/
        }
        .nav-tabs.wizard li::after {
            z-index: 1;
            -webkit-transform: translateX(4px);
            -moz-transform: translateX(4px);
            -ms-transform: translateX(4px);
            -o-transform: translateX(4px);
            transform: translateX(4px);
            border-left-color: #fff;
            margin: 0
        }
        .nav-tabs.wizard li>*::after {
            z-index: 2;
            border-left-color: inherit
        }
        .nav-tabs.wizard > li:nth-of-type(1) > a {
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }
        .nav-tabs.wizard li:last-child a {
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
        .nav-tabs.wizard li:last-child {
            margin-right: 0;
        }
        .nav-tabs.wizard li:last-child a:after,
        .nav-tabs.wizard li:last-child:after {
            content: "";
            border: none;
        }
        span.nmbr {
            display: block;
        }
    }
    .timeline>li>.timeline-badge {
        left: 3%;
    }
    .timeline:before {
        left: 3%;
    }
    .timeline>li>.timeline-panel {
        width: 90%;
    }
    .comment-type {
        width: 40px;
        height: 40px;
        margin-right: 5px;
        display: inline-block;
        text-align: center;
        vertical-align: middle;
        border-radius: 100%;
    }
</style>

<div class="panel panel-default">
    <div class="panel-heading">
        Profil Akun / Konsumen
        <span class="pull-right font-weight-light text-muted" style="font-weight: normal"><small><em>Last Updated: 18 Sep 2019 18:00</em></small></span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td width="35%">Nama Perusahaan</td>
                        <td width="5%">:</td>
                        <td width="60%"><?=$owner['ct_name'] ?></td>
                    </tr>
                    <tr>
                        <td>Kota</td>
                        <td>:</td>
                        <td><?=$owner['ct_address'].','.$owner['ct_city'] ?></td>
                    </tr>
                    <tr>
                        <td>No. Telp</td>
                        <td>:</td>
                        <td><?=$owner['ct_phone'] ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><?=$owner['ct_email'] ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td>Tipe Akun</td>
                        <td>:</td>
                        <td><?=$owner['ct_type'] ?></td>
                    </tr>
                    <tr>
                        <td>Level</td>
                        <td>:</td>
                        <td><?=$owner['ct_level'] ?></td>
                    </tr>
                    <!--<tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><?=$owner['ct_status'] ?></td>
                    </tr>
                    -->
                    <tr>
                        <td>Total Pembelian / Total Revenue</td>
                        <td>:</td>
                        <td><?=$owner['total_item'] ?> Unit / Rp. <?=number_format($owner['total_sales'],2,',','.');?></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table class="table" width="100%" id="table">
                            <thead>
                                <tr> 
                                    <th>Nama</th>
                                    <th>Satuan</th>
                                    <th>Jumlah</th>
                                    <th>Penjualan</th>
                                </tr>
                                </thead>
                                
                                <tbody>
                                <?php
                                    if($detail){
                                        foreach($detail as $key=>$row){
                                ?>
                                <tr> 
                                    <td><?=$row->ccd_product ?></td>
                                    <td><?=$row->ccd_unit ?></td>
                                    <td><?=number_format($row->ccd_qty,0) ?></td>
                                    <td><?='Rp. '.number_format($row->ccd_total,0) ?></td>
                                </tr>
                                <?php
                                        } 
                                    }
                                ?> 
                                 </tbody>
                            </table>
                        </td> 
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-4">
                Sales Representative:<br><strong><?=$owner['ct_salesman'] ?></strong> &nbsp; <button class="box-title btn btn-primary btn btn-sm" onclick="edit_sales()"><i class="fa fa-pencil"></i> Ubah Data</button>
            </div>
            <div class="col-md-8" style="text-align: right;">
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-program')"><i class="fa fa-shopping-bag mr-1"></i> Rekomendasi Program Marketing</button>
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-prediksi')"><i class="fa fa-star mr-1"></i> Prediksi Tipe Konsumen</button>
                <button class="btn btn-primary btn-outline btn-sm" onclick="box('box-komunikasi')"><i class="fa fa-weixin mr-1"></i> Rekomendasi Metode Komunikasi</button>
            </div>
        </div>
    </div>
</div>

<!-- .row -->
<div class="row">
    <div class="col-md-5">

    </div>


</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="tabbable">
            <ul class="nav nav-tabs wizard">
                <li class="completed <?=(!array_key_exists(1,$data)?'active':'')?>"><a href="#t1" data-toggle="tab" aria-expanded="false"><span class="nmbr">1</span>KUALIFIKASI<br>&nbsp;</a></li>
                <li class="completed <?=((array_key_exists(1,$data) && !array_key_exists(2,$data))?'active':'')?>"><a href="#t2" data-toggle="tab" aria-expanded="false"><span class="nmbr">2</span>ANALISIS<br>KEBUTUHAN</a></li>
                <li class="completed <?=((array_key_exists(1,$data) && array_key_exists(2,$data) && !array_key_exists(3,$data))?'active':'')?>"><a href="#t3" data-toggle="tab" aria-expanded="false"><span class="nmbr">3</span>PENAWARAN<br>HARGA</a></li>
                <li class="completed <?=((array_key_exists(1,$data) && array_key_exists(2,$data) && array_key_exists(3,$data) && !array_key_exists(4,$data))?'active':'')?>"><a href="#t4" data-toggle="tab" aria-expanded="false"><span class="nmbr">4</span>NEGOSIASI<br>&nbsp;</a></li>
                <li class="<?=((array_key_exists(1,$data) && array_key_exists(2,$data) && array_key_exists(3,$data) && array_key_exists(4,$data))?'active':'')?>"><a href="#t5" data-toggle="tab" aria-expanded="true"><span class="nmbr">5</span>CLOSED<br>&nbsp;</a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane <?=(!array_key_exists(1,$data)?'active':'')?>" id="t1">
                    <table class="table table-borderless" width="100%">
                        <tr>
                            <td width="20%">Jumlah Rp</td>
                            <td width="5%">:</td>
                            <td width="75%"><?=number_format($data['1']['data']->ccs_sales,2,',','.');?></td>
                        </tr>
                        <tr>
                            <td>Probabilitas (%)</td>
                            <td>:</td>
                            <td><?=$data['1']['data']->ccs_probability ?></td>
                        </tr>
                        <tr>
                            <td>Close Date</td>
                            <td>:</td>
                            <td><?=$data['1']['data']->ccs_close_date; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div  class="col-md-12">
                        <?php
                        foreach($ask['1'] as $key => $row){ ?>
                            <p><i class="fa fa-check-square <?=($data['1']['answer'][$key]=='1'?'text-success':'text-muted') ?>"></i> &nbsp;<?=$row ?></p> 
                       <?php
                        } 
                        ?> 
                    </div>
                    <hr>
                    <div  class="col-md-12"> 
                    <?php if(array_key_exists(1,$data)){ ?>
                    
                        <button class="box-title btn btn-primary btn btn-sm" onclick="edit_step(1)"><i class="fa fa-add"></i> Ubah Data</button>
                    <?php }else{?>
                        <button class="box-title btn btn-primary btn btn-sm" onclick="add_step(1)"><i class="fa fa-add"></i> Tambah Data</button>
                    <?php } ?>
                    </div>
                </div>
                <div class="tab-pane <?=((array_key_exists(1,$data) && !array_key_exists(2,$data))?'active':'')?>" id="t2">
                    <table class="table table-borderless" width="100%">
                        <tr>
                            <td width="20%">Jumlah Rp</td>
                            <td width="5%">:</td>
                            <td width="75%"><?=number_format($data['2']['data']->ccs_sales,2,',','.');?></td>
                        </tr>
                        <tr>
                            <td>Probabilitas (%)</td>
                            <td>:</td>
                            <td><?=$data['2']['data']->ccs_probability ?></td>
                        </tr>
                        <tr>
                            <td>Close Date</td>
                            <td>:</td>
                            <td><?=$data['2']['data']->ccs_close_date; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div  class="col-md-12"> 
                        <?php
                            foreach($ask['2'] as $key => $row){ ?>
                                <p><i class="fa fa-check-square <?=($data['2']['answer'][$key]=='1'?'text-success':'text-muted') ?>"></i> &nbsp;<?=$row ?></p> 
                        <?php
                        } 
                        ?> 
                    </div>
                    <hr>
                    <div  class="col-md-12"> 
                    <?php if(array_key_exists(2,$data)){ ?>
                    
                        <button class="box-title btn btn-primary btn btn-sm" onclick="edit_step(2)"><i class="fa fa-add"></i> Ubah Data</button>
                    <?php }else{?>
                        <button class="box-title btn btn-primary btn btn-sm" onclick="add_step(2)"><i class="fa fa-add"></i> Tambah Data</button>
                    <?php } ?>
                    </div>
                </div>
                <div class="tab-pane  <?=((array_key_exists(1,$data) && array_key_exists(2,$data) && !array_key_exists(3,$data))?'active':'')?>" id="t3">
                    <table class="table table-borderless" width="100%">
                        <tr>
                            <td width="20%">Jumlah Rp</td>
                            <td width="5%">:</td>
                            <td width="75%"><?=number_format($data['3']['data']->ccs_sales,2,',','.');?></td>
                        </tr>
                        <tr>
                            <td>Probabilitas (%)</td>
                            <td>:</td>
                            <td><?=$data['3']['data']->ccs_probability ?></td>
                        </tr>
                        <tr>
                            <td>Rekomendasi Pemasaran</td>
                            <td>:</td>
                            <td><?=$data['3']['data']->ccs_market_recommend ?></td>
                        </tr>
                        <tr>
                            <td>Close Date</td>
                            <td>:</td>
                            <td><?=$data['3']['data']->ccs_close_date; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div  class="col-md-12"> 
                        <?php
                            foreach($ask['3'] as $key => $row){ ?>
                                <p><i class="fa fa-check-square <?=($data['3']['answer'][$key]=='1'?'text-success':'text-muted') ?>"></i> &nbsp;<?=$row ?></p> 
                        <?php
                        } 
                        ?> 
                    </div>
                    <hr>
                    <div  class="col-md-12"> 
                    <?php if(array_key_exists(3,$data)){ ?>
                    
                        <button class="box-title btn btn-primary btn btn-sm" onclick="edit_step(3)"><i class="fa fa-add"></i> Ubah Data</button>
                    <?php }else{?>
                        <button class="box-title btn btn-primary btn btn-sm" onclick="add_step(3)"><i class="fa fa-add"></i> Tambah Data</button>
                    <?php } ?>
                    </div>
                </div>
                <div class="tab-pane <?=((array_key_exists(1,$data) && array_key_exists(2,$data) && array_key_exists(3,$data) && !array_key_exists(4,$data))?'active':'')?>" id="t4">
                    <table class="table table-borderless" width="100%">
                        <tr>
                            <td width="20%">Jumlah Rp</td>
                            <td width="5%">:</td>
                            <td width="75%"><?=number_format($data['4']['data']->ccs_sales,2,',','.');?></td>
                        </tr>
                        <tr>
                            <td>Probabilitas (%)</td>
                            <td>:</td>
                            <td><?=$data['4']['data']->ccs_probability ?></td>
                        </tr>
                        <tr>
                            <td>Close Date</td>
                            <td>:</td>
                            <td><?=$data['4']['data']->ccs_close_date; ?></td>
                        </tr>
                    </table>
                    <hr>
                    <div  class="col-md-12"> 
                        <?php
                            foreach($ask['4'] as $key => $row){ ?>
                                <p><i class="fa fa-check-square <?=($data['4']['answer'][$key]=='1'?'text-success':'text-muted') ?>"></i> &nbsp;<?=$row ?></p> 
                        <?php
                        } 
                        ?> 
                    </div>
                    <hr>
                    <div  class="col-md-12"> 
                    <?php if(array_key_exists(4,$data)){ ?>
                    
                        <button class="box-title btn btn-primary btn btn-sm" onclick="edit_step(4)"><i class="fa fa-add"></i> Ubah Data</button>
                    <?php }else{?>
                        <button class="box-title btn btn-primary btn btn-sm" onclick="add_step(4)"><i class="fa fa-add"></i> Tambah Data</button>
                    <?php } ?>
                    </div>
                </div>
                <div class="tab-pane <?=((array_key_exists(1,$data) && array_key_exists(2,$data) && array_key_exists(3,$data) && array_key_exists(4,$data))?'active':'')?>" id="t5">
                    <table class="table table-borderless" width="100%">
                        <tr>
                            <td width="20%">Jumlah Rp</td>
                            <td width="5%">:</td>
                            <td width="75%"><?=number_format($data['5']['data']->ccs_sales,2,',','.');?></td>
                        </tr>
                        <tr>
                            <td>Probabilitas (%)</td>
                            <td>:</td>
                            <td><?=$data['5']['data']->ccs_probability ?> <strong>(Win)</strong></td>
                        </tr>
                        <tr>
                            <td>Close Date</td>
                            <td>:</td>
                            <td><?=$data['5']['data']->ccs_close_date; ?></td>
                        </tr> 
                    </table>
                    <hr>
                    <div  class="col-md-12"> 
                    <?php if(array_key_exists(5,$data)){ ?>
                    
                        <button class="box-title btn btn-primary btn btn-sm" onclick="edit_step(5)"><i class="fa fa-add"></i> Ubah Data</button>
                    <?php }else{?>
                        <button class="box-title btn btn-primary btn btn-sm" onclick="add_step(5)"><i class="fa fa-add"></i> Tambah Data</button>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="white-box">
    <h3 class="box-title" style="float:left;margin-right:20px;">Log Interaksi Marketing Sales</h3>
    <div>
    <button class="box-title btn btn-primary btn-outline btn-sm" onclick="add_log()"><i class="fa fa-add"></i> Tambah Log</button>
    </div>
    <div class="comment-center"  style="overflow-y: scroll;height:400px">
       <?php  foreach($log as $row){ ?>

        <div class="comment-body">
            <div class="user-img"> <img src="<?= base_url() ?>/asset/images/<?=($row->via_name=='call'?'telphone.png':'email.png')?>" alt="user" class="img-circle"> </div>
            <div class="mail-contnet" style="width:100%">
                <h5><?=$row->via_name?></h5>
                <span class="mail-desc" style="width:100%"><?=$row->ccl_desc ?></span>
                <span class="label label-rouded label-success"><?=strtoupper($row->ccl_status) ?></span> 
                <span class="time pull-right"><?=strtoupper($row->ccl_date) ?></span>
            </div>
        </div>  
       <?php  } ?> 
    </div>

<!-- Rekomendasi Program Marketing -->
<div class="modal fade bs-example-modal-lg" id="box-program" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Historikal Program Marketing yang Paling Banyak Direspon</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">Discount</div>
                    <div class="col-md-6">
                        <div id="progres1" class="progress progress-lg">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: <?=$promo_width['Discount'] ?>" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100"><?=$promo['Discount']?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Giveaways</div>
                    <div class="col-md-6">
                        <div id="progres1" class="progress progress-lg">
                            <div class="progress-bar progress-bar-primary" role="progressbar" style="width: <?=$promo_width['Giveaway']?>" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100"><?=$promo['Giveaway']?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">Free Samples</div>
                    <div class="col-md-6">
                        <div id="progres1" class="progress progress-lg">
                            <div class="progress-bar progress-bar-danger" role="progressbar" style="width: <?=$promo_width['Free Samples']?>;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100"><?=$promo['Free Samples']?></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">Rekomendasi Next Event: <strong><?= $promo_rec ?></strong></div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Rekomendasi Tindakan Selanjutnya -->
<div class="modal fade bs-example-modal-lg" id="box-komunikasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Rekomendasi Metode Komunikasi</h4>
            </div>
            <div class="modal-body">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td width="35%">Metode Komunikasi</td>
                        <td width="5%">:</td>
                        <td width="60%"><strong><?=$comm->ccc_metode?></strong></td>
                    </tr>
                    <tr>
                        <td>Preferensi Waktu</td>
                        <td>:</td>
                        <td><strong>Jam <?=substr($comm->ccc_starttime,0,5) ?> - <?=substr($comm->ccc_endtime,0,5) ?></strong></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Prediksi -->
<div class="modal fade bs-example-modal-lg" id="box-prediksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Prediksi Tipe Konsumen</h4>
            </div>
            <div class="modal-body">
                <div id="spline" style="height: 200px;"></div>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>


<!-- Rekomendasi Program Marketing -->
<div class="modal fade bs-example-modal-lg" id="add_log" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Log</h4>
            </div>
            <div class="modal-body">
                <form id="form_log" method="post">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="inp[ccl_id_trans]" id="ccl_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Via</div>
                    <div class="col-md-6">
                        <select class="form-control" name="inp[ccl_id_via]" id="ccl_id_via" required>
                            <option value="">-- Pilih Via --</option>
                            <?php foreach($via as $row){ ?>
                            <option value="<?=$row->via_id?>"><?=$row->via_name ?></option>
                            <?php } ?> 
                        </select>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Status</div>
                    <div class="col-md-6">
                        <select class="form-control" name="inp[ccl_status]" id="ccl_status" required>
                            <option value="">-- Pilih Status --</option>
                            <option value="Sukses">Sukses</option>
                            <option value="Gagal">Gagal</option>
                        </select>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Deskripsi</div>
                    <div class="col-md-6">
                        <textarea class="form-control" name="inp[ccl_desc]" id="ccl_desc" required  row="30" cols="20"></textarea>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6">
                        
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_log()"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


 
<div class="modal fade bs-example-modal-lg" id="add_step_1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Kualifikasi</h4>
            </div>
            <div class="modal-body">
                <form id="form_add_step_1" method="post"> 
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="1">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6">
                        <?php
                        foreach($ask['1'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"> <?=$row ?><br> 
                        <?php
                        } 
                        ?> 
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_step(1)"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal fade bs-example-modal-lg" id="edit_step_1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ubah Data Kualifikasi</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_step_1" method="post">
                <input type="hidden" name="id" id="id" value="<?=$data['1']['data']->ccs_id ?>" >
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="1">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" value="<?=$data['1']['data']->ccs_sales ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" value="<?=$data['1']['data']->ccs_probability ?>"  required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" value="<?=$data['1']['data']->ccs_close_date ?>"  required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6"> 
                        <input type="hidden" name="max_pilih" value="<?=count($ask[1]) ?>">
                        <?php
                        foreach($ask['1'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"
                            <?=($data['1']['answer'][$key]=='1'?'checked':'') ?>> <?=$row ?><br> 
                        <?php
                        } 
                        ?>  
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_step(1)"><i class="fa fa-pencil"></i>&nbsp;Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--========================================================================================================== -->



<div class="modal fade bs-example-modal-lg" id="add_step_2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Analisis Kebutuhan</h4>
            </div>
            <div class="modal-body">
                <form id="form_add_step_2" method="post"> 
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="2">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6">
                        <?php
                        foreach($ask['2'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"> <?=$row ?><br> 
                        <?php
                        } 
                        ?> 
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_step(2)"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal fade bs-example-modal-lg" id="edit_step_2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ubah Data Analisis Kebutuhan</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_step_2" method="post">
                <input type="hidden" name="id" id="id" value="<?=$data['2']['data']->ccs_id ?>" >
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="2">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" value="<?=$data['2']['data']->ccs_sales ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" value="<?=$data['2']['data']->ccs_probability ?>"  required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" value="<?=$data['2']['data']->ccs_close_date ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6"> 
                        <input type="hidden" name="max_pilih" value="<?=count($ask[2]) ?>">
                        <?php
                        foreach($ask['2'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"
                            <?=($data['2']['answer'][$key]=='1'?'checked':'') ?>> <?=$row ?><br> 
                        <?php
                        } 
                        ?>  
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_step(2)"><i class="fa fa-pencil"></i>&nbsp;Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--========================================================================================================== -->


<div class="modal fade bs-example-modal-lg" id="add_step_3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Penawaran Harga</h4>
            </div>
            <div class="modal-body">
                <form id="form_add_step_3" method="post"> 
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="3">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Rekomendasi Pemasaran</div>
                    <div class="col-md-6">
                        <select class="form-control" name="inp[ccs_market_recommend]" id="ccs_market_recommend" required>
                            <option value="">-- Pilih Rekomendasi Pemasaran --</option>
                            <option value="Cross Selling">Cross Selling</option>
                            <option value="Up Selling">Up Selling</option>
                            <option value="Bunding">Bunding</option>
                        </select>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6">
                        <?php
                        foreach($ask['3'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"> <?=$row ?><br> 
                        <?php
                        } 
                        ?> 
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_step(3)"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal fade bs-example-modal-lg" id="edit_step_3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ubah Data Penawaran Harga</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_step_3" method="post">
                <input type="hidden" name="id" id="id" value="<?=$data['3']['data']->ccs_id ?>" >
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="3">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" value="<?=$data['3']['data']->ccs_sales ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" value="<?=$data['3']['data']->ccs_probability ?>"  required>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" value="<?=$data['3']['data']->ccs_close_date ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Rekomendasi Pemasaran</div>
                    <div class="col-md-6">
                        <select class="form-control" name="inp[ccs_market_recommend]" id="ccs_market_recommend" required> 
                            <option value="Cross Selling" <?=($data['3']['data']->ccs_market_recommend=='Cross Selling'?'selected':'') ?>>Cross Selling</option>
                            <option value="Up Selling" <?=($data['3']['data']->ccs_market_recommend=='Up Selling'?'selected':'') ?>>Up Selling</option>
                            <option value="Bunding" <?=($data['3']['data']->ccs_market_recommend=='Bunding'?'selected':'') ?>>Bunding</option>
                        </select>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6"> 
                        <input type="hidden" name="max_pilih" value="<?=count($ask[3]) ?>">
                        <?php
                        foreach($ask['3'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"
                            <?=($data['3']['answer'][$key]=='1'?'checked':'') ?>> <?=$row ?><br> 
                        <?php
                        } 
                        ?>  
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_step(3)"><i class="fa fa-pencil"></i>&nbsp;Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--========================================================================================================== -->


<div class="modal fade bs-example-modal-lg" id="add_step_4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Negosiasi</h4>
            </div>
            <div class="modal-body">
                <form id="form_add_step_4" method="post"> 
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="4">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6">
                        <?php
                        foreach($ask['4'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"> <?=$row ?><br> 
                        <?php
                        } 
                        ?> 
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_step(4)"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal fade bs-example-modal-lg" id="edit_step_4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ubah Data Negosiasi</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_step_4" method="post">
                <input type="hidden" name="id" id="id" value="<?=$data['4']['data']->ccs_id ?>" >
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="4">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" value="<?=$data['4']['data']->ccs_sales ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" value="<?=$data['4']['data']->ccs_probability ?>"  required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" value="<?=$data['4']['data']->ccs_close_date ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Pilih</div>
                    <div class="col-md-6"> 
                        <input type="hidden" name="max_pilih" value="<?=count($ask[4]) ?>">
                        <?php
                        foreach($ask['4'] as $key => $row){ ?>
                            <input type="checkbox" name="inp[ccs_q<?=$key?>]" id="ccs_q<?=$key?>" value="1"
                            <?=($data['4']['answer'][$key]=='1'?'checked':'') ?>> <?=$row ?><br> 
                        <?php
                        } 
                        ?>  
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_step(4)"><i class="fa fa-pencil"></i>&nbsp;Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--========================================================================================================== -->

<div class="modal fade bs-example-modal-lg" id="add_step_5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Penawaran Harga</h4>
            </div>
            <div class="modal-body">
                <form id="form_add_step_5" method="post"> 
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="5">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" readonly name="inp[ccs_close_date]" id="ccs_close_date" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div> 
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="save_step(5)"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<div class="modal fade bs-example-modal-lg" id="edit_step_5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Ubah Data Penawaran Harga</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_step_5" method="post">
                <input type="hidden" name="id" id="id" value="<?=$data['5']['data']->ccs_id ?>" >
                <input type="hidden" name="inp[ccs_step]" id="ccs_step" value="5">
                <input type="hidden" name="inp[ccs_id_trans]" id="ccs_id_trans" value="<?=$cct_id ?>">
                <div class="row">
                    <div class="col-md-4">Jumlah Rp</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_sales]" id="ccs_sales" value="<?=$data['5']['data']->ccs_sales ?>" required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Probabilitas (%)</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[ccs_probability]" id="ccs_probability" value="<?=$data['5']['data']->ccs_probability ?>"  required>
                    </div>
                </div>  
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">Close Date</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control yearpicker" name="inp[ccs_close_date]" id="ccs_close_date" value="<?=$data['5']['data']->ccs_close_date ?>" readonly required>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_step(5)"><i class="fa fa-pencil"></i>&nbsp;Update</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--========================================================================================================== -->


<div class="modal fade bs-example-modal-lg" id="edit_sales" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Data Penawaran Harga</h4>
            </div>
            <div class="modal-body">
                <form id="form_edit_sales" method="post">  
                <input type="hidden" name="id" id="id" value="<?=$cct_id ?>" >
                <div class="row">
                    <div class="col-md-4">Sales</div>
                    <div class="col-md-6">
                    <input type="text" class="form-control" name="inp[cct_sales_name]" id="cct_sales_name" value="<?=$owner['ct_salesman'] ?>" required>
                    </div>
                </div> 
                <div class="row">&nbsp;</div>
                <div class="row"> 
                    <div class="col-md-4"></div>
                    <div class="col-md-6"> 
                    <button type="button" class="box-title btn btn-primary btn-sm" onclick="update_sales()"><i class="fa fa-save"></i>&nbsp;Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div> 

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>
<script src="<?= base_url() ?>assets/validation/validate.min.js"></script>
<script src="<?= base_url() ?>assets/plugin/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/grouped-categories.js"></script>

<script>
var tb 		= '#table';      
var baseurl = '<?=base_url()?>index.php/crm';
    $(document).ready(function () {
        //tipe();
        spline();

        
        $('.yearpicker').datepicker({
            format: "yyyy-mm-dd", 
            autoclose: true
        });

        $(tb).dataTable({
            'ajax': {
                'url':baseurl+'/json_detail',
                'data' : function(data) {
                    data.id	    = '<?=$cct_id ?>'
                }, 
                'method' : 'post'
            }, 
            'order':[
                [0, 'desc']
            ],
            'columnDefs': [   
            ],
            "drawCallback": function( settings ) { 
            },
            'scrollX': true, 
            autoWidth: false,
            dom: '<"datatable-scroll"t>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            "lengthMenu": [
                [10, 20, 30, 50, 100, 150, -1],
                [10, 20, 30, 50, 100, 150, "All"]
            ], 
            "pageLength": 30, // default records per page 
            "autoWidth": false, // disable fixed width and enable fluid table
            "processing": true, // enable/disable display message box on record load
            "serverSide": true, // enable/disable server side ajax loading,

        });  
    });

    function box(id)
    {
        $('#'+id).modal({keyboard: false, backdrop: 'static'});
    } 
	
    function add_log()
    {  
        $('#add_log').modal({keyboard: false, backdrop: 'static'});
    }
	
    function add_step(step)
    {  
        $('#add_step_'+step).modal({keyboard: false, backdrop: 'static'});
    }
	
    function edit_step(step)
    {  
        $('#edit_step_'+step).modal({keyboard: false, backdrop: 'static'});
    }
	
    function edit_sales()
    {  
        $('#edit_sales').modal({keyboard: false, backdrop: 'static'});
    }

    
    function save_log()
    {
        if($("#form_log").valid())
        {
            $.ajax({
                url:baseurl+'/save_log',
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $('#form_log').serialize(),
                success : function(e) {
                    if(e.status == 'ok;') 
                    { 
                        alert("Berhasil Ditambah");
                        location.reload();
                    } 
                    else alert(e.text);
                },
                error : function() {
                    alert('Error');	 
                },
                beforeSend : function() {
                    $('.preloader').show();
                },
                complete : function() {
                    $('.preloader').hide();
                }
            });
        }
    }

    function save_step(step)
    {
        if($("#form_add_step_"+step).valid())
        {
            $.ajax({
                url:baseurl+'/save_step',
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $("#form_add_step_"+step).serialize(),
                success : function(e) {
                    if(e.status == 'ok;') 
                    { 
                        alert("Berhasil Ditambah");
                        location.reload();
                    } 
                    else alert(e.text);
                },
                error : function() {
                    alert('Error');	 
                },
                beforeSend : function() {
                    $('.preloader').show();
                },
                complete : function() {
                    $('.preloader').hide();
                }
            });
        }
    }

    function update_sales()
    {
        if($("#form_edit_sales").valid())
        {
            $.ajax({
                url:baseurl+'/update_sales',
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $("#form_edit_sales").serialize(),
                success : function(e) {
                    if(e.status == 'ok;') 
                    { 
                        alert("Berhasil Diubah");
                        location.reload();
                    } 
                    else alert(e.text);
                },
                error : function() {
                    alert('Error');	 
                },
                beforeSend : function() {
                    $('.preloader').show();
                },
                complete : function() {
                    $('.preloader').hide();
                }
            });
        }
    }

    
    function update_step(step)
    {
        if($("#form_edit_step_"+step).valid())
        {
            $.ajax({
                url:baseurl+'/update_step',
                global:false,
                async:true,
                type:'post',
                dataType:'json',
                data: $("#form_edit_step_"+step).serialize(),
                success : function(e) {
                    if(e.status == 'ok;') 
                    { 
                        alert("Berhasil Diubah");
                        location.reload();
                    } 
                    else alert(e.text);
                },
                error : function() {
                    alert('Error');	 
                },
                beforeSend : function() {
                    $('.preloader').show();
                },
                complete : function() {
                    $('.preloader').hide();
                }
            });
        }
    }


    function tipe()
    {
        Highcharts.chart('tipe', {
            chart: {
                type: 'column'
            },
            title: {
                text: null
            },
            xAxis: {
                labels: {
                    rotation: 0,
                    x: -8,
                    align: 'right',
                    style: {
                        fontSize: '10px'
                    }
                },
                categories: [{
                    name: '2018',
                    categories: ['Q1', 'Q2', 'Q3', 'Q4']
                }, {
                    name: '2019',
                    categories: ['Q1', 'Q2', 'Q3', 'Q4']
                }]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'x-axis'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: [{
                name: 'x',
                data: [5, 3, 4, 5, 5, 5, 5, 5]
            }]
        });
    }

    function spline()
    {
        Highcharts.chart('spline', {
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            xAxis: { 
                categories: [{
                    name: '<?=date('Y')?>',
                    categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
                }],
                plotBands: [{
                    color: '#efefef',
                    from: -1,
                    to: <?=$level_point?>,
                    label: {
                        text: 'Data Real',
                        style: {
                            color: '#606060'
                        }
                    }
                }, {
                    color: '#CFD1D1',
                    from: <?=$level_point?>,
                    to: 12,
                    label: {
                        text: 'Rekomendasi Prediksi',
                        style: {
                            color: '#606060'
                        }
                    }
                }]
            },
            yAxis: {
                title: {
                    text: 'Sales Profit'
                },
                plotLines: [{
                    value: 250000000,
                    color: 'red',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Bottom Sales'
                    }
                }, {
                    value: 325000000,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: 'Goal Sales <?=date('Y')?>'
                    }
                }]
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            if(this.y == 1 || this.y == 2)
                                return 'Reguler';
                            else if(this.y == 3 || this.y == 4)
                                return 'Silver';
                            else if(this.y == 5 || this.y == 6 || this.y == 7)
                                return 'Gold';
                            else if(this.y == 8)
                                return 'Platinum';
                        }
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                showInLegend: false,
                name: 'Profit in <?=date('Y')?>',
                data: <?=$level ?>
            }]
        });
    }
</script>