
<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<?php
$area = array('AMBON','BALIKPAPAN','BANDA ACEH','BANDAR LAMPUNG','BANDUNG','BANJARMASIN','BATAM','BEKASI','BENGKULU','BOGOR','CIREBON','DENPASAR','DEPOK','GORONTALO','JAKARTA MAJAPAHIT','JAKARTA MATRAMAN','JAMBI','JAYAPURA','JEMBER','KANTOR PUSAT','KENDARI','KUPANG','MADIUN','MAKASSAR','MALANG','MANADO','MATARAM','MEDAN','PADANG','PALANGKARAYA','PALEMBANG','PALU','PANGKAL PINANG','PEKANBARU','PEMATANG SIANTAR','PONTIANAK','PURWOKERTO','SAMARINDA','SEMARANG','SERANG','SERPONG','SIDOARJO','SORONG','SUKABUMI','SURABAYA','SURAKARTA','TANJUNG PINANG','TASIKMALAYA','TEGAL','TERNATE','YOGYAKARTA');

$prod = array('ASIFIT','BATUGIN','ENKASARI','FITOCARE','FITUNO','LOVETEST','MAGASIDA','NEUROVIT-E','OTC','SALICYLKF','VIDISEP (ANTIFECT)');

$obat = array('ALBUNORM 20% 100 ML','ALBUNORM 25% 100 ML','ALBUNORM 25% 50 ML','BFLUID 500 ML INFUS','BIOPROST KAPSUL @ 30','BIORAZON 1 GR INJ','BIOVISION KAPSUL @ 100','BIOVISION KAPSUL @ 30','BURNAZIN PLUS 25 GR','DEXTOFEN 25 MG INJ','ESOMAX 40 MG INJ','FRAMYCETIN SULFATE 10X10 CM','GERDILIUM 60 ML SUSP (OTTO)','GLIARIDE 2 MG TAB @ 50 (OTTO)','GLIARIDE 3 MG TAB @ 50 (OTTO)','HI-BONE 600 TABLET (OTTO)','HI-BONE ACT CHEW GRAPE (OTTO)','HI-BONE ACT CHEW STRAW (OTTO)','JAMSI 100 ML','NAIRET 0','NAIRET 2','NATUR E DAILY NOURISHING CALMING 100 ML','NATUR E DAILY NOURISHING CALMING 245 ML','NATUR E DAILY NOURISHING ENERGIZING 100','NATUR E DAILY NOURISHING ENERGIZING 245','NATUR E DAILY NOURISHING RELAXING 100 M','NATUR E DAILY NOURISHING RELAXING 245 M','NATUR E NOURISHING 300IU 16\'S','NATUR E NOURISHING 300IU 32\'S','NATUR E NOURISHING 300IU 4 X 20\'S','NICARDIPINE HCL INJ 1 MG','OCTANATE 1000 IU','OCTANATE 250 IU','OCTANATE 500 IU','OCTANINE 1000 IU','OCTANINE 250 IU','OCTANINE 500 IU','OCTAPLEX','OMEPRAZOLE 40 MG INJ (DARYA)','OPICORT 4 MG TABLET','OPIDIAR SUSP 120 ML (OTTO)','OPIPENTIN 300 MG KPS (OTTO)','OPIPHEN 500 MG KPS (OTTO)','OSTARIN FORTE 200 MG SUSP (OTTO)','OTSULIP 20 % 250 ML INFUS','PREDNICORT 125 MG INJ (OTTO)','SCANLUX 300 @ 100 ML','SCANLUX 300 @ 50 ML','SCANLUX 370 @ 100 ML','SCANLUX 370 @ 50 ML','TORAMINE 10 MG TABLET','TROGYSTATIN OVULA','VALEPTIK 250 MG SYR 120 ML (OTTO)','1527-1 TRANSPORE','1533-0 MICROPORE SKIN TONE','1533-1 MICROPORE SKIN TONE','4-EPEEDO-10 EPIRUBICIN 10MG','4-EPEEDO-50 EPIRUBICIN 50MG','AAFACT 500 UI','ABBOCAT-T 18 GA WISO GREEN','ABBOCAT-T 20 GA WISO PINK','ABBOCAT-T 22 GA WISO BLUE','ABBOCAT-T 24 GA WISO YELLOW','ABBOCAT-T 26 GA WISO WHITE','ABINGEM 200 MG GEMCITABINE INJ','ABINGEM-1000MG GEMCITABINE INJ','ACDAT 20 MG / G CREAM','ACEPTAN 500 CC + DISPENSER','ACETOSAL 100 MG (MARIN)','ACETYLCYSTEINE 200 MG @ 60','ACYCLOVIR 200 MG (DUS 100 TAB)','ACYCLOVIR 200 MG TABLET (100)','ACYCLOVIR 200 MG TABLET (EXPORT)','ACYCLOVIR 400 MG (DUS 100 TAB)','ACYCLOVIR 400 MG TABLET (100)','ACYCLOVIR 400 MG TABLET (EXPORT)','ACYCLOVIR CREAM 5 GR (GIF)','ACYCLOVIR CREAM 5%(DUS 25 TUBE @ 5 GRAM)','ACYCLOVIR KRIM','ADULT/PEDIATRIC TYPE T-BLUE','ADULT/PEDIATRIC TYPE T-PINK','AG-0103B ANGIO DRAPE','AG-2106 ANGIOGRAPHY SET','AGANI NEEDLE 18GX38MM','AGANI NEEDLE 21GX38MM','AGANI NEEDLE 22GX38MM','AGANI NEEDLE 23GX32MM','AGANI NEEDLE 24GX25MM','AGANI NEEDLE 25GX16MM','AGANI NEEDLE 25GX25MM','AGANI NEEDLE 26GX13MM','AGANI NEEDLE 27GX13MM','AGS BED SIDE CABINET','AGS EMERGENCY MOBILE STRETCHER','AGS INFUSE STAND 5 LEGS','AGS OVER BED TABLE','AIM CKMB REAGEN KIT','AIM DENGUE ANTIGEN','AIM Salmonella Thypi IgM'); 

$js_obat = json_encode($obat); 

$products = array();
foreach($prod as $p) {
    $data = array();
    for($i=1; $i<=count($area); $i++) {
        $data[] = rand(50,200);
    }
    $products[] = array('name' => $p, 'data' => $data);
}
?>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    th {
        color:#3e4d6c;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div id="maps" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>

<!-- .row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                EVALUASI PROFITABILITAS EVENT
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- .row
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                by Status Biaya
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="chart2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->

<!-- .row
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                by Area per Product
            </div>
            <div class="panel-body">
                <div id="chart3" style="height: 1000px"></div>
                <br>
                <h4><strong><i data-icon="7" class="linea-icon linea-basic fa-fw"></i> REKOMENDASI</strong></h4><hr>
                <table>
                    <?php foreach($area as $a) { ?>
                    <tr>
                        <td width="200"><?= $a ?></td>
                        <td>Batugin, Fituno</td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>-->

<!-- /.row -->

<div class="modal fade bs-example-modal-lg" id="frmbox" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="frmbox-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="total_sales">
                    </div>
                </div>
                <?php 
                for($i=0;$i<=9;$i++) {
                ?>
                <div class="row">
                    <div class="col-md-4" id="merk_<?=$i?>"></div>
                    <div class="col-md-6">
                        <div id="progres1" class="progress progress-lg">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 100%;" aria-valuenow="71" aria-valuemin="0" aria-valuemax="100" id="progressbar_<?=$i?>"></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12" id="top_3"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade bs-example-modal-lg" id="frmbox-rec" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="frmbox-title">Evaluasi Profitable Event</h4>
            </div>
            <div class="modal-body" id="body-rec">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script>
    $(document).ready(function () {
        $('.tdselect').click(function() {
            window.location.href = '<?= base_url() ?>index.php/crm/crm_detail';
        });
        
        maps();
    });

    Highcharts.chart('chart1', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            title: {
                text: '<strong>Tahun 2019</strong>'
            },
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            min: 0,
            title: {
                text: '<strong>OI (%)</strong>'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y} %<br/>Total: {point.stackTotal} %'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    //formatter: function () { return 'aa'; }
                    format: '{point.y}%'
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            rec(this.series.name, this.y)
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Big Event',
            color: '#f75b36',
            data: [23.38,24.58,23.55,25.88,25.61,25.48,23.17,23.69,28.79,23.92,24.68,23.35]
        },{
            name: 'Reguler Event',
            color: '#00b5c2',
            data: [9,9.21,9.1,8.98,9.16,9.02,9.12,9.14,9.55,9.05,8.93,8.98]
        }]
    });

    /*Highcharts.chart('chart2', {
        chart: {
            type: 'column'
        },
        title: {
            text: null
        },
        xAxis: {
            title: {
                text: '<strong>Status Biaya</strong>'
            },
            categories: ['Free/Tanpa Biaya', 'Reimburse', 'Kasbon Pusat']
        },
        yAxis: {
            min: 0,
            title: {
                text: '<strong>Sales (Milion Rupiah)</strong>'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y} Juta Rupiah<br/>Total: {point.stackTotal} Juta Rupiah'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
            name: 'Big Event',
            color: '#f75b36',
            data: [rnd(300,500), rnd(300,500), rnd(300,500)]
        },{
            name: 'Reguler Event',
            color: '#00b5c2',
            data: [rnd(100,200), rnd(100,200), rnd(100,200)]
        }]
    });

    Highcharts.chart('chart3', {
        chart: {
            type: 'bar'
        },
        title: {
            text: null
        },
        xAxis: {
            categories: < ?= json_encode($area) ?>
        },
        yAxis: {
            min: 0,
            title: {
                text: '<strong>Sales (Milion Rupiah)</strong>'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                /*dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }*
            }
        },
        series: < ?= json_encode($products) ?>
    });*/

    function rec(event, oi) {
        var sign = '';
        var rec = '';
        var e = '';

        if(oi >= 20) {
            sign = '>';
            e = 'Profitable';
            rec = 'OK, GO!';
        } else {
            sign = '<';
            e = 'Not Profitable';
            rec = 'Not OK!';
        }
        var html = '<h3 class="text-center">'+event+'<br>Besarnya OI '+oi+'% '+sign+' 20%, '+e+' Event.<br>Rekomendasi: <strong>'+rec+'</strong></h3>';

        $('#body-rec').html(html);
        $('#frmbox-rec').modal({keyboard: false, backdrop: 'static'});
    }

    function rnd(min, max) {
        return Math.floor(Math.random() * (+max - +min)) + +min;
    }

    function box(total) {
        var obat = <?= $js_obat ?>;
        var top3 = [];

        var prosentase = [rnd(11, 13),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90),rnd(85, 90)];
        $("#total_sales").html('Total Sales: <strong>Rp. '+total+'.000.000</strong>');
        total     = total*1000000;
        totaltemp = total;
        var max = (prosentase[0]/100)*totaltemp;

        $(".progress-bar").attr('aria-valuemax',max);
        for(var i=0;i<=9;i++){
            var tot = obat.length-1;
            var key = rnd(0,tot);
            $("#merk_"+i).html(obat[key]);


            var point = (prosentase[i]/100)*totaltemp;
            totaltemp = point;
            var width = (totaltemp/max)*100;
            console.log(width);
            // $("#progressbar_"+i).css("width",width+"%");
            $("#progressbar_"+i).attr('aria-valuenow', totaltemp).css("width",width+"%");
            $("#progressbar_"+i).html(formatRupiah(totaltemp, ""));
            if(top3.length!=3){
                top3[i] = obat[key];
            }
            obat = obat.slice(0, key).concat(obat.slice(key + 1, obat.length));
        }
        $("#top_3").html('Rekomendasi Top 3: '+top3.join(", "));
        $('#frmbox').modal({keyboard: false, backdrop: 'static'});
    }

    function formatRupiah(angka, prefix){
        var number_string = angka.toString().replace('.', ',').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        // rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function maps()
    {
        var data = [
            ['id-3700', rnd(25,400)],
            ['id-ac', rnd(25,400)],
            ['id-ki', rnd(25,400)],
            ['id-jt', rnd(25,400)],
            ['id-be', rnd(25,400)],
            ['id-bt', rnd(25,400)],
            ['id-kb', rnd(25,400)],
            ['id-bb', rnd(25,400)],
            ['id-ba', rnd(25,400)],
            ['id-ji', rnd(25,400)],
            ['id-ks', rnd(25,400)],
            ['id-nt', rnd(25,400)],
            ['id-se', rnd(25,400)],
            ['id-kr', rnd(25,400)],
            ['id-ib', rnd(25,400)],
            ['id-su', rnd(25,400)],
            ['id-ri', rnd(25,400)],
            ['id-sw', rnd(25,400)],
            ['id-la', rnd(25,400)],
            ['id-sb', rnd(25,400)],
            ['id-ma', rnd(25,400)],
            ['id-nb', rnd(25,400)],
            ['id-sg', rnd(25,400)],
            ['id-st', rnd(25,400)],
            ['id-pa', rnd(25,400)],
            ['id-jr', rnd(25,400)],
            ['id-1024', rnd(25,400)],
            ['id-jk', rnd(25,400)],
            ['id-go', rnd(25,400)],
            ['id-yo', rnd(25,400)],
            ['id-kt', rnd(25,400)],
            ['id-sl', rnd(25,400)],
            ['id-sr', rnd(25,400)],
            ['id-ja', rnd(25,400)]
        ];

// Create the chart
        Highcharts.mapChart('maps', {
            chart: {
                map: 'countries/id/id-all'
            },

            title: {
                text: 'Marketing Event'
            },

            subtitle: {
                text: '<strong>Klik Salah Satu Daerah Untuk Melihat Data Detail dan Rekomendasi </strong>',
                style: {
                    color: '#2c5ca9'
                }
            },

            mapNavigation: {
                enabled: true,
                buttonOptions: {
                    verticalAlign: 'bottom'
                }
            },

            colorAxis: {
                min: 0
            },

            series: [{
                data: data,
                name: 'Provinsi',
                cursor: 'pointer',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.value}'
                },
                point:{
                    events:{
                        click: function(){
                            box(this.value);
                        }
                    }
                }
            }]
        });
    }
</script>