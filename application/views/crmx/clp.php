
<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<?php
$d1 = json_decode('[{"id":10002567,"name":"AP RAUDAH FARMA(BRB)","rp":18545000},{"id":10002581,"name":"AP. AMANDIT","rp":22801000},{"id":10002695,"name":"APOTEK K-24 (BJM)","rp":11324000},{"id":10002854,"name":"R S K BEDAH BANJARMASIN SIAGA","rp":42234000},{"id":10002876,"name":"RSU. PAMBALAH BATUNG (RUTIN)","rp":30360000},{"id":10004363,"name":"AP.NURUL FIKRI","rp":27600000},{"id":10004514,"name":"PT. BUNDA MEDIK(AP. BUNDA MARGONDA)","rp":85857000},{"id":10007057,"name":"RS. KAMBANG","rp":85560000},{"id":10008919,"name":"GROSIR NUSA INDAH","rp":48760000},{"id":10014039,"name":"APOTEK BANDUNG","rp":97808000}]');
$d2 = json_decode('[{"id":10015478,"name":"APOTEK ASIH HUSADA","rp":248400000},{"id":10017808,"name":"RSUI. MUTIARA BUNDA (BREBES)","rp":25286000},{"id":10020499,"name":"ABDUL WACHID/AP. SIDO WARAS (JPR)","rp":26950000},{"id":10020910,"name":"APOTEK 39  (KDS)","rp":900000},{"id":10025982,"name":"AP. PRIMA - TSK","rp":160380000},{"id":10028323,"name":"KIMIA FARMA (PERSERO) Tbk, PT  - MM","rp":19690000},{"id":10028340,"name":"APOTIK KIMIA FARMA (RSCM)","rp":6716000},{"id":10051893,"name":"APOTEK JESAYA FARMA","rp":21450000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23980000},{"id":10055432,"name":"TOKO GUNASALMA I","rp":23276000}]');
$d3 = json_decode('[{"id":20000129,"name":"PT. KIMIA FARMA APOTEK","rp":10966000},{"id":20000880,"name":"KFTD AMBON","rp":14006000},{"id":20000882,"name":"KFTD BANDA ACEH","rp":658000},{"id":20000883,"name":"KFTD BANDAR LAMPUNG","rp":9460000},{"id":20000884,"name":"KFTD BANDUNG","rp":13320000},{"id":20000885,"name":"KFTD BANJARMASIN","rp":96725000},{"id":20000886,"name":"PT.KIMIA FARMA TRADING & DISTRIBUTI","rp":1528000},{"id":20000887,"name":"KFTD BEKASI","rp":1826000},{"id":20000888,"name":"KFTD BENGKULU","rp":2015000},{"id":20000889,"name":"KFTD BOGOR","rp":139000}]');
$d4 = json_decode('[{"id":20000890,"name":"KFTD CIREBON","rp":48473000},{"id":20000891,"name":"KFTD DENPASAR","rp":35000000},{"id":20000893,"name":"KFTD JAKARTA I","rp":16699000},{"id":20000894,"name":"KFTD JAKARTA II","rp":7155000},{"id":20000895,"name":"KFTD JAMBI","rp":2450000},{"id":20000896,"name":"KFTD JAYAPURA","rp":2300000},{"id":20000897,"name":"KFTD JEMBER","rp":18172000},{"id":20000898,"name":"KFTD KENDARI","rp":176283000},{"id":20000899,"name":"KFTD KUPANG","rp":128451000},{"id":20000900,"name":"KFTD MADIUN","rp":13061000}]');
$d5 = json_decode('[{"id":20000902,"name":"KFTD MALANG","rp":43989000},{"id":20000903,"name":"KFTD TERNATE","rp":95930000},{"id":20000904,"name":"KFTD MANADO","rp":26550000},{"id":20000906,"name":"KFTD MEDAN","rp":45540000},{"id":20000907,"name":"KFTD PADANG","rp":39146000},{"id":20000909,"name":"KFTD PALEMBANG","rp":24050000},{"id":20000910,"name":"KFTD PALU","rp":11855000},{"id":20000911,"name":"KFTD PANGKAL PINANG","rp":10070000},{"id":20000912,"name":"KFTD PEKAN  BARU","rp":225000},{"id":20000913,"name":"KFTD PEMATANGSIANTAR","rp":14136000}]');

function sum($data) {
    $a = 0;
    foreach($data as $row) {
        $a += $row->rp;
    }
    return 'Rp. '.number_format($a,0);
}
?>

<style>
    .tdselect {
        cursor: pointer;
    }
    .tdselect:hover {
        background-color: #ffd54f !important;
    }
    th {
        color:#3e4d6c;
    }
</style>

<!-- .row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ranking Program Marketing yang Direspon Setiap Konsumen
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th width="40%" colspan="2" rowspan="2" style="vertical-align: middle">ID KONSUMEN</th>
                        <th width="40%" colspan="4" style="text-align: center">PROGRAM MARKETING</th>
                        <th width="20%" rowspan="2" style="vertical-align: middle">REKOMENDASI</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;vertical-align: middle" width="10%">DISCOUNT</th>
                        <th style="text-align: center;vertical-align: middle" width="10%">SAMPLES</th>
                        <th style="text-align: center;vertical-align: middle" width="10%">SEMINAR SUPPORT</th>
                        <th style="text-align: center;vertical-align: middle" width="10%">GIVE A WAYS</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($d1 as $d) {
                        $a = array(
                            'discount' => rand(1,10),
                            'samples' => rand(1,10),
                            'seminar_support' => rand(1,10),
                            'give_a_ways' => rand(1,10),
                        );
                        $high = array_keys($a, max($a));
                    ?>
                    <tr>
                        <td><strong><?= $d->id ?></strong></td>
                        <td><strong><?= $d->name ?></strong></td>
                        <td style="text-align: center"><?= $a['discount'] ?>x</td>
                        <td style="text-align: center"><?= $a['samples'] ?>x</td>
                        <td style="text-align: center"><?= $a['seminar_support'] ?>x</td>
                        <td style="text-align: center"><?= $a['give_a_ways'] ?>x</td>
                        <td><?= strtoupper(str_replace('_', ' ', $high[0])) ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                    foreach($d2 as $d) {
                        $a = array(
                            'discount' => rand(1,10),
                            'samples' => rand(1,10),
                            'seminar_support' => rand(1,10),
                            'give_a_ways' => rand(1,10),
                        );
                        $high = array_keys($a, max($a));
                        ?>
                        <tr>
                            <td><strong><?= $d->id ?></strong></td>
                            <td><strong><?= $d->name ?></strong></td>
                            <td style="text-align: center"><?= $a['discount'] ?>x</td>
                            <td style="text-align: center"><?= $a['samples'] ?>x</td>
                            <td style="text-align: center"><?= $a['seminar_support'] ?>x</td>
                            <td style="text-align: center"><?= $a['give_a_ways'] ?>x</td>
                            <td><?= strtoupper(str_replace('_', ' ', $high[0])) ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Ranking Loyalitas Setiap Konsumen
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th width="40%" colspan="2">ID KONSUMEN</th>
                        <th width="20%">LEVEL</th>
                        <th width="40%">PREDIKSI TH.2020</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; foreach($d1 as $d) { ?>
                        <tr>
                            <td><strong><?= $d->id ?></strong></td>
                            <td><strong><?= $d->name ?></strong></td>
                            <td><?= ($i>=1 and $i<=5) ? '<i class="fa fa-star" style="color: #78909c"></i> Platinum' : '<i class="fa fa-star" style="color: #fdd835"></i> Gold' ?></td>
                            <td><?= ($i>=1 and $i<=5) ? 'Tetap' : 'Menjadi Platinum' ?></td>
                        </tr>
                    <?php $i++; } ?>
                    <?php $i = 1; foreach($d1 as $d) { ?>
                        <tr>
                            <td><strong><?= $d->id ?></strong></td>
                            <td><strong><?= $d->name ?></strong></td>
                            <td><i class="fa fa-star" style="color: #e0e0e0"></i> Silver</td>
                            <td><?= ($i>=1 and $i<=5) ? 'Menjadi Gold' : 'Tetap' ?></td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/export-data.js"></script>
<script>
    $(document).ready(function () {
        $('.tdselect').click(function() {
            window.location.href = '<?= base_url() ?>index.php/crm/crm_detail';
        })
    });

    Highcharts.chart('area-chart', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        },
        yAxis: {
            title: {
                text: 'Sales Won and Negotiation > 70%'
            },
            plotLines: [{
                value: 250000000,
                color: 'red',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Bottom Sales'
                }
            }, {
                value: 325000000,
                color: 'green',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Goal Sales 2019'
                }
            }]
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Sales in 2019',
            data: [118545000, 152801000, 185286000, 226950000, 227276000, 235679000, 257823400, 311491000]
        }]
    });
</script>