<style type="text/css">
    .last_update {
        position: absolute;


        font-family: Roboto;
        font-style: italic;
        font-weight: normal;
        font-size: 12px;

        color: #4F4F4F;
    }

    .form-horizontal .form-group {
        margin-bottom: 0px;
    }
    .svg-center svg {
        margin: auto;
    }
    svg {
        display: block;
    }
    .chart-widget-legend {
        margin: 10px 0 0 0;
        padding: 0;
        font-size: 12px;
        text-align: center;
    }
    .chart-widget-legend li {
        margin: 5px 10px 0;
        padding: 7px 8px 5px;
        display: inline-block;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div id="maps" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>

        <div class="row">
    <div class="col-md-12">
        <div class="white-box" style="min-height: 110px">
            <form class="form-horizontal">
                <div class="form-group" style="width: 100px; float: left; margin-right: 8px">
                    <label class="col-md-12">Jenis</label>
                    <div class="col-md-12">
                        <select class="form-control"><option value="">B2C</option></select>
                    </div>
                </div>
                <div class="form-group" style="width: 150px; float: left; margin-right: 8px">
                    <label class="col-md-12">Wilayah</label>
                    <div class="col-md-12">
                        <select class="form-control"><option value="">Jawa Barat</option></select>
                    </div>
                </div>
                <div class="form-group" style="width: 150px; float: left; margin-right: 8px">
                    <label class="col-md-12">Lokasi</label>
                    <div class="col-md-12">
                        <select class="form-control"><option value="">Bandung</option></select>
                    </div>
                </div>
                <div class="form-group" style="width: 150px; float: left; margin-right: 8px">
                    <label class="col-md-12">Bulan</label>
                    <div class="col-md-12">
                        <select class="form-control"><option value="">Januari</option></select>
                    </div>
                </div>
                <div class="form-group" style="width: 100px; float: left; margin-right: 8px">
                    <label class="col-md-12">Tahun</label>
                    <div class="col-md-12">
                        <select class="form-control"><option value="">2018</option></select>
                    </div>
                </div>
                <div class="form-group" style="width: 100px; float: left; margin-right: 8px">
                    <label class="col-md-12">&nbsp;</label>
                    <div class="col-md-12">
                        <button class="btn btn-primary">Filter</button>
                    </div>
                </div>
                <div style="float: none">&nbsp;</div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Segmentasi by Resensi</h3>
            <div id="resensi" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Segmentasi by Monetary</h3>
            <div id="monetary" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Segmentasi by Frequency</h3>
            <div id="frequency" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Segmentasi Dokter Berdasarkan Jumlah Peresapan Produk KF</h3>
            <div id="peresapan" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="white-box">
            <h3 class="box-title">Total Sales</h3>
            <div id="ts" class="svg-center" style="width: 100%; height: 300px;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="white-box">
            <h3 class="box-title">Top 5 Dokter Non-KFA</h3>
            <div id="top-5-non" style="width: 100%; height: 300px;"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="white-box">
            <h3 class="box-title">Top 5 Dokter KFA</h3>
            <div id="top-5" style="width: 100%; height: 300px;"></div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="frmbox" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="frmbox-title"></h4>
            </div>
            <div class="modal-body">
                <h3 class="box-title">Top 5 Dokter</h3>
                <div id="top-5-modal" style="width: 100%; height: 300px;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript" src="<?= base_url() ?>assets/d3/d3.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/d3/d3_tooltip.js"></script>

<script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script>
$(document).ready(function () {
    //resensi
    var r_cat = [
        '1 Bulan Terakhir',
        '2 Bulan Terakhir',
        '3 Bulan Terakhir',
        '4 Bulan Terakhir',
        '5 Bulan Terakhir',
        '>6 Bulan Terakhir'
    ];
    var r_data = [{
        name: 'Jumlah Toko Obat',
        data: [124, 242, 258, 386, 329, 295],
        color: '#7A9BEA'

    }, {
        name: 'Jumlah Rumah Sakit',
        data: [225, 278, 346, 398, 354, 321],
        color: '#25457E'
    }, {
        name: 'Jumlah Apotek',
        data: [152, 245, 271, 325, 254, 272],
        color: '#F4B400'
    }];
    columnchart('resensi', r_cat, r_data);

    var m_cat = [
        '0 - 150 Juta',
        '151 - 300 Juta',
        '300 - 500 Juta',
        '501 - 750 Juta',
        '751 - 1 Miliar',
        '> 1 Miliar'
    ];
    columnchart('monetary', m_cat, r_data);

    var f_cat = [
        '< 10 Kali',
        '11 - 20 Kali',
        '21 - 30 Kali',
        '31 - 50 Kali',
        '51 - 70 Kali',
        '> 70 Kali'
    ];
    columnchart('frequency', f_cat, r_data);

    var p_cat = [
        '< 5000',
        '5000 s.d 10000',
        '10000 s.d 20000',
        '20000 s.d 30000',
        '30000 s.d 50000',
        '> 50000'
    ];
    columnchart('peresapan', p_cat, r_data);

    /*var los_cat = [
        '<6 Bulan',
        '>6 Bulan - 12 Bulan',
        '>12 Bulan - 24 Bulan',
        '>24 Bulan'
    ];
    var los_data = [{
        name: 'Persentase Dokter Non KFA',
        data: [41, 20, 21, 12],
        color: '#7A9BEA'

    }, {
        name: 'Persentase Dokter Non KFA',
        data: [28, 30, 23, 19],
        color: '#25457E'
    }];
    barchart('los', los_cat, los_data);*/

    var top_non_cat = [
        'Dokter 1',
        'Dokter 2',
        'Dokter 3',
        'Dokter 4',
        'Dokter 5'
    ];
    var top_non_data = [{
        name: null,
        data: [88, 61, 75, 60, 50],
        color: '#7A9BEA'

    }];
    barchart('top-5-non', top_non_cat, top_non_data);

    var top_cat = [
        'Dokter 1',
        'Dokter 2',
        'Dokter 3',
        'Dokter 4',
        'Dokter 5'
    ];
    var top_data = [{
        data: [88, 61, 75, 60, 50],
        color: '#25457E'

    }];
    barchart('top-5', top_cat, top_data);

    barchart('top-5-modal', top_cat, top_data);

    maps();

    var data_participant_status = [
        {
            "status": "Total Obat",
            "value": '23.1',
            "color": "#7A9BEA"
        }, {
            "status": "Rumah Sakit",
            "value": '25.5',
            "color": "#25457E"
        }, {
            "status": "Apotek",
            "value": '51.4',
            "color": "#F4B400"
        }
    ];
    animatedDonutWithLegend("#ts", 200, data_participant_status);
});

function columnchart(container, categories, data)
{
    Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                pointWidth: 15,
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            //alert('Category: ' + this.category + ', value: ' + this.y);
                            $('#frmbox-title').html(this.category);
                            $('#frmbox').modal({keyboard: false, backdrop: 'static'});
                        }
                    }
                }
            }
        },
        series: data
    });
}

function barchart(container, categories, data)
{
    Highcharts.chart(container, {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            bar: {
                pointPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                pointWidth: 15
            }
        },
        series: data
    });
}

function maps()
{
    var data = [
        ['id-3700', 0],
        ['id-ac', 1],
        ['id-ki', 2],
        ['id-jt', 3],
        ['id-be', 4],
        ['id-bt', 5],
        ['id-kb', 6],
        ['id-bb', 7],
        ['id-ba', 8],
        ['id-ji', 9],
        ['id-ks', 10],
        ['id-nt', 11],
        ['id-se', 12],
        ['id-kr', 13],
        ['id-ib', 14],
        ['id-su', 15],
        ['id-ri', 16],
        ['id-sw', 17],
        ['id-la', 18],
        ['id-sb', 19],
        ['id-ma', 20],
        ['id-nb', 21],
        ['id-sg', 22],
        ['id-st', 23],
        ['id-pa', 24],
        ['id-jr', 25],
        ['id-1024', 26],
        ['id-jk', 27],
        ['id-go', 28],
        ['id-yo', 29],
        ['id-kt', 30],
        ['id-sl', 31],
        ['id-sr', 32],
        ['id-ja', 33]
    ];

// Create the chart
    Highcharts.mapChart('maps', {
        chart: {
            map: 'countries/id/id-all'
        },

        title: {
            text: 'Consumer analytic'
        },

        subtitle: {
            text: 'Segmentasi konsumen berdasar nilai rata-rata peresepan dan per daerah'
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series: [{
            data: data,
            name: 'Random data',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.x}'
            }
        }]
    });
}

function animatedDonutWithLegend(element, size, data)
{
    // Main variables
    var d3Container = d3.select(element),
        distance = 2, // reserve 2px space for mouseover arc moving
        radius = (size/2) - distance,
        sum = d3.sum(data, function(d) { return d.value; });


    // Create chart
    // ------------------------------

    // Add svg element
    var container = d3Container.append("svg");

    // Add SVG group
    var svg = container
        .attr("width", size)
        .attr("height", size)
        .append("g")
        .attr("transform", "translate(" + (size / 2) + "," + (size / 2) + ")");


    // Construct chart layout
    // ------------------------------

    // Pie
    var pie = d3.layout.pie()
        .sort(null)
        .startAngle(Math.PI)
        .endAngle(3 * Math.PI)
        .value(function (d) {
            return d.value;
        });

    // Arc
    var arc = d3.svg.arc()
        .outerRadius(radius)
        .innerRadius(radius / 1.5);


    //
    // Append chart elements
    //

    // Group chart elements
    var arcGroup = svg.selectAll(".d3-arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "d3-arc")
        .style({
            'stroke': '#fff',
            'stroke-width': 2,
            'cursor': 'pointer'
        });

    // Append path
    var arcPath = arcGroup
        .append("path")
        .style("fill", function (d) {
            return d.data.color;
        });


    // Add interactions
    arcPath
        .on('mouseover', function (d, i) {

            // Transition on mouseover
            d3.select(this)
                .transition()
                .duration(500)
                .ease('elastic')
                .attr('transform', function (d) {
                    d.midAngle = ((d.endAngle - d.startAngle) / 2) + d.startAngle;
                    var x = Math.sin(d.midAngle) * distance;
                    var y = -Math.cos(d.midAngle) * distance;
                    return 'translate(' + x + ',' + y + ')';
                });

            // Animate legend
            $(element + ' [data-slice]').css({
                'opacity': 0.3,
                'transition': 'all ease-in-out 0.15s'
            });
            $(element + ' [data-slice=' + i + ']').css({'opacity': 1});
        })
        .on('mouseout', function (d, i) {

            // Mouseout transition
            d3.select(this)
                .transition()
                .duration(500)
                .ease('bounce')
                .attr('transform', 'translate(0,0)');

            // Revert legend animation
            $(element + ' [data-slice]').css('opacity', 1);
        });

    // Animate chart on load
    arcPath
        .transition()
        .delay(function(d, i) {
            return i * 500;
        })
        .duration(500)
        .attrTween("d", function(d) {
            var interpolate = d3.interpolate(d.startAngle,d.endAngle);
            return function(t) {
                d.endAngle = interpolate(t);
                return arc(d);
            };
        });


    //
    // Append counter
    //

    // Append text
    svg
        .append('text')
        .attr('text-anchor', 'middle')
        .attr('dy', 6)
        .style({
            'font-size': '17px',
            'font-weight': 500
        });

    // Animate text
    svg.select('text')
        .transition()
        .duration(1500)
        .tween("text", function(d) {
            var i = d3.interpolate(this.textContent, sum);
            return function(t) {
                this.textContent = d3.format(",d")(Math.round(i(t)));
            };
        });


    //
    // Append legend
    //

    // Add element
    var legend = d3.select(element)
        .append('ul')
        .attr('class', 'chart-widget-legend')
        .selectAll('li').data(pie(data))
        .enter().append('li')
        .attr('data-slice', function(d, i) {
            return i;
        })
        .attr('style', function(d, i) {
            return 'border-bottom: 2px solid ' + d.data.color;
        })
        .text(function(d, i) {
            return d.data.status + ': ';
        });

    // Add value
    legend.append('span')
        .text(function(d, i) {
            return d.data.value;
        });
}
</script>    