<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>

<div class="white-box">
    <h5 class="box-title text-center">
        Nasional &mdash; YTD 2019
    </h5>
    <div id="spline" style="height: 400px;"></div>
</div>
<div class="white-box">
    <h3 class="box-title">
        Klik Salah Satu Daerah Untuk Melihat Data Detail dan Rekomendasi
    </h3>
    <div id="maps" style="width: 100%; height: 400px;"></div>
</div>


<!-- Rekomendasi Tindakan Selanjutnya -->
<div class="modal fade bs-example-modal-lg" id="box" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-borderless" width="100%">
                    <tr>
                        <td width="35%">Total Penjualan</td>
                        <td width="5%">:</td>
                        <td width="60%" id="total_sales"></td>
                    </tr>
                    <tr>
                        <td width="35%">Total Bonus & PTD 5%</td>
                        <td width="5%">:</td>
                        <td width="60%" id="total_bonus"></td>
                    </tr>
                    <tr>
                        <td width="35%">Pangsa Pasar</td>
                        <td width="5%">:</td>
                        <td width="60%" id="market_share"></td>
                    </tr>
                    <tr>
                        <td width="35%">Rasio Bonus & PTD 5% / Penjualan per Propinsi</td>
                        <td width="5%">:</td>
                        <td width="60%" id="rasio_bonus"></td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?= base_url() ?>assets/highmaps/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/modules/export-data.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>

<script src="<?= base_url() ?>assets/highmaps/code/grouped-categories.js"></script>

<script>

var baseurl = '<?=base_url()?>index.php/crm';
$(document).ready(function () {
    maps();
    spline();
});

function maps()
{
    var data = <?=$map ?>;

    // Create the chart
    Highcharts.mapChart('maps', {
        chart: {
            map: 'countries/id/id-all'
        },

        title: {
            text: null
        },

        subtitle: {
            text: null,
            style: {
                color: '#2c5ca9'
            }
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series: [{
            data: data,
            name: 'Provinsi',
            cursor: 'pointer',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.value}'
            },
            point:{
                events:{
                    click: function() {
                        area(this['hc-key'],this.name); 
                    }
                }
            }
        }]
    });
}


function area(id,name) {
    $.ajax({
		url:baseurl+'/getDetailMap',
		global:false,
		async:true,
		dataType:'json',
		type:'post',
		data: ({ id : id }),
		success: function(e) { 
            $("#box #total_sales").html(number_format(e.total_sales, 2, ',', '.'));
            $("#box #total_bonus").html(number_format(e.total_bonus, 2, ',', '.'));
            $("#box #market_share").html(e.market_share);
            $("#box #rasio_bonus").html(e.rasio_bonus); 
            $("#box .modal-title").html(name); 
            $('#box').modal({keyboard: false, backdrop: 'static'});
		},
		error : function() {
			// alert('<?= $this->config->item('alert_error') ?>');	 
		},
		beforeSend : function() {
			$('#loading-img').show();
		},
		complete : function() {
			$('#loading-img').hide();
		}
	});	
}


function number_format(number, decimals, dec_point, thousands_point) { 
	if (number == null || !isFinite(number)) {
			throw new TypeError("number is not valid");
	} 
	if (!decimals) {
			var len = number.toString().split('.').length;
			decimals = len > 1 ? len : 0;
	} 
	if (!dec_point) {
			dec_point = '.';
	} 
	if (!thousands_point) {
			thousands_point = ',';
	} 
	number = parseFloat(number).toFixed(decimals); 
	number = number.replace(".", dec_point); 
	var splitNum = number.split(dec_point);
	splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
	number = splitNum.join(dec_point); 
	return number;
}

function spline()
{
    Highcharts.chart('spline', {
        chart: {
            type: 'spline'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [{
                name: 'Q1',
                categories: ['Instansi<br>Pemerintah', 'Swasta', 'Inter<br>Company']
            }, {
                name: 'Q2',
                categories: ['Instansi<br>Pemerintah', 'Swasta', 'Inter<br>Company']
            }, {
                name: 'Q3',
                categories: ['Instansi<br>Pemerintah', 'Swasta', 'Inter<br>Company']
            }, {
                name: 'Q4',
                categories: ['Instansi<br>Pemerintah', 'Swasta', 'Inter<br>Company']
            }],
            plotBands: [{
                color: '#efefef',
                from: -1,
                to: 9,
                label: {
                    text: 'Data Real',
                    style: {
                        color: '#606060'
                    }
                }
            }, {
                color: '#CFD1D1',
                from: 9,
                to: 12,
                label: {
                    text: 'Data Prediksi',
                    style: {
                        color: '#606060'
                    }
                }
            }]/**/
        },
        yAxis: {
            title: {
                text: 'Sales Profit'
            },
            /*plotLines: [{
                value: 250000000,
                color: 'red',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: 'Bottom Sales'
                }
            }, {
                value: 325000000,
                color: 'green',
                dashStyle: 'shortdash',
                width: 2,
                label: {
                    text: ''
                }
            }]*/
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                    /*formatter: function () {
                        if(this.y == 1 || this.y == 2)
                            return 'Reguler';
                        else if(this.y == 3 || this.y == 4)
                            return 'Silver';
                        else if(this.y == 5 || this.y == 6 || this.y == 7)
                            return 'Gold';
                        else if(this.y == 8)
                            return 'Platinum';
                    }*/
                },
                enableMouseTracking: false
            }
        },
        series: [
            {
                name: 'Total sales',
                data: <?=$market['total']?>
            },
            {
                name: 'Market Share',
                data: <?=$market['share']?>
            },
            {
                name: 'Rasio Bonus/Sales',
                data: <?=$market['bonus']?>
            }
        ]
    });
}
</script>