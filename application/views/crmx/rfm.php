 
<div class="col-md-12">
    <h4 class="page-title"><?= $title ?></h4>
</div>
</div>


<style type="text/css">
    .last_update {
        position: absolute;


        font-family: Roboto;
        font-style: italic;
        font-weight: normal;
        font-size: 12px;

        color: #4F4F4F;
    }
    .form-horizontal .form-group {
        margin-bottom: 0px;
    }
    .highcharts-button {
        display: none;
    }
</style>

<div class="row mb-2" style="margin-bottom: 10px">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="box-title text-center">Apotek</h3>
                <div class="pull-left"><div id="pie-apotek"></div></div>
                <div class="pull-right" style="margin-top: 15px">
                    <table>
                        <tr>
                            <td width="15%" style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#6060A4; margin-right:5px"></td>
                            <td width="85%" style="padding-bottom: 5px">Platinum</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#FFDA83; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Gold</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#D9D9D9; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Silver</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-circle-o" style="color:#54D8FF; margin-right:5px"></i></td>
                            <td>Reguler</td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-footer text-center" style="padding: 10px 25px;">
                <a href="javascript:title('Apotek','apotek')">Lihat Detail</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="box-title text-center">Dokter</h3>
                <div class="pull-left"><div id="pie-dokter"></div></div>
                <div class="pull-right" style="margin-top: 15px">
                    <table>
                        <tr>
                            <td width="15%" style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#6060A4; margin-right:5px"></td>
                            <td width="85%" style="padding-bottom: 5px">Platinum</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#FFDA83; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Gold</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#D9D9D9; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Silver</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-circle-o" style="color:#54D8FF; margin-right:5px"></i></td>
                            <td>Reguler</td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-footer text-center" style="padding: 10px 25px;">
                <a href="javascript:title('Dokter','dokter')">Lihat Detail</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="box-title text-center">Rumah Sakit</h3>
                <div class="pull-left"><div id="pie-rs"></div></div>
                <div class="pull-right" style="margin-top: 15px">
                    <table>
                        <tr>
                            <td width="15%" style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#6060A4; margin-right:5px"></td>
                            <td width="60%" style="padding-bottom: 5px">Platinum</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#FFDA83; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Gold</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 5px"><i class="fa fa-circle-o" style="color:#D9D9D9; margin-right:5px"></i></td>
                            <td style="padding-bottom: 5px">Silver</td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-circle-o" style="color:#54D8FF; margin-right:5px"></i></td>
                            <td>Reguler</td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-footer text-center" style="padding: 10px 25px;">
                <a href="javascript:title('Rumah Sakit','rumah_sakit')">Lihat Detail</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title"><strong><span class="rtitle">Apotek</span></strong> : Segmentasi by Frekuensi-Monetary</h3>
            <p align="center">
                <span><i class="fa fa-circle-o" style="color:#6060A4; margin-right:5px"></i> Platinum</span>
                <span><i class="fa fa-circle-o" style="color:#FFDA83; margin-right:5px"></i> Gold</span>
                <span><i class="fa fa-circle-o" style="color:#D9D9D9; margin-right:5px"></i> Silver</span>
                <span><i class="fa fa-circle-o" style="color:#54D8FF; margin-right:5px"></i> Reguler</span>
            </p>
            <div id="fm" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title"><strong><span class="rtitle">Apotek</span></strong>: Segmentasi by Resensi-Monetary</h3>
            <p align="center">
                <span><i class="fa fa-circle-o" style="color:#6060A4; margin-right:5px"></i> Platinum</span>
                <span><i class="fa fa-circle-o" style="color:#FFDA83; margin-right:5px"></i> Gold</span>
                <span><i class="fa fa-circle-o" style="color:#D9D9D9; margin-right:5px"></i> Silver</span>
                <span><i class="fa fa-circle-o" style="color:#54D8FF; margin-right:5px"></i> Reguler</span>
            </p>
            <div id="rm" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="frmbox" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="frmbox-title"></h4>
            </div>
            <div class="modal-body">
                <p><strong>Rekomendasi Keuntungan yang akan ditawarkan kepada pelanggan.</strong></p>
                <ul id="uplatinum" class="u" style="display: none">
                    <li>Fleksibel TOP & Credit Limit;</li>
                    <li>Program edukasi;</li>
                    <li>gifts;</li>
                    <li>medical representative;</li>
                    <li>iklan;</li>
                    <li>Contoh obat;</li>
                    <li>Advertisement in hospital journal;</li>
                    <li>Seminar support/sponsoring;</li>
                </ul>
                <ul id="ugold" class="u" style="display: none">
                    <li>Credit Limit;</li>
                    <li>Program edukasi;</li>
                    <li>gifts;</li>
                    <li>medical representative;</li>
                    <li>iklan;</li>
                    <li>Contoh obat;</li>
                    <li>Seminar support;</li>
                </ul>
                <ul id="usilver" class="u" style="display: none">
                    <li>Credit Limit;</li>
                    <li>Program edukasi;</li>
                    <li>gifts;</li>
                    <li>medical representative;</li>
                    <li>iklan;</li>
                    <li>Contoh obat;</li>
                </ul>
                <ul id="ureguler" class="u" style="display: none">
                    <li>medical representative;</li>
                    <li>iklan;</li>
                    <li>Contoh obat;</li>
                </ul>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- <script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
<script src="<?= base_url() ?>assets/highcharts/code/modules/export-data.js"></script> -->

<script src="<?= base_url() ?>assets/highmaps/code/modules/map.js"></script>
<script src="<?= base_url() ?>assets/highmaps/code/mapdata/countries/id/id-all.js"></script>


<!-- <script src="http://demo.interface.club/limitless/demo/bs4/Template/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
<script src="http://demo.interface.club/limitless/demo/bs4/Template/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script> -->

<script>
$(document).ready(function () {

    var data_rs = [
        {
            "status": "Platinum",
            "icon": "<i class='badge badge-mark border-blue-300 mr-2'></i>",
            "value": '<?=$rs->platinum ?>',
            "color": "#6060A4"
        }, {
            "status": "Gold",
            "icon": "<i class='badge badge-mark border-success-300 mr-2'></i>",
            "value": '<?=$rs->gold ?>',
            "color": "#FFDA83"
        }, {
            "status": "Silver",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$rs->silver ?>',
            "color": "#D9D9D9"
        }, {
            "status": "Reguler",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$rs->reguler ?>',
            "color": "#54D8FF"
        }
    ];
    var data_dokter = [
        {
            "status": "Platinum",
            "icon": "<i class='badge badge-mark border-blue-300 mr-2'></i>",
            "value": '<?=$dokter->platinum ?>',
            "color": "#6060A4"
        }, {
            "status": "Gold",
            "icon": "<i class='badge badge-mark border-success-300 mr-2'></i>",
            "value": '<?=$dokter->gold ?>',
            "color": "#FFDA83"
        }, {
            "status": "Silver",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$dokter->silver ?>',
            "color": "#D9D9D9"
        }, {
            "status": "Reguler",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$dokter->reguler ?>',
            "color": "#54D8FF"
        }
    ];
    var data_apotek = [
        {
            "status": "Platinum",
            "icon": "<i class='badge badge-mark border-blue-300 mr-2'></i>",
            "value": '<?=$apotek->platinum ?>',
            "color": "#6060A4"
        }, {
            "status": "Gold",
            "icon": "<i class='badge badge-mark border-success-300 mr-2'></i>",
            "value": '<?=$apotek->gold ?>',
            "color": "#FFDA83"
        }, {
            "status": "Silver",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$apotek->silver ?>',
            "color": "#D9D9D9"
        }, {
            "status": "Reguler",
            "icon": "<i class='badge badge-mark border-danger-300 mr-2'></i>",
            "value": '<?=$apotek->reguler ?>',
            "color": "#54D8FF"
        }
    ];

    var fm = <?= json_encode($type['apotek']['frekuensi']); ?>;
    var rm = <?= json_encode($type['apotek']['resensi']); ?>;
    //pie('pie-apotek');
    pied3('#pie-apotek', 150, data_apotek);
    pied3('#pie-dokter', 150, data_dokter);
    pied3('#pie-rs', 150, data_rs);
    doubley_fm('Frequecy', fm);
    doubley_rm('Resensi', rm);
    //scq('rm');
    //scq('fm');
});

function title(t,data) {
    $('.rtitle').html(t);
    
    if(data=='rumah_sakit'){ 
        var fm = <?= json_encode($type['rumah_sakit']['frekuensi']); ?>;
        var rm = <?= json_encode($type['rumah_sakit']['resensi']); ?>;
    }
    else if(data=='dokter'){ 
        var fm = <?= json_encode($type['dokter']['frekuensi']); ?>;
        var rm = <?= json_encode($type['dokter']['resensi']); ?>;
    }
    else if(data=='apotek'){ 
        var fm = <?= json_encode($type['apotek']['frekuensi']); ?>;
        var rm = <?= json_encode($type['apotek']['resensi']); ?>;
    }
    
    doubley_fm('Frequecy', fm);
    doubley_rm('Resensi', rm);
}

function box() {
    var title = 'platinum';

    $('.u').hide();
    $('#u'+title).show();

    $('#frmbox-title').html('Pelanggan '+title);
    $('#frmbox').modal({keyboard: false, backdrop: 'static'});
}

function doubley_fm(titley, data)
{
    Highcharts.chart('fm', {
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Monetary'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            gridLineWidth: 1
        },
        yAxis: {
            title: {
                text: titley
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    },
                    cursor: 'pointer'
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '<b>{point.name}</b><br>Sales: {point.x}, Frekuensi: {point.y}'
                },
                cursor: 'pointer',
                events: {
                    click: function (event) {
                        box();
                    }
                }

            }
        },
        series: [{
            showInLegend: false,
            color: '#0B4168',
            data: data
        }]
    }, function(chart) { // on complete

        var width = chart.plotBox.width / 2.0;
        var height = chart.plotBox.height / 2.0 + 1;

        chart.renderer.rect(chart.plotBox.x,chart.plotBox.y, width, height, 1)
            .on('click', function() {
                alert('yo');
            })
            .attr({
                fill: '#F0F0F7',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x + width,
            chart.plotBox.y, width, height, 1)
            .attr({
                fill: '#8293C9',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x,
            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#B9E2FA',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x + width,
            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#FFDA83',
                zIndex: 0
            })
            .add();

    });
} 

function doubley_rm(titley, data)
{
    Highcharts.chart('rm', {
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Monetary'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            gridLineWidth: 1
        },
        yAxis: {
            title: {
                text: titley
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    },
                    cursor: 'pointer'
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '<b>{point.name}</b><br>Sales: {point.x}, Resensi: {point.y}'
                },
                cursor: 'pointer',
                events: {
                    click: function (event) {
                        box();
                    }
                }

            }
        },
        series: [{
            showInLegend: false,
            color: '#0B4168',
            data: data
        }]
    }, function(chart) { // on complete

        var width = chart.plotBox.width / 2.0;
        var height = chart.plotBox.height / 2.0 + 1;

        chart.renderer.rect(chart.plotBox.x,chart.plotBox.y, width, height, 1)
            .on('click', function() {
                alert('yo');
            })
            .attr({
                fill: '#F0F0F7',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x + width,
            chart.plotBox.y, width, height, 1)
            .attr({
                fill: '#8293C9',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x,
            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#B9E2FA',
                zIndex: 0
            })
            .add();

        chart.renderer.rect(chart.plotBox.x + width,
            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#FFDA83',
                zIndex: 0
            })
            .add();

    });
}

function pied3(element, size, data) {
    if (typeof d3 == 'undefined') {
        console.warn('Warning - d3.min.js is not loaded.');
        return;
    }

    // Initialize chart only if element exsists in the DOM
    if(element) {


        // Basic setup
        // ------------------------------

        // Add data set

        // Main variables
        var d3Container = d3.select(element),
            distance = 2, // reserve 2px space for mouseover arc moving
            radius = (size/2) - distance,
            sum = d3.sum(data, function(d) { return d.value; });


        // Tooltip
        // ------------------------------

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .direction('e')
            .html(function (d) {
                return "<ul class='list-unstyled mb-1'>" +
                    "<li>" + "<div class='font-size-base my-1'>" + d.data.icon + d.data.status + "</div>" + "</li>" +
                    "<li>" + "Total: &nbsp;" + "<span class='font-weight-semibold float-right'>" + d.value + "</span>" + "</li>" +
                    "<li>" + "Share: &nbsp;" + "<span class='font-weight-semibold float-right'>" + (100 / (sum / d.value)).toFixed(2) + "%" + "</span>" + "</li>" +
                    "</ul>";
            });


        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append("svg").call(tip);

        // Add SVG group
        var svg = container
            .attr("width", size)
            .attr("height", size)
            .append("g")
            .attr("transform", "translate(" + (size / 2) + "," + (size / 2) + ")");


        // Construct chart layout
        // ------------------------------

        // Pie
        var pie = d3.layout.pie()
            .sort(null)
            .startAngle(Math.PI)
            .endAngle(3 * Math.PI)
            .value(function (d) {
                return d.value;
            });

        // Arc
        var arc = d3.svg.arc()
            .outerRadius(radius)
            .innerRadius(radius / 1.35);


        //
        // Append chart elements
        //

        // Group chart elements
        var arcGroup = svg.selectAll(".d3-arc")
            .data(pie(data))
            .enter()
            .append("g")
            .attr("class", "d3-arc")
            .style({
                'stroke': '#fff',
                'stroke-width': 2,
                'cursor': 'pointer'
            });

        // Append path
        var arcPath = arcGroup
            .append("path")
            .style("fill", function (d) {
                return d.data.color;
            });


        //
        // Add interactions
        //

        // Mouse
        arcPath
            .on('mouseover', function(d, i) {

                // Transition on mouseover
                d3.select(this)
                    .transition()
                    .duration(500)
                    .ease('elastic')
                    .attr('transform', function (d) {
                        d.midAngle = ((d.endAngle - d.startAngle) / 2) + d.startAngle;
                        var x = Math.sin(d.midAngle) * distance;
                        var y = -Math.cos(d.midAngle) * distance;
                        return 'translate(' + x + ',' + y + ')';
                    });

                $(element + ' [data-slice]').css({
                    'opacity': 0.3,
                    'transition': 'all ease-in-out 0.15s'
                });
                $(element + ' [data-slice=' + i + ']').css({'opacity': 1});
            })
            .on('mouseout', function(d, i) {

                // Mouseout transition
                d3.select(this)
                    .transition()
                    .duration(500)
                    .ease('bounce')
                    .attr('transform', 'translate(0,0)');

                $(element + ' [data-slice]').css('opacity', 1);
            });

        // Animate chart on load
        arcPath
            .transition()
            .delay(function(d, i) {
                return i * 500;
            })
            .duration(500)
            .attrTween("d", function(d) {
                var interpolate = d3.interpolate(d.startAngle,d.endAngle);
                return function(t) {
                    d.endAngle = interpolate(t);
                    return arc(d);
                };
            });


        //
        // Add text
        //

        // Total
        svg.append('text')
            .attr('class', 'text-muted')
            .attr({
                'class': 'half-donut-total',
                'text-anchor': 'middle',
                'dy': -13
            })
            .style({
                'font-size': '12px',
                'fill': '#999'
            })
            .text('Total');

        // Count
        svg
            .append('text')
            .attr('class', 'half-donut-count')
            .attr('text-anchor', 'middle')
            .attr('dy', 14)
            .style({
                'font-size': '21px',
                'font-weight': 500
            });

        // Animate count
        svg.select('.half-donut-count')
            .transition()
            .duration(1500)
            .ease('linear')
            .tween("text", function(d) {
                var i = d3.interpolate(this.textContent, sum);

                return function(t) {
                    this.textContent = d3.format(",d")(Math.round(i(t)));
                };
            });


        //
        // Add legend
        //

        /*// Append list
        var legend = d3.select(element)
            .append('ul')
            .attr('class', 'chart-widget-legend')
            .selectAll('li')
            .data(pie(data))
            .enter()
            .append('li')
            .attr('data-slice', function(d, i) {
                return i;
            })
            .attr('style', function(d, i) {
                return 'border-bottom: solid 2px ' + d.data.color;
            })
            .text(function(d, i) {
                return d.data.status + ': ';
            });

        // Append text
        legend.append('span')
            .text(function(d, i) {
                return d.data.value;
            });*/
    }
};
</script>    