<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(

	//'dsn'	=> 'MariaNetwan',
	// old connection database 
	//  'hostname' => '172.16.100.106',
	// new connection database 
	 'hostname' => '172.16.100.123',
	 'username' => 'usr_kf',
	 'password' => 'u5rKF2019',
	 'database' => 'datamart_kf',

	/*'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'pass',
	'database' => 'datamart_kf',*/
	
	// 'hostname' => '45.32.127.32',
	// 'username' => 'usr_kf',
	// 'password' => 'u5rKF2018',
	// 'database' => 'datamart_kf',

	// 'hostname' => 'localhost',
	// 'username' => 'usr_kf',
	// 'password' => 'u5rKF2019',
	// 'database' => 'datamart_kf',

	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
