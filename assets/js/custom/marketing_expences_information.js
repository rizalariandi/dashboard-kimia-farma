var newRow = [];
var data_table
$(document).ready(function () {
    setDates();
    data_table = $('#datatablesModals').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": true,
        "ordering": true,
        "bFilter": true,
        "bLengthChange": false,
        "lengthMenu": [100],
        "dom": 'Bfrtip',
        fixedHeader: false,
        buttons: [
            'pageLength',
        ],
        "drawCallback": function (settings) {
            //alert( 'DataTables has redrawn the table' );

            jQuery("#table-data-filter").show();
            jQuery(".stage").hide();

            //  drawmycanvasbar();
        },
        dom: 'Bfrtip',
        buttons: [
            'excel'
        ],
        "columns": [{
                data: 'enter_document',
                className: 'text-center',
            },
            {
                data: 'cost_center',
                className: 'text-center',
            },
            {
                data: 'document_number',
                className: 'text-center',
            },
            {
                data: 'line_item',
                className: 'text-center',
            },
            {
                data: 'expense_ty',
                className: 'text-right',
                render: $.fn.dataTable.render.number( '.', '.' )
            },
            {
                data: 'text',
                className: 'text-center',
            }
        ]
    });
    
    choosetableperiod($('#head_filter').val());
})

var path = jQuery("#pathData").val();
console.log(path)
$('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
});

$('#period_start').datepicker({
    dateFormat: 'dd/mm/yy',

});
$('#period_end').datepicker({
    dateFormat: 'dd/mm/yy',

});

function setDates(){
    const curdate = new Date();
    const currMonth = curdate.getMonth();
    const currYear = curdate.getFullYear();
    const endDate = new Date(currYear, currMonth, 1);
    const startDate = new Date(currYear, currMonth - 5, 1);

    $("#tanggal_faktur_end").datepicker("setDate", endDate);
    $("#tanggal_faktur_start").datepicker("setDate", startDate);
}

function generatefilename() {
    var bulan_awal = jQuery('.bulan_awal').find(':selected').text();
    var bulan_akhir = jQuery('.bulan_akhir').find(':selected').text();
    var tahun = jQuery('#tahun').find(':selected').text();
    var filterby = jQuery('#head_filter').find(':selected').text();

    return 'Marketing Expenses ' + bulan_awal + ' sd ' + bulan_akhir + ' Tahun ' + tahun + ' Filter by ' + filterby;
}
jQuery(".select2").select2();
jQuery(".filter-select2").select2({
    //dropdownCssClass: "font-filter-select2"
    placeholder: "(All)",
    allowClear: true
});
jQuery(".filter-multiple-select2").select2({
    width: 'resolve',
    placeholder: "(All)",
    allowClear: true
    //dropdownCssClass: "font-filter-select2"

});

function callMenu() {
    //jQuery('#sidebar-filtering').remove();
    /*jQuery(".menu-navigation").css("width","220px");
    //jQuery("#container-dom").addClass("col-md-11");
    jQuery("#page-wrapper").addClass("col-md-12");
    jQuery("#page-wrapper").css("width",'100%');
    jQuery("#container-dom").css("margin-left","10px");
    jQuery("#container-dom").css("width","82%");*/
    jQuery('#filtering-side').hide();
    /*jQuery('#table-name').removeClass("col-md-8");
    jQuery('#table-name').addClass("col-md-6");*/
    jQuery('#side-menu').fadeIn("slow");
}

function callFilter() {
    //alert();
    //jQuery('#sidebar-filtering').remove();
    /*jQuery(".menu-navigation").css("width","30%");
    jQuery("#container-dom").addClass("col-md-10");
    jQuery("#container-dom").css("margin-left","20%");*/
    jQuery('#side-menu').hide();
    /*jQuery('#table-name').removeClass("col-md-6");
    jQuery('#table-name').addClass("col-md-8");*/
    jQuery('#filtering-side').fadeIn("slow");
}

function choosetable(vals) {
    if ($('html, body').animate({
            scrollTop: $('body').offset().top
        }, 'slow')) {
        rightvar = [];
        formatnumber = [];

        if (jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#head_filter").val() == "") {

            swal("Pastikan Periode Tanggal dan Filter name terisi");
        } else {
            var startdate = jQuery("#tanggal_faktur_start").val();
            var enddate = jQuery("#tanggal_faktur_end").val()
            if (jQuery("#myTable #datatablesE").remove() && jQuery("#myTable #datatablesE_wrapper").remove()) {
                jQuery("#myTable").append("<table id='datatablesE' style='overflow:auto;' class='table table-striped table-bordered'  width='100%' ></table>");                
                if (vals == 'cost_center') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7);
                    formatnumber.push(1, 2, 3);
                    //console.log(rightvar);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'nama_biaya') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8);
                    formatnumber.push(2, 3, 4);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'gl_account_description') {
                    console.log('vals',vals);
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9);
                    formatnumber.push(3, 4, 5);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'gpm_pm_code') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                    formatnumber.push(4, 5, 6);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        "<th id='th-gpm_pm_code'><b>GPM/PM</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'rsm_shopper_code') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                    formatnumber.push(5, 6, 7);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        //"<th id='th-gpm_pm_code'><b>GPM/PM</b></th>"+
                        "<th id='th-rsm_shopper_code'><b>RSM/Shopper</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
                    formatnumber.push(7, 8, 9);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target'>Realisasi <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                }
                var buttonCommon = {
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                // Strip $ from salary column to make it numeric

                                //return jQuery.inArray(column,formatnumber)
                                return vals == 'cost_center' ? column == 1 || column == 2 || column == 3 ?
                                    data.replace(/[.]/g, '').replace(',', '.') : data :
                                    (vals == 'biaya' ?
                                        column == 2 || column == 3 || column == 4 ?
                                        data.replace(/[.]/g, '').replace(',', '.') : data :
                                        column == 3 || column == 4 || column == 5 ? data.replace(/[.]/g, '').replace(',', '.') : data)



                            }
                        }
                    }
                };
                var path = jQuery("#pathData").val();
                var tableses = $("#datatablesE").DataTable({
                    serverSide: true,
                    ordering: true,
                    lengthMenu: [
                        [-1],
                        ["All"]
                    ],
                    dom: 'Bfrtip',
                    "processing": true,
                    //"autoWidth": true,
                    // "pageLength": 10,
                    "bFilter": false,
                    //"bLengthChange": true,
                    //"sScrollX": '100%',
                    rowReorder: true,
                    // scrollY: 400,
                    // "scrollCollapse": true,
                    fixedHeader: false,
                    // fixedHeader: {
                    //     header: true,
                    //     footer: true
                    // },
                    "columnDefs": [{
                        className: "dt-body-right",
                        "targets": rightvar
                    }],
                    ajax: {
                        url: "" + path + "/getListPeriod",
                        data: function (data) {
                            data.f_search = jQuery("#form-filter").serialize(); //+jQuery("#head-filter").val();
                            data.f_head = jQuery("#form-header").serialize();
                        },
                        'beforeSend': function (request) {}
                    },
                    buttons: [
                        'pageLength',
                        $.extend(true, {}, buttonCommon, {
                            extend: 'excelHtml5',
                            filename: function () {
                                return generatefilename();
                            },
                            title: function () {
                                return generatefilename();
                            },
                        }),
                        $.extend(true, {}, buttonCommon, {
                            extend: 'csvHtml5',
                            filename: function () {
                                return generatefilename();
                            },
                            title: function () {
                                return generatefilename();
                            },
                        }),
                        'colvis'
                    ],

                    "drawCallback": function (settings) {
                        jQuery("#totaltext").text(jQuery("#grandtotal").text());
                    }

                });

            }
        }
    }




}

function getJsonFilter(url, data) {
    //console.log(url);

    return JSON.parse(jQuery.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        global: false,
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest')
            var interval = setInterval(function () {
                $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
            }, 1000);
            //swal("loading");

        },
        success: function (msg) {

        }
    }).responseText);
}

function selectedfilter(id_filter) {
    var branch_code = getJsonFilter(path + '/getfiltervalue', {
        id: id_filter
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_biaya(id_filter) {
    var lini = [];

    $("#cost_center_description :selected").each(function () {
        lini.push($(this).val());
    });
    var brand = getJsonFilter(path + '/getfiltervalue_biaya', {
        id: id_filter,
        param: JSON.stringify(lini)
    });
    domoption(brand, id_filter, id_filter);
}

function selectedfilter_uraian(id_filter) {
    var biaya = [];

    $("#nama_biaya :selected").each(function () {
        biaya.push($(this).val());
    });
    var brand = getJsonFilter(path + '/getfiltervalue_uraian', {
        id: id_filter,
        param: JSON.stringify(biaya)
    });
    domoption(brand, id_filter, id_filter);
}

function domoption(data, id, re) {
    jQuery("#" + id + " option").remove();

    ///jQuery("#"+id).append("<option value=''>(All)</option>")
    jQuery.each(data, function (key, val) {
        //console.log(key);
        if (re == 'cost_center_description') {
            jQuery("#" + id).append("<option value='" + val.cost_center_description + "'>" + val.cost_center_description + "(" + val.cost_center_description + ")</option>")
        }
        if (re == 'nama_biaya') {
            jQuery("#" + id).append("<option value='" + val.nama_biaya + "'>" + val.nama_biaya + "(" + val.nama_biaya + ")</option>")
        }
        if (re == 'gl_account_description') {
            jQuery("#" + id).append("<option value='" + val.gl_account_description + "'>" + val.gl_account_description + "(" + val.gl_account_description + ")</option>")
        }

    })
}

function viewUraian(gl_account_code, bulan_tahun, cost_center_code, header, lini, biaya) {
    console.log('we call it')
    $.ajax({
        url: "" + path + "/getDetailExpen",
        method: "GET",
        data: {
            gl_account_code: gl_account_code, //+jQuery("#head-filter").val();
            start: $("#tahun").val() + (parseInt($(".bulan_awal").val()) < 10 ? ('0' + $(".bulan_awal").val()) : $(".bulan_awal").val()),
            end: $("#tahun").val() + (parseInt($(".bulan_akhir").val()) < 10 ? ('0' + $(".bulan_akhir").val()) : $(".bulan_akhir").val()),
            cost_center_code: cost_center_code
        },
        'beforeSend': function (request) {
            //jQuery("#table-data-filter").hide();            
        },
        'complete': function (data) {
            $(".modal-title").text(lini + "/" + biaya + "/" + header);
            result = JSON.parse(data.responseText)
            newRow = result.data;
            console.log(newRow);
            data_table.rows.add(newRow);
            data_table.columns.adjust().draw();
        }
    }).done(function () {
        if (newRow.length !== 0) {
            data_table.clear().draw();
        } else {
            data_table.clear().draw();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        // if (jqXHR.status != 422)
        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
    })
}

function choosetableperiod(vals) {
    if (jQuery("#head_filter").val() === "gl_account_description") {
        if ($('html, body').animate({
                scrollTop: $('body').offset().top
            }, 'slow')) {
            rightvar = [];
            formatnumber = [];

            var startdate = jQuery("#tanggal_faktur_start").val();
            var enddate = jQuery("#tanggal_faktur_end").val()
            if (jQuery("#myTable #datatablesE").remove() && jQuery("#myTable #datatablesE_wrapper").remove()) {
                jQuery("#myTable").append("<table id='datatablesE' style='overflow:auto;' class='table table-striped table-bordered'  width='100%' ></table>");
                console.log(vals,'vals')
                if (vals == 'cost_center') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7);
                    formatnumber.push(1, 2, 3);
                    //console.log(rightvar);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'nama_biaya') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8);
                    formatnumber.push(2, 3, 4);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'gl_account_description') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7);
                    formatnumber.push(3, 4, 5);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'gpm_pm_code') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                    formatnumber.push(4, 5, 6);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        "<th id='th-gpm_pm_code'><b>GPM/PM</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else if (vals == 'rsm_shopper_code') {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                    formatnumber.push(5, 6, 7);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        //"<th id='th-gpm_pm_code'><b>GPM/PM</b></th>"+
                        "<th id='th-rsm_shopper_code'><b>RSM/Shopper</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                } else {
                    rightvar.push(1, 2, 3, 4, 5, 6, 7);
                    formatnumber.push(3, 4, 5);
                    jQuery("#datatablesE").append("<thead>" +
                        "<tr>" +
                        "<th id='th-grand'><b>Lini</b></th>" +
                        "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                        "<th id='th-uraian'><b>Uraian</b></th>" +
                        "<th id='th-qtytarget'>Budget <b></b></th>" +
                        "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                        "<th id='th-target' >Expense <b></b></th>" +
                        "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                        "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                        "<th id='th-disc'>Growth (%)</th>" +
                        "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                        "<th id='th-achievment'>Share Last Year (%)</th>" +
                        "</tr>" +
                        "</thead>" + "");
                }
                var buttonCommon = {
                    exportOptions: {
                        format: {
                            body: function (data, row, column, node) {
                                // Strip $ from salary column to make it numeric

                                //return jQuery.inArray(column,formatnumber)
                                return vals == 'cost_center' ? column == 1 || column == 2 || column == 3 ?
                                    data.replace(/[.]/g, '').replace(',', '.') : data :
                                    (vals == 'biaya' ?
                                        column == 2 || column == 3 || column == 4 ?
                                        data.replace(/[.]/g, '').replace(',', '.') : data :
                                        column == 3 || column == 4 || column == 5 ? data.replace(/[.]/g, '').replace(',', '.') : data)



                            }
                        }
                    }
                }; 
                ajaxchart_marketing()              

                var tableses = $("#datatablesE").DataTable({
                    serverSide: true,
                    lengthMenu: [
                        [-1, 10],
                        ["All", 10]
                    ],
                    // pageLength: -1,
                    dom: 'Bfrtip',
                    "processing": true,
                    //"autoWidth": true,
                    // "pageLength": 10,
                    "bFilter": false,
                    //"bLengthChange": true,
                    //"sScrollX": '100%',
                    rowReorder: true,
                    // scrollY: 400,
                    // "scrollCollapse": true,
                    fixedHeader: false,
                    // fixedHeader: {
                    //     header: true,
                    //     footer: true
                    // },
                    "columns": [{
                            data: 0
                            // title: 'Vendor",                   
                            // className: 'text-left',
                            // render: $.fn.dataTable.render.number(",", ".", 0)
                        },
                        {
                            data: 1,
                            // title: 'Vendor",                   
                            className: 'text-center',
                            // render: $.fn.dataTable.render.number(",", ".", 0)
                        },
                        {
                            // "mRender": function (data, type, row) {
                            data: 2,
                            // >
                            // return '<a data-toggle="modal" class="pointer" data-target="#myModal" onclick="viewUraian(' + row[10] + ',' + row[11] + ',' + row[12] + ',' + "'" + row[2] + "'" + ',' + "'" + row[0] + "'" + ',' + "'" + row[1] + "'" + ')">' + row[2] + '</a>'
                            className: "enabled",
                            // render: $.fn.dataTable.render.number(",", ".", 0)
                            // }
                        },
                        {
                            data: 3,
                            className: 'text-right',
                        },
                        {
                            data: 4,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        },
                        {
                            data: 5,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }, {
                            data: 6,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }, {
                            data: 7,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }, {
                            data: 8,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }, {
                            data: 9,
                            className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }, {
                            data: 10,
                            // className: 'text-right',
                            // render: $.fn.dataTable.render.number(",", ".", 0)

                        }
                    ],
                    ajax: {
                        url: "" + path + "/getListPeriod",
                        data: function (data) {
                            data.bulan_awal = jQuery('.bulan_awal').find(':selected').val();
                            data.bulan_akhir = jQuery('.bulan_akhir').find(':selected').val();
                            data.tahun = jQuery('#tahun').find(':selected').text();

                            data.f_search = jQuery("#period-filter").serialize(); //+jQuery("#head-filter").val();
                            data.f_head = jQuery("#form-header").serialize();

                        },
                        'beforeSend': function (request) {}
                    },                    
                    buttons: [
                        'pageLength',
                        $.extend(true, {}, buttonCommon, {
                            extend: 'excelHtml5',
                            filename: function () {
                                return generatefilename();
                            },
                            title: function () {
                                return generatefilename();
                            },
                        }),
                        $.extend(true, {}, buttonCommon, {
                            extend: 'csvHtml5',
                            filename: function () {
                                return generatefilename();
                            },
                            title: function () {
                                return generatefilename();
                            },
                        }),
                        'colvis'


                    ],

                    "drawCallback": function (settings) {
                        jQuery("#totaltext").text(jQuery("#grandtotal").text());
                    }

                });

                $('#datatablesE tbody').on('click', 'tr', function (x) {
                    if (x.target.classList[0] === 'enabled') {
                        $("#myModalex").show();
                        row = tableses.row(this).data()
                        viewUraian(row[11], row[12], row[13], row[2], row[0], row[1])
                        // console.log(tableses.row(this).data());
                        // data_select.material_brand = tableses.row(this).data().material_brand;
                        // running(false)
                    }
                });

            }

        }
    } else {
        ajaxchart_marketing()
        choosetableperiodOld(vals);
    }
}

function ajaxchart_marketing() {
    var path = jQuery("#pathData").val();
    $.ajax({
        url: "" + path + "/getListChart",
        type: 'GET',
        data :{
            bulan_awal : jQuery('.bulan_awal').find(':selected').val(),
            bulan_akhir : jQuery('.bulan_akhir').find(':selected').val(),
            tahun : jQuery('#tahun').find(':selected').text(),

            f_search : jQuery("#period-filter").serialize(), //+jQuery("#head-filter").val();
            f_head : jQuery("#form-header").serialize(),
        },
        beforeSend: function() {
          // loading();          
        },
        success: function(data) {
        var x = []
        var expenses = []
        var realisasi = []
        var ratio = []
        data =JSON.parse(data);
        data.forEach(function (element){
        x.push(element.bulan_name)
        expenses.push(parseFloat(element.expenses || 0))
        realisasi.push(parseFloat(element.realisasi || 0))
        ratio.push(parseFloat(element.ratio || 0))
        });
         chartmarketing(x,expenses,realisasi,ratio)
            
        //   $(".loading_layanan").hide();
        //   data = JSON.parse(data);
        // if (data.length) {
        //   data.forEach( function (element){ 
        //     $('#layanan_name')[0].sumo.add(element.layanan_name)
        //   });
          
        // }
        }
      }).done(function(data) {        
        // $('#layanan_name')[0].sumo.selectAll();
      }).fail(function(jqXHR, textStatus, errorThrown) {
        // swal.close()
      });
}

function chartmarketing(x,expenses,realisasi,ratio){
    console.log(x,expenses,realisasi,ratio)
    Highcharts.chart('chart_expen', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: x,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis    
        
            title: {
                text: 'Value (Rp)',            
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                formatter: function(){
                    if (this.value>=1000000000000){
                    return "Rp"+this.value/1000000000000 + " Triliun"
                    } else if (this.value>=1000000000){
                    return "Rp"+this.value/1000000000 + " Miliar"
                    } else if (this.value>=1000000){
                    return "Rp"+this.value/1000000 + " Juta"
                    
                    }

                
     
                },

                style: {
                    color: Highcharts.getOptions().colors[0]
                }
                
            }
        }, { // Secondary yAxis
        labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {        		
                text: 'Rasio',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
                shared: true,
                formatter: function () {
                    var txt = '<strong>'+this.x+'</strong><br>';
                    $.each(this.points, function(i,p) {                
                        if(p.series.name == 'Realisasi Sales')
                            // var y = parseInt(p.y) * parseInt(100);
                            var y = parseInt(p.y);

                        else
                            var y = p.y;
    
                        y = y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
                        if(p.series.name == 'Rasio Realisasi Expense')
                            y = y+'%';
                        else
                            y = 'Rp. '+y;
    
                        txt += '<span style="color:'+p.color+'">\u25CF</span> '+p.series.name+': <b>'+y+'</b><br/>';
                    });
                    return txt;
                }
        },
        legend: {                
        },
        series: [
        {
            type: 'column',
            name: 'Marketing Expense',
            yAxis: 0,
            data:expenses,
              tooltip: {
                valueSuffix: 'Rp. '
            }
        }, {
            type: 'column',
            name: 'Realisasi Sales',
            data: realisasi,
            
              tooltip: {
                valueSuffix: 'Rp. '
            }
        },
        {
            name: 'Rasio Realisasi Expense',
            type: 'spline',
            yAxis: 1,
            data: ratio,
            tooltip: {
                valueSuffix: ' %'
            }
        }]
    });
    // Highcharts.chart('chart_expen', {
    //     chart: {
    //         zoomType: 'xy'
    //     },
    //     title: {
    //         text: ''
    //     },
    //     subtitle: {
    //         text: ''
    //     },
    //     xAxis: [{
    //         categories: x,
    //         crosshair: true
    //     }],
    //     yAxis: [{ // Primary yAxis    
    //         labels: {
    //             format: '{value}',
    //             style: {
    //                 color: Highcharts.getOptions().colors[0]
    //             }
    //         },
    //         title: {        		
    //             text: 'Value (Rp)',
    //             style: {
    //                 color: Highcharts.getOptions().colors[0]
    //             }
    //         }    
    //     }, { // Secondary yAxis                    
    //         title: {
    //             text: 'Rasio',            
    //             style: {
    //                 color: Highcharts.getOptions().colors[1]
    //             }
    //         },
    //         labels: {
    //             format: '{value}',
    //             style: {
    //                 color: Highcharts.getOptions().colors[1]
    //             }
    //         },
    //         opposite: true   
    //     }],
    //     tooltip: {
    //             shared: true,
    //             formatter: function () {
    //                 var txt = '<strong>'+this.x+'</strong><br>';
    //                 $.each(this.points, function(i,p) {                
    //                     if(p.series.name == 'Realisasi Sales')
    //                         var y = parseInt(p.y) * parseInt(100);
    //                     else
    //                         var y = p.y;
    
    //                     y = y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    //                     if(p.series.name == 'Rasio Realisasi Expense')
    //                         y = y+' %';
    //                     else
    //                         y = 'Rp. '+y;
    
    //                     txt += '<span style="color:'+p.color+'">\u25CF</span> '+p.series.name+': <b>'+y+'</b><br/>';
    //                 });
    //                 return txt;
    //             }
    //     },
    //     legend: {                
    //     },
    //     series: [
    //     {
    //         type: 'column',
    //         name: 'Marketing Expense',
    //         yAxis: 1,
    //         color:"#69d0ef",
    //         data: expenses,
    //           tooltip: {
    //             valueSuffix: 'Rp. '
    //         }
    //     }, {
    //         type: 'column',
    //         name: 'Realisasi Sales',
    //         color:"#ab82c4",
    //         data: realisasi,
    //           tooltip: {
    //             valueSuffix: 'Rp. '
    //         }
    //     },
    //     {
    //         name: 'Rasio Realisasi Expense',
    //         type: 'spline',
    //         color:"#db2347",
    //         data: ratio,
    //         tooltip: {
    //             valueSuffix: ' %'
    //         }
    //     }]
    // });    
}
function closeModal() {
    $("#myModalex").hide();
}


function choosetableperiodOld(vals) {
    if ($('html, body').animate({
            scrollTop: $('body').offset().top
        }, 'slow')) {
        rightvar = [];
        formatnumber = [];

        var startdate = jQuery("#tanggal_faktur_start").val();
        var enddate = jQuery("#tanggal_faktur_end").val()
        if (jQuery("#myTable #datatablesE").remove() && jQuery("#myTable #datatablesE_wrapper").remove()) {
            jQuery("#myTable").append("<table id='datatablesE' style='overflow:auto;' class='table table-striped table-bordered'  width='100%' ></table>");
            if (vals == 'cost_center') {
                rightvar.push(1, 2, 3, 4, 5, 6, 7);
                formatnumber.push(1, 2, 3);
                //console.log(rightvar);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            } else if (vals == 'nama_biaya') {
                rightvar.push(1, 2, 3, 4, 5, 6, 7, 8);
                formatnumber.push(2, 3, 4);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            } else if (vals == 'gl_account_description') {
                rightvar.push(1, 2, 3, 4, 5, 6, 7, 8);
                formatnumber.push(3, 4, 5);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                    "<th id='th-uraian'><b>Uraian</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            } else if (vals == 'gpm_pm_code') {
                rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                formatnumber.push(4, 5, 6);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                    "<th id='th-uraian'><b>Uraian</b></th>" +
                    "<th id='th-gpm_pm_code'><b>GPM/PM</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            } else if (vals == 'rsm_shopper_code') {
                rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
                formatnumber.push(5, 6, 7);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                    "<th id='th-uraian'><b>Uraian</b></th>" +
                    //"<th id='th-gpm_pm_code'><b>GPM/PM</b></th>"+
                    "<th id='th-rsm_shopper_code'><b>RSM/Shopper</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            } else {
                rightvar.push(1, 2, 3, 4, 5, 6, 7);
                formatnumber.push(3, 4, 5);
                jQuery("#datatablesE").append("<thead>" +
                    "<tr>" +
                    "<th id='th-grand'><b>Lini</b></th>" +
                    "<th id='th-biaya'><b>Beban Biaya</b></th>" +
                    "<th id='th-uraian'><b>Uraian</b></th>" +
                    "<th id='th-qtytarget'>Budget <b></b></th>" +
                    "<th id='th-sisa-expense'>Sisa Expense <b></b></th>" +
                    "<th id='th-target' >Expense <b></b></th>" +
                    "<th id='th-qtyterjual'>Last Year <b></b></th>" +
                    "<th id='th-realisasi'>Achiev (%) <b></b></th>" +
                    "<th id='th-disc'>Growth (%)</th>" +
                    "<th id='th-hjp'>Share Relisasi (%) <b></b></th>" +
                    "<th id='th-achievment'>Share Last Year (%)</th>" +
                    "</tr>" +
                    "</thead>" + "");
            }
            var buttonCommon = {
                exportOptions: {
                    format: {
                        body: function (data, row, column, node) {
                            // Strip $ from salary column to make it numeric

                            //return jQuery.inArray(column,formatnumber)
                            return vals == 'cost_center' ? column == 1 || column == 2 || column == 3 ?
                                data.replace(/[.]/g, '').replace(',', '.') : data :
                                (vals == 'biaya' ?
                                    column == 2 || column == 3 || column == 4 ?
                                    data.replace(/[.]/g, '').replace(',', '.') : data :
                                    column == 3 || column == 4 || column == 5 ? data.replace(/[.]/g, '').replace(',', '.') : data)



                        }
                    }
                }
            };
            var path = jQuery("#pathData").val();

            var tableses = $("#datatablesE").DataTable({
                serverSide: true,
                lengthMenu: [
                    [-1, 10],
                    ["All", 10]
                ],
                // pageLength: -1,
                dom: 'Bfrtip',
                "processing": true,
                //"autoWidth": true,
                // "pageLength": 10,
                "bFilter": false,
                //"bLengthChange": true,
                //"sScrollX": '100%',
                rowReorder: true,
                // scrollY: 400,
                // "scrollCollapse": true,
                // fixedHeader: {
                //     header: true,
                //     footer: true
                // },
                fixedHeader: false,
                "columnDefs": [{
                    className: "dt-body-right",
                    "targets": rightvar
                }],
                ajax: {
                    url: "" + path + "/getListPeriod",
                    data: function (data) {
                        data.bulan_awal = jQuery('.bulan_awal').find(':selected').val();
                        data.bulan_akhir = jQuery('.bulan_akhir').find(':selected').val();
                        data.tahun = jQuery('#tahun').find(':selected').text();

                        data.f_search = jQuery("#period-filter").serialize(); //+jQuery("#head-filter").val();
                        data.f_head = jQuery("#form-header").serialize();

                    },
                    'beforeSend': function (request) {}
                },
                buttons: [
                    'pageLength',
                    $.extend(true, {}, buttonCommon, {
                        extend: 'excelHtml5',
                        filename: function () {
                            return generatefilename();
                        },
                        title: function () {
                            return generatefilename();
                        },
                    }),
                    $.extend(true, {}, buttonCommon, {
                        extend: 'csvHtml5',
                        filename: function () {
                            return generatefilename();
                        },
                        title: function () {
                            return generatefilename();
                        },
                    }),
                    'colvis'


                ],

                "drawCallback": function (settings) {
                    jQuery("#totaltext").text(jQuery("#grandtotal").text());
                }

            });

        }

    }




}

function hideSidebar() {
    $("#filter-side").stop(true, true).hide(0);
    $("#content-side").removeClass("col-md-8");
    $("#content-side").addClass("col-md-12");
    jQuery("#valFilter").val(1);
    filteringform()
}

function filteringform() {
    var valFilter = jQuery("#valFilter").val();
    if (valFilter == 1) {
        $("#filter-side").stop(true, true).hide(0);
        $("#content-side").removeClass("col-md-8");
        $("#content-side").addClass("col-md-12");
        jQuery("#valFilter").val(0);
        jQuery("body #wrapper").removeClass("sidebar_minimize");
        //alert(1)
    } else {
        $("#filter-side").stop(true, true).show(0);
        $("#content-side").removeClass("col-md-12");
        $("#content-side").addClass("col-md-8");
        jQuery("#valFilter").val(1);
        jQuery("body #wrapper").addClass("sidebar_minimize");
        //alert(2);
    }
}

function dommodal(cost_center_code = '', biaya = '', gl_account_code = '', gpm_pm_code_code = '', rsm_shopper_code_code = '') {
    var filtering = rsm_shopper_code_code != '' && rsm_shopper_code_code == 'filter' ? ['rsm_shopper_code', gpm_pm_code_code] : ['gpm_pm_code', gpm_pm_code_code];
    if (filtering[0] == 'rsm_shopper_code') {
        var dataM = {
            cost_center: cost_center_code,
            biaya: biaya,
            gl_account: gl_account_code,
            rsm_shopper_code: filtering[1]
        }
    } else if (filtering[0] == 'gpm_pm_code') {
        var dataM = {
            cost_center: cost_center_code,
            biaya: biaya,
            gl_account: gl_account_code,
            gpm_pm_code: filtering[1]
        }
    }


    console.log(dataM);
    //var objModal = getJson(path+'/dommodal',data);
    if (jQuery("#myTableModal #datatablesModal").remove() && jQuery("#myTableModal #datatablesModal_wrapper").remove()) {
        jQuery("#myTableModal").append("<table id='datatablesModal' class='table table-bordered' width='100%' style='border-style:&quot;solid&quot;'></table>");
        jQuery("#datatablesModal").append("<thead width='100%'>" +
            "<tr>" +
            "<td><b>Keterangan :</b></td>" +
            "<td ><b></b></td>" +
            "</tr>" +
            "</thead>" + "");
        var tablemodal = $("#datatablesModal").DataTable({
            serverSide: true,
            "processing": true,
            // "pageLength": 10,
            "bFilter": false,
            "bLengthChange": false,
            //"sScrollX": '100%',
            fixedHeader: false,
            ajax: {
                url: "" + path + "/getListmodal",
                data: function (data) {
                    data.f_search = jQuery("#form-filter").serialize(); //+jQuery("#head-filter").val();
                    data.f_head = jQuery("#form-header").serialize();
                    data.datam = dataM;
                },
                'beforeSend': function (request) {}
            },
            "drawCallback": function (settings) {
                jQuery("#totaltext").text(jQuery("#grandtotal").text());
            }

        });
    }
}

function getJson(url, data) {
    //console.log(url);
    return JSON.parse(jQuery.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        global: false,
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest')

            //swal("loading");
        },
        success: function (msg) {
            //swal.close();
        }
    }).responseText);
}

function printdata(url, filename) {
    var data = {
        'f_search': jQuery("#form-filter").serialize(),
        'f_head': jQuery("#form-header").serialize(),
        'filename': filename
    }
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        beforeSend: function () {
            jQuery(".stage").show();
        },
        statusCode: {
            500: function () {

            },
            200: function () {
                jQuery(".stage").hide();
                jQuery(".btn-download").show();
            }
        },
        success: function (result) {
            //alert();
            jQuery(".stage").hide();
            jQuery(".btn-download").show();
        }
    })

}