$('.datepicker').datepicker({
    dateFormat: 'dd/mm/yy',
});
		
jQuery(".select2").select2();
jQuery(".filter-select2").select2({ 
    //dropdownCssClass: "font-filter-select2"
    placeholder: "(All)",
    allowClear: true
});
jQuery(".filter-multiple-select2").select2({ 
	width: 'resolve',
	placeholder: "(All)",
    allowClear: true
    //dropdownCssClass: "font-filter-select2"
    
});

$(".sidebar").hover(function () {
    $("#filter-side").stop(true,true).hide(0);
    $("#content-side").removeClass("col-md-8");
    $("#content-side").addClass("col-md-12");
    jQuery("#valFilter").val(0);
    
}, function () {
});

function filteringform(){
    var valFilter = jQuery("#valFilter").val();
    if(valFilter == 1){
        $("#filter-side").stop(true,true).hide(0);
        $("#content-side").removeClass("col-md-8");
        $("#content-side").addClass("col-md-12");
        jQuery("#valFilter").val(0);
        jQuery("body #wrapper").removeClass("sidebar_minimize");
        //alert(1)
    }else{
        $("#filter-side").stop(true,true).show(0);
        $("#content-side").removeClass("col-md-12");
        $("#content-side").addClass("col-md-8");
        jQuery("#valFilter").val(1);
        jQuery("body #wrapper").addClass("sidebar_minimize");
        //alert(2);
    }
    }
    
    function hideSidebar(){
        $("#filter-side").stop(true,true).hide(0);
        $("#content-side").removeClass("col-md-8");
        $("#content-side").addClass("col-md-12");
        jQuery("#valFilter").val(1);
        filteringform()
    }

var path = jQuery("#pathData").val();

preparing_dashboard();
jQuery(".stage").hide();
var tableses = $("#datatablesE").DataTable({    
serverSide: true,
"processing": true,   
"pageLength": 7,
"bFilter" : false,               
"bLengthChange": false,
"sScrollX": '100%',


ajax: {
 url: ""+path+"/getList",
 data :function(data){
                data.f_search = jQuery("#form-filter").serialize();//+jQuery("#head-filter").val();
                data.f_head = jQuery("#form-header").serialize();
 },
 'beforeSend': function (request) {
    //jQuery("#table-data-filter").hide();
    jQuery(".stage").show();
    }
},
"drawCallback": function( settings ) {
  //alert( 'DataTables has redrawn the table' );
  
  jQuery("#table-data-filter").show();
  jQuery(".stage").hide();
 
  //  drawmycanvasbar();
}

});	
/*$(window).bind('resize', function () {
    tableses.fnAdjustColumnSizing();
  } );*/

function domCardtable(){

//alert(jQuery("#head_filter option:selected").text());


}	
function getJson(url,data){
//console.log(url);
return JSON.parse(jQuery.ajax({
    type : "POST",
    url : url,
    data : data,
    dataType : "json",
    global : false,
    async : false,
    beforeSend: function(request){
        request.setRequestHeader("HTTP_X_REQUESTED_WITH",'xmlhttprequest')
        
        //swal("loading");
    },
    success : function(msg){
        //swal.close();
    }
}).responseText);
}

function getJsonFilter(url,data){
    //console.log(url);
   
    return JSON.parse(jQuery.ajax({
        type : "POST",
        url : url,
        data : data,
        dataType : "json",
        global : false,
        async : false,
        beforeSend: function(request){
            request.setRequestHeader("HTTP_X_REQUESTED_WITH",'xmlhttprequest')
            var interval = setInterval(function() {
                $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
            }, 1000);
            //swal("loading");
           
        },
        success : function(msg){
           
        }
    }).responseText);
}   

function printdata(url,filename){
var data = {
        'f_search' : jQuery("#form-filter").serialize(),
        'f_head' :jQuery("#form-header").serialize(),
        'filename' :filename
    }
    $.ajax({
            type: "POST",
            url: url,
            dataType:"json",
            data: data,
            beforeSend: function() {    
                jQuery(".stage").show();
              },
              statusCode: {
                500: function() {
                  
                },
                200 : function() {
                    jQuery(".stage").hide();
                    jQuery(".btn-download").show();
                }
              },
              success : function(result){
                //alert();
                jQuery(".stage").hide();
                jQuery(".btn-download").show();
            }
    })
    
}

function choosetable(vals){
if($('html, body').animate({ scrollTop: $('body').offset().top }, 'slow')){


if(jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#value").val() == "" || jQuery("#head_filter").val() == "" )  {
    
    swal("Pastikan tanggal faktur, Value terisi, Filter table name terisi");
}  else{
    
    jQuery("#th-grand").find("b").text("");
    jQuery("#th-grand").find("b").text(jQuery("#head_filter option:selected").text());
   jQuery(".stage").show();
   //jQuery("#table-data-filter").hide();
    domCardtable();
    //getcardsum();
    
    //drawmycanvasbar(); 
   var ntable =  Math.random() * (50 - 10) + 10
    jQuery(".box h1").text("Getting Table ....");
   tableses.ajax.reload(null,true);
    
    
    
    //choosedisplay(jQuery("#display_filter").val());
}
}
//getcardsum();
//choosedisplay(jQuery("#display_filter").val());

}

function formatRupiah(angka,prefix){
if(angka != null){
    var	number_string = angka.toString(),
    split	= number_string.split('.'),
    sisa 	= split[0].length % 3,
    rupiah 	= split[0].substr(0, sisa),
    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
        
if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
}
return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

    //return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}else{
    return 0,00;
}

}
function preparing_dashboard(){

   jQuery("#form-filter").hide();
   jQuery("#btn-filter").hide();
    jQuery(".progress").show();
    jQuery("#preparing_desktop").show();
    jQuery("#preparing_desktop").text("Preparing Distibutor.....");
    
    var h =6;
    for(n = 0 ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
   var distributor_code = getJsonFilter(path+'/getfiltervalue',{id:'distributor_code'});	
   domoption(distributor_code,'distributor_code','distributor_code');
    jQuery("#preparing_desktop").text("Preparing Branch.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var branch_code = getJsonFilter(path+'/getfiltervalue',{id:'branch_code'});	
    domoption(branch_code,'branch_code','branch_code');
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing GM/PM.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var gpm_pm_code = getJsonFilter(path+'/getfiltervalue',{id:'gpm_pm_code'});	
    domoption(gpm_pm_code,'gpm_pm_code','gpm_pm_code');

    jQuery("#preparing_desktop").text("Preparing RSM.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var rsm_code = getJsonFilter(path+'/getfiltervalue',{id:'rsm_code'});	
    domoption(rsm_code,'rsm_code','rsm_code');
    //
    
    jQuery("#preparing_desktop").text("Preparing Shopper.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var shopper_code = getJsonFilter(path+'/getfiltervalue',{id:'shopper_code'});	
    domoption(shopper_code,'shopper_code','shopper_code');

    jQuery("#preparing_desktop").text("Preparing AM/APM/ASM.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var am_apm_asm_code = getJsonFilter(path+'/getfiltervalue',{id:'am_apm_asm_code'});	
    domoption(am_apm_asm_code,'am_apm_asm_code','am_apm_asm_code');
    
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing MSR/MD/SE.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var msr_md_se_code = getJsonFilter(path+'/getfiltervalue',{id:'msr_md_se_code'});	
    domoption(msr_md_se_code,'msr_md_se_code','msr_md_se_code');
    
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Segment.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var segment = getJsonFilter(path+'/getfiltervalue',{id:'segment'});	
    domoption(segment,'segment','segment');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Customer.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var customer_code = getJsonFilter(path+'/getfiltervalue',{id:'customer_code'});	
    domoption(customer_code,'customer_code','customer_code');
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Layanan.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var layanan_code = getJsonFilter(path+'/getfiltervalue',{id:'layanan_code'});	
    domoption(layanan_code,'layanan_code','layanan_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Lini.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var lini_code = getJsonFilter(path+'/getfiltervalue',{id:'lini_code'});	
    domoption(lini_code,'lini_code','lini_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Group1.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group1_code = getJsonFilter(path+'/getfiltervalue',{id:'material_group1_code'});	
    domoption(material_group1_code,'material_group1_code','material_group1_code');

    jQuery("#preparing_desktop").text("Preparing Group2.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group2_code = getJsonFilter(path+'/getfiltervalue',{id:'material_group2_code'});	
    domoption(material_group2_code,'material_group2_code','material_group2_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Group3.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow",n+"%").css("width",n+"%");
        jQuery(".progress .progress-bar").text(n+"%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group3_code = getJsonFilter(path+'/getfiltervalue',{id:'material_group3_code'});	
    domoption(material_group3_code,'material_group3_code','material_group3_code');
    
    jQuery("#preparing_desktop").text("Preparing Brand.....");
    h = h + 6;
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow","92%").css("width","92%");
        jQuery(".progress .progress-bar").text("92%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var brand = getJsonFilter(path+'/getfiltervalue',{id:'brand'});
    domoption(brand,'brand','brand');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Product.....");
    for(n = n ; n <= h ; n++){
        jQuery(".progress .progress-bar").attr("aria-valuenow","98%").css("width","98%");
        jQuery(".progress .progress-bar").text("98%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_code = getJsonFilter(path+'/getfiltervalue',{id:'material_code'});	
    domoption(material_code,'material_code','material_code');
    
    $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    jQuery("#preparing_desktop").text("Preparing Filter Component.....");
    jQuery(".progress .progress-bar").attr("aria-valuenow","100%").css("width","100%");
    jQuery(".progress .progress-bar").text("100%");
    //jQuery(".progress .progress-bar").hide();
    $('.progress').delay(2000).hide(0);
    jQuery("#preparing_desktop").delay(2000).hide(0);
    //jQuery("#preparing_desktop").text("");
    jQuery("#form-filter").delay(1700).show(0)
   jQuery("#btn-filter").delay(1700).show(0)
}
function domoption(data,id,re){
        jQuery("#"+id+" option").remove();
        ///jQuery("#"+id).append("<option value=''>(All)</option>")
        jQuery.each(data,function(key,val){
            //console.log(key);
            if(re == 'layanan_code'){
                jQuery("#"+id).append("<option value='"+val.layanan_code+"'>"+val.layanan_name+"</option>")
            }
            if(re == 'lini_code'){
                jQuery("#"+id).append("<option value='"+val.lini_code+"'>"+val.lini_name+"</option>")
            }
            if(re == 'distributor_code'){
                jQuery("#"+id).append("<option value='"+val.distributor_code+"'>"+val.distributor_name+"</option>")
            }
            if(re == 'branch_code'){
                jQuery("#"+id).append("<option value='"+val.branch_code+"'>"+val.branch_name+"</option>")
            }
            if(re == 'gpm_pm_code'){
                jQuery("#"+id).append("<option value='"+val.gpm_pm_code+"'>"+val.gpm_pm_name+"</option>")
            }
            if(re == 'rsm_code'){
                jQuery("#"+id).append("<option value='"+val.rsm_code+"'>"+val.rsm_name+"</option>")
            }
            if(re == 'shopper_code'){
                jQuery("#"+id).append("<option value='"+val.shopper_code+"'>"+val.shopper_name+"</option>")
            }
            if(re == 'am_apm_asm_code'){
                jQuery("#"+id).append("<option value='"+val.am_apm_asm_code+"'>"+val.am_apm_asm_name+"</option>")
            }
            if(re == 'msr_md_se_code'){
                jQuery("#"+id).append("<option value='"+val.msr_md_se_code+"'>"+val.msr_md_se_code+"</option>")
            }
            if(re == 'customer_code'){
                jQuery("#"+id).append("<option value='"+val.customer_code+"'>"+val.customer_name+"</option>")
            }
            if(re == 'material_group1_code'){
                jQuery("#"+id).append("<option value='"+val.material_group1_code+"'>"+val.material_group1_name+"</option>")
            }
            if(re == 'material_group2_code'){
                jQuery("#"+id).append("<option value='"+val.material_group2_code+"'>"+val.material_group2_name+"</option>")
            }
            if(re == 'material_group3_code'){
                jQuery("#"+id).append("<option value='"+val.material_group3_code+"'>"+val.material_group3_name+"</option>")
            }
            if(re == 'segment'){
                jQuery("#"+id).append("<option value='"+val.segment+"'>"+val.segment+"</option>")
            }
            if(re == 'brand'){
                jQuery("#"+id).append("<option value='"+val.brand+"'>"+val.brand+"</option>")
            }
            if(re == 'material_code'){
                jQuery("#"+id).append("<option value='"+val.material_code+"'>"+val.material_name+" "+val.kemasan+"</option>")
            }
           
        })
    }
function loadingswal(){
var myhost 	= '<?= base_url() ?>';
var el 			= document.createElement("img");
el.src 			= ""+myhost+"/assets/images/icon/loadingrole.gif";
swal({
    title : "Loading",
    content: el,
    
    closeOnClickOutside: false,
    closeOnEsc: false,
    allowOutsideClick: false,
    buttons: false,
    showCancelButton: false,
    showConfirmButton: false,
    dangerMode: true,
})
}





