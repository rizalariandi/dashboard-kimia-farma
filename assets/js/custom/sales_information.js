var param = {}
var data_table
var search = ''
var customer_selected = []
var grand_total = []
var newRow = [];
var newRow_ = [];
$(document).ready(function () {

    setTimeout(function () {
        swal.close();
    }, 2000);
    // $('#realisasi-data').DataTable({
    data_table = $('#datatablesE').DataTable({
        "scrollY": "400px",
        "scrollCollapse": true,
        "scrollX": true,
        "ordering": true,
        "bFilter": true,
        "bLengthChange": false,
        "lengthMenu": [100, 1000],
        "dom": 'Bfrtip',
        "columnDefs": [{
            className: "dt-body-right",
            "targets": rightvar
        }],
        buttons: [
            'pageLength',
            // {
            //     extend: 'excelHtml5',
            //     footer: true,
            //     filename: function () {
            //         return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
            //     },
            //     exportOptions: {
            //         columns: ':visible',
            //         modifier: {
            //             selected: null
            //         }
            //     },
            //     title: function () {
            //         return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
            //     },
            // },
            // {
            //     extend: 'csvHtml5',
            //     footer: true,
            //     filename: function () {
            //         return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
            //     },
            //     exportOptions: {
            //         columns: ':visible',
            //         modifier: {
            //             selected: null
            //         }
            //     },
            //     title: function () {
            //         return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
            //     },
            // },
            // 'colvis'
        ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(),
                data;
            console.log(api)
            // Update footer by showing the total with the reference of the column index 
            $(api.column(0).footer()).html('Grand Total');
            $(api.column(1).footer()).html('');
            $(api.column(2).footer()).html(numberWithCommas(grand_total[1]));
            $(api.column(3).footer()).html(numberWithCommas(grand_total[2]));
            $(api.column(4).footer()).html(numberWithCommas(grand_total[3]));
            $(api.column(5).footer()).html(numberWithCommas(grand_total[4]));
            $(api.column(6).footer()).html(numberWithCommas(grand_total[5]));
            $(api.column(7).footer()).html(numberWithCommas(grand_total[6]));
            $(api.column(8).footer()).html(numberWithCommas(grand_total[7]));
            $(api.column(9).footer()).html(numberWithCommas(grand_total[8]));
            $(api.column(10).footer()).html(numberWithCommas(grand_total[9]));
            $(api.column(11).footer()).html(numberWithCommas(grand_total[10]));
            $(api.column(12).footer()).html(numberWithCommas(grand_total[11]));
            $(api.column(13).footer()).html(numberWithCommas(grand_total[12]));
            $(api.column(14).footer()).html(numberWithCommas(grand_total[13]));
            $(api.column(15).footer()).html(numberWithCommas(grand_total[14]));
            $(api.column(16).footer()).html(numberWithCommas(grand_total[15]));
            $(api.column(17).footer()).html(numberWithCommas(grand_total[16]));
            $(api.column(17).footer()).html(numberWithCommas(grand_total[17]));
            $(api.column(19).footer()).html(numberWithCommas(grand_total[18]));
            // $(api.column(2).footer()).html('Rp ' + numberWithCommas('0'));

        },

        "drawCallback": function (settings) {
            //alert( 'DataTables has redrawn the table' );

            jQuery("#table-data-filter").show();
            jQuery(".stage").hide();

            //  drawmycanvasbar();
        },
        "columns": [{
                data: "nama",
                // title: 'Vendor",                   
                className: 'text-left',
                // render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "code",
                // title: 'Vendor",                   
                className: 'text-left',
                // render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "qty",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "target",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "realisasiqtyty",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)

            },
            {
                data: "realisasity",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)

            },
            {
                data: "realisasiqtyly",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "realisasily",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "ptd",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "achievement",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "growth",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "share_realisasi_ty",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "share_realisasi_ly",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "margin_marketing_amount",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "margin_marketing_persen",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "margin_distributor_amount",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "margin_distributor_persen",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 2)
            },
            {
                data: "noo",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "npp",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            },
            {
                data: "otr",
                className: 'text-right',
                render: $.fn.dataTable.render.number(",", ".", 0)
            }
        ]
    });

    loadDataTable()
})

$('.dates').datepicker({
    dateFormat: 'dd/mm/yy',
});

setDates();

jQuery(".select2").select2();
jQuery(".filter-select2").select2({
    //dropdownCssClass: "font-filter-select2"
    placeholder: "(All)",
    allowClear: true
});

jQuery(".filter-multiple-select-typing").select2({
    //dropdownCssClass: "font-filter-select2"
    placeholder: "",
    allowClear: true
});



jQuery(".filter-multiple-select2").select2({
    width: 'resolve',
    placeholder: "(All)",
    allowClear: true
    //dropdownCssClass: "font-filter-select2"

});

$(".sidebar").hover(function () {
    $("#filter-side").stop(true, true).hide(0);
    $("#content-side").removeClass("col-md-8");
    $("#content-side").addClass("col-md-12");
    jQuery("#valFilter").val(0);

}, function () {});


var oprationStat = jQuery("#oprationStat").val();
if (oprationStat == 1) {
    //jQuery('#side-menu').hide();

    jQuery(".bg-title").css("background", "#eeeeee");
    jQuery('#filtering-side').hide();
    /*jQuery('#menu-navigation').append('<div class="sidebar-filtering row" align="center" style="margin-top:10px;">'+
                                    '<button class="btn btn-circle" style="margin-bottom:10px;background-color:#08388F;color:white;" data-toggle="tooltip" data-placement="bottom" title="menu" onClick="callMenu();" ><i class="fa fa-bars"></i></button>&nbsp;&nbsp;'+
                                    '<button class="btn btn-circle" style="margin-bottom:10px;background-color:#08388F;color:white;" data-toggle="tooltip" data-placement="bottom" title="filter" onClick="callFilter();"><i class="fa fa-filter"></i></button>'+
                                    '</div>');*/

} else {

}

function setDates(){
    const curdate = new Date();
    const currMonth = curdate.getMonth();
    const currYear = curdate.getFullYear();
    const endDate = new Date(currYear, currMonth, 1);
    const startDate = new Date(currYear, currMonth - 2, 1);

    $("#tanggal_faktur_end").datepicker("setDate", endDate);
    $("#tanggal_faktur_start").datepicker("setDate", startDate);
}

function callMenu() {
    //jQuery('#sidebar-filtering').remove();
    /*jQuery(".menu-navigation").css("width","220px");
    //jQuery("#container-dom").addClass("col-md-11");
    jQuery("#page-wrapper").addClass("col-md-12");
    jQuery("#page-wrapper").css("width",'100%');
    jQuery("#container-dom").css("margin-left","10px");
    jQuery("#container-dom").css("width","82%");*/
    jQuery('#filtering-side').hide();
    /*jQuery('#table-name').removeClass("col-md-8");
    jQuery('#table-name').addClass("col-md-6");*/
    jQuery('#side-menu').fadeIn("slow");
}

function callFilter() {
    //alert();
    //jQuery('#sidebar-filtering').remove();
    /*jQuery(".menu-navigation").css("width","30%");
    jQuery("#container-dom").addClass("col-md-10");
    jQuery("#container-dom").css("margin-left","20%");*/
    jQuery('#side-menu').hide();
    /*jQuery('#table-name').removeClass("col-md-6");
    jQuery('#table-name').addClass("col-md-8");*/
    jQuery('#filtering-side').fadeIn("slow");
}

function filteringform() {
    var valFilter = jQuery("#valFilter").val();
    if (valFilter == 1) {
        $("#filter-side").stop(true, true).hide(0);
        $("#content-side").removeClass("col-md-8");
        $("#content-side").addClass("col-md-12");
        jQuery("#valFilter").val(0);
        jQuery("body #wrapper").removeClass("sidebar_minimize");
        //alert(1)
    } else {
        $("#filter-side").stop(true, true).show(0);
        $("#content-side").removeClass("col-md-12");
        $("#content-side").addClass("col-md-8");
        jQuery("#valFilter").val(1);
        jQuery("body #wrapper").addClass("sidebar_minimize");
        //alert(2);
    }
}

function hideSidebar() {
    $("#filter-side").stop(true, true).hide(0);
    $("#content-side").removeClass("col-md-8");
    $("#content-side").addClass("col-md-12");
    jQuery("#valFilter").val(1);
    filteringform()
}

var path = jQuery("#pathData").val();

//preparing_dashboard();
jQuery(".stage").hide();
rightvar = [];
rightvar.push(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
var buttonCommon = {
    exportOptions: {
        format: {
            body: function (data, row, column, node) {
                // Strip $ from salary column to make it numeric
                return column > 0 && column < 17 && (column != 8 || column != 9 || column != 12 || column != 14) ?
                    data : data;
            }
        }
    }
};

function loadDataTable() {

    $.ajax({
        url: "" + path + "/getList",
        method: "GET",
        data: {
            f_search: jQuery("#form-filter").serialize(), //+jQuery("#head-filter").val();
            f_head: jQuery("#form-header").serialize()
        },
        'beforeSend': function (request) {
            //jQuery("#table-data-filter").hide();
            loading();
            jQuery(".stage").show();
        },
        'complete': function (data) {
            console.log(data,'data');
            result = JSON.parse(data.responseText)
            newRow = result.data;
            grand_total = result.Grand_Total;
            data_table.rows.add(newRow);
            data_table.columns.adjust().draw();
            swal.close()
            jQuery("#val-targetqty").text(numberWithCommas(result.Grand_Total[1] || 0))
            jQuery("#val-targetvalue").text(numberWithCommas(result.Grand_Total[2] || 0));
            jQuery("#val-realisasiqty").text(numberWithCommas(result.Grand_Total[3] || 0));
            jQuery("#val-realisasivalue").text(numberWithCommas(result.Grand_Total[4] || 0));
            jQuery("#val-lastyearqty").text(numberWithCommas(result.Grand_Total[5] || 0));
            jQuery("#val-lastyearvalue").text(numberWithCommas(result.Grand_Total[6] || 0));
            jQuery("#val-ptd").text(numberWithCommas(result.Grand_Total[7] || 0));
            jQuery("#val-achievement").text(numberWithCommas(result.Grand_Total[8] || 0));
            jQuery("#val-growth").text(numberWithCommas(result.Grand_Total[9] || 0));
            jQuery("#val-noo").text(numberWithCommas(result.Grand_Total[16] || 0));
            jQuery("#val-npp").text(numberWithCommas(result.Grand_Total[17] || 0));
            jQuery("#val-otr").text(numberWithCommas(result.Grand_Total[18] || 0));
            jQuery("#val-margin").text(numberWithCommas(result.Grand_Total[12] || 0));

            // ---------------------------------------------------------------------
            var footer_ = []
            var header_ = []
            header_ = [{
                "nama": "Nama",
                "code": "Kode",
                "qty": "Target QTY",
                "target": "Target (Value)",
                "realisasiqtyty": "Realisasi Qty",
                "realisasity": "Realisasi (Value)",
                "realisasiqtyly": "Last Year QTY",
                "realisasily": "Last Year (Value)",
                "ptd": "PTD",
                "achievement": "Achiev( % )",
                "growth": "Growth ( % )",
                "share_realisasi_ty": "Share Realisasi ( % )",
                "share_realisasi_ly": "Share Last Year( % )",
                "margin_marketing_amount": "Margin Marketing Amount",
                "margin_marketing_persen": "Margin Marketing ( % )",
                "margin_distributor_amount": "Margin Distributor Amount",
                "margin_distributor_persen": "Margin Distributor ( % )",
                "noo": "Noo",
                "otr": "OTR"
            }];
            // if (grand_total > 0) {
                footer_ = [{
                    "nama": 'Grand Total',
                    "code": "",
                    "qty": grand_total[1] ? grand_total[1] : 0 ,
                    "target": grand_total[2] ? grand_total[2] : 0 ,
                    "realisasiqtyty": grand_total[3] ? grand_total[3] : 0 ,
                    "realisasity": grand_total[4] ? grand_total[4] : 0 ,
                    "realisasiqtyly": grand_total[5] ? grand_total[5] : 0 ,
                    "realisasily": grand_total[6] ? grand_total[6] : 0 ,
                    "ptd": grand_total[7] ? grand_total[7] : 0 ,
                    "achievement": grand_total[8] ? grand_total[8] : 0 ,
                    "growth": grand_total[9] ? grand_total[9] : 0 ,
                    "share_realisasi_ty": grand_total[10] ? grand_total[10] : 0 ,
                    "share_realisasi_ly": grand_total[11] ? grand_total[11] : 0 ,
                    "margin_marketing_amount": grand_total[12] ? grand_total[12] : 0 ,
                    "margin_marketing_persen": grand_total[13] ? grand_total[13] : 0 ,
                    "margin_distributor_amount": grand_total[14] ? grand_total[14] : 0 ,
                    "margin_distributor_persen": grand_total[15] ? grand_total[15] : 0 ,
                    "noo": grand_total[16] ? grand_total[16] : 0 ,
                    "otr": grand_total[17] ? grand_total[17] : 0 
                }];
            // }

            newRow_ = header_.concat(newRow).concat(footer_);
        }
    }).done(function () {
        if (newRow.length !== 0) {
            data_table.clear().draw();
        } else {
            data_table.clear().draw();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        // if (jqXHR.status != 422)
        //     swal("Error " + jqXHR.status, textStatus + ', ' + errorThrown, "error");
    })
}

// var tableses = $("#datatablesE").DataTable({
//     fixedHeader: {
//         header: false,
//         footer: false
//     },
//     ordering: true,
//     rowReorder: true,
//     scrollY: 400,
//     serverSide: true,
//     lengthMenu: [[-1], ["All"]],
//     dom: 'Bfrtip',
//     "processing": true,
//     //"pageLength": 10,
//     "bFilter": false,
//     //"bLengthChange": true,
//     "sScrollX": '100%',
//     "columnDefs": [
//         {
//             className: "dt-body-right", "targets": rightvar
//         }
//     ],

//     ajax: {
//         url: "" + path + "/getList",
//         data: function (data) {
//             data.f_search = jQuery("#form-filter").serialize();//+jQuery("#head-filter").val();
//             data.f_head = jQuery("#form-header").serialize();
//         },
//         'beforeSend': function (request) {
//             //jQuery("#table-data-filter").hide();
//             loading();
//             jQuery(".stage").show();
//         }
//         ,
//         'complete': function (data) {
//             swal.close()
//         }
//     }
//     ,
//     buttons: [
//         'pageLength',
//         $.extend(true, {}, buttonCommon, {
//             extend: 'excelHtml5',
//             filename: function () {
//                 return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
//             },
//             title: function () {
//                 return 'Sales Information_' + jQuery('#tanggal_faktur_start').val() + ' s/d ' + jQuery('#tanggal_faktur_end').val()
//             },
//         }),
//         $.extend(true, {}, buttonCommon, {
//             extend: 'csvHtml5',
//             filename: function () {
//                 return 'Sales Information_' + jQuery('#tanggal_faktur_start').val().replace('/', '') + ' s/d ' + jQuery('#tanggal_faktur_end').val().replace('/', '')
//             },
//             title: function () {
//                 return 'Sales Information_' + jQuery('#tanggal_faktur_start').val() + ' s/d ' + jQuery('#tanggal_faktur_end').val()
//             },
//         }),
//         'colvis'


//     ],

//     "drawCallback": function (settings) {
//         //alert( 'DataTables has redrawn the table' );

//         jQuery("#table-data-filter").show();
//         jQuery(".stage").hide();

//         //  drawmycanvasbar();
//     }

// });
/*$(window).bind('resize', function () {
    tableses.fnAdjustColumnSizing();
  } );*/
function getcardsum() {
    //alert();

    //jQuery("#table-data-filter").hide();
    var path = jQuery("#pathData").val();
    var data = {
        'f_search': jQuery("#form-filter").serialize(),
        'f_head': jQuery("#form-header").serialize()
    }

    //var oC_qsales = getJson(path+'/qsales',data);
    jQuery(".box h1").text("Getting Card 0%");
    jQuery("#val-qtysales").text("");
    var oC_targetqty = getJson(path + '/targetqty', data);
    //jQuery("#val-targetqty").text(formatRupiah(oC_targetqty,''));
    jQuery("#val-targetqty").text(oC_targetqty).css("font=-size", "10");


    jQuery(".box h1").text("Getting Card 5%");
    var oC_targetvalue = getJson(path + '/targetvalue', data);
    jQuery("#val-targetvalue").text(oC_targetvalue);
    //jQuery("#val-targetvalue").text(formatRupiah(oC_targetvalue,''));


    jQuery(".box h1").text("Getting Card 10%");
    var oC_realisasiqty = getJson(path + '/realisasiqty', data);
    //jQuery("#val-realisasiqty").text(formatRupiah(oC_realisasiqty,''));
    jQuery("#val-realisasiqty").text(oC_realisasiqty);

    jQuery(".box h1").text("Getting Card 25%");
    var oC_realisasivalue = getJson(path + '/realisasivalue', data);
    //jQuery("#val-realisasivalue").text(formatRupiah(oC_realisasivalue,''));
    jQuery("#val-realisasivalue").text(oC_realisasivalue);

    jQuery(".box h1").text("Getting Card 35%");
    var oC_lastyearqty = getJson(path + '/lastyearqty', data);
    jQuery("#val-lastyearqty").text(oC_lastyearqty);
    //jQuery("#val-lastyearqty").text(formatRupiah(oC_lastyearqty,''));

    jQuery(".box h1").text("Getting Card 40%");
    var oC_lastyearvalue = getJson(path + '/lastyearvalue', data);
    //jQuery("#val-lastyearvalue").text(formatRupiah(oC_lastyearvalue,''));
    jQuery("#val-lastyearvalue").text(oC_lastyearvalue);

    jQuery(".box h1").text("Getting Card 52%");
    var oC_ptd = getJson(path + '/ptd', data);
    //jQuery("#val-ptd").text(formatRupiah(oC_ptd,''));
    jQuery("#val-ptd").text(oC_ptd);


    jQuery(".box h1").text("Getting Card 65%");
    var oC_achievement = getJson(path + '/achievement', data);
    //jQuery("#val-achievement").text(formatRupiah(oC_achievement,''));
    jQuery("#val-achievement").text(oC_achievement);

    jQuery(".box h1").text("Getting Card 77%");
    var oC_growth = getJson(path + '/growth', data);
    //jQuery("#val-growth").text(formatRupiah(oC_growth,''));
    jQuery("#val-growth").text(oC_growth);

    jQuery(".box h1").text("Getting Card 84%");
    var oC_noo = getJson(path + '/noo', data);
    jQuery("#val-noo").text(oC_noo);

    jQuery(".box h1").text("Getting Card 91%");
    var oC_otr = getJson(path + '/otr', data);
    jQuery("#val-otr").text(oC_otr);


    jQuery(".box h1").text("Getting Card 94%");
    var oC_margin = getJson(path + '/margin', data);
    jQuery("#val-margin").text(oC_margin);
    jQuery(".box h1").text("Getting Card 100%");
}

function domCardtable() {

    //alert(jQuery("#head_filter option:selected").text());


}

function getJson(url, data) {


    return JSON.parse(jQuery.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        global: false,
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest')

            //loading();
        },
        success: function (msg) {
            //swal.close();
        }
    }).responseText);
}

function getJsonFilter(url, data) {


    return JSON.parse(jQuery.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        global: false,
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("HTTP_X_REQUESTED_WITH", 'xmlhttprequest')
            var interval = setInterval(function () {
                $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
            }, 1000);


        },
        success: function (msg) {

        }
    }).responseText);
}

function printdata(url, filename) {
    var data = {
        'f_search': jQuery("#form-filter").serialize(),
        'f_head': jQuery("#form-header").serialize(),
        'filename': filename
    }
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        beforeSend: function () {
            jQuery(".stage").show();
        },
        statusCode: {
            500: function () {

            },
            200: function () {
                jQuery(".stage").hide();
                jQuery(".btn-download").show();
            }
        },
        success: function (result) {
            //alert();
            jQuery(".stage").hide();
            jQuery(".btn-download").show();
        }
    })

}

function drawmycanvasbar(chart) {
    var data = {
        'f_search': jQuery("#form-filter").serialize(),
        'f_head': jQuery("#form-header").serialize(),
    }
    //var url = jQuery("#pathData").val()+'/getdatabar';
    jQuery(".box h1").text("Getting Chart...");
    //var oBjc = getJson(url,data);
    var mycanvas = jQuery(".my-canvas .canvas-body");
    jQuery(".my-canvas .canvas-body").show();
    mycanvas.html("");
    var i = 0;
    //var tahun = jQuery("#f_tahun").val();
    //var prevtahun = tahun.length > 0 ? parseInt(tahun-1) : '';
    var n = 40;
    jQuery.each(chart, function (key, val) {
        mycanvas.append('<div class="col-md-8 canvas-dom" style="width:40%"><p align="center">' + val.nama + '</p><canvas id="canvas' + i + '"></canvas></div>');
        var idcanvas = 'canvas' + i + '';

        n = n + 10;

        var datac = [Math.round(val.sales_target), Math.round(val.realisasi_next), Math.round(val.realisasi_prev)];
        /*if( val.nama == 'SALICYL'){


        }*/
        var minvalues = Math.min.apply(null, datac);
        var maxvalues = Math.max.apply(null, datac);
        var labeling = ['Sales Target', 'Realisasi Year', 'Realisasi Last Year '];
        var labelhead = val.nama;
        chartdrawline(idcanvas, datac, labeling, labelhead, minvalues, maxvalues);

        if (i === 50) {


            return false;
        }
        i++;
    })
    jQuery(".box h1").text("");
    swal.close();
}

function choosedisplay(vals) {
    if (vals == 'table') {
        jQuery(".my-canvas").stop(true, true).hide(0);
        jQuery(".my-table").stop(true, true).show(0);
    } else if (vals == 'diagram') {
        jQuery(".my-canvas").stop(true, true).show(0);
        jQuery(".my-table").stop(true, true).hide(0);
    } else {
        jQuery(".my-canvas").stop(true, true).show(0);
        jQuery(".my-table").stop(true, true).show(0);
    }
}

$("#filter_combo").change(function (e) {

    e.preventDefault();


    choosetable('m');


});

function choosetable(vals) {



    if ($('html, body').animate({
            scrollTop: $('body').offset().top
        }, 'slow')) {

        if (jQuery("#tanggal_faktur_start").val() == "" || jQuery("#tanggal_faktur_end").val() == "" || jQuery("#value").val() == "" || jQuery("#head_filter").val() == "") {

            swal("Pastikan tanggal faktur, Value terisi, Filter table name terisi");
        }
        // else if ($('#filter_combo').val() === 'customer_code' && !$("#customer_code").val()) {
        //     swal("Pastikan filter customer terisi");
        // }
        else {
            jQuery("#th-grand").find("b").text("");
            jQuery("#th-grand").find("b").text(jQuery("#head_filter option:selected").text());
            jQuery(".stage").show();
            //jQuery("#table-data-filter").hide();
            domCardtable();
            //getcardsum();


            var ntable = Math.random() * (50 - 10) + 10
            //DICOMMENT SESUAI PERMINTAAN
            //jQuery(".box h1").text("Getting Table ....");

            //tableses.ajax.reload(null,true);
            // tableses.ajax.reload(function (json) {
            //     //console.debug(json.data[0]);
            //     var target_qty = 0;
            //     var target_value = 0;
            //     var realisasi_qty = 0;
            //     var realisasi_value = 0;
            //     var ly_qty = 0;
            //     var ly_value = 0;
            //     var ptd = 0;
            //     var achievment = 0;
            //     var growth = 0;
            //     var noo = 0;
            //     var otr = 0;
            //     var margin = 0;
            //     var chart = [];
            //     var chart_data = {
            //         nama: undefined,
            //         realisasi_next: undefined,
            //         realisasi_prev: undefined,
            //         sales_target: undefined
            //     };
            //     var i = 0;
            //     $.each(json.data, function (key, value) {
            //         target_qty = target_qty + parseInt(value[1].replace(/\./g, ""));
            //         target_value = target_value + parseInt(value[2].replace(/\./g, ""));
            //         realisasi_qty = realisasi_qty + parseInt(value[3].replace(/\./g, ""));
            //         realisasi_value = realisasi_value + parseInt(value[4].replace(/\./g, ""));
            //         ly_qty = ly_qty + parseInt(value[5].replace(/\./g, ""));
            //         ly_value = ly_value + parseInt(value[6].replace(/\./g, ""));
            //         ptd = ptd + parseInt(value[7].replace(/\./g, ""));
            //         achievment = (achievment + parseFloat(value[8])) || 0;
            //         growth = (growth + parseFloat(value[9]) || 0);
            //         noo = (noo + parseInt(value[16])) || 0;
            //         otr = otr + parseInt(value[17].replace(/\./g, ""));
            //         margin = margin + parseInt(value[12].replace(/\./g, ""));
            //         chart_data = new Object();
            //         chart_data.nama = value[0];
            //         chart_data.realisasi_next = value[4].replace(/\./g, "");
            //         chart_data.realisasi_prev = value[6].replace(/\./g, "");
            //         chart_data.sales_target = value[2].replace(/\./g, "");

            //         chart.push(chart_data);

            //     })
            //     jQuery("#val-targetqty").text(numberWithCommas(target_qty)).css("font=-size", "10");
            //     jQuery("#val-targetvalue").text(format_round(target_value));
            //     jQuery("#val-realisasiqty").text(numberWithCommas(realisasi_qty));
            //     jQuery("#val-realisasivalue").text(format_round(realisasi_value));
            //     jQuery("#val-lastyearqty").text(numberWithCommas(ly_qty));
            //     jQuery("#val-lastyearvalue").text(format_round(ly_value));
            //     jQuery("#val-ptd").text(numberWithCommas(ptd));
            //     jQuery("#val-achievement").text(achievment);
            //     jQuery("#val-growth").text(growth);
            //     jQuery("#val-noo").text(noo);
            //     jQuery("#val-otr").text(numberWithCommas(otr));
            //     jQuery("#val-margin").text(format_round(margin));

            //     // $('#myInput').val( json.lastInput );
            //     drawmycanvasbar(chart);
            //     swal.close();
            // });
            loadDataTable()


            //  choosedisplay(jQuery("#display_filter").val());
        }
    }

    //getcardsum();
    //choosedisplay(jQuery("#display_filter").val());


}



function format_round(rp) {

    if (rp < 1 && rp > 0) {
        return (Math.round(rp * 100)) + '%';

    } else if (Math.round(rp).toString().length >= 10) {

        return "Rp " + numberWithCommas(Math.round(rp / Math.pow(10, 9))) + 'M';
    } else {
        return ("Rp " + numberWithCommas(rp));
    }
}

function numberWithCommas(x) {
    //return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
        return 0;
    }

}

function loading() {
    swal({
        title: 'Tunggu Sebentar...',
        text: ' ',
        icon: 'info',
        buttons: false,
        closeOnClickOutside: false,
    });
}

function formatRupiah(angka, prefix) {
    if (angka != null) {
        var number_string = angka.toString(),
            split = number_string.split('.'),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

        //return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    } else {
        return 0, 00;
    }

}

function preparing_dashboard() {

    jQuery("#form-filter").hide();
    jQuery("#btn-filter").hide();
    jQuery(".progress").show();
    jQuery("#preparing_desktop").show();
    jQuery("#preparing_desktop").text("Preparing Distibutor.....");

    var h = 6;
    for (n = 0; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var distributor_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'distributor_code'
    });
    domoption(distributor_code, 'distributor_code', 'distributor_code');
    jQuery("#preparing_desktop").text("Preparing Branch.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var branch_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'branch_code'
    });
    domoption(branch_code, 'branch_code', 'branch_code');
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing GM/PM.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var gpm_pm_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'gpm_pm_code'
    });
    domoption(gpm_pm_code, 'gpm_pm_code', 'gpm_pm_code');

    jQuery("#preparing_desktop").text("Preparing RSM.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var rsm_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'rsm_code'
    });
    domoption(rsm_code, 'rsm_code', 'rsm_code');
    //

    jQuery("#preparing_desktop").text("Preparing Shopper.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var shopper_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'shopper_code'
    });
    domoption(shopper_code, 'shopper_code', 'shopper_code');

    jQuery("#preparing_desktop").text("Preparing AM/APM/ASM.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var am_apm_asm_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'am_apm_asm_code'
    });
    domoption(am_apm_asm_code, 'am_apm_asm_code', 'am_apm_asm_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing MSR/MD/SE.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var msr_md_se_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'msr_md_se_code'
    });
    domoption(msr_md_se_code, 'msr_md_se_code', 'msr_md_se_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Segment.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var segment = getJsonFilter(path + '/getfiltervalue', {
        id: 'segment'
    });
    domoption(segment, 'segment', 'segment');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Customer.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var customer_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'customer_code'
    });
    domoption(customer_code, 'customer_code', 'customer_code');
    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Layanan.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var layanan_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'layanan_code'
    });
    domoption(layanan_code, 'layanan_code', 'layanan_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Lini.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var lini_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'lini_code'
    });
    domoption(lini_code, 'lini_code', 'lini_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Group1.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group1_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'material_group1_code'
    });
    domoption(material_group1_code, 'material_group1_code', 'material_group1_code');

    jQuery("#preparing_desktop").text("Preparing Group2.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group2_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'material_group2_code'
    });
    domoption(material_group2_code, 'material_group2_code', 'material_group2_code');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Group3.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", n + "%").css("width", n + "%");
        jQuery(".progress .progress-bar").text(n + "%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_group3_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'material_group3_code'
    });
    domoption(material_group3_code, 'material_group3_code', 'material_group3_code');

    jQuery("#preparing_desktop").text("Preparing Brand.....");
    h = h + 6;
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", "92%").css("width", "92%");
        jQuery(".progress .progress-bar").text("92%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var brand = getJsonFilter(path + '/getfiltervalue', {
        id: 'brand'
    });
    domoption(brand, 'brand', 'brand');

    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Product.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", "98%").css("width", "98%");
        jQuery(".progress .progress-bar").text("98%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var material_code = getJsonFilter(path + '/getfiltervalue', {
        id: 'material_code'
    });
    domoption(material_code, 'material_code', 'material_code');


    h = h + 6;
    jQuery("#preparing_desktop").text("Preparing Product.....");
    for (n = n; n <= h; n++) {
        jQuery(".progress .progress-bar").attr("aria-valuenow", "98%").css("width", "98%");
        jQuery(".progress .progress-bar").text("98%");
        $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    }
    var layanan_group = getJsonFilter(path + '/getfiltervalue', {
        id: 'layanan_group'
    });
    domoption(layanan_group, 'layanan_group', 'layanan_group');

    $(".progress-bar").css("background-color", "hsla(" + Math.floor(Math.random() * (360)) + ", 75%, 58%, 1)")
    jQuery("#preparing_desktop").text("Preparing Filter Component.....");
    jQuery(".progress .progress-bar").attr("aria-valuenow", "100%").css("width", "100%");
    jQuery(".progress .progress-bar").text("100%");
    //jQuery(".progress .progress-bar").hide();
    $('.progress').delay(2000).hide(0);
    jQuery("#preparing_desktop").delay(2000).hide(0);
    //jQuery("#preparing_desktop").text("");
    jQuery("#form-filter").delay(1700).show(0)
    jQuery("#btn-filter").delay(1700).show(0)
}

function saveselected(id_filter, name) {
    param = {
        distributor_code: [],
        branch_code: [],
        gpm_pm_code: [],
        rsm_code: [],
        shopper_code: [],
        am_apm_asm_code: [],
        msr_md_se_code: [],
        segment: [],
        customer_code: [],
        layanan_group: [],
        layanan_code: [],
        lini_code: [],
        material_group1_code: [],
        material_group2_code: [],
        material_group3_code: [],
        brand: [],
        material_code: []
    }
    $("#distributor_code :selected").each(function () {
        param.distributor_code.push($(this).val());
    });
    $("#branch_code :selected").each(function () {
        param.branch_code.push($(this).val());
    });
    $("#gpm_pm_code :selected").each(function () {
        param.gpm_pm_code.push($(this).val());
    });
    $("#rsm_code :selected").each(function () {
        param.rsm_code.push($(this).val());
    });
    $("#shopper_code :selected").each(function () {
        param.shopper_code.push($(this).val());
    });
    $("#am_apm_asm_code :selected").each(function () {
        param.am_apm_asm_code.push($(this).val());
    });
    $("#msr_md_se_code :selected").each(function () {
        param.msr_md_se_code.push($(this).val());
    });
    $("#segment :selected").each(function () {
        param.segment.push($(this).val());
    });
    $("#customer_code :selected").each(function () {
        param.customer_code.push($(this).val());
    });
    $("#layanan_group :selected").each(function () {
        param.layanan_group.push($(this).val());
    });
    $("#layanan_code :selected").each(function () {
        param.layanan_code.push($(this).val());
    });
    $("#lini_code :selected").each(function () {
        param.lini_code.push($(this).val());
    });
    $("#material_group1_code :selected").each(function () {
        param.material_group1_code.push($(this).val());
    });
    $("#material_group2_code :selected").each(function () {
        param.material_group2_code.push($(this).val());
    });
    $("#material_group3_code :selected").each(function () {
        param.material_group3_code.push($(this).val());
    });
    $("#brand :selected").each(function () {
        param.brand.push($(this).val());
    });
    $("#material_code :selected").each(function () {
        param.material_code.push($(this).val());
    });
    param.start = $("#tanggal_faktur_start").val();
    param.end = $("#tanggal_faktur_end").val();
    selectedFilter(id_filter, name)
}

function selectedFilter(id_filter, name) {
    var result = getJsonFilter(path + '/getfiltervalue', {
        id: id_filter,
        param: JSON.stringify(param),
        name: name
    });
    domoption(result, id_filter, id_filter);
}

function selectedfilter(id_filter) {
    var branch = [];
    var brand = [];
    $("#lini_code :selected").each(function () {
        branch.push($(this).val());
    });

    $("#brand :selected").each(function () {
        brand.push($(this).val());
    });

    var branch_code = getJsonFilter(path + '/getfiltervalue', {
        id: id_filter,
        param: JSON.stringify(branch),
        brand: JSON.stringify(brand)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_branch(id_filter) {
    var distributor = [];

    $("#distributor_code :selected").each(function () {
        distributor.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_branch', {
        id: id_filter,
        param: JSON.stringify(distributor)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_am_apm_asm(id_filter) {
    var am_apm_asm = [];

    // $("#distributor_code :selected").each(function() {
    //     am_apm_asm.push($(this).val());
    // });
    var branch_code = getJsonFilter(path + '/getfiltervalue_am_apm_asm', {
        id: id_filter,
        param: JSON.stringify(am_apm_asm)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_gm(id_filter) {
    var branch = [];

    $("#gpm_pm_code :selected").each(function () {
        branch.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_gm', {
        id: id_filter,
        param: JSON.stringify(branch)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_group1(id_filter) {
    var branch = [];

    $("#lini_code :selected").each(function () {
        branch.push($(this).val());
    });

    var branch_code = getJsonFilter(path + '/getfiltervalue_group1', {
        id: id_filter,
        param: JSON.stringify(branch)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_group2(id_filter) {
    var branch = [];

    $("#lini_code :selected").each(function () {
        branch.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_group2', {
        id: id_filter,
        param: JSON.stringify(branch)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_group3(id_filter) {
    var branch = [];

    $("#lini_code :selected").each(function () {
        branch.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_group3', {
        id: id_filter,
        param: JSON.stringify(branch)
    });
    domoption(branch_code, id_filter, id_filter);
}

function index(element, index, array) {
    return (element.customer_code == idcs);
}

$("#customer_code").on("select2:unselect", function (evt) {
    idcs = evt.params.data.id
    customer_selected.splice(customer_selected.findIndex(index), 1)
    var newOption = domoption(customer_selected, id_filter, id_filter);
    $("#customer_code > option").prop("selected", "selected"); // Select All Options
    $("#customer_code").trigger("change"); // Trigger change to select 2
    $('.customer_code .select_customer').val('')

    evt.params.originalEvent.stopPropagation();
});


$(".customer_code .select_customer").change(function () {
    search = $('.customer_code .select_customer').val();

});

function selected_customer() {

    arraySelected = [];
    id_filter = 'customer_code'
    let customer_selected_temp = Array();
    const result = customer_selected_temp.filter(obj => obj.customer_code === $('.customer_code .select_customer').val());

    console.log("res selected_customer = ", result);
    // ========================================= initial code ==========================================
    if (result.length)
        arraySelected = []
    if (customer_selected.filter(obj => obj.customer_code === result[0].customer_code).length < 1) {
        arraySelected.push(result[0].customer_code)
        customer_selected.push(result[0])
    }
    // ==================================================================================================

    // if (result)
    //     arraySelected = []
    // if (customer_selected.filter(obj => obj.customer_code === result[0].customer_code).length == 1) {
    //     arraySelected.push(result[0].customer_code)
    //     customer_selected.push(result[0])
    // }

    // }    
    var newOption = domoption(customer_selected, id_filter, id_filter);
    $("#customer_code > option").prop("selected", "selected"); // Select All Options
    $("#customer_code").trigger("change"); // Trigger change to select 2
    $('.customer_code .select_customer').val('')
}

function selectedfilter_customer(id_filter) {
    $('branch_code').val()
    $('#select_customer')
        .empty();
    var segment = [];
    $('.please_wait').show()
    $("#segment :selected").each(function () {

        segment.push($(this).val());
    });
    customer_selected_temp = getJsonFilter(path + '/getfiltervalue_customer', {
        id: search ? search : '',
        param: JSON.stringify(segment)
    });
    // domoption(branch_code,id_filter,id_filter);

    customer_selected_temp.forEach(function (item) {
        $('#select_customer').append(' <option value="' + item.customer_code + '">' + item.customer_name + '</option>')
    });
    $('.please_wait').hide()
    //   $('#customer_code').val(['10016148', '10017438']);
}

function selectedfilter_layanan(id_filter) {
    var layanan_group = [];

    $("#layanan_group :selected").each(function () {
        layanan_group.push($(this).val());
    });
    console.log(layanan_group);
    var branch_code = getJsonFilter(path + '/getfiltervalue_layanan', {
        id: id_filter,
        param: JSON.stringify(layanan_group)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_lini(id_filter) {
    var layanan = [];

    $("#layanan_code :selected").each(function () {
        layanan.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_lini', {
        id: id_filter,
        param: JSON.stringify(layanan)
    });
    domoption(branch_code, id_filter, id_filter);
}

function selectedfilter_product(id_filter) {
    var brand = [];

    $("#brand :selected").each(function () {
        brand.push($(this).val());
    });
    var branch_code = getJsonFilter(path + '/getfiltervalue_product', {
        id: id_filter,
        param: JSON.stringify(brand)
    });
    domoption(branch_code, id_filter, id_filter);
}

function domoption(data, id, re) {
    jQuery("#" + id + " option").remove();

    ///jQuery("#"+id).append("<option value=''>(All)</option>")

    jQuery.each(data, function (key, val) {

        if (re == 'layanan_code') {
            jQuery("#" + id).append("<option value='" + val.layanan_code + "'>" + val.layanan_name + "(" + val.layanan_code + ")</option>")
        }
        if (re == 'lini_code') {
            jQuery("#" + id).append("<option value='" + val.lini_code + "'>" + val.lini_name + "(" + val.lini_code + ")</option>")
        }
        if (re == 'distributor_code') {
            jQuery("#" + id).append("<option value='" + val.distributor_code + "'>" + val.distributor_name + "(" + val.distributor_code + ")</option>")
        }
        if (re == 'branch_code') {
            jQuery("#" + id).append("<option value='" + val.branch_code + "'>" + val.branch_name + "(" + val.branch_code + ")</option>")
        }
        if (re == 'gpm_pm_code') {
            jQuery("#" + id).append("<option value='" + val.gpm_pm_code + "'>" + val.jabatan_pm + "(" + val.gpm_pm_code + ")</option>")
        }
        if (re == 'rsm_code') {
            jQuery("#" + id).append("<option value='" + val.rsm_code + "'>" + val.jabatan_rsm + "(" + val.rsm_code + ")</option>")
        }
        if (re == 'shopper_code') {
            jQuery("#" + id).append("<option value='" + val.shopper_code + "'>" + val.jabatan_shopper + "(" + val.shopper_code + ")</option>")
        }
        if (re == 'am_apm_asm_code') {
            jQuery("#" + id).append("<option value='" + val.am_apm_asm_code + "'>" + val.jabatan_am + "(" + val.am_apm_asm_code + ")</option>")
        }
        if (re == 'msr_md_se_code') {
            jQuery("#" + id).append("<option value='" + val.msr_md_se_code + "'>" + val.msr_md_se_code + "(" + val.msr_md_se_code + ")</option>")
        }
        if (re == 'customer_code') {
            jQuery("#" + id).append("<option value='" + val.customer_code + "'>" + val.customer_name + "(" + val.customer_code + ")</option>")
        }
        if (re == 'material_group1_code') {

            jQuery("#" + id).append("<option value='" + val.material_group1_code + "'>" + val.material_group1_name + "(" + val.material_group1_code + ")</option>")
        }
        if (re == 'material_group2_code') {

            jQuery("#" + id).append("<option value='" + val.material_group2_code + "'>" + val.material_group2_name + "(" + val.material_group2_code + ")</option>")
        }
        if (re == 'material_group3_code') {
            jQuery("#" + id).append("<option value='" + val.material_group3_code + "'>" + val.material_group3_name + "(" + val.material_group3_code + ")</option>")
        }
        if (re == 'segment') {
            jQuery("#" + id).append("<option value='" + val.segment + "'>" + val.segment + "</option>")
        }
        if (re == 'brand') {
            jQuery("#" + id).append("<option value='" + val.brand + "'>" + val.brand + "</option>")
        }
        if (re == 'material_code') {
            jQuery("#" + id).append("<option value='" + val.material_code + "'>" + val.material_name + " (" + val.material_code + ")</option>")
        }
        if (re == 'layanan_group') {
            jQuery("#" + id).append("<option value='" + val.layanan_group + "'>" + val.layanan_group + "(" + val.layanan_group + ")</option>")
        }

    })
}

function loadingswal() {
    var myhost = '<?= base_url() ?>';
    var el = document.createElement("img");
    el.src = "" + myhost + "/assets/images/icon/loadingrole.gif";
    swal({
        title: "Loading",
        content: el,

        closeOnClickOutside: false,
        closeOnEsc: false,
        allowOutsideClick: false,
        buttons: false,
        showCancelButton: false,
        showConfirmButton: false,
        dangerMode: true,
    })
}

function chartdrawline(idcanvas, datac, labeling, labelhead, minvalues, maxvalues) {
    var chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)',
        aquamarine: 'rgb(127,255,212)',
        cyan: 'rgb(127,255,212)',
        mediumturquoise: 'rgb(72,209,204)',
        teal: 'rgb(0,128,128)'
    };
    Chart.elements.Rectangle.prototype.draw = function () {
        var ctx = this._chart.ctx;
        var vm = this._view;
        var left, right, top, bottom, signX, signY, borderSkipped, radius;
        var borderWidth = vm.borderWidth;

        // If radius is less than 0 or is large enough to cause drawing errors a max
        //      radius is imposed. If cornerRadius is not defined set it to 0.
        var cornerRadius = this._chart.config.options.cornerRadius;
        var fullCornerRadius = this._chart.config.options.fullCornerRadius;
        var stackedRounded = this._chart.config.options.stackedRounded;
        var typeOfChart = this._chart.config.type;

        if (cornerRadius < 0) {
            cornerRadius = 0;
        }
        if (typeof cornerRadius == 'undefined') {
            cornerRadius = 0;
        }
        if (typeof fullCornerRadius == 'undefined') {
            fullCornerRadius = true;
        }
        if (typeof stackedRounded == 'undefined') {
            stackedRounded = false;
        }

        if (!vm.horizontal) {
            // bar
            left = vm.x - vm.width / 2;
            right = vm.x + vm.width / 2;
            top = vm.y;
            bottom = vm.base;
            signX = 1;
            signY = bottom > top ? 1 : -1;
            borderSkipped = vm.borderSkipped || 'bottom';
        } else {
            // horizontal bar
            left = vm.base;
            right = vm.x;
            top = vm.y - vm.height / 2;
            bottom = vm.y + vm.height / 2;
            signX = right > left ? 1 : -1;
            signY = 1;
            borderSkipped = vm.borderSkipped || 'left';
        }

        // Canvas doesn't allow us to stroke inside the width so we can
        // adjust the sizes to fit if we're setting a stroke on the line
        if (borderWidth) {
            // borderWidth shold be less than bar width and bar height.
            var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
            borderWidth = borderWidth > barSize ? barSize : borderWidth;
            var halfStroke = borderWidth / 2;
            // Adjust borderWidth when bar top position is near vm.base(zero).
            var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
            var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
            var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
            var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
            // not become a vertical line?
            if (borderLeft !== borderRight) {
                top = borderTop;
                bottom = borderBottom;
            }
            // not become a horizontal line?
            if (borderTop !== borderBottom) {
                left = borderLeft;
                right = borderRight;
            }
        }

        ctx.beginPath();
        ctx.fillStyle = vm.backgroundColor;
        ctx.strokeStyle = vm.borderColor;
        ctx.lineWidth = borderWidth;

        // Corner points, from bottom-left to bottom-right clockwise
        // | 1 2 |
        // | 0 3 |
        var corners = [
            [left, bottom],
            [left, top],
            [right, top],
            [right, bottom]
        ];

        // Find first (starting) corner with fallback to 'bottom'
        var borders = ['bottom', 'left', 'top', 'right'];
        var startCorner = borders.indexOf(borderSkipped, 0);
        if (startCorner === -1) {
            startCorner = 0;
        }

        function cornerAt(index) {
            return corners[(startCorner + index) % 4];
        }

        // Draw rectangle from 'startCorner'
        var corner = cornerAt(0);
        ctx.moveTo(corner[0], corner[1]);


        var nextCornerId, nextCorner, width, height, x, y;
        for (var i = 1; i < 4; i++) {
            corner = cornerAt(i);
            nextCornerId = i + 1;
            if (nextCornerId == 4) {
                nextCornerId = 0
            }

            nextCorner = cornerAt(nextCornerId);

            width = corners[2][0] - corners[1][0];
            height = corners[0][1] - corners[1][1];
            x = corners[1][0];
            y = corners[1][1];

            var radius = cornerRadius;
            // Fix radius being too large
            if (radius > Math.abs(height) / 2) {
                radius = Math.floor(Math.abs(height) / 2);
            }
            if (radius > Math.abs(width) / 2) {
                radius = Math.floor(Math.abs(width) / 2);
            }

            var x_tl, x_tr, y_tl, y_tr, x_bl, x_br, y_bl, y_br;
            if (height < 0) {
                // Negative values in a standard bar chart
                x_tl = x;
                x_tr = x + width;
                y_tl = y + height;
                y_tr = y + height;

                x_bl = x;
                x_br = x + width;
                y_bl = y;
                y_br = y;

                // Draw
                ctx.moveTo(x_bl + radius, y_bl);

                ctx.lineTo(x_br - radius, y_br);

                // bottom right
                ctx.quadraticCurveTo(x_br, y_br, x_br, y_br - radius);


                ctx.lineTo(x_tr, y_tr + radius);

                // top right
                fullCornerRadius ? ctx.quadraticCurveTo(x_tr, y_tr, x_tr - radius, y_tr) : ctx.lineTo(x_tr, y_tr, x_tr - radius, y_tr);


                ctx.lineTo(x_tl + radius, y_tl);

                // top left
                fullCornerRadius ? ctx.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + radius) : ctx.lineTo(x_tl, y_tl, x_tl, y_tl + radius);


                ctx.lineTo(x_bl, y_bl - radius);

                //  bottom left
                ctx.quadraticCurveTo(x_bl, y_bl, x_bl + radius, y_bl);

            } else if (width < 0) {
                // Negative values in a horizontal bar chart
                x_tl = x + width;
                x_tr = x;
                y_tl = y;
                y_tr = y;

                x_bl = x + width;
                x_br = x;
                y_bl = y + height;
                y_br = y + height;

                // Draw
                ctx.moveTo(x_bl + radius, y_bl);

                ctx.lineTo(x_br - radius, y_br);

                //  Bottom right corner
                fullCornerRadius ? ctx.quadraticCurveTo(x_br, y_br, x_br, y_br - radius) : ctx.lineTo(x_br, y_br, x_br, y_br - radius);

                ctx.lineTo(x_tr, y_tr + radius);

                // top right Corner
                fullCornerRadius ? ctx.quadraticCurveTo(x_tr, y_tr, x_tr - radius, y_tr) : ctx.lineTo(x_tr, y_tr, x_tr - radius, y_tr);

                ctx.lineTo(x_tl + radius, y_tl);

                // top left corner
                ctx.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + radius);

                ctx.lineTo(x_bl, y_bl - radius);

                //  bttom left corner
                ctx.quadraticCurveTo(x_bl, y_bl, x_bl + radius, y_bl);

            } else {

                var lastVisible = 0;
                for (var findLast = 0, findLastTo = this._chart.data.datasets.length; findLast < findLastTo; findLast++) {
                    if (!this._chart.getDatasetMeta(findLast).hidden) {
                        lastVisible = findLast;
                    }
                }
                var rounded = this._datasetIndex === lastVisible;

                if (rounded) {
                    //Positive Value
                    ctx.moveTo(x + radius, y);

                    ctx.lineTo(x + width - radius, y);

                    // top right
                    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);


                    ctx.lineTo(x + width, y + height - radius);

                    // bottom right
                    if (fullCornerRadius || typeOfChart == 'horizontalBar')
                        ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                    else
                        ctx.lineTo(x + width, y + height, x + width - radius, y + height);


                    ctx.lineTo(x + radius, y + height);

                    // bottom left
                    if (fullCornerRadius)
                        ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                    else
                        ctx.lineTo(x, y + height, x, y + height - radius);


                    ctx.lineTo(x, y + radius);

                    // top left
                    if (fullCornerRadius || typeOfChart == 'bar')
                        ctx.quadraticCurveTo(x, y, x + radius, y);
                    else
                        ctx.lineTo(x, y, x + radius, y);
                } else {
                    ctx.moveTo(x, y);
                    ctx.lineTo(x + width, y);
                    ctx.lineTo(x + width, y + height);
                    ctx.lineTo(x, y + height);
                    ctx.lineTo(x, y);
                }
            }

        }

        ctx.fill();
        if (borderWidth) {
            ctx.stroke();
        }
    };


    var data = {
        labels: labeling,
        datasets: [{
            label: labelhead,
            data: datac,
            backgroundColor: [chartColors.blue, chartColors.teal, chartColors.teal],
            borderWidth: 0
        }]
    };
    var options = {
        tooltips: {
            callbacks: {
                label: function (tooltipItem) {
                    return minutesToHours(tooltipItem.yLabel);
                }
            }
        },
        legend: {
            labels: {
                filter: function (label) {
                    return false; //only show when the label is cash
                }
            }
        },
        elements: {
            point: {
                radius: 25,
                hoverRadius: 35,
                pointStyle: 'rectRounded',

            }
        },
        cornerRadius: 10,
        fullCornerRadius: false,
        scales: {
            yAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true,
                    userCallback: function (item) {
                        return minutesToHours(item);
                    },
                    /* steps: minvalues,
                     stepValue: 5,
                     max: maxvalues*/
                },
                stacked: true,
                radius: 25
            }],
            xAxes: [{
                gridLines: {
                    display: false
                },
                ticks: {
                    beginAtZero: true
                },
                stacked: true,

            }]
        }
    };




    var ctxBar = document.getElementById(idcanvas);
    var myBarChart = new Chart(ctxBar, {
        type: 'bar',
        data: data,
        options: options,

    });

}

function minutesToHours(str) {
    //var str  = str;
    switch (String(str).length) {
        case 2:
            return String(str);
            break;
        case 3:
            return String(str);
            break;
        case 4:
            return String(str);
            break;
        case 5:
            return String(str);
            break;
        case 6:
            return String(str);
            break;
        case 7:
            //return substr($angka[0], 0,1).",".substr($angka[0], 1,2)." Juta";
            // return String(str).substr(0,1)+" Juta"
            return "Rp. " + String(str).substr(0, 1) + "," + String(str).substr(1, 2) + " Juta";
            break;
        case 8:
            //return substr($angka[0], 0,2).",".substr($angka[0], 2,2)." Juta";
            return "Rp. " + String(str).substr(0, 2) + "," + String(str).substr(2, 2) + " Juta";
            break;
        case 9:
            //return substr($angka[0], 0,3).",".substr($angka[0], 3,2)." Juta";
            return "Rp. " + String(str).substr(0, 3) + "," + String(str).substr(3, 2) + " Juta";
            break;
        case 10:
            //return substr($angka[0], 0,1).",".substr($angka[0], 1,2)." Milyar";
            return "Rp. " + String(str).substr(0, 1) + "," + String(str).substr(1, 2) + " Milyar";
            break;
        case 11:
            //return substr($angka[0], 0,2).",".substr($angka[0], 2,2)." Milyar";
            return "Rp. " + String(str).substr(0, 2) + "," + String(str).substr(2, 2) + " Milyar";
            break;
        case 12:
            //return substr($angka[0], 0,3)." Milyar";
            //return String(str).substr(0,3)+" Milyar"
            return "Rp. " + String(str).substr(0, 3) + "," + String(str).substr(3, 2) + " Milyar";
            break;
        case 13:
            //return substr($angka[0], 0,1)." Triliun";
            //return String(str).substr(0,1)+" Triliun"
            return "Rp. " + String(str).substr(0, 1) + "," + String(str).substr(1, 2) + " Triliun";
            break;
        case 14:
            //return substr($angka[0], 0,2)." Triliun";
            //return String(str).substr(0,2)+" Triliun"
            return "Rp. " + String(str).substr(0, 2) + "," + String(str).substr(2, 2) + " Triliun";
            break;
        case 15:
            // return substr($angka[0], 0,3)." Triliun";
            // return String(str).substr(0,3)+" Triliun"
            return "Rp. " + String(str).substr(0, 3) + "," + String(str).substr(3, 2) + " Triliun";
            break;

        default:
            //return $angka[0].",00";
            return "Rp. " + String(str);
    }


    // return res;
}

/* generate a download */
function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function csv() {
    const fileName = 'Sales Information ' + jQuery('#tanggal_faktur_start').val().replace(/\//g, "") + ' sd ' + jQuery('#tanggal_faktur_end').val().replace(/\//g, "") + '.csv';
    const ws = XLSX.utils.json_to_sheet(newRow_, {
        skipHeader: true
    });
    var csv = XLSX.utils.sheet_to_csv(ws);
    saveAs(new Blob([s2ab(csv)], {
        type: "application/octet-stream"
    }), fileName);
}

function excel() {
    const fileName = 'Sales Information ' + jQuery('#tanggal_faktur_start').val().replace(/\//g, "") + ' sd ' + jQuery('#tanggal_faktur_end').val().replace(/\//g, "") + '.xlsx';
    const ws = XLSX.utils.json_to_sheet(newRow_, {
        skipHeader: true
    });
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'test');
    XLSX.writeFile(wb, fileName);
    return false;
}